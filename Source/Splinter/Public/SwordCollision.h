// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SwordCollision.generated.h"

class ASwordMover;
class UCapsuleComponent;

USTRUCT(BlueprintType)
struct FSwordCollisionProps
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	float ColliderSize;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float SwordLength;

	UPROPERTY(EditAnywhere, Category = "Deflect Strength")
	float BaseDeflectStrength;
	UPROPERTY(EditAnywhere, Category = "Deflect Strength")
	float SwordSpeedMultiplier;
	UPROPERTY(EditAnywhere, Category = "Deflect Strength")
	float IsForwardMultiplier;


};

UCLASS()
class SPLINTER_API ASwordCollision : public AActor
{
	GENERATED_BODY()
	
public:	
	ASwordCollision();

	void Init(ASwordMover* SwordMoverToLink, FSwordCollisionProps& props);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
	ASwordMover* SwordMover;

	UPROPERTY(EditAnywhere)
	float ColliderSize = 1.0f;
	UPROPERTY(EditAnywhere)
	float SwordLength = 70.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Deflect Strength")
	float BaseDeflectStrength;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Deflect Strength")
	float SwordSpeedMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Deflect Strength")
	float IsForwardMultiplier;

	UFUNCTION(BlueprintCallable)
	void LinkSwordMover(ASwordMover* SwordMoverToLink);

private:
	UFUNCTION()
	void HandleSwordCollision(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void HandleTrailCollision(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	void HandleCollision(AActor* OtherActor);

	UFUNCTION()
	void HandleSpawnTrail(AActor* SpawnedTrail);

	UCapsuleComponent* Collision;
};
