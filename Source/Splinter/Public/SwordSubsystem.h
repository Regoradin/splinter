// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SwordSubsystem.generated.h"

class ASwordMover;

UCLASS()
class SPLINTER_API ASwordSubsystem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASwordSubsystem();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ASwordMover* Sword;

	UFUNCTION()
	void Init(ASwordMover* OwningSword);

	UFUNCTION(BlueprintImplementableEvent)
	void OnSwordSubsystemInit();


	// Audio events
	// Called on the first frame of a slash
	UFUNCTION(BlueprintImplementableEvent)
	void OnStartSlash();

	// Called on the first frame of a stab
	UFUNCTION(BlueprintImplementableEvent)
	void OnStartStab();

	// Called when this sword is hit (presumably by another sword)
	UFUNCTION(BlueprintImplementableEvent)
	void OnGetHit();


};
