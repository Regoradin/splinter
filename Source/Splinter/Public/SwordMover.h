// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include"SwordCollision.h"
#include "PaperSprite.h"
#include "SwordMover.generated.h"

class ASwordSubsystem;


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FSpawnTrailEvent, AActor*, SpawnedTrail);

UCLASS(Blueprintable)
class SPLINTER_API ASwordMover : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Sword")
	FSwordCollisionProps CollisionProps;

	ASwordMover();
	//The distance between the two opponents
	UPROPERTY(EditAnywhere, Category = "Sword")
	float WieldingDistance;
	UPROPERTY(EditAnywhere, Category = "Sword")
	float OppositionDistance;

	UPROPERTY(EditAnywhere, Category = "Sword|Interpolation");
	float SpringConstant;
	UPROPERTY(EditAnywhere, Category = "Sword|Interpolation")
	float Damping;
	UPROPERTY(EditAnywhere, Category = "Sword|Interpolation")
	float Bounce;

	UPROPERTY(EditAnywhere, Category = "Sword|Slashing")
	float SlashingSpeedThreshold;
	UPROPERTY(EditAnywhere, Category = "Sword|Slashing")
	float SlashDuration;
	UPROPERTY(EditAnywhere, Category = "Sword|Slashing")
	TSubclassOf<AActor> SlashActorClass;

	UPROPERTY(EditAnywhere, Category = "Sword|Stabbing")
	float StabDistance;

	UPROPERTY(EditAnywhere, Category = "Sword|Stun Taken")
	float StunMultiplier;
	UPROPERTY(EditAnywhere, Category = "Sword|Stun Taken")
	float AddedStunTimeWhenDeflected;
	UPROPERTY(EditAnywhere, Category = "Sword|Pushback Taken")
	float PushBackTime;
	UPROPERTY(EditAnywhere, Category = "Sword")
	TArray<TSubclassOf<ASwordSubsystem>>  SwordSubsystems;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sword|Sword Circle")
	UPaperSprite* SwordCircleSprite;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sword|Sword Circle")
	float SwordCircleSize;

	UFUNCTION(BlueprintCallable)
	void InitializeToPlayerController(APlayerController* PlayerController);

	UFUNCTION(BlueprintCallable)
	void MoveSwordTowardsTargetPosition(float MouseX, float MouseY, bool bIsStabbing, float DeltaTime, bool bIsOpposed = false);

	UFUNCTION(BlueprintCallable)
	FVector GetSwordPosition();

	UFUNCTION(BlueprintCallable)
	FVector2D GetSwordPositionNormalizedScreenSpace();
	
	UFUNCTION(BlueprintCallable)
	FVector GetSwordTargetPosition();

	UFUNCTION(BlueprintCallable)
	FVector GetSwordVelocity();

	UFUNCTION(BlueprintCallable)
	FVector2D GetSwordScreenSpaceVelocity();

	UFUNCTION(BlueprintCallable)
	void SetSwordPosition(FVector NewPosition);

	FString ToString();

	UFUNCTION(BlueprintCallable)
	void Deflect(FVector Direction);

	UFUNCTION(BlueprintCallable)
	void PushBack();

	// Whether the sword is being used to slash
	bool bIsSlashing;
	//TODO: might we at some point want to differentiate between a sword held forward and a sword mid-stab?
	// Whether the sword is currently moving or moved forward towards its opponent
	bool bIsForward;

	FSpawnTrailEvent OnSpawnTrail;

	UFUNCTION(BlueprintCallable)
	float GetSlashSpeed();

	void OnGetHit();

	UFUNCTION(BlueprintCallable)
	bool GetIsStunned();
	UFUNCTION(BlueprintCallable)
	bool GetIsPushedBack();

protected:
	void BeginPlay() override;

private:
	TArray<ASwordSubsystem*> SubsystemInstances;

	APlayerController* EquippedPlayerController;

	FVector TargetPosition;
	FVector TargetVelocity;

	FVector Position;
	FVector Velocity;
	FVector ExternalForce;

	FVector2D LastFrameScreenPosition;
	FVector2D ScreenSpaceVelocity;


	float StunTime;
	float PushedBackTime;

	void UpdateTargetPosition(float MouseX, float MouseY, bool bIsOpposed, float DeltaTime);
	void MoveSword(bool bIsStabbing, float DeltaTime);

};