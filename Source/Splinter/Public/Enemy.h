// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Containers/Array.h"
#include "Engine/LatentActionManager.h"
#include "Enemy.generated.h"

class ASwordMover;
	
UENUM()
enum class EFightingStance
{
	Defensive,
	Aggressive
};

UCLASS()
class SPLINTER_API AEnemy : public AActor
{
	GENERATED_BODY()

public:
	AEnemy();
	virtual void Tick(float DeltaTime) override;


protected:
	virtual void BeginPlay() override;

public:
	UPROPERTY(EditAnywhere)
	ASwordMover* Sword;

	UPROPERTY(EditAnywhere)
	AActor* SwordPointVisual;

	//AI PARAMETERS
	
	//How ofter to consider changing stances
	UPROPERTY(EditAnywhere, Category = "AI Parameters")
	float StanceChangeInterval;
	//How likely it is that the AI changes stances
	UPROPERTY(EditAnywhere, Category = "AI Parameters", meta = (ClampMin = 0.0f, ClampMax = 1.0f))
	float StanceChangeChance;

	//Range of durations of random movements
	UPROPERTY(EditAnywhere, Category = "AI Parameters")
	FVector2D RandomMoveDurationRange;
	//How much screen space border there is to random movemements
	UPROPERTY(EditAnywhere, Category = "AI Parameters", meta = (ClampMin = 0.0f, CampMax = 0.5f))
	float RandomMoveBorder;
	//Range of durations of slashes
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Aggressive")
	FVector2D DiagonalSlashDurationRange;
	//How aggressively diagonal slashes will target the opposite corner
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Aggressive", meta=(ClampMin = 0.0f, ClampMax = 0.5f))
	float DiagonalSlashCornerPreference;
	//How close to the center of the screen the sword has to be in order to consider stabbing, in screen space
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Aggressive", meta = (ClampMin = 0.0f, ClampMax = 0.5f))
	float StabZoneBorder;
	//Range of durations of stabs
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Aggressive")
	FVector2D StabDurationRange;
	// How close to a corner the sword needs to be before an aggressive slash is considered
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Aggressive", meta = (ClampMin = 0.0f, ClampMax = 0.5f));
	float SlashRange;
	// How fast the sword is moved towards the opponent when defending
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive")
	float DefensiveTrackingSpeed;
	// How fast the player needs to be moving to provoke a defensive slash
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive")
	float DefensiveSlashingSpeedThreshold;
	// How likely you are to respond to high speeds with a defensive slash
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive", meta = (ClampMin = 0.0f, ClampMax = 1.0f))
	float DefensiveSlashChance;
	// How far ahead of the opponent a defensive slash is done
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive")
	float DefensiveSlashOffset;
	// How big the defense slash is
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive")
	float DefensiveSlashSize;
	// Range of durations of defensive slashes are
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive")
	FVector2D DefensiveSlashDurationRange;
	//Chance of defending against stabs rather than wandering aimlessly
	UPROPERTY(EditAnywhere, Category = "AI Parameters|Defensive", meta = (ClampMin = 0.0f, ClampMax = 1.0f))
	float DefendAgainstStabChance;

	UFUNCTION()
	void SetTargetPoint(FVector2D TargetPoint);

	UFUNCTION()
	void SetIsStabbing(bool bIsStabbing);

	UFUNCTION()
	void SetIsBusy(bool isBusy);

private:
	APlayerController* PlayerController;

	ASwordMover* OtherSword;

	TArray<FVector2D> TargetPoints = { FVector2D(0.25f, 0.25f),
									   FVector2D(0.25f, 0.75f),
									   FVector2D(0.75f, 0.75f),
									   FVector2D(0.75f, 0.25f) };
	int TargetIndex;

	EFightingStance FightingStance;

	bool bIsStabbing;
	FVector2D TargetPoint;

	bool bIsBusy;

	void ConsiderChangingStance();

	void StartRandomMove(bool bIsAggressiveSlash);

	/* Aggressive Stance*/

	void StartDiagonalSlash();

	// If we have a decent chance of stabbing if we stab right now. Eventually factor in opponent location
	bool IsThreateningPlayer();

	void Stab();
	
	// If we are non-central enough to do a good slash
	bool IsSetupForSlash();

	/* Defensive Stance */
	// Move towards the opponents sword, with some offset. Try to make this move roughly as long as it takes to get there, to allow for quick responsiveness without going so fast as to lose control or scare the opponent away
	void MoveTowardsOpponent();

	// Slash near you against the direction of their slash, so as to parry
	void SlashAcrossOpponent();

	UFUNCTION()
	void ChangeTarget();

};

class FSwordManeuverLatentAction : public FPendingLatentAction
{
private:
	AEnemy* Enemy;
	float TimeElapsed;
	float TotalTime;


	FVector2D StartPosition;
	FVector2D EndPosition;

	// maybe nuance this such that we can stab for some but not all of the maneuver
	bool bIsStabbing;
	// set to true if the maneuver should end in a stab forward
	bool bEndInStab;

	FString ActionName;

public:
	FSwordManeuverLatentAction(
		AEnemy* Enemy,
		float TotalTime,
		FVector2D StartPosition,
		FVector2D EndPosition,
		bool bIsStabbing,
		FString ActionName = FString(TEXT("")),
		bool bEndInStab = false
		) : Enemy(Enemy), TimeElapsed(0.0f), TotalTime(TotalTime), StartPosition(StartPosition), EndPosition(EndPosition), bIsStabbing(bIsStabbing), bEndInStab(bEndInStab), ActionName(ActionName)
	{
		Enemy->SetIsBusy(true);
	};

	~FSwordManeuverLatentAction() {
		Enemy->SetIsBusy(false);
	}

	virtual void UpdateOperation(FLatentResponse& Response) override
	{
		TimeElapsed += Response.ElapsedTime();		
		
		FVector2D TargetPosition = FMath::Lerp(StartPosition, EndPosition, TimeElapsed / TotalTime);
		Enemy->SetTargetPoint(TargetPosition);
		Enemy->SetIsStabbing(bIsStabbing);

		GEngine->AddOnScreenDebugMessage(0, 0.1, FColor::Green, FString(TEXT("Action: ")) + ActionName + FString(TEXT(" Time remaining: ")) + FString::SanitizeFloat(TotalTime - TimeElapsed));
		bool bIsFinished = TimeElapsed > TotalTime;
		if (bIsFinished)
		{
			Enemy->SetIsStabbing(bEndInStab);
		}
		Response.DoneIf(bIsFinished);
	}
};