// Fill out your copyright notice in the Description page of Project Settings.


#include "SwordCollision.h"
#include "Components/CapsuleComponent.h"
#include "SwordMover.h"


void ASwordCollision::Init(ASwordMover* SwordMoverToLink, FSwordCollisionProps& props)
{
	SwordMover = SwordMoverToLink;
	SwordMover->OnSpawnTrail.AddDynamic(this, &ASwordCollision::HandleSpawnTrail);

	//little bit of JS sneaking in here.. this is almost certainly an anti-pattern
	ColliderSize = props.ColliderSize;
	SwordLength = props.SwordLength;
	BaseDeflectStrength = props.BaseDeflectStrength;
	SwordSpeedMultiplier = props.SwordSpeedMultiplier;
	IsForwardMultiplier = props.IsForwardMultiplier;

	Collision->SetCapsuleSize(ColliderSize, SwordLength);
}

ASwordCollision::ASwordCollision()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collision = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollision"));
	Collision->AddLocalRotation(FRotator::MakeFromEuler(FVector(0.0f, 90.0f, 0.0f)));
	Collision->SetCapsuleSize(ColliderSize, SwordLength);
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Collision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	Collision->SetupAttachment(RootComponent);
	
	Collision->OnComponentBeginOverlap.AddDynamic(this, &ASwordCollision::HandleSwordCollision);
}

void ASwordCollision::LinkSwordMover(ASwordMover* SwordMoverToLink)
{
	//This is different from the one in the constructor, because constructors get a different component creation thing.
	SwordMover = SwordMoverToLink;

	Collision = Cast<UCapsuleComponent>(AddComponentByClass(UCapsuleComponent::StaticClass(), false, FTransform::Identity, false));
	Collision->AddLocalRotation(FRotator::MakeFromEuler(FVector(0.0f, 90.0f, 0.0f)));
	Collision->SetCapsuleSize(ColliderSize, SwordLength);
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Collision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);


	Collision->OnComponentBeginOverlap.AddDynamic(this, &ASwordCollision::HandleSwordCollision);
}

void ASwordCollision::HandleSwordCollision(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	HandleCollision(OtherActor);
}

void ASwordCollision::HandleTrailCollision(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor == this) {
		// don't trigger collisions from colliding with ourselves
		return;
	}
	HandleCollision(OtherActor);
}

void ASwordCollision::HandleCollision(AActor* OtherActor)
{
	//check if you're colliding with another sword/swordCollision
	//do the logic, poke at your sword

	if (OtherActor)
	{
		ASwordCollision* OtherSwordCollision = Cast<ASwordCollision>(OtherActor);
		if (OtherSwordCollision)
		{
			UE_LOG(LogTemp, Log, TEXT("====== COLLISION OCCURING for %s ======"), *SwordMover->GetName());

			ASwordMover* OtherSword = OtherSwordCollision->SwordMover;

			FVector NoX = FVector::YAxisVector + FVector::ZAxisVector;
			FVector DeflectDirection = (OtherSword->GetSwordPosition() * NoX)  - (SwordMover->GetSwordPosition() * NoX);
			DeflectDirection.Normalize();

			float DeflectStrength = BaseDeflectStrength;
			DeflectStrength += (SwordMover->GetSwordVelocity() * NoX).Length() * SwordSpeedMultiplier;
			if (SwordMover->bIsForward)
			{
				DeflectStrength *= IsForwardMultiplier;
			}

			UE_LOG(LogTemp, Log, TEXT("DeflectDirection applied: %s"), *DeflectDirection.ToString());
			UE_LOG(LogTemp, Log, TEXT("DeflectStrength applied: %f"), DeflectStrength);

			OtherSword->PushBack();
			OtherSword->Deflect(DeflectDirection * DeflectStrength);
			OtherSword->OnGetHit();
		}
	}

}


void ASwordCollision::HandleSpawnTrail(AActor* SpawnedTrail)
{
	SpawnedTrail->OnActorBeginOverlap.AddDynamic(this, &ASwordCollision::HandleTrailCollision);
}

void ASwordCollision::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SwordMover) 
	{
		SetActorLocation(SwordMover->GetSwordPosition() - (SwordMover->GetActorForwardVector() * SwordLength));
	}
}

