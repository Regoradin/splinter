// Fill out your copyright notice in the Description page of Project Settings.


#include "SwordMover.h"
#include "Components/ShapeComponent.h"
#include "SwordSubsystem.h"

// This should probably be refactored to be ASwordEntity or something, which just has references to SwordMover, SwordSlasher, etc.
// and handles lifecycle stuff

ASwordMover::ASwordMover()
{
 	PrimaryActorTick.bCanEverTick = false;
}

float ASwordMover::GetSlashSpeed()
{
    //Do both target speed and sword speed, so we get the whole slash
    FVector NoX = FVector::YAxisVector + FVector::ZAxisVector;
	return FMath::Max(TargetVelocity.Length(), (Velocity * NoX).Length());
}

void ASwordMover::OnGetHit()
{
    for (ASwordSubsystem* Subsystem : SubsystemInstances)
    {
        Subsystem->OnGetHit();
    }
}

bool ASwordMover::GetIsStunned()
{
    return StunTime > 0;
}

bool ASwordMover::GetIsPushedBack()
{
    return PushedBackTime > 0;
}

void ASwordMover::BeginPlay()
{
    Super::BeginPlay();

    ASwordCollision* Collision = GetWorld()->SpawnActor<ASwordCollision>(ASwordCollision::StaticClass());
    Collision->Init(this, CollisionProps);

    for (TSubclassOf<ASwordSubsystem> SubsystemType : SwordSubsystems)
    {
        ASwordSubsystem* Subsystem = GetWorld()->SpawnActor<ASwordSubsystem>(SubsystemType, FVector::Zero(), FRotator::ZeroRotator, FActorSpawnParameters());
        Subsystem->Init(this);
        SubsystemInstances.Add(Subsystem);
    }

    //prevent swords pinging off into space on startup
    UpdateTargetPosition(0.5f, 0.5f, false, UE_MAX_FLT);
    SetSwordPosition(TargetPosition);
}


FVector ASwordMover::GetSwordPosition() 
{
    return Position;
}

FVector2D ASwordMover::GetSwordPositionNormalizedScreenSpace()
{
    FVector2D ScreenLocation;
    FIntPoint ScreenDimensions = GEngine->GameViewport->Viewport->GetSizeXY();
    EquippedPlayerController->ProjectWorldLocationToScreen(Position, ScreenLocation);
    ScreenLocation /= ScreenDimensions;
    return ScreenLocation;
}

FVector ASwordMover::GetSwordTargetPosition()
{
    return TargetPosition;
}

FVector ASwordMover::GetSwordVelocity()
{
    return Velocity;
}

FVector2d ASwordMover::GetSwordScreenSpaceVelocity()
{
    return ScreenSpaceVelocity;
}

void ASwordMover::SetSwordPosition(FVector NewPosition)
{
    Position = NewPosition;
    Velocity = FVector::Zero();
}

FString ASwordMover::ToString()
{
	return FString("This is my sword at position ") + GetSwordPosition().ToString();
}

void ASwordMover::Deflect(FVector Direction)
{
    UE_LOG(LogTemp, Log, TEXT("%s getting deflected: %s"), *GetActorNameOrLabel(), *Direction.ToString());
    ExternalForce += FVector(0.0f, Direction.Y, Direction.Z);
    StunTime += AddedStunTimeWhenDeflected;
}

void ASwordMover::PushBack()
{
    UE_LOG(LogTemp, Log, TEXT("%s getting pushed back!"), *GetActorNameOrLabel());
    PushedBackTime += PushBackTime;
    //immediately snap back into un-stab position
    Position = FVector(TargetPosition.X, Position.Y, Position.Z);
}

void ASwordMover::InitializeToPlayerController(APlayerController *PlayerController)
{
    this->EquippedPlayerController = PlayerController;
}

/**
* Move the sword towards the point.DeltaTime is the time since this has last been called, for interpolating velocity and such
* @param TargetPosition the position to move the sword to
* @param bIsAttacking whether the attack button is pressed, not yet implemented
* @param DelatTime time since last called, for interpolation purposes
* @param bisOpposed whether the sword is the player or the enemy. The enemy should have this set to true, which will flip the positions to oppose the camera
*/
void ASwordMover::MoveSwordTowardsTargetPosition(float MouseX, float MouseY, bool bIsStabbing, float DeltaTime, bool bIsOpposed)
{
    FVector2D NewScreenPosition = *new FVector2D(MouseX, MouseY);
    ScreenSpaceVelocity = (NewScreenPosition - LastFrameScreenPosition) / DeltaTime;
    LastFrameScreenPosition = NewScreenPosition;

    UpdateTargetPosition(MouseX, MouseY, bIsOpposed, DeltaTime);
    MoveSword(bIsStabbing, DeltaTime);
}

void ASwordMover::UpdateTargetPosition(float MouseX, float MouseY, bool bIsOpposed, float DeltaTime)
{
    float _x, _y;
    if (!EquippedPlayerController
        || !EquippedPlayerController->GetMousePosition(_x, _y))
    {
        //if we aren't equipped or the mouse isn't on the screen, don't do any of this
        return;
    }

    FVector NewTargetPosition;
    FVector WorldDirection;
    EquippedPlayerController->DeprojectScreenPositionToWorld(MouseX, MouseY, NewTargetPosition, WorldDirection);

    NewTargetPosition += WorldDirection * WieldingDistance;

    if (bIsOpposed)
    {
        // offset straight forward, such that stabbing will work predictably
        // this is subtracted because the opposed sword is facing the player. Hacky and bad but it works.
        NewTargetPosition -= GetActorForwardVector() * (OppositionDistance - (WieldingDistance * 2));
    }    

    TargetVelocity = (NewTargetPosition - TargetPosition) / DeltaTime;

    TargetPosition = NewTargetPosition;
}

void ASwordMover::MoveSword(bool bIsStabbing, float DeltaTime)
{
    //This should probably be determined better
    FVector StabOffset = FVector::Zero();
    if (bIsStabbing && !(PushedBackTime > 0))
    {
        if (!bIsForward)
        {
            for (ASwordSubsystem* Subsystem : SubsystemInstances)
            {
				Subsystem->OnStartStab();
            }
        }
        bIsForward = true;
		StabOffset = GetActorForwardVector() * StabDistance;
        // hypothetically, we should move faster forward when we're in stab mode?
        // that might require detecting when we're mid-stab, which could be a whole thing
    }
    else
    {
        bIsForward = false;
    }

    float EffectiveStunMultiplier = 1.0f;
    if (StunTime > 0)
    {
        EffectiveStunMultiplier = StunMultiplier;
        StunTime -= DeltaTime;
    }
    if (PushedBackTime > 0)
    {
        PushedBackTime -= DeltaTime;
    }

    // Sort of Verlet-esque, with some very artificial damping to get it to behave
    // TODO: Put some reasonable ranges on this, maybe divide by Bounce instead of multiplying?
    FVector Accel = (TargetPosition + StabOffset - Position) * SpringConstant * EffectiveStunMultiplier;
    Position += ((Velocity * DeltaTime) + (0.5 * Accel * DeltaTime * DeltaTime)) * Damping;
    Velocity += Accel * DeltaTime * Bounce;

    GEngine->AddOnScreenDebugMessage(
		uint64(GetActorInstanceGuid().A),
        0.1,
        FColor::Red,
        FString(TEXT("Sword velocity pre force: ")) + Velocity.ToString() + FString(TEXT(" External force: ")) + ExternalForce.ToString()
    );
	
    // Not physical, but very responsive and easy to control
	Velocity += ExternalForce;
    ExternalForce = FVector::Zero();


    if (GetSlashSpeed() > SlashingSpeedThreshold)
    {
        if (!bIsSlashing)
        {
            for (ASwordSubsystem* Subsystem : SubsystemInstances)
            {
				Subsystem->OnStartSlash();
            }
        }
        bIsSlashing = true;

        AActor* TrailElement = GetWorld()->SpawnActor<AActor>(SlashActorClass, Position, FRotator::ZeroRotator, FActorSpawnParameters());

        FTimerHandle Handle;
        FTimerDelegate DeleteCallback;
        DeleteCallback.BindLambda([=]() { TrailElement->Destroy(); });
        GetWorldTimerManager().SetTimer(Handle, DeleteCallback, 1.0, false, SlashDuration);

        OnSpawnTrail.Broadcast(TrailElement);
    }
    else
    {
        bIsSlashing = false;
    }

    GetSwordPositionNormalizedScreenSpace();
}