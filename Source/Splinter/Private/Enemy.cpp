// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "SwordMover.h"
#include "Kismet/GameplayStatics.h"

AEnemy::AEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	TargetIndex = 0;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	FightingStance = EFightingStance::Defensive;
}

void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	PrimaryActorTick.bCanEverTick = true;

	// This is going to explicitly make this a single player game, but I think that's fine
	if (Sword) {
		Sword->InitializeToPlayerController(GetWorld()->GetFirstPlayerController<APlayerController>());
		Sword->SetSwordPosition(GetActorLocation());
	}

	// Start stance deciding loop
	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(Handle, this, &AEnemy::ConsiderChangingStance, StanceChangeInterval, true);
}

void AEnemy::SetTargetPoint(FVector2D NewTargetPoint)
{	
	TargetPoint = NewTargetPoint;
}

void AEnemy::SetIsStabbing(bool NewIsStabbing)
{
	bIsStabbing = NewIsStabbing;
}

void AEnemy::SetIsBusy(bool isBusy)
{
	bIsBusy = isBusy;
}

void AEnemy::ConsiderChangingStance()
{
	//TODO: make this consider the player's actions a bit
	if (FMath::RandRange(0.0f, 1.0f) < StanceChangeChance)
	{
		if (FightingStance == EFightingStance::Defensive)
		{
			FightingStance = EFightingStance::Aggressive;
		}
		else
		{
			FightingStance = EFightingStance::Defensive;
		}
	}

}

void AEnemy::StartRandomMove(bool bIsAggressiveSlash)
{
	int32 id = FMath::RandRange(INT_MIN, INT_MAX);
	float Duration = FMath::RandRange(RandomMoveDurationRange.X, RandomMoveDurationRange.Y);
	FVector2D Start = TargetPoint;
	FVector2D End = *new FVector2D(
		FMath::RandRange(0.f + RandomMoveBorder, 1.f - RandomMoveBorder), 
		FMath::RandRange(0.f + RandomMoveBorder, 1.f - RandomMoveBorder));
	GetWorld()->GetLatentActionManager().AddNewAction(this, id, new FSwordManeuverLatentAction(this, Duration, Start, End, bIsAggressiveSlash, FString(TEXT("Moving randomly"))));
}

void AEnemy::StartDiagonalSlash()
{
	int32 id = FMath::RandRange(INT_MIN, INT_MAX);
	float Duration = FMath::RandRange(DiagonalSlashDurationRange.X, DiagonalSlashDurationRange.Y);

	//Put the end position in the opposite corner. Parameterize how far into the opposite corner?
	FVector2D Start = TargetPoint;
	FVector2D End = *new FVector2d(
		TargetPoint.X > 0.5f ? FMath::RandRange(0.0f, 0.5f - DiagonalSlashCornerPreference) : FMath::RandRange(0.5f + DiagonalSlashCornerPreference, 1.0f),
		TargetPoint.Y > 0.5f ? FMath::RandRange(0.0f, 0.5f - DiagonalSlashCornerPreference) : FMath::RandRange(0.5f + DiagonalSlashCornerPreference, 1.0f)
	);

	GetWorld()->GetLatentActionManager().AddNewAction(this, id, new FSwordManeuverLatentAction(this, Duration, Start, End, true, FString(TEXT("Diagonal Slash"))));
}

bool AEnemy::IsThreateningPlayer()
{
	FVector2D SwordPos = Sword->GetSwordPositionNormalizedScreenSpace();
	return SwordPos.X > StabZoneBorder && SwordPos.X < (1 - StabZoneBorder)
		&& SwordPos.Y > StabZoneBorder && SwordPos.Y < (1 - StabZoneBorder);
}

void AEnemy::Stab()
{
	int32 id = FMath::RandRange(INT_MIN, INT_MAX);
	float Duration = FMath::RandRange(StabDurationRange.X, StabDurationRange.Y);
	GetWorld()->GetLatentActionManager().AddNewAction(this, id, new FSwordManeuverLatentAction(this, Duration, TargetPoint, TargetPoint, true, FString(TEXT("Stab"))));
}

bool AEnemy::IsSetupForSlash()
{
	return (TargetPoint.X < SlashRange || TargetPoint.X > (1 - SlashRange))
		&& (TargetPoint.Y < SlashRange || TargetPoint.Y > (1 - SlashRange));
}

void AEnemy::MoveTowardsOpponent()
{
	int32 id = FMath::RandRange(INT_MIN, INT_MAX);
	//these multiples are all a bit screwey because of the scale difference between screen space and world space and time delta
	FVector2D EndPoint = OtherSword->GetSwordPositionNormalizedScreenSpace();
	//The velocity anticipation here is acting up a bit... not entirely sure why yet
	//EndPoint += (OtherSword->GetSwordScreenSpaceVelocity() * 0.00008f);

	//This uses world space not screen space, so it will be consistent on different sized monitors
	//although that might be an issue with the rest of this AI as well... that's alright
	float DistanceToOpponent = (OtherSword->GetSwordPosition() - Sword->GetSwordPosition()).Length();
	float Duration = DistanceToOpponent * 0.001f * DefensiveTrackingSpeed;
	GetWorld()->GetLatentActionManager().AddNewAction(this, id, new FSwordManeuverLatentAction(this, Duration, TargetPoint, EndPoint, false, FString(TEXT("Defending to point"))));
}

void AEnemy::SlashAcrossOpponent()
{
	int32 id = FMath::RandRange(INT_MIN, INT_MAX);


	FVector2D OtherSwordVelocity = OtherSword->GetSwordScreenSpaceVelocity();
	FVector SlashDirection = FVector::CrossProduct(FVector(0, 0, 1), FVector(OtherSwordVelocity.X, OtherSwordVelocity.Y, 0));
	SlashDirection.Normalize();

	UE_LOG(LogTemp, Log, TEXT("slashdirection %s"), *SlashDirection.ToString());

	//offset in front of the opponent slightly
	// TODO: might be a good thing to look deeper into how C++ is handling these constructors and copying and whatnot
	FVector2D Offset = FVector2D(OtherSwordVelocity.X, OtherSwordVelocity.Y);
	Offset.Normalize();	
	Offset *= DefensiveSlashOffset;

	// Start point is along a longer slash to account for the time it takes to arrive
	FVector2D StartPoint = OtherSword->GetSwordPositionNormalizedScreenSpace() + (FVector2D(SlashDirection.X, SlashDirection.Y) * DefensiveSlashOffset  * 2.0f) + Offset;
	FVector2D EndPoint = OtherSword->GetSwordPositionNormalizedScreenSpace() - (FVector2D(SlashDirection.X, SlashDirection.Y) * DefensiveSlashOffset) + Offset;

	float Duration = FMath::RandRange(DefensiveSlashDurationRange.X, DefensiveSlashDurationRange.Y);
	GetWorld()->GetLatentActionManager().AddNewAction(this, id, new FSwordManeuverLatentAction(this, Duration, StartPoint, EndPoint, false, FString(TEXT("Defending slash"))));

}

void AEnemy::ChangeTarget()
{
	TargetIndex++;
	TargetIndex %= TargetPoints.Num();
	TargetPoint = TargetPoints[TargetIndex];
}

void AEnemy::Tick(float DeltaTime) 
{
	Super::Tick(DeltaTime);

	// terrible, terrible code :(
	if (!OtherSword)
	{
		TArray<AActor*> AllSwords;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASwordMover::StaticClass(), AllSwords);
		for (AActor* sword : AllSwords)
		{
			if (sword != Sword)
			{
				OtherSword = Cast<ASwordMover>(sword);
			}
		}
		if (!OtherSword)
		{
			//we're in some weird initializing state, and should probably just chill a bit and wait for things to sort themselves out next frame
			return;
		}
	}

	if(!bIsBusy)
	{
		if (FightingStance == EFightingStance::Aggressive)
		{
			// TODO: add some sense of paying attention to where the player sword is, and how open they are to defending themselves.
			// This lets the player have some more influence and introduces the idea of feinting
			if (IsThreateningPlayer())
			{
				//all of these things can get dials and probabilities to adjust AI tendencies.
				Stab();
			}
			else if (IsSetupForSlash())
			{
				StartDiagonalSlash();
			}
			else
			{
				StartRandomMove(false);
			}
		}
		else if (FightingStance == EFightingStance::Defensive)
		{
			//fun question: why is this number so big??
			UE_LOG(LogTemp, Log, TEXT("OTher sword v %f"), OtherSword->GetSwordScreenSpaceVelocity().SquaredLength());
			if (OtherSword->GetSwordScreenSpaceVelocity().Length() > DefensiveSlashingSpeedThreshold && FMath::RandRange(0.0f, 1.0f) < DefensiveSlashChance)
			{
				SlashAcrossOpponent();
			}
			else if (FMath::RandRange(0.0f, 1.0f) < DefendAgainstStabChance)
			{
				MoveTowardsOpponent();
			}
			else
			{
				StartRandomMove(false);
			}
			
		}

	}

	//go there
	FIntPoint ScreenDimensions = GEngine->GameViewport->Viewport->GetSizeXY();
	Sword->MoveSwordTowardsTargetPosition(TargetPoint.X * ScreenDimensions.X, TargetPoint.Y * ScreenDimensions.Y, bIsStabbing, DeltaTime, true);

	//tell the physical manifestation of the thing to go there as well
	SwordPointVisual->SetActorLocation(Sword->GetSwordPosition());
}