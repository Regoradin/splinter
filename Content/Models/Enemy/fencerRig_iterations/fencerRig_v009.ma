//Maya ASCII 2023 scene
//Name: fencerRig.ma
//Last modified: Mon, May 20, 2024 07:59:45 PM
//Codeset: 1252
requires maya "2023";
requires "stereoCamera" "10.0";
requires -nodeType "aiOptions" -nodeType "aiAOVDriver" -nodeType "aiAOVFilter" "mtoa" "5.2.1.1";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t film;
fileInfo "application" "maya";
fileInfo "product" "Maya 2023";
fileInfo "version" "2023";
fileInfo "cutIdentifier" "202211021031-847a9f9623";
fileInfo "osv" "Windows 11 Home v2009 (Build: 22631)";
fileInfo "UUID" "7ABDE666-4C5D-E287-22B7-7DAC860C46D0";
createNode transform -s -n "persp";
	rename -uid "D5538756-4EF4-EF8F-450F-63950483AAA7";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1243.6633503416788 813.42193015254384 -383.76025500074184 ;
	setAttr ".r" -type "double3" -3.3383527205039201 -2988.5999999974847 0 ;
createNode camera -s -n "perspShape" -p "persp";
	rename -uid "0E3E30C4-49EE-B4CE-08CA-F58BFB667F10";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 1256.0468150163895;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 0 678.10797221849964 391.23579695438036 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	rename -uid "220F58EB-4C6A-5B1E-A17E-1DA13C06CC07";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 1000.1 0 ;
	setAttr ".r" -type "double3" -90 0 0 ;
createNode camera -s -n "topShape" -p "top";
	rename -uid "506D153B-4C5F-68C4-A4AB-B1928F1D0FEA";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "front";
	rename -uid "F3EFAA51-456C-9F35-AE71-70A9B8E4002C";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 32.635239807519611 234.540399156869 1000.1 ;
createNode camera -s -n "frontShape" -p "front";
	rename -uid "6E4D707F-44C2-1451-AF5F-97B647CBEDFF";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 933.81570788556053;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -s -n "side";
	rename -uid "2424ABAC-4EF9-CB24-93D9-D1B6CB1A1BFF";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 1235.0572342665455 422.03076594036827 -21.636492595205041 ;
	setAttr ".r" -type "double3" 0 90 0 ;
createNode camera -s -n "sideShape" -p "side";
	rename -uid "E33614C5-4F95-8A4C-7681-09A9E66763CA";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 1243.3900610612386;
	setAttr ".ow" 642.23208870854398;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -8.3328267946930694 695.09954741472347 347.80642721322153 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode transform -n "left";
	rename -uid "D92FDBDE-4D05-F618-1628-3C81D3CEE085";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -1000.1 -15.878127420805242 10.146771957347106 ;
	setAttr ".r" -type "double3" 0 -90 0 ;
createNode camera -n "leftShape" -p "left";
	rename -uid "645D2FB0-42E8-1F8D-2614-9B846175983C";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".coi" 1000.1;
	setAttr ".ow" 411.33120601411935;
	setAttr ".imn" -type "string" "left1";
	setAttr ".den" -type "string" "left1_depth";
	setAttr ".man" -type "string" "left1_mask";
	setAttr ".hc" -type "string" "viewSet -ls %camera";
	setAttr ".o" yes;
	setAttr ".ai_translator" -type "string" "orthographic";
createNode joint -n "root";
	rename -uid "6E6DCFC5-41AE-7945-01AF-C9ADF6B96453";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".v" no;
	setAttr ".uoc" 1;
	setAttr ".ove" yes;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.0900958487397349e-16 -4.1498425465336221e-15 -11.257815202538596 ;
	setAttr ".bps" -type "matrix" -0.15680631874941819 0.98762937299386555 -1.224646799147353e-16 0
		 0.98762937299386555 0.15680631874941808 1.2325951644078309e-32 0 1.9203235634255452e-17 -1.2094971503808445e-16 -1 0
		 0 563.51405376879643 0 1;
	setAttr ".radi" 2;
createNode joint -n "hips" -p "root";
	rename -uid "73A1489A-48CF-5846-1457-5D990FB62B53";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -2.8869197489127578 7.8129954425729533 -2.4973214890125e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 5.4623050481274369 ;
	setAttr ".bps" -type "matrix" 0.062080978492094624 -0.99807111575752172 2.4437325261173946e-16 0
		 0.99807111575752161 0.062080978492094485 1.1657518157731658e-17 0 -2.6805962794083387e-17 2.431781747614721e-16 1 0
		 8.1690310485042588 561.88797408104995 6.0327785189137903e-16 1;
	setAttr ".radi" 2;
createNode joint -n "hips_end" -p "hips";
	rename -uid "F6D8187C-49D5-FBF6-99FF-F3AFDBC1AC6B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 59.47718877123598 1.865174681370263e-14 -6.590507595793226e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 180 5.4623050481274369 ;
	setAttr ".bps" -type "matrix" -0.15680631874941819 0.98762937299386566 -3.668379325264748e-16 0
		 0.98762937299386544 0.15680631874941808 -1.1657518157731643e-17 0 4.6009198428338845e-17 -3.641278897995566e-16 -1 0
		 11.86143312538163 502.52550992202168 8.547404332327516e-15 1;
	setAttr ".radi" 2;
createNode joint -n "bottom_torso" -p "root";
	rename -uid "E1885AAB-4CBE-3096-12CE-768561A431D7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 180 0 -1.5881061613835965 ;
	setAttr ".bps" -type "matrix" -0.18411737904022712 0.98290426326034297 -1.2241763996512782e-16 0
		 -0.98290426326034319 -0.18411737904022704 -1.1907067755956352e-16 0 -1.3957429162126213e-16 9.8401839147183055e-17 1 0
		 1.6893831428279855 563.18231012059277 1.2539203008425267e-16 1;
	setAttr ".radi" 2;
createNode joint -n "mid_torso" -p "|root|bottom_torso";
	rename -uid "117C5494-4438-229A-FE25-3993B3112D7D";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -18.870565475228737 ;
	setAttr ".bps" -type "matrix" 0.14368067469031004 0.98962410223303354 -7.7326703334545362e-17 0
		 -0.98962410223303376 0.14368067469031001 -1.5226452567993771e-16 0 -1.3957429162126213e-16 9.8401839147183055e-17 1 0
		 -9.0495163762149939 620.51155397043976 -6.8168235306516791e-15 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "chest" -p "|root|bottom_torso|mid_torso";
	rename -uid "57BE07C1-45A7-8A4B-EF2D-FDA731A81665";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 6.0151448785396022 ;
	setAttr ".bps" -type "matrix" 0.039185566695351517 0.99923195073154292 -9.28569639733298e-17 0
		 -0.99923195073154314 0.039185566695351517 -1.4332302351953412e-16 0 -1.3957429162126213e-16 9.8401839147183055e-17 1 0
		 4.9103169027355911 716.66218456694264 -1.7864623661089073e-14 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "neck" -p "chest";
	rename -uid "6B75A66F-46E0-58D8-0CB5-37A391438366";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 56.867538368461034 9.6001549776665056e-15 -6.3093790175078314e-16 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -8.2201029382776772e-17 -1.2236953947524957e-15 7.6860745969004167 ;
	setAttr ".bps" -type "matrix" -0.094809092627995806 0.99549547259395232 -1.1119149730874619e-16 0
		 -0.99549547259395255 -0.094809092627995778 -1.2961618631700399e-16 0 -1.3957429162126213e-16 9.8401839147183055e-17 1 0
		 7.1387036202733727 773.48604586416081 -2.3776108524371994e-14 1;
	setAttr ".radi" 2;
createNode joint -n "head" -p "neck";
	rename -uid "D118AC6A-420A-F508-7F12-B0B5F3386AAE";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 58.759836629845822 7.1801865948466039e-14 1.2549504621248149e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -6.5012437212697369 ;
	setAttr ".bps" -type "matrix" 0.018515344006020687 0.99982857632513134 -9.580070471853827e-17 0
		 -0.99982857632513156 0.018515344006020687 -1.4137230918591038e-16 0 -1.3957429162126213e-16 9.8401839147183055e-17 1 0
		 1.567736826428348 831.98119719953263 -2.9054752278737055e-14 1;
	setAttr ".radi" 2;
createNode joint -n "head_end" -p "|root|bottom_torso|mid_torso|chest|neck|head";
	rename -uid "91D48523-4A9C-2179-D6F0-1993977FF518";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 60.176757094367673 8.2198113017797041e-14 -1.2009154515051715e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 -88.939088309735723 ;
	setAttr ".bps" -type "matrix" 1.0000000000000004 1.1553258350005535e-15 1.3957429162126199e-16 0
		 -1.1587952819525071e-15 1.0000000000000002 -9.8401839147183203e-17 0 -1.3957429162126213e-16 9.8401839147183055e-17 1 0
		 2.6819301851972295 892.14763857305752 -3.6020643467558962e-14 1;
	setAttr ".radi" 2;
createNode orientConstraint -n "head_orientConstraint1" -p "|root|bottom_torso|mid_torso|chest|neck|head";
	rename -uid "5FFE8D4D-4369-54C0-CB8C-30913AAECC91";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "headW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 -88.93908830973578 ;
	setAttr ".o" -type "double3" 0 0 88.939088309735766 ;
	setAttr ".rsrr" -type "double3" 0 0 -1.2722218725854067e-14 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "neck_orientConstraint1" -p "neck";
	rename -uid "F2656BE2-4AFE-732D-07BC-E1A576478274";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "head_1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0 0 -95.440332031005511 ;
	setAttr ".o" -type "double3" 0 0 95.440332031005511 ;
	setAttr -k on ".w0";
createNode joint -n "right_clavicle" -p "chest";
	rename -uid "F34F747A-4348-D6CA-7245-B289F5C073A7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 35.0198978734702 1.3733262287262811 -45.371628126067975 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000057 -42.61405596961125 2.2457425658950125 ;
	setAttr ".bps" -type "matrix" -1.6453424732001397e-15 0.73593101176182907 0.67705653082088402 0
		 2.4230403313734553e-15 -0.67705653082088424 0.73593101176182896 0 1.0000000000000004 2.8310687127941492e-15 -7.4860412807886339e-16 0
		 4.9103199999999072 751.70899999999995 -45.371628126067996 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "right_shoulder" -p "right_clavicle";
	rename -uid "2DEF99C9-47E8-F264-BA64-9795D0835361";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" -50.142253252473779 -0.00035324688565196993 -7.2830630415410269e-14 ;
	setAttr ".r" -type "double3" 2.4367132033846111 0.15720632705364576 0.12347978193571052 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -4.1423779810953117e-15 -1.1558034113920338e-14 39.435170603995743 ;
	setAttr ".bps" -type "matrix" 2.6835661757730551e-16 0.13832224525180559 0.99038727600292775 0
		 2.9165495071027353e-15 -0.99038727600292797 0.13832224525180575 0 1.0000000000000004 2.8310687127941492e-15 -7.4860412807886339e-16 0
		 4.910319999999917 714.80799999999999 -79.321028126068043 1;
	setAttr ".radi" 2;
createNode joint -n "right_elbow" -p "right_shoulder";
	rename -uid "D474FB6F-4181-EE5E-8D5C-31B0575151C2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" -133.39016696660818 0.00017428602893687639 -6.775850000000025 ;
	setAttr ".r" -type "double3" 0 0 7.6639684517140893e-14 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -7.4007308328849877e-16 -1.1409437543609797e-14 7.4225822066050036 ;
	setAttr ".bps" -type "matrix" 6.4288651703680495e-16 0.0092185452926789313 0.99995750830857155 0
		 2.8574417278032155e-15 -0.99995750830857177 0.0092185452926791256 0 1.0000000000000004 2.8310687127941492e-15 -7.4860412807886339e-16 0
		 -1.8655300000001462 696.35700000000008 -211.428928126068 1;
	setAttr ".radi" 2;
createNode joint -n "right_wrist" -p "right_elbow";
	rename -uid "8ABACB0E-4C98-CCC5-AD01-33A7E2564E16";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" -136.38379277953928 -0.00026018198104793555 -6.4672999999999936 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 5.4949873323284419e-17 -1.1427089597785349e-14 -0.55103647027694602 ;
	setAttr ".bps" -type "matrix" 6.1537604711892819e-16 0.018834962757058316 0.99982260635471742 0
		 2.8634923820766525e-15 -0.99982260635471765 0.018834962757058503 0 1.0000000000000004 2.8310687127941492e-15 -7.4860412807886339e-16 0
		 -8.3328300000002304 695.10000000000002 -347.80692812606804 1;
	setAttr ".radi" 2;
createNode joint -n "right_hand_1" -p "right_wrist";
	rename -uid "CF4D47DD-4B03-7D0B-FF15-A7B447BB81B5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" -24.16596491874208 0.58893942487863182 -1.4185358912410087e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.9261103069464355e-12 180.00000000000003 1.1427089250514295 ;
	setAttr ".bps" -type "matrix" -3.5073493487579265e-16 0.0011079563370644668 -0.99999938621618933 0
		 2.8506506365490745e-15 -0.99999938621618945 -0.001107956337064276 0 -1.0000000000000004 -2.830712366858052e-15 4.2697979555146567e-16 0
		 -8.3328300000002571 694.05599999999993 -371.95751350406766 1;
	setAttr ".radi" 1.5244715959100552;
createNode joint -n "right_hand_2" -p "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1";
	rename -uid "94B5BEFF-423E-18B3-520A-91BD98C36E1E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" 20.805296744352869 -1.2684298056342413e-13 -5.0059444244360736e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -3.4766957703501912e-16 3.9941624504594552e-14 -0.99743045527799312 ;
	setAttr ".bps" -type "matrix" -4.0030468017699624e-16 0.01851534400602535 -0.99982857632513122 0
		 2.8441132553448645e-15 -0.99982857632513134 -0.018515344006025159 0 -1.0000000000000004 -2.830712366858052e-15 4.2697979555146567e-16 0
		 -8.3328300000002589 694.07905136037243 -392.76279747846621 1;
	setAttr ".radi" 1.0002123964792611;
createNode joint -n "right_hand_end" -p "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2";
	rename -uid "C06808CA-4256-61E6-A10A-828163A2BD22";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 10.656170497062567 -1.2129186544029835e-13 -6.5800582896441245e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -90.000000000000028 -2.2457425658950108 91.060911690264447 ;
	setAttr ".bps" -type "matrix" -0.03918556669535072 -0.99923195073154292 -3.3715214384311825e-16 0
		 0.99923195073154314 -0.03918556669535074 -8.9589455428357252e-16 0 7.9166553301291077e-16 -5.6898930012039145e-16 1.0000000000000002 0
		 -8.3328300000002571 694.27635402291253 -403.41714125562214 1;
	setAttr ".radi" 1.0002123964792611;
createNode orientConstraint -n "right_hand_2_orientConstraint1" -p "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2";
	rename -uid "06123D0D-4B76-4644-E5F0-AF976990E65B";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "right_hand_2W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 178.93908830973567 89.999999999995282 0 ;
	setAttr ".o" -type "double3" -90.000000000254929 88.939088309735666 89.999999999745086 ;
	setAttr ".rsrr" -type "double3" 179.99999999999994 180 180 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "right_hand_1_orientConstraint1" -p "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1";
	rename -uid "BD29D219-4E7E-151A-C99F-1BB25495BA18";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "right_hand_1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 179.93651876501369 89.999999999998977 0 ;
	setAttr ".o" -type "double3" -90.0000000009301 89.936518765013673 89.999999999064173 ;
	setAttr ".rsrr" -type "double3" 180.00000000000574 179.99999999999997 180 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "right_wrist_orientConstraint1" -p "right_wrist";
	rename -uid "D16F22D5-4C9C-80A4-0903-6CB9CC0E99CE";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "right_hand_ikW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 94.138154764815653 -87.558223973591097 86.820936537225464 ;
	setAttr ".o" -type "double3" 90.000000000010132 -88.920772309935089 89.999999999990891 ;
	setAttr ".rsrr" -type "double3" -179.99999999999898 -180 180.00000000000003 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector5" -p "right_elbow";
	rename -uid "CD496725-4560-2AAA-FA5B-11A95A3C1569";
	setAttr ".v" no;
	setAttr ".dla" yes;
	setAttr ".hd" yes;
createNode joint -n "left_clavicle" -p "chest";
	rename -uid "4FCAE875-4D17-5BDC-9119-68985A03B974";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 35.019897873470313 1.3733262287261851 45.371599999999994 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 90.000000000000057 -42.614055969611265 -177.75425743410494 ;
	setAttr ".bps" -type "matrix" 2.0114546141589697e-15 -0.73593101176182874 0.67705653082088435 0
		 -2.6250049837190533e-15 0.67705653082088457 0.73593101176182851 0 -1.0000000000000004 -3.219646771412954e-15 -3.6161889654629305e-16 0
		 4.9103199999999951 751.70900000000006 45.371599999999972 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "left_shoulder" -p "left_clavicle";
	rename -uid "E7858BA6-4401-7F79-DAC6-8D84F0557EB2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 5;
	setAttr ".t" -type "double3" 50.142253252473836 0.00035324688565196993 9.8587804586713901e-14 ;
	setAttr ".r" -type "double3" 2.4366905046826934 0.15720495487784508 0.12347855794317918 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2074182606101265e-06 -2.5434303296458131e-14 39.435170603995758 ;
	setAttr ".bps" -type "matrix" -1.1388107682290096e-16 -0.13832224525180481 0.99038727600292775 0
		 -3.3050918186508726e-15 0.99038727600292797 0.13832224525180481 0 -1.0000000000000004 -3.219646771412954e-15 -3.6161889654629305e-16 0
		 4.9103199999999978 714.80800000000011 79.321000000000083 1;
	setAttr ".radi" 2;
createNode joint -n "left_elbow" -p "left_shoulder";
	rename -uid "E7BCCA5B-4539-942D-A50B-BDBDEFC4108F";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 6;
	setAttr ".t" -type "double3" 133.39026600533569 -0.00016045380448304059 6.7758500000000108 ;
	setAttr ".r" -type "double3" 0 0 -2.4772171149104776e-12 ;
	setAttr -l on -k off ".rx";
	setAttr -l on -k off ".ry";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.9090959086912713e-06 -2.6596031056514992e-14 7.4225822066050018 ;
	setAttr ".bps" -type "matrix" -5.3989982819991318e-16 -0.0092185452926782097 0.99995750830857144 0
		 -3.3320012605700046e-08 0.99995750830857111 0.0092185452926782374 0 -0.99999999999999989 -3.3318596739107079e-08 -3.0716237689994932e-10 0
		 -1.8655300000000308 696.3570000000002 211.42899999999995 1;
	setAttr ".radi" 2;
createNode joint -n "left_wrist" -p "left_elbow";
	rename -uid "D548A4D2-4572-77B6-DD2A-CB9DE3947456";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 7;
	setAttr ".t" -type "double3" 136.38379277953931 0.00026039747149297909 6.467299999991293 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 2.6999576181657328e-06 -1.8360283823004784e-08 -0.55103647027694547 ;
	setAttr ".bps" -type "matrix" 3.2044643286830594e-10 -0.018834962757057573 0.99982260635471742 0
		 -8.0441621766840052e-08 0.99982260635471443 0.018834962757057573 0 -0.99999999999999722 -8.0433387493029242e-08 -1.1947251540530923e-09 0
		 -8.3328300000000723 695.10000000000014 347.80700000000002 1;
	setAttr ".radi" 2;
createNode joint -n "left_hand_1" -p "left_wrist";
	rename -uid "1F0255B9-4D45-7649-C2BF-8EA4648D10DA";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".t" -type "double3" 24.166379467191121 -0.58893161548871831 5.5118649200380787e-08 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 4.1817337552045947e-06 180.00000007355865 1.1427089250514295 ;
	setAttr ".bps" -type "matrix" 8.4358196049207707e-16 -0.0011079563370652501 -0.99999938621618933 0
		 -7.4469919885969757e-09 0.99999938621618945 -0.0011079563370652123 0 1.0000000000000007 7.4469873807666873e-09 -8.2503067195900634e-12 0
		 -8.3328300000000777 694.05600000000004 371.95799999999997 1;
	setAttr ".radi" 1.5244715959100552;
createNode joint -n "left_hand_2" -p "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1";
	rename -uid "C644DD38-45BA-4104-8652-A59D205B8A6B";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -20.805012713223505 -5.1045709597019595e-05 -3.694822225952521e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.7074826403838875e-06 7.4274852995406584e-09 -0.9974304552779929 ;
	setAttr ".bps" -type "matrix" 1.2963484974335183e-10 -0.018515344006026131 -0.99982857632513122 0
		 2.2355330413196988e-08 0.99982857632513111 -0.018515344006026085 0 1.0000000000000004 -2.2349097984388288e-08 5.4352905189454407e-10 0
		 -8.3328300000000848 694.07900000000006 392.76299999999992 1;
	setAttr ".radi" 1.0002123964792611;
createNode joint -n "left_hand_end" -p "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2";
	rename -uid "7ABD371D-49AE-475E-8457-C7A0AE5A71ED";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -10.655821174937159 -0.00029624550427342911 1.3879812854611373e-09 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" -89.999999968834146 -2.2457412853858303 91.060911689043209 ;
	setAttr ".bps" -type "matrix" 0.039185566695350095 0.99923195073154325 -9.769487131720425e-16 0
		 -0.99923195073154358 0.03918556669535006 -2.4403176819322357e-16 0 -4.8268844397418366e-16 1.11542288147832e-15 1.0000000000000004 0
		 -8.3328300000000919 694.27599999999995 403.41699999999997 1;
	setAttr ".radi" 1.0002123964792611;
createNode orientConstraint -n "left_hand_2_orientConstraint1" -p "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2";
	rename -uid "751DAB67-45D6-0FEE-4803-FE910DDC9B69";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "hand_2W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 1.0609116902646862 -89.999994388025598 0 ;
	setAttr ".o" -type "double3" -89.999696953382198 88.939088309720489 -89.999696901424088 ;
	setAttr ".rsrr" -type "double3" 1.6051861908011185e-13 7.848111856824136e-15 1.8089404750823763e-14 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "left_hand_1_orientConstraint1" -p "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1";
	rename -uid "909ADF25-485E-8635-298C-BF8EB1D17146";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "hand_1W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0.063481234986856191 -89.999994683729724 0 ;
	setAttr ".o" -type "double3" -89.99520173600061 89.936518764790549 -89.995201733055325 ;
	setAttr ".rsrr" -type "double3" -1.905164678238371e-13 6.2555717389619179e-15 -1.9629985924657745e-15 ;
	setAttr -k on ".w0";
createNode orientConstraint -n "left_wrist_orientConstraint1" -p "left_wrist";
	rename -uid "CA025E38-46CC-662C-1261-B68D394721F0";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "left_hand_ikW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 85.861852641129033 92.441757887391958 86.820945100988425 ;
	setAttr ".o" -type "double3" 89.999825265702086 -88.920772309929987 -89.9998252346996 ;
	setAttr ".rsrr" -type "double3" 2.1796738801404664e-13 2.0530728949681883e-15 -2.0574213095717122e-14 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector4" -p "left_elbow";
	rename -uid "7113503F-40F0-2916-14A7-97B31E70DABE";
	setAttr ".v" no;
	setAttr ".dla" yes;
	setAttr ".hd" yes;
createNode parentConstraint -n "chest_parentConstraint1" -p "chest";
	rename -uid "9237975B-4381-AB6C-DE81-5F9A14D10067";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "upper_torsoW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -8.8817841970012523e-16 0 -3.1554436208840472e-30 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 87.754257434105099 ;
	setAttr ".lr" -type "double3" 0 0 -3.1805546814635168e-15 ;
	setAttr ".rst" -type "double3" 97.158739747287996 1.4210854715202004e-14 -3.5348350856405438e-15 ;
	setAttr ".rsrr" -type "double3" 0 0 -3.1805546814635168e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "mid_torso_parentConstraint1" -p "|root|bottom_torso|mid_torso";
	rename -uid "B37FBF4D-4888-B6A8-145C-B19295C31E14";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "mid_torsoW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -3.5527136788005009e-15 1.1368683772161603e-13 
		-7.0997481469891062e-30 ;
	setAttr ".tg[0].tor" -type "double3" 0 0 81.739112555565512 ;
	setAttr ".lr" -type "double3" 0 0 3.180554681463516e-15 ;
	setAttr ".rst" -type "double3" 58.326376222729095 -1.4210854715202004e-13 1.9796176416869704e-16 ;
	setAttr ".rsrr" -type "double3" 0 0 3.180554681463516e-15 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "bottom_torso_parentConstraint1" -p "|root|bottom_torso";
	rename -uid "200D7145-41A1-15B0-DABE-BE990E117787";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "bottom_torsoW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".tg[0].tot" -type "double3" -1.5765166949677223e-14 0 0 ;
	setAttr ".tg[0].tor" -type "double3" -5.7248111730014452e-15 6.8967534835884195e-15 
		100.60967803079424 ;
	setAttr ".lr" -type "double3" 5.7248111730014444e-15 -6.8967534835884195e-15 1.987846675914694e-16 ;
	setAttr ".rst" -type "double3" -0.59254572285431095 1.6164649138542728 -5.2826107800044846e-17 ;
	setAttr ".rsrr" -type "double3" 5.7248111730014444e-15 -6.8967534835884195e-15 1.987846675914694e-16 ;
	setAttr -k on ".w0";
createNode parentConstraint -n "root_parentConstraint1" -p "root";
	rename -uid "2660E788-4797-7AF8-C90F-13AC119B5F36";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "center_cogW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".tg[0].tor" -type "double3" 180 0 99.02157186941065 ;
	setAttr ".lr" -type "double3" 0 180 -69.720612928050755 ;
	setAttr ".rst" -type "double3" 0 563.51405376879643 0 ;
	setAttr ".rsrr" -type "double3" -1.5681345282026479e-46 1.4124500153760508e-30 -1.2722218725854067e-14 ;
	setAttr -k on ".w0";
createNode joint -n "right_leg_1" -p "root";
	rename -uid "A327D1D8-4F7C-EBF0-4437-FABCD1847B3A";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -46.894398495393652 13.528202567486003 35.327424579458587 ;
	setAttr ".r" -type "double3" -0.62505380863406368 -0.10471449528400285 0.00057118396860783387 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" -0.156796211160799 0.98762928672710637 -0.0018276116998478895 0
		 0.98756904080779073 0.15680683367561501 0.010909012357140285 0 0.011060642097021307 -9.4400928281943572e-05 -0.99993882477122886 0
		 20.714188217491138 519.32107602977021 -35.32742457945858 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "right_leg_2" -p "right_leg_1";
	rename -uid "4EC28D78-434B-ABF1-CF96-BC898BB9EDFC";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" -246.84990221864285 -58.639166378603129 3.0230394262189805e-14 ;
	setAttr ".r" -type "double3" 2.328284765147316e-29 1.0009318505715131e-30 9.5235123399820285e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.526666247102488e-13 180 4.9232706133283974 ;
	setAttr ".bps" -type "matrix" 0.071462979010819772 -0.9974428605403024 0.00088464008402846638 0
		 0.99738192525596781 0.071468391934084108 0.011025612321109005 0 -0.011060642097021315 9.4400928282065721e-05 0.99993882477122886 0
		 1.5090923165025067 266.32986116370967 -35.515974200694096 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "right_foot_1" -p "right_leg_2";
	rename -uid "17A5BDB5-42F3-0D0B-51F6-73A162CAF480";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" 213.13709907530887 6.7501559897209518e-14 -5.2107231339574471e-14 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999914622634 2.4354162102360393e-15 -77.535097398448727 ;
	setAttr ".bps" -type "matrix" -0.95844736613851556 -0.28507335966558334 -0.010574779010435116 0
		 -0.28505492213345085 0.95850569674892316 -0.0032435578234796913 0 0.011060637849371939 -9.4386645434055879e-05 -0.99993882481956187 0
		 16.740504354148385 53.737783374771737 -35.32742457945858 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode joint -n "right_foot_2" -p "right_foot_1";
	rename -uid "A0C15FD1-4440-6497-EFB1-A1AFD01C3D23";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" 160.30064856315661 -4.6185277964332406e-14 6.8137435467150344e-15 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 0 97.541631988222889 ;
	setAttr ".bps" -type "matrix" -0.15679621132418961 0.98762928672850114 -0.0018275969284890518 0
		 0.98756904082942254 0.15680683367543027 0.010909010401533808 0 0.011060637849371939 -9.4386645434055879e-05 -0.99993882481956187 0
		 -136.8992300515049 8.0403389323006778 -37.022568513243392 1;
	setAttr ".radi" 2;
	setAttr ".liw" yes;
createNode orientConstraint -n "right_foot_1_orientConstraint1" -p "right_foot_1";
	rename -uid "3885A7F6-46F0-DF74-3100-54BD6BFD3472";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "right_foot_ikW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".dla" yes;
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 0.0054082872626453095 -179.36625921032763 16.563173951679659 ;
	setAttr ".o" -type "double3" 0.18585289167267646 179.39409850047548 16.564186567112188 ;
	setAttr ".rsrr" -type "double3" 4.8702243559910117e-15 4.8717773612065684e-15 1.9083328088781104e-14 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector2" -p "right_leg_2";
	rename -uid "7D3E7CEE-46D3-7C67-ED50-268959A27E5E";
	setAttr ".v" no;
	setAttr ".dla" yes;
	setAttr ".hd" yes;
createNode joint -n "left_leg_1" -p "root";
	rename -uid "DAC139DB-496F-A8CE-232D-17BA030C1FC2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 1;
	setAttr ".t" -type "double3" -46.89447543219984 13.528202282289456 -35.32739999999999 ;
	setAttr ".r" -type "double3" -0.62497062349138077 -0.10470058332108698 0.00057103207597659781 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 7.0167092985348752e-15 -3.5083546492674375e-14 -180 ;
	setAttr ".bps" -type "matrix" 0.15679621384860601 -0.98762928674967942 -0.0018273688906948061 0
		 -0.98756905686476459 -0.156806833540764 0.010907560596157814 0 -0.011059170221222997 9.4388768125693251e-05 -0.99993884105218078 0
		 20.714200000000005 519.32099999999991 35.327399999999997 1;
	setAttr ".radi" 2;
createNode joint -n "left_leg_2" -p "left_leg_1";
	rename -uid "6592BABA-4D73-10FE-4E75-14B48A2D36D7";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 2;
	setAttr ".t" -type "double3" 246.84969702424172 58.639145268860133 2.1316282072803006e-14 ;
	setAttr ".r" -type "double3" 5.6530064379838956e-28 4.3317589113491273e-20 -2.936094021423665e-12 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999914622586 2.0998352443479342e-14 -175.07672938667159 ;
	setAttr ".bps" -type "matrix" -0.071462980310674745 0.99744286055121911 0.00088452276313637028 0
		 -0.99738194164916616 -0.071468391796386255 0.011024130177842987 0 0.011059155359065377 -9.4389833088331481e-05 0.99993884121645349 0
		 1.5090924945524833 266.32999110242679 35.51592457330743 1;
	setAttr ".radi" 2;
createNode joint -n "left_foot_1" -p "left_leg_2";
	rename -uid "500E4BF0-46DC-2D0F-DC76-B386013BB764";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 3;
	setAttr ".t" -type "double3" -213.13721175299813 1.2895289717818059e-05 1.4921397450962104e-13 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 179.99999933050577 8.3364931715476024e-07 -77.53509739844867 ;
	setAttr ".bps" -type "matrix" 0.9584473818647371 0.28507335953348828 -0.010573357126367139 0
		 0.28505492690540829 -0.95850569678899933 -0.0032431265760458185 0 -0.011059152028233186 9.4378633065703155e-05 -0.99993884125434918 0
		 16.740500000000008 53.737799999999794 35.32739999999999 1;
	setAttr ".radi" 2;
createNode joint -n "left_foot_2" -p "left_foot_1";
	rename -uid "056A81A2-4766-41B8-FCF8-0FBB9680BF1E";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".uoc" 1;
	setAttr ".oc" 4;
	setAttr ".t" -type "double3" -160.30102741125862 -9.6428994432073978e-05 -2.3323694335886103e-06 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.248532045669577e-07 1.0941310125479623e-07 97.541631988222875 ;
	setAttr ".bps" -type "matrix" 0.1567962139908565 -0.98762928675089356 -0.0018273560288380485 0
		 -0.98756905704591147 -0.15680683353921848 0.010907544217367296 0 -0.011059152028233186 9.4378633065703155e-05 -0.99993884125434918 0
		 -136.89962749431419 8.0403400067228787 37.022322655501021 1;
	setAttr ".radi" 2;
createNode orientConstraint -n "left_foot_1_orientConstraint1" -p "left_foot_1";
	rename -uid "4E18AB67-404D-F10B-7994-27B61C6FFB29";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "left_foot_ikW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".lr" -type "double3" 179.99459217192918 0.63365565318485551 16.563173956884658 ;
	setAttr ".o" -type "double3" -179.81417182129113 0.60582002701358262 16.56418630296443 ;
	setAttr ".rsrr" -type "double3" -7.5165452433024537e-15 -9.9392333795734924e-17 
		6.5195637458992852e-33 ;
	setAttr -k on ".w0";
createNode ikEffector -n "effector3" -p "left_leg_2";
	rename -uid "19387618-4B3C-AD4E-2FF9-1483A012231A";
	setAttr ".v" no;
	setAttr ".hd" yes;
createNode transform -n "mesh";
	rename -uid "E717A6B5-42E6-8028-9F50-9A8ABA812E5B";
	setAttr ".ove" yes;
createNode transform -n "garb" -p "mesh";
	rename -uid "643595C7-4DB9-FE89-48E3-E9B2257DF062";
	setAttr ".t" -type "double3" 3.6557316780090332 161.18052673339844 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" -90.00000933466734 0 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 100 100 100 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "garbShape" -p "garb";
	rename -uid "11B93652-41D4-664B-18FD-038CABEBE316";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.60425934195518494 0.15764250606298447 ;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 5 ".pt";
	setAttr ".pt[35]" -type "float3" -1.3411045e-07 3.7252878e-08 -8.1956387e-08 ;
	setAttr ".pt[36]" -type "float3" -1.3411045e-07 3.7252867e-08 -8.1956387e-08 ;
	setAttr ".pt[39]" -type "float3" -1.4901161e-08 -8.8817842e-16 0 ;
	setAttr ".pt[159]" -type "float3" -1.3411045e-07 3.7252878e-08 -8.1956387e-08 ;
	setAttr ".pt[161]" -type "float3" -1.4901161e-08 -8.8817842e-16 0 ;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "garbShapeOrig" -p "garb";
	rename -uid "092FE7AC-48EA-EE3C-F4E8-F5BBF0B81A34";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 270 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.375 0.12499999 0.39813858
		 0.125 0.42127717 0.125 0.42544973 0.125 0.45891702 0.125 0.49655691 0.125 0.5 0.125
		 0.55212969 0.125 0.60425937 0.125 0.625 0.125 0.37499997 0.14132123 0.37499997 0.14132123
		 0.60425931 0.14132123 0.60425931 0.14132123 0.62499994 0.14132123 0.62499994 0.14132123
		 0.39813858 0.14132124 0.39813858 0.14132124 0.42127717 0.14132124 0.42127717 0.14132124
		 0.42544973 0.14132124 0.42544973 0.14132124 0.45891705 0.14132124 0.45891705 0.14132124
		 0.49655694 0.14132124 0.49655694 0.14132124 0.5 0.14132124 0.5 0.14132124 0.55212963
		 0.14132124 0.55212963 0.14132124 0.375 0.19028498 0.375 0.19028498 0.39813858 0.190285
		 0.39813858 0.190285 0.42127717 0.190285 0.42127717 0.190285 0.42544979 0.190285 0.42544979
		 0.190285 0.45891705 0.190285 0.45891705 0.190285 0.49655694 0.190285 0.49655694 0.190285
		 0.5 0.190285 0.5 0.190285 0.55212969 0.190285 0.55212969 0.190285 0.625 0.190285
		 0.625 0.190285 0.60425937 0.19028501 0.60425937 0.19028501 0.49655691 0.24999999
		 0.49655691 0.24999999 0.375 0.25 0.375 0.25 0.39813858 0.25 0.39813858 0.25 0.42127717
		 0.25 0.42127717 0.25 0.42544979 0.25 0.42544979 0.25 0.45891702 0.25 0.45891702 0.25
		 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25
		 0.5 0.25 0.55212963 0.25 0.55212963 0.25 0.55212963 0.25 0.55212963 0.25 0.55212963
		 0.25 0.55212963 0.25 0.55212963 0.25 0.55212963 0.25 0.55212963 0.25 0.55212963 0.25
		 0.60425931 0.25 0.60425931 0.25 0.60425931 0.25 0.60425931 0.25 0.60425931 0.25 0.60425931
		 0.25 0.60425931 0.25 0.60425931 0.25 0.60425931 0.25 0.60425931 0.25 0.625 0.25 0.625
		 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.656389 0.35425931 0.656389 0.35425931
		 0.375 0.375 0.375 0.375 0.39813858 0.375 0.39813858 0.375 0.42127717 0.375 0.42127717
		 0.375 0.42544973 0.375 0.42544973 0.375 0.45891705 0.375 0.45891705 0.375 0.49655694
		 0.375 0.49655694 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375
		 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.60425931 0.42712966 0.60425931 0.42712966
		 0.81277794 0.45851862 0.81277794 0.45851862 0.81277794 0.45851862 0.81277794 0.45851862
		 0.81277794 0.45851862 0.81277794 0.45851862 0.70851862 0.47925931 0.70851862 0.47925931
		 0.70851862 0.47925931 0.70851862 0.47925931 0.70851862 0.47925931 0.70851862 0.47925931
		 0.42544973 0.49999997 0.42544973 0.49999997 0.125 0.5 0.125 0.5 0.25 0.5 0.25 0.5
		 0.375 0.5 0.375 0.5 0.39813858 0.5 0.39813858 0.5 0.4212772 0.5 0.4212772 0.5 0.45891708
		 0.5 0.45891708 0.5 0.49655694 0.5 0.49655694 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
		 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.55212963 0.5 0.55212963 0.5 0.55212963
		 0.5 0.55212963 0.5 0.55212963 0.5 0.55212963 0.5 0.55212963 0.5 0.55212963 0.5 0.55212963
		 0.5 0.55212963 0.5 0.60425931 0.5 0.60425931 0.5 0.60425931 0.5 0.60425931 0.5 0.60425931
		 0.5 0.60425931 0.5 0.60425931 0.5 0.60425931 0.5 0.60425931 0.5 0.60425931 0.5 0.625
		 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.75 0.5 0.75 0.5 0.75 0.5
		 0.75 0.5 0.75 0.5 0.75 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875
		 0.5 0.60425925 0.55971497 0.60425925 0.55971497 0.875 0.55971497 0.875 0.55971497
		 0.125 0.55971503 0.125 0.55971503 0.25 0.55971503 0.25 0.55971503 0.375 0.55971503
		 0.375 0.55971503 0.39813858 0.55971503 0.39813858 0.55971503 0.42127717 0.55971503
		 0.42127717 0.55971503 0.42544973 0.55971503 0.42544973 0.55971503 0.45891708 0.55971503
		 0.45891708 0.55971503 0.5 0.55971503 0.5 0.55971503 0.55212963 0.55971503 0.55212963
		 0.55971503 0.625 0.55971503 0.625 0.55971503 0.75 0.55971503 0.75 0.55971503 0.49655697
		 0.55971509 0.49655697 0.55971509 0.45891702 0.6086787 0.45891702 0.6086787 0.49655691
		 0.6086787 0.49655691 0.6086787 0.49999997 0.6086787 0.49999997 0.6086787 0.125 0.60867876
		 0.125 0.60867876 0.25 0.60867876 0.25 0.60867876 0.375 0.60867876 0.375 0.60867876
		 0.39813858 0.60867876 0.39813858 0.60867876 0.42127717 0.60867876 0.42127717 0.60867876
		 0.42544973 0.60867876 0.42544973 0.60867876 0.55212969 0.60867876 0.55212969 0.60867876
		 0.875 0.60867876 0.875 0.60867876;
	setAttr ".uvst[0].uvsp[250:269]" 0.60425937 0.60867882 0.60425937 0.60867882
		 0.625 0.60867882 0.625 0.60867882 0.75 0.60867882 0.75 0.60867882 0.125 0.625 0.25
		 0.625 0.375 0.625 0.39813858 0.625 0.42127717 0.625 0.42544973 0.625 0.45891702 0.625
		 0.49655688 0.625 0.5 0.625 0.55212969 0.625 0.60425937 0.625 0.625 0.625 0.75 0.625
		 0.875 0.625;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 242 ".vt";
	setAttr ".vt[0:165]"  0.47773921 0.78686446 5.90768147 0.31954458 0.6947329 3.71858239
		 -0.35297078 0.78686446 5.90768147 -0.35413569 0.64532727 3.68920255 0.26695189 0.60065156 4.96494055
		 -0.37925357 0.60065156 4.98973656 -0.40480101 0.4052 3.51049471 0.54101342 0.51814252 5.97072601
		 -0.52917826 0.51814264 5.97072601 0.61181897 0.4052 3.51049471 0.42498854 0.35315078 4.96494055
		 -0.66492474 0.35315081 5.032814026 0.2586602 0.48483604 4.04820919 -0.28472865 0.43543041 3.99016571
		 -0.47195917 0.26546094 3.87139821 0.48877436 0.26546094 3.95110965 -0.40480101 0 3.36078572
		 0.54101342 0 6.010128975 -0.64705634 0 6.010128975 0.42219698 0 3.36078572 0.51447451 0 5.17837238
		 -0.75358379 0 5.33625889 0.48877436 0 3.86164594 -0.45647526 0 3.69491386 -0.63383651 0.16395563 6.010128975
		 0.53197306 0.096076012 3.25393367 -0.75358379 0.15209839 5.23096466 -0.47195917 0.078186102 3.73903489
		 -0.3213689 0.096076012 3.25393367 0.54101342 0.1639556 6.010128975 0.51447451 0.15209839 5.11270094
		 0.48877436 0.078186102 3.87246966 0.5361284 0.78633857 5.75125694 -0.46952838 0.78633857 5.75125694
		 0.64916801 0.51379013 5.75125694 -0.69286644 0.53438449 5.75125694 -0.72866213 0 5.83396482
		 0.64916801 0 5.75125694 0.64916801 0.16457117 5.75125694 -0.7272625 0.16457118 5.815485
		 0.23336038 0.57041806 4.89952469 -0.37555361 0.57041794 4.89952469 -0.62964118 0.33545396 4.96049356
		 0.39490786 0.33545393 4.89262009 0.39490786 0 4.88572788 -0.66642594 0 5.13204098
		 0.39490786 0.14470391 4.88745117 -0.66642594 0.14470392 5.068729877 0.161654 0.47601533 4.59513664
		 -0.36185127 0.47601527 4.59513664 -0.55997062 0.26708776 4.50327873 0.29264271 0.26708773 4.50327873
		 0.29264271 0 4.41159058 -0.5675714 0 4.41159058 0.29264271 0.11144501 4.43451214
		 -0.5675714 0.11144501 4.43451214 -0.066017359 0.6947329 3.71858239 0.085669227 0.78686446 5.99881077
		 0.060411751 0.4052 3.51049471 0.062993437 0.51814258 6.11028481 -0.13003862 0.48483604 4.04820919
		 -0.09851262 0.6428327 4.9037509 -0.0067788064 0 6.14968777 0.060411751 0 3.36078572
		 0.060411751 0.059116866 3.25393367 0.010664314 0.16395561 6.14968777 -0.10437021 0.58836085 4.83833504
		 -0.14277881 0.50793892 4.59513664 0.16705096 0.41961262 4.13962746 -0.23702353 0.41961262 4.066036701
		 -0.40724742 0.25993744 3.94524193 0.40102679 0.25993744 4.090610504 0.40102679 0 4.014246941
		 -0.40724742 0 3.80301619 0.40102679 0.099075533 4.024601936 -0.40724742 0.10951187 3.8385725
		 -0.12194654 0.45128283 4.1014843 0.33130696 0.6185798 3.90468478 -0.3714416 0.56917417 3.85935235
		 -0.49811167 0.34235087 3.69047093 0.60415095 0.34235087 3.73483419 -0.47984633 0 3.50892949
		 0.60415095 0 3.60172343 0.60415095 0.04233294 3.54827929 -0.49811167 0.066927202 3.5371213
		 -0.10679153 0.6185798 3.90468478 0.10643041 0.99275565 6.15474749 0.50977492 0.99275565 6.0052433014
		 0.61738956 0.97460848 5.76900482 0.26262492 0.7052319 4.9786644 -0.48475349 0.97460848 5.78139877
		 -0.35384881 0.70511299 4.9786644 -0.09023644 0.71430713 4.89416695 -0.36718228 0.99275565 5.99590015
		 0.10836238 1.13710773 6.17237806 0.47246355 1.13710773 6.0046319962 0.5529809 1.12443328 5.78078747
		 0.24090858 0.76638007 5.035020351 -0.46640348 1.12443328 5.78078747 -0.29835939 0.76638007 5.03826046
		 -0.077715471 0.79088044 4.99767208 -0.34984592 1.13710773 6.0046319962 0.098011985 1.21741939 5.93832016
		 0.4364548 0.69349509 5.35809898 -0.45146757 0.69349509 5.35809898 0.64751768 0.44376761 5.35809898
		 -0.67889559 0.44376767 5.35809898 -0.7399025 0 5.57039642 0.64751768 0 5.46481466
		 0.64751768 0.15833478 5.43197918 -0.72203088 0.15833479 5.51774931 0.45443064 0.8525883 5.38003159
		 -0.44925702 0.8525883 5.38003159 0.4394643 1.0024130344 5.37942028 -0.41180474 1.0024130344 5.37942028
		 0.21497753 0.89082861 5.49288893 0.26222518 1.0013921261 5.46267271 0.20073532 0.89934427 5.27161217
		 -0.31909153 1.0013921261 5.46267271 -0.28325477 0.89934427 5.27161217 -0.03899115 0.88365203 5.15897989
		 0.10186358 1.16143441 5.82222986 -0.27596998 1.10860968 5.67084217 0.3084628 1.10860968 5.67084217
		 0.16654623 0.81748426 5.38847733 -0.24288532 0.89082861 5.49288893 -0.2146592 0.81748426 5.38847733
		 -0.02226969 0.79180694 5.31121969 0.088671789 1.071666718 5.76691818 -0.18855447 0.98542774 5.71045637
		 0.245184 0.98542774 5.71045637 0.033201054 0.89954984 5.52390766 0.47773921 -0.78686446 5.90768147
		 0.31954458 -0.6947329 3.71858239 -0.35297078 -0.78686446 5.90768147 -0.35413569 -0.64532727 3.68920255
		 0.26695189 -0.60065156 4.96494055 -0.37925357 -0.60065156 4.98973656 -0.40480101 -0.4052 3.51049471
		 0.54101342 -0.51814252 5.97072601 -0.52917826 -0.51814264 5.97072601 0.61181897 -0.4052 3.51049471
		 0.42498854 -0.35315078 4.96494055 -0.66492474 -0.35315081 5.032814026 0.2586602 -0.48483604 4.04820919
		 -0.28472865 -0.43543041 3.99016571 -0.47195917 -0.26546094 3.87139821 0.48877436 -0.26546094 3.95110965
		 -0.63383651 -0.16395563 6.010128975 0.53197306 -0.096076012 3.25393367 -0.75358379 -0.15209839 5.23096466
		 -0.47195917 -0.078186102 3.73903489 -0.3213689 -0.096076012 3.25393367 0.54101342 -0.1639556 6.010128975
		 0.51447451 -0.15209839 5.11270094 0.48877436 -0.078186102 3.87246966 0.5361284 -0.78633857 5.75125694
		 -0.46952838 -0.78633857 5.75125694 0.64916801 -0.51379013 5.75125694 -0.69286644 -0.53438449 5.75125694
		 0.64916801 -0.16457117 5.75125694 -0.7272625 -0.16457118 5.815485 0.23336038 -0.57041806 4.89952469
		 -0.37555361 -0.57041794 4.89952469 -0.62964118 -0.33545396 4.96049356 0.39490786 -0.33545393 4.89262009;
	setAttr ".vt[166:241]" 0.39490786 -0.14470391 4.88745117 -0.66642594 -0.14470392 5.068729877
		 0.161654 -0.47601533 4.59513664 -0.36185127 -0.47601527 4.59513664 -0.55997062 -0.26708776 4.50327873
		 0.29264271 -0.26708773 4.50327873 0.29264271 -0.11144501 4.43451214 -0.5675714 -0.11144501 4.43451214
		 -0.066017359 -0.6947329 3.71858239 0.085669227 -0.78686446 5.99881077 0.060411751 -0.4052 3.51049471
		 0.062993437 -0.51814258 6.11028481 -0.13003862 -0.48483604 4.04820919 -0.09851262 -0.6428327 4.9037509
		 0.060411751 -0.059116866 3.25393367 0.010664314 -0.16395561 6.14968777 -0.10437021 -0.58836085 4.83833504
		 -0.14277881 -0.50793892 4.59513664 0.16705096 -0.41961262 4.13962746 -0.23702353 -0.41961262 4.066036701
		 -0.40724742 -0.25993744 3.94524193 0.40102679 -0.25993744 4.090610504 0.40102679 -0.099075533 4.024601936
		 -0.40724742 -0.10951187 3.8385725 -0.12194654 -0.45128283 4.1014843 0.33130696 -0.6185798 3.90468478
		 -0.3714416 -0.56917417 3.85935235 -0.49811167 -0.34235087 3.69047093 0.60415095 -0.34235087 3.73483419
		 0.60415095 -0.04233294 3.54827929 -0.49811167 -0.066927202 3.5371213 -0.10679153 -0.6185798 3.90468478
		 0.10643041 -0.99275565 6.15474749 0.50977492 -0.99275565 6.0052433014 0.61738956 -0.97460848 5.76900482
		 0.26262492 -0.7052319 4.9786644 -0.48475349 -0.97460848 5.78139877 -0.35384881 -0.70511299 4.9786644
		 -0.09023644 -0.71430713 4.89416695 -0.36718228 -0.99275565 5.99590015 0.10836238 -1.13710773 6.17237806
		 0.47246355 -1.13710773 6.0046319962 0.5529809 -1.12443328 5.78078747 0.24090858 -0.76638007 5.035020351
		 -0.46640348 -1.12443328 5.78078747 -0.29835939 -0.76638007 5.03826046 -0.077715471 -0.79088044 4.99767208
		 -0.34984592 -1.13710773 6.0046319962 0.098011985 -1.21741939 5.93832016 0.4364548 -0.69349509 5.35809898
		 -0.45146757 -0.69349509 5.35809898 0.64751768 -0.44376761 5.35809898 -0.67889559 -0.44376767 5.35809898
		 0.64751768 -0.15833478 5.43197918 -0.72203088 -0.15833479 5.51774931 0.45443064 -0.8525883 5.38003159
		 -0.44925702 -0.8525883 5.38003159 0.4394643 -1.0024130344 5.37942028 -0.41180474 -1.0024130344 5.37942028
		 0.21497753 -0.89082861 5.49288893 0.26222518 -1.0013921261 5.46267271 0.20073532 -0.89934427 5.27161217
		 -0.31909153 -1.0013921261 5.46267271 -0.28325477 -0.89934427 5.27161217 -0.03899115 -0.88365203 5.15897989
		 0.10186358 -1.16143441 5.82222986 -0.27596998 -1.10860968 5.67084217 0.3084628 -1.10860968 5.67084217
		 0.16654623 -0.81748426 5.38847733 -0.24288532 -0.89082861 5.49288893 -0.2146592 -0.81748426 5.38847733
		 -0.02226969 -0.79180694 5.31121969 0.088671789 -1.071666718 5.76691818 -0.18855447 -0.98542774 5.71045637
		 0.245184 -0.98542774 5.71045637 0.033201054 -0.89954984 5.52390766;
	setAttr -s 480 ".ed";
	setAttr ".ed[0:165]"  35 8 0 8 2 0 2 33 0 33 35 0 2 57 0 57 86 0 86 93 0
		 93 2 0 66 61 0 61 4 0 4 40 0 40 66 0 42 11 0 11 5 0 5 41 0 41 42 0 47 26 0 26 11 0
		 42 47 0 4 10 0 10 43 0 43 40 0 32 0 0 0 7 0 7 34 0 34 32 0 56 1 0 1 9 0 9 58 0 58 56 0
		 39 24 0 24 8 0 35 39 0 8 59 0 59 57 0 77 12 0 12 15 0 15 80 0 80 77 0 84 27 0 27 14 0
		 14 79 0 79 84 0 14 13 0 13 78 0 78 79 0 85 60 0 60 12 0 77 85 0 83 31 0 31 22 0 22 82 0
		 82 83 0 65 24 0 24 18 0 18 62 0 62 65 0 64 25 0 25 19 0 19 63 0 63 64 0 38 29 0 29 17 0
		 17 37 0 37 38 0 46 30 0 30 20 0 20 44 0 44 46 0 10 30 0 46 43 0 7 29 0 38 34 0 9 25 0
		 64 58 0 65 59 0 15 31 0 83 80 0 81 23 0 23 27 0 84 81 0 36 18 0 39 36 0 45 21 0 21 26 0
		 47 45 0 107 36 0 39 110 0 110 107 0 105 34 0 38 109 0 109 105 0 37 108 0 108 109 0
		 35 106 0 106 110 0 103 32 0 105 103 0 57 0 0 0 87 0 87 86 0 33 104 0 104 106 0 53 45 0
		 47 55 0 55 53 0 51 43 0 46 54 0 54 51 0 44 52 0 52 54 0 48 40 0 51 48 0 42 50 0 50 55 0
		 41 49 0 49 50 0 67 66 0 48 67 0 76 67 0 48 68 0 68 76 0 70 50 0 49 69 0 69 70 0 75 55 0
		 70 75 0 51 71 0 71 68 0 74 54 0 52 72 0 72 74 0 74 71 0 73 53 0 75 73 0 49 67 0 76 69 0
		 41 66 0 61 5 0 5 91 0 91 92 0 92 61 0 7 59 0 65 29 0 6 58 0 64 28 0 28 6 0 63 16 0
		 16 28 0 62 17 0 13 60 0 85 78 0 3 56 0 6 3 0 33 90 0 90 112 0 112 104 0 13 69 0 76 60 0
		 23 73 0 75 27 0 15 71 0 74 31 0 72 22 0 12 68 0 70 14 0;
	setAttr ".ed[166:331]" 3 78 0 85 56 0 16 81 0 84 28 0 9 80 0 83 25 0 82 19 0
		 77 1 0 6 79 0 91 99 0 99 100 0 100 92 0 111 89 0 89 97 0 97 113 0 113 111 0 89 92 0
		 100 97 0 90 93 0 93 101 0 101 98 0 98 90 0 32 88 0 88 87 0 103 4 0 4 89 0 111 103 0
		 97 117 0 117 116 0 116 113 0 94 95 0 95 96 0 96 102 0 102 94 0 101 94 0 102 98 0
		 100 120 0 120 117 0 88 96 0 95 87 0 98 114 0 114 112 0 94 86 0 91 112 0 114 99 0
		 98 122 0 122 118 0 118 114 0 96 123 0 123 121 0 121 102 0 111 88 0 113 96 0 5 104 0
		 11 106 0 105 10 0 26 110 0 30 109 0 108 20 0 21 107 0 120 119 0 119 126 0 126 127 0
		 127 120 0 122 129 0 129 125 0 125 118 0 119 118 0 125 126 0 122 121 0 121 128 0 128 129 0
		 99 119 0 116 123 0 131 115 0 115 124 0 124 127 0 127 131 0 125 131 0 128 131 0 128 130 0
		 130 115 0 124 117 0 115 116 0 130 123 0 159 157 0 157 134 0 134 140 0 140 159 0 134 205 0
		 205 198 0 198 175 0 175 134 0 182 162 0 162 136 0 136 179 0 179 182 0 164 163 0 163 137 0
		 137 143 0 143 164 0 167 164 0 143 150 0 150 167 0 162 165 0 165 142 0 142 136 0 156 158 0
		 158 139 0 139 132 0 132 156 0 174 176 0 176 141 0 141 133 0 133 174 0 161 159 0 140 148 0
		 148 161 0 175 177 0 177 140 0 191 194 0 194 147 0 147 144 0 144 191 0 196 193 0 193 146 0
		 146 151 0 151 196 0 193 192 0 192 145 0 145 146 0 197 191 0 144 178 0 178 197 0 195 82 0
		 22 155 0 155 195 0 181 62 0 18 148 0 148 181 0 180 63 0 19 149 0 149 180 0 160 37 0
		 17 153 0 153 160 0 166 44 0 20 154 0 154 166 0 165 166 0 154 142 0 158 160 0 153 139 0
		 176 180 0 149 141 0 177 181 0 194 195 0 155 147 0 81 196 0 151 23 0 36 161 0 45 167 0
		 150 21 0 107 220 0 220 161 0 217 219 0;
	setAttr ".ed[332:479]" 219 160 0 158 217 0 219 108 0 220 218 0 218 159 0 215 217 0
		 156 215 0 198 199 0 199 132 0 132 175 0 218 216 0 216 157 0 53 173 0 173 167 0 171 172 0
		 172 166 0 165 171 0 172 52 0 168 171 0 162 168 0 173 170 0 170 164 0 170 169 0 169 163 0
		 183 168 0 182 183 0 190 184 0 184 168 0 183 190 0 186 185 0 185 169 0 170 186 0 189 186 0
		 173 189 0 184 187 0 187 171 0 188 72 0 172 188 0 187 188 0 73 189 0 185 190 0 183 169 0
		 182 163 0 179 204 0 204 203 0 203 137 0 137 179 0 153 181 0 177 139 0 138 152 0 152 180 0
		 176 138 0 152 16 0 192 197 0 178 145 0 135 138 0 174 135 0 216 222 0 222 202 0 202 157 0
		 178 190 0 185 145 0 151 189 0 155 188 0 187 147 0 184 144 0 146 186 0 174 197 0 192 135 0
		 152 196 0 149 195 0 194 141 0 133 191 0 193 138 0 204 212 0 212 211 0 211 203 0 221 223 0
		 223 209 0 209 201 0 201 221 0 209 212 0 204 201 0 202 210 0 210 213 0 213 205 0 205 202 0
		 199 200 0 200 156 0 215 221 0 201 136 0 136 215 0 223 226 0 226 227 0 227 209 0 206 214 0
		 214 208 0 208 207 0 207 206 0 210 214 0 206 213 0 227 230 0 230 212 0 199 207 0 208 200 0
		 222 224 0 224 210 0 198 206 0 211 224 0 222 203 0 224 228 0 228 232 0 232 210 0 214 231 0
		 231 233 0 233 208 0 200 221 0 208 223 0 216 137 0 218 143 0 142 217 0 220 150 0 219 154 0
		 230 237 0 237 236 0 236 229 0 229 230 0 228 235 0 235 239 0 239 232 0 236 235 0 228 229 0
		 239 238 0 238 231 0 231 232 0 229 211 0 233 226 0 241 237 0 237 234 0 234 225 0 225 241 0
		 241 235 0 241 238 0 225 240 0 240 238 0 227 234 0 226 225 0 233 240 0;
	setAttr -s 960 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.65178508 0.5618369 0.50942653 -0.65178508
		 0.5618369 0.50942653 -0.65178508 0.5618369 0.50942653 -0.65178508 0.5618369 0.50942653
		 -0.22983311 -0.49365261 0.83873945 -0.22983311 -0.49365261 0.83873945 -0.22983311
		 -0.49365261 0.83873945 -0.22983311 -0.49365261 0.83873945 0.16807356 0.80359304 -0.57095486
		 0.16807356 0.80359304 -0.57095486 0.16807356 0.80359304 -0.57095486 0.16807356 0.80359304
		 -0.57095486 -0.66032249 0.66245401 -0.35373563 -0.66032249 0.66245401 -0.35373563
		 -0.66032249 0.66245401 -0.35373563 -0.66032249 0.66245401 -0.35373563 -0.88732058
		 -0.072090559 -0.45548344 -0.88732058 -0.072090559 -0.45548344 -0.88732058 -0.072090559
		 -0.45548344 -0.88732058 -0.072090559 -0.45548344 0.71854573 0.48312199 -0.50028503
		 0.71854573 0.48312199 -0.50028503 0.71854573 0.48312199 -0.50028503 0.71854573 0.48312199
		 -0.50028503 0.86582357 0.32622236 0.379379 0.86582357 0.32622236 0.379379 0.86582357
		 0.32622236 0.379379 0.86582357 0.32622236 0.379379 0 0.58361 -0.81203413 0 0.58361
		 -0.81203413 0 0.58361 -0.81203413 0 0.58361 -0.81203413 -0.82206064 0.23229961 0.51985884
		 -0.82206064 0.23229961 0.51985884 -0.82206064 0.23229961 0.51985884 -0.82206064 0.23229961
		 0.51985884 -0.20293136 0.36954567 0.90678269 -0.20293136 0.36954567 0.90678269 -0.20293136
		 0.36954567 0.90678269 -0.20293136 0.36954567 0.90678269 0.70158207 0.39116603 0.59562707
		 0.70158207 0.39116603 0.59562707 0.70158207 0.39116603 0.59562707 0.70158207 0.39116603
		 0.59562707 -0.98962218 -0.075496614 0.12226306 -0.98962218 -0.075496614 0.12226306
		 -0.98962218 -0.075496614 0.12226306 -0.98962218 -0.075496614 0.12226306 -0.81976408
		 0.29087776 0.49333262 -0.81976408 0.29087776 0.49333262 -0.81976408 0.29087776 0.49333262
		 -0.81976408 0.29087776 0.49333262 0 0.73159504 0.68173951 0 0.73159504 0.68173951
		 0 0.73159504 0.68173951 0 0.73159504 0.68173951 0.92521238 0.12651056 0.35773885
		 0.92521238 0.12651056 0.35773885 0.92521238 0.12651056 0.35773885 0.92521238 0.12651056
		 0.35773885 -0.21225609 0.019848038 0.97701246 -0.21225609 0.019848038 0.97701246
		 -0.21225609 0.019848038 0.97701246 -0.21225609 0.019848038 0.97701246 0.036250941
		 -0.81737709 -0.57496142 0.036250941 -0.81737709 -0.57496142 0.036250941 -0.81737709
		 -0.57496142 0.036250941 -0.81737709 -0.57496142 0.92270792 0 0.38549969 0.92270792
		 0 0.38549969 0.92270792 0 0.38549969 0.92270792 0 0.38549969 0.90471387 -0.089730069
		 -0.41646287 0.90471387 -0.089730069 -0.41646287 0.90471387 -0.089730069 -0.41646287
		 0.90471387 -0.089730069 -0.41646287 0.8913694 0.03923941 -0.45157591 0.8913694 0.03923941
		 -0.45157591 0.8913694 0.03923941 -0.45157591 0.8913694 0.03923941 -0.45157591 0.91099274
		 0.023066673 0.41177681 0.91099274 0.023066673 0.41177681 0.91099274 0.023066673 0.41177681
		 0.91099274 0.023066673 0.41177681 -0.022331348 0.61810166 -0.78578097 -0.022331348
		 0.61810166 -0.78578097 -0.022331348 0.61810166 -0.78578097 -0.022331348 0.61810166
		 -0.78578097 -0.21748847 0.15539989 0.96361274 -0.21748847 0.15539989 0.96361274 -0.21748847
		 0.15539989 0.96361274 -0.21748847 0.15539989 0.96361274 0.9061386 -0.20219213 0.37152565
		 0.9061386 -0.20219213 0.37152565 0.9061386 -0.20219213 0.37152565 0.9061386 -0.20219213
		 0.37152565 -0.94961667 -0.28539771 0.12952302 -0.94961667 -0.28539771 0.12952302
		 -0.94961667 -0.28539771 0.12952302 -0.94961667 -0.28539771 0.12952302 -0.9024139
		 0.064124614 0.42607185 -0.9024139 0.064124614 0.42607185 -0.9024139 0.064124614 0.42607185
		 -0.9024139 0.064124614 0.42607185 -0.87958139 -0.23499021 -0.41366184 -0.87958139
		 -0.23499021 -0.41366184 -0.87958139 -0.23499021 -0.41366184 -0.87958139 -0.23499021
		 -0.41366184 -0.99804062 0.061765932 0.0099977367 -0.99804062 0.061765932 0.0099977367
		 -0.99804062 0.061765932 0.0099977367 -0.99804062 0.061765932 0.0099977367 0.99998939
		 -0.00053265481 -0.0045758677 0.99998939 -0.00053265481 -0.0045758677 0.99998939 -0.00053265481
		 -0.0045758677 0.99998939 -0.00053265481 -0.0045758677 0.99998498 -0.00055343256 -0.0054433774
		 0.99998498 -0.00055343256 -0.0054433774 0.99998498 -0.00055343256 -0.0054433774 0.99998498
		 -0.00055343256 -0.0054433774 -0.99376911 0.10320608 -0.042088486 -0.99376911 0.10320608
		 -0.042088486 -0.99376911 0.10320608 -0.042088486 -0.99376911 0.10320608 -0.042088486
		 0.8300705 0.51510686 -0.21365377 0.8300705 0.51510686 -0.21365377 0.8300705 0.51510686
		 -0.21365377 0.8300705 0.51510686 -0.21365377 0.24540125 -0.53083092 0.81117004 0.24540125
		 -0.53083092 0.81117004 0.24540125 -0.53083092 0.81117004 0.24540125 -0.53083092 0.81117004
		 -0.73127097 0.65705454 -0.18309075 -0.73127097 0.65705454 -0.18309075 -0.73127097
		 0.65705454 -0.18309075 -0.73127097 0.65705454 -0.18309075 -0.98934191 -0.022680027
		 -0.14383419 -0.98934191 -0.022680027 -0.14383419 -0.98934191 -0.022680027 -0.14383419
		 -0.98934191 -0.022680027 -0.14383419 0.96902376 0.051552635 -0.24152745 0.96902376
		 0.051552635 -0.24152745 0.96902376 0.051552635 -0.24152745 0.96902376 0.051552635
		 -0.24152745 0.97614926 0.020791737 -0.21610259 0.97614926 0.020791737 -0.21610259
		 0.97614926 0.020791737 -0.21610259 0.97614926 0.020791737 -0.21610259 0.75554818
		 0.56982994 -0.32317317 0.75554818 0.56982994 -0.32317317 0.75554818 0.56982994 -0.32317317
		 0.75554818 0.56982994 -0.32317317 -0.98102653 0.10730619 -0.16146898 -0.98102653
		 0.10730619 -0.16146898 -0.98102653 0.10730619 -0.16146898 -0.98102653 0.10730619
		 -0.16146898 -0.67485118 0.7030912 -0.22414 -0.67485118 0.7030912 -0.22414 -0.67485118
		 0.7030912 -0.22414 -0.67485118 0.7030912 -0.22414 0.103714 0.94125843 -0.3213658
		 0.103714 0.94125843 -0.3213658 0.103714 0.94125843 -0.3213658 0.103714 0.94125843
		 -0.3213658 0.11312214 0.98696899 -0.11443629 0.11312214 0.98696899 -0.11443629;
	setAttr ".n[166:331]" -type "float3"  0.11312214 0.98696899 -0.11443629 0.11312214
		 0.98696899 -0.11443629 -0.6307621 0.74846494 -0.20479086 -0.6307621 0.74846494 -0.20479086
		 -0.6307621 0.74846494 -0.20479086 -0.6307621 0.74846494 -0.20479086 -0.95036578 0.17215396
		 -0.25916755 -0.95036578 0.17215396 -0.25916755 -0.95036578 0.17215396 -0.25916755
		 -0.95036578 0.17215396 -0.25916755 0.71815979 0.69453114 0.043278534 0.71815979 0.69453114
		 0.043278534 0.71815979 0.69453114 0.043278534 0.71815979 0.69453114 0.043278534 0.96481466
		 -0.041050877 0.2597062 0.96481466 -0.041050877 0.2597062 0.96481466 -0.041050877
		 0.2597062 0.96481466 -0.041050877 0.2597062 0.96059763 -0.10889298 0.25572354 0.96059763
		 -0.10889298 0.25572354 0.96059763 -0.10889298 0.25572354 0.96059763 -0.10889298 0.25572354
		 -0.9640851 0.067952059 -0.2567535 -0.9640851 0.067952059 -0.2567535 -0.9640851 0.067952059
		 -0.2567535 -0.9640851 0.067952059 -0.2567535 -0.171745 0.97620738 -0.13237332 -0.171745
		 0.97620738 -0.13237332 -0.171745 0.97620738 -0.13237332 -0.171745 0.97620738 -0.13237332
		 -0.13315134 0.94586986 -0.29597446 -0.13315134 0.94586986 -0.29597446 -0.13315134
		 0.94586986 -0.29597446 -0.13315134 0.94586986 -0.29597446 -0.29360041 -0.055834338
		 -0.95429629 -0.29360041 -0.055834338 -0.95429629 -0.29360041 -0.055834338 -0.95429629
		 -0.29360041 -0.055834338 -0.95429629 0.26575336 0.087177373 0.96009123 0.26575336
		 0.087177373 0.96009123 0.26575336 0.087177373 0.96009123 0.26575336 0.087177373 0.96009123
		 0.026987221 0.61847335 -0.78534234 0.026987221 0.61847335 -0.78534234 0.026987221
		 0.61847335 -0.78534234 0.026987221 0.61847335 -0.78534234 -0.035000443 -0.80210489
		 -0.5961566 -0.035000443 -0.80210489 -0.5961566 -0.035000443 -0.80210489 -0.5961566
		 -0.035000443 -0.80210489 -0.5961566 0.2506029 -0.013330703 0.96799815 0.2506029 -0.013330703
		 0.96799815 0.2506029 -0.013330703 0.96799815 0.2506029 -0.013330703 0.96799815 -0.3227061
		 0.63928646 0.6979782 -0.3227061 0.63928646 0.6979782 -0.3227061 0.63928646 0.6979782
		 -0.3227061 0.63928646 0.6979782 0.24313292 0.31614318 0.91702771 0.24313292 0.31614318
		 0.91702771 0.24313292 0.31614318 0.91702771 0.24313292 0.31614318 0.91702771 -0.0071196393
		 0.58907503 -0.808047 -0.0071196393 0.58907503 -0.808047 -0.0071196393 0.58907503
		 -0.808047 -0.0071196393 0.58907503 -0.808047 -0.21734308 0.85859346 -0.46430498 -0.21734308
		 0.85859346 -0.46430498 -0.21734308 0.85859346 -0.46430498 -0.21734308 0.85859346
		 -0.46430498 -0.99781877 -0.028455531 -0.059565689 -0.99781877 -0.028455531 -0.059565689
		 -0.99781877 -0.028455531 -0.059565689 -0.99781877 -0.028455531 -0.059565689 -0.39986426
		 0.78515059 0.4729133 -0.39986426 0.78515059 0.4729133 -0.39986426 0.78515059 0.4729133
		 -0.39986426 0.78515059 0.4729133 -0.82392472 -0.27767423 0.49400923 -0.82392472 -0.27767423
		 0.49400923 -0.82392472 -0.27767423 0.49400923 -0.82392472 -0.27767423 0.49400923
		 0.83221936 -0.21273668 0.5120098 0.83221936 -0.21273668 0.5120098 0.83221936 -0.21273668
		 0.5120098 0.83221936 -0.21273668 0.5120098 0.8632341 -0.059886228 0.50123912 0.8632341
		 -0.059886228 0.50123912 0.8632341 -0.059886228 0.50123912 0.8632341 -0.059886228
		 0.50123912 0.60351121 0.49925214 0.62170851 0.60351121 0.49925214 0.62170851 0.60351121
		 0.49925214 0.62170851 0.60351121 0.49925214 0.62170851 -0.69929421 -0.41298884 0.58346194
		 -0.69929421 -0.41298884 0.58346194 -0.69929421 -0.41298884 0.58346194 -0.69929421
		 -0.41298884 0.58346194 -0.72331041 0.35305816 0.59344083 -0.72331041 0.35305816 0.59344083
		 -0.72331041 0.35305816 0.59344083 -0.72331041 0.35305816 0.59344083 0.0066322233
		 0.82409805 0.5664084 0.0066322233 0.82409805 0.5664084 0.0066322233 0.82409805 0.5664084
		 0.0066322233 0.82409805 0.5664084 -0.21079351 0.91078508 0.35501653 -0.21079351 0.91078508
		 0.35501653 -0.21079351 0.91078508 0.35501653 -0.21079351 0.91078508 0.35501653 -0.86190951
		 0.10521515 -0.49602595 -0.86190951 0.10521515 -0.49602595 -0.86190951 0.10521515
		 -0.49602595 -0.86190951 0.10521515 -0.49602595 0.99077952 -0.034577761 -0.13099758
		 0.99077952 -0.034577761 -0.13099758 0.99077952 -0.034577761 -0.13099758 0.99077952
		 -0.034577761 -0.13099758 0.51966739 -0.78972453 -0.32600772 0.51966739 -0.78972453
		 -0.32600772 0.51966739 -0.78972453 -0.32600772 0.51966739 -0.78972453 -0.32600772
		 0 0.92551148 0.37871933 0 0.92551148 0.37871933 0 0.92551148 0.37871933 0 0.92551148
		 0.37871933 -0.89745986 0.42599925 -0.11441369 -0.89745986 0.42599925 -0.11441369
		 -0.89745986 0.42599925 -0.11441369 -0.89745986 0.42599925 -0.11441369 -0.86303967
		 0.2016073 -0.46316001 -0.86303967 0.2016073 -0.46316001 -0.86303967 0.2016073 -0.46316001
		 -0.86303967 0.2016073 -0.46316001 0.75720608 0.62091851 0.20272966 0.75720608 0.62091851
		 0.20272966 0.75720608 0.62091851 0.20272966 0.75720608 0.62091851 0.20272966 -0.20479774
		 0.7880258 -0.58058012 -0.20479774 0.7880258 -0.58058012 -0.20479774 0.7880258 -0.58058012
		 -0.20479774 0.7880258 -0.58058012 0.78244597 0.28265837 -0.55487168 0.78244597 0.28265837
		 -0.55487168 0.78244597 0.28265837 -0.55487168 0.78244597 0.28265837 -0.55487168 0.15375416
		 0.75296754 -0.63984334 0.15375416 0.75296754 -0.63984334 0.15375416 0.75296754 -0.63984334
		 0.15375416 0.75296754 -0.63984334 -0.88088799 0.094054565 0.46388578 -0.88088799
		 0.094054565 0.46388578 -0.88088799 0.094054565 0.46388578 -0.88088799 0.094054565
		 0.46388578 0.85444987 -0.35623306 0.37817103 0.85444987 -0.35623306 0.37817103 0.85444987
		 -0.35623306 0.37817103 0.85444987 -0.35623306 0.37817103 0.90920442 0.0092245899
		 -0.4162477 0.90920442 0.0092245899 -0.4162477 0.90920442 0.0092245899 -0.4162477
		 0.90920442 0.0092245899 -0.4162477 0.20000486 0.018549144 -0.97961932 0.20000486
		 0.018549144 -0.97961932 0.20000486 0.018549144 -0.97961932 0.20000486 0.018549144
		 -0.97961932;
	setAttr ".n[332:497]" -type "float3"  -0.81988782 -0.21995662 0.52858579 -0.81988782
		 -0.21995662 0.52858579 -0.81988782 -0.21995662 0.52858579 -0.81988782 -0.21995662
		 0.52858579 -0.16302545 0.86806899 -0.46891251 -0.16302545 0.86806899 -0.46891251
		 -0.16302545 0.86806899 -0.46891251 -0.16302545 0.86806899 -0.46891251 0.17770177
		 0.96922016 0.17039472 0.17770177 0.96922016 0.17039472 0.17770177 0.96922016 0.17039472
		 0.17770177 0.96922016 0.17039472 -0.14698501 0.97182006 0.18428545 -0.14698501 0.97182006
		 0.18428545 -0.14698501 0.97182006 0.18428545 -0.14698501 0.97182006 0.18428545 0.14462525
		 0.86086053 -0.48785514 0.14462525 0.86086053 -0.48785514 0.14462525 0.86086053 -0.48785514
		 0.14462525 0.86086053 -0.48785514 0.89116061 0.29505628 0.34463701 0.89116061 0.29505628
		 0.34463701 0.89116061 0.29505628 0.34463701 0.89116061 0.29505628 0.34463701 -0.96997654
		 0.17996676 -0.16357765 -0.96997654 0.17996676 -0.16357765 -0.96997654 0.17996676
		 -0.16357765 -0.96997654 0.17996676 -0.16357765 0.38201869 -0.0076629245 0.92412275
		 0.38201869 -0.0076629245 0.92412275 0.38201869 -0.0076629245 0.92412275 0.38201869
		 -0.0076629245 0.92412275 -0.33008498 -0.063966684 0.9417814 -0.33008498 -0.063966684
		 0.9417814 -0.33008498 -0.063966684 0.9417814 -0.33008498 -0.063966684 0.9417814 -0.76224416
		 0.46222624 -0.45313439 -0.76224416 0.46222624 -0.45313439 -0.76224416 0.46222624
		 -0.45313439 -0.76224416 0.46222624 -0.45313439 0.022534747 0.93589687 -0.35155261
		 0.022534747 0.93589687 -0.35155261 0.022534747 0.93589687 -0.35155261 0.022534747
		 0.93589687 -0.35155261 0.045107991 0.93723285 -0.34577429 0.045107991 0.93723285
		 -0.34577429 0.045107991 0.93723285 -0.34577429 0.045107991 0.93723285 -0.34577429
		 0.93832976 -0.23959452 -0.2492625 0.93832976 -0.23959452 -0.2492625 0.93832976 -0.23959452
		 -0.2492625 0.93832976 -0.23959452 -0.2492625 0.88745785 0.24952787 -0.38749772 0.88745785
		 0.24952787 -0.38749772 0.88745785 0.24952787 -0.38749772 0.88745785 0.24952787 -0.38749772
		 -0.96329165 0.1110063 -0.24443145 -0.96329165 0.1110063 -0.24443145 -0.96329165 0.1110063
		 -0.24443145 -0.96329165 0.1110063 -0.24443145 -0.68241912 0.68124521 -0.26496997
		 -0.68241912 0.68124521 -0.26496997 -0.68241912 0.68124521 -0.26496997 -0.68241912
		 0.68124521 -0.26496997 0.70670158 0.5245946 -0.47473487 0.70670158 0.5245946 -0.47473487
		 0.70670158 0.5245946 -0.47473487 0.70670158 0.5245946 -0.47473487 -0.96737343 0.25305843
		 -0.012255368 -0.96737343 0.25305843 -0.012255368 -0.96737343 0.25305843 -0.012255368
		 -0.96737343 0.25305843 -0.012255368 0.90870607 -0.12625739 -0.39788499 0.90870607
		 -0.12625739 -0.39788499 0.90870607 -0.12625739 -0.39788499 0.90870607 -0.12625739
		 -0.39788499 0.89613223 -0.036658239 -0.44227055 0.89613223 -0.036658239 -0.44227055
		 0.89613223 -0.036658239 -0.44227055 0.89613223 -0.036658239 -0.44227055 -0.99133426
		 0.10025715 0.084881976 -0.99133426 0.10025715 0.084881976 -0.99133426 0.10025715
		 0.084881976 -0.99133426 0.10025715 0.084881976 0.27872437 0.844724 0.45689616 0.27872437
		 0.844724 0.45689616 0.27872437 0.844724 0.45689616 0.27872437 0.844724 0.45689616
		 0.8039068 0.44477224 -0.39485654 0.8039068 0.44477224 -0.39485654 0.8039068 0.44477224
		 -0.39485654 0.8039068 0.44477224 -0.39485654 0.83800703 0.52932423 -0.132514 0.83800703
		 0.52932423 -0.132514 0.83800703 0.52932423 -0.132514 0.83800703 0.52932423 -0.132514
		 0.2667858 0.16300827 -0.94987035 0.2667858 0.16300827 -0.94987035 0.2667858 0.16300827
		 -0.94987035 0.2667858 0.16300827 -0.94987035 -0.17173797 0.86505473 -0.4713667 -0.17173797
		 0.86505473 -0.4713667 -0.17173797 0.86505473 -0.4713667 -0.17173797 0.86505473 -0.4713667
		 -0.037209131 0.94272971 -0.33147573 -0.037209131 0.94272971 -0.33147573 -0.037209131
		 0.94272971 -0.33147573 -0.037209131 0.94272971 -0.33147573 -0.015378046 0.93720549
		 -0.34843865 -0.015378046 0.93720549 -0.34843865 -0.015378046 0.93720549 -0.34843865
		 -0.015378046 0.93720549 -0.34843865 0.29610059 0.84165853 -0.45159194 0.29610059
		 0.84165853 -0.45159194 0.29610059 0.84165853 -0.45159194 0.29610059 0.84165853 -0.45159194
		 0.0229946 0.86488336 -0.50144601 0.0229946 0.86488336 -0.50144601 0.0229946 0.86488336
		 -0.50144601 0.0229946 0.86488336 -0.50144601 -0.017379813 0.86888677 -0.49470568
		 -0.017379813 0.86888677 -0.49470568 -0.017379813 0.86888677 -0.49470568 -0.017379813
		 0.86888677 -0.49470568 -0.072427712 0.87049079 -0.48682636 -0.072427712 0.87049079
		 -0.48682636 -0.072427712 0.87049079 -0.48682636 -0.072427712 0.87049079 -0.48682636
		 0.1064236 0.85174531 -0.51303405 0.1064236 0.85174531 -0.51303405 0.1064236 0.85174531
		 -0.51303405 0.1064236 0.85174531 -0.51303405 -0.30118796 0.8099457 0.50326324 -0.30118796
		 0.8099457 0.50326324 -0.30118796 0.8099457 0.50326324 -0.30118796 0.8099457 0.50326324
		 -0.8966496 0.43642655 0.074507259 -0.8966496 0.43642655 0.074507259 -0.8966496 0.43642655
		 0.074507259 -0.8966496 0.43642655 0.074507259 -0.90751225 0.41852722 -0.035448655
		 -0.90751225 0.41852722 -0.035448655 -0.90751225 0.41852722 -0.035448655 -0.90751225
		 0.41852722 -0.035448655 -0.42134029 0.21622796 -0.88074839 -0.42134029 0.21622796
		 -0.88074839 -0.42134029 0.21622796 -0.88074839 -0.42134029 0.21622796 -0.88074839
		 -0.6517849 -0.56183708 0.50942665 -0.6517849 -0.56183708 0.50942665 -0.6517849 -0.56183708
		 0.50942665 -0.6517849 -0.56183708 0.50942665 -0.22983313 0.49365267 0.83873945 -0.22983313
		 0.49365267 0.83873945 -0.22983313 0.49365267 0.83873945 -0.22983313 0.49365267 0.83873945
		 0.16807356 -0.80359304 -0.57095492 0.16807356 -0.80359304 -0.57095492 0.16807356
		 -0.80359304 -0.57095492 0.16807356 -0.80359304 -0.57095492 -0.66032332 -0.66245341
		 -0.35373521 -0.66032332 -0.66245341 -0.35373521 -0.66032332 -0.66245341 -0.35373521
		 -0.66032332 -0.66245341 -0.35373521 -0.88732076 0.072090648 -0.45548314 -0.88732076
		 0.072090648 -0.45548314;
	setAttr ".n[498:663]" -type "float3"  -0.88732076 0.072090648 -0.45548314 -0.88732076
		 0.072090648 -0.45548314 0.71854508 -0.48312265 -0.50028557 0.71854508 -0.48312265
		 -0.50028557 0.71854508 -0.48312265 -0.50028557 0.71854508 -0.48312265 -0.50028557
		 0.86582375 -0.32622218 0.37937889 0.86582375 -0.32622218 0.37937889 0.86582375 -0.32622218
		 0.37937889 0.86582375 -0.32622218 0.37937889 0 -0.58361006 -0.81203413 0 -0.58361006
		 -0.81203413 0 -0.58361006 -0.81203413 0 -0.58361006 -0.81203413 -0.82206076 -0.23229949
		 0.51985854 -0.82206076 -0.23229949 0.51985854 -0.82206076 -0.23229949 0.51985854
		 -0.82206076 -0.23229949 0.51985854 -0.20293137 -0.36954567 0.90678269 -0.20293137
		 -0.36954567 0.90678269 -0.20293137 -0.36954567 0.90678269 -0.20293137 -0.36954567
		 0.90678269 0.70158207 -0.39116603 0.59562713 0.70158207 -0.39116603 0.59562713 0.70158207
		 -0.39116603 0.59562713 0.70158207 -0.39116603 0.59562713 -0.98962218 0.075496539
		 0.12226306 -0.98962218 0.075496539 0.12226306 -0.98962218 0.075496539 0.12226306
		 -0.98962218 0.075496539 0.12226306 -0.81976408 -0.29087776 0.49333254 -0.81976408
		 -0.29087776 0.49333254 -0.81976408 -0.29087776 0.49333254 -0.81976408 -0.29087776
		 0.49333254 0 -0.73159504 0.68173945 0 -0.73159504 0.68173945 0 -0.73159504 0.68173945
		 0 -0.73159504 0.68173945 0.92521238 -0.12651061 0.35773885 0.92521238 -0.12651061
		 0.35773885 0.92521238 -0.12651061 0.35773885 0.92521238 -0.12651061 0.35773885 -0.21225609
		 -0.019848038 0.97701246 -0.21225609 -0.019848038 0.97701246 -0.21225609 -0.019848038
		 0.97701246 -0.21225609 -0.019848038 0.97701246 0.036250941 0.81737709 -0.57496142
		 0.036250941 0.81737709 -0.57496142 0.036250941 0.81737709 -0.57496142 0.036250941
		 0.81737709 -0.57496142 0.92270792 0 0.38549969 0.92270792 0 0.38549969 0.92270792
		 0 0.38549969 0.92270792 0 0.38549969 0.90471387 0.089729987 -0.41646287 0.90471387
		 0.089729987 -0.41646287 0.90471387 0.089729987 -0.41646287 0.90471387 0.089729987
		 -0.41646287 0.89136934 -0.039239645 -0.45157611 0.89136934 -0.039239645 -0.45157611
		 0.89136934 -0.039239645 -0.45157611 0.89136934 -0.039239645 -0.45157611 0.9109928
		 -0.023066621 0.4117766 0.9109928 -0.023066621 0.4117766 0.9109928 -0.023066621 0.4117766
		 0.9109928 -0.023066621 0.4117766 -0.022331277 -0.61810166 -0.78578091 -0.022331277
		 -0.61810166 -0.78578091 -0.022331277 -0.61810166 -0.78578091 -0.022331277 -0.61810166
		 -0.78578091 -0.21748886 -0.15539989 0.96361268 -0.21748886 -0.15539989 0.96361268
		 -0.21748886 -0.15539989 0.96361268 -0.21748886 -0.15539989 0.96361268 0.90613848
		 0.20219214 0.37152576 0.90613848 0.20219214 0.37152576 0.90613848 0.20219214 0.37152576
		 0.90613848 0.20219214 0.37152576 -0.94961679 0.28539756 0.1295229 -0.94961679 0.28539756
		 0.1295229 -0.94961679 0.28539756 0.1295229 -0.94961679 0.28539756 0.1295229 -0.90241385
		 -0.064124614 0.42607185 -0.90241385 -0.064124614 0.42607185 -0.90241385 -0.064124614
		 0.42607185 -0.90241385 -0.064124614 0.42607185 -0.87958157 0.23499009 -0.41366166
		 -0.87958157 0.23499009 -0.41366166 -0.87958157 0.23499009 -0.41366166 -0.87958157
		 0.23499009 -0.41366166 -0.99804062 -0.061766021 0.0099977367 -0.99804062 -0.061766021
		 0.0099977367 -0.99804062 -0.061766021 0.0099977367 -0.99804062 -0.061766021 0.0099977367
		 0.99998933 0.00053265499 -0.0045758691 0.99998933 0.00053265499 -0.0045758691 0.99998933
		 0.00053265499 -0.0045758691 0.99998933 0.00053265499 -0.0045758691 0.99998498 0.00055343256
		 -0.0054433774 0.99998498 0.00055343256 -0.0054433774 0.99998498 0.00055343256 -0.0054433774
		 0.99998498 0.00055343256 -0.0054433774 -0.99376911 -0.10320621 -0.042088486 -0.99376911
		 -0.10320621 -0.042088486 -0.99376911 -0.10320621 -0.042088486 -0.99376911 -0.10320621
		 -0.042088486 0.83007061 -0.51510662 -0.21365367 0.83007061 -0.51510662 -0.21365367
		 0.83007061 -0.51510662 -0.21365367 0.83007061 -0.51510662 -0.21365367 0.24540126
		 0.53083098 0.81116998 0.24540126 0.53083098 0.81116998 0.24540126 0.53083098 0.81116998
		 0.24540126 0.53083098 0.81116998 -0.73127097 -0.65705454 -0.18309076 -0.73127097
		 -0.65705454 -0.18309076 -0.73127097 -0.65705454 -0.18309076 -0.73127097 -0.65705454
		 -0.18309076 -0.98934191 0.022680031 -0.1438342 -0.98934191 0.022680031 -0.1438342
		 -0.98934191 0.022680031 -0.1438342 -0.98934191 0.022680031 -0.1438342 0.96902364
		 -0.051552564 -0.24152747 0.96902364 -0.051552564 -0.24152747 0.96902364 -0.051552564
		 -0.24152747 0.96902364 -0.051552564 -0.24152747 0.97614926 -0.020791812 -0.21610259
		 0.97614926 -0.020791812 -0.21610259 0.97614926 -0.020791812 -0.21610259 0.97614926
		 -0.020791812 -0.21610259 0.75554812 -0.56982994 -0.32317314 0.75554812 -0.56982994
		 -0.32317314 0.75554812 -0.56982994 -0.32317314 0.75554812 -0.56982994 -0.32317314
		 -0.98102653 -0.10730634 -0.16146898 -0.98102653 -0.10730634 -0.16146898 -0.98102653
		 -0.10730634 -0.16146898 -0.98102653 -0.10730634 -0.16146898 -0.67485142 -0.70309091
		 -0.22413996 -0.67485142 -0.70309091 -0.22413996 -0.67485142 -0.70309091 -0.22413996
		 -0.67485142 -0.70309091 -0.22413996 0.103714 -0.94125843 -0.3213658 0.103714 -0.94125843
		 -0.3213658 0.103714 -0.94125843 -0.3213658 0.103714 -0.94125843 -0.3213658 0.11312214
		 -0.98696899 -0.11443629 0.11312214 -0.98696899 -0.11443629 0.11312214 -0.98696899
		 -0.11443629 0.11312214 -0.98696899 -0.11443629 -0.63076222 -0.74846482 -0.20479086
		 -0.63076222 -0.74846482 -0.20479086 -0.63076222 -0.74846482 -0.20479086 -0.63076222
		 -0.74846482 -0.20479086 -0.9503659 -0.17215391 -0.25916758 -0.9503659 -0.17215391
		 -0.25916758 -0.9503659 -0.17215391 -0.25916758 -0.9503659 -0.17215391 -0.25916758
		 0.71815979 -0.69453114 0.043278534 0.71815979 -0.69453114 0.043278534 0.71815979
		 -0.69453114 0.043278534 0.71815979 -0.69453114 0.043278534 0.96481466 0.041050974
		 0.2597062 0.96481466 0.041050974 0.2597062 0.96481466 0.041050974 0.2597062 0.96481466
		 0.041050974 0.2597062;
	setAttr ".n[664:829]" -type "float3"  0.96059746 0.1088931 0.25572363 0.96059746
		 0.1088931 0.25572363 0.96059746 0.1088931 0.25572363 0.96059746 0.1088931 0.25572363
		 -0.9640851 -0.067951858 -0.2567535 -0.9640851 -0.067951858 -0.2567535 -0.9640851
		 -0.067951858 -0.2567535 -0.9640851 -0.067951858 -0.2567535 -0.171745 -0.97620738
		 -0.13237333 -0.171745 -0.97620738 -0.13237333 -0.171745 -0.97620738 -0.13237333 -0.171745
		 -0.97620738 -0.13237333 -0.13315113 -0.94586986 -0.29597437 -0.13315113 -0.94586986
		 -0.29597437 -0.13315113 -0.94586986 -0.29597437 -0.13315113 -0.94586986 -0.29597437
		 -0.2936005 0.05583439 -0.95429623 -0.2936005 0.05583439 -0.95429623 -0.2936005 0.05583439
		 -0.95429623 -0.2936005 0.05583439 -0.95429623 0.26575291 -0.087177396 0.96009141
		 0.26575291 -0.087177396 0.96009141 0.26575291 -0.087177396 0.96009141 0.26575291
		 -0.087177396 0.96009141 0.026987137 -0.61847335 -0.78534234 0.026987137 -0.61847335
		 -0.78534234 0.026987137 -0.61847335 -0.78534234 0.026987137 -0.61847335 -0.78534234
		 -0.035000443 0.80210489 -0.5961566 -0.035000443 0.80210489 -0.5961566 -0.035000443
		 0.80210489 -0.5961566 -0.035000443 0.80210489 -0.5961566 0.2506029 0.013330703 0.96799815
		 0.2506029 0.013330703 0.96799815 0.2506029 0.013330703 0.96799815 0.2506029 0.013330703
		 0.96799815 -0.32270646 -0.6392864 0.69797808 -0.32270646 -0.6392864 0.69797808 -0.32270646
		 -0.6392864 0.69797808 -0.32270646 -0.6392864 0.69797808 0.24313287 -0.31614316 0.91702771
		 0.24313287 -0.31614316 0.91702771 0.24313287 -0.31614316 0.91702771 0.24313287 -0.31614316
		 0.91702771 -0.0071196393 -0.58907503 -0.808047 -0.0071196393 -0.58907503 -0.808047
		 -0.0071196393 -0.58907503 -0.808047 -0.0071196393 -0.58907503 -0.808047 -0.21734256
		 -0.8585937 -0.46430489 -0.21734256 -0.8585937 -0.46430489 -0.21734256 -0.8585937
		 -0.46430489 -0.21734256 -0.8585937 -0.46430489 -0.99781877 0.028455542 -0.059565745
		 -0.99781877 0.028455542 -0.059565745 -0.99781877 0.028455542 -0.059565745 -0.99781877
		 0.028455542 -0.059565745 -0.39986426 -0.78515059 0.47291341 -0.39986426 -0.78515059
		 0.47291341 -0.39986426 -0.78515059 0.47291341 -0.39986426 -0.78515059 0.47291341
		 -0.82392508 0.27767405 0.49400863 -0.82392508 0.27767405 0.49400863 -0.82392508 0.27767405
		 0.49400863 -0.82392508 0.27767405 0.49400863 0.83221954 0.21273659 0.51200944 0.83221954
		 0.21273659 0.51200944 0.83221954 0.21273659 0.51200944 0.83221954 0.21273659 0.51200944
		 0.86323398 0.0598864 0.50123906 0.86323398 0.0598864 0.50123906 0.86323398 0.0598864
		 0.50123906 0.86323398 0.0598864 0.50123906 0.60351127 -0.49925211 0.62170845 0.60351127
		 -0.49925211 0.62170845 0.60351127 -0.49925211 0.62170845 0.60351127 -0.49925211 0.62170845
		 -0.69929403 0.41298893 0.58346206 -0.69929403 0.41298893 0.58346206 -0.69929403 0.41298893
		 0.58346206 -0.69929403 0.41298893 0.58346206 -0.72330976 -0.35305852 0.59344143 -0.72330976
		 -0.35305852 0.59344143 -0.72330976 -0.35305852 0.59344143 -0.72330976 -0.35305852
		 0.59344143 0.0066322233 -0.82409805 0.5664084 0.0066322233 -0.82409805 0.5664084
		 0.0066322233 -0.82409805 0.5664084 0.0066322233 -0.82409805 0.5664084 -0.21079351
		 -0.91078502 0.35501656 -0.21079351 -0.91078502 0.35501656 -0.21079351 -0.91078502
		 0.35501656 -0.21079351 -0.91078502 0.35501656 -0.86190945 -0.10521524 -0.49602589
		 -0.86190945 -0.10521524 -0.49602589 -0.86190945 -0.10521524 -0.49602589 -0.86190945
		 -0.10521524 -0.49602589 0.99077952 0.034577694 -0.13099763 0.99077952 0.034577694
		 -0.13099763 0.99077952 0.034577694 -0.13099763 0.99077952 0.034577694 -0.13099763
		 0.51966733 0.78972453 -0.32600766 0.51966733 0.78972453 -0.32600766 0.51966733 0.78972453
		 -0.32600766 0.51966733 0.78972453 -0.32600766 0 -0.92551166 0.37871936 0 -0.92551166
		 0.37871936 0 -0.92551166 0.37871936 0 -0.92551166 0.37871936 -0.89745975 -0.42599946
		 -0.11441372 -0.89745975 -0.42599946 -0.11441372 -0.89745975 -0.42599946 -0.11441372
		 -0.89745975 -0.42599946 -0.11441372 -0.86303967 -0.20160727 -0.46315992 -0.86303967
		 -0.20160727 -0.46315992 -0.86303967 -0.20160727 -0.46315992 -0.86303967 -0.20160727
		 -0.46315992 0.75720596 -0.62091857 0.20272966 0.75720596 -0.62091857 0.20272966 0.75720596
		 -0.62091857 0.20272966 0.75720596 -0.62091857 0.20272966 -0.20479773 -0.78802568
		 -0.58058017 -0.20479773 -0.78802568 -0.58058017 -0.20479773 -0.78802568 -0.58058017
		 -0.20479773 -0.78802568 -0.58058017 0.78244603 -0.28265843 -0.55487156 0.78244603
		 -0.28265843 -0.55487156 0.78244603 -0.28265843 -0.55487156 0.78244603 -0.28265843
		 -0.55487156 0.15375416 -0.75296748 -0.63984352 0.15375416 -0.75296748 -0.63984352
		 0.15375416 -0.75296748 -0.63984352 0.15375416 -0.75296748 -0.63984352 -0.88088781
		 -0.094054624 0.46388602 -0.88088781 -0.094054624 0.46388602 -0.88088781 -0.094054624
		 0.46388602 -0.88088781 -0.094054624 0.46388602 0.85444987 0.35623312 0.37817112 0.85444987
		 0.35623312 0.37817112 0.85444987 0.35623312 0.37817112 0.85444987 0.35623312 0.37817112
		 0.90920442 -0.0092245946 -0.41624761 0.90920442 -0.0092245946 -0.41624761 0.90920442
		 -0.0092245946 -0.41624761 0.90920442 -0.0092245946 -0.41624761 0.20000519 -0.018549148
		 -0.9796192 0.20000519 -0.018549148 -0.9796192 0.20000519 -0.018549148 -0.9796192
		 0.20000519 -0.018549148 -0.9796192 -0.81988806 0.21995646 0.52858555 -0.81988806
		 0.21995646 0.52858555 -0.81988806 0.21995646 0.52858555 -0.81988806 0.21995646 0.52858555
		 -0.16302545 -0.86806899 -0.46891251 -0.16302545 -0.86806899 -0.46891251 -0.16302545
		 -0.86806899 -0.46891251 -0.16302545 -0.86806899 -0.46891251 0.17770155 -0.9692201
		 0.17039497 0.17770155 -0.9692201 0.17039497 0.17770155 -0.9692201 0.17039497 0.17770155
		 -0.9692201 0.17039497 -0.14698476 -0.97182018 0.1842853 -0.14698476 -0.97182018 0.1842853
		 -0.14698476 -0.97182018 0.1842853 -0.14698476 -0.97182018 0.1842853 0.14462513 -0.86086047
		 -0.48785523 0.14462513 -0.86086047 -0.48785523;
	setAttr ".n[830:959]" -type "float3"  0.14462513 -0.86086047 -0.48785523 0.14462513
		 -0.86086047 -0.48785523 0.89116043 -0.29505658 0.34463716 0.89116043 -0.29505658
		 0.34463716 0.89116043 -0.29505658 0.34463716 0.89116043 -0.29505658 0.34463716 -0.96997654
		 -0.17996673 -0.16357762 -0.96997654 -0.17996673 -0.16357762 -0.96997654 -0.17996673
		 -0.16357762 -0.96997654 -0.17996673 -0.16357762 0.38201872 0.007662944 0.92412281
		 0.38201872 0.007662944 0.92412281 0.38201872 0.007662944 0.92412281 0.38201872 0.007662944
		 0.92412281 -0.33008501 0.063966684 0.94178134 -0.33008501 0.063966684 0.94178134
		 -0.33008501 0.063966684 0.94178134 -0.33008501 0.063966684 0.94178134 -0.76224363
		 -0.46222672 -0.45313486 -0.76224363 -0.46222672 -0.45313486 -0.76224363 -0.46222672
		 -0.45313486 -0.76224363 -0.46222672 -0.45313486 0.022535233 -0.93589687 -0.35155255
		 0.022535233 -0.93589687 -0.35155255 0.022535233 -0.93589687 -0.35155255 0.022535233
		 -0.93589687 -0.35155255 0.045107871 -0.93723297 -0.34577408 0.045107871 -0.93723297
		 -0.34577408 0.045107871 -0.93723297 -0.34577408 0.045107871 -0.93723297 -0.34577408
		 0.93832982 0.23959449 -0.24926244 0.93832982 0.23959449 -0.24926244 0.93832982 0.23959449
		 -0.24926244 0.93832982 0.23959449 -0.24926244 0.88745785 -0.24952778 -0.38749772
		 0.88745785 -0.24952778 -0.38749772 0.88745785 -0.24952778 -0.38749772 0.88745785
		 -0.24952778 -0.38749772 -0.96329165 -0.11100619 -0.24443144 -0.96329165 -0.11100619
		 -0.24443144 -0.96329165 -0.11100619 -0.24443144 -0.96329165 -0.11100619 -0.24443144
		 -0.68241912 -0.68124521 -0.26497 -0.68241912 -0.68124521 -0.26497 -0.68241912 -0.68124521
		 -0.26497 -0.68241912 -0.68124521 -0.26497 0.70670158 -0.52459472 -0.47473499 0.70670158
		 -0.52459472 -0.47473499 0.70670158 -0.52459472 -0.47473499 0.70670158 -0.52459472
		 -0.47473499 -0.96737337 -0.25305843 -0.012255357 -0.96737337 -0.25305843 -0.012255357
		 -0.96737337 -0.25305843 -0.012255357 -0.96737337 -0.25305843 -0.012255357 0.90870607
		 0.12625735 -0.39788499 0.90870607 0.12625735 -0.39788499 0.90870607 0.12625735 -0.39788499
		 0.90870607 0.12625735 -0.39788499 0.89613211 0.036658257 -0.44227076 0.89613211 0.036658257
		 -0.44227076 0.89613211 0.036658257 -0.44227076 0.89613211 0.036658257 -0.44227076
		 -0.99133426 -0.1002572 0.084881939 -0.99133426 -0.1002572 0.084881939 -0.99133426
		 -0.1002572 0.084881939 -0.99133426 -0.1002572 0.084881939 0.27872473 -0.844724 0.45689598
		 0.27872473 -0.844724 0.45689598 0.27872473 -0.844724 0.45689598 0.27872473 -0.844724
		 0.45689598 0.80390674 -0.44477209 -0.39485657 0.80390674 -0.44477209 -0.39485657
		 0.80390674 -0.44477209 -0.39485657 0.80390674 -0.44477209 -0.39485657 0.83800703
		 -0.52932417 -0.13251415 0.83800703 -0.52932417 -0.13251415 0.83800703 -0.52932417
		 -0.13251415 0.83800703 -0.52932417 -0.13251415 0.26678586 -0.16300832 -0.94987035
		 0.26678586 -0.16300832 -0.94987035 0.26678586 -0.16300832 -0.94987035 0.26678586
		 -0.16300832 -0.94987035 -0.17173833 -0.86505461 -0.47136658 -0.17173833 -0.86505461
		 -0.47136658 -0.17173833 -0.86505461 -0.47136658 -0.17173833 -0.86505461 -0.47136658
		 -0.037209131 -0.94272971 -0.33147579 -0.037209131 -0.94272971 -0.33147579 -0.037209131
		 -0.94272971 -0.33147579 -0.037209131 -0.94272971 -0.33147579 -0.015378329 -0.93720549
		 -0.34843862 -0.015378329 -0.93720549 -0.34843862 -0.015378329 -0.93720549 -0.34843862
		 -0.015378329 -0.93720549 -0.34843862 0.29610011 -0.84165877 -0.45159197 0.29610011
		 -0.84165877 -0.45159197 0.29610011 -0.84165877 -0.45159197 0.29610011 -0.84165877
		 -0.45159197 0.022995051 -0.86488312 -0.50144613 0.022995051 -0.86488312 -0.50144613
		 0.022995051 -0.86488312 -0.50144613 0.022995051 -0.86488312 -0.50144613 -0.017380159
		 -0.86888683 -0.49470565 -0.017380159 -0.86888683 -0.49470565 -0.017380159 -0.86888683
		 -0.49470565 -0.017380159 -0.86888683 -0.49470565 -0.072427556 -0.87049073 -0.48682639
		 -0.072427556 -0.87049073 -0.48682639 -0.072427556 -0.87049073 -0.48682639 -0.072427556
		 -0.87049073 -0.48682639 0.10642338 -0.85174531 -0.51303399 0.10642338 -0.85174531
		 -0.51303399 0.10642338 -0.85174531 -0.51303399 0.10642338 -0.85174531 -0.51303399
		 -0.30118757 -0.80994576 0.50326329 -0.30118757 -0.80994576 0.50326329 -0.30118757
		 -0.80994576 0.50326329 -0.30118757 -0.80994576 0.50326329 -0.8966496 -0.43642655
		 0.074507259 -0.8966496 -0.43642655 0.074507259 -0.8966496 -0.43642655 0.074507259
		 -0.8966496 -0.43642655 0.074507259 -0.90751225 -0.41852716 -0.035448655 -0.90751225
		 -0.41852716 -0.035448655 -0.90751225 -0.41852716 -0.035448655 -0.90751225 -0.41852716
		 -0.035448655 -0.42134041 -0.21622804 -0.88074833 -0.42134041 -0.21622804 -0.88074833
		 -0.42134041 -0.21622804 -0.88074833 -0.42134041 -0.21622804 -0.88074833;
	setAttr -s 240 -ch 960 ".fc[0:239]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 48 46 92 82
		f 4 4 5 6 7
		mu 0 4 194 188 189 195
		f 4 8 9 10 11
		mu 0 4 110 112 152 150
		f 4 12 13 14 15
		mu 0 4 40 42 62 50
		f 4 16 17 -13 18
		mu 0 4 24 26 42 40
		f 4 -11 19 20 21
		mu 0 4 150 152 218 226
		f 4 22 23 24 25
		mu 0 4 172 182 222 200
		f 4 26 27 28 29
		mu 0 4 140 142 208 206
		f 4 30 31 -1 32
		mu 0 4 12 14 46 48
		f 4 -5 -2 33 34
		mu 0 4 188 194 202 224
		f 4 35 36 37 38
		mu 0 4 144 146 212 210
		f 4 39 40 41 42
		mu 0 4 16 18 34 32
		f 4 -42 43 44 45
		mu 0 4 32 34 56 54
		f 4 46 47 -36 48
		mu 0 4 102 104 146 144
		f 4 49 50 51 52
		mu 0 4 240 242 260 259
		f 4 53 54 55 56
		mu 0 4 254 248 269 268
		f 4 57 58 59 60
		mu 0 4 236 238 258 257
		f 4 61 62 63 64
		mu 0 4 250 252 267 266
		f 4 65 66 67 68
		mu 0 4 230 232 264 263
		f 4 -21 69 -66 70
		mu 0 4 226 218 232 230
		f 4 -25 71 -62 72
		mu 0 4 200 222 252 250
		f 4 -29 73 -58 74
		mu 0 4 206 208 238 236
		f 4 -34 -32 -54 75
		mu 0 4 224 202 248 254
		f 4 -38 76 -50 77
		mu 0 4 210 212 242 240
		f 4 78 79 -40 80
		mu 0 4 1 2 18 16
		f 4 81 -55 -31 82
		mu 0 4 8 9 14 12
		f 4 83 84 -17 85
		mu 0 4 5 6 26 24
		f 4 86 -83 87 88
		mu 0 4 7 8 12 28
		f 4 89 -73 90 91
		mu 0 4 220 200 250 246
		f 4 -91 -65 92 93
		mu 0 4 246 250 266 265
		f 4 -88 -33 94 95
		mu 0 4 28 12 48 44
		f 4 96 -26 -90 97
		mu 0 4 162 172 200 220
		f 4 98 99 100 -6
		mu 0 4 188 182 183 189
		f 4 -95 -4 101 102
		mu 0 4 44 48 82 72
		f 4 103 -86 104 105
		mu 0 4 4 5 24 22
		f 4 106 -71 107 108
		mu 0 4 216 226 230 228
		f 4 -108 -69 109 110
		mu 0 4 228 230 263 262
		f 4 111 -22 -107 112
		mu 0 4 148 150 226 216
		f 4 -105 -19 113 114
		mu 0 4 22 24 40 38
		f 4 -114 -16 115 116
		mu 0 4 38 40 50 60
		f 4 117 -12 -112 118
		mu 0 4 108 110 150 148
		f 4 119 -119 120 121
		mu 0 4 106 108 148 136
		f 4 122 -117 123 124
		mu 0 4 36 38 60 58
		f 4 125 -115 -123 126
		mu 0 4 20 22 38 36
		f 4 -121 -113 127 128
		mu 0 4 136 148 216 214
		f 4 129 -111 130 131
		mu 0 4 244 228 262 261
		f 4 -128 -109 -130 132
		mu 0 4 214 216 228 244
		f 4 133 -106 -126 134
		mu 0 4 3 4 22 20
		f 4 -124 135 -120 136
		mu 0 4 58 60 108 106
		f 4 -116 137 -118 -136
		mu 0 4 60 50 110 108
		f 4 138 139 140 141
		mu 0 4 112 62 63 113
		f 4 142 -76 143 -72
		mu 0 4 222 224 254 252
		f 4 144 -75 145 146
		mu 0 4 204 206 236 234
		f 4 -146 -61 147 148
		mu 0 4 234 236 257 256
		f 4 -144 -57 149 -63
		mu 0 4 252 254 268 267
		f 4 -45 150 -47 151
		mu 0 4 54 56 104 102
		f 4 -99 -35 -143 -24
		mu 0 4 182 188 224 222
		f 4 152 -30 -145 153
		mu 0 4 138 140 206 204
		f 4 -15 -139 -9 -138
		mu 0 4 50 62 112 110
		f 4 -102 154 155 156
		mu 0 4 72 82 83 73
		f 4 157 -137 158 -151
		mu 0 4 56 58 106 104
		f 4 159 -135 160 -80
		mu 0 4 2 3 20 18
		f 4 161 -133 162 -77
		mu 0 4 212 214 244 242
		f 4 -163 -132 163 -51
		mu 0 4 242 244 261 260
		f 4 164 -129 -162 -37
		mu 0 4 146 136 214 212
		f 4 -161 -127 165 -41
		mu 0 4 18 20 36 34
		f 4 -166 -125 -158 -44
		mu 0 4 34 36 58 56
		f 4 -159 -122 -165 -48
		mu 0 4 104 106 136 146
		f 4 166 -152 167 -153
		mu 0 4 52 54 102 100
		f 4 168 -81 169 -149
		mu 0 4 0 1 16 10
		f 4 170 -78 171 -74
		mu 0 4 208 210 240 238
		f 4 -172 -53 172 -59
		mu 0 4 238 240 259 258
		f 4 -168 -49 173 -27
		mu 0 4 100 102 144 142
		f 4 174 -46 -167 -154
		mu 0 4 30 32 54 52
		f 4 -170 -43 -175 -147
		mu 0 4 10 16 32 30
		f 4 -174 -39 -171 -28
		mu 0 4 142 144 210 208
		f 4 -141 175 176 177
		mu 0 4 113 63 64 114
		f 4 178 179 180 181
		mu 0 4 163 153 154 164
		f 4 182 -178 183 -180
		mu 0 4 153 113 114 154
		f 4 184 185 186 187
		mu 0 4 83 93 94 84
		f 4 -23 188 189 -100
		mu 0 4 182 172 173 183
		f 4 190 191 -179 192
		mu 0 4 162 152 153 163
		f 4 -10 -142 -183 -192
		mu 0 4 152 112 113 153
		f 4 -3 -8 -185 -155
		mu 0 4 82 92 93 83
		f 4 -181 193 194 195
		mu 0 4 164 154 155 166
		f 4 196 197 198 199
		mu 0 4 190 184 174 130
		f 4 200 -200 201 -187
		mu 0 4 196 190 130 124
		f 4 -184 202 203 -194
		mu 0 4 154 114 115 155
		f 4 -190 204 -198 205
		mu 0 4 183 173 174 184
		f 4 -156 -188 206 207
		mu 0 4 73 83 84 74
		f 4 -101 -206 -197 208
		mu 0 4 189 183 184 190
		f 4 -7 -209 -201 -186
		mu 0 4 195 189 190 196
		f 4 209 -208 210 -176
		mu 0 4 63 73 74 64
		f 4 -207 211 212 213
		mu 0 4 74 84 85 75
		f 4 -199 214 215 216
		mu 0 4 130 174 175 131
		f 4 -97 -193 217 -189
		mu 0 4 172 162 163 173
		f 4 -218 -182 218 -205
		mu 0 4 173 163 164 174
		f 4 219 -157 -210 -140
		mu 0 4 62 72 73 63
		f 4 220 -103 -220 -14
		mu 0 4 42 44 72 62
		f 4 -191 -98 221 -20
		mu 0 4 152 162 220 218
		f 4 222 -96 -221 -18
		mu 0 4 26 28 44 42
		f 4 223 -94 224 -67
		mu 0 4 232 246 265 264
		f 4 -222 -92 -224 -70
		mu 0 4 218 220 246 232
		f 4 225 -89 -223 -85
		mu 0 4 6 7 28 26
		f 4 226 227 228 229
		mu 0 4 115 65 66 116
		f 4 -213 230 231 232
		mu 0 4 75 85 86 76
		f 4 233 -233 234 -228
		mu 0 4 65 75 76 66
		f 4 235 236 237 -231
		mu 0 4 125 131 132 126
		f 4 -177 238 -227 -203
		mu 0 4 114 64 65 115
		f 4 -202 -217 -236 -212
		mu 0 4 124 130 131 125
		f 4 -219 -196 239 -215
		mu 0 4 174 164 166 175
		f 4 -211 -214 -234 -239
		mu 0 4 64 74 75 65
		f 4 240 241 242 243
		mu 0 4 122 165 156 116
		f 4 244 -244 -229 -235
		mu 0 4 98 122 116 66
		f 4 -238 245 -245 -232
		mu 0 4 126 132 122 98
		f 4 246 247 -241 -246
		mu 0 4 132 176 165 122
		f 4 -204 -230 -243 248
		mu 0 4 155 115 116 156
		f 4 -195 -249 -242 249
		mu 0 4 166 155 156 165
		f 4 -240 -250 -248 250
		mu 0 4 175 166 165 176
		f 4 -216 -251 -247 -237
		mu 0 4 131 175 176 132
		f 4 251 252 253 254
		mu 0 4 49 87 95 47
		f 4 255 256 257 258
		mu 0 4 197 198 192 191
		f 4 259 260 261 262
		mu 0 4 111 151 157 117
		f 4 263 264 265 266
		mu 0 4 41 51 67 43
		f 4 267 -267 268 269
		mu 0 4 25 41 43 27
		f 4 270 271 272 -261
		mu 0 4 151 227 219 157
		f 4 273 274 275 276
		mu 0 4 177 201 223 185
		f 4 277 278 279 280
		mu 0 4 141 207 209 143
		f 4 281 -255 282 283
		mu 0 4 13 49 47 15
		f 4 284 285 -254 -259
		mu 0 4 191 225 203 197
		f 4 286 287 288 289
		mu 0 4 145 211 213 147
		f 4 290 291 292 293
		mu 0 4 17 33 35 19
		f 4 294 295 296 -292
		mu 0 4 33 55 57 35
		f 4 297 -290 298 299
		mu 0 4 103 145 147 105
		f 4 300 -52 301 302
		mu 0 4 241 259 260 243
		f 4 303 -56 304 305
		mu 0 4 255 268 269 249
		f 4 306 -60 307 308
		mu 0 4 237 257 258 239
		f 4 309 -64 310 311
		mu 0 4 251 266 267 253
		f 4 312 -68 313 314
		mu 0 4 231 263 264 233
		f 4 315 -315 316 -272
		mu 0 4 227 231 233 219
		f 4 317 -312 318 -275
		mu 0 4 201 251 253 223
		f 4 319 -309 320 -279
		mu 0 4 207 237 239 209
		f 4 321 -306 -283 -286
		mu 0 4 225 255 249 203
		f 4 322 -303 323 -288
		mu 0 4 211 241 243 213
		f 4 324 -294 325 -79
		mu 0 4 1 17 19 2
		f 4 326 -284 -305 -82
		mu 0 4 8 13 15 9
		f 4 327 -270 328 -84
		mu 0 4 5 25 27 6
		f 4 329 330 -327 -87
		mu 0 4 7 29 13 8
		f 4 331 332 -318 333
		mu 0 4 221 247 251 201
		f 4 334 -93 -310 -333
		mu 0 4 247 265 266 251
		f 4 335 336 -282 -331
		mu 0 4 29 45 49 13
		f 4 337 -334 -274 338
		mu 0 4 167 221 201 177
		f 4 -258 339 340 341
		mu 0 4 191 192 186 185
		f 4 342 343 -252 -337
		mu 0 4 45 77 87 49
		f 4 344 345 -328 -104
		mu 0 4 4 23 25 5
		f 4 346 347 -316 348
		mu 0 4 217 229 231 227
		f 4 349 -110 -313 -348
		mu 0 4 229 262 263 231
		f 4 350 -349 -271 351
		mu 0 4 149 217 227 151
		f 4 352 353 -268 -346
		mu 0 4 23 39 41 25
		f 4 354 355 -264 -354
		mu 0 4 39 61 51 41
		f 4 356 -352 -260 357
		mu 0 4 109 149 151 111
		f 4 358 359 -357 360
		mu 0 4 107 137 149 109
		f 4 361 362 -355 363
		mu 0 4 37 59 61 39
		f 4 364 -364 -353 365
		mu 0 4 21 37 39 23
		f 4 366 367 -351 -360
		mu 0 4 137 215 217 149
		f 4 368 -131 -350 369
		mu 0 4 245 261 262 229
		f 4 370 -370 -347 -368
		mu 0 4 215 245 229 217
		f 4 371 -366 -345 -134
		mu 0 4 3 21 23 4
		f 4 372 -361 373 -363
		mu 0 4 59 107 109 61
		f 4 -374 -358 374 -356
		mu 0 4 61 109 111 51
		f 4 375 376 377 378
		mu 0 4 117 118 68 67
		f 4 -319 379 -322 380
		mu 0 4 223 253 255 225
		f 4 381 382 -320 383
		mu 0 4 205 235 237 207
		f 4 384 -148 -307 -383
		mu 0 4 235 256 257 237
		f 4 -311 -150 -304 -380
		mu 0 4 253 267 268 255
		f 4 385 -300 386 -296
		mu 0 4 55 103 105 57
		f 4 -276 -381 -285 -342
		mu 0 4 185 223 225 191
		f 4 387 -384 -278 388
		mu 0 4 139 205 207 141
		f 4 -375 -263 -379 -265
		mu 0 4 51 111 117 67
		f 4 389 390 391 -344
		mu 0 4 77 78 88 87
		f 4 -387 392 -373 393
		mu 0 4 57 105 107 59
		f 4 -326 394 -372 -160
		mu 0 4 2 19 21 3
		f 4 -324 395 -371 396
		mu 0 4 213 243 245 215
		f 4 -302 -164 -369 -396
		mu 0 4 243 260 261 245
		f 4 -289 -397 -367 397
		mu 0 4 147 213 215 137
		f 4 -293 398 -365 -395
		mu 0 4 19 35 37 21
		f 4 -297 -394 -362 -399
		mu 0 4 35 57 59 37
		f 4 -299 -398 -359 -393
		mu 0 4 105 147 137 107
		f 4 -389 399 -386 400
		mu 0 4 53 101 103 55
		f 4 -385 401 -325 -169
		mu 0 4 0 11 17 1
		f 4 -321 402 -323 403
		mu 0 4 209 239 241 211
		f 4 -308 -173 -301 -403
		mu 0 4 239 258 259 241
		f 4 -281 404 -298 -400
		mu 0 4 101 143 145 103
		f 4 -388 -401 -295 405
		mu 0 4 31 53 55 33
		f 4 -382 -406 -291 -402
		mu 0 4 11 31 33 17
		f 4 -280 -404 -287 -405
		mu 0 4 143 209 211 145
		f 4 406 407 408 -377
		mu 0 4 118 119 69 68
		f 4 409 410 411 412
		mu 0 4 168 169 159 158
		f 4 -412 413 -407 414
		mu 0 4 158 159 119 118
		f 4 415 416 417 418
		mu 0 4 88 89 97 96
		f 4 -341 419 420 -277
		mu 0 4 185 186 178 177
		f 4 421 -413 422 423
		mu 0 4 167 168 158 157
		f 4 -423 -415 -376 -262
		mu 0 4 157 158 118 117
		f 4 -392 -419 -256 -253
		mu 0 4 87 88 96 95
		f 4 424 425 426 -411
		mu 0 4 169 171 160 159
		f 4 427 428 429 430
		mu 0 4 193 133 179 187
		f 4 -417 431 -428 432
		mu 0 4 199 127 133 193
		f 4 -427 433 434 -414
		mu 0 4 159 160 120 119
		f 4 435 -430 436 -420
		mu 0 4 186 187 179 178
		f 4 437 438 -416 -391
		mu 0 4 78 79 89 88
		f 4 439 -431 -436 -340
		mu 0 4 192 193 187 186
		f 4 -418 -433 -440 -257
		mu 0 4 198 199 193 192
		f 4 -409 440 -438 441
		mu 0 4 68 69 79 78
		f 4 442 443 444 -439
		mu 0 4 79 80 90 89
		f 4 445 446 447 -429
		mu 0 4 133 134 180 179
		f 4 -421 448 -422 -339
		mu 0 4 177 178 168 167
		f 4 -437 449 -410 -449
		mu 0 4 178 179 169 168
		f 4 -378 -442 -390 450
		mu 0 4 67 68 78 77
		f 4 -266 -451 -343 451
		mu 0 4 43 67 77 45
		f 4 -273 452 -338 -424
		mu 0 4 157 219 221 167
		f 4 -269 -452 -336 453
		mu 0 4 27 43 45 29
		f 4 -314 -225 -335 454
		mu 0 4 233 264 265 247
		f 4 -317 -455 -332 -453
		mu 0 4 219 233 247 221
		f 4 -329 -454 -330 -226
		mu 0 4 6 27 29 7
		f 4 455 456 457 458
		mu 0 4 120 121 71 70
		f 4 459 460 461 -444
		mu 0 4 80 81 91 90
		f 4 -458 462 -460 463
		mu 0 4 70 71 81 80
		f 4 -462 464 465 466
		mu 0 4 128 129 135 134
		f 4 -435 -459 467 -408
		mu 0 4 119 120 70 69
		f 4 -445 -467 -446 -432
		mu 0 4 127 128 134 133
		f 4 -448 468 -425 -450
		mu 0 4 179 180 171 169
		f 4 -468 -464 -443 -441
		mu 0 4 69 70 80 79
		f 4 469 470 471 472
		mu 0 4 123 121 161 170
		f 4 -463 -457 -470 473
		mu 0 4 99 71 121 123
		f 4 -461 -474 474 -465
		mu 0 4 129 99 123 135
		f 4 -475 -473 475 476
		mu 0 4 135 123 170 181
		f 4 477 -471 -456 -434
		mu 0 4 160 161 121 120
		f 4 478 -472 -478 -426
		mu 0 4 171 170 161 160
		f 4 479 -476 -479 -469
		mu 0 4 180 181 170 171
		f 4 -466 -477 -480 -447
		mu 0 4 134 135 181 180;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "crown" -p "mesh";
	rename -uid "F09C650C-48EF-A5D0-7161-C9B499CBD2BF";
	setAttr ".t" -type "double3" 3.6557316780090332 909.06689453125 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" -90.00000933466734 0 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 100 100 100 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "crownShape" -p "crown";
	rename -uid "C0440DED-4A01-8DA1-F601-68B5E6ACF399";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "crownShapeOrig" -p "crown";
	rename -uid "0C2F175C-460B-3F4E-C81D-C2A904C557BF";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 80 ".uvst[0].uvsp[0:79]" -type "float2" 0.375 0.12499999
		 0.4375 0.12499999 0.49999997 0.12499999 0.5625 0.125 0.625 0.125 0.375 0.1875 0.375
		 0.1875 0.4375 0.1875 0.4375 0.1875 0.49999997 0.1875 0.49999997 0.1875 0.5625 0.1875
		 0.5625 0.1875 0.625 0.1875 0.625 0.1875 0.375 0.25 0.375 0.25 0.4375 0.25 0.4375
		 0.25 0.49999997 0.25 0.49999997 0.25 0.5625 0.25 0.5625 0.25 0.625 0.25 0.625 0.25
		 0.375 0.375 0.375 0.375 0.4375 0.375 0.4375 0.375 0.49999997 0.375 0.49999997 0.375
		 0.5625 0.375 0.5625 0.375 0.625 0.375 0.625 0.375 0.125 0.5 0.125 0.5 0.25 0.5 0.25
		 0.5 0.375 0.5 0.375 0.5 0.4375 0.5 0.4375 0.5 0.49999997 0.5 0.49999997 0.5 0.5625
		 0.5 0.5625 0.5 0.625 0.5 0.625 0.5 0.75 0.5 0.75 0.5 0.875 0.5 0.875 0.5 0.125 0.5625
		 0.125 0.5625 0.25 0.5625 0.25 0.5625 0.375 0.5625 0.375 0.5625 0.4375 0.5625 0.4375
		 0.5625 0.49999997 0.5625 0.49999997 0.5625 0.5625 0.5625 0.5625 0.5625 0.625 0.5625
		 0.625 0.5625 0.75 0.5625 0.75 0.5625 0.875 0.5625 0.875 0.5625 0.125 0.625 0.25 0.625
		 0.375 0.625 0.4375 0.625 0.49999997 0.625 0.5625 0.625 0.625 0.625 0.75 0.625 0.875
		 0.625;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 66 ".vt[0:65]"  0.47902378 0.24519484 -0.55558062 0.41448453 0.17646976 0.023225307
		 -0.67465603 0.64953303 -1.91261435 -0.32795513 0.45623997 0.55474806 0.57490456 0.7407434 -1.47281313
		 0.34807494 0.46280715 -0.55558062 0.59184778 0.64953303 -1.91261435 0.34807494 0.45623997 0.55474806
		 -0.78654927 0.30003026 -1.88525105 -0.81855375 0 -1.88525105 0.28503034 0.39408207 0.023225307
		 -0.60745239 0.7407434 -1.47281313 -0.49584705 0 0.55474806 -0.48011261 0.19758272 0.55474806
		 -0.32795513 0.46280715 -0.55558062 0.75455779 0 -1.88525105 0.72772866 0.30003026 -1.88525105
		 0.47902378 0.19758272 0.55474806 0.49774858 0 0.55474806 -0.27279109 0.39408207 0.023225307
		 0.71038377 0.39124066 -1.47281313 0.4307535 0 0.023225307 0.49774858 0 -0.55558062
		 0.73521566 0 -1.47281313 -0.43722636 0 0.023225307 -0.49584705 0 -0.55558062 -0.77734828 0 -1.47281313
		 -0.42364079 0.17646976 0.023225307 -0.48011261 0.24519484 -0.55558062 -0.74902755 0.39124066 -1.47281313
		 -0.015135381 0.73914587 -1.91808701 -0.0075622052 0.53941089 0.55474806 -0.01816655 0.18719707 0.55474806
		 -0.016671367 0 0.55474806 -0.028224565 0.29632679 -2.30360293 -0.030812241 0 -2.30360293
		 -0.0091594514 0.79595238 -1.47281313 -0.018062271 0.52020895 -0.55558062 -0.011502489 0.44828621 0.023225307
		 0.47902378 -0.24519484 -0.55558062 0.41448453 -0.17646976 0.023225307 -0.67465603 -0.64953303 -1.91261435
		 -0.32795513 -0.45623997 0.55474806 0.57490456 -0.7407434 -1.47281313 0.34807494 -0.46280715 -0.55558062
		 0.59184778 -0.64953303 -1.91261435 0.34807494 -0.45623997 0.55474806 -0.78654927 -0.30003026 -1.88525105
		 0.28503034 -0.39408207 0.023225307 -0.60745239 -0.7407434 -1.47281313 -0.48011261 -0.19758272 0.55474806
		 -0.32795513 -0.46280715 -0.55558062 0.72772866 -0.30003026 -1.88525105 0.47902378 -0.19758272 0.55474806
		 -0.27279109 -0.39408207 0.023225307 0.71038377 -0.39124066 -1.47281313 -0.42364079 -0.17646976 0.023225307
		 -0.48011261 -0.24519484 -0.55558062 -0.74902755 -0.39124066 -1.47281313 -0.015135381 -0.73914587 -1.91808701
		 -0.0075622052 -0.53941089 0.55474806 -0.01816655 -0.18719707 0.55474806 -0.028224565 -0.29632679 -2.30360293
		 -0.0091594514 -0.79595238 -1.47281313 -0.018062271 -0.52020895 -0.55558062 -0.011502489 -0.44828621 0.023225307;
	setAttr -s 128 ".ed[0:127]"  27 13 0 13 3 0 3 19 0 19 27 0 38 31 0 31 7 0
		 7 10 0 10 38 0 20 0 0 0 22 0 22 23 0 23 20 0 6 4 0 4 20 0 20 16 0 16 6 0 4 5 0 5 0 0
		 0 1 0 1 21 0 21 22 0 31 3 0 13 32 0 32 31 0 13 12 0 12 33 0 33 32 0 26 25 0 25 28 0
		 28 29 0 29 26 0 30 6 0 16 34 0 34 30 0 16 15 0 15 35 0 35 34 0 25 24 0 24 27 0 27 28 0
		 7 17 0 17 1 0 1 10 0 17 18 0 18 21 0 9 26 0 29 8 0 8 9 0 5 10 0 23 15 0 24 12 0 30 36 0
		 36 4 0 36 37 0 37 5 0 37 38 0 29 11 0 11 2 0 2 8 0 28 14 0 14 11 0 19 14 0 19 38 0
		 37 14 0 36 11 0 30 2 0 8 34 0 35 9 0 17 32 0 33 18 0 56 54 0 54 42 0 42 50 0 50 56 0
		 65 48 0 48 46 0 46 60 0 60 65 0 55 23 0 22 39 0 39 55 0 45 52 0 52 55 0 55 43 0 43 45 0
		 39 44 0 44 43 0 21 40 0 40 39 0 60 61 0 61 50 0 42 60 0 61 33 0 12 50 0 26 58 0 58 57 0
		 57 25 0 59 62 0 62 52 0 45 59 0 62 35 0 15 52 0 57 56 0 56 24 0 48 40 0 40 53 0 53 46 0
		 18 53 0 9 47 0 47 58 0 48 44 0 43 63 0 63 59 0 44 64 0 64 63 0 65 64 0 47 41 0 41 49 0
		 49 58 0 49 51 0 51 57 0 51 54 0 51 64 0 65 54 0 49 63 0 41 59 0 62 47 0 61 53 0;
	setAttr -s 256 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.83661032 0.5322594 -0.12954988 -0.83661032
		 0.5322594 -0.12954988 -0.83661032 0.5322594 -0.12954988 -0.83661032 0.5322594 -0.12954988
		 0.20371573 0.96711355 -0.15228662 0.20371573 0.96711355 -0.15228662 0.20371573 0.96711355
		 -0.15228662 0.20371573 0.96711355 -0.15228662 0.96546853 0.066075131 0.25200137 0.96546853
		 0.066075131 0.25200137 0.96546853 0.066075131 0.25200137 0.96546853 0.066075131 0.25200137
		 0.93200892 0.3602635 -0.039616395 0.93200892 0.3602635 -0.039616395 0.93200892 0.3602635
		 -0.039616395 0.93200892 0.3602635 -0.039616395 0.8609435 0.40446758 0.30851626 0.8609435
		 0.40446758 0.30851626 0.8609435 0.40446758 0.30851626 0.8609435 0.40446758 0.30851626
		 0.98969072 0.08213383 0.11733034 0.98969072 0.08213383 0.11733034 0.98969072 0.08213383
		 0.11733034 0.98969072 0.08213383 0.11733034 2.3979389e-08 0 1 2.3979389e-08 0 1 2.3979389e-08
		 0 1 2.3979389e-08 0 1 0 0 1 0 0 1 0 0 1 0 0 1 -0.95426357 0.066055603 0.29157779
		 -0.95426357 0.066055603 0.29157779 -0.95426357 0.066055603 0.29157779 -0.95426357
		 0.066055603 0.29157779 0.29259065 0.43115962 -0.85351747 0.29259065 0.43115962 -0.85351747
		 0.29259065 0.43115962 -0.85351747 0.29259065 0.43115962 -0.85351747 0.47695985 0.019388054
		 -0.87871116 0.47695985 0.019388054 -0.87871116 0.47695985 0.019388054 -0.87871116
		 0.47695985 0.019388054 -0.87871116 -0.9923107 0.068999216 0.10275424 -0.9923107 0.068999216
		 0.10275424 -0.9923107 0.068999216 0.10275424 -0.9923107 0.068999216 0.10275424 0.86859351
		 0.47490838 -0.14144693 0.86859351 0.47490838 -0.14144693 0.86859351 0.47490838 -0.14144693
		 0.86859351 0.47490838 -0.14144693 0.98795837 0.092426576 -0.12407921 0.98795837 0.092426576
		 -0.12407921 0.98795837 0.092426576 -0.12407921 0.98795837 0.092426576 -0.12407921
		 -0.9925952 0.086620741 0.085156478 -0.9925952 0.086620741 0.085156478 -0.9925952
		 0.086620741 0.085156478 -0.9925952 0.086620741 0.085156478 0.84793323 0.50733435
		 0.15369208 0.84793323 0.50733435 0.15369208 0.84793323 0.50733435 0.15369208 0.84793323
		 0.50733435 0.15369208 0.99656951 0.07447692 0.036087967 0.99656951 0.07447692 0.036087967
		 0.99656951 0.07447692 0.036087967 0.99656951 0.07447692 0.036087967 -0.99101973 0.077680871
		 -0.10883733 -0.99101973 0.077680871 -0.10883733 -0.99101973 0.077680871 -0.10883733
		 -0.99101973 0.077680871 -0.10883733 0.11983742 0.97943544 -0.16231224 0.11983742
		 0.97943544 -0.16231224 0.11983742 0.97943544 -0.16231224 0.11983742 0.97943544 -0.16231224
		 0.11225653 0.94721186 0.30031338 0.11225653 0.94721186 0.30031338 0.11225653 0.94721186
		 0.30031338 0.11225653 0.94721186 0.30031338 0.16473737 0.97814256 0.12688093 0.16473737
		 0.97814256 0.12688093 0.16473737 0.97814256 0.12688093 0.16473737 0.97814256 0.12688093
		 -0.93873727 0.34204775 0.04213956 -0.93873727 0.34204775 0.04213956 -0.93873727 0.34204775
		 0.04213956 -0.93873727 0.34204775 0.04213956 -0.83231187 0.43108916 0.34845257 -0.83231187
		 0.43108916 0.34845257 -0.83231187 0.43108916 0.34845257 -0.83231187 0.43108916 0.34845257
		 -0.81196654 0.56529814 0.14542432 -0.81196654 0.56529814 0.14542432 -0.81196654 0.56529814
		 0.14542432 -0.81196654 0.56529814 0.14542432 -0.19018112 0.97331673 0.12839687 -0.19018112
		 0.97331673 0.12839687 -0.19018112 0.97331673 0.12839687 -0.19018112 0.97331673 0.12839687
		 -0.11727738 0.94582123 0.30276749 -0.11727738 0.94582123 0.30276749 -0.11727738 0.94582123
		 0.30276749 -0.11727738 0.94582123 0.30276749 -0.113669 0.98139644 -0.15472697 -0.113669
		 0.98139644 -0.15472697 -0.113669 0.98139644 -0.15472697 -0.113669 0.98139644 -0.15472697
		 -0.47572154 0.027594564 -0.87916297 -0.47572154 0.027594564 -0.87916297 -0.47572154
		 0.027594564 -0.87916297 -0.47572154 0.027594564 -0.87916297 -0.282244 0.43176946
		 -0.85668743 -0.282244 0.43176946 -0.85668743 -0.282244 0.43176946 -0.85668743 -0.282244
		 0.43176946 -0.85668743 0 0 1 0 0 1 0 0 1 0 0 1 -2.1818995e-08 0 1 -2.1818995e-08
		 0 1 -2.1818995e-08 0 1 -2.1818995e-08 0 1 -0.22725585 0.96226013 -0.1497007 -0.22725585
		 0.96226013 -0.1497007 -0.22725585 0.96226013 -0.1497007 -0.22725585 0.96226013 -0.1497007
		 -0.83661032 -0.5322594 -0.12954988 -0.83661032 -0.5322594 -0.12954988 -0.83661032
		 -0.5322594 -0.12954988 -0.83661032 -0.5322594 -0.12954988 0.20371571 -0.96711349
		 -0.15228662 0.20371571 -0.96711349 -0.15228662 0.20371571 -0.96711349 -0.15228662
		 0.20371571 -0.96711349 -0.15228662 0.96546853 -0.066075131 0.25200137 0.96546853
		 -0.066075131 0.25200137 0.96546853 -0.066075131 0.25200137 0.96546853 -0.066075131
		 0.25200137 0.93200892 -0.36026353 -0.039616417 0.93200892 -0.36026353 -0.039616417
		 0.93200892 -0.36026353 -0.039616417 0.93200892 -0.36026353 -0.039616417 0.8609435
		 -0.40446758 0.30851626 0.8609435 -0.40446758 0.30851626 0.8609435 -0.40446758 0.30851626
		 0.8609435 -0.40446758 0.30851626 0.98969072 -0.08213383 0.11733034 0.98969072 -0.08213383
		 0.11733034 0.98969072 -0.08213383 0.11733034 0.98969072 -0.08213383 0.11733034 0
		 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 -0.95426357 -0.066055603 0.29157779
		 -0.95426357 -0.066055603 0.29157779 -0.95426357 -0.066055603 0.29157779 -0.95426357
		 -0.066055603 0.29157779 0.29259068 -0.43115962 -0.85351747 0.29259068 -0.43115962
		 -0.85351747;
	setAttr ".n[166:255]" -type "float3"  0.29259068 -0.43115962 -0.85351747 0.29259068
		 -0.43115962 -0.85351747 0.47695985 -0.019388054 -0.87871116 0.47695985 -0.019388054
		 -0.87871116 0.47695985 -0.019388054 -0.87871116 0.47695985 -0.019388054 -0.87871116
		 -0.9923107 -0.068999216 0.10275424 -0.9923107 -0.068999216 0.10275424 -0.9923107
		 -0.068999216 0.10275424 -0.9923107 -0.068999216 0.10275424 0.86859351 -0.47490841
		 -0.14144695 0.86859351 -0.47490841 -0.14144695 0.86859351 -0.47490841 -0.14144695
		 0.86859351 -0.47490841 -0.14144695 0.98795837 -0.092426576 -0.12407921 0.98795837
		 -0.092426576 -0.12407921 0.98795837 -0.092426576 -0.12407921 0.98795837 -0.092426576
		 -0.12407921 -0.9925952 -0.086620726 0.085156463 -0.9925952 -0.086620726 0.085156463
		 -0.9925952 -0.086620726 0.085156463 -0.9925952 -0.086620726 0.085156463 0.84793323
		 -0.50733435 0.15369208 0.84793323 -0.50733435 0.15369208 0.84793323 -0.50733435 0.15369208
		 0.84793323 -0.50733435 0.15369208 0.99656951 -0.07447692 0.036087967 0.99656951 -0.07447692
		 0.036087967 0.99656951 -0.07447692 0.036087967 0.99656951 -0.07447692 0.036087967
		 -0.99101973 -0.077680871 -0.10883732 -0.99101973 -0.077680871 -0.10883732 -0.99101973
		 -0.077680871 -0.10883732 -0.99101973 -0.077680871 -0.10883732 0.11983739 -0.97943544
		 -0.16231224 0.11983739 -0.97943544 -0.16231224 0.11983739 -0.97943544 -0.16231224
		 0.11983739 -0.97943544 -0.16231224 0.11225653 -0.94721186 0.30031335 0.11225653 -0.94721186
		 0.30031335 0.11225653 -0.94721186 0.30031335 0.11225653 -0.94721186 0.30031335 0.16473737
		 -0.97814256 0.12688091 0.16473737 -0.97814256 0.12688091 0.16473737 -0.97814256 0.12688091
		 0.16473737 -0.97814256 0.12688091 -0.93873721 -0.34204778 0.042139564 -0.93873721
		 -0.34204778 0.042139564 -0.93873721 -0.34204778 0.042139564 -0.93873721 -0.34204778
		 0.042139564 -0.83231187 -0.43108919 0.3484526 -0.83231187 -0.43108919 0.3484526 -0.83231187
		 -0.43108919 0.3484526 -0.83231187 -0.43108919 0.3484526 -0.81196654 -0.56529814 0.14542435
		 -0.81196654 -0.56529814 0.14542435 -0.81196654 -0.56529814 0.14542435 -0.81196654
		 -0.56529814 0.14542435 -0.19018112 -0.97331673 0.12839684 -0.19018112 -0.97331673
		 0.12839684 -0.19018112 -0.97331673 0.12839684 -0.19018112 -0.97331673 0.12839684
		 -0.11727741 -0.94582123 0.30276749 -0.11727741 -0.94582123 0.30276749 -0.11727741
		 -0.94582123 0.30276749 -0.11727741 -0.94582123 0.30276749 -0.113669 -0.98139644 -0.15472698
		 -0.113669 -0.98139644 -0.15472698 -0.113669 -0.98139644 -0.15472698 -0.113669 -0.98139644
		 -0.15472698 -0.47572154 -0.027594564 -0.87916297 -0.47572154 -0.027594564 -0.87916297
		 -0.47572154 -0.027594564 -0.87916297 -0.47572154 -0.027594564 -0.87916297 -0.28224403
		 -0.43176946 -0.85668743 -0.28224403 -0.43176946 -0.85668743 -0.28224403 -0.43176946
		 -0.85668743 -0.28224403 -0.43176946 -0.85668743 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0
		 1 0 0 1 0 0 1 -0.22725588 -0.96226013 -0.14970073 -0.22725588 -0.96226013 -0.14970073
		 -0.22725588 -0.96226013 -0.14970073 -0.22725588 -0.96226013 -0.14970073;
	setAttr -s 64 -ch 256 ".fc[0:63]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 11 13 23 21
		f 4 4 5 6 7
		mu 0 4 31 33 47 45
		f 4 8 9 10 11
		mu 0 4 59 61 75 74
		f 4 12 13 14 15
		mu 0 4 39 41 59 57
		f 4 16 17 -9 -14
		mu 0 4 41 43 61 59
		f 4 18 19 20 -10
		mu 0 4 61 63 76 75
		f 4 21 -2 22 23
		mu 0 4 49 51 69 67
		f 4 -23 24 25 26
		mu 0 4 67 69 79 78
		f 4 27 28 29 30
		mu 0 4 1 2 9 7
		f 4 31 -16 32 33
		mu 0 4 37 39 57 55
		f 4 -33 34 35 36
		mu 0 4 55 57 73 72
		f 4 37 38 39 -29
		mu 0 4 2 3 11 9
		f 4 -7 40 41 42
		mu 0 4 45 47 65 63
		f 4 -42 43 44 -20
		mu 0 4 63 65 77 76
		f 4 45 -31 46 47
		mu 0 4 0 1 7 5
		f 4 48 -43 -19 -18
		mu 0 4 43 45 63 61
		f 4 -15 -12 49 -35
		mu 0 4 57 59 74 73
		f 4 50 -25 -1 -39
		mu 0 4 3 4 13 11
		f 4 51 52 -13 -32
		mu 0 4 25 27 41 39
		f 4 53 54 -17 -53
		mu 0 4 27 29 43 41
		f 4 55 -8 -49 -55
		mu 0 4 29 31 45 43
		f 4 -47 56 57 58
		mu 0 4 5 7 17 15
		f 4 -30 59 60 -57
		mu 0 4 7 9 19 17
		f 4 -40 -4 61 -60
		mu 0 4 9 11 21 19
		f 4 -62 62 -56 63
		mu 0 4 19 21 31 29
		f 4 -61 -64 -54 64
		mu 0 4 17 19 29 27
		f 4 -58 -65 -52 65
		mu 0 4 15 17 27 25
		f 4 66 -37 67 -48
		mu 0 4 53 55 72 71
		f 4 -66 -34 -67 -59
		mu 0 4 35 37 55 53
		f 4 68 -27 69 -44
		mu 0 4 65 67 78 77
		f 4 -6 -24 -69 -41
		mu 0 4 47 49 67 65
		f 4 -3 -22 -5 -63
		mu 0 4 21 23 33 31
		f 4 70 71 72 73
		mu 0 4 12 22 24 14
		f 4 74 75 76 77
		mu 0 4 32 46 48 34
		f 4 78 -11 79 80
		mu 0 4 60 74 75 62
		f 4 81 82 83 84
		mu 0 4 40 58 60 42
		f 4 -84 -81 85 86
		mu 0 4 42 60 62 44
		f 4 -80 -21 87 88
		mu 0 4 62 75 76 64
		f 4 89 90 -73 91
		mu 0 4 50 68 70 52
		f 4 92 -26 93 -91
		mu 0 4 68 78 79 70
		f 4 94 95 96 -28
		mu 0 4 1 8 10 2
		f 4 97 98 -82 99
		mu 0 4 38 56 58 40
		f 4 100 -36 101 -99
		mu 0 4 56 72 73 58
		f 4 -97 102 103 -38
		mu 0 4 2 10 12 3
		f 4 104 105 106 -76
		mu 0 4 46 64 66 48
		f 4 -88 -45 107 -106
		mu 0 4 64 76 77 66
		f 4 108 109 -95 -46
		mu 0 4 0 6 8 1
		f 4 -86 -89 -105 110
		mu 0 4 44 62 64 46
		f 4 -102 -50 -79 -83
		mu 0 4 58 73 74 60
		f 4 -104 -74 -94 -51
		mu 0 4 3 12 14 4
		f 4 -100 -85 111 112
		mu 0 4 26 40 42 28
		f 4 -112 -87 113 114
		mu 0 4 28 42 44 30
		f 4 -114 -111 -75 115
		mu 0 4 30 44 46 32
		f 4 116 117 118 -110
		mu 0 4 6 16 18 8
		f 4 -119 119 120 -96
		mu 0 4 8 18 20 10
		f 4 -121 121 -71 -103
		mu 0 4 10 20 22 12
		f 4 122 -116 123 -122
		mu 0 4 20 30 32 22
		f 4 124 -115 -123 -120
		mu 0 4 18 28 30 20
		f 4 125 -113 -125 -118
		mu 0 4 16 26 28 18
		f 4 -109 -68 -101 126
		mu 0 4 54 71 72 56
		f 4 -117 -127 -98 -126
		mu 0 4 36 54 56 38
		f 4 -108 -70 -93 127
		mu 0 4 66 77 78 68
		f 4 -107 -128 -90 -77
		mu 0 4 48 66 68 50
		f 4 -124 -78 -92 -72
		mu 0 4 22 32 34 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "legs" -p "mesh";
	rename -uid "8B3FD636-4B03-FB39-845F-F9B37472EF43";
	setAttr ".t" -type "double3" 103.66405487060547 213.98854064941406 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" -90.00000933466734 0 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 100 100 100 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "legsShape" -p "legs";
	rename -uid "ACA2B313-48BA-0247-15A0-5388E91DF8BF";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "legsShapeOrig" -p "legs";
	rename -uid "6A2B6DE8-43A4-86FC-27C9-80B51197A587";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 178 ".uvst[0].uvsp[0:177]" -type "float2" 0.375 0.1875 0.375
		 0.1875 0.43827981 0.1875 0.43827981 0.1875 0.48561507 0.1875 0.48561507 0.1875 0.49282902
		 0.1875 0.49282902 0.1875 0.50004297 0.1875 0.50004297 0.1875 0.54784048 0.1875 0.54784048
		 0.1875 0.60545957 0.1875 0.60545957 0.1875 0.625 0.1875 0.625 0.1875 0.625 0.1875
		 0.625 0.1875 0.375 0.21875 0.375 0.21875 0.43827981 0.21875 0.43827981 0.21875 0.48561507
		 0.21875 0.48561507 0.21875 0.49282905 0.21875 0.49282905 0.21875 0.50004303 0.21875
		 0.50004303 0.21875 0.54784048 0.21875 0.54784048 0.21875 0.60545957 0.21875 0.60545957
		 0.21875 0.625 0.21875 0.625 0.21875 0.625 0.21875 0.625 0.21875 0.375 0.25 0.375
		 0.25 0.43827984 0.25 0.43827984 0.25 0.4856151 0.25 0.4856151 0.25 0.49282908 0.25
		 0.49282908 0.25 0.50004303 0.25 0.50004303 0.25 0.54784054 0.25 0.54784054 0.25 0.60545963
		 0.25 0.60545963 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.375 0.375 0.375
		 0.375 0.43827981 0.375 0.43827981 0.375 0.48561507 0.375 0.48561507 0.375 0.49282905
		 0.375 0.49282905 0.375 0.50004303 0.375 0.50004303 0.375 0.54784048 0.375 0.54784048
		 0.375 0.60545957 0.375 0.60545957 0.375 0.625 0.375 0.625 0.375 0.625 0.375 0.625
		 0.375 0.125 0.5 0.125 0.5 0.25 0.5 0.25 0.5 0.375 0.5 0.375 0.5 0.43827981 0.5 0.43827981
		 0.5 0.48561507 0.5 0.48561507 0.5 0.49282905 0.5 0.49282905 0.5 0.50004303 0.5 0.50004303
		 0.5 0.54784048 0.5 0.54784048 0.5 0.60545957 0.5 0.60545957 0.5 0.625 0.5 0.625 0.5
		 0.625 0.5 0.625 0.5 0.75 0.5 0.75 0.5 0.875 0.5 0.875 0.5 0.125 0.53125 0.125 0.53125
		 0.25 0.53125 0.25 0.53125 0.375 0.53125 0.375 0.53125 0.43827981 0.53125 0.43827981
		 0.53125 0.48561507 0.53125 0.48561507 0.53125 0.49282905 0.53125 0.49282905 0.53125
		 0.50004303 0.53125 0.50004303 0.53125 0.54784048 0.53125 0.54784048 0.53125 0.60545957
		 0.53125 0.60545957 0.53125 0.625 0.53125 0.625 0.53125 0.625 0.53125 0.625 0.53125
		 0.75 0.53125 0.75 0.53125 0.875 0.53125 0.875 0.53125 0.50004292 0.56249994 0.50004292
		 0.56249994 0.64352143 0.56249994 0.64352143 0.56249994 0.125 0.5625 0.125 0.5625
		 0.25 0.5625 0.25 0.5625 0.31483942 0.5625 0.31483942 0.5625 0.375 0.5625 0.375 0.5625
		 0.37655962 0.5625 0.37655962 0.5625 0.43827981 0.5625 0.43827981 0.5625 0.45684525
		 0.5625 0.45684525 0.5625 0.47123015 0.5625 0.47123015 0.5625 0.47848713 0.5625 0.47848713
		 0.5625 0.48561507 0.5625 0.48561507 0.5625 0.48565805 0.5625 0.48565805 0.5625 0.49282899
		 0.5625 0.49282899 0.5625 0.50008595 0.5625 0.50008595 0.5625 0.50012898 0.5625 0.50012898
		 0.5625 0.54784048 0.5625 0.54784048 0.5625 0.59568095 0.5625 0.59568095 0.5625 0.60545957
		 0.5625 0.60545957 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.7109192
		 0.5625 0.7109192 0.5625 0.75 0.5625 0.75 0.5625 0.75 0.5625 0.75 0.5625 0.81637883
		 0.5625 0.81637883 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 148 ".vt[0:147]"  -1.027901411 0.41435939 -1.57541716 -1.21523464 0.48766065 3.15848541
		 -0.82229692 0.41435939 -1.57541716 -0.61888838 0.48766065 3.15848541 -1.21523464 0.14650095 3.15848541
		 -0.82229692 0.26028612 -1.57541716 -1.027901411 0.26028612 -1.57541716 -0.61888838 0.14650095 3.15848541
		 -1.020491004 0.43796766 0.10692585 -0.74470878 0.43796766 0.10692585 -0.74470878 0.20771322 0.10692585
		 -1.020491004 0.20771322 0.10692585 -1.21523464 0.62413096 1.86311412 -0.80264789 0.62413096 1.86311412
		 -0.80264789 0.28297132 1.86311412 -1.21523464 0.28297132 1.86311412 -1.0007455349 0.40514556 -0.42604095
		 -0.71894652 0.40514556 -0.42604095 -0.71894652 0.18600351 -0.42604095 -1.0007455349 0.18600351 -0.42604095
		 -1.22844696 0.56209856 2.79537177 -0.66540408 0.56209856 2.79537177 -0.66540408 0.20170498 2.79537177
		 -1.22844696 0.20170498 2.79537177 -0.92509907 0.45322031 -1.57541716 -1.005907774 0.54025733 3.15848541
		 -0.92509907 0.22142518 -1.57541716 -1.005907774 0.093904257 3.15848541 -0.88259983 0.15644714 0.10692579
		 -0.88259983 0.48923361 0.10692579 -1.005907774 0.23037463 1.86311412 -1.005907774 0.67672765 1.86311412
		 -0.85984606 0.45774218 -0.42604095 -0.85984606 0.13340682 -0.42604095 -1.0073186159 0.146143 2.79537177
		 -1.0073186159 0.61766046 2.79537177 -1.28894877 0.29586032 3.15848541 -0.78609526 0.33144319 -1.57541716
		 -1.064103007 0.33144319 -1.57541716 -0.5451743 0.29586032 3.15848541 -0.69615066 0.32858318 0.10692579
		 -1.069049239 0.32858318 0.10692579 -0.72893381 0.4323307 1.86311412 -1.28894877 0.4323307 1.86311412
		 -1.050363064 0.29822707 -0.42604095 -0.66932905 0.29822707 -0.42604095 -0.58753419 0.35948491 2.79537177
		 -1.30631685 0.35948491 2.79537177 -0.92509907 0.33144319 -1.57541716 -1.08947432 0.47318262 0.57526165
		 -0.80290163 0.47318268 0.57526165 -0.80290163 0.24073675 0.57526165 -1.08947432 0.24073675 0.57526159
		 -0.94559455 0.52036071 0.57526159 -0.94559455 0.19355866 0.57526159 -1.14014137 0.35693416 0.57526159
		 -0.75223464 0.35693416 0.57526159 -1.032119036 0.42317051 0.51668429 -0.84130412 0.42317054 0.51668429
		 -0.84130412 0.26911044 0.51668429 -1.032119036 0.26911044 0.51668423 -0.9365074 0.45589748 0.51668417
		 -0.9365074 0.23638344 0.51668417 -1.065788507 0.3479794 0.51668417 -0.80763453 0.3479794 0.51668417
		 -1.11771429 0.25505364 3.37875319 -1.080878496 0.35089856 3.37875319 -0.97627527 0.37718177 3.37875319
		 -0.78287703 0.35089856 3.37875319 -0.74604118 0.25505364 3.37875319 -0.78287703 0.18041694 3.37875319
		 -0.97627527 0.15413374 3.37875319 -1.080878496 0.18041694 3.37875319 -0.97627527 0.25505364 3.37875319
		 -1.027901411 -0.41435939 -1.57541716 -1.21523464 -0.48766065 3.15848541 -0.82229692 -0.41435939 -1.57541716
		 -0.61888838 -0.48766065 3.15848541 -1.21523464 -0.14650095 3.15848541 -0.82229692 -0.26028612 -1.57541716
		 -1.027901411 -0.26028612 -1.57541716 -0.61888838 -0.14650095 3.15848541 -1.020491004 -0.43796766 0.10692585
		 -0.74470878 -0.43796766 0.10692585 -0.74470878 -0.20771322 0.10692585 -1.020491004 -0.20771322 0.10692585
		 -1.21523464 -0.62413096 1.86311412 -0.80264789 -0.62413096 1.86311412 -0.80264789 -0.28297132 1.86311412
		 -1.21523464 -0.28297132 1.86311412 -1.0007455349 -0.40514556 -0.42604095 -0.71894652 -0.40514556 -0.42604095
		 -0.71894652 -0.18600351 -0.42604095 -1.0007455349 -0.18600351 -0.42604095 -1.22844696 -0.56209856 2.79537177
		 -0.66540408 -0.56209856 2.79537177 -0.66540408 -0.20170498 2.79537177 -1.22844696 -0.20170498 2.79537177
		 -0.92509907 -0.45322031 -1.57541716 -1.005907774 -0.54025733 3.15848541 -0.92509907 -0.22142518 -1.57541716
		 -1.005907774 -0.093904257 3.15848541 -0.88259983 -0.15644714 0.10692579 -0.88259983 -0.48923361 0.10692579
		 -1.005907774 -0.23037463 1.86311412 -1.005907774 -0.67672765 1.86311412 -0.85984606 -0.45774218 -0.42604095
		 -0.85984606 -0.13340682 -0.42604095 -1.0073186159 -0.146143 2.79537177 -1.0073186159 -0.61766046 2.79537177
		 -1.28894877 -0.29586032 3.15848541 -0.78609526 -0.33144319 -1.57541716 -1.064103007 -0.33144319 -1.57541716
		 -0.5451743 -0.29586032 3.15848541 -0.69615066 -0.32858318 0.10692579 -1.069049239 -0.32858318 0.10692579
		 -0.72893381 -0.4323307 1.86311412 -1.28894877 -0.4323307 1.86311412 -1.050363064 -0.29822707 -0.42604095
		 -0.66932905 -0.29822707 -0.42604095 -0.58753419 -0.35948491 2.79537177 -1.30631685 -0.35948491 2.79537177
		 -0.92509907 -0.33144319 -1.57541716 -1.08947432 -0.47318262 0.57526165 -0.80290163 -0.47318268 0.57526165
		 -0.80290163 -0.24073675 0.57526165 -1.08947432 -0.24073675 0.57526159 -0.94559455 -0.52036071 0.57526159
		 -0.94559455 -0.19355866 0.57526159 -1.14014137 -0.35693416 0.57526159 -0.75223464 -0.35693416 0.57526159
		 -1.032119036 -0.42317051 0.51668429 -0.84130412 -0.42317054 0.51668429 -0.84130412 -0.26911044 0.51668429
		 -1.032119036 -0.26911044 0.51668423 -0.9365074 -0.45589748 0.51668417 -0.9365074 -0.23638344 0.51668417
		 -1.065788507 -0.3479794 0.51668417 -0.80763453 -0.3479794 0.51668417 -1.11771429 -0.25505364 3.37875319
		 -1.080878496 -0.35089856 3.37875319 -0.97627527 -0.37718177 3.37875319 -0.78287703 -0.35089856 3.37875319
		 -0.74604118 -0.25505364 3.37875319 -0.78287703 -0.18041694 3.37875319 -0.97627527 -0.15413374 3.37875319
		 -1.080878496 -0.18041694 3.37875319 -0.97627527 -0.25505364 3.37875319;
	setAttr -s 288 ".ed";
	setAttr ".ed[0:165]"  47 36 0 36 1 0 1 20 0 20 47 0 35 25 0 25 3 0 3 21 0
		 21 35 0 33 19 0 19 6 0 6 26 0 26 33 0 46 39 0 39 7 0 7 22 0 22 46 0 48 37 0 37 5 0
		 5 26 0 26 48 0 39 3 0 3 68 0 68 69 0 69 39 0 45 40 0 40 10 0 10 18 0 18 45 0 62 60 0
		 60 11 0 11 28 0 28 62 0 32 29 0 29 9 0 9 17 0 17 32 0 44 41 0 41 8 0 8 16 0 16 44 0
		 34 23 0 23 15 0 15 30 0 30 34 0 56 42 0 42 14 0 14 51 0 51 56 0 53 31 0 31 13 0 13 50 0
		 50 53 0 55 43 0 43 12 0 12 49 0 49 55 0 38 44 0 16 0 0 0 38 0 24 32 0 17 2 0 2 24 0
		 37 45 0 18 5 0 11 19 0 33 28 0 27 4 0 4 23 0 34 27 0 42 46 0 22 14 0 31 35 0 21 13 0
		 43 47 0 20 12 0 20 35 0 31 12 0 7 27 0 34 22 0 10 28 0 33 18 0 16 32 0 24 0 0 53 49 0
		 30 14 0 8 29 0 59 62 0 10 59 0 69 70 0 70 7 0 38 48 0 6 38 0 1 25 0 24 48 0 36 65 0
		 65 66 0 66 1 0 23 47 0 43 15 0 21 46 0 42 13 0 17 45 0 37 2 0 19 44 0 52 15 0 55 52 0
		 56 50 0 11 41 0 9 40 0 36 4 0 4 72 0 72 65 0 58 50 0 56 64 0 64 58 0 60 52 0 55 63 0
		 63 60 0 30 54 0 54 51 0 57 49 0 53 61 0 61 57 0 57 63 0 58 61 0 51 59 0 59 64 0 52 54 0
		 40 64 0 29 61 0 58 9 0 41 63 0 57 8 0 54 62 0 73 65 0 72 71 0 71 73 0 69 73 0 71 70 0
		 68 67 0 67 73 0 67 66 0 27 71 0 25 67 0 121 94 0 94 75 0 75 110 0 110 121 0 109 95 0
		 95 77 0 77 99 0 99 109 0 107 100 0 100 80 0 80 93 0 93 107 0 120 96 0 96 81 0 81 113 0
		 113 120 0 122 100 0 100 79 0 79 111 0 111 122 0 113 143 0 143 142 0;
	setAttr ".ed[166:287]" 142 77 0 77 113 0 119 92 0 92 84 0 84 114 0 114 119 0
		 136 102 0 102 85 0 85 134 0 134 136 0 106 91 0 91 83 0 83 103 0 103 106 0 118 90 0
		 90 82 0 82 115 0 115 118 0 108 104 0 104 89 0 89 97 0 97 108 0 130 125 0 125 88 0
		 88 116 0 116 130 0 127 124 0 124 87 0 87 105 0 105 127 0 129 123 0 123 86 0 86 117 0
		 117 129 0 112 74 0 74 90 0 118 112 0 98 76 0 76 91 0 106 98 0 79 92 0 119 111 0 102 107 0
		 93 85 0 101 108 0 97 78 0 78 101 0 88 96 0 120 116 0 87 95 0 109 105 0 86 94 0 121 117 0
		 86 105 0 109 94 0 96 108 0 101 81 0 92 107 0 102 84 0 74 98 0 106 90 0 123 127 0
		 88 104 0 103 82 0 133 84 0 136 133 0 81 144 0 144 143 0 112 80 0 122 112 0 99 75 0
		 122 98 0 75 140 0 140 139 0 139 110 0 89 117 0 121 97 0 87 116 0 120 95 0 76 111 0
		 119 91 0 118 93 0 126 129 0 89 126 0 124 130 0 115 85 0 114 83 0 139 146 0 146 78 0
		 78 110 0 132 138 0 138 130 0 124 132 0 134 137 0 137 129 0 126 134 0 125 128 0 128 104 0
		 131 135 0 135 127 0 123 131 0 137 131 0 135 132 0 138 133 0 133 125 0 128 126 0 138 114 0
		 83 132 0 135 103 0 82 131 0 137 115 0 136 128 0 147 145 0 145 146 0 139 147 0 144 145 0
		 147 143 0 147 141 0 141 142 0 140 141 0 145 101 0 141 99 0;
	setAttr -s 576 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.92808795 0.35668927 0.10689041 -0.92808795
		 0.35668927 0.10689041 -0.92808795 0.35668927 0.10689041 -0.92808795 0.35668927 0.10689041
		 0.1440009 0.97049296 0.19340938 0.1440009 0.97049296 0.19340938 0.1440009 0.97049296
		 0.19340938 0.1440009 0.97049296 0.19340938 -0.35088363 -0.93497926 -0.051907837 -0.35088363
		 -0.93497926 -0.051907837 -0.35088363 -0.93497926 -0.051907837 -0.35088363 -0.93497926
		 -0.051907837 0.88221705 -0.43540484 -0.17920859 0.88221705 -0.43540484 -0.17920859
		 0.88221705 -0.43540484 -0.17920859 0.88221705 -0.43540484 -0.17920859 0 0 -1 0 0
		 -1 0 0 -1 0 0 -1 0.68773866 0.26431653 0.67613041 0.68773866 0.26431653 0.67613041
		 0.68773866 0.26431653 0.67613041 0.68773866 0.26431653 0.67613041 0.91968501 -0.38735807
		 0.064289838 0.91968501 -0.38735807 0.064289838 0.91968501 -0.38735807 0.064289838
		 0.91968501 -0.38735807 0.064289838 -0.33542505 -0.93248898 0.13399333 -0.33542505
		 -0.93248898 0.13399333 -0.33542505 -0.93248898 0.13399333 -0.33542505 -0.93248898
		 0.13399333 0.34881917 0.93630946 -0.04061614 0.34881917 0.93630946 -0.04061614 0.34881917
		 0.93630946 -0.04061614 0.34881917 0.93630946 -0.04061614 -0.90910137 0.41262457 -0.057233483
		 -0.90910137 0.41262457 -0.057233483 -0.90910137 0.41262457 -0.057233483 -0.90910137
		 0.41262457 -0.057233483 -0.24275281 -0.96611953 -0.087658517 -0.24275281 -0.96611953
		 -0.087658517 -0.24275281 -0.96611953 -0.087658517 -0.24275281 -0.96611953 -0.087658517
		 0.90553236 -0.42413226 0.011088877 0.90553236 -0.42413226 0.011088877 0.90553236
		 -0.42413226 0.011088877 0.90553236 -0.42413226 0.011088877 0.2755037 0.95526457 -0.10755141
		 0.2755037 0.95526457 -0.10755141 0.2755037 0.95526457 -0.10755141 0.2755037 0.95526457
		 -0.10755141 -0.91932219 0.37119466 -0.13061842 -0.91932219 0.37119466 -0.13061842
		 -0.91932219 0.37119466 -0.13061842 -0.91932219 0.37119466 -0.13061842 -0.91095483
		 0.41181839 0.023807559 -0.91095483 0.41181839 0.023807559 -0.91095483 0.41181839
		 0.023807559 -0.91095483 0.41181839 0.023807559 0.35125729 0.93597507 -0.023852762
		 0.35125729 0.93597507 -0.023852762 0.35125729 0.93597507 -0.023852762 0.35125729
		 0.93597507 -0.023852762 0.90062767 -0.42147878 -0.10594946 0.90062767 -0.42147878
		 -0.10594946 0.90062767 -0.42147878 -0.10594946 0.90062767 -0.42147878 -0.10594946
		 -0.34899512 -0.93677992 0.025413333 -0.34899512 -0.93677992 0.025413333 -0.34899512
		 -0.93677992 0.025413333 -0.34899512 -0.93677992 0.025413333 -0.24138407 -0.96067238
		 -0.13726763 -0.24138407 -0.96067238 -0.13726763 -0.24138407 -0.96067238 -0.13726763
		 -0.24138407 -0.96067238 -0.13726763 0.88396388 -0.43626675 -0.16816421 0.88396388
		 -0.43626675 -0.16816421 0.88396388 -0.43626675 -0.16816421 0.88396388 -0.43626675
		 -0.16816421 0.19436166 0.97968173 0.049470365 0.19436166 0.97968173 0.049470365 0.19436166
		 0.97968173 0.049470365 0.19436166 0.97968173 0.049470365 -0.93338299 0.35872409 0.010641336
		 -0.93338299 0.35872409 0.010641336 -0.93338299 0.35872409 0.010641336 -0.93338299
		 0.35872409 0.010641336 -0.24323747 0.96804881 0.060966808 -0.24323747 0.96804881
		 0.060966808 -0.24323747 0.96804881 0.060966808 -0.24323747 0.96804881 0.060966808
		 0.14501779 -0.97734481 -0.15416503 0.14501779 -0.97734481 -0.15416503 0.14501779
		 -0.97734481 -0.15416503 0.14501779 -0.97734481 -0.15416503 0.34857652 -0.93565667
		 0.055146229 0.34857652 -0.93565667 0.055146229 0.34857652 -0.93565667 0.055146229
		 0.34857652 -0.93565667 0.055146229 -0.35131183 0.93612117 0.016033264 -0.35131183
		 0.93612117 0.016033264 -0.35131183 0.93612117 0.016033264 -0.35131183 0.93612117
		 0.016033264 -0.26942018 0.95375824 -0.13325915 -0.26942018 0.95375824 -0.13325915
		 -0.26942018 0.95375824 -0.13325915 -0.26942018 0.95375824 -0.13325915 0.19361028
		 -0.97589397 -0.10072701 0.19361028 -0.97589397 -0.10072701 0.19361028 -0.97589397
		 -0.10072701 0.19361028 -0.97589397 -0.10072701 -0.34824404 0.9347654 -0.070284098
		 -0.34824404 0.9347654 -0.070284098 -0.34824404 0.9347654 -0.070284098 -0.34824404
		 0.9347654 -0.070284098 0.3307685 -0.9179371 0.21905196 0.3307685 -0.9179371 0.21905196
		 0.3307685 -0.9179371 0.21905196 0.3307685 -0.9179371 0.21905196 0.72229362 -0.35647744
		 0.5926345 0.72229362 -0.35647744 0.5926345 0.72229362 -0.35647744 0.5926345 0.72229362
		 -0.35647744 0.5926345 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0.34988403 -0.93231493 -0.091487594
		 0.34988403 -0.93231493 -0.091487594 0.34988403 -0.93231493 -0.091487594 0.34988403
		 -0.93231493 -0.091487594 -0.23859939 0.94958973 0.20334637 -0.23859939 0.94958973
		 0.20334637 -0.23859939 0.94958973 0.20334637 -0.23859939 0.94958973 0.20334637 0
		 0 -1 0 0 -1 0 0 -1 0 0 -1 -0.73169965 0.28121221 0.620915 -0.73169965 0.28121221
		 0.620915 -0.73169965 0.28121221 0.620915 -0.73169965 0.28121221 0.620915 -0.89555711
		 -0.44198841 -0.051220998 -0.89555711 -0.44198841 -0.051220998 -0.89555711 -0.44198841
		 -0.051220998 -0.89555711 -0.44198841 -0.051220998 0.92747611 0.35645393 -0.11282133
		 0.92747611 0.35645393 -0.11282133 0.92747611 0.35645393 -0.11282133 0.92747611 0.35645393
		 -0.11282133 0.90833664 0.41063428 -0.079398207 0.90833664 0.41063428 -0.079398207
		 0.90833664 0.41063428 -0.079398207 0.90833664 0.41063428 -0.079398207 -0.90571922
		 -0.42386201 -0.0037082972 -0.90571922 -0.42386201 -0.0037082972 -0.90571922 -0.42386201
		 -0.0037082972 -0.90571922 -0.42386201 -0.0037082972 -0.90290421 -0.42290068 -0.076935068
		 -0.90290421 -0.42290068 -0.076935068 -0.90290421 -0.42290068 -0.076935068 -0.90290421
		 -0.42290068 -0.076935068 0.92647326 0.3740828 -0.041345708 0.92647326 0.3740828 -0.041345708;
	setAttr ".n[166:331]" -type "float3"  0.92647326 0.3740828 -0.041345708 0.92647326
		 0.3740828 -0.041345708 -0.92149729 -0.38812268 -0.014266164 -0.92149729 -0.38812268
		 -0.014266164 -0.92149729 -0.38812268 -0.014266164 -0.92149729 -0.38812268 -0.014266164
		 0.91040462 0.41321468 0.020420052 0.91040462 0.41321468 0.020420052 0.91040462 0.41321468
		 0.020420052 0.91040462 0.41321468 0.020420052 -0.76379997 -0.37696207 0.52393633
		 -0.76379997 -0.37696207 0.52393633 -0.76379997 -0.37696207 0.52393633 -0.76379997
		 -0.37696207 0.52393633 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0.93244839 0.35836506 -0.045984387
		 0.93244839 0.35836506 -0.045984387 0.93244839 0.35836506 -0.045984387 0.93244839
		 0.35836506 -0.045984387 -0.89619613 -0.44230413 -0.034634322 -0.89619613 -0.44230413
		 -0.034634322 -0.89619613 -0.44230413 -0.034634322 -0.89619613 -0.44230413 -0.034634322
		 0.668244 0.29438829 -0.68321699 0.668244 0.29438829 -0.68321699 0.668244 0.29438829
		 -0.68321699 0.668244 0.29438829 -0.68321699 -0.61814797 -0.26725435 -0.73923492 -0.61814797
		 -0.26725435 -0.73923492 -0.61814797 -0.26725435 -0.73923492 -0.61814797 -0.26725435
		 -0.73923492 0.27693233 -0.96021748 0.035927318 0.27693233 -0.96021748 0.035927318
		 0.27693233 -0.96021748 0.035927318 0.27693233 -0.96021748 0.035927318 -0.2122207
		 0.63606685 -0.7418769 -0.2122207 0.63606685 -0.7418769 -0.2122207 0.63606685 -0.7418769
		 -0.2122207 0.63606685 -0.7418769 -0.57680452 0.25410485 -0.77635509 -0.57680452 0.25410485
		 -0.77635509 -0.57680452 0.25410485 -0.77635509 -0.57680452 0.25410485 -0.77635509
		 0.22447495 0.66831559 -0.7092005 0.22447495 0.66831559 -0.7092005 0.22447495 0.66831559
		 -0.7092005 0.22447495 0.66831559 -0.7092005 0.71650177 -0.30977905 -0.62502968 0.71650177
		 -0.30977905 -0.62502968 0.71650177 -0.30977905 -0.62502968 0.71650177 -0.30977905
		 -0.62502968 -0.27183142 -0.96229392 0.0098960176 -0.27183142 -0.96229392 0.0098960176
		 -0.27183142 -0.96229392 0.0098960176 -0.27183142 -0.96229392 0.0098960176 0.8922168
		 -0.36730349 0.26274934 0.8922168 -0.36730349 0.26274934 0.8922168 -0.36730349 0.26274934
		 0.8922168 -0.36730349 0.26274934 0.3366845 0.93435669 0.11670975 0.3366845 0.93435669
		 0.11670975 0.3366845 0.93435669 0.11670975 0.3366845 0.93435669 0.11670975 -0.91339314
		 0.40691304 -0.011609324 -0.91339314 0.40691304 -0.011609324 -0.91339314 0.40691304
		 -0.011609324 -0.91339314 0.40691304 -0.011609324 -0.33834234 0.94060075 0.028188277
		 -0.33834234 0.94060075 0.028188277 -0.33834234 0.94060075 0.028188277 -0.33834234
		 0.94060075 0.028188277 -0.92434239 -0.38052958 0.028077606 -0.92434239 -0.38052958
		 0.028077606 -0.92434239 -0.38052958 0.028077606 -0.92434239 -0.38052958 0.028077606
		 0.8902809 0.39661589 0.22382087 0.8902809 0.39661589 0.22382087 0.8902809 0.39661589
		 0.22382087 0.8902809 0.39661589 0.22382087 0.26625696 -0.79270977 -0.54837811 0.26625696
		 -0.79270977 -0.54837811 0.26625696 -0.79270977 -0.54837811 0.26625696 -0.79270977
		 -0.54837811 -0.25246838 -0.7566967 -0.60305035 -0.25246838 -0.7566967 -0.60305035
		 -0.25246838 -0.7566967 -0.60305035 -0.25246838 -0.7566967 -0.60305035 -2.1173348e-06
		 0 0.99999994 -2.1173348e-06 0 0.99999994 -2.1173348e-06 0 0.99999994 -2.1173348e-06
		 0 0.99999994 1.2180241e-06 0 0.99999994 1.2180241e-06 0 0.99999994 1.2180241e-06
		 0 0.99999994 1.2180241e-06 0 0.99999994 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1
		 0 0 1 -0.23354284 -0.92946643 0.28556943 -0.23354284 -0.92946643 0.28556943 -0.23354284
		 -0.92946643 0.28556943 -0.23354284 -0.92946643 0.28556943 0.10951824 0.80586153 0.58188742
		 0.10951824 0.80586153 0.58188742 0.10951824 0.80586153 0.58188742 0.10951824 0.80586153
		 0.58188742 -0.19487584 0.77557874 0.60041738 -0.19487584 0.77557874 0.60041738 -0.19487584
		 0.77557874 0.60041738 -0.19487584 0.77557874 0.60041738 0.13055591 -0.9606626 0.24511766
		 0.13055591 -0.9606626 0.24511766 0.13055591 -0.9606626 0.24511766 0.13055591 -0.9606626
		 0.24511766 -0.92808795 -0.35668936 0.10689043 -0.92808795 -0.35668936 0.10689043
		 -0.92808795 -0.35668936 0.10689043 -0.92808795 -0.35668936 0.10689043 0.1440009 -0.97049296
		 0.19340932 0.1440009 -0.97049296 0.19340932 0.1440009 -0.97049296 0.19340932 0.1440009
		 -0.97049296 0.19340932 -0.35088357 0.93497926 -0.051907834 -0.35088357 0.93497926
		 -0.051907834 -0.35088357 0.93497926 -0.051907834 -0.35088357 0.93497926 -0.051907834
		 0.88221705 0.43540484 -0.17920861 0.88221705 0.43540484 -0.17920861 0.88221705 0.43540484
		 -0.17920861 0.88221705 0.43540484 -0.17920861 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0.68773872
		 -0.26431656 0.67613041 0.68773872 -0.26431656 0.67613041 0.68773872 -0.26431656 0.67613041
		 0.68773872 -0.26431656 0.67613041 0.91968513 0.38735792 0.064289846 0.91968513 0.38735792
		 0.064289846 0.91968513 0.38735792 0.064289846 0.91968513 0.38735792 0.064289846 -0.33542499
		 0.93248904 0.13399333 -0.33542499 0.93248904 0.13399333 -0.33542499 0.93248904 0.13399333
		 -0.33542499 0.93248904 0.13399333 0.3488192 -0.93630952 -0.040616132 0.3488192 -0.93630952
		 -0.040616132 0.3488192 -0.93630952 -0.040616132 0.3488192 -0.93630952 -0.040616132
		 -0.90910137 -0.4126246 -0.057233483 -0.90910137 -0.4126246 -0.057233483 -0.90910137
		 -0.4126246 -0.057233483 -0.90910137 -0.4126246 -0.057233483 -0.24275278 0.96611953
		 -0.087658517 -0.24275278 0.96611953 -0.087658517 -0.24275278 0.96611953 -0.087658517
		 -0.24275278 0.96611953 -0.087658517;
	setAttr ".n[332:497]" -type "float3"  0.90553236 0.42413226 0.01108888 0.90553236
		 0.42413226 0.01108888 0.90553236 0.42413226 0.01108888 0.90553236 0.42413226 0.01108888
		 0.2755037 -0.95526457 -0.10755141 0.2755037 -0.95526457 -0.10755141 0.2755037 -0.95526457
		 -0.10755141 0.2755037 -0.95526457 -0.10755141 -0.91932219 -0.37119466 -0.13061842
		 -0.91932219 -0.37119466 -0.13061842 -0.91932219 -0.37119466 -0.13061842 -0.91932219
		 -0.37119466 -0.13061842 -0.91095483 -0.41181839 0.023807552 -0.91095483 -0.41181839
		 0.023807552 -0.91095483 -0.41181839 0.023807552 -0.91095483 -0.41181839 0.023807552
		 0.35125732 -0.93597507 -0.023852738 0.35125732 -0.93597507 -0.023852738 0.35125732
		 -0.93597507 -0.023852738 0.35125732 -0.93597507 -0.023852738 0.90062773 0.42147878
		 -0.10594947 0.90062773 0.42147878 -0.10594947 0.90062773 0.42147878 -0.10594947 0.90062773
		 0.42147878 -0.10594947 -0.34899512 0.93677992 0.025413336 -0.34899512 0.93677992
		 0.025413336 -0.34899512 0.93677992 0.025413336 -0.34899512 0.93677992 0.025413336
		 -0.24138407 0.96067238 -0.13726765 -0.24138407 0.96067238 -0.13726765 -0.24138407
		 0.96067238 -0.13726765 -0.24138407 0.96067238 -0.13726765 0.88396388 0.43626675 -0.16816422
		 0.88396388 0.43626675 -0.16816422 0.88396388 0.43626675 -0.16816422 0.88396388 0.43626675
		 -0.16816422 0.19436163 -0.97968173 0.049470395 0.19436163 -0.97968173 0.049470395
		 0.19436163 -0.97968173 0.049470395 0.19436163 -0.97968173 0.049470395 -0.93338299
		 -0.35872412 0.010641337 -0.93338299 -0.35872412 0.010641337 -0.93338299 -0.35872412
		 0.010641337 -0.93338299 -0.35872412 0.010641337 -0.24323747 -0.96804881 0.060966831
		 -0.24323747 -0.96804881 0.060966831 -0.24323747 -0.96804881 0.060966831 -0.24323747
		 -0.96804881 0.060966831 0.14501779 0.97734481 -0.15416501 0.14501779 0.97734481 -0.15416501
		 0.14501779 0.97734481 -0.15416501 0.14501779 0.97734481 -0.15416501 0.34857655 0.93565667
		 0.055146232 0.34857655 0.93565667 0.055146232 0.34857655 0.93565667 0.055146232 0.34857655
		 0.93565667 0.055146232 -0.3513118 -0.93612117 0.016033277 -0.3513118 -0.93612117
		 0.016033277 -0.3513118 -0.93612117 0.016033277 -0.3513118 -0.93612117 0.016033277
		 -0.26942015 -0.95375824 -0.13325916 -0.26942015 -0.95375824 -0.13325916 -0.26942015
		 -0.95375824 -0.13325916 -0.26942015 -0.95375824 -0.13325916 0.19361025 0.97589397
		 -0.10072701 0.19361025 0.97589397 -0.10072701 0.19361025 0.97589397 -0.10072701 0.19361025
		 0.97589397 -0.10072701 -0.34824404 -0.9347654 -0.070284083 -0.34824404 -0.9347654
		 -0.070284083 -0.34824404 -0.9347654 -0.070284083 -0.34824404 -0.9347654 -0.070284083
		 0.33076856 0.9179371 0.21905205 0.33076856 0.9179371 0.21905205 0.33076856 0.9179371
		 0.21905205 0.33076856 0.9179371 0.21905205 0.72229362 0.35647744 0.5926345 0.72229362
		 0.35647744 0.5926345 0.72229362 0.35647744 0.5926345 0.72229362 0.35647744 0.5926345
		 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0.34988403 0.93231493 -0.091487594 0.34988403 0.93231493
		 -0.091487594 0.34988403 0.93231493 -0.091487594 0.34988403 0.93231493 -0.091487594
		 -0.23859939 -0.94958973 0.20334639 -0.23859939 -0.94958973 0.20334639 -0.23859939
		 -0.94958973 0.20334639 -0.23859939 -0.94958973 0.20334639 0 0 -1 0 0 -1 0 0 -1 0
		 0 -1 -0.73169982 -0.28121212 0.62091482 -0.73169982 -0.28121212 0.62091482 -0.73169982
		 -0.28121212 0.62091482 -0.73169982 -0.28121212 0.62091482 -0.89555711 0.44198841
		 -0.051220998 -0.89555711 0.44198841 -0.051220998 -0.89555711 0.44198841 -0.051220998
		 -0.89555711 0.44198841 -0.051220998 0.92747611 -0.35645393 -0.11282136 0.92747611
		 -0.35645393 -0.11282136 0.92747611 -0.35645393 -0.11282136 0.92747611 -0.35645393
		 -0.11282136 0.90833664 -0.41063428 -0.079398207 0.90833664 -0.41063428 -0.079398207
		 0.90833664 -0.41063428 -0.079398207 0.90833664 -0.41063428 -0.079398207 -0.90571922
		 0.42386201 -0.0037083011 -0.90571922 0.42386201 -0.0037083011 -0.90571922 0.42386201
		 -0.0037083011 -0.90571922 0.42386201 -0.0037083011 -0.90290421 0.42290065 -0.07693506
		 -0.90290421 0.42290065 -0.07693506 -0.90290421 0.42290065 -0.07693506 -0.90290421
		 0.42290065 -0.07693506 0.92647326 -0.3740828 -0.041345704 0.92647326 -0.3740828 -0.041345704
		 0.92647326 -0.3740828 -0.041345704 0.92647326 -0.3740828 -0.041345704 -0.92149729
		 0.38812271 -0.014266171 -0.92149729 0.38812271 -0.014266171 -0.92149729 0.38812271
		 -0.014266171 -0.92149729 0.38812271 -0.014266171 0.91040474 -0.41321453 0.020420039
		 0.91040474 -0.41321453 0.020420039 0.91040474 -0.41321453 0.020420039 0.91040474
		 -0.41321453 0.020420039 -0.76380002 0.37696198 0.52393609 -0.76380002 0.37696198
		 0.52393609 -0.76380002 0.37696198 0.52393609 -0.76380002 0.37696198 0.52393609 0
		 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0.93244839 -0.35836497
		 -0.045984376 0.93244839 -0.35836497 -0.045984376 0.93244839 -0.35836497 -0.045984376
		 0.93244839 -0.35836497 -0.045984376 -0.89619613 0.44230422 -0.034634326 -0.89619613
		 0.44230422 -0.034634326 -0.89619613 0.44230422 -0.034634326 -0.89619613 0.44230422
		 -0.034634326 0.66824394 -0.2943882 -0.68321711 0.66824394 -0.2943882 -0.68321711
		 0.66824394 -0.2943882 -0.68321711 0.66824394 -0.2943882 -0.68321711 -0.61814791 0.26725441
		 -0.73923498 -0.61814791 0.26725441 -0.73923498 -0.61814791 0.26725441 -0.73923498
		 -0.61814791 0.26725441 -0.73923498 0.27693227 0.96021748 0.035927303 0.27693227 0.96021748
		 0.035927303 0.27693227 0.96021748 0.035927303 0.27693227 0.96021748 0.035927303 -0.21222064
		 -0.63606662 -0.74187702 -0.21222064 -0.63606662 -0.74187702 -0.21222064 -0.63606662
		 -0.74187702 -0.21222064 -0.63606662 -0.74187702 -0.57680452 -0.25410479 -0.77635515
		 -0.57680452 -0.25410479 -0.77635515;
	setAttr ".n[498:575]" -type "float3"  -0.57680452 -0.25410479 -0.77635515 -0.57680452
		 -0.25410479 -0.77635515 0.22447495 -0.66831559 -0.7092005 0.22447495 -0.66831559
		 -0.7092005 0.22447495 -0.66831559 -0.7092005 0.22447495 -0.66831559 -0.7092005 0.71650177
		 0.30977884 -0.6250298 0.71650177 0.30977884 -0.6250298 0.71650177 0.30977884 -0.6250298
		 0.71650177 0.30977884 -0.6250298 -0.27183142 0.96229404 0.0098960269 -0.27183142
		 0.96229404 0.0098960269 -0.27183142 0.96229404 0.0098960269 -0.27183142 0.96229404
		 0.0098960269 0.89221692 0.36730331 0.26274937 0.89221692 0.36730331 0.26274937 0.89221692
		 0.36730331 0.26274937 0.89221692 0.36730331 0.26274937 0.33668455 -0.93435663 0.11670978
		 0.33668455 -0.93435663 0.11670978 0.33668455 -0.93435663 0.11670978 0.33668455 -0.93435663
		 0.11670978 -0.91339308 -0.40691316 -0.011609323 -0.91339308 -0.40691316 -0.011609323
		 -0.91339308 -0.40691316 -0.011609323 -0.91339308 -0.40691316 -0.011609323 -0.33834234
		 -0.94060081 0.028188277 -0.33834234 -0.94060081 0.028188277 -0.33834234 -0.94060081
		 0.028188277 -0.33834234 -0.94060081 0.028188277 -0.92434239 0.38052961 0.028077604
		 -0.92434239 0.38052961 0.028077604 -0.92434239 0.38052961 0.028077604 -0.92434239
		 0.38052961 0.028077604 0.89028096 -0.39661571 0.22382091 0.89028096 -0.39661571 0.22382091
		 0.89028096 -0.39661571 0.22382091 0.89028096 -0.39661571 0.22382091 0.2662569 0.79270983
		 -0.54837799 0.2662569 0.79270983 -0.54837799 0.2662569 0.79270983 -0.54837799 0.2662569
		 0.79270983 -0.54837799 -0.25246838 0.75669682 -0.60305035 -0.25246838 0.75669682
		 -0.60305035 -0.25246838 0.75669682 -0.60305035 -0.25246838 0.75669682 -0.60305035
		 -2.8231132e-06 0 0.99999994 -2.8231132e-06 0 0.99999994 -2.8231132e-06 0 0.99999994
		 -2.8231132e-06 0 0.99999994 1.6240321e-06 0 0.99999994 1.6240321e-06 0 0.99999994
		 1.6240321e-06 0 0.99999994 1.6240321e-06 0 0.99999994 0 0 1 0 0 1 0 0 1 0 0 1 0 0
		 1 0 0 1 0 0 1 0 0 1 -0.23354264 0.92946643 0.28556943 -0.23354264 0.92946643 0.28556943
		 -0.23354264 0.92946643 0.28556943 -0.23354264 0.92946643 0.28556943 0.10951853 -0.80586153
		 0.58188742 0.10951853 -0.80586153 0.58188742 0.10951853 -0.80586153 0.58188742 0.10951853
		 -0.80586153 0.58188742 -0.19487567 -0.77557874 0.60041738 -0.19487567 -0.77557874
		 0.60041738 -0.19487567 -0.77557874 0.60041738 -0.19487567 -0.77557874 0.60041738
		 0.13055591 0.9606626 0.24511766 0.13055591 0.9606626 0.24511766 0.13055591 0.9606626
		 0.24511766 0.13055591 0.9606626 0.24511766;
	setAttr -s 144 -ch 576 ".fc[0:143]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 30 32 50 48
		f 4 4 5 6 7
		mu 0 4 66 68 90 88
		f 4 8 9 10 11
		mu 0 4 136 132 128 130
		f 4 12 13 14 15
		mu 0 4 114 116 162 160
		f 4 16 17 18 19
		mu 0 4 100 102 134 130
		f 4 20 21 22 23
		mu 0 4 116 90 91 117
		f 4 24 25 26 27
		mu 0 4 104 106 146 138
		f 4 28 29 30 31
		mu 0 4 148 144 140 142
		f 4 32 33 34 35
		mu 0 4 56 58 80 78
		f 4 36 37 38 39
		mu 0 4 20 22 40 38
		f 4 40 41 42 43
		mu 0 4 166 172 126 158
		f 4 44 45 46 47
		mu 0 4 110 112 156 124
		f 4 48 49 50 51
		mu 0 4 62 64 86 84
		f 4 52 53 54 55
		mu 0 4 26 28 46 44
		f 4 56 -40 57 58
		mu 0 4 18 20 38 36
		f 4 59 -36 60 61
		mu 0 4 54 56 78 76
		f 4 62 -28 63 -18
		mu 0 4 102 104 138 134
		f 4 -31 64 -9 65
		mu 0 4 142 140 132 136
		f 4 66 67 -41 68
		mu 0 4 168 174 172 166
		f 4 69 -16 70 -46
		mu 0 4 112 114 160 156
		f 4 71 -8 72 -50
		mu 0 4 64 66 88 86
		f 4 73 -4 74 -54
		mu 0 4 28 30 48 46
		f 4 -75 75 -72 76
		mu 0 4 46 48 66 64
		f 4 77 -69 78 -15
		mu 0 4 162 168 166 160
		f 4 79 -66 80 -27
		mu 0 4 146 142 136 138
		f 4 -58 81 -60 82
		mu 0 4 36 38 56 54
		f 4 -55 -77 -49 83
		mu 0 4 44 46 64 62
		f 4 -79 -44 84 -71
		mu 0 4 160 166 158 156
		f 4 -39 85 -33 -82
		mu 0 4 38 40 58 56
		f 4 86 -32 -80 87
		mu 0 4 150 148 142 146
		f 4 -14 -24 88 89
		mu 0 4 162 116 117 163
		f 4 90 -20 -11 91
		mu 0 4 98 100 130 128
		f 4 -81 -12 -19 -64
		mu 0 4 138 136 130 134
		f 4 -3 92 -5 -76
		mu 0 4 48 50 68 66
		f 4 -83 93 -91 -59
		mu 0 4 72 74 100 98
		f 4 -2 94 95 96
		mu 0 4 50 32 33 51
		f 4 -42 97 -74 98
		mu 0 4 10 12 30 28
		f 4 -73 99 -70 100
		mu 0 4 86 88 114 112
		f 4 -61 101 -63 102
		mu 0 4 76 78 104 102
		f 4 -10 103 -57 -92
		mu 0 4 0 2 20 18
		f 4 104 -99 -53 105
		mu 0 4 8 10 28 26
		f 4 -51 -101 -45 106
		mu 0 4 84 86 112 110
		f 4 -65 107 -37 -104
		mu 0 4 2 4 22 20
		f 4 -35 108 -25 -102
		mu 0 4 78 80 106 104
		f 4 109 110 111 -95
		mu 0 4 32 14 15 33
		f 4 -62 -103 -17 -94
		mu 0 4 74 76 102 100
		f 4 -7 -21 -13 -100
		mu 0 4 88 90 116 114
		f 4 -68 -110 -1 -98
		mu 0 4 12 14 32 30
		f 4 112 -107 113 114
		mu 0 4 82 84 110 108
		f 4 115 -106 116 117
		mu 0 4 6 8 26 24
		f 4 -85 118 119 -47
		mu 0 4 156 158 152 124
		f 4 120 -84 121 122
		mu 0 4 42 44 62 60
		f 4 -117 -56 -121 123
		mu 0 4 24 26 44 42
		f 4 -122 -52 -113 124
		mu 0 4 60 62 84 82
		f 4 -114 -48 125 126
		mu 0 4 108 110 124 150
		f 4 -43 -105 127 -119
		mu 0 4 158 126 154 152
		f 4 128 -127 -88 -26
		mu 0 4 106 108 150 146
		f 4 129 -125 130 -34
		mu 0 4 58 60 82 80
		f 4 131 -124 132 -38
		mu 0 4 22 24 42 40
		f 4 -133 -123 -130 -86
		mu 0 4 40 42 60 58
		f 4 -30 -118 -132 -108
		mu 0 4 4 6 24 22
		f 4 -131 -115 -129 -109
		mu 0 4 80 82 108 106
		f 4 -120 133 -87 -126
		mu 0 4 124 152 148 150
		f 4 -128 -116 -29 -134
		mu 0 4 152 154 144 148
		f 4 134 -112 135 136
		mu 0 4 120 122 175 169
		f 4 137 -137 138 -89
		mu 0 4 117 120 169 163
		f 4 139 140 -138 -23
		mu 0 4 91 94 120 117
		f 4 141 -96 -135 -141
		mu 0 4 94 96 122 120
		f 4 -67 142 -136 -111
		mu 0 4 174 168 169 175
		f 4 -6 143 -140 -22
		mu 0 4 90 68 69 91
		f 4 -93 -97 -142 -144
		mu 0 4 68 50 51 69
		f 4 -78 -90 -139 -143
		mu 0 4 168 162 163 169
		f 4 144 145 146 147
		mu 0 4 31 49 52 34
		f 4 148 149 150 151
		mu 0 4 67 89 92 70
		f 4 152 153 154 155
		mu 0 4 137 131 129 133
		f 4 156 157 158 159
		mu 0 4 115 161 164 118
		f 4 160 161 162 163
		mu 0 4 101 131 135 103
		f 4 164 165 166 167
		mu 0 4 118 119 93 92
		f 4 168 169 170 171
		mu 0 4 105 139 147 107
		f 4 172 173 174 175
		mu 0 4 149 143 141 145
		f 4 176 177 178 179
		mu 0 4 57 79 81 59
		f 4 180 181 182 183
		mu 0 4 21 39 41 23
		f 4 184 185 186 187
		mu 0 4 167 159 127 173
		f 4 188 189 190 191
		mu 0 4 111 125 157 113
		f 4 192 193 194 195
		mu 0 4 63 85 87 65
		f 4 196 197 198 199
		mu 0 4 27 45 47 29
		f 4 200 201 -181 202
		mu 0 4 19 37 39 21
		f 4 203 204 -177 205
		mu 0 4 55 77 79 57
		f 4 -163 206 -169 207
		mu 0 4 103 135 139 105
		f 4 208 -156 209 -174
		mu 0 4 143 137 133 141
		f 4 210 -188 211 212
		mu 0 4 170 167 173 176
		f 4 -191 213 -157 214
		mu 0 4 113 157 161 115
		f 4 -195 215 -149 216
		mu 0 4 65 87 89 67
		f 4 -199 217 -145 218
		mu 0 4 29 47 49 31
		f 4 219 -217 220 -218
		mu 0 4 47 65 67 49
		f 4 -158 221 -211 222
		mu 0 4 164 161 167 170
		f 4 -170 223 -209 224
		mu 0 4 147 139 137 143
		f 4 225 -206 226 -202
		mu 0 4 37 55 57 39
		f 4 227 -196 -220 -198
		mu 0 4 45 63 65 47
		f 4 -214 228 -185 -222
		mu 0 4 161 157 159 167
		f 4 -227 -180 229 -182
		mu 0 4 39 57 59 41
		f 4 230 -225 -173 231
		mu 0 4 151 147 143 149
		f 4 232 233 -165 -159
		mu 0 4 164 165 119 118
		f 4 234 -154 -161 235
		mu 0 4 99 129 131 101
		f 4 -207 -162 -153 -224
		mu 0 4 139 135 131 137
		f 4 -221 -152 236 -146
		mu 0 4 49 67 70 52
		f 4 -201 -236 237 -226
		mu 0 4 73 99 101 75
		f 4 238 239 240 -147
		mu 0 4 52 53 35 34
		f 4 241 -219 242 -187
		mu 0 4 11 29 31 13
		f 4 243 -215 244 -216
		mu 0 4 87 113 115 89
		f 4 245 -208 246 -205
		mu 0 4 77 103 105 79
		f 4 -235 -203 247 -155
		mu 0 4 1 19 21 3
		f 4 248 -200 -242 249
		mu 0 4 9 27 29 11
		f 4 250 -192 -244 -194
		mu 0 4 85 111 113 87
		f 4 -248 -184 251 -210
		mu 0 4 3 21 23 5
		f 4 -247 -172 252 -178
		mu 0 4 79 105 107 81
		f 4 -241 253 254 255
		mu 0 4 34 35 17 16
		f 4 -238 -164 -246 -204
		mu 0 4 75 101 103 77
		f 4 -245 -160 -168 -150
		mu 0 4 89 115 118 92
		f 4 -243 -148 -256 -212
		mu 0 4 13 31 34 16
		f 4 256 257 -251 258
		mu 0 4 83 109 111 85
		f 4 259 260 -249 261
		mu 0 4 7 25 27 9
		f 4 -190 262 263 -229
		mu 0 4 157 125 153 159
		f 4 264 265 -228 266
		mu 0 4 43 61 63 45
		f 4 267 -267 -197 -261
		mu 0 4 25 43 45 27
		f 4 268 -259 -193 -266
		mu 0 4 61 83 85 63
		f 4 269 270 -189 -258
		mu 0 4 109 151 125 111
		f 4 -264 271 -250 -186
		mu 0 4 159 153 155 127
		f 4 -171 -231 -270 272
		mu 0 4 107 147 151 109
		f 4 -179 273 -269 274
		mu 0 4 59 81 83 61
		f 4 -183 275 -268 276
		mu 0 4 23 41 43 25
		f 4 -230 -275 -265 -276
		mu 0 4 41 59 61 43
		f 4 -252 -277 -260 -175
		mu 0 4 5 23 25 7
		f 4 -253 -273 -257 -274
		mu 0 4 81 107 109 83
		f 4 -271 -232 277 -263
		mu 0 4 125 151 149 153
		f 4 -278 -176 -262 -272
		mu 0 4 153 149 145 155
		f 4 278 279 -254 280
		mu 0 4 121 171 177 123
		f 4 -234 281 -279 282
		mu 0 4 119 165 171 121
		f 4 -166 -283 283 284
		mu 0 4 93 119 121 95
		f 4 -284 -281 -240 285
		mu 0 4 95 121 123 97
		f 4 -255 -280 286 -213
		mu 0 4 176 177 171 170
		f 4 -167 -285 287 -151
		mu 0 4 92 93 71 70
		f 4 -288 -286 -239 -237
		mu 0 4 70 71 53 52
		f 4 -287 -282 -233 -223
		mu 0 4 170 171 165 164;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "head" -p "mesh";
	rename -uid "4D4526E9-4A9E-552B-D7AD-188261002158";
	setAttr ".t" -type "double3" -0.33757352828979492 772.7559814453125 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" -90.00000933466734 0 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 100 100 100 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "headShape" -p "|mesh|head";
	rename -uid "69C175EA-4453-4A89-9E6F-FF9BB4035C45";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "headShapeOrig" -p "|mesh|head";
	rename -uid "7130E85F-4DB8-EDD4-E18C-5FB5F56E9E2B";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 80 ".uvst[0].uvsp[0:79]" -type "float2" 0.375 0.12499999
		 0.4375 0.12499999 0.49999997 0.12499999 0.5625 0.125 0.625 0.125 0.375 0.1875 0.375
		 0.1875 0.4375 0.1875 0.4375 0.1875 0.49999997 0.1875 0.49999997 0.1875 0.5625 0.1875
		 0.5625 0.1875 0.625 0.1875 0.625 0.1875 0.375 0.25 0.375 0.25 0.4375 0.25 0.4375
		 0.25 0.49999997 0.25 0.49999997 0.25 0.5625 0.25 0.5625 0.25 0.625 0.25 0.625 0.25
		 0.375 0.375 0.375 0.375 0.4375 0.375 0.4375 0.375 0.49999997 0.375 0.49999997 0.375
		 0.5625 0.375 0.5625 0.375 0.625 0.375 0.625 0.375 0.125 0.5 0.125 0.5 0.25 0.5 0.25
		 0.5 0.375 0.5 0.375 0.5 0.4375 0.5 0.4375 0.5 0.49999997 0.5 0.49999997 0.5 0.5625
		 0.5 0.5625 0.5 0.625 0.5 0.625 0.5 0.75 0.5 0.75 0.5 0.875 0.5 0.875 0.5 0.125 0.5625
		 0.125 0.5625 0.25 0.5625 0.25 0.5625 0.375 0.5625 0.375 0.5625 0.4375 0.5625 0.4375
		 0.5625 0.49999997 0.5625 0.49999997 0.5625 0.5625 0.5625 0.5625 0.5625 0.625 0.5625
		 0.625 0.5625 0.75 0.5625 0.75 0.5625 0.875 0.5625 0.875 0.5625 0.125 0.625 0.25 0.625
		 0.375 0.625 0.4375 0.625 0.49999997 0.625 0.5625 0.625 0.625 0.625 0.75 0.625 0.875
		 0.625;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 66 ".vt[0:65]"  -0.35178694 0.30226782 0.02318716 -0.51900649 0.29314673 1.094493866
		 0.18544939 0.30226782 0.025087357 0.18544939 0.34255266 1.094493866 -0.39907065 0.1277079 -0.03153944
		 -0.39907065 0 -0.039140224 -0.52068472 0 1.20850754 -0.52068472 0.11858682 1.20850754
		 0.18544939 0 -0.037240028 0.18544939 0.1277079 -0.029639244 0.18544939 0.16799274 1.20850754
		 0.18544939 0 1.20850754 -0.6535275 0.31138891 0.85298538 -0.6535275 0.31138891 0.52026606
		 -0.65582287 0.25286189 0.18298626 0.18544939 0.36079484 0.85298538 0.18544939 0.36079484 0.52026606
		 0.18544939 0.30226782 0.18298626 0.18544939 0.18623492 0.85298538 0.18544939 0.18623492 0.52026606
		 0.18544939 0.1277079 0.12825966 0.18544939 0 0.85298538 0.18544939 0 0.52026606 0.18544939 0 0.12065887
		 -0.72210884 0 0.85298538 -0.72210884 0 0.52026606 -0.70310658 0 0.12065887 -0.72210884 0.13682899 0.85298538
		 -0.72210884 0.13682899 0.52026606 -0.70310658 0.078301974 0.12825966 -0.10735127 0.33647192 0.024137259
		 -0.24468786 0.33875221 1.094493866 -0.26832974 0.20219684 1.20850754 -0.26832974 0 1.20850754
		 -0.10681063 0.16191199 -0.030589342 -0.10681063 0 -0.038190126 -0.24468786 0.33647192 0.18298626
		 -0.24468786 0.39499894 0.52026606 -0.24468786 0.39499894 0.85298538 -0.35178694 -0.30226782 0.02318716
		 -0.51900649 -0.29314673 1.094493866 0.18544939 -0.30226782 0.025087357 0.18544939 -0.34255266 1.094493866
		 -0.39907065 -0.1277079 -0.03153944 -0.52068472 -0.11858682 1.20850754 0.18544939 -0.1277079 -0.029639244
		 0.18544939 -0.16799274 1.20850754 -0.6535275 -0.31138891 0.85298538 -0.6535275 -0.31138891 0.52026606
		 -0.65582287 -0.25286189 0.18298626 0.18544939 -0.36079484 0.85298538 0.18544939 -0.36079484 0.52026606
		 0.18544939 -0.30226782 0.18298626 0.18544939 -0.18623492 0.85298538 0.18544939 -0.18623492 0.52026606
		 0.18544939 -0.1277079 0.12825966 -0.72210884 -0.13682899 0.85298538 -0.72210884 -0.13682899 0.52026606
		 -0.70310658 -0.078301974 0.12825966 -0.10735127 -0.33647192 0.024137259 -0.24468786 -0.33875221 1.094493866
		 -0.26832974 -0.20219684 1.20850754 -0.10681063 -0.16191199 -0.030589342 -0.24468786 -0.33647192 0.18298626
		 -0.24468786 -0.39499894 0.52026606 -0.24468786 -0.39499894 0.85298538;
	setAttr -s 128 ".ed[0:127]"  27 7 0 7 1 0 1 12 0 12 27 0 38 31 0 31 3 0
		 3 15 0 15 38 0 31 1 0 7 32 0 32 31 0 7 6 0 6 33 0 33 32 0 30 2 0 2 9 0 9 34 0 34 30 0
		 9 8 0 8 35 0 35 34 0 3 10 0 10 18 0 18 15 0 10 11 0 11 21 0 21 18 0 24 6 0 27 24 0
		 5 26 0 26 29 0 29 4 0 4 5 0 26 25 0 25 28 0 28 29 0 25 24 0 27 28 0 9 20 0 20 23 0
		 23 8 0 20 19 0 19 22 0 22 23 0 19 18 0 21 22 0 2 17 0 17 20 0 17 16 0 16 19 0 16 15 0
		 30 36 0 36 17 0 36 37 0 37 16 0 37 38 0 29 14 0 14 0 0 0 4 0 28 13 0 13 14 0 12 13 0
		 12 38 0 37 13 0 36 14 0 30 0 0 4 34 0 35 5 0 10 32 0 33 11 0 56 47 0 47 40 0 40 44 0
		 44 56 0 65 50 0 50 42 0 42 60 0 60 65 0 60 61 0 61 44 0 40 60 0 61 33 0 6 44 0 59 62 0
		 62 45 0 45 41 0 41 59 0 62 35 0 8 45 0 50 53 0 53 46 0 46 42 0 53 21 0 11 46 0 24 56 0
		 5 43 0 43 58 0 58 26 0 58 57 0 57 25 0 57 56 0 23 55 0 55 45 0 22 54 0 54 55 0 53 54 0
		 55 52 0 52 41 0 54 51 0 51 52 0 50 51 0 52 63 0 63 59 0 51 64 0 64 63 0 65 64 0 43 39 0
		 39 49 0 49 58 0 49 48 0 48 57 0 48 47 0 48 64 0 65 47 0 49 63 0 39 59 0 62 43 0 61 46 0;
	setAttr -s 256 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.81689215 0.32091546 0.47927079 -0.81689215
		 0.32091546 0.47927079 -0.81689215 0.32091546 0.47927079 -0.81689215 0.32091546 0.47927079
		 0.034907598 0.98771447 0.15232126 0.034907598 0.98771447 0.15232126 0.034907598 0.98771447
		 0.15232126 0.034907598 0.98771447 0.15232126 -0.14537619 0.5925442 0.79231125 -0.14537619
		 0.5925442 0.79231125 -0.14537619 0.5925442 0.79231125 -0.14537619 0.5925442 0.79231125
		 3.6815103e-07 0 1 3.6815103e-07 0 1 3.6815103e-07 0 1 3.6815103e-07 0 1 0.038056381
		 0.29899141 -0.95349669 0.038056381 0.29899141 -0.95349669 0.038056381 0.29899141
		 -0.95349669 0.038056381 0.29899141 -0.95349669 0.0063134669 0.052414797 -0.99860543
		 0.0063134669 0.052414797 -0.99860543 0.0063134669 0.052414797 -0.99860543 0.0063134669
		 0.052414797 -0.99860543 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 -0.87006259
		 0 0.49294141 -0.87006259 0 0.49294141 -0.87006259 0 0.49294141 -0.87006259 0 0.49294141
		 -0.46839502 0.065018483 -0.8811236 -0.46839502 0.065018483 -0.8811236 -0.46839502
		 0.065018483 -0.8811236 -0.46839502 0.065018483 -0.8811236 -0.99884212 0.0016985568
		 -0.048078917 -0.99884212 0.0016985568 -0.048078917 -0.99884212 0.0016985568 -0.048078917
		 -0.99884212 0.0016985568 -0.048078917 -1 0 0 -1 0 0 -1 0 0 -1 0 0 1 0 0 1 0 0 1 0
		 0 1 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0
		 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0
		 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 0.094072938 0.9947294 0.040788434 0.094072938
		 0.9947294 0.040788434 0.094072938 0.9947294 0.040788434 0.094072938 0.9947294 0.040788434
		 0.078108817 0.98226571 -0.17044932 0.078108817 0.98226571 -0.17044932 0.078108817
		 0.98226571 -0.17044932 0.078108817 0.98226571 -0.17044932 0.079268821 0.99685329
		 0 0.079268821 0.99685329 0 0.079268821 0.99685329 0 0.079268821 0.99685329 0 -0.47810924
		 0.37805089 -0.79277301 -0.47810924 0.37805089 -0.79277301 -0.47810924 0.37805089
		 -0.79277301 -0.47810924 0.37805089 -0.79277301 -0.9430061 0.32451406 -0.073688976
		 -0.9430061 0.32451406 -0.073688976 -0.9430061 0.32451406 -0.073688976 -0.9430061
		 0.32451406 -0.073688976 -0.93074381 0.36567193 0 -0.93074381 0.36567193 0 -0.93074381
		 0.36567193 0 -0.93074381 0.36567193 0 -0.20035884 0.9797225 0 -0.20035884 0.9797225
		 0 -0.20035884 0.9797225 0 -0.20035884 0.9797225 0 -0.19701532 0.96607769 -0.16696987
		 -0.19701532 0.96607769 -0.16696987 -0.19701532 0.96607769 -0.16696987 -0.19701532
		 0.96607769 -0.16696987 -0.17600252 0.98009753 -0.091825642 -0.17600252 0.98009753
		 -0.091825642 -0.17600252 0.98009753 -0.091825642 -0.17600252 0.98009753 -0.091825642
		 0.0001792086 0.05241584 -0.99862528 0.0001792086 0.05241584 -0.99862528 0.0001792086
		 0.05241584 -0.99862528 0.0001792086 0.05241584 -0.99862528 -0.035283394 0.30326653
		 -0.95225233 -0.035283394 0.30326653 -0.95225233 -0.035283394 0.30326653 -0.95225233
		 -0.035283394 0.30326653 -0.95225233 -1.7741135e-07 0 1 -1.7741135e-07 0 1 -1.7741135e-07
		 0 1 -1.7741135e-07 0 1 0.020294888 0.59002936 0.8071267 0.020294888 0.59002936 0.8071267
		 0.020294888 0.59002936 0.8071267 0.020294888 0.59002936 0.8071267 -0.18212388 0.96288353
		 0.19921386 -0.18212388 0.96288353 0.19921386 -0.18212388 0.96288353 0.19921386 -0.18212388
		 0.96288353 0.19921386 -0.81689221 -0.3209154 0.47927079 -0.81689221 -0.3209154 0.47927079
		 -0.81689221 -0.3209154 0.47927079 -0.81689221 -0.3209154 0.47927079 0.034907602 -0.98771447
		 0.15232126 0.034907602 -0.98771447 0.15232126 0.034907602 -0.98771447 0.15232126
		 0.034907602 -0.98771447 0.15232126 -0.14537604 -0.5925442 0.79231125 -0.14537604
		 -0.5925442 0.79231125 -0.14537604 -0.5925442 0.79231125 -0.14537604 -0.5925442 0.79231125
		 3.6815103e-07 0 1 3.6815103e-07 0 1 3.6815103e-07 0 1 3.6815103e-07 0 1 0.038056381
		 -0.29899141 -0.95349669 0.038056381 -0.29899141 -0.95349669 0.038056381 -0.29899141
		 -0.95349669 0.038056381 -0.29899141 -0.95349669 0.0063134669 -0.052414797 -0.99860543
		 0.0063134669 -0.052414797 -0.99860543 0.0063134669 -0.052414797 -0.99860543 0.0063134669
		 -0.052414797 -0.99860543 1 3.6473189e-08 0 1 3.6473189e-08 0 1 3.6473189e-08 0 1
		 3.6473189e-08 0 1 0 0 1 0 0 1 0 0 1 0 0 -0.87006259 0 0.49294141 -0.87006259 0 0.49294141
		 -0.87006259 0 0.49294141 -0.87006259 0 0.49294141 -0.46839496 -0.06501852 -0.8811236
		 -0.46839496 -0.06501852 -0.8811236;
	setAttr ".n[166:255]" -type "float3"  -0.46839496 -0.06501852 -0.8811236 -0.46839496
		 -0.06501852 -0.8811236 -0.99884212 -0.0016985569 -0.048078921 -0.99884212 -0.0016985569
		 -0.048078921 -0.99884212 -0.0016985569 -0.048078921 -0.99884212 -0.0016985569 -0.048078921
		 -1 0 0 -1 0 0 -1 0 0 -1 0 0 1 -1.7319467e-08 0 1 -1.7319467e-08 0 1 -1.7319467e-08
		 0 1 -1.7319467e-08 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0
		 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 1 0 0 1 0 0 1 0 0 1 0
		 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 0.094072938 -0.9947294 0.040788434
		 0.094072938 -0.9947294 0.040788434 0.094072938 -0.9947294 0.040788434 0.094072938
		 -0.9947294 0.040788434 0.078108817 -0.98226571 -0.17044932 0.078108817 -0.98226571
		 -0.17044932 0.078108817 -0.98226571 -0.17044932 0.078108817 -0.98226571 -0.17044932
		 0.079268821 -0.99685329 0 0.079268821 -0.99685329 0 0.079268821 -0.99685329 0 0.079268821
		 -0.99685329 0 -0.47810924 -0.37805095 -0.79277301 -0.47810924 -0.37805095 -0.79277301
		 -0.47810924 -0.37805095 -0.79277301 -0.47810924 -0.37805095 -0.79277301 -0.94300616
		 -0.32451406 -0.073688969 -0.94300616 -0.32451406 -0.073688969 -0.94300616 -0.32451406
		 -0.073688969 -0.94300616 -0.32451406 -0.073688969 -0.93074381 -0.36567193 0 -0.93074381
		 -0.36567193 0 -0.93074381 -0.36567193 0 -0.93074381 -0.36567193 0 -0.20035884 -0.9797225
		 0 -0.20035884 -0.9797225 0 -0.20035884 -0.9797225 0 -0.20035884 -0.9797225 0 -0.1970153
		 -0.96607769 -0.16696987 -0.1970153 -0.96607769 -0.16696987 -0.1970153 -0.96607769
		 -0.16696987 -0.1970153 -0.96607769 -0.16696987 -0.17600252 -0.98009753 -0.091825642
		 -0.17600252 -0.98009753 -0.091825642 -0.17600252 -0.98009753 -0.091825642 -0.17600252
		 -0.98009753 -0.091825642 0.0001792086 -0.05241584 -0.99862528 0.0001792086 -0.05241584
		 -0.99862528 0.0001792086 -0.05241584 -0.99862528 0.0001792086 -0.05241584 -0.99862528
		 -0.035283383 -0.30326647 -0.95225227 -0.035283383 -0.30326647 -0.95225227 -0.035283383
		 -0.30326647 -0.95225227 -0.035283383 -0.30326647 -0.95225227 -1.7741135e-07 0 1 -1.7741135e-07
		 0 1 -1.7741135e-07 0 1 -1.7741135e-07 0 1 0.020294931 -0.5900293 0.8071267 0.020294931
		 -0.5900293 0.8071267 0.020294931 -0.5900293 0.8071267 0.020294931 -0.5900293 0.8071267
		 -0.18212388 -0.96288353 0.19921386 -0.18212388 -0.96288353 0.19921386 -0.18212388
		 -0.96288353 0.19921386 -0.18212388 -0.96288353 0.19921386;
	setAttr -s 64 -ch 256 ".fc[0:63]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 11 13 23 21
		f 4 4 5 6 7
		mu 0 4 31 33 47 45
		f 4 8 -2 9 10
		mu 0 4 49 51 69 67
		f 4 -10 11 12 13
		mu 0 4 67 69 79 78
		f 4 14 15 16 17
		mu 0 4 37 39 57 55
		f 4 -17 18 19 20
		mu 0 4 55 57 73 72
		f 4 -7 21 22 23
		mu 0 4 45 47 65 63
		f 4 -23 24 25 26
		mu 0 4 63 65 77 76
		f 4 27 -12 -1 28
		mu 0 4 3 4 13 11
		f 4 29 30 31 32
		mu 0 4 0 1 7 5
		f 4 33 34 35 -31
		mu 0 4 1 2 9 7
		f 4 36 -29 37 -35
		mu 0 4 2 3 11 9
		f 4 38 39 40 -19
		mu 0 4 57 59 74 73
		f 4 41 42 43 -40
		mu 0 4 59 61 75 74
		f 4 44 -27 45 -43
		mu 0 4 61 63 76 75
		f 4 46 47 -39 -16
		mu 0 4 39 41 59 57
		f 4 48 49 -42 -48
		mu 0 4 41 43 61 59
		f 4 50 -24 -45 -50
		mu 0 4 43 45 63 61
		f 4 51 52 -47 -15
		mu 0 4 25 27 41 39
		f 4 53 54 -49 -53
		mu 0 4 27 29 43 41
		f 4 55 -8 -51 -55
		mu 0 4 29 31 45 43
		f 4 -32 56 57 58
		mu 0 4 5 7 17 15
		f 4 -36 59 60 -57
		mu 0 4 7 9 19 17
		f 4 -38 -4 61 -60
		mu 0 4 9 11 21 19
		f 4 -62 62 -56 63
		mu 0 4 19 21 31 29
		f 4 -61 -64 -54 64
		mu 0 4 17 19 29 27
		f 4 -58 -65 -52 65
		mu 0 4 15 17 27 25
		f 4 66 -21 67 -33
		mu 0 4 53 55 72 71
		f 4 -66 -18 -67 -59
		mu 0 4 35 37 55 53
		f 4 68 -14 69 -25
		mu 0 4 65 67 78 77
		f 4 -6 -11 -69 -22
		mu 0 4 47 49 67 65
		f 4 -3 -9 -5 -63
		mu 0 4 21 23 33 31
		f 4 70 71 72 73
		mu 0 4 12 22 24 14
		f 4 74 75 76 77
		mu 0 4 32 46 48 34
		f 4 78 79 -73 80
		mu 0 4 50 68 70 52
		f 4 81 -13 82 -80
		mu 0 4 68 78 79 70
		f 4 83 84 85 86
		mu 0 4 38 56 58 40
		f 4 87 -20 88 -85
		mu 0 4 56 72 73 58
		f 4 89 90 91 -76
		mu 0 4 46 64 66 48
		f 4 92 -26 93 -91
		mu 0 4 64 76 77 66
		f 4 94 -74 -83 -28
		mu 0 4 3 12 14 4
		f 4 95 96 97 -30
		mu 0 4 0 6 8 1
		f 4 -98 98 99 -34
		mu 0 4 1 8 10 2
		f 4 -100 100 -95 -37
		mu 0 4 2 10 12 3
		f 4 -89 -41 101 102
		mu 0 4 58 73 74 60
		f 4 -102 -44 103 104
		mu 0 4 60 74 75 62
		f 4 -104 -46 -93 105
		mu 0 4 62 75 76 64
		f 4 -86 -103 106 107
		mu 0 4 40 58 60 42
		f 4 -107 -105 108 109
		mu 0 4 42 60 62 44
		f 4 -109 -106 -90 110
		mu 0 4 44 62 64 46
		f 4 -87 -108 111 112
		mu 0 4 26 40 42 28
		f 4 -112 -110 113 114
		mu 0 4 28 42 44 30
		f 4 -114 -111 -75 115
		mu 0 4 30 44 46 32
		f 4 116 117 118 -97
		mu 0 4 6 16 18 8
		f 4 -119 119 120 -99
		mu 0 4 8 18 20 10
		f 4 -121 121 -71 -101
		mu 0 4 10 20 22 12
		f 4 122 -116 123 -122
		mu 0 4 20 30 32 22
		f 4 124 -115 -123 -120
		mu 0 4 18 28 30 20
		f 4 125 -113 -125 -118
		mu 0 4 16 26 28 18
		f 4 -96 -68 -88 126
		mu 0 4 54 71 72 56
		f 4 -117 -127 -84 -126
		mu 0 4 36 54 56 38
		f 4 -94 -70 -82 127
		mu 0 4 66 77 78 68
		f 4 -92 -128 -79 -77
		mu 0 4 48 66 68 50
		f 4 -124 -78 -81 -72
		mu 0 4 22 32 34 24;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "feet" -p "mesh";
	rename -uid "5B0A1D22-4E92-F45C-8F26-2B8BA2B35DDE";
	setAttr ".t" -type "double3" 0 33.957920074462891 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" -90.00000933466734 0 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 100 43.915473937988281 100 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "feetShape" -p "feet";
	rename -uid "2FF4E29A-4469-F0B8-158C-DFB36AC8BF58";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "feetShapeOrig" -p "feet";
	rename -uid "C55A2FC2-4204-4509-F08E-828FA973FEC3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 508 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.375 0.1875 0.375 0.1875 0.5
		 0.1875 0.5 0.1875 0.5625 0.1875 0.5625 0.1875 0.625 0.1875 0.625 0.1875 0.375 0.20833333
		 0.375 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5625 0.20833333 0.5625 0.20833333
		 0.625 0.20833333 0.625 0.20833333 0.375 0.22916666 0.375 0.22916666 0.5 0.22916666
		 0.5 0.22916666 0.5625 0.22916666 0.5625 0.22916666 0.625 0.22916666 0.625 0.22916666
		 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375
		 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25
		 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5 0.25 0.5625 0.25
		 0.5625 0.25 0.5625 0.25 0.5625 0.25 0.5625 0.25 0.5625 0.25 0.5625 0.25 0.5625 0.25
		 0.5625 0.25 0.5625 0.25 0.5625 0.25 0.5625 0.25 0.625 0.25 0.625 0.25 0.625 0.25
		 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625
		 0.25 0.625 0.25 0.375 0.28125 0.375 0.28125 0.5 0.28125 0.5 0.28125 0.5625 0.28125
		 0.5625 0.28125 0.625 0.28125 0.625 0.28125 0.375 0.3125 0.375 0.3125 0.5 0.3125 0.5
		 0.3125 0.5625 0.3125 0.5625 0.3125 0.625 0.3125 0.625 0.3125 0.375 0.34375 0.375
		 0.34375 0.5 0.34375 0.5 0.34375 0.5625 0.34375 0.5625 0.34375 0.625 0.34375 0.625
		 0.34375 0.375 0.375 0.375 0.375 0.5 0.375 0.5 0.375 0.5625 0.375 0.5625 0.375 0.625
		 0.375 0.625 0.375 0.375 0.40625 0.375 0.40625 0.5 0.40625 0.5 0.40625 0.5625 0.40625
		 0.5625 0.40625 0.625 0.40625 0.625 0.40625 0.375 0.4375 0.375 0.4375 0.5 0.4375 0.5
		 0.4375 0.5625 0.4375 0.5625 0.4375 0.625 0.4375 0.625 0.4375 0.375 0.46875 0.375
		 0.46875 0.5 0.46875 0.5 0.46875 0.5625 0.46875 0.5625 0.46875 0.625 0.46875 0.625
		 0.46875 0.375 0.484375 0.375 0.484375 0.5 0.484375 0.5 0.484375 0.5625 0.484375 0.5625
		 0.484375 0.625 0.484375 0.625 0.484375 0.375 0.4921875 0.375 0.4921875 0.5 0.4921875
		 0.5 0.4921875 0.5625 0.4921875 0.5625 0.4921875 0.625 0.4921875 0.625 0.4921875 0.125
		 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5
		 0.125 0.5 0.125 0.5 0.125 0.5 0.15625 0.5 0.15625 0.5 0.1875 0.5 0.1875 0.5 0.21875
		 0.5 0.21875 0.5 0.24999999 0.5 0.24999999 0.5 0.28125 0.5 0.28125 0.5 0.3125 0.5
		 0.3125 0.5 0.34375 0.5 0.34375 0.5 0.359375 0.5 0.359375 0.5 0.3671875 0.5 0.3671875
		 0.5 0.375 0.5 0.375 0.5 0.5 0.5 0.5 0.5 0.5625 0.5 0.5625 0.5 0.625 0.5 0.625 0.5
		 0.6328125 0.5 0.6328125 0.5 0.640625 0.5 0.640625 0.5 0.65625 0.5 0.65625 0.5 0.6875
		 0.5 0.6875 0.5 0.71875 0.5 0.71875 0.5 0.75 0.5 0.75 0.5 0.78125 0.5 0.78125 0.5
		 0.8125 0.5 0.8125 0.5 0.84375 0.5 0.84375 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875
		 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5
		 0.125 0.52083331 0.125 0.52083331 0.125 0.52083331 0.125 0.52083331 0.125 0.52083331
		 0.125 0.52083331 0.125 0.52083331 0.125 0.52083331 0.125 0.52083331 0.125 0.52083331
		 0.125 0.52083331 0.125 0.52083331 0.15625 0.52083331 0.15625 0.52083331 0.1875 0.52083331
		 0.1875 0.52083331 0.21875 0.52083331 0.21875 0.52083331 0.24999997 0.52083331 0.24999997
		 0.52083331 0.28125 0.52083331 0.28125 0.52083331 0.3125 0.52083331 0.3125 0.52083331
		 0.34375 0.52083331 0.34375 0.52083331 0.359375 0.52083331 0.359375 0.52083331 0.3671875
		 0.52083331 0.3671875 0.52083331 0.375 0.52083331 0.375 0.52083331 0.5 0.52083331
		 0.5 0.52083331 0.5 0.52083331 0.5 0.52083331 0.5 0.52083331 0.5 0.52083331;
	setAttr ".uvst[0].uvsp[250:499]" 0.5 0.52083331 0.5 0.52083331 0.5 0.52083331
		 0.5 0.52083331 0.5625 0.52083331 0.5625 0.52083331 0.625 0.52083331 0.625 0.52083331
		 0.6328125 0.52083331 0.6328125 0.52083331 0.640625 0.52083331 0.640625 0.52083331
		 0.65625 0.52083331 0.65625 0.52083331 0.6875 0.52083331 0.6875 0.52083331 0.6875
		 0.52083331 0.6875 0.52083331 0.6875 0.52083331 0.6875 0.52083331 0.6875 0.52083331
		 0.6875 0.52083331 0.6875 0.52083331 0.6875 0.52083331 0.71875 0.52083331 0.71875
		 0.52083331 0.75 0.52083331 0.75 0.52083331 0.78125 0.52083331 0.78125 0.52083331
		 0.8125 0.52083331 0.8125 0.52083331 0.84374994 0.52083331 0.84374994 0.52083331 0.875
		 0.52083331 0.875 0.52083331 0.875 0.52083331 0.875 0.52083331 0.875 0.52083331 0.875
		 0.52083331 0.875 0.52083331 0.875 0.52083331 0.875 0.52083331 0.875 0.52083331 0.875
		 0.52083331 0.875 0.52083331 0.125 0.54166663 0.125 0.54166663 0.125 0.54166663 0.125
		 0.54166663 0.125 0.54166663 0.125 0.54166663 0.125 0.54166663 0.125 0.54166663 0.125
		 0.54166663 0.125 0.54166663 0.125 0.54166663 0.125 0.54166663 0.15625 0.54166663
		 0.15625 0.54166663 0.1875 0.54166663 0.1875 0.54166663 0.21875 0.54166663 0.21875
		 0.54166663 0.24999997 0.54166663 0.24999997 0.54166663 0.28125 0.54166663 0.28125
		 0.54166663 0.3125 0.54166663 0.3125 0.54166663 0.34375 0.54166663 0.34375 0.54166663
		 0.359375 0.54166663 0.359375 0.54166663 0.3671875 0.54166663 0.3671875 0.54166663
		 0.375 0.54166663 0.375 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5
		 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663
		 0.5 0.54166663 0.5625 0.54166663 0.5625 0.54166663 0.625 0.54166663 0.625 0.54166663
		 0.6328125 0.54166663 0.6328125 0.54166663 0.640625 0.54166663 0.640625 0.54166663
		 0.65625 0.54166663 0.65625 0.54166663 0.6875 0.54166663 0.6875 0.54166663 0.6875
		 0.54166663 0.6875 0.54166663 0.6875 0.54166663 0.6875 0.54166663 0.6875 0.54166663
		 0.6875 0.54166663 0.6875 0.54166663 0.6875 0.54166663 0.71875 0.54166663 0.71875
		 0.54166663 0.75 0.54166663 0.75 0.54166663 0.78125 0.54166663 0.78125 0.54166663
		 0.8125 0.54166663 0.8125 0.54166663 0.84375 0.54166663 0.84375 0.54166663 0.875 0.54166663
		 0.875 0.54166663 0.875 0.54166663 0.875 0.54166663 0.875 0.54166663 0.875 0.54166663
		 0.875 0.54166663 0.875 0.54166663 0.875 0.54166663 0.875 0.54166663 0.875 0.54166663
		 0.875 0.54166663 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125 0.5625
		 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125
		 0.5625 0.15625 0.5625 0.15625 0.5625 0.1875 0.5625 0.1875 0.5625 0.21875 0.5625 0.21875
		 0.5625 0.24999999 0.5625 0.24999999 0.5625 0.28125 0.5625 0.28125 0.5625 0.3125 0.5625
		 0.3125 0.5625 0.34375 0.5625 0.34375 0.5625 0.359375 0.5625 0.359375 0.5625 0.3671875
		 0.5625 0.3671875 0.5625 0.375 0.5625 0.375 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625
		 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5
		 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625
		 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5
		 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5 0.5625 0.5625 0.5625
		 0.5625 0.5625 0.56640625 0.5625 0.56640625 0.5625 0.5703125 0.5625 0.5703125 0.5625
		 0.578125 0.5625 0.578125 0.5625 0.59375 0.5625 0.59375 0.5625 0.609375 0.5625 0.609375
		 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.6328125 0.5625 0.6328125
		 0.5625 0.640625 0.5625 0.640625 0.5625 0.640625 0.5625 0.640625 0.5625 0.65625 0.5625
		 0.65625 0.5625 0.65625 0.5625 0.65625 0.5625 0.671875 0.5625 0.671875 0.5625 0.6875
		 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875
		 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875 0.5625 0.6875
		 0.5625 0.6875 0.5625 0.71875 0.5625 0.71875 0.5625 0.75 0.5625 0.75 0.5625 0.78125
		 0.5625 0.78125 0.5625 0.8125 0.5625 0.8125 0.5625 0.84375 0.5625 0.84375 0.5625 0.875
		 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625;
	setAttr ".uvst[0].uvsp[500:507]" 0.875 0.5625 0.875 0.5625 0.875 0.5625 0.875
		 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 424 ".vt";
	setAttr ".vt[0:165]"  -0.75727785 0.96230012 -0.30016381 -0.75727785 0.93115067 -0.14389275
		 0.41272557 1.065325975 -0.30016381 0.18990588 1.024077535 0.20087162 -0.75727785 0.63645154 -0.30016381
		 -0.75727785 0.6181156 -0.14389275 0.41272557 0.56352729 -0.30016381 0.18990588 0.52518868 0.20087162
		 0.12022477 1.01322484 -0.14789285 -0.12246937 0.99476796 -0.17375019 -0.42053056 0.96230012 -0.21841748
		 -0.42053062 0.93115067 -0.10079719 -0.12246937 0.9672308 0.12042663 0.12022477 0.98774117 0.20087162
		 -0.42053062 0.6181156 -0.10079719 -0.12246937 0.58203554 0.12042663 0.12022477 0.56152493 0.20087162
		 0.12022477 0.5855267 -0.14789285 -0.12246937 0.60398382 -0.17375019 -0.42053056 0.63645154 -0.21841748
		 -0.75727785 1.097271919 -0.2277744 0.3534351 1.20570612 -0.11925504 0.3534351 0.42742217 -0.11925504
		 -0.75727785 0.47405612 -0.2277744 -0.42053056 0.47405612 -0.18467881 -0.12246937 0.42281014 -0.04964608
		 0.12022477 0.39367807 -0.04964608 -0.42053056 1.097271919 -0.18467884 -0.12246937 1.14851809 -0.049646109
		 0.12022477 1.17764974 -0.049646109 0.1550653 1.024077535 0.20087162 0.1550653 0.52518868 0.20087162
		 0.23501831 1.045923233 -0.11054337 0.23501831 0.55282843 -0.11054337 0.21732378 0.3420682 -0.049646094
		 0.21732378 1.22925973 -0.049646109 -0.026025683 1.01322484 -0.30016381 -0.026025683 0.98774117 0.17788735
		 -0.026025683 0.56152493 0.17788735 -0.026025683 0.5855267 -0.30016381 -0.026025712 1.17764974 -0.049646109
		 -0.026025712 0.39367807 -0.04964608 -0.31852651 0.96810389 -0.30016381 -0.31852651 0.93760014 -0.043336451
		 -0.31852651 0.61166614 -0.043336451 -0.31852651 0.63064784 -0.30016381 -0.31852651 1.10643232 -0.1272181
		 -0.31852651 0.46489567 -0.12721807 -0.61102736 0.96230012 -0.30016381 -0.61102736 0.63645154 -0.30016381
		 -0.61102742 0.93115067 -0.14389275 -0.61102742 0.6181156 -0.14389275 -0.61102736 1.097271919 -0.2277744
		 -0.61102736 0.47405612 -0.22777438 0.17248559 1.024077535 0.20087162 0.17248559 0.52518868 0.20087162
		 0.26587325 1.22925973 -0.049646109 0.30639589 1.045923233 -0.15028706 0.30639589 0.55282843 -0.15028706
		 0.26587325 0.3420682 -0.049646102 -1.021702528 1.045923233 -0.14669082 -1.021702528 1.024077535 -0.073088847
		 -1.021702528 0.55282843 -0.14669082 -1.021702528 0.52518868 -0.073088847 -1.021702528 1.22925973 -0.11259619
		 -1.021702528 0.3420682 -0.11259619 -1.28758359 1.088357687 -0.21365815 -1.28758359 1.071233153 -0.14005619
		 -1.28758359 0.51039404 -0.21365815 -1.28758359 0.4780331 -0.14005619 -1.28758359 1.25751936 -0.17956352
		 -1.28758359 0.34528118 -0.17956352 -1.13863945 1.073472738 -0.073088847 -1.13863945 0.47579342 -0.073088847
		 -1.13863945 0.27191007 -0.11259619 -1.13863945 0.50837857 -0.14669082 -1.13863945 1.090373039 -0.14669082
		 -1.13863945 1.29941773 -0.11259619 -1.41746378 1.11317992 -0.33489209 -1.41746378 1.09881711 -0.26129013
		 -1.41746378 0.48557171 -0.33489209 -1.41746378 0.45044914 -0.26129013 -1.41746378 1.29669797 -0.30079746
		 -1.41746378 0.30610254 -0.30079746 -0.75727785 0.7388162 -0.14389275 -0.75727785 0.85951686 -0.14389275
		 0.41272557 0.6078499 -0.30016381 0.41272557 0.99028105 -0.30016381 -0.75727785 0.85951686 -0.30016381
		 -0.75727785 0.7388162 -0.30016381 0.18990588 0.89049238 0.20087162 0.18990588 0.7078405 0.20087162
		 -0.42053056 0.85951686 -0.21841748 -0.42053056 0.7388162 -0.21841748 -0.12246937 0.87154353 -0.17375019
		 -0.12246937 0.72678953 -0.17375019 0.12022477 0.8783803 -0.14789285 0.12022477 0.71995264 -0.14789285
		 0.12022477 0.8783803 0.20087162 0.12022477 0.71995264 0.20087162 -0.12246937 0.87154353 0.12042663
		 -0.12246937 0.72678953 0.12042663 -0.42053062 0.85951686 -0.10079719 -0.42053062 0.7388162 -0.10079719
		 0.3534351 0.9462781 -0.11925504 0.3534351 0.68685007 -0.11925504 0.1550653 0.89049238 0.20087162
		 0.1550653 0.7078405 0.20087162 0.23501831 0.89049238 -0.11054337 0.23501831 0.7078405 -0.11054337
		 -0.026025683 0.8783803 0.17788735 -0.026025683 0.71995264 0.17788735 -0.026025683 0.8783803 -0.30016381
		 -0.026025683 0.71995264 -0.30016381 -0.31852651 0.86166668 -0.043336451 -0.31852651 0.73666638 -0.043336451
		 -0.31852651 0.86166668 -0.30016381 -0.31852651 0.73666638 -0.30016381 -0.61102736 0.85951686 -0.30016381
		 -0.61102736 0.7388162 -0.30016381 -0.61102742 0.85951686 -0.14389275 -0.61102742 0.7388162 -0.14389275
		 0.17248559 0.89049238 0.20087162 0.17248559 0.7078405 0.20087162 0.30639589 0.89049238 -0.15028706
		 0.30639589 0.7078405 -0.15028706 -1.021702528 0.7078405 -0.073088847 -1.021702528 0.89049238 -0.073088847
		 -1.021702528 0.89049238 -0.14669082 -1.021702528 0.7078405 -0.14669082 -1.28758359 0.54839456 -0.14005619
		 -1.28758359 1.04993856 -0.14005619 -1.28758359 1.04993856 -0.21365815 -1.28758359 0.5483945 -0.21365815
		 -1.13863945 0.69137543 -0.14669082 -1.13863945 0.90695751 -0.14669082 -1.13863945 0.90695745 -0.073088847
		 -1.13863945 0.69137543 -0.073088847 -1.41746378 0.52685434 -0.26129013 -1.41746378 1.071478724 -0.26129013
		 -1.41746378 1.071478724 -0.33489209 -1.41746378 0.52685428 -0.33489209 -1.41746378 0.41462401 -0.29944429
		 -1.41746378 1.18817651 -0.29944429 -1.28758359 1.15758216 -0.17685717 -1.13863945 0.9545002 -0.10988984
		 -1.13863945 0.64830011 -0.10988984 -1.28758359 0.44521847 -0.17685717 -1.15567386 1.035847187 -0.080016509
		 -1.15567386 1.056514502 -0.15361847 -1.15567386 1.20725918 -0.11952384 -1.15567386 1.022305727 -0.15361847
		 -1.15567386 1.022305727 -0.080016509 -1.15567386 1.11833405 -0.11681749 -1.15567386 0.51341909 -0.080016509
		 -1.15567386 0.39554134 -0.11952384 -1.15567386 0.54223716 -0.15361847 -1.15567386 0.57602733 -0.15361847
		 -1.15567386 0.57602733 -0.080016509 -1.15567386 0.48446646 -0.11681749 0.18119574 1.024077535 0.20087162
		 0.18119574 0.52518868 0.20087162 0.29014802 1.22925973 -0.049646109 0.39724588 1.055624604 -0.30059582
		 0.39724588 0.57322866 -0.30059582 0.29014802 0.3420682 -0.049646109;
	setAttr ".vt[166:331]" 0.18119574 0.89049238 0.20087162 0.18119574 0.7078405 0.20087162
		 0.39724591 0.98668736 -0.30059582 0.39724591 0.61144346 -0.30059582 -0.42053059 0.54692316 -0.142738
		 -0.12246937 0.50734597 0.035390273 0.12022477 0.48484725 0.093894109 -0.75727785 1.028232336 -0.18583357
		 0.27167046 1.10612059 0.11489579 0.27167046 0.50505227 0.11489579 -0.75727785 0.54692316 -0.18583357
		 -0.42053059 1.028232336 -0.14273801 -0.12246937 1.06780982 0.035390258 0.12022477 1.090308189 0.093894094
		 0.18619454 0.44498897 0.10351585 0.18619454 1.13016653 0.10351583 -0.026025653 1.090308189 0.06412062
		 -0.026025653 0.48484725 0.064120635 -0.31852651 1.03530705 -0.085277274 -0.31852651 0.53984857 -0.085277259
		 -0.61102736 1.028232336 -0.18583357 -0.61102736 0.54692316 -0.18583357 0.21917939 0.44498897 0.11025107
		 0.21917939 1.13016653 0.11025107 -1.021702528 1.13016653 -0.092842519 -1.021702528 0.44498897 -0.092842519
		 -1.28758359 1.15199137 -0.15980986 -1.28758359 0.44747034 -0.15980986 -1.13863945 1.18434954 -0.092842519
		 -1.13863945 0.39080587 -0.092842519 -1.41746378 1.18224907 -0.2810438 -1.41746378 0.41721269 -0.2810438
		 -1.41746378 0.50102377 -0.2803672 -1.41746378 1.098438025 -0.2803672 0.27167046 0.9057644 0.11489579
		 0.27167046 0.70540828 0.11489579 -1.28758359 1.074810028 -0.15845668 -1.13863945 0.91796988 -0.091489345
		 -1.13863945 0.68149173 -0.091489345 -1.28758359 0.52465189 -0.15845668 -1.15567386 1.044498682 -0.098416999
		 -1.15567386 1.11317551 -0.099770173 -1.15567386 0.55496305 -0.098416999 -1.15567386 0.48628625 -0.099770173
		 0.23567188 0.44498897 0.1122556 0.23567188 1.13016653 0.1122556 -0.75727785 -0.96230012 -0.30016381
		 -0.75727785 -0.93115067 -0.14389275 0.41272557 -1.065325975 -0.30016381 0.18990588 -1.024077535 0.20087162
		 -0.75727785 -0.63645154 -0.30016381 -0.75727785 -0.6181156 -0.14389275 0.41272557 -0.56352729 -0.30016381
		 0.18990588 -0.52518868 0.20087162 0.12022477 -1.01322484 -0.14789285 -0.12246937 -0.99476796 -0.17375019
		 -0.42053056 -0.96230012 -0.21841748 -0.42053062 -0.93115067 -0.10079719 -0.12246937 -0.9672308 0.12042663
		 0.12022477 -0.98774117 0.20087162 -0.42053062 -0.6181156 -0.10079719 -0.12246937 -0.58203554 0.12042663
		 0.12022477 -0.56152493 0.20087162 0.12022477 -0.5855267 -0.14789285 -0.12246937 -0.60398382 -0.17375019
		 -0.42053056 -0.63645154 -0.21841748 -0.75727785 -1.097271919 -0.2277744 0.3534351 -1.20570612 -0.11925504
		 0.3534351 -0.42742217 -0.11925504 -0.75727785 -0.47405612 -0.2277744 -0.42053056 -0.47405612 -0.18467881
		 -0.12246937 -0.42281014 -0.04964608 0.12022477 -0.39367807 -0.04964608 -0.42053056 -1.097271919 -0.18467884
		 -0.12246937 -1.14851809 -0.049646109 0.12022477 -1.17764974 -0.049646109 0.1550653 -1.024077535 0.20087162
		 0.1550653 -0.52518868 0.20087162 0.23501831 -1.045923233 -0.11054337 0.23501831 -0.55282843 -0.11054337
		 0.21732378 -0.3420682 -0.049646094 0.21732378 -1.22925973 -0.049646109 -0.026025683 -1.01322484 -0.30016381
		 -0.026025683 -0.98774117 0.17788735 -0.026025683 -0.56152493 0.17788735 -0.026025683 -0.5855267 -0.30016381
		 -0.026025712 -1.17764974 -0.049646109 -0.026025712 -0.39367807 -0.04964608 -0.31852651 -0.96810389 -0.30016381
		 -0.31852651 -0.93760014 -0.043336451 -0.31852651 -0.61166614 -0.043336451 -0.31852651 -0.63064784 -0.30016381
		 -0.31852651 -1.10643232 -0.1272181 -0.31852651 -0.46489567 -0.12721807 -0.61102736 -0.96230012 -0.30016381
		 -0.61102736 -0.63645154 -0.30016381 -0.61102742 -0.93115067 -0.14389275 -0.61102742 -0.6181156 -0.14389275
		 -0.61102736 -1.097271919 -0.2277744 -0.61102736 -0.47405612 -0.22777438 0.17248559 -1.024077535 0.20087162
		 0.17248559 -0.52518868 0.20087162 0.26587325 -1.22925973 -0.049646109 0.30639589 -1.045923233 -0.15028706
		 0.30639589 -0.55282843 -0.15028706 0.26587325 -0.3420682 -0.049646102 -1.021702528 -1.045923233 -0.14669082
		 -1.021702528 -1.024077535 -0.073088847 -1.021702528 -0.55282843 -0.14669082 -1.021702528 -0.52518868 -0.073088847
		 -1.021702528 -1.22925973 -0.11259619 -1.021702528 -0.3420682 -0.11259619 -1.28758359 -1.088357687 -0.21365815
		 -1.28758359 -1.071233153 -0.14005619 -1.28758359 -0.51039404 -0.21365815 -1.28758359 -0.4780331 -0.14005619
		 -1.28758359 -1.25751936 -0.17956352 -1.28758359 -0.34528118 -0.17956352 -1.13863945 -1.073472738 -0.073088847
		 -1.13863945 -0.47579342 -0.073088847 -1.13863945 -0.27191007 -0.11259619 -1.13863945 -0.50837857 -0.14669082
		 -1.13863945 -1.090373039 -0.14669082 -1.13863945 -1.29941773 -0.11259619 -1.41746378 -1.11317992 -0.33489209
		 -1.41746378 -1.09881711 -0.26129013 -1.41746378 -0.48557171 -0.33489209 -1.41746378 -0.45044914 -0.26129013
		 -1.41746378 -1.29669797 -0.30079746 -1.41746378 -0.30610254 -0.30079746 -0.75727785 -0.7388162 -0.14389275
		 -0.75727785 -0.85951686 -0.14389275 0.41272557 -0.6078499 -0.30016381 0.41272557 -0.99028105 -0.30016381
		 -0.75727785 -0.85951686 -0.30016381 -0.75727785 -0.7388162 -0.30016381 0.18990588 -0.89049238 0.20087162
		 0.18990588 -0.7078405 0.20087162 -0.42053056 -0.85951686 -0.21841748 -0.42053056 -0.7388162 -0.21841748
		 -0.12246937 -0.87154353 -0.17375019 -0.12246937 -0.72678953 -0.17375019 0.12022477 -0.8783803 -0.14789285
		 0.12022477 -0.71995264 -0.14789285 0.12022477 -0.8783803 0.20087162 0.12022477 -0.71995264 0.20087162
		 -0.12246937 -0.87154353 0.12042663 -0.12246937 -0.72678953 0.12042663 -0.42053062 -0.85951686 -0.10079719
		 -0.42053062 -0.7388162 -0.10079719 0.3534351 -0.9462781 -0.11925504 0.3534351 -0.68685007 -0.11925504
		 0.1550653 -0.89049238 0.20087162 0.1550653 -0.7078405 0.20087162 0.23501831 -0.89049238 -0.11054337
		 0.23501831 -0.7078405 -0.11054337 -0.026025683 -0.8783803 0.17788735 -0.026025683 -0.71995264 0.17788735
		 -0.026025683 -0.8783803 -0.30016381 -0.026025683 -0.71995264 -0.30016381 -0.31852651 -0.86166668 -0.043336451
		 -0.31852651 -0.73666638 -0.043336451 -0.31852651 -0.86166668 -0.30016381 -0.31852651 -0.73666638 -0.30016381
		 -0.61102736 -0.85951686 -0.30016381 -0.61102736 -0.7388162 -0.30016381;
	setAttr ".vt[332:423]" -0.61102742 -0.85951686 -0.14389275 -0.61102742 -0.7388162 -0.14389275
		 0.17248559 -0.89049238 0.20087162 0.17248559 -0.7078405 0.20087162 0.30639589 -0.89049238 -0.15028706
		 0.30639589 -0.7078405 -0.15028706 -1.021702528 -0.7078405 -0.073088847 -1.021702528 -0.89049238 -0.073088847
		 -1.021702528 -0.89049238 -0.14669082 -1.021702528 -0.7078405 -0.14669082 -1.28758359 -0.54839456 -0.14005619
		 -1.28758359 -1.04993856 -0.14005619 -1.28758359 -1.04993856 -0.21365815 -1.28758359 -0.5483945 -0.21365815
		 -1.13863945 -0.69137543 -0.14669082 -1.13863945 -0.90695751 -0.14669082 -1.13863945 -0.90695745 -0.073088847
		 -1.13863945 -0.69137543 -0.073088847 -1.41746378 -0.52685434 -0.26129013 -1.41746378 -1.071478724 -0.26129013
		 -1.41746378 -1.071478724 -0.33489209 -1.41746378 -0.52685428 -0.33489209 -1.41746378 -0.41462401 -0.29944429
		 -1.41746378 -1.18817651 -0.29944429 -1.28758359 -1.15758216 -0.17685717 -1.13863945 -0.9545002 -0.10988984
		 -1.13863945 -0.64830011 -0.10988984 -1.28758359 -0.44521847 -0.17685717 -1.15567386 -1.035847187 -0.080016509
		 -1.15567386 -1.056514502 -0.15361847 -1.15567386 -1.20725918 -0.11952384 -1.15567386 -1.022305727 -0.15361847
		 -1.15567386 -1.022305727 -0.080016509 -1.15567386 -1.11833405 -0.11681749 -1.15567386 -0.51341909 -0.080016509
		 -1.15567386 -0.39554134 -0.11952384 -1.15567386 -0.54223716 -0.15361847 -1.15567386 -0.57602733 -0.15361847
		 -1.15567386 -0.57602733 -0.080016509 -1.15567386 -0.48446646 -0.11681749 0.18119574 -1.024077535 0.20087162
		 0.18119574 -0.52518868 0.20087162 0.29014802 -1.22925973 -0.049646109 0.39724588 -1.055624604 -0.30059582
		 0.39724588 -0.57322866 -0.30059582 0.29014802 -0.3420682 -0.049646109 0.18119574 -0.89049238 0.20087162
		 0.18119574 -0.7078405 0.20087162 0.39724591 -0.98668736 -0.30059582 0.39724591 -0.61144346 -0.30059582
		 -0.42053059 -0.54692316 -0.142738 -0.12246937 -0.50734597 0.035390273 0.12022477 -0.48484725 0.093894109
		 -0.75727785 -1.028232336 -0.18583357 0.27167046 -1.10612059 0.11489579 0.27167046 -0.50505227 0.11489579
		 -0.75727785 -0.54692316 -0.18583357 -0.42053059 -1.028232336 -0.14273801 -0.12246937 -1.06780982 0.035390258
		 0.12022477 -1.090308189 0.093894094 0.18619454 -0.44498897 0.10351585 0.18619454 -1.13016653 0.10351583
		 -0.026025653 -1.090308189 0.06412062 -0.026025653 -0.48484725 0.064120635 -0.31852651 -1.03530705 -0.085277274
		 -0.31852651 -0.53984857 -0.085277259 -0.61102736 -1.028232336 -0.18583357 -0.61102736 -0.54692316 -0.18583357
		 0.21917939 -0.44498897 0.11025107 0.21917939 -1.13016653 0.11025107 -1.021702528 -1.13016653 -0.092842519
		 -1.021702528 -0.44498897 -0.092842519 -1.28758359 -1.15199137 -0.15980986 -1.28758359 -0.44747034 -0.15980986
		 -1.13863945 -1.18434954 -0.092842519 -1.13863945 -0.39080587 -0.092842519 -1.41746378 -1.18224907 -0.2810438
		 -1.41746378 -0.41721269 -0.2810438 -1.41746378 -0.50102377 -0.2803672 -1.41746378 -1.098438025 -0.2803672
		 0.27167046 -0.9057644 0.11489579 0.27167046 -0.70540828 0.11489579 -1.28758359 -1.074810028 -0.15845668
		 -1.13863945 -0.91796988 -0.091489345 -1.13863945 -0.68149173 -0.091489345 -1.28758359 -0.52465189 -0.15845668
		 -1.15567386 -1.044498682 -0.098416999 -1.15567386 -1.11317551 -0.099770173 -1.15567386 -0.55496305 -0.098416999
		 -1.15567386 -0.48628625 -0.099770173 0.23567188 -0.44498897 0.1122556 0.23567188 -1.13016653 0.1122556;
	setAttr -s 840 ".ed";
	setAttr ".ed[0:165]"  89 4 0 4 62 0 62 129 0 129 89 0 211 160 0 160 3 0 3 174 0
		 174 211 0 121 84 0 84 5 0 5 51 0 51 121 0 169 86 0 86 6 0 6 164 0 164 169 0 201 91 0
		 91 7 0 7 175 0 175 201 0 210 175 0 7 161 0 161 210 0 187 170 0 170 14 0 14 51 0 51 187 0
		 185 171 0 171 15 0 15 44 0 44 185 0 183 172 0 172 16 0 16 38 0 38 183 0 119 93 0
		 93 19 0 19 49 0 49 119 0 117 95 0 95 18 0 18 45 0 45 117 0 113 97 0 97 17 0 17 39 0
		 39 113 0 107 99 0 99 16 0 16 31 0 31 107 0 111 101 0 101 15 0 15 38 0 38 111 0 115 103 0
		 103 14 0 14 44 0 44 115 0 186 50 0 50 11 0 11 177 0 177 186 0 184 43 0 43 12 0 12 178 0
		 178 184 0 182 37 0 37 13 0 13 179 0 179 182 0 36 40 0 40 29 0 29 8 0 8 36 0 42 46 0
		 46 28 0 28 9 0 9 42 0 48 52 0 52 27 0 27 10 0 10 48 0 17 26 0 26 41 0 41 39 0 18 25 0
		 25 47 0 47 45 0 19 24 0 24 53 0 53 49 0 6 22 0 22 165 0 165 164 0 86 105 0 105 22 0
		 163 162 0 162 21 0 21 2 0 2 163 0 176 5 0 5 63 0 63 191 0 191 176 0 29 35 0 35 32 0
		 32 8 0 17 33 0 33 34 0 34 26 0 123 107 0 31 55 0 55 123 0 172 180 0 180 31 0 97 109 0
		 109 33 0 13 30 0 30 181 0 181 179 0 18 39 0 41 25 0 28 40 0 36 9 0 12 37 0 182 178 0
		 99 111 0 95 113 0 171 183 0 19 45 0 47 24 0 27 46 0 42 10 0 11 43 0 184 177 0 101 115 0
		 93 117 0 170 185 0 4 49 0 53 23 0 23 4 0 0 20 0 20 52 0 48 0 0 173 1 0 1 50 0 186 173 0
		 89 119 0 176 187 0 103 121 0 167 123 0 55 161 0 161 167 0 35 56 0 56 57 0 57 32 0
		 33 58 0 58 59 0 59 34 0 180 188 0 188 55 0 109 125 0 125 58 0 30 54 0 54 189 0;
	setAttr ".ed[166:331]" 189 181 0 209 154 0 154 69 0 69 193 0 193 209 0 152 148 0
		 148 67 0 67 131 0 131 152 0 85 1 0 1 61 0 61 127 0 127 85 0 23 65 0 65 62 0 173 20 0
		 20 64 0 64 190 0 190 173 0 0 60 0 60 64 0 133 68 0 68 80 0 80 141 0 141 133 0 192 70 0
		 70 82 0 82 196 0 196 192 0 156 155 0 155 71 0 71 68 0 68 156 0 150 149 0 149 66 0
		 66 70 0 70 150 0 207 150 0 192 207 0 157 156 0 133 157 0 62 75 0 75 134 0 134 129 0
		 64 77 0 77 194 0 194 190 0 60 76 0 76 77 0 65 74 0 74 75 0 61 72 0 72 136 0 136 127 0
		 63 73 0 73 195 0 195 191 0 199 139 0 139 79 0 79 196 0 196 199 0 140 143 0 143 82 0
		 82 78 0 78 140 0 67 79 0 139 131 0 69 81 0 81 197 0 197 193 0 71 83 0 83 80 0 66 78 0
		 69 130 0 130 138 0 138 81 0 198 205 0 205 147 0 147 142 0 142 198 0 83 142 0 142 141 0
		 144 143 0 140 132 0 132 144 0 138 198 0 198 197 0 153 144 0 132 151 0 151 153 0 63 126 0
		 126 137 0 137 73 0 126 127 0 136 137 0 60 128 0 128 135 0 135 76 0 128 129 0 134 135 0
		 149 151 0 132 66 0 146 145 0 145 135 0 134 146 0 159 146 0 134 157 0 157 159 0 84 126 0
		 84 85 0 154 158 0 158 130 0 147 133 0 57 124 0 124 108 0 108 32 0 124 125 0 109 108 0
		 160 54 0 54 122 0 122 166 0 166 160 0 122 123 0 167 166 0 50 120 0 120 102 0 102 11 0
		 120 121 0 103 102 0 48 118 0 118 88 0 88 0 0 118 119 0 89 88 0 42 116 0 116 92 0
		 92 10 0 116 117 0 93 92 0 43 114 0 114 100 0 100 12 0 114 115 0 101 100 0 36 112 0
		 112 94 0 94 9 0 112 113 0 95 94 0 37 110 0 110 98 0 98 13 0 110 111 0 99 98 0 108 96 0
		 96 8 0 97 96 0 30 106 0 106 122 0 106 107 0 21 104 0 104 87 0 87 2 0 90 166 0 167 91 0
		 91 90 0;
	setAttr ".ed[332:497]" 102 114 0 100 110 0 98 106 0 96 112 0 94 116 0 92 118 0
		 3 90 0 90 200 0 200 174 0 201 200 0 87 168 0 168 163 0 57 163 0 168 124 0 85 120 0
		 88 128 0 208 204 0 204 146 0 159 208 0 204 203 0 203 145 0 206 202 0 202 144 0 153 206 0
		 202 199 0 199 143 0 203 206 0 153 145 0 135 151 0 149 76 0 77 150 0 207 194 0 72 148 0
		 152 136 0 205 208 0 159 147 0 137 158 0 154 73 0 75 156 0 74 155 0 209 195 0 56 162 0
		 58 164 0 165 59 0 188 210 0 125 169 0 211 189 0 124 104 0 104 105 0 105 125 0 56 189 0
		 211 162 0 165 210 0 188 59 0 74 195 0 209 155 0 158 208 0 205 130 0 72 194 0 207 148 0
		 152 206 0 203 136 0 202 131 0 204 137 0 104 200 0 201 105 0 21 174 0 83 197 0 71 193 0
		 65 191 0 61 190 0 192 67 0 35 181 0 180 34 0 53 187 0 176 23 0 186 52 0 47 185 0
		 170 24 0 27 177 0 184 46 0 41 183 0 171 25 0 28 178 0 182 40 0 29 179 0 172 26 0
		 22 175 0 301 341 0 341 274 0 274 216 0 216 301 0 423 386 0 386 215 0 215 372 0 372 423 0
		 333 263 0 263 217 0 217 296 0 296 333 0 381 376 0 376 218 0 218 298 0 298 381 0 413 387 0
		 387 219 0 219 303 0 303 413 0 422 373 0 373 219 0 387 422 0 399 263 0 263 226 0 226 382 0
		 382 399 0 397 256 0 256 227 0 227 383 0 383 397 0 395 250 0 250 228 0 228 384 0 384 395 0
		 331 261 0 261 231 0 231 305 0 305 331 0 329 257 0 257 230 0 230 307 0 307 329 0 325 251 0
		 251 229 0 229 309 0 309 325 0 319 243 0 243 228 0 228 311 0 311 319 0 323 250 0 250 227 0
		 227 313 0 313 323 0 327 256 0 256 226 0 226 315 0 315 327 0 398 389 0 389 223 0 223 262 0
		 262 398 0 396 390 0 390 224 0 224 255 0 255 396 0 394 391 0 391 225 0 225 249 0 249 394 0
		 248 220 0 220 241 0 241 252 0 252 248 0 254 221 0 221 240 0 240 258 0;
	setAttr ".ed[498:663]" 258 254 0 260 222 0 222 239 0 239 264 0 264 260 0 251 253 0
		 253 238 0 238 229 0 257 259 0 259 237 0 237 230 0 261 265 0 265 236 0 236 231 0 376 377 0
		 377 234 0 234 218 0 234 317 0 317 298 0 375 214 0 214 233 0 233 374 0 374 375 0 388 403 0
		 403 275 0 275 217 0 217 388 0 220 244 0 244 247 0 247 241 0 238 246 0 246 245 0 245 229 0
		 335 267 0 267 243 0 319 335 0 243 392 0 392 384 0 245 321 0 321 309 0 391 393 0 393 242 0
		 242 225 0 237 253 0 251 230 0 221 248 0 252 240 0 390 394 0 249 224 0 323 311 0 325 307 0
		 395 383 0 236 259 0 257 231 0 222 254 0 258 239 0 389 396 0 255 223 0 327 313 0 329 305 0
		 397 382 0 216 235 0 235 265 0 261 216 0 212 260 0 264 232 0 232 212 0 385 398 0 262 213 0
		 213 385 0 331 301 0 399 388 0 333 315 0 379 373 0 373 267 0 335 379 0 244 269 0 269 268 0
		 268 247 0 246 271 0 271 270 0 270 245 0 267 400 0 400 392 0 270 337 0 337 321 0 393 401 0
		 401 266 0 266 242 0 421 405 0 405 281 0 281 366 0 366 421 0 364 343 0 343 279 0 279 360 0
		 360 364 0 297 339 0 339 273 0 273 213 0 213 297 0 274 277 0 277 235 0 385 402 0 402 276 0
		 276 232 0 232 385 0 276 272 0 272 212 0 345 353 0 353 292 0 292 280 0 280 345 0 404 408 0
		 408 294 0 294 282 0 282 404 0 368 280 0 280 283 0 283 367 0 367 368 0 362 282 0 282 278 0
		 278 361 0 361 362 0 419 404 0 362 419 0 369 345 0 368 369 0 341 346 0 346 287 0 287 274 0
		 402 406 0 406 289 0 289 276 0 289 288 0 288 272 0 287 286 0 286 277 0 339 348 0 348 284 0
		 284 273 0 403 407 0 407 285 0 285 275 0 411 408 0 408 291 0 291 351 0 351 411 0 352 290 0
		 290 294 0 294 355 0 355 352 0 343 351 0 291 279 0 405 409 0 409 293 0 293 281 0 292 295 0
		 295 283 0 290 278 0 293 350 0 350 342 0 342 281 0 410 354 0 354 359 0;
	setAttr ".ed[664:829]" 359 417 0 417 410 0 353 354 0 354 295 0 356 344 0 344 352 0
		 355 356 0 409 410 0 410 350 0 365 363 0 363 344 0 356 365 0 285 349 0 349 338 0 338 275 0
		 349 348 0 339 338 0 288 347 0 347 340 0 340 272 0 347 346 0 341 340 0 278 344 0 363 361 0
		 358 346 0 347 357 0 357 358 0 371 369 0 369 346 0 358 371 0 338 296 0 297 296 0 342 370 0
		 370 366 0 345 359 0 244 320 0 320 336 0 336 269 0 320 321 0 337 336 0 372 378 0 378 334 0
		 334 266 0 266 372 0 378 379 0 335 334 0 223 314 0 314 332 0 332 262 0 314 315 0 333 332 0
		 212 300 0 300 330 0 330 260 0 300 301 0 331 330 0 222 304 0 304 328 0 328 254 0 304 305 0
		 329 328 0 224 312 0 312 326 0 326 255 0 312 313 0 327 326 0 221 306 0 306 324 0 324 248 0
		 306 307 0 325 324 0 225 310 0 310 322 0 322 249 0 310 311 0 323 322 0 220 308 0 308 320 0
		 308 309 0 334 318 0 318 242 0 319 318 0 214 299 0 299 316 0 316 233 0 302 303 0 303 379 0
		 378 302 0 326 314 0 322 312 0 318 310 0 324 308 0 328 306 0 330 304 0 386 412 0 412 302 0
		 302 215 0 412 413 0 375 380 0 380 299 0 336 380 0 375 269 0 332 297 0 340 300 0 420 371 0
		 358 416 0 416 420 0 357 415 0 415 416 0 418 365 0 356 414 0 414 418 0 355 411 0 411 414 0
		 357 365 0 418 415 0 288 361 0 363 347 0 406 419 0 362 289 0 348 364 0 360 284 0 359 371 0
		 420 417 0 285 366 0 370 349 0 368 287 0 367 286 0 407 421 0 374 268 0 271 377 0 376 270 0
		 422 400 0 381 337 0 401 423 0 337 317 0 317 316 0 316 336 0 374 423 0 401 268 0 271 400 0
		 422 377 0 367 421 0 407 286 0 342 417 0 420 370 0 360 419 0 406 284 0 348 415 0 418 364 0
		 343 414 0 349 416 0 317 413 0 412 316 0 386 233 0 409 295 0 405 283 0 403 277 0 402 273 0
		 279 404 0 393 247 0 246 392 0 235 388 0 399 265 0 264 398 0 236 382 0;
	setAttr ".ed[830:839]" 397 259 0 258 396 0 389 239 0 237 383 0 395 253 0 252 394 0
		 390 240 0 391 241 0 238 384 0 387 234 0;
	setAttr -s 1680 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.50197935 0 -0.86487961 -0.50197935
		 0 -0.86487961 -0.50197935 0 -0.86487961 -0.50197935 0 -0.86487961 0.25129494 0.55435169
		 0.79343873 0.25129494 0.55435169 0.79343873 0.25129494 0.55435169 0.79343873 0.25129494
		 0.55435169 0.79343873 0 0 1 0 0 1 0 0 1 0 0 1 0.027896708 0 -0.99961078 0.027896708
		 0 -0.99961078 0.027896708 0 -0.99961078 0.027896708 0 -0.99961078 0.72463089 0 0.68913716
		 0.72463089 0 0.68913716 0.72463089 0 0.68913716 0.72463089 0 0.68913716 0.56127322
		 -0.44836855 0.69565648 0.56127322 -0.44836855 0.69565648 0.56127322 -0.44836855 0.69565648
		 0.56127322 -0.44836855 0.69565648 -0.19131726 -0.49820977 0.8456859 -0.19131726 -0.49820977
		 0.8456859 -0.19131726 -0.49820977 0.8456859 -0.19131726 -0.49820977 0.8456859 -0.54612517
		 -0.54864806 0.63303435 -0.54612517 -0.54864806 0.63303435 -0.54612517 -0.54864806
		 0.63303435 -0.54612517 -0.54864806 0.63303435 -0.10236788 -0.81694961 0.56755096
		 -0.10236788 -0.81694961 0.56755096 -0.10236788 -0.81694961 0.56755096 -0.10236788
		 -0.81694961 0.56755096 0.39434659 0 -0.91896176 0.39434659 0 -0.91896176 0.39434659
		 0 -0.91896176 0.39434659 0 -0.91896176 0.54190022 0 -0.84044284 0.54190022 0 -0.84044284
		 0.54190022 0 -0.84044284 0.54190022 0 -0.84044284 0.72122085 0 -0.69270521 0.72122085
		 0 -0.69270521 0.72122085 0 -0.69270521 0.72122085 0 -0.69270521 -7.8371798e-08 0
		 1 -7.8371798e-08 0 1 -7.8371798e-08 0 1 -7.8371798e-08 0 1 -0.51183748 0 0.85908228
		 -0.51183748 0 0.85908228 -0.51183748 0 0.85908228 -0.51183748 0 0.85908228 -0.49080238
		 0 0.8712709 -0.49080238 0 0.8712709 -0.49080238 0 0.8712709 -0.49080238 0 0.8712709
		 -0.20333718 0.38830385 0.89881814 -0.20333718 0.38830385 0.89881814 -0.20333718 0.38830385
		 0.89881814 -0.20333718 0.38830385 0.89881814 -0.57142961 0.4425582 0.69109356 -0.57142961
		 0.4425582 0.69109356 -0.57142961 0.4425582 0.69109356 -0.57142961 0.4425582 0.69109356
		 -0.1218668 0.72707319 0.67565745 -0.1218668 0.72707319 0.67565745 -0.1218668 0.72707319
		 0.67565745 -0.1218668 0.72707319 0.67565745 0.33632979 0.68518865 -0.64606416 0.33632979
		 0.68518865 -0.64606416 0.33632979 0.68518865 -0.64606416 0.33632979 0.68518865 -0.64606416
		 0.2331102 0.69340378 -0.68179971 0.2331102 0.69340378 -0.68179971 0.2331102 0.69340378
		 -0.68179971 0.2331102 0.69340378 -0.68179971 0.29169142 0.34997615 -0.89018703 0.29169142
		 0.34997615 -0.89018703 0.29169142 0.34997615 -0.89018703 0.29169142 0.34997615 -0.89018703
		 0.35947514 -0.62765735 -0.6905244 0.35947514 -0.62765735 -0.6905244 0.35947514 -0.62765735
		 -0.6905244 0.35947514 -0.62765735 -0.6905244 0.27063307 -0.62612253 -0.7312513 0.27063307
		 -0.62612253 -0.7312513 0.27063307 -0.62612253 -0.7312513 0.27063307 -0.62612253 -0.7312513
		 0.29737741 -0.29654592 -0.90753907 0.29737741 -0.29654592 -0.90753907 0.29737741
		 -0.29654592 -0.90753907 0.29737741 -0.29654592 -0.90753907 0.25273129 -0.78293318
		 -0.56845623 0.25273129 -0.78293318 -0.56845623 0.25273129 -0.78293318 -0.56845623
		 0.25273129 -0.78293318 -0.56845623 0.95026678 0 0.31143728 0.95026678 0 0.31143728
		 0.95026678 0 0.31143728 0.95026678 0 0.31143728 -0.44390431 0.63470179 -0.63253671
		 -0.44390431 0.63470179 -0.63253671 -0.44390431 0.63470179 -0.63253671 -0.44390431
		 0.63470179 -0.63253671 0.39181775 -0.34720737 0.85201287 0.39181775 -0.34720737 0.85201287
		 0.39181775 -0.34720737 0.85201287 0.39181775 -0.34720737 0.85201287 -0.0051828697
		 0.41589808 -0.90939653 -0.0051828697 0.41589808 -0.90939653 -0.0051828697 0.41589808
		 -0.90939653 -0.0051828697 0.41589808 -0.90939653 0.017349895 -0.36820936 -0.92958105
		 0.017349895 -0.36820936 -0.92958105 0.017349895 -0.36820936 -0.92958105 0.017349895
		 -0.36820936 -0.92958105 0 0 1 0 0 1 0 0 1 0 0 1 -0.5664171 -0.69310594 0.44584271
		 -0.5664171 -0.69310594 0.44584271 -0.5664171 -0.69310594 0.44584271 -0.5664171 -0.69310594
		 0.44584271 0.30939752 0 -0.95093286 0.30939752 0 -0.95093286 0.30939752 0 -0.95093286
		 0.30939752 0 -0.95093286 -0.53141361 0.63184059 0.5642491 -0.53141361 0.63184059
		 0.5642491 -0.53141361 0.63184059 0.5642491 -0.53141361 0.63184059 0.5642491 -0.53741223
		 -0.59759134 -0.59504002 -0.53741223 -0.59759134 -0.59504002 -0.53741223 -0.59759134
		 -0.59504002 -0.53741223 -0.59759134 -0.59504002 -0.52218956 0.6500212 -0.55207831
		 -0.52218956 0.6500212 -0.55207831 -0.52218956 0.6500212 -0.55207831 -0.52218956 0.6500212
		 -0.55207831 -0.42929038 0.63169813 0.64549768 -0.42929038 0.63169813 0.64549768 -0.42929038
		 0.63169813 0.64549768 -0.42929038 0.63169813 0.64549768 -0.15525137 0 0.98787493
		 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493 -0.79504192
		 0 -0.60655445 -0.79504192 0 -0.60655445 -0.79504192 0 -0.60655445 -0.79504192 0 -0.60655445
		 -0.40892199 -0.72606593 0.55282104 -0.40892199 -0.72606593 0.55282104 -0.40892199
		 -0.72606593 0.55282104 -0.40892199 -0.72606593 0.55282104 -0.13847256 -0.52781367
		 -0.83799648 -0.13847256 -0.52781367 -0.83799648 -0.13847256 -0.52781367 -0.83799648
		 -0.13847256 -0.52781367 -0.83799648 -0.13786311 0.59742755 -0.78998363 -0.13786311
		 0.59742755 -0.78998363;
	setAttr ".n[166:331]" -type "float3"  -0.13786311 0.59742755 -0.78998363 -0.13786311
		 0.59742755 -0.78998363 -0.47759843 0.34749112 0.80693853 -0.47759843 0.34749112 0.80693853
		 -0.47759843 0.34749112 0.80693853 -0.47759843 0.34749112 0.80693853 -0.64106673 0
		 0.76748508 -0.64106673 0 0.76748508 -0.64106673 0 0.76748508 -0.64106673 0 0.76748508
		 -0.6253624 0 -0.78033441 -0.6253624 0 -0.78033441 -0.6253624 0 -0.78033441 -0.6253624
		 0 -0.78033441 -0.46096259 -0.44897768 0.7654624 -0.46096259 -0.44897768 0.7654624
		 -0.46096259 -0.44897768 0.7654624 -0.46096259 -0.44897768 0.7654624 0 -0.40714175
		 -0.91336501 0 -0.40714175 -0.91336501 0 -0.40714175 -0.91336501 0 -0.40714175 -0.91336501
		 0 0.47264287 -0.88125402 0 0.47264287 -0.88125402 0 0.47264287 -0.88125402 0 0.47264287
		 -0.88125402 0 0.39658925 0.91799617 0 0.39658925 0.91799617 0 0.39658925 0.91799617
		 0 0.39658925 0.91799617 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 -0.50758582 0.86160123 0 -0.50758582
		 0.86160123 0 -0.50758582 0.86160123 0 -0.50758582 0.86160123 -0.22065128 0 0.97535276
		 -0.22065128 0 0.97535276 -0.22065128 0 0.97535276 -0.22065128 0 0.97535276 0 0 1
		 0 0 1 0 0 1 0 0 1 -0.29519635 0.34555593 -0.89075822 -0.29519635 0.34555593 -0.89075822
		 -0.29519635 0.34555593 -0.89075822 -0.29519635 0.34555593 -0.89075822 -0.29958034
		 -0.30505681 -0.90398669 -0.29958034 -0.30505681 -0.90398669 -0.29958034 -0.30505681
		 -0.90398669 -0.29958034 -0.30505681 -0.90398669 -0.083719559 -0.77487868 0.62654132
		 -0.083719559 -0.77487868 0.62654132 -0.083719559 -0.77487868 0.62654132 -0.083719559
		 -0.77487868 0.62654132 -0.4864797 0 -0.87369192 -0.4864797 0 -0.87369192 -0.4864797
		 0 -0.87369192 -0.4864797 0 -0.87369192 -0.097145118 0.67971778 0.72701198 -0.097145118
		 0.67971778 0.72701198 -0.097145118 0.67971778 0.72701198 -0.097145118 0.67971778
		 0.72701198 -0.21172875 -0.55218023 0.80639189 -0.21172875 -0.55218023 0.80639189
		 -0.21172875 -0.55218023 0.80639189 -0.21172875 -0.55218023 0.80639189 -0.41426182
		 0 0.91015786 -0.41426182 0 0.91015786 -0.41426182 0 0.91015786 -0.41426182 0 0.91015786
		 0.258654 0 0.96596998 0.258654 0 0.96596998 0.258654 0 0.96596998 0.258654 0 0.96596998
		 -0.35244945 -0.25679821 -0.89990783 -0.35244945 -0.25679821 -0.89990783 -0.35244945
		 -0.25679821 -0.89990783 -0.35244945 -0.25679821 -0.89990783 0.46268159 0.30538979
		 0.83226365 0.46268159 0.30538979 0.83226365 0.46268159 0.30538979 0.83226365 0.46268159
		 0.30538979 0.83226365 -0.33237264 0.29921341 -0.894427 -0.33237264 0.29921341 -0.894427
		 -0.33237264 0.29921341 -0.894427 -0.33237264 0.29921341 -0.894427 0.68235594 0 -0.73102003
		 0.68235594 0 -0.73102003 0.68235594 0 -0.73102003 0.68235594 0 -0.73102003 -0.65699953
		 0.13326502 0.742019 -0.65699953 0.13326502 0.742019 -0.65699953 0.13326502 0.742019
		 -0.65699953 0.13326502 0.742019 0.45512876 -0.19023092 -0.86986774 0.45512876 -0.19023092
		 -0.86986774 0.45512876 -0.19023092 -0.86986774 0.45512876 -0.19023092 -0.86986774
		 0.45435104 0.1857103 -0.87125009 0.45435104 0.1857103 -0.87125009 0.45435104 0.1857103
		 -0.87125009 0.45435104 0.1857103 -0.87125009 -0.35598642 0.18143561 0.91670865 -0.35598642
		 0.18143561 0.91670865 -0.35598642 0.18143561 0.91670865 -0.35598642 0.18143561 0.91670865
		 0.41426399 0 -0.91015673 0.41426399 0 -0.91015673 0.41426399 0 -0.91015673 0.41426399
		 0 -0.91015673 -9.4249764e-08 0 -1 -9.4249764e-08 0 -1 -9.4249764e-08 0 -1 -9.4249764e-08
		 0 -1 0.096004613 0.18057568 0.97886443 0.096004613 0.18057568 0.97886443 0.096004613
		 0.18057568 0.97886443 0.096004613 0.18057568 0.97886443 0.083609588 0.17061757 -0.98178363
		 0.083609588 0.17061757 -0.98178363 0.083609588 0.17061757 -0.98178363 0.083609588
		 0.17061757 -0.98178363 0.073662631 -0.1503192 -0.98588943 0.073662631 -0.1503192
		 -0.98588943 0.073662631 -0.1503192 -0.98588943 0.073662631 -0.1503192 -0.98588943
		 0 0 1 0 0 1 0 0 1 0 0 1 0.10247489 -0.23138231 0.96745074 0.10247489 -0.23138231
		 0.96745074 0.10247489 -0.23138231 0.96745074 0.10247489 -0.23138231 0.96745074 -1
		 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.68235612 0 0.73102003 -0.68235612
		 0 0.73102003 -0.68235612 0 0.73102003 -0.68235612 0 0.73102003 -0.56024665 -0.43609378
		 0.70423436 -0.56024665 -0.43609378 0.70423436 -0.56024665 -0.43609378 0.70423436
		 -0.56024665 -0.43609378 0.70423436 0.69384295 -0.1397948 -0.70642716 0.69384295 -0.1397948
		 -0.70642716 0.69384295 -0.1397948 -0.70642716 0.69384295 -0.1397948 -0.70642716 0.69374049
		 0.13672072 -0.707129 0.69374049 0.13672072 -0.707129 0.69374049 0.13672072 -0.707129
		 0.69374049 0.13672072 -0.707129 -0.68235552 0 0.73102045 -0.68235552 0 0.73102045
		 -0.68235552 0 0.73102045 -0.68235552 0 0.73102045;
	setAttr ".n[332:497]" -type "float3"  0.65725535 0.16613652 -0.73512864 0.65725535
		 0.16613652 -0.73512864 0.65725535 0.16613652 -0.73512864 0.65725535 0.16613652 -0.73512864
		 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.69063729 -0.22169262 0.68838394 -0.69063729 -0.22169262
		 0.68838394 -0.69063729 -0.22169262 0.68838394 -0.69063729 -0.22169262 0.68838394
		 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.45730376 -0.30224484
		 0.83637393 -0.45730376 -0.30224484 0.83637393 -0.45730376 -0.30224484 0.83637393
		 -0.45730376 -0.30224484 0.83637393 0 0 1 0 0 1 0 0 1 0 0 1 0 0 0.99999994 0 0 0.99999994
		 0 0 0.99999994 0 0 0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0
		 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0.41426381
		 0 -0.91015685 0.41426381 0 -0.91015685 0.41426381 0 -0.91015685 0.41426381 0 -0.91015685
		 -1 0 0 -1 0 0 -1 0 0 -1 0 0 0.68235546 0 -0.73102045 0.68235546 0 -0.73102045 0.68235546
		 0 -0.73102045 0.68235546 0 -0.73102045 -0.97386348 0.10895109 0.19929767 -0.97386348
		 0.10895109 0.19929767 -0.97386348 0.10895109 0.19929767 -0.97386348 0.10895109 0.19929767
		 0.25865385 0 0.96597004 0.25865385 0 0.96597004 0.25865385 0 0.96597004 0.25865385
		 0 0.96597004 0.2586537 0 0.96597016 0.2586537 0 0.96597016 0.2586537 0 0.96597016
		 0.2586537 0 0.96597016 -0.4142645 0 0.91015649 -0.4142645 0 0.91015649 -0.4142645
		 0 0.91015649 -0.4142645 0 0.91015649 -0.69017023 0.23011728 0.68608385 -0.69017023
		 0.23011728 0.68608385 -0.69017023 0.23011728 0.68608385 -0.69017023 0.23011728 0.68608385
		 -0.48647961 0 -0.87369198 -0.48647961 0 -0.87369198 -0.48647961 0 -0.87369198 -0.48647961
		 0 -0.87369198 -0.48647973 0 -0.87369192 -0.48647973 0 -0.87369192 -0.48647973 0 -0.87369192
		 -0.48647973 0 -0.87369192 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 -0.22065118
		 0 0.97535282 -0.22065118 0 0.97535282 -0.22065118 0 0.97535282 -0.22065118 0 0.97535282
		 -0.22065137 0 0.97535282 -0.22065137 0 0.97535282 -0.22065137 0 0.97535282 -0.22065137
		 0 0.97535282 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 -0.62536222
		 0 -0.78033465 -0.62536222 0 -0.78033465 -0.62536222 0 -0.78033465 -0.62536222 0 -0.78033465
		 -0.62536228 0 -0.78033447 -0.62536228 0 -0.78033447 -0.62536228 0 -0.78033447 -0.62536228
		 0 -0.78033447 -0.64106667 0 0.76748514 -0.64106667 0 0.76748514 -0.64106667 0 0.76748514
		 -0.64106667 0 0.76748514 -0.64106697 0 0.76748496 -0.64106697 0 0.76748496 -0.64106697
		 0 0.76748496 -0.64106697 0 0.76748496 -0.7950418 0 -0.60655469 -0.7950418 0 -0.60655469
		 -0.7950418 0 -0.60655469 -0.7950418 0 -0.60655469 -0.79504186 0 -0.60655463 -0.79504186
		 0 -0.60655463 -0.79504186 0 -0.60655463 -0.79504186 0 -0.60655463 -0.15525134 0 0.98787498
		 -0.15525134 0 0.98787498 -0.15525134 0 0.98787498 -0.15525134 0 0.98787498 -0.15525137
		 0 0.98787493 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493
		 0.30939752 0 -0.9509328 0.30939752 0 -0.9509328 0.30939752 0 -0.9509328 0.30939752
		 0 -0.9509328 0.30939761 0 -0.95093274 0.30939761 0 -0.95093274 0.30939761 0 -0.95093274
		 0.30939761 0 -0.95093274 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0.95026672
		 0 0.31143734 0.95026672 0 0.31143734 0.95026672 0 0.31143734 0.95026672 0 0.31143734
		 0 0 1 0 0 1 0 0 1 0 0 1 -0.4908025 0 0.87127084 -0.4908025 0 0.87127084 -0.4908025
		 0 0.87127084 -0.4908025 0 0.87127084 -0.4908025 0 0.87127078 -0.4908025 0 0.87127078
		 -0.4908025 0 0.87127078 -0.4908025 0 0.87127078 -0.51183718 0 0.85908246 -0.51183718
		 0 0.85908246;
	setAttr ".n[498:663]" -type "float3"  -0.51183718 0 0.85908246 -0.51183718
		 0 0.85908246 -0.51183754 0 0.85908234 -0.51183754 0 0.85908234 -0.51183754 0 0.85908234
		 -0.51183754 0 0.85908234 1.6504293e-07 0 1 1.6504293e-07 0 1 1.6504293e-07 0 1 1.6504293e-07
		 0 1 -3.1348711e-07 0 1 -3.1348711e-07 0 1 -3.1348711e-07 0 1 -3.1348711e-07 0 1 0.72122091
		 0 -0.69270521 0.72122091 0 -0.69270521 0.72122091 0 -0.69270521 0.72122091 0 -0.69270521
		 0.72122079 0 -0.69270527 0.72122079 0 -0.69270527 0.72122079 0 -0.69270527 0.72122079
		 0 -0.69270527 0.54190022 0 -0.84044278 0.54190022 0 -0.84044278 0.54190022 0 -0.84044278
		 0.54190022 0 -0.84044278 0.5419004 0 -0.84044278 0.5419004 0 -0.84044278 0.5419004
		 0 -0.84044278 0.5419004 0 -0.84044278 0.39434642 0 -0.91896182 0.39434642 0 -0.91896182
		 0.39434642 0 -0.91896182 0.39434642 0 -0.91896182 0.39434651 0 -0.91896182 0.39434651
		 0 -0.91896182 0.39434651 0 -0.91896182 0.39434651 0 -0.91896182 0.72463101 0 0.68913704
		 0.72463101 0 0.68913704 0.72463101 0 0.68913704 0.72463101 0 0.68913704 0.72463089
		 0 0.68913716 0.72463089 0 0.68913716 0.72463089 0 0.68913716 0.72463089 0 0.68913716
		 0.027897056 0 -0.99961084 0.027897056 0 -0.99961084 0.027897056 0 -0.99961084 0.027897056
		 0 -0.99961084 -0.85581863 0 -0.51727587 -0.85581863 0 -0.51727587 -0.85581863 0 -0.51727587
		 -0.85581863 0 -0.51727587 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 -0.50197947
		 0 -0.86487955 -0.50197947 0 -0.86487955 -0.50197947 0 -0.86487955 -0.50197947 0 -0.86487955
		 -0.50197899 0 -0.86487985 -0.50197899 0 -0.86487985 -0.50197899 0 -0.86487985 -0.50197899
		 0 -0.86487985 -0.92677963 0.12563223 -0.35397166 -0.92677963 0.12563223 -0.35397166
		 -0.92677963 0.12563223 -0.35397166 -0.92677963 0.12563223 -0.35397166 -1 0 0 -1 0
		 0 -1 0 0 -1 0 0 0.35747999 -0.21364163 -0.90915638 0.35747999 -0.21364163 -0.90915638
		 0.35747999 -0.21364163 -0.90915638 0.35747999 -0.21364163 -0.90915638 0.65870601
		 -0.15973206 -0.73524964 0.65870601 -0.15973206 -0.73524964 0.65870601 -0.15973206
		 -0.73524964 0.65870601 -0.15973206 -0.73524964 -0.91771126 -0.12565939 -0.37684971
		 -0.91771126 -0.12565939 -0.37684971 -0.91771126 -0.12565939 -0.37684971 -0.91771126
		 -0.12565939 -0.37684971 0.37672406 0 -0.92632562 0.37672406 0 -0.92632562 0.37672406
		 0 -0.92632562 0.37672406 0 -0.92632562 -0.97152406 -0.10809223 0.21084854 -0.97152406
		 -0.10809223 0.21084854 -0.97152406 -0.10809223 0.21084854 -0.97152406 -0.10809223
		 0.21084854 -0.79021943 0.11374673 0.60217524 -0.79021943 0.11374673 0.60217524 -0.79021943
		 0.11374673 0.60217524 -0.79021943 0.11374673 0.60217524 -0.27781716 0.17888092 -0.94383228
		 -0.27781716 0.17888092 -0.94383228 -0.27781716 0.17888092 -0.94383228 -0.27781716
		 0.17888092 -0.94383228 -0.37672356 0 0.92632574 -0.37672356 0 0.92632574 -0.37672356
		 0 0.92632574 -0.37672356 0 0.92632574 0.35455176 0.22289281 -0.90808147 0.35455176
		 0.22289281 -0.90808147 0.35455176 0.22289281 -0.90808147 0.35455176 0.22289281 -0.90808147
		 -0.37672395 0 0.92632562 -0.37672395 0 0.92632562 -0.37672395 0 0.92632562 -0.37672395
		 0 0.92632562 -0.45812365 0.31426305 0.83148146 -0.45812365 0.31426305 0.83148146
		 -0.45812365 0.31426305 0.83148146 -0.45812365 0.31426305 0.83148146 0.37672436 0
		 -0.92632544 0.37672436 0 -0.92632544 0.37672436 0 -0.92632544 0.37672436 0 -0.92632544
		 -0.37899685 -0.16213898 -0.91108304 -0.37899685 -0.16213898 -0.91108304 -0.37899685
		 -0.16213898 -0.91108304 -0.37899685 -0.16213898 -0.91108304 -0.85956061 -0.16983569
		 0.48198691 -0.85956061 -0.16983569 0.48198691 -0.85956061 -0.16983569 0.48198691
		 -0.85956061 -0.16983569 0.48198691 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0
		 1 -0.77659959 0.24887568 -0.5787521 -0.77659959 0.24887568 -0.5787521 -0.77659959
		 0.24887568 -0.5787521 -0.77659959 0.24887568 -0.5787521 -0.75639874 -0.23346254 -0.61102885
		 -0.75639874 -0.23346254 -0.61102885 -0.75639874 -0.23346254 -0.61102885 -0.75639874
		 -0.23346254 -0.61102885 -0.051668692 -0.75850111 0.64962023 -0.051668692 -0.75850111
		 0.64962023 -0.051668692 -0.75850111 0.64962023 -0.051668692 -0.75850111 0.64962023
		 -0.85581869 0 -0.51727593 -0.85581869 0 -0.51727593 -0.85581869 0 -0.51727593 -0.85581869
		 0 -0.51727593 -0.059524346 0.66058314 0.74838948 -0.059524346 0.66058314 0.74838948
		 -0.059524346 0.66058314 0.74838948 -0.059524346 0.66058314 0.74838948 0.5506708 0
		 -0.83472252 0.5506708 0 -0.83472252 0.5506708 0 -0.83472252 0.5506708 0 -0.83472252;
	setAttr ".n[664:829]" -type "float3"  0.45888314 0.85423577 -0.24435116 0.45888314
		 0.85423577 -0.24435116 0.45888314 0.85423577 -0.24435116 0.45888314 0.85423577 -0.24435116
		 0.68752456 -0.72614849 0.0042900774 0.68752456 -0.72614849 0.0042900774 0.68752456
		 -0.72614849 0.0042900774 0.68752456 -0.72614849 0.0042900774 -0.025490064 0.85475498
		 0.51840544 -0.025490064 0.85475498 0.51840544 -0.025490064 0.85475498 0.51840544
		 -0.025490064 0.85475498 0.51840544 -0.026198106 -0.8458308 0.53280753 -0.026198106
		 -0.8458308 0.53280753 -0.026198106 -0.8458308 0.53280753 -0.026198106 -0.8458308
		 0.53280753 -0.84660614 -0.098563358 0.52301353 -0.84660614 -0.098563358 0.52301353
		 -0.84660614 -0.098563358 0.52301353 -0.84660614 -0.098563358 0.52301353 0.2076997
		 0.62085015 -0.75591397 0.2076997 0.62085015 -0.75591397 0.2076997 0.62085015 -0.75591397
		 0.2076997 0.62085015 -0.75591397 -0.72547418 0.1413935 0.67356896 -0.72547418 0.1413935
		 0.67356896 -0.72547418 0.1413935 0.67356896 -0.72547418 0.1413935 0.67356896 -0.9805705
		 -0.14564352 -0.13141355 -0.9805705 -0.14564352 -0.13141355 -0.9805705 -0.14564352
		 -0.13141355 -0.9805705 -0.14564352 -0.13141355 0.5488776 -0.48979154 -0.67737567
		 0.5488776 -0.48979154 -0.67737567 0.5488776 -0.48979154 -0.67737567 0.5488776 -0.48979154
		 -0.67737567 0.21790169 -0.6011734 -0.76883632 0.21790169 -0.6011734 -0.76883632 0.21790169
		 -0.6011734 -0.76883632 0.21790169 -0.6011734 -0.76883632 -1 0 0 -1 0 0 -1 0 0 -1
		 0 0 -0.98181576 0.14529026 -0.12218201 -0.98181576 0.14529026 -0.12218201 -0.98181576
		 0.14529026 -0.12218201 -0.98181576 0.14529026 -0.12218201 0.94409466 0 0.32967439
		 0.94409466 0 0.32967439 0.94409466 0 0.32967439 0.94409466 0 0.32967439 0.94409466
		 0 0.32967457 0.94409466 0 0.32967457 0.94409466 0 0.32967457 0.94409466 0 0.32967457
		 -0.99999994 1.0541995e-06 0 -0.99999994 1.0541995e-06 0 -0.99999994 1.0541995e-06
		 0 -0.99999994 1.0541995e-06 0 0.54013216 0.5075233 -0.67132503 0.54013216 0.5075233
		 -0.67132503 0.54013216 0.5075233 -0.67132503 0.54013216 0.5075233 -0.67132503 -0.65599394
		 -0.13746002 0.74214333 -0.65599394 -0.13746002 0.74214333 -0.65599394 -0.13746002
		 0.74214333 -0.65599394 -0.13746002 0.74214333 -1 -1.0562272e-06 0 -1 -1.0562272e-06
		 0 -1 -1.0562272e-06 0 -1 -1.0562272e-06 0 0.092823274 -0.1745916 0.98025584 0.092823274
		 -0.1745916 0.98025584 0.092823274 -0.1745916 0.98025584 0.092823274 -0.1745916 0.98025584
		 0.079091087 0.17858337 0.98074085 0.079091087 0.17858337 0.98074085 0.079091087 0.17858337
		 0.98074085 0.079091087 0.17858337 0.98074085 -0.34979704 0.22713655 0.90887356 -0.34979704
		 0.22713655 0.90887356 -0.34979704 0.22713655 0.90887356 -0.34979704 0.22713655 0.90887356
		 -0.65004206 0.17777216 0.73881143 -0.65004206 0.17777216 0.73881143 -0.65004206 0.17777216
		 0.73881143 -0.65004206 0.17777216 0.73881143 0.37412092 0.26945758 0.88737035 0.37412092
		 0.26945758 0.88737035 0.37412092 0.26945758 0.88737035 0.37412092 0.26945758 0.88737035
		 -0.35372275 -0.18763913 0.91633606 -0.35372275 -0.18763913 0.91633606 -0.35372275
		 -0.18763913 0.91633606 -0.35372275 -0.18763913 0.91633606 -0.043503754 0.84897465
		 0.52663988 -0.043503754 0.84897465 0.52663988 -0.043503754 0.84897465 0.52663988
		 -0.043503754 0.84897465 0.52663988 -0.044695251 -0.83979166 0.54106599 -0.044695251
		 -0.83979166 0.54106599 -0.044695251 -0.83979166 0.54106599 -0.044695251 -0.83979166
		 0.54106599 0 -0.4988488 0.86668903 0 -0.4988488 0.86668903 0 -0.4988488 0.86668903
		 0 -0.4988488 0.86668903 0 0.5191946 0.85465604 0 0.5191946 0.85465604 0 0.5191946
		 0.85465604 0 0.5191946 0.85465604 -0.46774727 -0.43621534 0.76871884 -0.46774727
		 -0.43621534 0.76871884 -0.46774727 -0.43621534 0.76871884 -0.46774727 -0.43621534
		 0.76871884 -0.46430451 0.45480996 0.75997978 -0.46430451 0.45480996 0.75997978 -0.46430451
		 0.45480996 0.75997978 -0.46430451 0.45480996 0.75997978 -0.28664953 -0.71785045 0.63444686
		 -0.28664953 -0.71785045 0.63444686 -0.28664953 -0.71785045 0.63444686 -0.28664953
		 -0.71785045 0.63444686 -0.28785449 0.7313804 0.61824143 -0.28785449 0.7313804 0.61824143
		 -0.28785449 0.7313804 0.61824143 -0.28785449 0.7313804 0.61824143 -0.45940429 0.77299756
		 0.43751866 -0.45940429 0.77299756 0.43751866 -0.45940429 0.77299756 0.43751866 -0.45940429
		 0.77299756 0.43751866 -0.4563008 -0.76582754 0.45309785 -0.4563008 -0.76582754 0.45309785
		 -0.4563008 -0.76582754 0.45309785 -0.4563008 -0.76582754 0.45309785 0.45988092 -0.29406139
		 0.83787686 0.45988092 -0.29406139 0.83787686 0.45988092 -0.29406139 0.83787686 0.45988092
		 -0.29406139 0.83787686 -0.057080012 0.82600445 0.56076598 -0.057080012 0.82600445
		 0.56076598 -0.057080012 0.82600445 0.56076598 -0.057080012 0.82600445 0.56076598
		 -0.45422202 0.57152539 0.68340409 -0.45422202 0.57152539 0.68340409 -0.45422202 0.57152539
		 0.68340409 -0.45422202 0.57152539 0.68340409 -0.18983075 0.50975394 0.8391158 -0.18983075
		 0.50975394 0.8391158 -0.18983075 0.50975394 0.8391158 -0.18983075 0.50975394 0.8391158
		 -0.058751382 -0.81449652 0.57718617 -0.058751382 -0.81449652 0.57718617 -0.058751382
		 -0.81449652 0.57718617 -0.058751382 -0.81449652 0.57718617 -0.45714861 -0.55396342
		 0.6958015 -0.45714861 -0.55396342 0.6958015 -0.45714861 -0.55396342 0.6958015 -0.45714861
		 -0.55396342 0.6958015 -0.19240536 -0.48952833 0.85049522 -0.19240536 -0.48952833
		 0.85049522 -0.19240536 -0.48952833 0.85049522 -0.19240536 -0.48952833 0.85049522
		 0.8216936 -0.35609072 0.4449932 0.8216936 -0.35609072 0.4449932;
	setAttr ".n[830:995]" -type "float3"  0.8216936 -0.35609072 0.4449932 0.8216936
		 -0.35609072 0.4449932 0.9440946 0 0.32967448 0.9440946 0 0.32967448 0.9440946 0 0.32967448
		 0.9440946 0 0.32967448 0.62668431 0.58851224 0.5108034 0.62668431 0.58851224 0.5108034
		 0.62668431 0.58851224 0.5108034 0.62668431 0.58851224 0.5108034 -0.50197935 0 -0.86487961
		 -0.50197935 0 -0.86487961 -0.50197935 0 -0.86487961 -0.50197935 0 -0.86487961 0.25129494
		 -0.55435169 0.79343873 0.25129494 -0.55435169 0.79343873 0.25129494 -0.55435169 0.79343873
		 0.25129494 -0.55435169 0.79343873 0 0 1 0 0 1 0 0 1 0 0 1 0.027897071 0 -0.99961078
		 0.027897071 0 -0.99961078 0.027897071 0 -0.99961078 0.027897071 0 -0.99961078 0.72463089
		 0 0.68913716 0.72463089 0 0.68913716 0.72463089 0 0.68913716 0.72463089 0 0.68913716
		 0.56127322 0.44836855 0.69565648 0.56127322 0.44836855 0.69565648 0.56127322 0.44836855
		 0.69565648 0.56127322 0.44836855 0.69565648 -0.19131723 0.49820971 0.84568596 -0.19131723
		 0.49820971 0.84568596 -0.19131723 0.49820971 0.84568596 -0.19131723 0.49820971 0.84568596
		 -0.54612517 0.548648 0.63303435 -0.54612517 0.548648 0.63303435 -0.54612517 0.548648
		 0.63303435 -0.54612517 0.548648 0.63303435 -0.10236788 0.81694961 0.56755096 -0.10236788
		 0.81694961 0.56755096 -0.10236788 0.81694961 0.56755096 -0.10236788 0.81694961 0.56755096
		 0.39434659 0 -0.91896176 0.39434659 0 -0.91896176 0.39434659 0 -0.91896176 0.39434659
		 0 -0.91896176 0.54190022 0 -0.84044284 0.54190022 0 -0.84044284 0.54190022 0 -0.84044284
		 0.54190022 0 -0.84044284 0.72122085 0 -0.69270521 0.72122085 0 -0.69270521 0.72122085
		 0 -0.69270521 0.72122085 0 -0.69270521 0 0 1 0 0 1 0 0 1 0 0 1 -0.51183748 0 0.85908228
		 -0.51183748 0 0.85908228 -0.51183748 0 0.85908228 -0.51183748 0 0.85908228 -0.49080238
		 0 0.8712709 -0.49080238 0 0.8712709 -0.49080238 0 0.8712709 -0.49080238 0 0.8712709
		 -0.20333718 -0.38830385 0.89881814 -0.20333718 -0.38830385 0.89881814 -0.20333718
		 -0.38830385 0.89881814 -0.20333718 -0.38830385 0.89881814 -0.57142961 -0.44255829
		 0.69109368 -0.57142961 -0.44255829 0.69109368 -0.57142961 -0.44255829 0.69109368
		 -0.57142961 -0.44255829 0.69109368 -0.12186681 -0.72707313 0.67565751 -0.12186681
		 -0.72707313 0.67565751 -0.12186681 -0.72707313 0.67565751 -0.12186681 -0.72707313
		 0.67565751 0.33632979 -0.68518865 -0.64606416 0.33632979 -0.68518865 -0.64606416
		 0.33632979 -0.68518865 -0.64606416 0.33632979 -0.68518865 -0.64606416 0.2331102 -0.69340372
		 -0.68179971 0.2331102 -0.69340372 -0.68179971 0.2331102 -0.69340372 -0.68179971 0.2331102
		 -0.69340372 -0.68179971 0.29169142 -0.34997615 -0.89018703 0.29169142 -0.34997615
		 -0.89018703 0.29169142 -0.34997615 -0.89018703 0.29169142 -0.34997615 -0.89018703
		 0.35947514 0.62765735 -0.6905244 0.35947514 0.62765735 -0.6905244 0.35947514 0.62765735
		 -0.6905244 0.35947514 0.62765735 -0.6905244 0.27063304 0.62612253 -0.73125124 0.27063304
		 0.62612253 -0.73125124 0.27063304 0.62612253 -0.73125124 0.27063304 0.62612253 -0.73125124
		 0.29737741 0.29654592 -0.90753907 0.29737741 0.29654592 -0.90753907 0.29737741 0.29654592
		 -0.90753907 0.29737741 0.29654592 -0.90753907 0.25273135 0.78293329 -0.56845611 0.25273135
		 0.78293329 -0.56845611 0.25273135 0.78293329 -0.56845611 0.25273135 0.78293329 -0.56845611
		 0.95026678 0 0.31143728 0.95026678 0 0.31143728 0.95026678 0 0.31143728 0.95026678
		 0 0.31143728 -0.44390446 -0.63470167 -0.63253683 -0.44390446 -0.63470167 -0.63253683
		 -0.44390446 -0.63470167 -0.63253683 -0.44390446 -0.63470167 -0.63253683 0.39181772
		 0.34720746 0.85201287 0.39181772 0.34720746 0.85201287 0.39181772 0.34720746 0.85201287
		 0.39181772 0.34720746 0.85201287 -0.0051828576 -0.415898 -0.90939647 -0.0051828576
		 -0.415898 -0.90939647 -0.0051828576 -0.415898 -0.90939647 -0.0051828576 -0.415898
		 -0.90939647 0.017349906 0.3682093 -0.92958105 0.017349906 0.3682093 -0.92958105 0.017349906
		 0.3682093 -0.92958105 0.017349906 0.3682093 -0.92958105 0 0 1 0 0 1 0 0 1 0 0 1 -0.56641716
		 0.69310594 0.44584271 -0.56641716 0.69310594 0.44584271 -0.56641716 0.69310594 0.44584271
		 -0.56641716 0.69310594 0.44584271 0.30939752 0 -0.95093286 0.30939752 0 -0.95093286
		 0.30939752 0 -0.95093286 0.30939752 0 -0.95093286 -0.53141356 -0.63184059 0.5642491
		 -0.53141356 -0.63184059 0.5642491 -0.53141356 -0.63184059 0.5642491 -0.53141356 -0.63184059
		 0.5642491 -0.53741223 0.59759134 -0.59504002 -0.53741223 0.59759134 -0.59504002 -0.53741223
		 0.59759134 -0.59504002 -0.53741223 0.59759134 -0.59504002 -0.52218968 -0.65002126
		 -0.55207825 -0.52218968 -0.65002126 -0.55207825 -0.52218968 -0.65002126 -0.55207825
		 -0.52218968 -0.65002126 -0.55207825 -0.42929038 -0.63169813 0.64549774 -0.42929038
		 -0.63169813 0.64549774 -0.42929038 -0.63169813 0.64549774 -0.42929038 -0.63169813
		 0.64549774 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493
		 -0.15525137 0 0.98787493 -0.79504192 0 -0.60655445 -0.79504192 0 -0.60655445 -0.79504192
		 0 -0.60655445 -0.79504192 0 -0.60655445;
	setAttr ".n[996:1161]" -type "float3"  -0.40892199 0.72606587 0.5528211 -0.40892199
		 0.72606587 0.5528211 -0.40892199 0.72606587 0.5528211 -0.40892199 0.72606587 0.5528211
		 -0.13847256 0.52781367 -0.83799648 -0.13847256 0.52781367 -0.83799648 -0.13847256
		 0.52781367 -0.83799648 -0.13847256 0.52781367 -0.83799648 -0.13786308 -0.59742767
		 -0.78998351 -0.13786308 -0.59742767 -0.78998351 -0.13786308 -0.59742767 -0.78998351
		 -0.13786308 -0.59742767 -0.78998351 -0.47759843 -0.34749112 0.80693853 -0.47759843
		 -0.34749112 0.80693853 -0.47759843 -0.34749112 0.80693853 -0.47759843 -0.34749112
		 0.80693853 -0.64106673 0 0.76748508 -0.64106673 0 0.76748508 -0.64106673 0 0.76748508
		 -0.64106673 0 0.76748508 -0.6253624 0 -0.78033441 -0.6253624 0 -0.78033441 -0.6253624
		 0 -0.78033441 -0.6253624 0 -0.78033441 -0.46096253 0.44897771 0.76546228 -0.46096253
		 0.44897771 0.76546228 -0.46096253 0.44897771 0.76546228 -0.46096253 0.44897771 0.76546228
		 0 0.4071418 -0.91336501 0 0.4071418 -0.91336501 0 0.4071418 -0.91336501 0 0.4071418
		 -0.91336501 0 -0.47264287 -0.88125402 0 -0.47264287 -0.88125402 0 -0.47264287 -0.88125402
		 0 -0.47264287 -0.88125402 0 -0.39658928 0.91799623 0 -0.39658928 0.91799623 0 -0.39658928
		 0.91799623 0 -0.39658928 0.91799623 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0.50758594 0.86160117
		 0 0.50758594 0.86160117 0 0.50758594 0.86160117 0 0.50758594 0.86160117 -0.22065128
		 0 0.97535276 -0.22065128 0 0.97535276 -0.22065128 0 0.97535276 -0.22065128 0 0.97535276
		 0 0 1 0 0 1 0 0 1 0 0 1 -0.29519635 -0.34555593 -0.89075822 -0.29519635 -0.34555593
		 -0.89075822 -0.29519635 -0.34555593 -0.89075822 -0.29519635 -0.34555593 -0.89075822
		 -0.29958034 0.30505681 -0.90398669 -0.29958034 0.30505681 -0.90398669 -0.29958034
		 0.30505681 -0.90398669 -0.29958034 0.30505681 -0.90398669 -0.083719559 0.77487868
		 0.62654132 -0.083719559 0.77487868 0.62654132 -0.083719559 0.77487868 0.62654132
		 -0.083719559 0.77487868 0.62654132 -0.4864797 0 -0.87369192 -0.4864797 0 -0.87369192
		 -0.4864797 0 -0.87369192 -0.4864797 0 -0.87369192 -0.097145118 -0.67971778 0.72701198
		 -0.097145118 -0.67971778 0.72701198 -0.097145118 -0.67971778 0.72701198 -0.097145118
		 -0.67971778 0.72701198 -0.21172875 0.55218023 0.80639189 -0.21172875 0.55218023 0.80639189
		 -0.21172875 0.55218023 0.80639189 -0.21172875 0.55218023 0.80639189 -0.41426173 0
		 0.91015786 -0.41426173 0 0.91015786 -0.41426173 0 0.91015786 -0.41426173 0 0.91015786
		 0.25865403 0 0.96596998 0.25865403 0 0.96596998 0.25865403 0 0.96596998 0.25865403
		 0 0.96596998 -0.35244945 0.25679821 -0.89990783 -0.35244945 0.25679821 -0.89990783
		 -0.35244945 0.25679821 -0.89990783 -0.35244945 0.25679821 -0.89990783 0.46268159
		 -0.30538982 0.83226359 0.46268159 -0.30538982 0.83226359 0.46268159 -0.30538982 0.83226359
		 0.46268159 -0.30538982 0.83226359 -0.33237264 -0.29921344 -0.89442706 -0.33237264
		 -0.29921344 -0.89442706 -0.33237264 -0.29921344 -0.89442706 -0.33237264 -0.29921344
		 -0.89442706 0.68235606 0 -0.73102003 0.68235606 0 -0.73102003 0.68235606 0 -0.73102003
		 0.68235606 0 -0.73102003 -0.65699953 -0.13326482 0.742019 -0.65699953 -0.13326482
		 0.742019 -0.65699953 -0.13326482 0.742019 -0.65699953 -0.13326482 0.742019 0.45512879
		 0.19023079 -0.8698678 0.45512879 0.19023079 -0.8698678 0.45512879 0.19023079 -0.8698678
		 0.45512879 0.19023079 -0.8698678 0.4543511 -0.18571016 -0.87125015 0.4543511 -0.18571016
		 -0.87125015 0.4543511 -0.18571016 -0.87125015 0.4543511 -0.18571016 -0.87125015 -0.35598642
		 -0.18143561 0.91670865 -0.35598642 -0.18143561 0.91670865 -0.35598642 -0.18143561
		 0.91670865 -0.35598642 -0.18143561 0.91670865 0.41426405 0 -0.91015667 0.41426405
		 0 -0.91015667 0.41426405 0 -0.91015667 0.41426405 0 -0.91015667 -1.1781221e-07 0
		 -1 -1.1781221e-07 0 -1 -1.1781221e-07 0 -1 -1.1781221e-07 0 -1 0.096004613 -0.18057568
		 0.97886443 0.096004613 -0.18057568 0.97886443 0.096004613 -0.18057568 0.97886443
		 0.096004613 -0.18057568 0.97886443 0.083609551 -0.17061757 -0.98178363 0.083609551
		 -0.17061757 -0.98178363 0.083609551 -0.17061757 -0.98178363 0.083609551 -0.17061757
		 -0.98178363 0.073662631 0.1503192 -0.98588943 0.073662631 0.1503192 -0.98588943 0.073662631
		 0.1503192 -0.98588943 0.073662631 0.1503192 -0.98588943 0 0 1 0 0 1 0 0 1 0 0 1 0.10247491
		 0.23138231 0.96745074 0.10247491 0.23138231 0.96745074 0.10247491 0.23138231 0.96745074
		 0.10247491 0.23138231 0.96745074 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 -6.4358767e-07 0
		 -1 -6.4358767e-07 0 -1 -6.4358767e-07 0 -1 -6.4358767e-07 0 -0.68235612 0 0.73102003
		 -0.68235612 0 0.73102003 -0.68235612 0 0.73102003 -0.68235612 0 0.73102003 -0.56024671
		 0.43609321 0.70423448 -0.56024671 0.43609321 0.70423448 -0.56024671 0.43609321 0.70423448
		 -0.56024671 0.43609321 0.70423448 0.69384295 0.13979492 -0.70642716 0.69384295 0.13979492
		 -0.70642716;
	setAttr ".n[1162:1327]" -type "float3"  0.69384295 0.13979492 -0.70642716 0.69384295
		 0.13979492 -0.70642716 0.69374049 -0.13672084 -0.707129 0.69374049 -0.13672084 -0.707129
		 0.69374049 -0.13672084 -0.707129 0.69374049 -0.13672084 -0.707129 -0.68235552 0 0.73102045
		 -0.68235552 0 0.73102045 -0.68235552 0 0.73102045 -0.68235552 0 0.73102045 0.65725517
		 -0.16613689 -0.73512864 0.65725517 -0.16613689 -0.73512864 0.65725517 -0.16613689
		 -0.73512864 0.65725517 -0.16613689 -0.73512864 -1 6.4600823e-07 0 -1 6.4600823e-07
		 0 -1 6.4600823e-07 0 -1 6.4600823e-07 0 -0.69063729 0.22169247 0.688384 -0.69063729
		 0.22169247 0.688384 -0.69063729 0.22169247 0.688384 -0.69063729 0.22169247 0.688384
		 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.45730379 0.3022446 0.83637393 -0.45730379 0.3022446
		 0.83637393 -0.45730379 0.3022446 0.83637393 -0.45730379 0.3022446 0.83637393 -2.4998862e-08
		 0 1 -2.4998862e-08 0 1 -2.4998862e-08 0 1 -2.4998862e-08 0 1 2.4998863e-08 0 0.99999994
		 2.4998863e-08 0 0.99999994 2.4998863e-08 0 0.99999994 2.4998863e-08 0 0.99999994
		 1.1752101e-08 0 -0.99999994 1.1752101e-08 0 -0.99999994 1.1752101e-08 0 -0.99999994
		 1.1752101e-08 0 -0.99999994 3.9998181e-08 0 -0.99999994 3.9998181e-08 0 -0.99999994
		 3.9998181e-08 0 -0.99999994 3.9998181e-08 0 -0.99999994 0.41426381 0 -0.91015685
		 0.41426381 0 -0.91015685 0.41426381 0 -0.91015685 0.41426381 0 -0.91015685 -1 0 0
		 -1 0 0 -1 0 0 -1 0 0 0.68235546 0 -0.73102045 0.68235546 0 -0.73102045 0.68235546
		 0 -0.73102045 0.68235546 0 -0.73102045 -0.97386348 -0.10895092 0.19929765 -0.97386348
		 -0.10895092 0.19929765 -0.97386348 -0.10895092 0.19929765 -0.97386348 -0.10895092
		 0.19929765 0.25865385 0 0.96597004 0.25865385 0 0.96597004 0.25865385 0 0.96597004
		 0.25865385 0 0.96597004 0.25865373 0 0.96597016 0.25865373 0 0.96597016 0.25865373
		 0 0.96597016 0.25865373 0 0.96597016 -0.4142645 0 0.91015649 -0.4142645 0 0.91015649
		 -0.4142645 0 0.91015649 -0.4142645 0 0.91015649 -0.69017029 -0.2301171 0.68608385
		 -0.69017029 -0.2301171 0.68608385 -0.69017029 -0.2301171 0.68608385 -0.69017029 -0.2301171
		 0.68608385 -0.48647961 0 -0.87369198 -0.48647961 0 -0.87369198 -0.48647961 0 -0.87369198
		 -0.48647961 0 -0.87369198 -0.48647973 0 -0.87369192 -0.48647973 0 -0.87369192 -0.48647973
		 0 -0.87369192 -0.48647973 0 -0.87369192 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1
		 0 0 1 -0.22065118 0 0.97535282 -0.22065118 0 0.97535282 -0.22065118 0 0.97535282
		 -0.22065118 0 0.97535282 -0.22065137 0 0.97535282 -0.22065137 0 0.97535282 -0.22065137
		 0 0.97535282 -0.22065137 0 0.97535282 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0
		 0 -1 0 0 -1 -0.62536222 0 -0.78033465 -0.62536222 0 -0.78033465 -0.62536222 0 -0.78033465
		 -0.62536222 0 -0.78033465 -0.62536228 0 -0.78033447 -0.62536228 0 -0.78033447 -0.62536228
		 0 -0.78033447 -0.62536228 0 -0.78033447 -0.64106667 0 0.76748514 -0.64106667 0 0.76748514
		 -0.64106667 0 0.76748514 -0.64106667 0 0.76748514 -0.64106697 0 0.76748496 -0.64106697
		 0 0.76748496 -0.64106697 0 0.76748496 -0.64106697 0 0.76748496 -0.7950418 0 -0.60655469
		 -0.7950418 0 -0.60655469 -0.7950418 0 -0.60655469 -0.7950418 0 -0.60655469 -0.79504186
		 0 -0.60655463 -0.79504186 0 -0.60655463 -0.79504186 0 -0.60655463 -0.79504186 0 -0.60655463
		 -0.15525134 0 0.98787498 -0.15525134 0 0.98787498 -0.15525134 0 0.98787498 -0.15525134
		 0 0.98787498 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493 -0.15525137 0 0.98787493
		 -0.15525137 0 0.98787493 0.30939749 0 -0.9509328 0.30939749 0 -0.9509328 0.30939749
		 0 -0.9509328 0.30939749 0 -0.9509328 0.30939761 0 -0.95093274 0.30939761 0 -0.95093274
		 0.30939761 0 -0.95093274 0.30939761 0 -0.95093274 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0
		 0 1 0 0 1 0 0 1 0.95026666 0 0.31143731 0.95026666 0 0.31143731 0.95026666 0 0.31143731
		 0.95026666 0 0.31143731 0 0 1 0 0 1 0 0 1 0 0 1;
	setAttr ".n[1328:1493]" -type "float3"  -0.4908025 0 0.87127084 -0.4908025 0
		 0.87127084 -0.4908025 0 0.87127084 -0.4908025 0 0.87127084 -0.4908025 0 0.87127078
		 -0.4908025 0 0.87127078 -0.4908025 0 0.87127078 -0.4908025 0 0.87127078 -0.51183712
		 0 0.85908246 -0.51183712 0 0.85908246 -0.51183712 0 0.85908246 -0.51183712 0 0.85908246
		 -0.51183754 0 0.85908234 -0.51183754 0 0.85908234 -0.51183754 0 0.85908234 -0.51183754
		 0 0.85908234 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0.72122091 0 -0.69270521
		 0.72122091 0 -0.69270521 0.72122091 0 -0.69270521 0.72122091 0 -0.69270521 0.72122079
		 0 -0.69270527 0.72122079 0 -0.69270527 0.72122079 0 -0.69270527 0.72122079 0 -0.69270527
		 0.54190022 0 -0.84044278 0.54190022 0 -0.84044278 0.54190022 0 -0.84044278 0.54190022
		 0 -0.84044278 0.5419004 0 -0.84044278 0.5419004 0 -0.84044278 0.5419004 0 -0.84044278
		 0.5419004 0 -0.84044278 0.39434642 0 -0.91896182 0.39434642 0 -0.91896182 0.39434642
		 0 -0.91896182 0.39434642 0 -0.91896182 0.39434651 0 -0.91896182 0.39434651 0 -0.91896182
		 0.39434651 0 -0.91896182 0.39434651 0 -0.91896182 0.72463101 0 0.68913704 0.72463101
		 0 0.68913704 0.72463101 0 0.68913704 0.72463101 0 0.68913704 0.72463089 0 0.68913716
		 0.72463089 0 0.68913716 0.72463089 0 0.68913716 0.72463089 0 0.68913716 0.027897056
		 0 -0.99961084 0.027897056 0 -0.99961084 0.027897056 0 -0.99961084 0.027897056 0 -0.99961084
		 -0.85581863 0 -0.51727593 -0.85581863 0 -0.51727593 -0.85581863 0 -0.51727593 -0.85581863
		 0 -0.51727593 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 -0.50197947 0 -0.86487955
		 -0.50197947 0 -0.86487955 -0.50197947 0 -0.86487955 -0.50197947 0 -0.86487955 -0.50197899
		 0 -0.86487985 -0.50197899 0 -0.86487985 -0.50197899 0 -0.86487985 -0.50197899 0 -0.86487985
		 -0.92677969 -0.12563188 -0.35397172 -0.92677969 -0.12563188 -0.35397172 -0.92677969
		 -0.12563188 -0.35397172 -0.92677969 -0.12563188 -0.35397172 -1 0 0 -1 0 0 -1 0 0
		 -1 0 0 0.35748002 0.21364149 -0.9091565 0.35748002 0.21364149 -0.9091565 0.35748002
		 0.21364149 -0.9091565 0.35748002 0.21364149 -0.9091565 0.65870607 0.15973172 -0.7352497
		 0.65870607 0.15973172 -0.7352497 0.65870607 0.15973172 -0.7352497 0.65870607 0.15973172
		 -0.7352497 -0.91771126 0.12565939 -0.37684971 -0.91771126 0.12565939 -0.37684971
		 -0.91771126 0.12565939 -0.37684971 -0.91771126 0.12565939 -0.37684971 0.37672406
		 0 -0.92632562 0.37672406 0 -0.92632562 0.37672406 0 -0.92632562 0.37672406 0 -0.92632562
		 -0.97152406 0.10809208 0.21084854 -0.97152406 0.10809208 0.21084854 -0.97152406 0.10809208
		 0.21084854 -0.97152406 0.10809208 0.21084854 -0.79021943 -0.11374642 0.60217524 -0.79021943
		 -0.11374642 0.60217524 -0.79021943 -0.11374642 0.60217524 -0.79021943 -0.11374642
		 0.60217524 -0.27781701 -0.17888063 -0.94383228 -0.27781701 -0.17888063 -0.94383228
		 -0.27781701 -0.17888063 -0.94383228 -0.27781701 -0.17888063 -0.94383228 -0.3767235
		 0 0.92632574 -0.3767235 0 0.92632574 -0.3767235 0 0.92632574 -0.3767235 0 0.92632574
		 0.35455176 -0.22289246 -0.90808147 0.35455176 -0.22289246 -0.90808147 0.35455176
		 -0.22289246 -0.90808147 0.35455176 -0.22289246 -0.90808147 -0.37672395 0 0.92632562
		 -0.37672395 0 0.92632562 -0.37672395 0 0.92632562 -0.37672395 0 0.92632562 -0.45812362
		 -0.31426305 0.83148146 -0.45812362 -0.31426305 0.83148146 -0.45812362 -0.31426305
		 0.83148146 -0.45812362 -0.31426305 0.83148146 0.37672436 0 -0.92632544 0.37672436
		 0 -0.92632544 0.37672436 0 -0.92632544 0.37672436 0 -0.92632544 -0.37899703 0.16213948
		 -0.91108286 -0.37899703 0.16213948 -0.91108286 -0.37899703 0.16213948 -0.91108286
		 -0.37899703 0.16213948 -0.91108286 -0.85956061 0.16983569 0.48198691 -0.85956061
		 0.16983569 0.48198691 -0.85956061 0.16983569 0.48198691 -0.85956061 0.16983569 0.48198691
		 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 0 1 -0.77659959 -0.24887568 -0.5787521
		 -0.77659959 -0.24887568 -0.5787521 -0.77659959 -0.24887568 -0.5787521 -0.77659959
		 -0.24887568 -0.5787521 -0.75639874 0.23346244 -0.61102873 -0.75639874 0.23346244
		 -0.61102873 -0.75639874 0.23346244 -0.61102873 -0.75639874 0.23346244 -0.61102873
		 -0.051668692 0.75850111 0.64962023 -0.051668692 0.75850111 0.64962023 -0.051668692
		 0.75850111 0.64962023 -0.051668692 0.75850111 0.64962023 -0.85581869 0 -0.51727599
		 -0.85581869 0 -0.51727599;
	setAttr ".n[1494:1659]" -type "float3"  -0.85581869 0 -0.51727599 -0.85581869
		 0 -0.51727599 -0.059524346 -0.66058314 0.74838948 -0.059524346 -0.66058314 0.74838948
		 -0.059524346 -0.66058314 0.74838948 -0.059524346 -0.66058314 0.74838948 0.5506708
		 0 -0.83472246 0.5506708 0 -0.83472246 0.5506708 0 -0.83472246 0.5506708 0 -0.83472246
		 0.45888305 -0.85423589 -0.24435116 0.45888305 -0.85423589 -0.24435116 0.45888305
		 -0.85423589 -0.24435116 0.45888305 -0.85423589 -0.24435116 0.68752468 0.72614831
		 0.0042902906 0.68752468 0.72614831 0.0042902906 0.68752468 0.72614831 0.0042902906
		 0.68752468 0.72614831 0.0042902906 -0.025490075 -0.8547554 0.51840478 -0.025490075
		 -0.8547554 0.51840478 -0.025490075 -0.8547554 0.51840478 -0.025490075 -0.8547554
		 0.51840478 -0.026198102 0.84583068 0.53280759 -0.026198102 0.84583068 0.53280759
		 -0.026198102 0.84583068 0.53280759 -0.026198102 0.84583068 0.53280759 -0.84660614
		 0.098563083 0.52301353 -0.84660614 0.098563083 0.52301353 -0.84660614 0.098563083
		 0.52301353 -0.84660614 0.098563083 0.52301353 0.20769984 -0.62084955 -0.75591433
		 0.20769984 -0.62084955 -0.75591433 0.20769984 -0.62084955 -0.75591433 0.20769984
		 -0.62084955 -0.75591433 -0.72547418 -0.14139311 0.67356896 -0.72547418 -0.14139311
		 0.67356896 -0.72547418 -0.14139311 0.67356896 -0.72547418 -0.14139311 0.67356896
		 -0.9805705 0.14564309 -0.13141355 -0.9805705 0.14564309 -0.13141355 -0.9805705 0.14564309
		 -0.13141355 -0.9805705 0.14564309 -0.13141355 0.54887772 0.48979118 -0.67737573 0.54887772
		 0.48979118 -0.67737573 0.54887772 0.48979118 -0.67737573 0.54887772 0.48979118 -0.67737573
		 0.21790178 0.60117316 -0.76883662 0.21790178 0.60117316 -0.76883662 0.21790178 0.60117316
		 -0.76883662 0.21790178 0.60117316 -0.76883662 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.98181587
		 -0.14528985 -0.12218203 -0.98181587 -0.14528985 -0.12218203 -0.98181587 -0.14528985
		 -0.12218203 -0.98181587 -0.14528985 -0.12218203 0.94409466 0 0.32967439 0.94409466
		 0 0.32967439 0.94409466 0 0.32967439 0.94409466 0 0.32967439 0.94409466 0 0.32967457
		 0.94409466 0 0.32967457 0.94409466 0 0.32967457 0.94409466 0 0.32967457 -0.99999994
		 -9.2242459e-07 0 -0.99999994 -9.2242459e-07 0 -0.99999994 -9.2242459e-07 0 -0.99999994
		 -9.2242459e-07 0 0.54013187 -0.50752413 -0.67132467 0.54013187 -0.50752413 -0.67132467
		 0.54013187 -0.50752413 -0.67132467 0.54013187 -0.50752413 -0.67132467 -0.65599394
		 0.13745983 0.74214333 -0.65599394 0.13745983 0.74214333 -0.65599394 0.13745983 0.74214333
		 -0.65599394 0.13745983 0.74214333 -1 9.2419884e-07 0 -1 9.2419884e-07 0 -1 9.2419884e-07
		 0 -1 9.2419884e-07 0 0.092823274 0.1745916 0.98025584 0.092823274 0.1745916 0.98025584
		 0.092823274 0.1745916 0.98025584 0.092823274 0.1745916 0.98025584 0.079091087 -0.17858337
		 0.98074085 0.079091087 -0.17858337 0.98074085 0.079091087 -0.17858337 0.98074085
		 0.079091087 -0.17858337 0.98074085 -0.34979704 -0.22713655 0.90887356 -0.34979704
		 -0.22713655 0.90887356 -0.34979704 -0.22713655 0.90887356 -0.34979704 -0.22713655
		 0.90887356 -0.65004212 -0.17777193 0.73881155 -0.65004212 -0.17777193 0.73881155
		 -0.65004212 -0.17777193 0.73881155 -0.65004212 -0.17777193 0.73881155 0.37412092
		 -0.26945764 0.88737035 0.37412092 -0.26945764 0.88737035 0.37412092 -0.26945764 0.88737035
		 0.37412092 -0.26945764 0.88737035 -0.35372269 0.18763913 0.91633606 -0.35372269 0.18763913
		 0.91633606 -0.35372269 0.18763913 0.91633606 -0.35372269 0.18763913 0.91633606 -0.043503746
		 -0.84897459 0.52663976 -0.043503746 -0.84897459 0.52663976 -0.043503746 -0.84897459
		 0.52663976 -0.043503746 -0.84897459 0.52663976 -0.044695247 0.83979172 0.54106593
		 -0.044695247 0.83979172 0.54106593 -0.044695247 0.83979172 0.54106593 -0.044695247
		 0.83979172 0.54106593 0 0.4988488 0.86668903 0 0.4988488 0.86668903 0 0.4988488 0.86668903
		 0 0.4988488 0.86668903 0 -0.5191946 0.85465604 0 -0.5191946 0.85465604 0 -0.5191946
		 0.85465604 0 -0.5191946 0.85465604 -0.46774727 0.43621546 0.76871884 -0.46774727
		 0.43621546 0.76871884 -0.46774727 0.43621546 0.76871884 -0.46774727 0.43621546 0.76871884
		 -0.46430457 -0.45480996 0.75997978 -0.46430457 -0.45480996 0.75997978 -0.46430457
		 -0.45480996 0.75997978 -0.46430457 -0.45480996 0.75997978 -0.28664953 0.71785045
		 0.63444686 -0.28664953 0.71785045 0.63444686 -0.28664953 0.71785045 0.63444686 -0.28664953
		 0.71785045 0.63444686 -0.28785449 -0.73138046 0.61824143 -0.28785449 -0.73138046
		 0.61824143 -0.28785449 -0.73138046 0.61824143 -0.28785449 -0.73138046 0.61824143
		 -0.45940435 -0.77299774 0.43751845 -0.45940435 -0.77299774 0.43751845 -0.45940435
		 -0.77299774 0.43751845 -0.45940435 -0.77299774 0.43751845 -0.45630082 0.76582754
		 0.45309788 -0.45630082 0.76582754 0.45309788 -0.45630082 0.76582754 0.45309788 -0.45630082
		 0.76582754 0.45309788 0.45988086 0.29406142 0.83787674 0.45988086 0.29406142 0.83787674
		 0.45988086 0.29406142 0.83787674 0.45988086 0.29406142 0.83787674 -0.057080012 -0.82600451
		 0.56076598 -0.057080012 -0.82600451 0.56076598 -0.057080012 -0.82600451 0.56076598
		 -0.057080012 -0.82600451 0.56076598 -0.45422202 -0.57152539 0.68340409 -0.45422202
		 -0.57152539 0.68340409 -0.45422202 -0.57152539 0.68340409 -0.45422202 -0.57152539
		 0.68340409 -0.18983072 -0.50975388 0.8391158 -0.18983072 -0.50975388 0.8391158 -0.18983072
		 -0.50975388 0.8391158 -0.18983072 -0.50975388 0.8391158 -0.058751382 0.81449652 0.57718617
		 -0.058751382 0.81449652 0.57718617 -0.058751382 0.81449652 0.57718617 -0.058751382
		 0.81449652 0.57718617;
	setAttr ".n[1660:1679]" -type "float3"  -0.45714861 0.55396342 0.6958015 -0.45714861
		 0.55396342 0.6958015 -0.45714861 0.55396342 0.6958015 -0.45714861 0.55396342 0.6958015
		 -0.19240536 0.48952833 0.85049522 -0.19240536 0.48952833 0.85049522 -0.19240536 0.48952833
		 0.85049522 -0.19240536 0.48952833 0.85049522 0.8216936 0.35609069 0.4449932 0.8216936
		 0.35609069 0.4449932 0.8216936 0.35609069 0.4449932 0.8216936 0.35609069 0.4449932
		 0.94409466 0 0.32967451 0.94409466 0 0.32967451 0.94409466 0 0.32967451 0.94409466
		 0 0.32967451 0.62668431 -0.5885123 0.5108034 0.62668431 -0.5885123 0.5108034 0.62668431
		 -0.5885123 0.5108034 0.62668431 -0.5885123 0.5108034;
	setAttr -s 420 -ch 1680 ".fc[0:419]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 296 380 381 297
		f 4 4 5 6 7
		mu 0 4 140 142 180 178
		f 4 8 9 10 11
		mu 0 4 366 368 496 494
		f 4 12 13 14 15
		mu 0 4 324 326 410 408
		f 4 16 17 18 19
		mu 0 4 338 340 456 444
		f 4 20 -19 21 22
		mu 0 4 446 444 456 460
		f 4 23 24 25 26
		mu 0 4 470 467 492 494
		f 4 27 28 29 30
		mu 0 4 463 457 488 490
		f 4 31 32 33 34
		mu 0 4 454 452 472 486
		f 4 35 36 37 38
		mu 0 4 308 310 394 392
		f 4 39 40 41 42
		mu 0 4 312 314 398 396
		f 4 43 44 45 46
		mu 0 4 316 318 402 400
		f 4 47 48 49 50
		mu 0 4 346 348 472 466
		f 4 51 52 53 54
		mu 0 4 358 360 488 486
		f 4 55 56 57 58
		mu 0 4 362 364 492 490
		f 4 59 60 61 62
		mu 0 4 76 78 86 84
		f 4 63 64 65 66
		mu 0 4 92 94 102 100
		f 4 67 68 69 70
		mu 0 4 108 110 118 116
		f 4 71 72 73 74
		mu 0 4 104 106 114 112
		f 4 75 76 77 78
		mu 0 4 88 90 98 96
		f 4 79 80 81 82
		mu 0 4 72 74 82 80
		f 4 -46 83 84 85
		mu 0 4 400 402 416 418
		f 4 -42 86 87 88
		mu 0 4 396 398 415 419
		f 4 -38 89 90 91
		mu 0 4 392 394 414 420
		f 4 -15 92 93 94
		mu 0 4 408 410 412 427
		f 4 95 96 -93 -14
		mu 0 4 326 328 412 410
		f 4 97 98 99 100
		mu 0 4 136 138 176 174
		f 4 101 102 103 104
		mu 0 4 473 496 497 474
		f 4 -74 105 106 107
		mu 0 4 112 114 122 120
		f 4 108 109 110 -84
		mu 0 4 402 404 417 416
		f 4 111 -51 112 113
		mu 0 4 344 346 466 462
		f 4 114 115 -50 -33
		mu 0 4 452 450 466 472
		f 4 116 117 -109 -45
		mu 0 4 318 320 404 402
		f 4 -70 118 119 120
		mu 0 4 116 118 126 124
		f 4 121 -86 122 -87
		mu 0 4 398 400 418 415
		f 4 -78 123 -72 124
		mu 0 4 96 98 106 104
		f 4 -66 125 -68 126
		mu 0 4 100 102 110 108
		f 4 127 -55 -34 -49
		mu 0 4 348 358 486 472
		f 4 128 -47 -122 -41
		mu 0 4 314 316 400 398
		f 4 129 -35 -54 -29
		mu 0 4 457 454 486 488
		f 4 130 -89 131 -90
		mu 0 4 394 396 419 414
		f 4 -82 132 -76 133
		mu 0 4 80 82 90 88
		f 4 -62 134 -64 135
		mu 0 4 84 86 94 92
		f 4 136 -59 -30 -53
		mu 0 4 360 362 490 488
		f 4 137 -43 -131 -37
		mu 0 4 310 312 396 394
		f 4 138 -31 -58 -25
		mu 0 4 467 463 490 492
		f 4 139 -92 140 141
		mu 0 4 380 392 420 413
		f 4 142 143 -80 144
		mu 0 4 24 36 74 72
		f 4 145 146 -60 147
		mu 0 4 48 60 78 76
		f 4 148 -39 -140 -1
		mu 0 4 296 308 392 380
		f 4 149 -27 -11 -102
		mu 0 4 473 470 494 496
		f 4 150 -12 -26 -57
		mu 0 4 364 366 494 492
		f 4 151 -114 152 153
		mu 0 4 342 344 462 460
		f 4 -107 154 155 156
		mu 0 4 120 122 130 128
		f 4 157 158 159 -110
		mu 0 4 404 406 421 417
		f 4 160 161 -113 -116
		mu 0 4 450 448 462 466
		f 4 162 163 -158 -118
		mu 0 4 320 322 406 404
		f 4 -120 164 165 166
		mu 0 4 124 126 134 132
		f 4 167 168 169 170
		mu 0 4 478 501 498 475
		f 4 171 172 173 174
		mu 0 4 289 205 202 286
		f 4 175 176 177 178
		mu 0 4 284 200 201 285
		f 4 -142 179 180 -2
		mu 0 4 380 413 422 381
		f 4 181 182 183 184
		mu 0 4 48 36 37 49
		f 4 -143 185 186 -183
		mu 0 4 36 24 25 37
		f 4 187 188 189 190
		mu 0 4 298 382 384 300
		f 4 191 192 193 194
		mu 0 4 50 38 40 52
		f 4 195 196 197 198
		mu 0 4 385 426 423 382
		f 4 199 200 201 202
		mu 0 4 41 29 26 38
		f 4 203 -203 -192 204
		mu 0 4 53 41 38 50
		f 4 205 -199 -188 206
		mu 0 4 301 385 382 298
		f 4 -3 207 208 209
		mu 0 4 297 381 383 299
		f 4 -184 210 211 212
		mu 0 4 49 37 39 51
		f 4 -187 213 214 -211
		mu 0 4 37 25 27 39
		f 4 -181 215 216 -208
		mu 0 4 381 422 424 383
		f 4 -178 217 218 219
		mu 0 4 285 201 203 287
		f 4 -104 220 221 222
		mu 0 4 474 497 499 476
		f 4 223 224 225 226
		mu 0 4 20 22 64 52
		f 4 227 228 229 230
		mu 0 4 16 18 40 28
		f 4 -174 231 -225 232
		mu 0 4 286 202 204 288
		f 4 -170 233 234 235
		mu 0 4 475 498 500 477
		f 4 -198 236 237 -189
		mu 0 4 382 423 425 384
		f 4 -202 238 -230 -193
		mu 0 4 38 26 28 40
		f 4 239 240 241 -234
		mu 0 4 498 370 372 500
		f 4 242 243 244 245
		mu 0 4 349 351 331 329
		f 4 -238 246 247 -190
		mu 0 4 0 2 10 8
		f 4 248 -228 249 250
		mu 0 4 246 245 216 214
		f 4 -235 -242 251 252
		mu 0 4 4 6 14 12
		f 4 253 -251 254 255
		mu 0 4 248 246 214 217
		f 4 256 257 258 -221
		mu 0 4 497 369 371 499
		f 4 259 -220 260 -258
		mu 0 4 369 285 287 371
		f 4 261 262 263 -214
		mu 0 4 145 213 215 147
		f 4 264 -210 265 -263
		mu 0 4 213 297 299 215
		f 4 266 -255 267 -201
		mu 0 4 149 217 214 146
		f 4 268 269 -266 270
		mu 0 4 330 247 215 299
		f 4 -268 -250 -231 -239
		mu 0 4 146 214 216 148
		f 4 271 -271 272 273
		mu 0 4 332 330 299 301
		f 4 -10 274 -257 -103
		mu 0 4 496 368 369 497
		f 4 275 -179 -260 -275
		mu 0 4 368 284 285 369
		f 4 276 277 -240 -169
		mu 0 4 501 373 370 498
		f 4 -245 278 -191 -248
		mu 0 4 329 331 298 300
		f 4 -157 279 280 281
		mu 0 4 168 170 238 236
		f 4 -281 282 -163 283
		mu 0 4 236 238 322 320
		f 4 284 285 286 287
		mu 0 4 182 184 260 258
		f 4 -287 288 -152 289
		mu 0 4 258 260 344 342
		f 4 -61 290 291 292
		mu 0 4 196 198 282 280
		f 4 -292 293 -151 294
		mu 0 4 280 282 366 364
		f 4 -145 295 296 297
		mu 0 4 144 156 224 212
		f 4 -297 298 -149 299
		mu 0 4 212 224 308 296
		f 4 -134 300 301 302
		mu 0 4 158 160 228 226
		f 4 -302 303 -138 304
		mu 0 4 226 228 312 310
		f 4 -65 305 306 307
		mu 0 4 192 194 278 276
		f 4 -307 308 -137 309
		mu 0 4 276 278 362 360
		f 4 -125 310 311 312
		mu 0 4 162 164 232 230
		f 4 -312 313 -129 314
		mu 0 4 230 232 316 314
		f 4 -69 315 316 317
		mu 0 4 188 190 274 264
		f 4 -317 318 -128 319
		mu 0 4 264 274 358 348
		f 4 -108 -282 320 321
		mu 0 4 166 168 236 234
		f 4 -321 -284 -117 322
		mu 0 4 234 236 320 318
		f 4 -165 323 324 -286
		mu 0 4 184 186 262 260
		f 4 -325 325 -112 -289
		mu 0 4 260 262 346 344
		f 4 -100 326 327 328
		mu 0 4 174 176 244 242
		f 4 329 -290 330 331
		mu 0 4 256 258 342 340
		f 4 -135 -293 332 -306
		mu 0 4 194 196 280 278
		f 4 -333 -295 -56 -309
		mu 0 4 278 280 364 362
		f 4 -126 -308 333 -316
		mu 0 4 190 192 276 274
		f 4 -334 -310 -52 -319
		mu 0 4 274 276 360 358
		f 4 -119 -318 334 -324
		mu 0 4 186 188 264 262
		f 4 -335 -320 -48 -326
		mu 0 4 262 264 348 346
		f 4 -75 -322 335 -311
		mu 0 4 164 166 234 232
		f 4 -336 -323 -44 -314
		mu 0 4 232 234 318 316
		f 4 -79 -313 336 -301
		mu 0 4 160 162 230 228
		f 4 -337 -315 -40 -304
		mu 0 4 228 230 314 312
		f 4 -83 -303 337 -296
		mu 0 4 156 158 226 224
		f 4 -338 -305 -36 -299
		mu 0 4 224 226 310 308
		f 4 -7 338 339 340
		mu 0 4 178 180 256 254
		f 4 -340 -332 -17 341
		mu 0 4 254 256 340 338
		f 4 -101 -329 342 343
		mu 0 4 172 174 242 240
		f 4 344 -344 345 -280
		mu 0 4 170 172 240 238
		f 4 -147 -176 346 -291
		mu 0 4 198 200 284 282
		f 4 -347 -276 -9 -294
		mu 0 4 282 284 368 366
		f 4 -298 347 -262 -186
		mu 0 4 144 212 213 145
		f 4 -300 -4 -265 -348
		mu 0 4 212 296 297 213
		f 4 348 349 -272 350
		mu 0 4 352 350 330 332
		f 4 351 352 -269 -350
		mu 0 4 350 267 247 330
		f 4 353 354 -254 355
		mu 0 4 268 266 246 248
		f 4 356 357 -249 -355
		mu 0 4 266 265 245 246
		f 4 358 -356 359 -353
		mu 0 4 267 268 248 247
		f 4 -264 360 -267 361
		mu 0 4 147 215 217 149
		f 4 -360 -256 -361 -270
		mu 0 4 247 248 217 215
		f 4 -212 362 -204 363
		mu 0 4 51 39 41 53
		f 4 -215 -362 -200 -363
		mu 0 4 39 27 29 41
		f 4 -219 364 -172 365
		mu 0 4 287 203 205 289
		f 4 366 -351 367 -244
		mu 0 4 351 352 332 331
		f 4 -259 368 -277 369
		mu 0 4 499 371 373 501
		f 4 -368 -274 -207 -279
		mu 0 4 331 332 301 298
		f 4 -209 370 -206 -273
		mu 0 4 299 383 385 301
		f 4 -217 371 -196 -371
		mu 0 4 383 424 426 385
		f 4 -222 -370 -168 372
		mu 0 4 476 499 501 478
		f 4 -6 -288 -330 -339
		mu 0 4 180 182 258 256
		f 4 -331 -154 -22 -18
		mu 0 4 340 342 460 456
		f 4 -156 373 -98 -345
		mu 0 4 128 130 138 136
		f 4 374 -95 375 -159
		mu 0 4 406 408 427 421
		f 4 376 -23 -153 -162
		mu 0 4 448 446 460 462
		f 4 377 -16 -375 -164
		mu 0 4 322 324 408 406
		f 4 -166 -285 -5 378
		mu 0 4 132 134 142 140
		f 4 -283 379 380 381
		mu 0 4 322 238 244 328
		f 4 -378 -382 -96 -13
		mu 0 4 324 322 328 326
		f 4 -346 -343 -328 -380
		mu 0 4 238 240 242 244
		f 4 382 -379 383 -374
		mu 0 4 130 132 140 138
		f 4 -376 384 -377 385
		mu 0 4 421 427 446 448
		f 4 386 -373 387 -372
		mu 0 4 424 476 478 426
		f 4 -278 388 -367 389
		mu 0 4 370 373 352 351
		f 4 390 -364 391 -365
		mu 0 4 63 51 53 65
		f 4 -366 392 -359 393
		mu 0 4 287 289 268 267
		f 4 -233 -224 -357 394
		mu 0 4 286 288 265 266
		f 4 -175 -395 -354 -393
		mu 0 4 289 286 266 268
		f 4 -261 -394 -352 395
		mu 0 4 371 287 267 350
		f 4 -369 -396 -349 -389
		mu 0 4 373 371 350 352
		f 4 396 -342 397 -381
		mu 0 4 244 254 338 328
		f 4 398 -341 -397 -327
		mu 0 4 176 178 254 244
		f 4 399 -253 -246 -247
		mu 0 4 2 4 12 10
		f 4 -241 -390 -243 -252
		mu 0 4 372 370 351 349
		f 4 400 -236 -400 -237
		mu 0 4 423 475 477 425
		f 4 -358 -227 -194 -229
		mu 0 4 18 20 52 40
		f 4 401 -223 -387 -216
		mu 0 4 422 474 476 424
		f 4 402 -213 -391 -218
		mu 0 4 61 49 51 63
		f 4 -392 -205 403 -173
		mu 0 4 65 53 50 62
		f 4 -404 -195 -226 -232
		mu 0 4 62 50 52 64
		f 4 -146 -185 -403 -177
		mu 0 4 60 48 49 61
		f 4 -388 -171 -401 -197
		mu 0 4 426 478 475 423
		f 4 404 -167 -383 -155
		mu 0 4 122 124 132 130
		f 4 -160 -386 -161 405
		mu 0 4 417 421 448 450
		f 4 -141 406 -150 407
		mu 0 4 413 420 470 473
		f 4 -182 -148 408 -144
		mu 0 4 36 48 76 74
		f 4 -132 409 -139 410
		mu 0 4 414 419 463 467
		f 4 411 -136 412 -133
		mu 0 4 82 84 92 90
		f 4 -123 413 -130 414
		mu 0 4 415 418 454 457
		f 4 415 -127 416 -124
		mu 0 4 98 100 108 106
		f 4 417 -121 -405 -106
		mu 0 4 114 116 124 122
		f 4 -111 -406 -115 418
		mu 0 4 416 417 450 452
		f 4 -408 -105 -402 -180
		mu 0 4 413 473 474 422
		f 4 -417 -71 -418 -73
		mu 0 4 106 108 116 114
		f 4 -413 -67 -416 -77
		mu 0 4 90 92 100 98
		f 4 -409 -63 -412 -81
		mu 0 4 74 76 84 82
		f 4 -85 -419 -32 -414
		mu 0 4 418 416 452 454
		f 4 -88 -415 -28 -410
		mu 0 4 419 415 457 463
		f 4 -91 -411 -24 -407
		mu 0 4 420 414 467 470
		f 4 -94 419 -21 -385
		mu 0 4 427 412 444 446
		f 4 -398 -20 -420 -97
		mu 0 4 328 338 444 412
		f 4 -384 -8 -399 -99
		mu 0 4 138 140 178 176
		f 4 420 421 422 423
		mu 0 4 302 303 387 386
		f 4 424 425 426 427
		mu 0 4 141 179 181 143
		f 4 428 429 430 431
		mu 0 4 367 495 502 374
		f 4 432 433 434 435
		mu 0 4 325 409 411 327
		f 4 436 437 438 439
		mu 0 4 339 445 458 341
		f 4 440 441 -438 442
		mu 0 4 447 461 458 445
		f 4 443 444 445 446
		mu 0 4 471 495 493 469
		f 4 447 448 449 450
		mu 0 4 465 491 489 459
		f 4 451 452 453 454
		mu 0 4 455 487 479 453
		f 4 455 456 457 458
		mu 0 4 309 393 395 311
		f 4 459 460 461 462
		mu 0 4 313 397 399 315
		f 4 463 464 465 466
		mu 0 4 317 401 403 319
		f 4 467 468 469 470
		mu 0 4 347 468 479 353
		f 4 471 472 473 474
		mu 0 4 359 487 489 361
		f 4 475 476 477 478
		mu 0 4 363 491 493 365
		f 4 479 480 481 482
		mu 0 4 77 85 87 79
		f 4 483 484 485 486
		mu 0 4 93 101 103 95
		f 4 487 488 489 490
		mu 0 4 109 117 119 111
		f 4 491 492 493 494
		mu 0 4 105 113 115 107
		f 4 495 496 497 498
		mu 0 4 89 97 99 91
		f 4 499 500 501 502
		mu 0 4 73 81 83 75
		f 4 503 504 505 -465
		mu 0 4 401 434 432 403
		f 4 506 507 508 -461
		mu 0 4 397 435 431 399
		f 4 509 510 511 -457
		mu 0 4 393 436 430 395
		f 4 512 513 514 -434
		mu 0 4 409 443 428 411
		f 4 -435 -515 515 516
		mu 0 4 327 411 428 333
		f 4 517 518 519 520
		mu 0 4 137 175 177 139
		f 4 521 522 523 524
		mu 0 4 480 481 503 502
		f 4 525 526 527 -493
		mu 0 4 113 121 123 115
		f 4 -506 528 529 530
		mu 0 4 403 432 433 405
		f 4 531 532 -468 533
		mu 0 4 345 464 468 347
		f 4 -454 -469 534 535
		mu 0 4 453 479 468 451
		f 4 -466 -531 536 537
		mu 0 4 319 403 405 321
		f 4 538 539 540 -489
		mu 0 4 117 125 127 119
		f 4 -509 541 -504 542
		mu 0 4 399 431 434 401
		f 4 543 -495 544 -497
		mu 0 4 97 105 107 99
		f 4 545 -491 546 -485
		mu 0 4 101 109 111 103
		f 4 -470 -453 -472 547
		mu 0 4 353 479 487 359
		f 4 -462 -543 -464 548
		mu 0 4 315 399 401 317
		f 4 -450 -473 -452 549
		mu 0 4 459 489 487 455
		f 4 -512 550 -507 551
		mu 0 4 395 430 435 397
		f 4 552 -499 553 -501
		mu 0 4 81 89 91 83
		f 4 554 -487 555 -481
		mu 0 4 85 93 95 87
		f 4 -474 -449 -476 556
		mu 0 4 361 489 491 363
		f 4 -458 -552 -460 557
		mu 0 4 311 395 397 313
		f 4 -446 -477 -448 558
		mu 0 4 469 493 491 465
		f 4 559 560 -510 561
		mu 0 4 386 429 436 393
		f 4 562 -503 563 564
		mu 0 4 30 73 75 42
		f 4 565 -483 566 567
		mu 0 4 54 77 79 66
		f 4 -424 -562 -456 568
		mu 0 4 302 386 393 309
		f 4 -525 -430 -444 569
		mu 0 4 480 502 495 471
		f 4 -478 -445 -429 570
		mu 0 4 365 493 495 367
		f 4 571 572 -532 573
		mu 0 4 343 461 464 345
		f 4 574 575 576 -527
		mu 0 4 121 129 131 123
		f 4 -530 577 578 579
		mu 0 4 405 433 437 407
		f 4 -535 -533 580 581
		mu 0 4 451 468 464 449
		f 4 -537 -580 582 583
		mu 0 4 321 405 407 323
		f 4 584 585 586 -540
		mu 0 4 125 133 135 127
		f 4 587 588 589 590
		mu 0 4 485 482 504 507
		f 4 591 592 593 594
		mu 0 4 295 292 208 211
		f 4 595 596 597 598
		mu 0 4 290 291 207 206
		f 4 -423 599 600 -560
		mu 0 4 386 387 438 429
		f 4 601 602 603 604
		mu 0 4 54 55 43 42
		f 4 -604 605 606 -565
		mu 0 4 42 43 31 30
		f 4 607 608 609 610
		mu 0 4 304 306 390 388
		f 4 611 612 613 614
		mu 0 4 56 58 46 44
		f 4 615 616 617 618
		mu 0 4 391 388 439 442
		f 4 619 620 621 622
		mu 0 4 47 44 32 35
		f 4 623 -615 -620 624
		mu 0 4 59 56 44 47
		f 4 625 -611 -616 626
		mu 0 4 307 304 388 391
		f 4 627 628 629 -422
		mu 0 4 303 305 389 387
		f 4 630 631 632 -603
		mu 0 4 55 57 45 43
		f 4 -633 633 634 -606
		mu 0 4 43 45 33 31
		f 4 -630 635 636 -600
		mu 0 4 387 389 440 438
		f 4 637 638 639 -597
		mu 0 4 291 293 209 207
		f 4 640 641 642 -523
		mu 0 4 481 483 505 503
		f 4 643 644 645 646
		mu 0 4 21 58 70 23
		f 4 647 648 649 650
		mu 0 4 17 34 46 19
		f 4 651 -646 652 -593
		mu 0 4 292 294 210 208
		f 4 653 654 655 -589
		mu 0 4 482 484 506 504
		f 4 -610 656 657 -617
		mu 0 4 388 390 441 439
		f 4 -614 -649 658 -621
		mu 0 4 44 46 34 32
		f 4 -656 659 660 661
		mu 0 4 504 506 378 376
		f 4 662 663 664 665
		mu 0 4 354 334 336 356
		f 4 -609 666 667 -657
		mu 0 4 1 9 11 3
		f 4 668 669 -651 670
		mu 0 4 251 220 222 250
		f 4 671 672 -660 -655
		mu 0 4 5 13 15 7
		f 4 673 674 -669 675
		mu 0 4 253 223 220 251
		f 4 -643 676 677 678
		mu 0 4 503 505 377 375
		f 4 -678 679 -638 680
		mu 0 4 375 377 293 291
		f 4 -635 681 682 683
		mu 0 4 151 153 221 219
		f 4 -683 684 -628 685
		mu 0 4 219 221 305 303
		f 4 -622 686 -675 687
		mu 0 4 155 152 220 223
		f 4 688 -685 689 690
		mu 0 4 335 305 221 252
		f 4 -659 -648 -670 -687
		mu 0 4 152 154 222 220
		f 4 691 692 -689 693
		mu 0 4 337 307 305 335
		f 4 -524 -679 694 -431
		mu 0 4 502 503 375 374
		f 4 -695 -681 -596 695
		mu 0 4 374 375 291 290
		f 4 -590 -662 696 697
		mu 0 4 507 504 376 379
		f 4 -667 -608 698 -664
		mu 0 4 334 306 304 336
		f 4 699 700 701 -575
		mu 0 4 169 237 239 171
		f 4 702 -584 703 -701
		mu 0 4 237 321 323 239
		f 4 704 705 706 707
		mu 0 4 183 259 261 185
		f 4 708 -574 709 -706
		mu 0 4 259 343 345 261
		f 4 710 711 712 -482
		mu 0 4 197 281 283 199
		f 4 713 -571 714 -712
		mu 0 4 281 365 367 283
		f 4 715 716 717 -563
		mu 0 4 150 218 225 157
		f 4 718 -569 719 -717
		mu 0 4 218 302 309 225
		f 4 720 721 722 -553
		mu 0 4 159 227 229 161
		f 4 723 -558 724 -722
		mu 0 4 227 311 313 229
		f 4 725 726 727 -486
		mu 0 4 193 277 279 195
		f 4 728 -557 729 -727
		mu 0 4 277 361 363 279
		f 4 730 731 732 -544
		mu 0 4 163 231 233 165
		f 4 733 -549 734 -732
		mu 0 4 231 315 317 233
		f 4 735 736 737 -490
		mu 0 4 189 269 275 191
		f 4 738 -548 739 -737
		mu 0 4 269 353 359 275
		f 4 740 741 -700 -526
		mu 0 4 167 235 237 169
		f 4 742 -538 -703 -742
		mu 0 4 235 319 321 237
		f 4 -707 743 744 -587
		mu 0 4 185 261 263 187
		f 4 -710 -534 745 -744
		mu 0 4 261 345 347 263
		f 4 746 747 748 -519
		mu 0 4 175 243 249 177
		f 4 749 750 -709 751
		mu 0 4 257 341 343 259
		f 4 -728 752 -711 -556
		mu 0 4 195 279 281 197
		f 4 -730 -479 -714 -753
		mu 0 4 279 363 365 281
		f 4 -738 753 -726 -547
		mu 0 4 191 275 277 193
		f 4 -740 -475 -729 -754
		mu 0 4 275 359 361 277
		f 4 -745 754 -736 -541
		mu 0 4 187 263 269 189
		f 4 -746 -471 -739 -755
		mu 0 4 263 347 353 269
		f 4 -733 755 -741 -492
		mu 0 4 165 233 235 167
		f 4 -735 -467 -743 -756
		mu 0 4 233 317 319 235
		f 4 -723 756 -731 -496
		mu 0 4 161 229 231 163
		f 4 -725 -463 -734 -757
		mu 0 4 229 313 315 231
		f 4 -718 757 -721 -500
		mu 0 4 157 225 227 159
		f 4 -720 -459 -724 -758
		mu 0 4 225 309 311 227
		f 4 758 759 760 -426
		mu 0 4 179 255 257 181
		f 4 761 -440 -750 -760
		mu 0 4 255 339 341 257
		f 4 762 763 -747 -518
		mu 0 4 173 241 243 175
		f 4 -702 764 -763 765
		mu 0 4 171 239 241 173
		f 4 -713 766 -599 -567
		mu 0 4 199 283 290 206
		f 4 -715 -432 -696 -767
		mu 0 4 283 367 374 290
		f 4 -607 -684 767 -716
		mu 0 4 150 151 219 218
		f 4 -768 -686 -421 -719
		mu 0 4 218 219 303 302
		f 4 768 -694 769 770
		mu 0 4 357 337 335 355
		f 4 -770 -691 771 772
		mu 0 4 355 335 252 272
		f 4 773 -676 774 775
		mu 0 4 273 253 251 271
		f 4 -775 -671 776 777
		mu 0 4 271 251 250 270
		f 4 -772 778 -774 779
		mu 0 4 272 252 253 273
		f 4 780 -688 781 -682
		mu 0 4 153 155 223 221
		f 4 -690 -782 -674 -779
		mu 0 4 252 221 223 253
		f 4 782 -625 783 -632
		mu 0 4 57 59 47 45
		f 4 -784 -623 -781 -634
		mu 0 4 45 47 35 33
		f 4 784 -595 785 -639
		mu 0 4 293 295 211 209
		f 4 -665 786 -769 787
		mu 0 4 356 336 337 357
		f 4 788 -698 789 -677
		mu 0 4 505 507 379 377
		f 4 -699 -626 -692 -787
		mu 0 4 336 304 307 337
		f 4 -693 -627 790 -629
		mu 0 4 305 307 391 389
		f 4 -791 -619 791 -636
		mu 0 4 389 391 442 440
		f 4 792 -591 -789 -642
		mu 0 4 483 485 507 505
		f 4 -761 -752 -705 -427
		mu 0 4 181 257 259 183
		f 4 -439 -442 -572 -751
		mu 0 4 341 458 461 343
		f 4 -766 -521 793 -576
		mu 0 4 129 137 139 131
		f 4 -579 794 -513 795
		mu 0 4 407 437 443 409
		f 4 -581 -573 -441 796
		mu 0 4 449 464 461 447
		f 4 -583 -796 -433 797
		mu 0 4 323 407 409 325
		f 4 798 -428 -708 -586
		mu 0 4 133 141 143 135
		f 4 799 800 801 -704
		mu 0 4 323 333 249 239
		f 4 -436 -517 -800 -798
		mu 0 4 325 327 333 323
		f 4 -802 -748 -764 -765
		mu 0 4 239 249 243 241
		f 4 -794 802 -799 803
		mu 0 4 131 139 141 133
		f 4 804 -797 805 -795
		mu 0 4 437 449 447 443
		f 4 -792 806 -793 807
		mu 0 4 440 442 485 483
		f 4 808 -788 809 -697
		mu 0 4 376 356 357 379
		f 4 -786 810 -783 811
		mu 0 4 69 71 59 57
		f 4 812 -780 813 -785
		mu 0 4 293 272 273 295
		f 4 814 -778 -647 -652
		mu 0 4 292 271 270 294
		f 4 -814 -776 -815 -592
		mu 0 4 295 273 271 292
		f 4 815 -773 -813 -680
		mu 0 4 377 355 272 293
		f 4 -810 -771 -816 -790
		mu 0 4 379 357 355 377
		f 4 -801 816 -762 817
		mu 0 4 249 333 339 255
		f 4 -749 -818 -759 818
		mu 0 4 177 249 255 179
		f 4 -668 -663 -672 819
		mu 0 4 3 11 13 5
		f 4 -673 -666 -809 -661
		mu 0 4 378 354 356 376
		f 4 -658 -820 -654 820
		mu 0 4 439 441 484 482
		f 4 -650 -613 -644 -777
		mu 0 4 19 46 58 21
		f 4 -637 -808 -641 821
		mu 0 4 438 440 483 481
		f 4 -640 -812 -631 822
		mu 0 4 67 69 57 55
		f 4 -594 823 -624 -811
		mu 0 4 71 68 56 59
		f 4 -653 -645 -612 -824
		mu 0 4 68 70 58 56
		f 4 -598 -823 -602 -568
		mu 0 4 66 67 55 54
		f 4 -618 -821 -588 -807
		mu 0 4 442 439 482 485
		f 4 -577 -804 -585 824
		mu 0 4 123 131 133 125
		f 4 825 -582 -805 -578
		mu 0 4 433 451 449 437
		f 4 826 -570 827 -561
		mu 0 4 429 480 471 436
		f 4 -564 828 -566 -605
		mu 0 4 42 75 77 54
		f 4 829 -559 830 -551
		mu 0 4 430 469 465 435
		f 4 -554 831 -555 832
		mu 0 4 83 91 93 85
		f 4 833 -550 834 -542
		mu 0 4 431 459 455 434
		f 4 -545 835 -546 836
		mu 0 4 99 107 109 101
		f 4 -528 -825 -539 837
		mu 0 4 115 123 125 117
		f 4 838 -536 -826 -529
		mu 0 4 432 453 451 433
		f 4 -601 -822 -522 -827
		mu 0 4 429 438 481 480
		f 4 -494 -838 -488 -836
		mu 0 4 107 115 117 109
		f 4 -498 -837 -484 -832
		mu 0 4 91 99 101 93
		f 4 -502 -833 -480 -829
		mu 0 4 75 83 85 77
		f 4 -835 -455 -839 -505
		mu 0 4 434 455 453 432
		f 4 -831 -451 -834 -508
		mu 0 4 435 465 459 431
		f 4 -828 -447 -830 -511
		mu 0 4 436 471 469 430
		f 4 -806 -443 839 -514
		mu 0 4 443 447 445 428
		f 4 -516 -840 -437 -817
		mu 0 4 333 428 445 339
		f 4 -520 -819 -425 -803
		mu 0 4 139 177 179 141;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "Cube" -p "mesh";
	rename -uid "93755D5F-49E4-B457-5497-F699283771CD";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" -90.00000933466734 0 0 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 100 100 100 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "CubeShape" -p "Cube";
	rename -uid "3006072B-4C05-1441-23D3-8BBCC253A54E";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dr" 1;
	setAttr ".vcs" 2;
createNode mesh -n "CubeShapeOrig" -p "Cube";
	rename -uid "7B2F78A8-46BD-8D3D-3C8E-3792B1FA4CE5";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "UVMap";
	setAttr -s 688 ".uvst[0].uvsp";
	setAttr ".uvst[0].uvsp[0:249]" -type "float2" 0.375 0.1875 0.375 0.1875 0.375
		 0.1875 0.375 0.1875 0.375 0.1875 0.375 0.1875 0.45833331 0.1875 0.45833331 0.1875
		 0.45833331 0.1875 0.45833331 0.1875 0.45833331 0.1875 0.45833331 0.1875 0.54166663
		 0.1875 0.54166663 0.1875 0.54166663 0.1875 0.54166663 0.1875 0.54166663 0.1875 0.54166663
		 0.1875 0.625 0.1875 0.625 0.1875 0.625 0.1875 0.625 0.1875 0.625 0.1875 0.625 0.1875
		 0.375 0.203125 0.375 0.203125 0.45833331 0.203125 0.45833331 0.203125 0.54166663
		 0.203125 0.54166663 0.203125 0.625 0.203125 0.625 0.203125 0.375 0.21791732 0.375
		 0.21791732 0.45833328 0.21791732 0.45833328 0.21791732 0.62500089 0.21791734 0.62500089
		 0.21791734 0.54166663 0.21791735 0.54166663 0.21791735 0.375 0.21874999 0.375 0.21874999
		 0.45833331 0.21875 0.45833331 0.21875 0.54166663 0.21875 0.54166663 0.21875 0.625
		 0.21875 0.625 0.21875 0.54166663 0.23437499 0.54166663 0.23437499 0.375 0.234375
		 0.375 0.234375 0.45833331 0.234375 0.45833331 0.234375 0.625 0.234375 0.625 0.234375
		 0.45833331 0.23958331 0.45833331 0.23958331 0.54166663 0.23958331 0.54166663 0.23958331
		 0.625 0.23958331 0.625 0.23958331 0.375 0.23958333 0.375 0.23958333 0.375 0.24479166
		 0.375 0.24479166 0.45833331 0.24479166 0.45833331 0.24479166 0.54166663 0.24479166
		 0.54166663 0.24479166 0.625 0.24479166 0.625 0.24479166 0.45833331 0.24999999 0.45833331
		 0.24999999 0.45833331 0.24999999 0.45833331 0.24999999 0.6249994 0.24999999 0.6249994
		 0.24999999 0.62500072 0.24999999 0.62500072 0.24999999 0.62500072 0.24999999 0.62500072
		 0.24999999 0.62500072 0.24999999 0.62500072 0.24999999 0.62500072 0.24999999 0.62500072
		 0.24999999 0.62500072 0.24999999 0.62500072 0.24999999 0.62500072 0.24999999 0.62500072
		 0.24999999 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375
		 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25
		 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375
		 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25
		 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.375 0.25 0.3750006 0.25 0.3750006 0.25
		 0.45833308 0.25 0.45833308 0.25 0.45833308 0.25 0.45833308 0.25 0.45833308 0.25 0.45833308
		 0.25 0.45833308 0.25 0.45833308 0.25 0.45833308 0.25 0.45833308 0.25 0.45833308 0.25
		 0.45833308 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331
		 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25
		 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331
		 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.45833331 0.25 0.54166663 0.25
		 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663
		 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25
		 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663
		 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166663 0.25 0.54166687 0.25
		 0.54166687 0.25 0.54166687 0.25 0.54166687 0.25 0.54166687 0.25 0.54166687 0.25 0.54166687
		 0.25 0.54166687 0.25 0.54166687 0.25 0.54166687 0.25 0.54166687 0.25 0.54166687 0.25
		 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625
		 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25
		 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.625 0.25 0.54166669
		 0.25000003 0.54166669 0.25000003 0.375 0.33333331 0.375 0.33333331 0.45833331 0.33333331
		 0.45833331 0.33333331 0.54166663 0.33333331 0.54166663 0.33333331 0.625 0.33333331
		 0.625 0.33333331 0.375 0.41666666 0.375 0.41666666 0.45833331 0.41666666 0.45833331
		 0.41666666 0.54166663 0.41666666 0.54166663 0.41666666 0.625 0.41666666 0.625 0.41666666
		 0.70833325 0.49999988 0.70833325 0.49999988 0.79166663 0.49999988 0.79166663 0.49999988
		 0.625 0.49999991 0.625 0.49999991 0.87499994 0.49999991 0.87499994 0.49999991 0.45833302
		 0.49999994 0.45833302 0.49999994 0.45833331 0.49999994 0.45833331 0.49999994 0.45833331
		 0.49999994 0.45833331 0.49999994 0.54166663 0.49999994 0.54166663 0.49999994 0.70833325
		 0.49999994 0.70833325 0.49999994;
	setAttr ".uvst[0].uvsp[250:499]" 0.70833325 0.49999994 0.70833325 0.49999994
		 0.70833325 0.49999994 0.70833325 0.49999994 0.70833325 0.49999994 0.70833325 0.49999994
		 0.70833325 0.49999994 0.70833325 0.49999994 0.79166663 0.49999994 0.79166663 0.49999994
		 0.79166663 0.49999994 0.79166663 0.49999994 0.79166663 0.49999994 0.79166663 0.49999994
		 0.79166663 0.49999994 0.79166663 0.49999994 0.45833331 0.49999997 0.45833331 0.49999997
		 0.45833331 0.49999997 0.45833331 0.49999997 0.45833331 0.49999997 0.45833331 0.49999997
		 0.54166603 0.49999997 0.54166603 0.49999997 0.54166663 0.49999997 0.54166663 0.49999997
		 0.54166663 0.49999997 0.54166663 0.49999997 0.54166663 0.49999997 0.54166663 0.49999997
		 0.54166663 0.49999997 0.54166663 0.49999997 0.625 0.49999997 0.625 0.49999997 0.625
		 0.49999997 0.625 0.49999997 0.625 0.49999997 0.625 0.49999997 0.70833325 0.49999997
		 0.70833325 0.49999997 0.79166663 0.49999997 0.79166663 0.49999997 0.79166663 0.49999997
		 0.79166663 0.49999997 0.87499994 0.49999997 0.87499994 0.49999997 0.87499994 0.49999997
		 0.87499994 0.49999997 0.87499994 0.49999997 0.87499994 0.49999997 0.87499994 0.49999997
		 0.87499994 0.49999997 0.87499994 0.49999997 0.87499994 0.49999997 0.125 0.5 0.125
		 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5
		 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125
		 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5
		 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125 0.5 0.125
		 0.5 0.20833331 0.5 0.20833331 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333
		 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333
		 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333
		 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333 0.5 0.20833333
		 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666
		 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666
		 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666
		 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166666 0.5 0.29166669 0.5 0.29166669
		 0.5 0.3749994 0.5 0.3749994 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5
		 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375
		 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5 0.375 0.5
		 0.45833331 0.5 0.45833331 0.5 0.45833331 0.5 0.45833331 0.5 0.45833331 0.5 0.45833331
		 0.5 0.45833331 0.5 0.45833331 0.5 0.45833331 0.5 0.45833331 0.5 0.45833334 0.5 0.45833334
		 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663
		 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663 0.5 0.54166663
		 0.5 0.6249997 0.5 0.6249997 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5
		 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.625 0.5 0.70833331
		 0.5 0.70833331 0.5 0.70833331 0.5 0.70833331 0.5 0.70833331 0.5 0.70833331 0.5 0.70833331
		 0.5 0.70833331 0.5 0.70833331 0.5 0.70833331 0.5 0.79166663 0.5 0.79166663 0.5 0.79166663
		 0.5 0.79166663 0.5 0.79166663 0.5 0.79166663 0.5 0.79166663 0.5 0.79166663 0.5 0.79166663
		 0.5 0.79166663 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875
		 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5
		 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875 0.5 0.875
		 0.5 0.62500006 0.50000006 0.62500006 0.50000006 0.125 0.50520831 0.125 0.50520831
		 0.20833333 0.50520831 0.20833333 0.50520831;
	setAttr ".uvst[0].uvsp[500:687]" 0.29166666 0.50520831 0.29166666 0.50520831
		 0.375 0.50520831 0.375 0.50520831 0.45833331 0.50520831 0.45833331 0.50520831 0.54166663
		 0.50520831 0.54166663 0.50520831 0.625 0.50520831 0.625 0.50520831 0.70833331 0.50520831
		 0.70833331 0.50520831 0.79166663 0.50520831 0.79166663 0.50520831 0.875 0.50520831
		 0.875 0.50520831 0.125 0.51041663 0.125 0.51041663 0.20833333 0.51041663 0.20833333
		 0.51041663 0.29166666 0.51041663 0.29166666 0.51041663 0.375 0.51041663 0.375 0.51041663
		 0.45833331 0.51041663 0.45833331 0.51041663 0.54166663 0.51041663 0.54166663 0.51041663
		 0.625 0.51041663 0.625 0.51041663 0.70833331 0.51041663 0.70833331 0.51041663 0.79166663
		 0.51041663 0.79166663 0.51041663 0.875 0.51041663 0.875 0.51041663 0.125 0.515625
		 0.125 0.515625 0.20833333 0.515625 0.20833333 0.515625 0.29166666 0.515625 0.29166666
		 0.515625 0.375 0.515625 0.375 0.515625 0.45833331 0.515625 0.45833331 0.515625 0.54166663
		 0.515625 0.54166663 0.515625 0.625 0.515625 0.625 0.515625 0.70833331 0.515625 0.70833331
		 0.515625 0.79166663 0.515625 0.79166663 0.515625 0.875 0.515625 0.875 0.515625 0.125
		 0.53125 0.125 0.53125 0.20833333 0.53125 0.20833333 0.53125 0.29166666 0.53125 0.29166666
		 0.53125 0.375 0.53125 0.375 0.53125 0.45833331 0.53125 0.45833331 0.53125 0.54166663
		 0.53125 0.54166663 0.53125 0.625 0.53125 0.625 0.53125 0.70833331 0.53125 0.70833331
		 0.53125 0.79166663 0.53125 0.79166663 0.53125 0.875 0.53125 0.875 0.53125 0.125 0.53208268
		 0.125 0.53208268 0.20833334 0.53208268 0.20833334 0.53208268 0.29166669 0.53208268
		 0.29166669 0.53208268 0.375 0.53208268 0.375 0.53208268 0.45833331 0.53208268 0.45833331
		 0.53208268 0.54166663 0.53208268 0.54166663 0.53208268 0.625 0.53208268 0.625 0.53208268
		 0.70833331 0.53208268 0.70833331 0.53208268 0.79166663 0.53208268 0.79166663 0.53208268
		 0.875 0.53208268 0.875 0.53208268 0.125 0.546875 0.125 0.546875 0.20833333 0.546875
		 0.20833333 0.546875 0.29166666 0.546875 0.29166666 0.546875 0.375 0.546875 0.375
		 0.546875 0.45833331 0.546875 0.45833331 0.546875 0.54166663 0.546875 0.54166663 0.546875
		 0.625 0.546875 0.625 0.546875 0.70833331 0.546875 0.70833331 0.546875 0.79166663
		 0.546875 0.79166663 0.546875 0.875 0.546875 0.875 0.546875 0.125 0.5625 0.125 0.5625
		 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.125 0.5625 0.20833333 0.5625 0.20833333
		 0.5625 0.20833333 0.5625 0.20833333 0.5625 0.20833333 0.5625 0.20833333 0.5625 0.29166666
		 0.5625 0.29166666 0.5625 0.29166666 0.5625 0.29166666 0.5625 0.29166666 0.5625 0.29166666
		 0.5625 0.375 0.5625 0.375 0.5625 0.375 0.5625 0.375 0.5625 0.375 0.5625 0.375 0.5625
		 0.375 0.5625 0.375 0.5625 0.40277776 0.5625 0.40277776 0.5625 0.43055552 0.5625 0.43055552
		 0.5625 0.45833331 0.5625 0.45833331 0.5625 0.45833331 0.5625 0.45833331 0.5625 0.45833331
		 0.5625 0.45833331 0.5625 0.54166663 0.5625 0.54166663 0.5625 0.54166663 0.5625 0.54166663
		 0.5625 0.54166663 0.5625 0.54166663 0.5625 0.56944442 0.5625 0.56944442 0.5625 0.59722221
		 0.5625 0.59722221 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.625
		 0.5625 0.625 0.5625 0.625 0.5625 0.625 0.5625 0.70833331 0.5625 0.70833331 0.5625
		 0.70833331 0.5625 0.70833331 0.5625 0.70833331 0.5625 0.70833331 0.5625 0.79166663
		 0.5625 0.79166663 0.5625 0.79166663 0.5625 0.79166663 0.5625 0.79166663 0.5625 0.79166663
		 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625 0.875 0.5625;
	setAttr ".cuvs" -type "string" "UVMap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 568 ".vt";
	setAttr ".vt[0:165]"  -0.11243874 2.8523922 6.84914923 -0.11243874 2.8523922 7.15833759
		 0.093425527 2.8523922 6.84914923 0.093425527 2.8523922 7.15833759 -0.14397521 1.062595606 6.88802195
		 -0.14397521 1.062595606 7.34121752 0.19195487 1.062595606 6.88802195 0.19195487 1.062595606 7.34121752
		 -0.12967703 1.59150124 7.21213722 -0.094900653 2.10991883 7.034269333 -0.098507479 2.35354257 7.10197401
		 0.14844602 1.59150124 6.88802195 0.034232326 2.10991883 6.89681673 0.082032636 2.35354257 6.88100767
		 -0.098507479 2.35354257 6.88100767 -0.094900653 2.10991883 6.89681673 -0.12967703 1.59150147 6.88802195
		 0.082032636 2.35354257 7.10197401 0.034232326 2.10991883 7.034269333 0.14844602 1.59150147 7.21213722
		 -0.10176185 2.57012057 7.12032318 -0.11953452 2.60162091 7.19143486 0.085551232 2.57012057 6.88100767
		 0.10488169 2.60162091 6.84914923 -0.11953452 2.60162091 6.84914923 -0.10176185 2.57012033 6.88100767
		 0.10488169 2.60162091 7.19143486 0.085551232 2.57012033 7.12032318 -0.10516898 3.27438521 6.8578167
		 -0.10516898 3.27438521 7.078715324 0.0086865202 3.27438521 6.8578167 0.0086865202 3.27438521 7.078715324
		 -0.091436416 4.2928896 6.8578167 -0.091436416 4.2928896 7.042488575 -0.060457956 4.2928896 6.8578167
		 -0.060457956 4.2928896 7.042488575 -0.043410365 3.43778539 6.8578167 -0.037713964 3.6903131 6.8578167
		 -0.035849962 3.89713597 6.8578167 -0.035849962 3.89713597 7.042488575 -0.037713964 3.6903131 7.042488575
		 -0.043410365 3.43778539 7.042488575 -0.10530289 3.43778539 6.8578167 -0.11019167 3.6903131 6.8578167
		 -0.11178818 3.89713597 6.8578167 -0.11178818 3.89713597 7.042488575 -0.11019167 3.6903131 7.042488575
		 -0.10530289 3.43778539 7.042488575 -0.041426163 3.47873592 6.90060139 -0.041426163 3.47873592 6.99970341
		 -0.10700637 3.47873592 6.90060139 -0.10700637 3.47873592 6.99970341 -0.037713964 3.72785091 6.90060139
		 -0.037713964 3.72785091 6.99970341 -0.11019167 3.72785091 6.90060139 -0.11019167 3.72785091 6.99970341
		 -0.035849962 3.92788506 6.90060139 -0.11178818 3.92788506 6.90060139 -0.035849962 3.92788506 6.99970341
		 -0.11178818 3.92788506 6.99970341 -0.15679315 2.8523922 7.055274963 -0.15679315 2.8523922 6.95221186
		 0.14023378 2.8523922 7.055274963 0.14023378 2.8523922 6.95221186 0.29636005 1.062595606 7.19015217
		 0.29636005 1.062595606 7.039086819 -0.18832964 1.062595606 7.19015217 -0.18832964 1.062595606 7.039086819
		 0.12559573 2.35354257 7.028318882 0.12559573 2.35354257 6.9546628 0.06485603 2.10991883 6.98845148
		 0.06485603 2.10991883 6.94263458 0.22725259 1.59150147 7.094169617 0.22725259 1.59150147 6.97620153
		 -0.17403145 1.59150124 7.094169617 -0.17403145 1.59150147 6.97620153 -0.12146078 2.10991883 6.98845148
		 -0.12146078 2.10991883 6.94263458 -0.13489282 2.35354257 7.028318882 -0.13489282 2.35354257 6.9546628
		 0.15915138 2.60162091 7.077339649 0.15915138 2.60162091 6.96324444 0.13084842 2.57012033 7.040551186
		 0.13084842 2.57012033 6.96077919 -0.13941237 2.57012057 7.040551186 -0.13941237 2.57012033 6.96077919
		 -0.16464284 2.60162091 7.077339649 -0.16464284 2.60162091 6.96324444 -0.13368207 3.27438521 7.003572464
		 -0.13368207 3.27438521 6.92843056 0.030591965 3.27438521 7.003572464 0.030591965 3.27438521 6.92843056
		 -0.10002482 4.2928896 6.99363518 -0.10002482 4.2928896 6.90666962 -0.055328202 4.2928896 6.99363518
		 -0.055328202 4.2928896 6.90666962 -0.12246186 3.43778539 6.98093081 -0.12246186 3.43778539 6.91937399
		 -0.13028526 3.6903131 6.98093081 -0.13028526 3.6903131 6.91937399 -0.13284117 3.89713597 6.98093081
		 -0.13284117 3.89713597 6.91937399 -0.033161521 3.43778539 6.91937399 -0.033161521 3.43778539 6.98093128
		 -0.025712311 3.6903131 6.91937399 -0.025712311 3.6903131 6.98093128 -0.02327529 3.89713597 6.91937399
		 -0.02327529 3.89713597 6.98093128 -0.03056667 3.47873592 6.93363523 -0.03056667 3.47873592 6.96666956
		 -0.12518771 3.47873592 6.96666956 -0.12518771 3.47873592 6.93363523 -0.025712311 3.72785091 6.93363523
		 -0.025712311 3.72785091 6.96666956 -0.13028526 3.72785091 6.96666956 -0.13028526 3.72785091 6.93363523
		 -0.13284117 3.92788506 6.96666956 -0.13284117 3.92788506 6.93363523 -0.02327529 3.92788506 6.93363523
		 -0.02327529 3.92788506 6.96666956 -0.060457956 4.034769058 6.89333725 -0.091436416 4.034769058 6.89333725
		 -0.060457956 4.034769058 7.0069675446 -0.091436416 4.034769058 7.0069675446 -0.10002482 4.034769058 6.93121433
		 -0.10002482 4.034769058 6.96909094 -0.055328202 4.034769058 6.96909094 -0.055328202 4.034769058 6.93121433
		 0.041224804 2.8523922 6.82359695 -0.057784174 2.8523922 6.82359695 -0.057784189 2.8523922 7.19102383
		 0.041224796 2.8523922 7.19102383 -0.026766401 1.062595606 7.40835047 0.13479681 1.062595606 7.40835047
		 0.1347968 1.062595606 6.86979151 -0.026766416 1.062595606 6.86979151 0.038766205 2.35354257 6.86160183
		 -0.048063312 2.35354257 6.86160183 0.0027504228 2.10991883 6.8806839 -0.059355181 2.10991883 6.8806839
		 0.093491226 1.59150124 6.86979151 -0.040270116 1.59150147 6.86979151 -0.048063297 2.35354257 7.12418938
		 0.038766213 2.35354257 7.12418938 -0.059355188 2.10991883 7.044026375 0.0027504154 2.10991883 7.044026375
		 -0.040270116 1.59150124 7.26056767 0.093491226 1.59150147 7.26056767 -0.056711454 2.60162091 7.23035526
		 0.051219955 2.60162091 7.23035526 -0.04932544 2.57012057 7.14599514 0.040761493 2.57012033 7.14599514
		 0.051219955 2.60162091 6.82359695 -0.056711454 2.60162091 6.82359695 0.040761478 2.57012057 6.86160183
		 -0.04932544 2.57012033 6.86160183 -0.024166051 3.27438521 6.83389711 -0.07892406 3.27438521 6.83389711
		 -0.07892406 3.27438521 7.096403599 -0.024166051 3.27438521 7.096403599 -0.070227087 4.2928896 6.83389711
		 -0.085125953 4.2928896 6.83389711 -0.085125953 4.2928896 7.05335331 -0.070227087 4.2928896 7.05335331
		 -0.062928304 3.43778539 7.05335331 -0.092695087 3.43778539 7.05335331;
	setAttr ".vt[166:331]" -0.060569961 3.6903131 7.05335331 -0.095427603 3.6903131 7.05335331
		 -0.059797246 3.89713597 7.05335331 -0.096319199 3.89713597 7.05335331 -0.092695087 3.43778539 6.83389711
		 -0.062928304 3.43778539 6.83389711 -0.095427603 3.6903131 6.83389711 -0.060569961 3.6903131 6.83389711
		 -0.096319199 3.89713597 6.83389711 -0.059797246 3.89713597 6.83389711 -0.093647361 3.47873592 6.88474035
		 -0.062107015 3.47873592 6.88474035 -0.062107015 3.47873592 7.0025091171 -0.093647361 3.47873592 7.0025091171
		 -0.095427603 3.72785091 6.88474035 -0.060569961 3.72785091 6.88474035 -0.060569961 3.72785091 7.0025091171
		 -0.095427603 3.72785091 7.0025091171 -0.059797246 3.92788506 7.0025091171 -0.096319199 3.92788506 7.0025091171
		 -0.096319199 3.92788506 6.88474035 -0.059797246 3.92788506 6.88474035 -0.085125953 4.2928896 6.89195156
		 -0.070227087 4.2928896 6.89195156 -0.085125953 4.2928896 6.99529791 -0.070227087 4.2928896 6.99529791
		 -0.070227087 4.034769058 7.011141777 -0.085125953 4.034769058 7.011141777 -0.085125953 4.034769058 6.87610817
		 -0.070227087 4.034769058 6.87610817 -0.035849962 4.014713764 7.024341106 -0.11178818 4.014713764 7.024341106
		 -0.035849962 4.014713764 6.87596369 -0.11178818 4.014713764 6.87596369 -0.13284117 4.014713764 6.92542315
		 -0.13284117 4.014713764 6.97488213 -0.02327529 4.014713764 6.97488213 -0.02327529 4.014713764 6.92542315
		 -0.059797246 4.014713764 6.85546207 -0.096319199 4.014713764 6.85546207 -0.096319199 4.014713764 7.031787872
		 -0.059797246 4.014713764 7.031787872 -0.040202457 3.54848146 6.8864975 -0.1080564 3.54848146 6.8864975
		 -0.040202457 3.54848146 7.013807297 -0.1080564 3.54848146 7.013807297 -0.028966449 3.54848146 6.9713707
		 -0.028966449 3.54848146 6.9289341 -0.1268681 3.54848146 6.9289341 -0.1268681 3.54848146 6.9713707
		 -0.061600331 3.54848146 6.86798 -0.094234213 3.54848146 6.86798 -0.094234213 3.54848146 7.019269943
		 -0.061600331 3.54848146 7.019269943 -0.16086134 3.54974151 7.013807297 -0.11963597 3.47999597 6.99970341
		 -0.13781729 3.47999597 6.93363523 -0.11963597 3.47999597 6.90060139 -0.13781729 3.47999597 6.96666956
		 -0.16086134 3.54974151 6.8864975 -0.17967305 3.54974151 6.9289341 -0.17967305 3.54974151 6.9713707
		 -0.13686691 3.55712199 6.92424965 -0.12130795 3.55712199 6.89286709 -0.12130795 3.55712199 7.0068979263
		 -0.13686691 3.55712199 6.97495317 -0.18054132 3.55838203 6.92424965 -0.16498236 3.55838203 6.89286709
		 -0.16498236 3.55838203 7.0068979263 -0.18054132 3.55838203 6.97495317 -0.26792434 3.71337795 6.87543106
		 -0.26033416 3.71337795 6.8573513 -0.2589151 3.71337795 7.049623013 -0.26725081 3.71337795 7.029767513
		 -0.2892302 3.71463799 6.87543106 -0.28163996 3.71463799 6.8573513 -0.28231356 3.71463799 7.049623013
		 -0.29064921 3.71463799 7.029767513 0.095205575 0.73367274 7.35936308 0.14241704 0.73367274 7.30391264
		 -0.038242605 0.73367274 6.9145236 -0.13505486 0.73367274 6.92958117 0.22865374 0.73367286 7.054358006
		 0.14241704 0.73367274 6.92958117 -0.17169078 0.73367286 7.054358006 0.22865374 0.73367286 7.17913532
		 -0.13505486 0.73367274 7.30391264 -0.17169078 0.73367286 7.17913532 -0.03824259 0.73367274 7.35936308
		 0.09520556 0.73367274 6.9145236 0.057919644 0.58111775 7.24047709 0.080992013 0.58111775 7.21337843
		 -0.0072968472 0.58111775 7.023082733 -0.054609269 0.58111775 7.030441761 0.12313612 0.58111787 7.091420174
		 0.080992013 0.58111775 7.030441761 -0.072513342 0.58111787 7.091420174 0.12313612 0.58111787 7.15239954
		 -0.054609269 0.58111775 7.21337843 -0.072513342 0.58111787 7.15239954 -0.0072968472 0.58111775 7.095547199
		 -0.0072968472 0.58111775 7.16801214 -0.0072968397 0.58111775 7.24047709 0.057919636 0.58111775 7.023082733
		 0.057919636 0.58111775 7.095547199 0.057919636 0.58111775 7.16801214 -0.11439174 2.087251902 7.063524246
		 0.056092862 2.087251902 6.87861967 -0.11439174 2.087251902 6.87861967 0.056092862 2.087251902 7.063524246
		 -0.14869386 2.087251902 6.94025517 -0.14869386 2.087251902 7.0018892288 0.097286269 2.087251902 6.94025517
		 0.097286269 2.087251902 7.0018892288 0.015292875 2.087251902 7.078034401 -0.066700488 2.087251902 7.078034401
		 -0.066700488 2.087251902 6.85830069 0.01529289 2.087251902 6.85830069 -0.11243874 -2.8523922 6.84914923
		 -0.11243874 -2.8523922 7.15833759 0.093425527 -2.8523922 6.84914923 0.093425527 -2.8523922 7.15833759
		 -0.14397521 -1.062595606 6.88802195 -0.14397521 -1.062595606 7.34121752 0.19195487 -1.062595606 6.88802195
		 0.19195487 -1.062595606 7.34121752 -0.12967703 -1.59150124 7.21213722 -0.094900653 -2.10991883 7.034269333
		 -0.098507479 -2.35354257 7.10197401 0.14844602 -1.59150124 6.88802195 0.034232326 -2.10991883 6.89681673
		 0.082032636 -2.35354257 6.88100767 -0.098507479 -2.35354257 6.88100767 -0.094900653 -2.10991883 6.89681673
		 -0.12967703 -1.59150147 6.88802195 0.082032636 -2.35354257 7.10197401 0.034232326 -2.10991883 7.034269333
		 0.14844602 -1.59150147 7.21213722 -0.10176185 -2.57012057 7.12032318 -0.11953452 -2.60162091 7.19143486
		 0.085551232 -2.57012057 6.88100767 0.10488169 -2.60162091 6.84914923 -0.11953452 -2.60162091 6.84914923
		 -0.10176185 -2.57012033 6.88100767 0.10488169 -2.60162091 7.19143486 0.085551232 -2.57012033 7.12032318
		 -0.10516898 -3.27438521 6.8578167 -0.10516898 -3.27438521 7.078715324 0.0086865202 -3.27438521 6.8578167
		 0.0086865202 -3.27438521 7.078715324 -0.091436416 -4.2928896 6.8578167 -0.091436416 -4.2928896 7.042488575
		 -0.060457956 -4.2928896 6.8578167 -0.060457956 -4.2928896 7.042488575 -0.043410365 -3.43778539 6.8578167
		 -0.037713964 -3.6903131 6.8578167 -0.035849962 -3.89713597 6.8578167 -0.035849962 -3.89713597 7.042488575
		 -0.037713964 -3.6903131 7.042488575 -0.043410365 -3.43778539 7.042488575 -0.10530289 -3.43778539 6.8578167
		 -0.11019167 -3.6903131 6.8578167 -0.11178818 -3.89713597 6.8578167 -0.11178818 -3.89713597 7.042488575
		 -0.11019167 -3.6903131 7.042488575 -0.10530289 -3.43778539 7.042488575;
	setAttr ".vt[332:497]" -0.041426163 -3.47873592 6.90060139 -0.041426163 -3.47873592 6.99970341
		 -0.10700637 -3.47873592 6.90060139 -0.10700637 -3.47873592 6.99970341 -0.037713964 -3.72785091 6.90060139
		 -0.037713964 -3.72785091 6.99970341 -0.11019167 -3.72785091 6.90060139 -0.11019167 -3.72785091 6.99970341
		 -0.035849962 -3.92788506 6.90060139 -0.11178818 -3.92788506 6.90060139 -0.035849962 -3.92788506 6.99970341
		 -0.11178818 -3.92788506 6.99970341 -0.15679315 -2.8523922 7.055274963 -0.15679315 -2.8523922 6.95221186
		 0.14023378 -2.8523922 7.055274963 0.14023378 -2.8523922 6.95221186 0.29636005 -1.062595606 7.19015217
		 0.29636005 -1.062595606 7.039086819 -0.18832964 -1.062595606 7.19015217 -0.18832964 -1.062595606 7.039086819
		 0.12559573 -2.35354257 7.028318882 0.12559573 -2.35354257 6.9546628 0.06485603 -2.10991883 6.98845148
		 0.06485603 -2.10991883 6.94263458 0.22725259 -1.59150147 7.094169617 0.22725259 -1.59150147 6.97620153
		 -0.17403145 -1.59150124 7.094169617 -0.17403145 -1.59150147 6.97620153 -0.12146078 -2.10991883 6.98845148
		 -0.12146078 -2.10991883 6.94263458 -0.13489282 -2.35354257 7.028318882 -0.13489282 -2.35354257 6.9546628
		 0.15915138 -2.60162091 7.077339649 0.15915138 -2.60162091 6.96324444 0.13084842 -2.57012033 7.040551186
		 0.13084842 -2.57012033 6.96077919 -0.13941237 -2.57012057 7.040551186 -0.13941237 -2.57012033 6.96077919
		 -0.16464284 -2.60162091 7.077339649 -0.16464284 -2.60162091 6.96324444 -0.13368207 -3.27438521 7.003572464
		 -0.13368207 -3.27438521 6.92843056 0.030591965 -3.27438521 7.003572464 0.030591965 -3.27438521 6.92843056
		 -0.10002482 -4.2928896 6.99363518 -0.10002482 -4.2928896 6.90666962 -0.055328202 -4.2928896 6.99363518
		 -0.055328202 -4.2928896 6.90666962 -0.12246186 -3.43778539 6.98093081 -0.12246186 -3.43778539 6.91937399
		 -0.13028526 -3.6903131 6.98093081 -0.13028526 -3.6903131 6.91937399 -0.13284117 -3.89713597 6.98093081
		 -0.13284117 -3.89713597 6.91937399 -0.033161521 -3.43778539 6.91937399 -0.033161521 -3.43778539 6.98093128
		 -0.025712311 -3.6903131 6.91937399 -0.025712311 -3.6903131 6.98093128 -0.02327529 -3.89713597 6.91937399
		 -0.02327529 -3.89713597 6.98093128 -0.03056667 -3.47873592 6.93363523 -0.03056667 -3.47873592 6.96666956
		 -0.12518771 -3.47873592 6.96666956 -0.12518771 -3.47873592 6.93363523 -0.025712311 -3.72785091 6.93363523
		 -0.025712311 -3.72785091 6.96666956 -0.13028526 -3.72785091 6.96666956 -0.13028526 -3.72785091 6.93363523
		 -0.13284117 -3.92788506 6.96666956 -0.13284117 -3.92788506 6.93363523 -0.02327529 -3.92788506 6.93363523
		 -0.02327529 -3.92788506 6.96666956 -0.060457956 -4.034769058 6.89333725 -0.091436416 -4.034769058 6.89333725
		 -0.060457956 -4.034769058 7.0069675446 -0.091436416 -4.034769058 7.0069675446 -0.10002482 -4.034769058 6.93121433
		 -0.10002482 -4.034769058 6.96909094 -0.055328202 -4.034769058 6.96909094 -0.055328202 -4.034769058 6.93121433
		 0.041224804 -2.8523922 6.82359695 -0.057784174 -2.8523922 6.82359695 -0.057784189 -2.8523922 7.19102383
		 0.041224796 -2.8523922 7.19102383 -0.026766401 -1.062595606 7.40835047 0.13479681 -1.062595606 7.40835047
		 0.1347968 -1.062595606 6.86979151 -0.026766416 -1.062595606 6.86979151 0.038766205 -2.35354257 6.86160183
		 -0.048063312 -2.35354257 6.86160183 0.0027504228 -2.10991883 6.8806839 -0.059355181 -2.10991883 6.8806839
		 0.093491226 -1.59150124 6.86979151 -0.040270116 -1.59150147 6.86979151 -0.048063297 -2.35354257 7.12418938
		 0.038766213 -2.35354257 7.12418938 -0.059355188 -2.10991883 7.044026375 0.0027504154 -2.10991883 7.044026375
		 -0.040270116 -1.59150124 7.26056767 0.093491226 -1.59150147 7.26056767 -0.056711454 -2.60162091 7.23035526
		 0.051219955 -2.60162091 7.23035526 -0.04932544 -2.57012057 7.14599514 0.040761493 -2.57012033 7.14599514
		 0.051219955 -2.60162091 6.82359695 -0.056711454 -2.60162091 6.82359695 0.040761478 -2.57012057 6.86160183
		 -0.04932544 -2.57012033 6.86160183 -0.024166051 -3.27438521 6.83389711 -0.07892406 -3.27438521 6.83389711
		 -0.07892406 -3.27438521 7.096403599 -0.024166051 -3.27438521 7.096403599 -0.070227087 -4.2928896 6.83389711
		 -0.085125953 -4.2928896 6.83389711 -0.085125953 -4.2928896 7.05335331 -0.070227087 -4.2928896 7.05335331
		 -0.062928304 -3.43778539 7.05335331 -0.092695087 -3.43778539 7.05335331 -0.060569961 -3.6903131 7.05335331
		 -0.095427603 -3.6903131 7.05335331 -0.059797246 -3.89713597 7.05335331 -0.096319199 -3.89713597 7.05335331
		 -0.092695087 -3.43778539 6.83389711 -0.062928304 -3.43778539 6.83389711 -0.095427603 -3.6903131 6.83389711
		 -0.060569961 -3.6903131 6.83389711 -0.096319199 -3.89713597 6.83389711 -0.059797246 -3.89713597 6.83389711
		 -0.093647361 -3.47873592 6.88474035 -0.062107015 -3.47873592 6.88474035 -0.062107015 -3.47873592 7.0025091171
		 -0.093647361 -3.47873592 7.0025091171 -0.095427603 -3.72785091 6.88474035 -0.060569961 -3.72785091 6.88474035
		 -0.060569961 -3.72785091 7.0025091171 -0.095427603 -3.72785091 7.0025091171 -0.059797246 -3.92788506 7.0025091171
		 -0.096319199 -3.92788506 7.0025091171 -0.096319199 -3.92788506 6.88474035 -0.059797246 -3.92788506 6.88474035
		 -0.085125953 -4.2928896 6.89195156 -0.070227087 -4.2928896 6.89195156 -0.085125953 -4.2928896 6.99529791
		 -0.070227087 -4.2928896 6.99529791 -0.070227087 -4.034769058 7.011141777 -0.085125953 -4.034769058 7.011141777
		 -0.085125953 -4.034769058 6.87610817 -0.070227087 -4.034769058 6.87610817 -0.035849962 -4.014713764 7.024341106
		 -0.11178818 -4.014713764 7.024341106 -0.035849962 -4.014713764 6.87596369 -0.11178818 -4.014713764 6.87596369
		 -0.13284117 -4.014713764 6.92542315 -0.13284117 -4.014713764 6.97488213 -0.02327529 -4.014713764 6.97488213
		 -0.02327529 -4.014713764 6.92542315 -0.059797246 -4.014713764 6.85546207 -0.096319199 -4.014713764 6.85546207
		 -0.096319199 -4.014713764 7.031787872 -0.059797246 -4.014713764 7.031787872 -0.040202457 -3.54848146 6.8864975
		 -0.1080564 -3.54848146 6.8864975 -0.040202457 -3.54848146 7.013807297 -0.1080564 -3.54848146 7.013807297
		 -0.028966449 -3.54848146 6.9713707 -0.028966449 -3.54848146 6.9289341;
	setAttr ".vt[498:567]" -0.1268681 -3.54848146 6.9289341 -0.1268681 -3.54848146 6.9713707
		 -0.061600331 -3.54848146 6.86798 -0.094234213 -3.54848146 6.86798 -0.094234213 -3.54848146 7.019269943
		 -0.061600331 -3.54848146 7.019269943 -0.16086134 -3.54974151 7.013807297 -0.11963597 -3.47999597 6.99970341
		 -0.13781729 -3.47999597 6.93363523 -0.11963597 -3.47999597 6.90060139 -0.13781729 -3.47999597 6.96666956
		 -0.16086134 -3.54974151 6.8864975 -0.17967305 -3.54974151 6.9289341 -0.17967305 -3.54974151 6.9713707
		 -0.13686691 -3.55712199 6.92424965 -0.12130795 -3.55712199 6.89286709 -0.12130795 -3.55712199 7.0068979263
		 -0.13686691 -3.55712199 6.97495317 -0.18054132 -3.55838203 6.92424965 -0.16498236 -3.55838203 6.89286709
		 -0.16498236 -3.55838203 7.0068979263 -0.18054132 -3.55838203 6.97495317 -0.26792434 -3.71337795 6.87543106
		 -0.26033416 -3.71337795 6.8573513 -0.2589151 -3.71337795 7.049623013 -0.26725081 -3.71337795 7.029767513
		 -0.2892302 -3.71463799 6.87543106 -0.28163996 -3.71463799 6.8573513 -0.28231356 -3.71463799 7.049623013
		 -0.29064921 -3.71463799 7.029767513 0.095205575 -0.73367274 7.35936308 0.14241704 -0.73367274 7.30391264
		 -0.038242605 -0.73367274 6.9145236 -0.13505486 -0.73367274 6.92958117 0.22865374 -0.73367286 7.054358006
		 0.14241704 -0.73367274 6.92958117 -0.17169078 -0.73367286 7.054358006 0.22865374 -0.73367286 7.17913532
		 -0.13505486 -0.73367274 7.30391264 -0.17169078 -0.73367286 7.17913532 -0.03824259 -0.73367274 7.35936308
		 0.09520556 -0.73367274 6.9145236 0.057919644 -0.58111775 7.24047709 0.080992013 -0.58111775 7.21337843
		 -0.0072968472 -0.58111775 7.023082733 -0.054609269 -0.58111775 7.030441761 0.12313612 -0.58111787 7.091420174
		 0.080992013 -0.58111775 7.030441761 -0.072513342 -0.58111787 7.091420174 0.12313612 -0.58111787 7.15239954
		 -0.054609269 -0.58111775 7.21337843 -0.072513342 -0.58111787 7.15239954 -0.0072968472 -0.58111775 7.095547199
		 -0.0072968472 -0.58111775 7.16801214 -0.0072968397 -0.58111775 7.24047709 0.057919636 -0.58111775 7.023082733
		 0.057919636 -0.58111775 7.095547199 0.057919636 -0.58111775 7.16801214 -0.11439174 -2.087251902 7.063524246
		 0.056092862 -2.087251902 6.87861967 -0.11439174 -2.087251902 6.87861967 0.056092862 -2.087251902 7.063524246
		 -0.14869386 -2.087251902 6.94025517 -0.14869386 -2.087251902 7.0018892288 0.097286269 -2.087251902 6.94025517
		 0.097286269 -2.087251902 7.0018892288 0.015292875 -2.087251902 7.078034401 -0.066700488 -2.087251902 7.078034401
		 -0.066700488 -2.087251902 6.85830069 0.01529289 -2.087251902 6.85830069;
	setAttr -s 1128 ".ed";
	setAttr ".ed[0:165]"  86 21 0 21 1 0 1 60 0 60 86 0 63 2 0 2 30 0 30 91 0
		 91 63 0 146 8 0 8 5 0 5 132 0 132 146 0 140 11 0 11 6 0 6 134 0 134 140 0 72 19 0
		 19 7 0 7 64 0 64 72 0 65 64 0 64 251 0 251 248 0 248 65 0 82 27 0 27 17 0 17 68 0
		 68 82 0 17 18 0 18 70 0 70 68 0 279 275 0 275 19 0 72 279 0 154 22 0 22 13 0 13 136 0
		 136 154 0 13 12 0 12 138 0 138 136 0 283 273 0 273 11 0 140 283 0 150 20 0 20 10 0
		 10 142 0 142 150 0 10 9 0 9 144 0 144 142 0 281 272 0 272 8 0 146 281 0 66 5 0 8 74 0
		 74 66 0 277 272 0 272 9 0 9 76 0 76 277 0 10 78 0 78 76 0 130 1 0 21 148 0 148 130 0
		 21 20 0 150 148 0 128 2 0 2 23 0 23 152 0 152 128 0 23 22 0 154 152 0 62 3 0 3 26 0
		 26 80 0 80 62 0 26 27 0 82 80 0 20 84 0 84 78 0 86 84 0 127 120 0 120 34 0 34 95 0
		 95 127 0 129 0 0 0 28 0 28 157 0 157 129 0 131 3 0 3 31 0 31 159 0 159 131 0 1 29 0
		 29 88 0 88 60 0 191 163 0 163 35 0 35 94 0 94 191 0 194 121 0 121 32 0 32 161 0 161 194 0
		 192 122 0 122 35 0 163 192 0 125 123 0 123 33 0 33 92 0 92 125 0 29 47 0 47 96 0
		 96 88 0 215 211 0 211 46 0 46 98 0 98 215 0 114 55 0 55 45 0 45 100 0 100 114 0 31 41 0
		 41 164 0 164 159 0 219 210 0 210 40 0 40 166 0 166 219 0 182 53 0 53 39 0 39 168 0
		 168 182 0 28 42 0 42 170 0 170 157 0 217 209 0 209 43 0 43 172 0 172 217 0 180 54 0
		 54 44 0 44 174 0 174 180 0 30 36 0 36 102 0 102 91 0 213 208 0 208 37 0 37 104 0
		 104 213 0 112 52 0 52 38 0 38 106 0 106 112 0 36 48 0 48 108 0 108 102 0 42 50 0
		 50 176 0 176 170 0 41 49 0 49 178 0 178 164 0;
	setAttr ".ed[166:331]" 47 51 0 51 110 0 110 96 0 37 52 0 112 104 0 43 54 0
		 180 172 0 40 53 0 182 166 0 46 55 0 114 98 0 45 59 0 59 116 0 116 100 0 39 58 0 58 184 0
		 184 168 0 44 57 0 57 186 0 186 174 0 38 56 0 56 118 0 118 106 0 39 107 0 107 119 0
		 119 58 0 107 106 0 118 119 0 44 101 0 101 117 0 117 57 0 101 100 0 116 117 0 43 99 0
		 99 115 0 115 54 0 99 98 0 114 115 0 40 105 0 105 113 0 113 53 0 105 104 0 112 113 0
		 42 97 0 97 111 0 111 50 0 97 96 0 110 111 0 41 103 0 103 109 0 109 49 0 103 102 0
		 108 109 0 113 107 0 210 212 0 212 105 0 212 213 0 31 90 0 90 103 0 90 91 0 115 101 0
		 209 214 0 214 99 0 214 215 0 28 89 0 89 97 0 89 88 0 121 124 0 124 93 0 93 32 0 160 189 0
		 189 95 0 34 160 0 0 61 0 61 89 0 61 60 0 122 126 0 126 94 0 25 85 0 85 87 0 87 24 0
		 24 25 0 85 84 0 86 87 0 14 79 0 79 85 0 25 14 0 79 78 0 23 81 0 81 83 0 83 22 0 81 80 0
		 82 83 0 63 81 0 63 62 0 15 77 0 77 79 0 14 15 0 77 76 0 274 276 0 276 77 0 15 274 0
		 276 277 0 4 67 0 67 75 0 75 16 0 16 4 0 67 66 0 74 75 0 273 278 0 278 73 0 73 11 0
		 278 279 0 72 73 0 13 69 0 69 71 0 71 12 0 69 68 0 70 71 0 83 69 0 135 134 0 134 255 0
		 255 246 0 246 135 0 7 133 0 133 244 0 244 245 0 245 7 0 73 65 0 65 6 0 62 90 0 87 61 0
		 0 24 0 202 203 0 203 127 0 127 126 0 126 202 0 196 202 0 122 196 0 200 201 0 201 125 0
		 125 124 0 124 200 0 199 200 0 121 199 0 201 197 0 197 123 0 207 196 0 192 207 0 205 199 0
		 194 205 0 203 198 0 198 120 0 198 204 0 204 195 0 195 120 0 204 205 0 194 195 0 197 206 0
		 206 193 0 193 123 0 206 207 0 192 193 0 245 251 0 6 249 0 249 255 0;
	setAttr ".ed[332:497]" 4 247 0 247 250 0 250 67 0 250 253 0 253 66 0 93 188 0
		 188 161 0 188 189 0 160 161 0 38 175 0 175 187 0 187 56 0 175 174 0 186 187 0 45 169 0
		 169 185 0 185 59 0 169 168 0 184 185 0 46 167 0 167 183 0 183 55 0 167 166 0 182 183 0
		 37 173 0 173 181 0 181 52 0 173 172 0 180 181 0 47 165 0 165 179 0 179 51 0 165 164 0
		 178 179 0 36 171 0 171 177 0 177 48 0 171 170 0 176 177 0 181 175 0 208 216 0 216 173 0
		 216 217 0 30 156 0 156 171 0 156 157 0 183 169 0 211 218 0 218 167 0 218 219 0 29 158 0
		 158 165 0 158 159 0 193 162 0 162 33 0 163 162 0 195 160 0 162 190 0 190 92 0 191 190 0
		 130 158 0 130 131 0 128 156 0 128 129 0 24 153 0 153 155 0 155 25 0 153 152 0 154 155 0
		 129 153 0 26 149 0 149 151 0 151 27 0 149 148 0 150 151 0 131 149 0 275 280 0 280 147 0
		 147 19 0 280 281 0 146 147 0 17 143 0 143 145 0 145 18 0 143 142 0 144 145 0 151 143 0
		 274 282 0 282 141 0 141 16 0 16 274 0 282 283 0 140 141 0 14 137 0 137 139 0 139 15 0
		 137 136 0 138 139 0 155 137 0 133 132 0 132 254 0 254 244 0 248 249 0 141 135 0 135 4 0
		 147 133 0 125 126 0 127 124 0 92 94 0 95 93 0 184 207 0 206 185 0 197 59 0 186 205 0
		 204 187 0 198 56 0 203 118 0 57 199 0 58 196 0 201 116 0 117 200 0 119 202 0 178 219 0
		 218 179 0 211 51 0 176 217 0 216 177 0 208 48 0 214 226 0 226 227 0 227 215 0 110 224 0
		 224 222 0 222 111 0 108 213 0 212 109 0 210 49 0 50 209 0 222 223 0 223 50 0 224 227 0
		 226 222 0 226 225 0 225 223 0 224 221 0 221 220 0 220 227 0 211 220 0 221 51 0 214 228 0
		 228 232 0 232 226 0 225 209 0 211 230 0 230 234 0 234 220 0 228 236 0 236 240 0 240 232 0
		 230 238 0 238 242 0 242 234 0 234 235 0 235 227 0 235 231 0 231 215 0;
	setAttr ".ed[498:663]" 231 230 0 232 233 0 233 225 0 233 229 0 229 209 0 229 228 0
		 238 239 0 239 243 0 243 242 0 236 237 0 237 241 0 241 240 0 241 233 0 229 237 0 235 243 0
		 239 231 0 254 252 0 252 264 0 264 268 0 268 254 0 247 259 0 259 262 0 262 250 0 255 269 0
		 269 258 0 258 246 0 252 253 0 253 265 0 265 264 0 248 260 0 260 261 0 261 249 0 261 269 0
		 251 263 0 263 260 0 247 246 0 258 259 0 268 256 0 256 244 0 5 252 0 271 263 0 263 257 0
		 257 256 0 256 271 0 260 270 0 270 269 0 271 270 0 262 266 0 266 267 0 267 265 0 265 262 0
		 266 270 0 271 267 0 258 266 0 267 268 0 257 245 0 138 283 0 282 139 0 144 281 0 280 145 0
		 275 18 0 70 279 0 278 71 0 273 12 0 74 277 0 276 75 0 370 344 0 344 285 0 285 305 0
		 305 370 0 347 375 0 375 314 0 314 286 0 286 347 0 430 416 0 416 289 0 289 292 0 292 430 0
		 424 418 0 418 290 0 290 295 0 295 424 0 356 348 0 348 291 0 291 303 0 303 356 0 349 532 0
		 532 535 0 535 348 0 348 349 0 366 352 0 352 301 0 301 311 0 311 366 0 352 354 0 354 302 0
		 302 301 0 563 356 0 303 559 0 559 563 0 438 420 0 420 297 0 297 306 0 306 438 0 420 422 0
		 422 296 0 296 297 0 567 424 0 295 557 0 557 567 0 434 426 0 426 294 0 294 304 0 304 434 0
		 426 428 0 428 293 0 293 294 0 565 430 0 292 556 0 556 565 0 350 358 0 358 292 0 289 350 0
		 561 360 0 360 293 0 293 556 0 556 561 0 360 362 0 362 294 0 414 432 0 432 305 0 285 414 0
		 432 434 0 304 305 0 412 436 0 436 307 0 307 286 0 286 412 0 436 438 0 306 307 0 346 364 0
		 364 310 0 310 287 0 287 346 0 364 366 0 311 310 0 362 368 0 368 304 0 368 370 0 411 379 0
		 379 318 0 318 404 0 404 411 0 413 441 0 441 312 0 312 284 0 284 413 0 415 443 0 443 315 0
		 315 287 0 287 415 0 344 372 0 372 313 0 313 285 0 475 378 0 378 319 0;
	setAttr ".ed[664:829]" 319 447 0 447 475 0 478 445 0 445 316 0 316 405 0 405 478 0
		 476 447 0 319 406 0 406 476 0 409 376 0 376 317 0 317 407 0 407 409 0 372 380 0 380 331 0
		 331 313 0 499 382 0 382 330 0 330 495 0 495 499 0 398 384 0 384 329 0 329 339 0 339 398 0
		 443 448 0 448 325 0 325 315 0 503 450 0 450 324 0 324 494 0 494 503 0 466 452 0 452 323 0
		 323 337 0 337 466 0 441 454 0 454 326 0 326 312 0 501 456 0 456 327 0 327 493 0 493 501 0
		 464 458 0 458 328 0 328 338 0 338 464 0 375 386 0 386 320 0 320 314 0 497 388 0 388 321 0
		 321 492 0 492 497 0 396 390 0 390 322 0 322 336 0 336 396 0 386 392 0 392 332 0 332 320 0
		 454 460 0 460 334 0 334 326 0 448 462 0 462 333 0 333 325 0 380 394 0 394 335 0 335 331 0
		 388 396 0 336 321 0 456 464 0 338 327 0 450 466 0 337 324 0 382 398 0 339 330 0 384 400 0
		 400 343 0 343 329 0 452 468 0 468 342 0 342 323 0 458 470 0 470 341 0 341 328 0 390 402 0
		 402 340 0 340 322 0 342 403 0 403 391 0 391 323 0 403 402 0 390 391 0 341 401 0 401 385 0
		 385 328 0 401 400 0 384 385 0 338 399 0 399 383 0 383 327 0 399 398 0 382 383 0 337 397 0
		 397 389 0 389 324 0 397 396 0 388 389 0 334 395 0 395 381 0 381 326 0 395 394 0 380 381 0
		 333 393 0 393 387 0 387 325 0 393 392 0 386 387 0 391 397 0 389 496 0 496 494 0 497 496 0
		 387 374 0 374 315 0 375 374 0 385 399 0 383 498 0 498 493 0 499 498 0 381 373 0 373 312 0
		 372 373 0 316 377 0 377 408 0 408 405 0 444 318 0 379 473 0 473 444 0 373 345 0 345 284 0
		 344 345 0 378 410 0 410 406 0 309 308 0 308 371 0 371 369 0 369 309 0 371 370 0 368 369 0
		 298 309 0 369 363 0 363 298 0 362 363 0 306 367 0 367 365 0 365 307 0 367 366 0 364 365 0
		 365 347 0 346 347 0 299 298 0 363 361 0 361 299 0 360 361 0 558 299 0;
	setAttr ".ed[830:995]" 361 560 0 560 558 0 561 560 0 288 300 0 300 359 0 359 351 0
		 351 288 0 359 358 0 350 351 0 295 357 0 357 562 0 562 557 0 357 356 0 563 562 0 296 355 0
		 355 353 0 353 297 0 355 354 0 352 353 0 353 367 0 419 530 0 530 539 0 539 418 0 418 419 0
		 291 529 0 529 528 0 528 417 0 417 291 0 290 349 0 349 357 0 374 346 0 308 284 0 345 371 0
		 486 410 0 410 411 0 411 487 0 487 486 0 480 406 0 486 480 0 484 408 0 408 409 0 409 485 0
		 485 484 0 483 405 0 484 483 0 407 481 0 481 485 0 491 476 0 480 491 0 489 478 0 483 489 0
		 404 482 0 482 487 0 404 479 0 479 488 0 488 482 0 479 478 0 489 488 0 407 477 0 477 490 0
		 490 481 0 477 476 0 491 490 0 535 529 0 539 533 0 533 290 0 351 534 0 534 531 0 531 288 0
		 350 537 0 537 534 0 445 472 0 472 377 0 445 444 0 473 472 0 340 471 0 471 459 0 459 322 0
		 471 470 0 458 459 0 343 469 0 469 453 0 453 329 0 469 468 0 452 453 0 339 467 0 467 451 0
		 451 330 0 467 466 0 450 451 0 336 465 0 465 457 0 457 321 0 465 464 0 456 457 0 335 463 0
		 463 449 0 449 331 0 463 462 0 448 449 0 332 461 0 461 455 0 455 320 0 461 460 0 454 455 0
		 459 465 0 457 500 0 500 492 0 501 500 0 455 440 0 440 314 0 441 440 0 453 467 0 451 502 0
		 502 495 0 503 502 0 449 442 0 442 313 0 443 442 0 317 446 0 446 477 0 446 447 0 444 479 0
		 376 474 0 474 446 0 474 475 0 442 414 0 415 414 0 440 412 0 413 412 0 309 439 0 439 437 0
		 437 308 0 439 438 0 436 437 0 437 413 0 311 435 0 435 433 0 433 310 0 435 434 0 432 433 0
		 433 415 0 303 431 0 431 564 0 564 559 0 431 430 0 565 564 0 302 429 0 429 427 0 427 301 0
		 429 428 0 426 427 0 427 435 0 558 300 0 300 425 0 425 566 0 566 558 0 425 424 0 567 566 0
		 299 423 0 423 421 0 421 298 0 423 422 0 420 421 0 421 439 0 528 538 0;
	setAttr ".ed[996:1127]" 538 416 0 416 417 0 533 532 0 288 419 0 419 425 0 417 431 0
		 408 411 0 410 409 0 378 376 0 377 379 0 469 490 0 491 468 0 343 481 0 471 488 0 489 470 0
		 340 482 0 402 487 0 483 341 0 480 342 0 400 485 0 484 401 0 486 403 0 463 502 0 503 462 0
		 335 495 0 461 500 0 501 460 0 332 492 0 499 511 0 511 510 0 510 498 0 395 506 0 506 508 0
		 508 394 0 393 496 0 497 392 0 333 494 0 493 334 0 334 507 0 507 506 0 506 510 0 511 508 0
		 507 509 0 509 510 0 511 504 0 504 505 0 505 508 0 335 505 0 504 495 0 510 516 0 516 512 0
		 512 498 0 493 509 0 504 518 0 518 514 0 514 495 0 516 524 0 524 520 0 520 512 0 518 526 0
		 526 522 0 522 514 0 511 519 0 519 518 0 499 515 0 515 519 0 514 515 0 509 517 0 517 516 0
		 493 513 0 513 517 0 512 513 0 526 527 0 527 523 0 523 522 0 524 525 0 525 521 0 521 520 0
		 517 525 0 521 513 0 515 523 0 527 519 0 538 552 0 552 548 0 548 536 0 536 538 0 534 546 0
		 546 543 0 543 531 0 530 542 0 542 553 0 553 539 0 548 549 0 549 537 0 537 536 0 533 545 0
		 545 544 0 544 532 0 553 545 0 544 547 0 547 535 0 543 542 0 530 531 0 528 540 0 540 552 0
		 536 289 0 555 540 0 540 541 0 541 547 0 547 555 0 553 554 0 554 544 0 554 555 0 546 549 0
		 549 551 0 551 550 0 550 546 0 551 555 0 554 550 0 550 542 0 552 551 0 529 541 0 423 566 0
		 567 422 0 429 564 0 565 428 0 302 559 0 355 562 0 563 354 0 296 557 0 359 560 0 561 358 0;
	setAttr -s 2240 ".n";
	setAttr ".n[0:165]" -type "float3"  -0.92238992 0.069279887 0.37999618 -0.92238992
		 0.069279887 0.37999618 -0.92238992 0.069279887 0.37999618 -0.92238992 0.069279887
		 0.37999618 0.91045934 0.20324095 -0.36021805 0.91045934 0.20324095 -0.36021805 0.91045934
		 0.20324095 -0.36021805 0.91045934 0.20324095 -0.36021805 -0.4758572 0.22303227 0.85077411
		 -0.4758572 0.22303227 0.85077411 -0.4758572 0.22303227 0.85077411 -0.4758572 0.22303227
		 0.85077411 0.30917498 0.024789533 -0.9506821 0.30917498 0.024789533 -0.9506821 0.30917498
		 0.024789533 -0.9506821 0.30917498 0.024789533 -0.9506821 0.80926037 0.20341107 0.55110943
		 0.80926037 0.20341107 0.55110943 0.80926037 0.20341107 0.55110943 0.80926037 0.20341107
		 0.55110943 0.97946477 -0.20161504 0 0.97946477 -0.20161504 0 0.97946477 -0.20161504
		 0 0.97946477 -0.20161504 0 0.86413401 -0.052833244 0.50048077 0.86413401 -0.052833244
		 0.50048077 0.86413401 -0.052833244 0.50048077 0.86413401 -0.052833244 0.50048077
		 0.81242633 -0.2923516 0.50447404 0.81242633 -0.2923516 0.50447404 0.81242633 -0.2923516
		 0.50447404 0.81242633 -0.2923516 0.50447404 0.79159886 0.30599773 0.52890134 0.79159886
		 0.30599773 0.52890134 0.79159886 0.30599773 0.52890134 0.79159886 0.30599773 0.52890134
		 0.4033134 -0.00513402 -0.91504747 0.4033134 -0.00513402 -0.91504747 0.4033134 -0.00513402
		 -0.91504747 0.4033134 -0.00513402 -0.91504747 0.42532653 -0.13722298 -0.89457655
		 0.42532653 -0.13722298 -0.89457655 0.42532653 -0.13722298 -0.89457655 0.42532653
		 -0.13722298 -0.89457655 0.3730886 0.044647358 -0.9267208 0.3730886 0.044647358 -0.9267208
		 0.3730886 0.044647358 -0.9267208 0.3730886 0.044647358 -0.9267208 -0.42034534 -0.088101074
		 0.90307695 -0.42034534 -0.088101074 0.90307695 -0.42034534 -0.088101074 0.90307695
		 -0.42034534 -0.088101074 0.90307695 -0.33568624 -0.26869473 0.90283877 -0.33568624
		 -0.26869473 0.90283877 -0.33568624 -0.26869473 0.90283877 -0.33568624 -0.26869473
		 0.90283877 -0.39976043 0.28632444 0.87075245 -0.39976043 0.28632444 0.87075245 -0.39976043
		 0.28632444 0.87075245 -0.39976043 0.28632444 0.87075245 -0.94568449 0.091909446 0.31182289
		 -0.94568449 0.091909446 0.31182289 -0.94568449 0.091909446 0.31182289 -0.94568449
		 0.091909446 0.31182289 -0.51521564 0.8058477 0.29182577 -0.51521564 0.8058477 0.29182577
		 -0.51521564 0.8058477 0.29182577 -0.51521564 0.8058477 0.29182577 -0.87690097 -0.13266303
		 0.46200135 -0.87690097 -0.13266303 0.46200135 -0.87690097 -0.13266303 0.46200135
		 -0.87690097 -0.13266303 0.46200135 -0.51615244 0.12848729 0.84680444 -0.51615244
		 0.12848729 0.84680444 -0.51615244 0.12848729 0.84680444 -0.51615244 0.12848729 0.84680444
		 -0.19155717 -0.92003375 0.34182429 -0.19155717 -0.92003375 0.34182429 -0.19155717
		 -0.92003375 0.34182429 -0.19155717 -0.92003375 0.34182429 0.43466184 0.018590886
		 -0.90040189 0.43466184 0.018590886 -0.90040189 0.43466184 0.018590886 -0.90040189
		 0.43466184 0.018590886 -0.90040189 0.26525229 -0.76956677 -0.58086842 0.26525229
		 -0.76956677 -0.58086842 0.26525229 -0.76956677 -0.58086842 0.26525229 -0.76956677
		 -0.58086842 0.90198582 0.10080004 0.41983449 0.90198582 0.10080004 0.41983449 0.90198582
		 0.10080004 0.41983449 0.90198582 0.10080004 0.41983449 0.50384378 -0.8241244 0.25876722
		 0.50384378 -0.8241244 0.25876722 0.50384378 -0.8241244 0.25876722 0.50384378 -0.8241244
		 0.25876722 -0.89963919 -0.04679551 0.43411916 -0.89963919 -0.04679551 0.43411916
		 -0.89963919 -0.04679551 0.43411916 -0.89963919 -0.04679551 0.43411916 -0.56070387
		 -0.79266655 0.2393553 -0.56070387 -0.79266655 0.2393553 -0.56070387 -0.79266655 0.2393553
		 -0.56070387 -0.79266655 0.2393553 0.99298334 -0.013666908 -0.1174625 0.99298334 -0.013666908
		 -0.1174625 0.99298334 -0.013666908 -0.1174625 0.99298334 -0.013666908 -0.1174625
		 -0.52168679 0.010598608 -0.85307127 -0.52168679 0.010598608 -0.85307127 -0.52168679
		 0.010598608 -0.85307127 -0.52168679 0.010598608 -0.85307127 0.49219486 0.25911829
		 0.83102459 0.49219486 0.25911829 0.83102459 0.49219486 0.25911829 0.83102459 0.49219486
		 0.25911829 0.83102459 -0.92169994 0.091820925 0.37687933 -0.92169994 0.091820925
		 0.37687933 -0.92169994 0.091820925 0.37687933 -0.92169994 0.091820925 0.37687933
		 0 1 2.895719e-06 0 1 2.895719e-06 0 1 2.895719e-06 0 1 2.895719e-06 -0.95511049 -0.04410978
		 -0.29294732 -0.95511049 -0.04410978 -0.29294732 -0.95511049 -0.04410978 -0.29294732
		 -0.95511049 -0.04410978 -0.29294732 0.60568762 -0.11847676 0.78683269 0.60568762
		 -0.11847676 0.78683269 0.60568762 -0.11847676 0.78683269 0.60568762 -0.11847676 0.78683269
		 -0.98069608 -0.022598699 0.19422807 -0.98069608 -0.022598699 0.19422807 -0.98069608
		 -0.022598699 0.19422807 -0.98069608 -0.022598699 0.19422807 -0.94470859 0.088904038
		 0.31562901 -0.94470859 0.088904038 0.31562901 -0.94470859 0.088904038 0.31562901
		 -0.94470859 0.088904038 0.31562901 -0.9345969 -0.065429702 0.34963918 -0.9345969
		 -0.065429702 0.34963918 -0.9345969 -0.065429702 0.34963918 -0.9345969 -0.065429702
		 0.34963918 -0.91419023 -0.078215867 0.39766639 -0.91419023 -0.078215867 0.39766639
		 -0.91419023 -0.078215867 0.39766639 -0.91419023 -0.078215867 0.39766639 0.4523685
		 0.32704318 0.82970196 0.4523685 0.32704318 0.82970196 0.4523685 0.32704318 0.82970196
		 0.4523685 0.32704318 0.82970196 0.33862612 -0.20727888 0.91780597 0.33862612 -0.20727888
		 0.91780597 0.33862612 -0.20727888 0.91780597 0.33862612 -0.20727888 0.91780597 0.27082413
		 -0.25853717 0.92726088 0.27082413 -0.25853717 0.92726088 0.27082413 -0.25853717 0.92726088
		 0.27082413 -0.25853717 0.92726088 -0.77581733 -0.03301058 -0.63009351 -0.77581733
		 -0.03301058 -0.63009351 -0.77581733 -0.03301058 -0.63009351 -0.77581733 -0.03301058
		 -0.63009351 -0.82210225 -0.13217808 -0.55378431 -0.82210225 -0.13217808 -0.55378431;
	setAttr ".n[166:331]" -type "float3"  -0.82210225 -0.13217808 -0.55378431 -0.82210225
		 -0.13217808 -0.55378431 -0.78449214 -0.17063786 -0.59620035 -0.78449214 -0.17063786
		 -0.59620035 -0.78449214 -0.17063786 -0.59620035 -0.78449214 -0.17063786 -0.59620035
		 0.92052877 0.32011858 -0.22394407 0.92052877 0.32011858 -0.22394407 0.92052877 0.32011858
		 -0.22394407 0.92052877 0.32011858 -0.22394407 0.97475547 -0.049096774 -0.21781012
		 0.97475547 -0.049096774 -0.21781012 0.97475547 -0.049096774 -0.21781012 0.97475547
		 -0.049096774 -0.21781012 0.96642309 -0.054583561 -0.25109187 0.96642309 -0.054583561
		 -0.25109187 0.96642309 -0.054583561 -0.25109187 0.96642309 -0.054583561 -0.25109187
		 0.97142255 0.096678622 -0.21677519 0.97142255 0.096678622 -0.21677519 0.97142255
		 0.096678622 -0.21677519 0.97142255 0.096678622 -0.21677519 -0.71878326 0.51305258
		 -0.4691776 -0.71878326 0.51305258 -0.4691776 -0.71878326 0.51305258 -0.4691776 -0.71878326
		 0.51305258 -0.4691776 0.21967211 0.73102111 0.6460281 0.21967211 0.73102111 0.6460281
		 0.21967211 0.73102111 0.6460281 0.21967211 0.73102111 0.6460281 -0.9197647 0.1896069
		 0.34363094 -0.9197647 0.1896069 0.34363094 -0.9197647 0.1896069 0.34363094 -0.9197647
		 0.1896069 0.34363094 0.9527809 0.18371466 -0.24177989 0.9527809 0.18371466 -0.24177989
		 0.9527809 0.18371466 -0.24177989 0.9527809 0.18371466 -0.24177989 -0.64443749 0.5965575
		 -0.4783507 -0.64443749 0.5965575 -0.4783507 -0.64443749 0.5965575 -0.4783507 -0.64443749
		 0.5965575 -0.4783507 0.18388222 0.76686543 0.61490226 0.18388222 0.76686543 0.61490226
		 0.18388222 0.76686543 0.61490226 0.18388222 0.76686543 0.61490226 -0.88226074 0.28481346
		 0.37482974 -0.88226074 0.28481346 0.37482974 -0.88226074 0.28481346 0.37482974 -0.88226074
		 0.28481346 0.37482974 -0.85478294 0.35294873 0.38049081 -0.85478294 0.35294873 0.38049081
		 -0.85478294 0.35294873 0.38049081 -0.85478294 0.35294873 0.38049081 0.15480103 0.8257522
		 0.54237431 0.15480103 0.8257522 0.54237431 0.15480103 0.8257522 0.54237431 0.15480103
		 0.8257522 0.54237431 -0.57670575 0.68282789 -0.44850481 -0.57670575 0.68282789 -0.44850481
		 -0.57670575 0.68282789 -0.44850481 -0.57670575 0.68282789 -0.44850481 0.94009143
		 0.23185129 -0.24994598 0.94009143 0.23185129 -0.24994598 0.94009143 0.23185129 -0.24994598
		 0.94009143 0.23185129 -0.24994598 0.94009066 0.23185486 0.24994577 0.94009066 0.23185486
		 0.24994577 0.94009066 0.23185486 0.24994577 0.94009066 0.23185486 0.24994577 1 0
		 0 1 0 0 1 0 0 1 0 0 -0.85478163 0.35294852 -0.38049415 -0.85478163 0.35294852 -0.38049415
		 -0.85478163 0.35294852 -0.38049415 -0.85478163 0.35294852 -0.38049415 -1 0 0 -1 0
		 0 -1 0 0 -1 0 0 -0.88225806 0.28481483 -0.3748349 -0.88225806 0.28481483 -0.3748349
		 -0.88225806 0.28481483 -0.3748349 -0.88225806 0.28481483 -0.3748349 -0.99999994 0
		 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 0.95278186 0.18371481 0.24177606
		 0.95278186 0.18371481 0.24177606 0.95278186 0.18371481 0.24177606 0.95278186 0.18371481
		 0.24177606 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 -0.91976273
		 0.18960787 -0.34363547 -0.91976273 0.18960787 -0.34363547 -0.91976273 0.18960787
		 -0.34363547 -0.91976273 0.18960787 -0.34363547 -0.99779195 -0.066416994 0 -0.99779195
		 -0.066416994 0 -0.99779195 -0.066416994 0 -0.99779195 -0.066416994 0 0.97142273 0.096679606
		 0.21677376 0.97142273 0.096679606 0.21677376 0.97142273 0.096679606 0.21677376 0.97142273
		 0.096679606 0.21677376 0.99799848 -0.063238479 0 0.99799848 -0.063238479 0 0.99799848
		 -0.063238479 0 0.99799848 -0.063238479 0 0.96642309 -0.054584291 0.25109187 0.96642309
		 -0.054584291 0.25109187 0.96642309 -0.054584291 0.25109187 0.96642309 -0.054584291
		 0.25109187 0.99989635 -0.014394498 0 0.99989635 -0.014394498 0 0.99989635 -0.014394498
		 0 0.99989635 -0.014394498 0 0.97475535 -0.049097478 0.2178106 0.97475535 -0.049097478
		 0.2178106 0.97475535 -0.049097478 0.2178106 0.97475535 -0.049097478 0.2178106 0.99973685
		 -0.022937782 0 0.99973685 -0.022937782 0 0.99973685 -0.022937782 0 0.99973685 -0.022937782
		 0 0.90802765 0.36037037 0.21358632 0.90802765 0.36037037 0.21358632 0.90802765 0.36037037
		 0.21358632 0.90802765 0.36037037 0.21358632 0.93160194 0.36348027 0 0.93160194 0.36348027
		 0 0.93160194 0.36348027 0 0.93160194 0.36348027 0 -0.91419023 -0.078215174 -0.39766639
		 -0.91419023 -0.078215174 -0.39766639 -0.91419023 -0.078215174 -0.39766639 -0.91419023
		 -0.078215174 -0.39766639 -0.99988604 -0.01509646 0 -0.99988604 -0.01509646 0 -0.99988604
		 -0.01509646 0 -0.99988604 -0.01509646 0 -0.934596 -0.065429457 -0.34964165 -0.934596
		 -0.065429457 -0.34964165 -0.934596 -0.065429457 -0.34964165 -0.934596 -0.065429457
		 -0.34964165 -0.9997099 -0.02408625 0 -0.9997099 -0.02408625 0 -0.9997099 -0.02408625
		 0 -0.9997099 -0.02408625 0 -0.94491196 0.023006225 -0.32651511 -0.94491196 0.023006225
		 -0.32651511 -0.94491196 0.023006225 -0.32651511 -0.94491196 0.023006225 -0.32651511
		 -0.99765068 0.068506181 0 -0.99765068 0.068506181 0 -0.99765068 0.068506181 0 -0.99765068
		 0.068506181 0 -0.9806965 -0.02259847 -0.19422609 -0.9806965 -0.02259847 -0.19422609
		 -0.9806965 -0.02259847 -0.19422609 -0.9806965 -0.02259847 -0.19422609;
	setAttr ".n[332:497]" -type "float3"  0 1 -3.0547817e-06 0 1 -3.0547817e-06
		 0 1 -3.0547817e-06 0 1 -3.0547817e-06 -0.92180848 0.026256474 -0.38675529 -0.92180848
		 0.026256474 -0.38675529 -0.92180848 0.026256474 -0.38675529 -0.92180848 0.026256474
		 -0.38675529 -0.99850368 0.054684319 0 -0.99850368 0.054684319 0 -0.99850368 0.054684319
		 0 -0.99850368 0.054684319 0 0.9929831 -0.01366705 0.11746372 0.9929831 -0.01366705
		 0.11746372 0.9929831 -0.01366705 0.11746372 0.9929831 -0.01366705 0.11746372 -0.71434104
		 -0.62986356 -0.30494073 -0.71434104 -0.62986356 -0.30494073 -0.71434104 -0.62986356
		 -0.30494073 -0.71434104 -0.62986356 -0.30494073 -0.78050536 -0.62514913 0 -0.78050536
		 -0.62514913 0 -0.78050536 -0.62514913 0 -0.78050536 -0.62514913 0 -0.9005807 -0.010026446
		 -0.43457305 -0.9005807 -0.010026446 -0.43457305 -0.9005807 -0.010026446 -0.43457305
		 -0.9005807 -0.010026446 -0.43457305 -0.99978232 -0.020863321 0 -0.99978232 -0.020863321
		 0 -0.99978232 -0.020863321 0 -0.99978232 -0.020863321 0 0.6659022 -0.66303146 -0.34199938
		 0.6659022 -0.66303146 -0.34199938 0.6659022 -0.66303146 -0.34199938 0.6659022 -0.66303146
		 -0.34199938 0.74385321 -0.66834301 0 0.74385321 -0.66834301 0 0.74385321 -0.66834301
		 0 0.74385321 -0.66834301 0 0.90566128 0.045574374 -0.42154527 0.90566128 0.045574374
		 -0.42154527 0.90566128 0.045574374 -0.42154527 0.90566128 0.045574374 -0.42154527
		 0.99716663 0.075224385 0 0.99716663 0.075224385 0 0.99716663 0.075224385 0 0.99716663
		 0.075224385 0 -0.88419151 -0.034534656 -0.46584627 -0.88419151 -0.034534656 -0.46584627
		 -0.88419151 -0.034534656 -0.46584627 -0.88419151 -0.034534656 -0.46584627 -0.99848354
		 -0.055050891 0 -0.99848354 -0.055050891 0 -0.99848354 -0.055050891 0 -0.99848354
		 -0.055050891 0 -0.57937229 0.7460835 -0.32815701 -0.57937229 0.7460835 -0.32815701
		 -0.57937229 0.7460835 -0.32815701 -0.57937229 0.7460835 -0.32815701 -0.63973123 0.76859874
		 0 -0.63973123 0.76859874 0 -0.63973123 0.76859874 0 -0.63973123 0.76859874 0 -0.93761104
		 0.0046793697 -0.34765443 -0.93761104 0.0046793697 -0.34765443 -0.93761104 0.0046793697
		 -0.34765443 -0.93761104 0.0046793697 -0.34765443 -0.99963474 0.027023688 2.6170857e-08
		 -0.99963474 0.027023688 2.6170857e-08 -0.99963474 0.027023688 2.6170857e-08 -0.99963474
		 0.027023688 2.6170857e-08 0.77225834 0.14486845 -0.61857116 0.77225834 0.14486845
		 -0.61857116 0.77225834 0.14486845 -0.61857116 0.77225834 0.14486845 -0.61857116 0.96731156
		 0.253591 0 0.96731156 0.253591 0 0.96731156 0.253591 0 0.96731156 0.253591 0 0.83409375
		 -0.18982345 -0.51793301 0.83409375 -0.18982345 -0.51793301 0.83409375 -0.18982345
		 -0.51793301 0.83409375 -0.18982345 -0.51793301 0.97029799 -0.24191301 0 0.97029799
		 -0.24191301 0 0.97029799 -0.24191301 0 0.97029799 -0.24191301 0 0.86529565 -0.01044542
		 -0.50115317 0.86529565 -0.01044542 -0.50115317 0.86529565 -0.01044542 -0.50115317
		 0.86529565 -0.01044542 -0.50115317 0.99970603 -0.024245847 0 0.99970603 -0.024245847
		 0 0.99970603 -0.024245847 0 0.99970603 -0.024245847 0 0 -0.13475522 -0.99087888 0
		 -0.13475522 -0.99087888 0 -0.13475522 -0.99087888 0 -0.13475522 -0.99087888 0.74827033
		 -0.1849504 0.63709098 0.74827033 -0.1849504 0.63709098 0.74827033 -0.1849504 0.63709098
		 0.74827033 -0.1849504 0.63709098 0.79301369 0.048323378 -0.60728425 0.79301369 0.048323378
		 -0.60728425 0.79301369 0.048323378 -0.60728425 0.79301369 0.048323378 -0.60728425
		 0.99157161 0.12955981 0 0.99157161 0.12955981 0 0.99157161 0.12955981 0 0.99157161
		 0.12955981 0 0.90058768 0.26144984 0.34725484 0.90058768 0.26144984 0.34725484 0.90058768
		 0.26144984 0.34725484 0.90058768 0.26144984 0.34725484 0.96786529 0.25146905 0 0.96786529
		 0.25146905 0 0.96786529 0.25146905 0 0.96786529 0.25146905 0 -0.92444175 0.019169867
		 -0.38084123 -0.92444175 0.019169867 -0.38084123 -0.92444175 0.019169867 -0.38084123
		 -0.92444175 0.019169867 -0.38084123 -0.99951041 0.031287074 0 -0.99951041 0.031287074
		 0 -0.99951041 0.031287074 0 -0.99951041 0.031287074 0 0.53042179 0.84773391 0 0.53042179
		 0.84773391 0 0.53042179 0.84773391 0 0.53042179 0.84773391 0 0.54384404 0.83191276
		 0.11024913 0.54384404 0.83191276 0.11024913 0.54384404 0.83191276 0.11024913 0.54384404
		 0.83191276 0.11024913 -0.52146661 0.85327166 0 -0.52146661 0.85327166 0 -0.52146661
		 0.85327166 0 -0.52146661 0.85327166 0 -0.53992879 0.82152051 -0.18325131 -0.53992879
		 0.82152051 -0.18325131 -0.53992879 0.82152051 -0.18325131 -0.53992879 0.82152051
		 -0.18325131 -0.53992581 0.821522 0.18325341 -0.53992581 0.821522 0.18325341 -0.53992581
		 0.821522 0.18325341 -0.53992581 0.821522 0.18325341 0.21058412 0.7631048 0.61100364
		 0.21058412 0.7631048 0.61100364 0.21058412 0.7631048 0.61100364 0.21058412 0.7631048
		 0.61100364 -0.56688601 0.75601548 -0.32723209 -0.56688601 0.75601548 -0.32723209
		 -0.56688601 0.75601548 -0.32723209 -0.56688601 0.75601548 -0.32723209 0.54384702
		 0.8319115 -0.11024323 0.54384702 0.8319115 -0.11024323 0.54384702 0.8319115 -0.11024323
		 0.54384702 0.8319115 -0.11024323 0.45839581 0.78871518 -0.40963596 0.45839581 0.78871518
		 -0.40963596 0.45839581 0.78871518 -0.40963596 0.45839581 0.78871518 -0.40963596 0
		 0.71730369 -0.69676071 0 0.71730369 -0.69676071;
	setAttr ".n[498:663]" -type "float3"  0 0.71730369 -0.69676071 0 0.71730369
		 -0.69676071 -0.30040151 0.76992625 0.56300318 -0.30040151 0.76992625 0.56300318 -0.30040151
		 0.76992625 0.56300318 -0.30040151 0.76992625 0.56300318 0 0.71729863 0.6967659 0
		 0.71729863 0.6967659 0 0.71729863 0.6967659 0 0.71729863 0.6967659 0.80842775 -0.18512265
		 0.55872554 0.80842775 -0.18512265 0.55872554 0.80842775 -0.18512265 0.55872554 0.80842775
		 -0.18512265 0.55872554 0.29975271 -0.16389303 -0.93983376 0.29975271 -0.16389303
		 -0.93983376 0.29975271 -0.16389303 -0.93983376 0.29975271 -0.16389303 -0.93983376
		 -0.95768046 -0.061500035 -0.28118679 -0.95768046 -0.061500035 -0.28118679 -0.95768046
		 -0.061500035 -0.28118679 -0.95768046 -0.061500035 -0.28118679 -0.99872297 -0.050521191
		 0 -0.99872297 -0.050521191 0 -0.99872297 -0.050521191 0 -0.99872297 -0.050521191
		 0 0 1 0 0 1 0 0 1 0 0 1 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994
		 0 0.41489837 0.76048905 -0.49951559 0.41489837 0.76048905 -0.49951559 0.41489837
		 0.76048905 -0.49951559 0.41489837 0.76048905 -0.49951559 0 0.85568345 -0.51749957
		 0 0.85568345 -0.51749957 0 0.85568345 -0.51749957 0 0.85568345 -0.51749957 -0.23573288
		 0.81227213 0.53352028 -0.23573288 0.81227213 0.53352028 -0.23573288 0.81227213 0.53352028
		 -0.23573288 0.81227213 0.53352028 0 0.8556878 0.51749247 0 0.8556878 0.51749247 0
		 0.8556878 0.51749247 0 0.8556878 0.51749247 -0.27817273 0.74937582 0.60087925 -0.27817273
		 0.74937582 0.60087925 -0.27817273 0.74937582 0.60087925 -0.27817273 0.74937582 0.60087925
		 0 0.80450076 0.59395164 0 0.80450076 0.59395164 0 0.80450076 0.59395164 0 0.80450076
		 0.59395164 0.47813988 0.68520534 -0.5494324 0.47813988 0.68520534 -0.5494324 0.47813988
		 0.68520534 -0.5494324 0.47813988 0.68520534 -0.5494324 0 0.80449539 -0.59395897 0
		 0.80449539 -0.59395897 0 0.80449539 -0.59395897 0 0.80449539 -0.59395897 -0.32995039
		 0.70586801 0.62680382 -0.32995039 0.70586801 0.62680382 -0.32995039 0.70586801 0.62680382
		 -0.32995039 0.70586801 0.62680382 0 0.77880627 0.62726456 0 0.77880627 0.62726456
		 0 0.77880627 0.62726456 0 0.77880627 0.62726456 0.55236083 0.61916012 -0.55815607
		 0.55236083 0.61916012 -0.55815607 0.55236083 0.61916012 -0.55815607 0.55236083 0.61916012
		 -0.55815607 0 0.77880055 -0.62727171 0 0.77880055 -0.62727171 0 0.77880055 -0.62727171
		 0 0.77880055 -0.62727171 0.63306701 -0.21090029 -0.74481356 0.63306701 -0.21090029
		 -0.74481356 0.63306701 -0.21090029 -0.74481356 0.63306701 -0.21090029 -0.74481356
		 0 -0.28764746 -0.95773625 0 -0.28764746 -0.95773625 0 -0.28764746 -0.95773625 0 -0.28764746
		 -0.95773625 0.68253642 -0.16595232 -0.71176118 0.68253642 -0.16595232 -0.71176118
		 0.68253642 -0.16595232 -0.71176118 0.68253642 -0.16595232 -0.71176118 0 -0.23365329
		 -0.97231996 0 -0.23365329 -0.97231996 0 -0.23365329 -0.97231996 0 -0.23365329 -0.97231996
		 0.66288406 0.18430251 -0.72568399 0.66288406 0.18430251 -0.72568399 0.66288406 0.18430251
		 -0.72568399 0.66288406 0.18430251 -0.72568399 0 0 -1 0 0 -1 0 0 -1 0 0 -1 -0.3992148
		 -0.24710278 0.88293135 -0.3992148 -0.24710278 0.88293135 -0.3992148 -0.24710278 0.88293135
		 -0.3992148 -0.24710278 0.88293135 0 -0.28765237 0.95773488 0 -0.28765237 0.95773488
		 0 -0.28765237 0.95773488 0 -0.28765237 0.95773488 -0.48652634 -0.19418474 0.85181242
		 -0.48652634 -0.19418474 0.85181242 -0.48652634 -0.19418474 0.85181242 -0.48652634
		 -0.19418474 0.85181242 0 -0.23365639 0.97231925 0 -0.23365639 0.97231925 0 -0.23365639
		 0.97231925 0 -0.23365639 0.97231925 -0.5837875 0.16786234 0.79436415 -0.5837875 0.16786234
		 0.79436415 -0.5837875 0.16786234 0.79436415 -0.5837875 0.16786234 0.79436415 0 0.25477138
		 0.96700132 0 0.25477138 0.96700132 0 0.25477138 0.96700132 0 0.25477138 0.96700132
		 -0.76246238 -0.096340336 0.6398201 -0.76246238 -0.096340336 0.6398201 -0.76246238
		 -0.096340336 0.6398201 -0.76246238 -0.096340336 0.6398201 0 -0.16139036 0.98689061
		 0 -0.16139036 0.98689061 0 -0.16139036 0.98689061 0 -0.16139036 0.98689061 0.90146202
		 -0.064449973 -0.42803332 0.90146202 -0.064449973 -0.42803332 0.90146202 -0.064449973
		 -0.42803332 0.90146202 -0.064449973 -0.42803332 0 -0.16138858 -0.98689091 0 -0.16138858
		 -0.98689091 0 -0.16138858 -0.98689091 0 -0.16138858 -0.98689091 0 1 0 0 1 0 0 1 0
		 0 1 0 0 1 0 0 1 0 0 1 0 0 1 0 -0.52140224 0.16430257 0.83734363 -0.52140224 0.16430257
		 0.83734363 -0.52140224 0.16430257 0.83734363 -0.52140224 0.16430257 0.83734363 0
		 0.21878982 0.97577208 0 0.21878982 0.97577208 0 0.21878982 0.97577208 0 0.21878982
		 0.97577208 0.4998467 0.10822356 -0.85932589 0.4998467 0.10822356 -0.85932589 0.4998467
		 0.10822356 -0.85932589 0.4998467 0.10822356 -0.85932589;
	setAttr ".n[664:829]" -type "float3"  0 0.024401095 -0.99970227 0 0.024401095
		 -0.99970227 0 0.024401095 -0.99970227 0 0.024401095 -0.99970227 -0.23514713 -0.76241457
		 -0.60285151 -0.23514713 -0.76241457 -0.60285151 -0.23514713 -0.76241457 -0.60285151
		 -0.23514713 -0.76241457 -0.60285151 2.3953348e-06 -0.76991564 -0.63814574 2.3953348e-06
		 -0.76991564 -0.63814574 2.3953348e-06 -0.76991564 -0.63814574 2.3953348e-06 -0.76991564
		 -0.63814574 -0.39889923 0.0047904393 -0.91698229 -0.39889923 0.0047904393 -0.91698229
		 -0.39889923 0.0047904393 -0.91698229 -0.39889923 0.0047904393 -0.91698229 0 0 -1
		 0 0 -1 0 0 -1 0 0 -1 0.2169226 -0.91849154 0.33063275 0.2169226 -0.91849154 0.33063275
		 0.2169226 -0.91849154 0.33063275 0.2169226 -0.91849154 0.33063275 -1.1098979e-06
		 -0.93682092 0.3498092 -1.1098979e-06 -0.93682092 0.3498092 -1.1098979e-06 -0.93682092
		 0.3498092 -1.1098979e-06 -0.93682092 0.3498092 0.55458516 0.14212345 0.81990016 0.55458516
		 0.14212345 0.81990016 0.55458516 0.14212345 0.81990016 0.55458516 0.14212345 0.81990016
		 0 0.15494741 0.98792273 0 0.15494741 0.98792273 0 0.15494741 0.98792273 0 0.15494741
		 0.98792273 0.51454288 0.34995219 0.7828021 0.51454288 0.34995219 0.7828021 0.51454288
		 0.34995219 0.7828021 0.51454288 0.34995219 0.7828021 3.0934157e-06 0.3455191 0.93841171
		 3.0934157e-06 0.3455191 0.93841171 3.0934157e-06 0.3455191 0.93841171 3.0934157e-06
		 0.3455191 0.93841171 0.37156963 -0.32754868 0.86870468 0.37156963 -0.32754868 0.86870468
		 0.37156963 -0.32754868 0.86870468 0.37156963 -0.32754868 0.86870468 0 -0.31255877
		 0.94989842 0 -0.31255877 0.94989842 0 -0.31255877 0.94989842 0 -0.31255877 0.94989842
		 0.4759264 -0.087187976 0.87515271 0.4759264 -0.087187976 0.87515271 0.4759264 -0.087187976
		 0.87515271 0.4759264 -0.087187976 0.87515271 0 -0.10017684 0.99496967 0 -0.10017684
		 0.99496967 0 -0.10017684 0.99496967 0 -0.10017684 0.99496967 -0.27061397 -0.023321815
		 -0.96240544 -0.27061397 -0.023321815 -0.96240544 -0.27061397 -0.023321815 -0.96240544
		 -0.27061397 -0.023321815 -0.96240544 5.8032271e-07 -0.023172431 -0.99973142 5.8032271e-07
		 -0.023172431 -0.99973142 5.8032271e-07 -0.023172431 -0.99973142 5.8032271e-07 -0.023172431
		 -0.99973142 -0.38126981 -0.060046364 -0.92251164 -0.38126981 -0.060046364 -0.92251164
		 -0.38126981 -0.060046364 -0.92251164 -0.38126981 -0.060046364 -0.92251164 0 -0.078086853
		 -0.99694657 0 -0.078086853 -0.99694657 0 -0.078086853 -0.99694657 0 -0.078086853
		 -0.99694657 -0.35296378 -0.0036803724 -0.93562973 -0.35296378 -0.0036803724 -0.93562973
		 -0.35296378 -0.0036803724 -0.93562973 -0.35296378 -0.0036803724 -0.93562973 0 0 -1
		 0 0 -1 0 0 -1 0 0 -1 0 -0.14730795 0.98909062 0 -0.14730795 0.98909062 0 -0.14730795
		 0.98909062 0 -0.14730795 0.98909062 0.80732602 -0.1920875 -0.55796689 0.80732602
		 -0.1920875 -0.55796689 0.80732602 -0.1920875 -0.55796689 0.80732602 -0.1920875 -0.55796689
		 -0.17378095 0.00013052054 -0.98478425 -0.17378095 0.00013052054 -0.98478425 -0.17378095
		 0.00013052054 -0.98478425 -0.17378095 0.00013052054 -0.98478425 0 0 -1 0 0 -1 0 0
		 -1 0 0 -1 0.69795901 0.23318258 0.67711085 0.69795901 0.23318258 0.67711085 0.69795901
		 0.23318258 0.67711085 0.69795901 0.23318258 0.67711085 0 0.26910496 0.96311092 0
		 0.26910496 0.96311092 0 0.26910496 0.96311092 0 0.26910496 0.96311092 0 1 0 0 1 0
		 0 1 0 0 1 0 0 0.094661266 -0.99550956 0 0.094661266 -0.99550956 0 0.094661266 -0.99550956
		 0 0.094661266 -0.99550956 0 0.094663076 0.99550927 0 0.094663076 0.99550927 0 0.094663076
		 0.99550927 0 0.094663076 0.99550927 0 -0.31952393 0.94757813 0 -0.31952393 0.94757813
		 0 -0.31952393 0.94757813 0 -0.31952393 0.94757813 -0.30174828 -0.28269207 0.91051269
		 -0.30174828 -0.28269207 0.91051269 -0.30174828 -0.28269207 0.91051269 -0.30174828
		 -0.28269207 0.91051269 0 -0.31951928 -0.9475798 0 -0.31951928 -0.9475798 0 -0.31951928
		 -0.9475798 0 -0.31951928 -0.9475798 0.58703202 -0.24004503 -0.77315706 0.58703202
		 -0.24004503 -0.77315706 0.58703202 -0.24004503 -0.77315706 0.58703202 -0.24004503
		 -0.77315706 0.95508283 -0.055080023 -0.29117528 0.95508283 -0.055080023 -0.29117528
		 0.95508283 -0.055080023 -0.29117528 0.95508283 -0.055080023 -0.29117528 -0.74668831
		 -0.19723128 -0.63526088 -0.74668831 -0.19723128 -0.63526088 -0.74668831 -0.19723128
		 -0.63526088 -0.74668831 -0.19723128 -0.63526088 0.20030418 -0.29050472 0.93567371
		 0.20030418 -0.29050472 0.93567371 0.20030418 -0.29050472 0.93567371 0.20030418 -0.29050472
		 0.93567371 -0.88740742 -0.085683867 0.45295292 -0.88740742 -0.085683867 0.45295292
		 -0.88740742 -0.085683867 0.45295292 -0.88740742 -0.085683867 0.45295292 -0.88740742
		 -0.085682712 -0.45295292 -0.88740742 -0.085682712 -0.45295292 -0.88740742 -0.085682712
		 -0.45295292 -0.88740742 -0.085682712 -0.45295292 -1 1.3002197e-07 0 -1 1.3002197e-07
		 0 -1 1.3002197e-07 0 -1 1.3002197e-07 0 0.95508283 -0.055080831 0.29117528 0.95508283
		 -0.055080831 0.29117528 0.95508283 -0.055080831 0.29117528 0.95508283 -0.055080831
		 0.29117528 1 -3.2505493e-08 0 1 -3.2505493e-08 0;
	setAttr ".n[830:995]" -type "float3"  1 -3.2505493e-08 0 1 -3.2505493e-08 0
		 0 -0.23366176 0.97231799 0 -0.23366176 0.97231799 0 -0.23366176 0.97231799 0 -0.23366176
		 0.97231799 -0.28451586 -0.2102939 0.93532205 -0.28451586 -0.2102939 0.93532205 -0.28451586
		 -0.2102939 0.93532205 -0.28451586 -0.2102939 0.93532205 0 -0.23365554 -0.97231948
		 0 -0.23365554 -0.97231948 0 -0.23365554 -0.97231948 0 -0.23365554 -0.97231948 0.62277663
		 -0.17638545 -0.76225811 0.62277663 -0.17638545 -0.76225811 0.62277663 -0.17638545
		 -0.76225811 0.62277663 -0.17638545 -0.76225811 0.023855396 0.99971539 0 0.023855396
		 0.99971539 0 0.023855396 0.99971539 0 0.023855396 0.99971539 0 -0.099275991 -0.99505997
		 0 -0.099275991 -0.99505997 0 -0.099275991 -0.99505997 0 -0.099275991 -0.99505997
		 0 0.99973685 -0.02293795 0 0.99973685 -0.02293795 0 0.99973685 -0.02293795 0 0.99973685
		 -0.02293795 0 0.958143 -0.057213653 0.28051487 0.958143 -0.057213653 0.28051487 0.958143
		 -0.057213653 0.28051487 0.958143 -0.057213653 0.28051487 0.958143 -0.057213698 -0.28051487
		 0.958143 -0.057213698 -0.28051487 0.958143 -0.057213698 -0.28051487 0.958143 -0.057213698
		 -0.28051487 -0.77616131 -0.14488827 -0.61366194 -0.77616131 -0.14488827 -0.61366194
		 -0.77616131 -0.14488827 -0.61366194 -0.77616131 -0.14488827 -0.61366194 0.18832491
		 -0.21440251 0.95841807 0.18832491 -0.21440251 0.95841807 0.18832491 -0.21440251 0.95841807
		 0.18832491 -0.21440251 0.95841807 -0.099127226 -0.99357778 -0.054561783 -0.099127226
		 -0.99357778 -0.054561783 -0.099127226 -0.99357778 -0.054561783 -0.099127226 -0.99357778
		 -0.054561783 -0.85744393 -0.51457739 0 -0.85744393 -0.51457739 0 -0.85744393 -0.51457739
		 0 -0.85744393 -0.51457739 0 -0.77195758 -0.51078838 -0.37838694 -0.77195758 -0.51078838
		 -0.37838694 -0.77195758 -0.51078838 -0.37838694 -0.77195758 -0.51078838 -0.37838694
		 -0.77195823 -0.5107888 0.37838507 -0.77195823 -0.5107888 0.37838507 -0.77195823 -0.5107888
		 0.37838507 -0.77195823 -0.5107888 0.37838507 -0.0077287983 -0.20044886 0.97967368
		 -0.0077287983 -0.20044886 0.97967368 -0.0077287983 -0.20044886 0.97967368 -0.0077287983
		 -0.20044886 0.97967368 0.01261015 0.48268324 0.87570417 0.01261015 0.48268324 0.87570417
		 0.01261015 0.48268324 0.87570417 0.01261015 0.48268324 0.87570417 -0.0077158525 -0.20044887
		 -0.9796738 -0.0077158525 -0.20044887 -0.9796738 -0.0077158525 -0.20044887 -0.9796738
		 -0.0077158525 -0.20044887 -0.9796738 -0.099129423 -0.9935776 0.054561771 -0.099129423
		 -0.9935776 0.054561771 -0.099129423 -0.9935776 0.054561771 -0.099129423 -0.9935776
		 0.054561771 0.016572665 0.63454551 0.77270788 0.016572665 0.63454551 0.77270788 0.016572665
		 0.63454551 0.77270788 0.016572665 0.63454551 0.77270788 0.011879005 0.30648801 0.95180041
		 0.011879005 0.30648801 0.95180041 0.011879005 0.30648801 0.95180041 0.011879005 0.30648801
		 0.95180041 -0.010197662 -0.27146736 0.96239364 -0.010197662 -0.27146736 0.96239364
		 -0.010197662 -0.27146736 0.96239364 -0.010197662 -0.27146736 0.96239364 -0.89320427
		 -0.17842133 0.41273719 -0.89320427 -0.17842133 0.41273719 -0.89320427 -0.17842133
		 0.41273719 -0.89320427 -0.17842133 0.41273719 0.010141407 0.38841167 -0.92143005
		 0.010141407 0.38841167 -0.92143005 0.010141407 0.38841167 -0.92143005 0.010141407
		 0.38841167 -0.92143005 0.59844524 0.751926 -0.27653316 0.59844524 0.751926 -0.27653316
		 0.59844524 0.751926 -0.27653316 0.59844524 0.751926 -0.27653316 -0.88527554 -0.21539742
		 -0.41217861 -0.88527554 -0.21539742 -0.41217861 -0.88527554 -0.21539742 -0.41217861
		 -0.88527554 -0.21539742 -0.41217861 0.01576256 0.60351282 -0.7971974 0.01576256 0.60351282
		 -0.7971974 0.01576256 0.60351282 -0.7971974 0.01576256 0.60351282 -0.7971974 0.58652699
		 0.76249868 0.27309689 0.58652699 0.76249868 0.27309689 0.58652699 0.76249868 0.27309689
		 0.58652699 0.76249868 0.27309689 0.053760555 0.99829853 -0.022577673 0.053760555
		 0.99829853 -0.022577673 0.053760555 0.99829853 -0.022577673 0.053760555 0.99829853
		 -0.022577673 0.059018984 0.99794948 0.024769589 0.059018984 0.99794948 0.024769589
		 0.059018984 0.99794948 0.024769589 0.059018984 0.99794948 0.024769589 -0.71849853
		 -0.6088388 -0.33626667 -0.71849853 -0.6088388 -0.33626667 -0.71849853 -0.6088388
		 -0.33626667 -0.71849853 -0.6088388 -0.33626667 0.67416996 0.66778481 0.31552875 0.67416996
		 0.66778481 0.31552875 0.67416996 0.66778481 0.31552875 0.67416996 0.66778481 0.31552875
		 -0.0088718086 -0.22851862 -0.97349912 -0.0088718086 -0.22851862 -0.97349912 -0.0088718086
		 -0.22851862 -0.97349912 -0.0088718086 -0.22851862 -0.97349912 0.012769866 0.33972552
		 -0.94043797 0.012769866 0.33972552 -0.94043797 0.012769866 0.33972552 -0.94043797
		 0.012769866 0.33972552 -0.94043797 -0.71197701 -0.62066829 0.32841989 -0.71197701
		 -0.62066829 0.32841989 -0.71197701 -0.62066829 0.32841989 -0.71197701 -0.62066829
		 0.32841989 0.67178917 0.6728071 -0.30988702 0.67178917 0.6728071 -0.30988702 0.67178917
		 0.6728071 -0.30988702 0.67178917 0.6728071 -0.30988702 -0.39245468 -0.61358303 0.68519723
		 -0.39245468 -0.61358303 0.68519723 -0.39245468 -0.61358303 0.68519723 -0.39245468
		 -0.61358303 0.68519723 -0.78892398 -0.56916052 -0.23163627 -0.78892398 -0.56916052
		 -0.23163627 -0.78892398 -0.56916052 -0.23163627 -0.78892398 -0.56916052 -0.23163627
		 0 -0.57979178 -0.81476468 0 -0.57979178 -0.81476468 0 -0.57979178 -0.81476468 0 -0.57979178
		 -0.81476468 -0.79595882 -0.55842 0.23370226 -0.79595882 -0.55842 0.23370226 -0.79595882
		 -0.55842 0.23370226 -0.79595882 -0.55842 0.23370226 0.67168254 -0.57736021 -0.46421725
		 0.67168254 -0.57736021 -0.46421725 0.67168254 -0.57736021 -0.46421725 0.67168254
		 -0.57736021 -0.46421725;
	setAttr ".n[996:1161]" -type "float3"  0.24282186 -0.60113978 -0.76135969 0.24282186
		 -0.60113978 -0.76135969 0.24282186 -0.60113978 -0.76135969 0.24282186 -0.60113978
		 -0.76135969 0.82243854 -0.56885391 0 0.82243854 -0.56885391 0 0.82243854 -0.56885391
		 0 0.82243854 -0.56885391 0 -0.12387165 -0.59188598 -0.79644632 -0.12387165 -0.59188598
		 -0.79644632 -0.12387165 -0.59188598 -0.79644632 -0.12387165 -0.59188598 -0.79644632
		 0 -0.6146881 0.78877026 0 -0.6146881 0.78877026 0 -0.6146881 0.78877026 0 -0.6146881
		 0.78877026 -0.1524242 -0.12795652 -0.97999692 -0.1524242 -0.12795652 -0.97999692
		 -0.1524242 -0.12795652 -0.97999692 -0.1524242 -0.12795652 -0.97999692 -0.4939284
		 -0.11120054 0.86236256 -0.4939284 -0.11120054 0.86236256 -0.4939284 -0.11120054 0.86236256
		 -0.4939284 -0.11120054 0.86236256 -0.95788872 -0.057875603 0.28124666 -0.95788872
		 -0.057875603 0.28124666 -0.95788872 -0.057875603 0.28124666 -0.95788872 -0.057875603
		 0.28124666 1.083613e-06 -1 -7.4643327e-07 1.083613e-06 -1 -7.4643327e-07 1.083613e-06
		 -1 -7.4643327e-07 1.083613e-06 -1 -7.4643327e-07 1.3329269e-06 -1 0 1.3329269e-06
		 -1 0 1.3329269e-06 -1 0 1.3329269e-06 -1 0 1.8278946e-06 -1 0 1.8278946e-06 -1 0
		 1.8278946e-06 -1 0 1.8278946e-06 -1 0 -1.8278944e-06 -1 0 -1.8278944e-06 -1 0 -1.8278944e-06
		 -1 0 -1.8278944e-06 -1 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994
		 0 -1.0377069e-06 -1 0 -1.0377069e-06 -1 0 -1.0377069e-06 -1 0 -1.0377069e-06 -1 0
		 0 -1 0 0 -1 0 0 -1 0 0 -1 0 -7.5894485e-07 -1 -5.2278966e-07 -7.5894485e-07 -1 -5.2278966e-07
		 -7.5894485e-07 -1 -5.2278966e-07 -7.5894485e-07 -1 -5.2278966e-07 0 -0.99999994 0
		 0 -0.99999994 0 0 -0.99999994 0 0 -0.99999994 0 0.62631851 -0.56864679 0.53325969
		 0.62631851 -0.56864679 0.53325969 0.62631851 -0.56864679 0.53325969 0.62631851 -0.56864679
		 0.53325969 -0.83840251 -0.54505175 0 -0.83840251 -0.54505175 0 -0.83840251 -0.54505175
		 0 -0.83840251 -0.54505175 0 0.68385863 -0.55583727 0.47263339 0.68385863 -0.55583727
		 0.47263339 0.68385863 -0.55583727 0.47263339 0.68385863 -0.55583727 0.47263339 0
		 0.70263714 -0.71154839 0 0.70263714 -0.71154839 0 0.70263714 -0.71154839 0 0.70263714
		 -0.71154839 -0.27562615 0.72656214 -0.62939477 -0.27562615 0.72656214 -0.62939477
		 -0.27562615 0.72656214 -0.62939477 -0.27562615 0.72656214 -0.62939477 0 0.83210588
		 0.55461675 0 0.83210588 0.55461675 0 0.83210588 0.55461675 0 0.83210588 0.55461675
		 0.17141378 0.84258062 0.51056361 0.17141378 0.84258062 0.51056361 0.17141378 0.84258062
		 0.51056361 0.17141378 0.84258062 0.51056361 0.57288325 0.81963706 0 0.57288325 0.81963706
		 0 0.57288325 0.81963706 0 0.57288325 0.81963706 0 0.51991242 0.78034943 -0.34748513
		 0.51991242 0.78034943 -0.34748513 0.51991242 0.78034943 -0.34748513 0.51991242 0.78034943
		 -0.34748513 -0.99869645 0.05104294 8.3569681e-08 -0.99869645 0.05104294 8.3569681e-08
		 -0.99869645 0.05104294 8.3569681e-08 -0.99869645 0.05104294 8.3569681e-08 -0.88528883
		 0.015012667 -0.4647992 -0.88528883 0.015012667 -0.4647992 -0.88528883 0.015012667
		 -0.4647992 -0.88528883 0.015012667 -0.4647992 -0.90777212 0.13378236 0.39755762 -0.90777212
		 0.13378236 0.39755762 -0.90777212 0.13378236 0.39755762 -0.90777212 0.13378236 0.39755762
		 -0.15486918 0.83296841 0.53120542 -0.15486918 0.83296841 0.53120542 -0.15486918 0.83296841
		 0.53120542 -0.15486918 0.83296841 0.53120542 0.29678777 0.75204015 -0.58851737 0.29678777
		 0.75204015 -0.58851737 0.29678777 0.75204015 -0.58851737 0.29678777 0.75204015 -0.58851737
		 0.45716444 0.83524871 0.3055492 0.45716444 0.83524871 0.3055492 0.45716444 0.83524871
		 0.3055492 0.45716444 0.83524871 0.3055492 -0.92238998 -0.069279909 0.37999612 -0.92238998
		 -0.069279909 0.37999612 -0.92238998 -0.069279909 0.37999612 -0.92238998 -0.069279909
		 0.37999612 0.91045934 -0.20324095 -0.36021787 0.91045934 -0.20324095 -0.36021787
		 0.91045934 -0.20324095 -0.36021787 0.91045934 -0.20324095 -0.36021787 -0.47585714
		 -0.22303224 0.85077411 -0.47585714 -0.22303224 0.85077411 -0.47585714 -0.22303224
		 0.85077411 -0.47585714 -0.22303224 0.85077411 0.30917498 -0.024789533 -0.9506821
		 0.30917498 -0.024789533 -0.9506821 0.30917498 -0.024789533 -0.9506821 0.30917498
		 -0.024789533 -0.9506821 0.80926031 -0.20341107 0.55110943 0.80926031 -0.20341107
		 0.55110943 0.80926031 -0.20341107 0.55110943 0.80926031 -0.20341107 0.55110943 0.97946477
		 0.20161505 0 0.97946477 0.20161505 0 0.97946477 0.20161505 0 0.97946477 0.20161505
		 0 0.86413407 0.052833226 0.50048065 0.86413407 0.052833226 0.50048065 0.86413407
		 0.052833226 0.50048065 0.86413407 0.052833226 0.50048065 0.81242633 0.2923516 0.50447404
		 0.81242633 0.2923516 0.50447404 0.81242633 0.2923516 0.50447404 0.81242633 0.2923516
		 0.50447404 0.79159886 -0.30599773 0.52890134 0.79159886 -0.30599773 0.52890134 0.79159886
		 -0.30599773 0.52890134 0.79159886 -0.30599773 0.52890134 0.40331331 0.0051340186
		 -0.91504753 0.40331331 0.0051340186 -0.91504753 0.40331331 0.0051340186 -0.91504753
		 0.40331331 0.0051340186 -0.91504753 0.42532653 0.13722298 -0.89457655 0.42532653
		 0.13722298 -0.89457655;
	setAttr ".n[1162:1327]" -type "float3"  0.42532653 0.13722298 -0.89457655 0.42532653
		 0.13722298 -0.89457655 0.3730886 -0.044647358 -0.9267208 0.3730886 -0.044647358 -0.9267208
		 0.3730886 -0.044647358 -0.9267208 0.3730886 -0.044647358 -0.9267208 -0.42034519 0.088101029
		 0.90307701 -0.42034519 0.088101029 0.90307701 -0.42034519 0.088101029 0.90307701
		 -0.42034519 0.088101029 0.90307701 -0.33568624 0.26869473 0.90283889 -0.33568624
		 0.26869473 0.90283889 -0.33568624 0.26869473 0.90283889 -0.33568624 0.26869473 0.90283889
		 -0.39976043 -0.28632444 0.87075245 -0.39976043 -0.28632444 0.87075245 -0.39976043
		 -0.28632444 0.87075245 -0.39976043 -0.28632444 0.87075245 -0.94568449 -0.091909446
		 0.31182292 -0.94568449 -0.091909446 0.31182292 -0.94568449 -0.091909446 0.31182292
		 -0.94568449 -0.091909446 0.31182292 -0.51521558 -0.8058477 0.29182571 -0.51521558
		 -0.8058477 0.29182571 -0.51521558 -0.8058477 0.29182571 -0.51521558 -0.8058477 0.29182571
		 -0.87690097 0.13266303 0.46200129 -0.87690097 0.13266303 0.46200129 -0.87690097 0.13266303
		 0.46200129 -0.87690097 0.13266303 0.46200129 -0.51615226 -0.12848724 0.84680444 -0.51615226
		 -0.12848724 0.84680444 -0.51615226 -0.12848724 0.84680444 -0.51615226 -0.12848724
		 0.84680444 -0.19155726 0.92003411 0.34182301 -0.19155726 0.92003411 0.34182301 -0.19155726
		 0.92003411 0.34182301 -0.19155726 0.92003411 0.34182301 0.4346619 -0.018590888 -0.90040183
		 0.4346619 -0.018590888 -0.90040183 0.4346619 -0.018590888 -0.90040183 0.4346619 -0.018590888
		 -0.90040183 0.26525232 0.76956677 -0.58086848 0.26525232 0.76956677 -0.58086848 0.26525232
		 0.76956677 -0.58086848 0.26525232 0.76956677 -0.58086848 0.90198582 -0.10080004 0.41983455
		 0.90198582 -0.10080004 0.41983455 0.90198582 -0.10080004 0.41983455 0.90198582 -0.10080004
		 0.41983455 0.50384384 0.8241244 0.25876725 0.50384384 0.8241244 0.25876725 0.50384384
		 0.8241244 0.25876725 0.50384384 0.8241244 0.25876725 -0.89963913 0.046795487 0.43411922
		 -0.89963913 0.046795487 0.43411922 -0.89963913 0.046795487 0.43411922 -0.89963913
		 0.046795487 0.43411922 -0.56070387 0.79266655 0.2393553 -0.56070387 0.79266655 0.2393553
		 -0.56070387 0.79266655 0.2393553 -0.56070387 0.79266655 0.2393553 0.99298334 0.013666908
		 -0.1174625 0.99298334 0.013666908 -0.1174625 0.99298334 0.013666908 -0.1174625 0.99298334
		 0.013666908 -0.1174625 -0.52168691 -0.010598617 -0.85307127 -0.52168691 -0.010598617
		 -0.85307127 -0.52168691 -0.010598617 -0.85307127 -0.52168691 -0.010598617 -0.85307127
		 0.49219486 -0.25911829 0.83102459 0.49219486 -0.25911829 0.83102459 0.49219486 -0.25911829
		 0.83102459 0.49219486 -0.25911829 0.83102459 -0.92169994 -0.091820925 0.37687942
		 -0.92169994 -0.091820925 0.37687942 -0.92169994 -0.091820925 0.37687942 -0.92169994
		 -0.091820925 0.37687942 0 -1 0 0 -1 0 0 -1 0 0 -1 0 -0.95511049 0.04410978 -0.29294732
		 -0.95511049 0.04410978 -0.29294732 -0.95511049 0.04410978 -0.29294732 -0.95511049
		 0.04410978 -0.29294732 0.60568762 0.11847679 0.78683269 0.60568762 0.11847679 0.78683269
		 0.60568762 0.11847679 0.78683269 0.60568762 0.11847679 0.78683269 -0.98069608 0.022598678
		 0.19422807 -0.98069608 0.022598678 0.19422807 -0.98069608 0.022598678 0.19422807
		 -0.98069608 0.022598678 0.19422807 -0.94470859 -0.088904038 0.31562904 -0.94470859
		 -0.088904038 0.31562904 -0.94470859 -0.088904038 0.31562904 -0.94470859 -0.088904038
		 0.31562904 -0.93459678 0.065429665 0.34963936 -0.93459678 0.065429665 0.34963936
		 -0.93459678 0.065429665 0.34963936 -0.93459678 0.065429665 0.34963936 -0.91419023
		 0.078215867 0.39766645 -0.91419023 0.078215867 0.39766645 -0.91419023 0.078215867
		 0.39766645 -0.91419023 0.078215867 0.39766645 0.45236906 -0.32704362 0.8297016 0.45236906
		 -0.32704362 0.8297016 0.45236906 -0.32704362 0.8297016 0.45236906 -0.32704362 0.8297016
		 0.33862585 0.2072787 0.91780615 0.33862585 0.2072787 0.91780615 0.33862585 0.2072787
		 0.91780615 0.33862585 0.2072787 0.91780615 0.27082422 0.25853729 0.92726088 0.27082422
		 0.25853729 0.92726088 0.27082422 0.25853729 0.92726088 0.27082422 0.25853729 0.92726088
		 -0.77581739 0.033010583 -0.63009346 -0.77581739 0.033010583 -0.63009346 -0.77581739
		 0.033010583 -0.63009346 -0.77581739 0.033010583 -0.63009346 -0.82210195 0.13217804
		 -0.55378467 -0.82210195 0.13217804 -0.55378467 -0.82210195 0.13217804 -0.55378467
		 -0.82210195 0.13217804 -0.55378467 -0.78449214 0.17063786 -0.59620029 -0.78449214
		 0.17063786 -0.59620029 -0.78449214 0.17063786 -0.59620029 -0.78449214 0.17063786
		 -0.59620029 0.92052895 -0.32011861 -0.22394347 0.92052895 -0.32011861 -0.22394347
		 0.92052895 -0.32011861 -0.22394347 0.92052895 -0.32011861 -0.22394347 0.97475547
		 0.049096759 -0.21781026 0.97475547 0.049096759 -0.21781026 0.97475547 0.049096759
		 -0.21781026 0.97475547 0.049096759 -0.21781026 0.96642298 0.054583568 -0.25109199
		 0.96642298 0.054583568 -0.25109199 0.96642298 0.054583568 -0.25109199 0.96642298
		 0.054583568 -0.25109199 0.97142243 -0.096678615 -0.21677564 0.97142243 -0.096678615
		 -0.21677564 0.97142243 -0.096678615 -0.21677564 0.97142243 -0.096678615 -0.21677564
		 -0.71878272 -0.51305211 -0.46917892 -0.71878272 -0.51305211 -0.46917892 -0.71878272
		 -0.51305211 -0.46917892 -0.71878272 -0.51305211 -0.46917892 0.21967182 -0.73102015
		 0.64602935 0.21967182 -0.73102015 0.64602935 0.21967182 -0.73102015 0.64602935 0.21967182
		 -0.73102015 0.64602935 -0.91976488 -0.18960695 0.34363011 -0.91976488 -0.18960695
		 0.34363011 -0.91976488 -0.18960695 0.34363011 -0.91976488 -0.18960695 0.34363011
		 0.9527809 -0.18371466 -0.24177989 0.9527809 -0.18371466 -0.24177989 0.9527809 -0.18371466
		 -0.24177989 0.9527809 -0.18371466 -0.24177989 -0.64443755 -0.59655744 -0.47835076
		 -0.64443755 -0.59655744 -0.47835076 -0.64443755 -0.59655744 -0.47835076 -0.64443755
		 -0.59655744 -0.47835076;
	setAttr ".n[1328:1493]" -type "float3"  0.18388222 -0.76686543 0.61490226 0.18388222
		 -0.76686543 0.61490226 0.18388222 -0.76686543 0.61490226 0.18388222 -0.76686543 0.61490226
		 -0.88226074 -0.28481346 0.37482974 -0.88226074 -0.28481346 0.37482974 -0.88226074
		 -0.28481346 0.37482974 -0.88226074 -0.28481346 0.37482974 -0.85478294 -0.35294873
		 0.38049081 -0.85478294 -0.35294873 0.38049081 -0.85478294 -0.35294873 0.38049081
		 -0.85478294 -0.35294873 0.38049081 0.15480103 -0.8257522 0.54237431 0.15480103 -0.8257522
		 0.54237431 0.15480103 -0.8257522 0.54237431 0.15480103 -0.8257522 0.54237431 -0.57670569
		 -0.68282789 -0.44850475 -0.57670569 -0.68282789 -0.44850475 -0.57670569 -0.68282789
		 -0.44850475 -0.57670569 -0.68282789 -0.44850475 0.94009143 -0.23185129 -0.24994598
		 0.94009143 -0.23185129 -0.24994598 0.94009143 -0.23185129 -0.24994598 0.94009143
		 -0.23185129 -0.24994598 0.94009066 -0.23185486 0.24994577 0.94009066 -0.23185486
		 0.24994577 0.94009066 -0.23185486 0.24994577 0.94009066 -0.23185486 0.24994577 1
		 2.0012141e-08 0 1 2.0012141e-08 0 1 2.0012141e-08 0 1 2.0012141e-08 0 -0.85478163
		 -0.35294852 -0.38049415 -0.85478163 -0.35294852 -0.38049415 -0.85478163 -0.35294852
		 -0.38049415 -0.85478163 -0.35294852 -0.38049415 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.88225806
		 -0.28481489 -0.3748349 -0.88225806 -0.28481489 -0.3748349 -0.88225806 -0.28481489
		 -0.3748349 -0.88225806 -0.28481489 -0.3748349 -0.99999994 6.5571911e-08 0 -0.99999994
		 6.5571911e-08 0 -0.99999994 6.5571911e-08 0 -0.99999994 6.5571911e-08 0 0.95278186
		 -0.18371481 0.24177606 0.95278186 -0.18371481 0.24177606 0.95278186 -0.18371481 0.24177606
		 0.95278186 -0.18371481 0.24177606 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994
		 0 0 -0.91976303 -0.18960793 -0.34363469 -0.91976303 -0.18960793 -0.34363469 -0.91976303
		 -0.18960793 -0.34363469 -0.91976303 -0.18960793 -0.34363469 -0.99779195 0.066416994
		 0 -0.99779195 0.066416994 0 -0.99779195 0.066416994 0 -0.99779195 0.066416994 0 0.97142297
		 -0.096679613 0.21677288 0.97142297 -0.096679613 0.21677288 0.97142297 -0.096679613
		 0.21677288 0.97142297 -0.096679613 0.21677288 0.99799848 0.063238494 0 0.99799848
		 0.063238494 0 0.99799848 0.063238494 0 0.99799848 0.063238494 0 0.96642309 0.054584283
		 0.25109175 0.96642309 0.054584283 0.25109175 0.96642309 0.054584283 0.25109175 0.96642309
		 0.054584283 0.25109175 0.99989635 0.014394502 0 0.99989635 0.014394502 0 0.99989635
		 0.014394502 0 0.99989635 0.014394502 0 0.97475547 0.049097497 0.21781038 0.97475547
		 0.049097497 0.21781038 0.97475547 0.049097497 0.21781038 0.97475547 0.049097497 0.21781038
		 0.99973685 0.022937788 0 0.99973685 0.022937788 0 0.99973685 0.022937788 0 0.99973685
		 0.022937788 0 0.90802783 -0.36037043 0.21358576 0.90802783 -0.36037043 0.21358576
		 0.90802783 -0.36037043 0.21358576 0.90802783 -0.36037043 0.21358576 0.93160194 -0.3634803
		 0 0.93160194 -0.3634803 0 0.93160194 -0.3634803 0 0.93160194 -0.3634803 0 -0.91419035
		 0.078215167 -0.39766625 -0.91419035 0.078215167 -0.39766625 -0.91419035 0.078215167
		 -0.39766625 -0.91419035 0.078215167 -0.39766625 -0.99988604 0.015096474 0 -0.99988604
		 0.015096474 0 -0.99988604 0.015096474 0 -0.99988604 0.015096474 0 -0.934596 0.065429457
		 -0.34964189 -0.934596 0.065429457 -0.34964189 -0.934596 0.065429457 -0.34964189 -0.934596
		 0.065429457 -0.34964189 -0.9997099 0.024086313 0 -0.9997099 0.024086313 0 -0.9997099
		 0.024086313 0 -0.9997099 0.024086313 0 -0.94491196 -0.023006225 -0.32651511 -0.94491196
		 -0.023006225 -0.32651511 -0.94491196 -0.023006225 -0.32651511 -0.94491196 -0.023006225
		 -0.32651511 -0.99765068 -0.068506137 0 -0.99765068 -0.068506137 0 -0.99765068 -0.068506137
		 0 -0.99765068 -0.068506137 0 -0.9806965 0.02259847 -0.19422609 -0.9806965 0.02259847
		 -0.19422609 -0.9806965 0.02259847 -0.19422609 -0.9806965 0.02259847 -0.19422609 0
		 -1 0 0 -1 0 0 -1 0 0 -1 0 -0.92180848 -0.026256474 -0.38675529 -0.92180848 -0.026256474
		 -0.38675529 -0.92180848 -0.026256474 -0.38675529 -0.92180848 -0.026256474 -0.38675529
		 -0.99850368 -0.05468433 0 -0.99850368 -0.05468433 0 -0.99850368 -0.05468433 0 -0.99850368
		 -0.05468433 0 0.9929831 0.01366704 0.11746372 0.9929831 0.01366704 0.11746372 0.9929831
		 0.01366704 0.11746372 0.9929831 0.01366704 0.11746372 -0.71434104 0.6298635 -0.30494073
		 -0.71434104 0.6298635 -0.30494073 -0.71434104 0.6298635 -0.30494073 -0.71434104 0.6298635
		 -0.30494073 -0.78050536 0.62514913 0 -0.78050536 0.62514913 0 -0.78050536 0.62514913
		 0 -0.78050536 0.62514913 0 -0.9005807 0.010026433 -0.43457311 -0.9005807 0.010026433
		 -0.43457311 -0.9005807 0.010026433 -0.43457311 -0.9005807 0.010026433 -0.43457311
		 -0.99978232 0.020863306 0 -0.99978232 0.020863306 0 -0.99978232 0.020863306 0 -0.99978232
		 0.020863306 0 0.66590178 0.66303182 -0.34199962 0.66590178 0.66303182 -0.34199962
		 0.66590178 0.66303182 -0.34199962 0.66590178 0.66303182 -0.34199962 0.74385321 0.66834301
		 0 0.74385321 0.66834301 0 0.74385321 0.66834301 0 0.74385321 0.66834301 0 0.90566128
		 -0.045574378 -0.42154539 0.90566128 -0.045574378 -0.42154539;
	setAttr ".n[1494:1659]" -type "float3"  0.90566128 -0.045574378 -0.42154539 0.90566128
		 -0.045574378 -0.42154539 0.99716663 -0.075224385 0 0.99716663 -0.075224385 0 0.99716663
		 -0.075224385 0 0.99716663 -0.075224385 0 -0.88419163 0.034534682 -0.46584621 -0.88419163
		 0.034534682 -0.46584621 -0.88419163 0.034534682 -0.46584621 -0.88419163 0.034534682
		 -0.46584621 -0.99848354 0.055050891 0 -0.99848354 0.055050891 0 -0.99848354 0.055050891
		 0 -0.99848354 0.055050891 0 -0.57937235 -0.74608356 -0.32815707 -0.57937235 -0.74608356
		 -0.32815707 -0.57937235 -0.74608356 -0.32815707 -0.57937235 -0.74608356 -0.32815707
		 -0.63973123 -0.76859874 0 -0.63973123 -0.76859874 0 -0.63973123 -0.76859874 0 -0.63973123
		 -0.76859874 0 -0.93761104 -0.0046793697 -0.34765443 -0.93761104 -0.0046793697 -0.34765443
		 -0.93761104 -0.0046793697 -0.34765443 -0.93761104 -0.0046793697 -0.34765443 -0.99963474
		 -0.027023692 2.6170861e-08 -0.99963474 -0.027023692 2.6170861e-08 -0.99963474 -0.027023692
		 2.6170861e-08 -0.99963474 -0.027023692 2.6170861e-08 0.77225858 -0.14486845 -0.6185708
		 0.77225858 -0.14486845 -0.6185708 0.77225858 -0.14486845 -0.6185708 0.77225858 -0.14486845
		 -0.6185708 0.96731156 -0.253591 0 0.96731156 -0.253591 0 0.96731156 -0.253591 0 0.96731156
		 -0.253591 0 0.83409357 0.18982343 -0.51793337 0.83409357 0.18982343 -0.51793337 0.83409357
		 0.18982343 -0.51793337 0.83409357 0.18982343 -0.51793337 0.97029799 0.24191302 0
		 0.97029799 0.24191302 0 0.97029799 0.24191302 0 0.97029799 0.24191302 0 0.86529565
		 0.010445432 -0.50115311 0.86529565 0.010445432 -0.50115311 0.86529565 0.010445432
		 -0.50115311 0.86529565 0.010445432 -0.50115311 0.99970603 0.024245854 0 0.99970603
		 0.024245854 0 0.99970603 0.024245854 0 0.99970603 0.024245854 0 0 0.13475524 -0.99087888
		 0 0.13475524 -0.99087888 0 0.13475524 -0.99087888 0 0.13475524 -0.99087888 0.74827033
		 0.1849504 0.63709098 0.74827033 0.1849504 0.63709098 0.74827033 0.1849504 0.63709098
		 0.74827033 0.1849504 0.63709098 0.79301369 -0.048323356 -0.60728425 0.79301369 -0.048323356
		 -0.60728425 0.79301369 -0.048323356 -0.60728425 0.79301369 -0.048323356 -0.60728425
		 0.99157161 -0.12955981 0 0.99157161 -0.12955981 0 0.99157161 -0.12955981 0 0.99157161
		 -0.12955981 0 0.90058768 -0.26144984 0.34725466 0.90058768 -0.26144984 0.34725466
		 0.90058768 -0.26144984 0.34725466 0.90058768 -0.26144984 0.34725466 0.96786529 -0.25146905
		 0 0.96786529 -0.25146905 0 0.96786529 -0.25146905 0 0.96786529 -0.25146905 0 -0.92444175
		 -0.019169852 -0.38084117 -0.92444175 -0.019169852 -0.38084117 -0.92444175 -0.019169852
		 -0.38084117 -0.92444175 -0.019169852 -0.38084117 -0.99951041 -0.031287052 0 -0.99951041
		 -0.031287052 0 -0.99951041 -0.031287052 0 -0.99951041 -0.031287052 0 0.53042179 -0.84773391
		 0 0.53042179 -0.84773391 0 0.53042179 -0.84773391 0 0.53042179 -0.84773391 0 0.54384422
		 -0.83191299 0.11024569 0.54384422 -0.83191299 0.11024569 0.54384422 -0.83191299 0.11024569
		 0.54384422 -0.83191299 0.11024569 -0.52146655 -0.85327172 0 -0.52146655 -0.85327172
		 0 -0.52146655 -0.85327172 0 -0.52146655 -0.85327172 0 -0.53992933 -0.82152116 -0.1832469
		 -0.53992933 -0.82152116 -0.1832469 -0.53992933 -0.82152116 -0.1832469 -0.53992933
		 -0.82152116 -0.1832469 -0.53992581 -0.821522 0.18325341 -0.53992581 -0.821522 0.18325341
		 -0.53992581 -0.821522 0.18325341 -0.53992581 -0.821522 0.18325341 0.21058413 -0.76310474
		 0.6110037 0.21058413 -0.76310474 0.6110037 0.21058413 -0.76310474 0.6110037 0.21058413
		 -0.76310474 0.6110037 -0.56688714 -0.75601685 -0.32722718 -0.56688714 -0.75601685
		 -0.32722718 -0.56688714 -0.75601685 -0.32722718 -0.56688714 -0.75601685 -0.32722718
		 0.54384696 -0.83191144 -0.11024438 0.54384696 -0.83191144 -0.11024438 0.54384696
		 -0.83191144 -0.11024438 0.54384696 -0.83191144 -0.11024438 0.45839581 -0.78871518
		 -0.40963596 0.45839581 -0.78871518 -0.40963596 0.45839581 -0.78871518 -0.40963596
		 0.45839581 -0.78871518 -0.40963596 0 -0.71730369 -0.69676071 0 -0.71730369 -0.69676071
		 0 -0.71730369 -0.69676071 0 -0.71730369 -0.69676071 -0.30040151 -0.76992625 0.56300318
		 -0.30040151 -0.76992625 0.56300318 -0.30040151 -0.76992625 0.56300318 -0.30040151
		 -0.76992625 0.56300318 0 -0.71729863 0.6967659 0 -0.71729863 0.6967659 0 -0.71729863
		 0.6967659 0 -0.71729863 0.6967659 0.80842721 0.18512286 0.55872631 0.80842721 0.18512286
		 0.55872631 0.80842721 0.18512286 0.55872631 0.80842721 0.18512286 0.55872631 0.29975271
		 0.16389303 -0.93983376 0.29975271 0.16389303 -0.93983376 0.29975271 0.16389303 -0.93983376
		 0.29975271 0.16389303 -0.93983376 -0.95768011 0.061500184 -0.28118744 -0.95768011
		 0.061500184 -0.28118744 -0.95768011 0.061500184 -0.28118744 -0.95768011 0.061500184
		 -0.28118744 -0.99872297 0.050521202 0 -0.99872297 0.050521202 0 -0.99872297 0.050521202
		 0 -0.99872297 0.050521202 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.99999994 0 0 -0.99999994
		 0 0 -0.99999994 0 0 -0.99999994 0 0.41489837 -0.76048905 -0.49951559 0.41489837 -0.76048905
		 -0.49951559 0.41489837 -0.76048905 -0.49951559 0.41489837 -0.76048905 -0.49951559
		 0 -0.85568345 -0.51749957 0 -0.85568345 -0.51749957 0 -0.85568345 -0.51749957 0 -0.85568345
		 -0.51749957;
	setAttr ".n[1660:1825]" -type "float3"  -0.23573288 -0.81227213 0.53352028 -0.23573288
		 -0.81227213 0.53352028 -0.23573288 -0.81227213 0.53352028 -0.23573288 -0.81227213
		 0.53352028 0 -0.8556878 0.51749247 0 -0.8556878 0.51749247 0 -0.8556878 0.51749247
		 0 -0.8556878 0.51749247 -0.2781727 -0.74937588 0.60087919 -0.2781727 -0.74937588
		 0.60087919 -0.2781727 -0.74937588 0.60087919 -0.2781727 -0.74937588 0.60087919 0
		 -0.80450076 0.59395164 0 -0.80450076 0.59395164 0 -0.80450076 0.59395164 0 -0.80450076
		 0.59395164 0.47813985 -0.68520534 -0.54943234 0.47813985 -0.68520534 -0.54943234
		 0.47813985 -0.68520534 -0.54943234 0.47813985 -0.68520534 -0.54943234 0 -0.80449539
		 -0.59395897 0 -0.80449539 -0.59395897 0 -0.80449539 -0.59395897 0 -0.80449539 -0.59395897
		 -0.32995003 -0.70586729 0.62680483 -0.32995003 -0.70586729 0.62680483 -0.32995003
		 -0.70586729 0.62680483 -0.32995003 -0.70586729 0.62680483 0 -0.77880675 0.62726396
		 0 -0.77880675 0.62726396 0 -0.77880675 0.62726396 0 -0.77880675 0.62726396 0.55236048
		 -0.6191597 -0.55815697 0.55236048 -0.6191597 -0.55815697 0.55236048 -0.6191597 -0.55815697
		 0.55236048 -0.6191597 -0.55815697 0 -0.77880085 -0.62727129 0 -0.77880085 -0.62727129
		 0 -0.77880085 -0.62727129 0 -0.77880085 -0.62727129 0.63306683 0.21090023 -0.74481374
		 0.63306683 0.21090023 -0.74481374 0.63306683 0.21090023 -0.74481374 0.63306683 0.21090023
		 -0.74481374 0 0.28764728 -0.95773637 0 0.28764728 -0.95773637 0 0.28764728 -0.95773637
		 0 0.28764728 -0.95773637 0.68253642 0.16595231 -0.71176106 0.68253642 0.16595231
		 -0.71176106 0.68253642 0.16595231 -0.71176106 0.68253642 0.16595231 -0.71176106 0
		 0.23365341 -0.97231996 0 0.23365341 -0.97231996 0 0.23365341 -0.97231996 0 0.23365341
		 -0.97231996 0.66288465 -0.18430267 -0.72568339 0.66288465 -0.18430267 -0.72568339
		 0.66288465 -0.18430267 -0.72568339 0.66288465 -0.18430267 -0.72568339 0 0 -1 0 0
		 -1 0 0 -1 0 0 -1 -0.39921483 0.2471028 0.88293129 -0.39921483 0.2471028 0.88293129
		 -0.39921483 0.2471028 0.88293129 -0.39921483 0.2471028 0.88293129 0 0.28765216 0.95773494
		 0 0.28765216 0.95773494 0 0.28765216 0.95773494 0 0.28765216 0.95773494 -0.48652655
		 0.19418482 0.85181224 -0.48652655 0.19418482 0.85181224 -0.48652655 0.19418482 0.85181224
		 -0.48652655 0.19418482 0.85181224 0 0.23365633 0.97231919 0 0.23365633 0.97231919
		 0 0.23365633 0.97231919 0 0.23365633 0.97231919 -0.5837875 -0.16786228 0.79436415
		 -0.5837875 -0.16786228 0.79436415 -0.5837875 -0.16786228 0.79436415 -0.5837875 -0.16786228
		 0.79436415 0 -0.25477138 0.96700132 0 -0.25477138 0.96700132 0 -0.25477138 0.96700132
		 0 -0.25477138 0.96700132 -0.76246238 0.096340336 0.6398201 -0.76246238 0.096340336
		 0.6398201 -0.76246238 0.096340336 0.6398201 -0.76246238 0.096340336 0.6398201 0 0.16139036
		 0.98689061 0 0.16139036 0.98689061 0 0.16139036 0.98689061 0 0.16139036 0.98689061
		 0.90146202 0.064449996 -0.42803332 0.90146202 0.064449996 -0.42803332 0.90146202
		 0.064449996 -0.42803332 0.90146202 0.064449996 -0.42803332 0 0.16138858 -0.98689091
		 0 0.16138858 -0.98689091 0 0.16138858 -0.98689091 0 0.16138858 -0.98689091 0 -1 0
		 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 -0.52140224 -0.1643026 0.83734363
		 -0.52140224 -0.1643026 0.83734363 -0.52140224 -0.1643026 0.83734363 -0.52140224 -0.1643026
		 0.83734363 0 -0.21878982 0.97577208 0 -0.21878982 0.97577208 0 -0.21878982 0.97577208
		 0 -0.21878982 0.97577208 0.4998467 -0.10822356 -0.85932589 0.4998467 -0.10822356
		 -0.85932589 0.4998467 -0.10822356 -0.85932589 0.4998467 -0.10822356 -0.85932589 0
		 -0.024401089 -0.99970222 0 -0.024401089 -0.99970222 0 -0.024401089 -0.99970222 0
		 -0.024401089 -0.99970222 -0.23514691 0.76241398 -0.60285223 -0.23514691 0.76241398
		 -0.60285223 -0.23514691 0.76241398 -0.60285223 -0.23514691 0.76241398 -0.60285223
		 3.0489391e-06 0.769916 -0.63814527 3.0489391e-06 0.769916 -0.63814527 3.0489391e-06
		 0.769916 -0.63814527 3.0489391e-06 0.769916 -0.63814527 -0.39889902 -0.0047904369
		 -0.91698235 -0.39889902 -0.0047904369 -0.91698235 -0.39889902 -0.0047904369 -0.91698235
		 -0.39889902 -0.0047904369 -0.91698235 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0.2169226 0.91849142
		 0.33063316 0.2169226 0.91849142 0.33063316 0.2169226 0.91849142 0.33063316 0.2169226
		 0.91849142 0.33063316 -1.671337e-06 0.93682086 0.34980959 -1.671337e-06 0.93682086
		 0.34980959 -1.671337e-06 0.93682086 0.34980959 -1.671337e-06 0.93682086 0.34980959
		 0.55458516 -0.14212345 0.8199001 0.55458516 -0.14212345 0.8199001 0.55458516 -0.14212345
		 0.8199001 0.55458516 -0.14212345 0.8199001 0 -0.15494736 0.98792273 0 -0.15494736
		 0.98792273 0 -0.15494736 0.98792273 0 -0.15494736 0.98792273 0.51454288 -0.34995219
		 0.7828021 0.51454288 -0.34995219 0.7828021 0.51454288 -0.34995219 0.7828021 0.51454288
		 -0.34995219 0.7828021 4.1835015e-06 -0.34551919 0.93841171 4.1835015e-06 -0.34551919
		 0.93841171;
	setAttr ".n[1826:1991]" -type "float3"  4.1835015e-06 -0.34551919 0.93841171
		 4.1835015e-06 -0.34551919 0.93841171 0.37156963 0.32754871 0.86870468 0.37156963
		 0.32754871 0.86870468 0.37156963 0.32754871 0.86870468 0.37156963 0.32754871 0.86870468
		 0 0.31255868 0.94989842 0 0.31255868 0.94989842 0 0.31255868 0.94989842 0 0.31255868
		 0.94989842 0.47592628 0.087187946 0.87515283 0.47592628 0.087187946 0.87515283 0.47592628
		 0.087187946 0.87515283 0.47592628 0.087187946 0.87515283 0 0.10017681 0.99496967
		 0 0.10017681 0.99496967 0 0.10017681 0.99496967 0 0.10017681 0.99496967 -0.27061397
		 0.023321811 -0.96240544 -0.27061397 0.023321811 -0.96240544 -0.27061397 0.023321811
		 -0.96240544 -0.27061397 0.023321811 -0.96240544 0 0.023172423 -0.99973142 0 0.023172423
		 -0.99973142 0 0.023172423 -0.99973142 0 0.023172423 -0.99973142 -0.38126975 0.060046352
		 -0.92251164 -0.38126975 0.060046352 -0.92251164 -0.38126975 0.060046352 -0.92251164
		 -0.38126975 0.060046352 -0.92251164 0 0.07808689 -0.99694657 0 0.07808689 -0.99694657
		 0 0.07808689 -0.99694657 0 0.07808689 -0.99694657 -0.35296372 0.0036803717 -0.93562973
		 -0.35296372 0.0036803717 -0.93562973 -0.35296372 0.0036803717 -0.93562973 -0.35296372
		 0.0036803717 -0.93562973 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0.14730798 0.98909068 0 0.14730798
		 0.98909068 0 0.14730798 0.98909068 0 0.14730798 0.98909068 0.80732536 0.19208786
		 -0.55796784 0.80732536 0.19208786 -0.55796784 0.80732536 0.19208786 -0.55796784 0.80732536
		 0.19208786 -0.55796784 -0.17378095 -0.00013052054 -0.98478436 -0.17378095 -0.00013052054
		 -0.98478436 -0.17378095 -0.00013052054 -0.98478436 -0.17378095 -0.00013052054 -0.98478436
		 0 0 -1 0 0 -1 0 0 -1 0 0 -1 0.69795901 -0.23318256 0.67711085 0.69795901 -0.23318256
		 0.67711085 0.69795901 -0.23318256 0.67711085 0.69795901 -0.23318256 0.67711085 0
		 -0.26910499 0.96311086 0 -0.26910499 0.96311086 0 -0.26910499 0.96311086 0 -0.26910499
		 0.96311086 0 -1 0 0 -1 0 0 -1 0 0 -1 0 0 -0.094661266 -0.99550956 0 -0.094661266
		 -0.99550956 0 -0.094661266 -0.99550956 0 -0.094661266 -0.99550956 0 -0.094663076
		 0.99550927 0 -0.094663076 0.99550927 0 -0.094663076 0.99550927 0 -0.094663076 0.99550927
		 0 0.31952393 0.94757813 0 0.31952393 0.94757813 0 0.31952393 0.94757813 0 0.31952393
		 0.94757813 -0.30174828 0.28269202 0.91051269 -0.30174828 0.28269202 0.91051269 -0.30174828
		 0.28269202 0.91051269 -0.30174828 0.28269202 0.91051269 0 0.31951928 -0.9475798 0
		 0.31951928 -0.9475798 0 0.31951928 -0.9475798 0 0.31951928 -0.9475798 0.58703202
		 0.24004498 -0.77315706 0.58703202 0.24004498 -0.77315706 0.58703202 0.24004498 -0.77315706
		 0.58703202 0.24004498 -0.77315706 0.95508283 0.055080023 -0.29117528 0.95508283 0.055080023
		 -0.29117528 0.95508283 0.055080023 -0.29117528 0.95508283 0.055080023 -0.29117528
		 -0.74668831 0.19723128 -0.63526088 -0.74668831 0.19723128 -0.63526088 -0.74668831
		 0.19723128 -0.63526088 -0.74668831 0.19723128 -0.63526088 0.20030418 0.29050466 0.93567371
		 0.20030418 0.29050466 0.93567371 0.20030418 0.29050466 0.93567371 0.20030418 0.29050466
		 0.93567371 -0.88740742 0.085683919 0.45295292 -0.88740742 0.085683919 0.45295292
		 -0.88740742 0.085683919 0.45295292 -0.88740742 0.085683919 0.45295292 -0.88740742
		 0.085682653 -0.45295292 -0.88740742 0.085682653 -0.45295292 -0.88740742 0.085682653
		 -0.45295292 -0.88740742 0.085682653 -0.45295292 -1 -9.7516477e-08 0 -1 -9.7516477e-08
		 0 -1 -9.7516477e-08 0 -1 -9.7516477e-08 0 0.95508283 0.055080839 0.29117528 0.95508283
		 0.055080839 0.29117528 0.95508283 0.055080839 0.29117528 0.95508283 0.055080839 0.29117528
		 1 1.6252747e-08 0 1 1.6252747e-08 0 1 1.6252747e-08 0 1 1.6252747e-08 0 0 0.23366208
		 0.97231787 0 0.23366208 0.97231787 0 0.23366208 0.97231787 0 0.23366208 0.97231787
		 -0.28451604 0.21029396 0.93532187 -0.28451604 0.21029396 0.93532187 -0.28451604 0.21029396
		 0.93532187 -0.28451604 0.21029396 0.93532187 0 0.23365547 -0.97231948 0 0.23365547
		 -0.97231948 0 0.23365547 -0.97231948 0 0.23365547 -0.97231948 0.62277573 0.17638516
		 -0.76225889 0.62277573 0.17638516 -0.76225889 0.62277573 0.17638516 -0.76225889 0.62277573
		 0.17638516 -0.76225889 0.023855396 -0.99971539 0 0.023855396 -0.99971539 0 0.023855396
		 -0.99971539 0 0.023855396 -0.99971539 0 -0.099275991 0.99505997 0 -0.099275991 0.99505997
		 0 -0.099275991 0.99505997 0 -0.099275991 0.99505997 0 0.99973685 0.02293795 0 0.99973685
		 0.02293795 0 0.99973685 0.02293795 0 0.99973685 0.02293795 0 0.95814294 0.057213649
		 0.28051516 0.95814294 0.057213649 0.28051516 0.95814294 0.057213649 0.28051516 0.95814294
		 0.057213649 0.28051516 0.95814294 0.057213672 -0.28051516 0.95814294 0.057213672
		 -0.28051516 0.95814294 0.057213672 -0.28051516 0.95814294 0.057213672 -0.28051516
		 -0.77616113 0.14488822 -0.61366224 -0.77616113 0.14488822 -0.61366224 -0.77616113
		 0.14488822 -0.61366224 -0.77616113 0.14488822 -0.61366224;
	setAttr ".n[1992:2157]" -type "float3"  0.1883246 0.21440214 0.95841831 0.1883246
		 0.21440214 0.95841831 0.1883246 0.21440214 0.95841831 0.1883246 0.21440214 0.95841831
		 -0.099127226 0.99357778 -0.054561783 -0.099127226 0.99357778 -0.054561783 -0.099127226
		 0.99357778 -0.054561783 -0.099127226 0.99357778 -0.054561783 -0.85744393 0.51457739
		 0 -0.85744393 0.51457739 0 -0.85744393 0.51457739 0 -0.85744393 0.51457739 0 -0.77195758
		 0.51078844 -0.37838694 -0.77195758 0.51078844 -0.37838694 -0.77195758 0.51078844
		 -0.37838694 -0.77195758 0.51078844 -0.37838694 -0.77195758 0.51078844 0.37838694
		 -0.77195758 0.51078844 0.37838694 -0.77195758 0.51078844 0.37838694 -0.77195758 0.51078844
		 0.37838694 -0.0077259662 0.20044886 0.97967368 -0.0077259662 0.20044886 0.97967368
		 -0.0077259662 0.20044886 0.97967368 -0.0077259662 0.20044886 0.97967368 0.012612227
		 -0.48268661 0.87570232 0.012612227 -0.48268661 0.87570232 0.012612227 -0.48268661
		 0.87570232 0.012612227 -0.48268661 0.87570232 -0.0077215284 0.20044918 -0.97967368
		 -0.0077215284 0.20044918 -0.97967368 -0.0077215284 0.20044918 -0.97967368 -0.0077215284
		 0.20044918 -0.97967368 -0.099129371 0.99357706 0.054570615 -0.099129371 0.99357706
		 0.054570615 -0.099129371 0.99357706 0.054570615 -0.099129371 0.99357706 0.054570615
		 0.01657453 -0.63454896 0.77270496 0.01657453 -0.63454896 0.77270496 0.01657453 -0.63454896
		 0.77270496 0.01657453 -0.63454896 0.77270496 0.011886057 -0.30648884 0.95180011 0.011886057
		 -0.30648884 0.95180011 0.011886057 -0.30648884 0.95180011 0.011886057 -0.30648884
		 0.95180011 -0.010208614 0.27146769 0.96239334 -0.010208614 0.27146769 0.96239334
		 -0.010208614 0.27146769 0.96239334 -0.010208614 0.27146769 0.96239334 -0.89320523
		 0.17842184 0.41273502 -0.89320523 0.17842184 0.41273502 -0.89320523 0.17842184 0.41273502
		 -0.89320523 0.17842184 0.41273502 0.010139198 -0.38840717 -0.92143202 0.010139198
		 -0.38840717 -0.92143202 0.010139198 -0.38840717 -0.92143202 0.010139198 -0.38840717
		 -0.92143202 0.5984453 -0.75192595 -0.27653319 0.5984453 -0.75192595 -0.27653319 0.5984453
		 -0.75192595 -0.27653319 0.5984453 -0.75192595 -0.27653319 -0.88527364 0.21539679
		 -0.4121829 -0.88527364 0.21539679 -0.4121829 -0.88527364 0.21539679 -0.4121829 -0.88527364
		 0.21539679 -0.4121829 0.015760617 -0.60350847 -0.7972008 0.015760617 -0.60350847
		 -0.7972008 0.015760617 -0.60350847 -0.7972008 0.015760617 -0.60350847 -0.7972008
		 0.58652693 -0.76249868 0.27309686 0.58652693 -0.76249868 0.27309686 0.58652693 -0.76249868
		 0.27309686 0.58652693 -0.76249868 0.27309686 0.053760562 -0.99829865 -0.022573672
		 0.053760562 -0.99829865 -0.022573672 0.053760562 -0.99829865 -0.022573672 0.053760562
		 -0.99829865 -0.022573672 0.059018996 -0.99794966 0.024764767 0.059018996 -0.99794966
		 0.024764767 0.059018996 -0.99794966 0.024764767 0.059018996 -0.99794966 0.024764767
		 -0.71849889 0.60883909 -0.33626512 -0.71849889 0.60883909 -0.33626512 -0.71849889
		 0.60883909 -0.33626512 -0.71849889 0.60883909 -0.33626512 0.6741699 -0.66778469 0.31552905
		 0.6741699 -0.66778469 0.31552905 0.6741699 -0.66778469 0.31552905 0.6741699 -0.66778469
		 0.31552905 -0.008876604 0.22851862 -0.97349912 -0.008876604 0.22851862 -0.97349912
		 -0.008876604 0.22851862 -0.97349912 -0.008876604 0.22851862 -0.97349912 0.012777765
		 -0.33972505 -0.94043803 0.012777765 -0.33972505 -0.94043803 0.012777765 -0.33972505
		 -0.94043803 0.012777765 -0.33972505 -0.94043803 -0.71197677 0.62066799 0.32842076
		 -0.71197677 0.62066799 0.32842076 -0.71197677 0.62066799 0.32842076 -0.71197677 0.62066799
		 0.32842076 0.67178899 -0.67280692 -0.30988789 0.67178899 -0.67280692 -0.30988789
		 0.67178899 -0.67280692 -0.30988789 0.67178899 -0.67280692 -0.30988789 -0.39245468
		 0.61358309 0.68519711 -0.39245468 0.61358309 0.68519711 -0.39245468 0.61358309 0.68519711
		 -0.39245468 0.61358309 0.68519711 -0.78892374 0.56916064 -0.23163638 -0.78892374
		 0.56916064 -0.23163638 -0.78892374 0.56916064 -0.23163638 -0.78892374 0.56916064
		 -0.23163638 0 0.57979178 -0.81476474 0 0.57979178 -0.81476474 0 0.57979178 -0.81476474
		 0 0.57979178 -0.81476474 -0.79595929 0.55841947 0.23370187 -0.79595929 0.55841947
		 0.23370187 -0.79595929 0.55841947 0.23370187 -0.79595929 0.55841947 0.23370187 0.67168242
		 0.57736039 -0.46421731 0.67168242 0.57736039 -0.46421731 0.67168242 0.57736039 -0.46421731
		 0.67168242 0.57736039 -0.46421731 0.24282183 0.60113972 -0.76135975 0.24282183 0.60113972
		 -0.76135975 0.24282183 0.60113972 -0.76135975 0.24282183 0.60113972 -0.76135975 0.82243854
		 0.56885391 0 0.82243854 0.56885391 0 0.82243854 0.56885391 0 0.82243854 0.56885391
		 0 -0.12387165 0.59188598 -0.79644632 -0.12387165 0.59188598 -0.79644632 -0.12387165
		 0.59188598 -0.79644632 -0.12387165 0.59188598 -0.79644632 0 0.6146881 0.78877026
		 0 0.6146881 0.78877026 0 0.6146881 0.78877026 0 0.6146881 0.78877026 -0.1524242 0.12795652
		 -0.97999692 -0.1524242 0.12795652 -0.97999692 -0.1524242 0.12795652 -0.97999692 -0.1524242
		 0.12795652 -0.97999692 -0.4939284 0.11120056 0.86236256 -0.4939284 0.11120056 0.86236256
		 -0.4939284 0.11120056 0.86236256 -0.4939284 0.11120056 0.86236256 -0.95788848 0.057875689
		 0.28124711 -0.95788848 0.057875689 0.28124711 -0.95788848 0.057875689 0.28124711
		 -0.95788848 0.057875689 0.28124711 1.083613e-06 1 -7.4643327e-07 1.083613e-06 1 -7.4643327e-07
		 1.083613e-06 1 -7.4643327e-07 1.083613e-06 1 -7.4643327e-07 1.3329269e-06 1 6.397924e-07
		 1.3329269e-06 1 6.397924e-07 1.3329269e-06 1 6.397924e-07 1.3329269e-06 1 6.397924e-07
		 1.8278946e-06 1 0 1.8278946e-06 1 0 1.8278946e-06 1 0 1.8278946e-06 1 0 -1.8278944e-06
		 1 0 -1.8278944e-06 1 0;
	setAttr ".n[2158:2239]" -type "float3"  -1.8278944e-06 1 0 -1.8278944e-06 1 0
		 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 0 0.99999994 0 -1.0377067e-06 0.99999994
		 0 -1.0377067e-06 0.99999994 0 -1.0377067e-06 0.99999994 0 -1.0377067e-06 0.99999994
		 0 0 1 0 0 1 0 0 1 0 0 1 0 -7.5894485e-07 1 -6.9241975e-07 -7.5894485e-07 1 -6.9241975e-07
		 -7.5894485e-07 1 -6.9241975e-07 -7.5894485e-07 1 -6.9241975e-07 0 0.99999994 -1.278853e-07
		 0 0.99999994 -1.278853e-07 0 0.99999994 -1.278853e-07 0 0.99999994 -1.278853e-07
		 0.62631851 0.56864679 0.53325969 0.62631851 0.56864679 0.53325969 0.62631851 0.56864679
		 0.53325969 0.62631851 0.56864679 0.53325969 -0.83840251 0.54505175 0 -0.83840251
		 0.54505175 0 -0.83840251 0.54505175 0 -0.83840251 0.54505175 0 0.68385923 0.55583692
		 0.47263309 0.68385923 0.55583692 0.47263309 0.68385923 0.55583692 0.47263309 0.68385923
		 0.55583692 0.47263309 0 -0.70263755 -0.71154803 0 -0.70263755 -0.71154803 0 -0.70263755
		 -0.71154803 0 -0.70263755 -0.71154803 -0.27562648 -0.72656298 -0.62939364 -0.27562648
		 -0.72656298 -0.62939364 -0.27562648 -0.72656298 -0.62939364 -0.27562648 -0.72656298
		 -0.62939364 0 -0.83210677 0.55461544 0 -0.83210677 0.55461544 0 -0.83210677 0.55461544
		 0 -0.83210677 0.55461544 0.17141366 -0.84258008 0.51056439 0.17141366 -0.84258008
		 0.51056439 0.17141366 -0.84258008 0.51056439 0.17141366 -0.84258008 0.51056439 0.57288325
		 -0.819637 0 0.57288325 -0.819637 0 0.57288325 -0.819637 0 0.57288325 -0.819637 0
		 0.51991296 -0.78035027 -0.34748232 0.51991296 -0.78035027 -0.34748232 0.51991296
		 -0.78035027 -0.34748232 0.51991296 -0.78035027 -0.34748232 -0.99869645 -0.05104294
		 8.3569681e-08 -0.99869645 -0.05104294 8.3569681e-08 -0.99869645 -0.05104294 8.3569681e-08
		 -0.99869645 -0.05104294 8.3569681e-08 -0.88528883 -0.015012655 -0.4647992 -0.88528883
		 -0.015012655 -0.4647992 -0.88528883 -0.015012655 -0.4647992 -0.88528883 -0.015012655
		 -0.4647992 -0.90777212 -0.13378234 0.39755762 -0.90777212 -0.13378234 0.39755762
		 -0.90777212 -0.13378234 0.39755762 -0.90777212 -0.13378234 0.39755762 -0.15486918
		 -0.83296841 0.53120542 -0.15486918 -0.83296841 0.53120542 -0.15486918 -0.83296841
		 0.53120542 -0.15486918 -0.83296841 0.53120542 0.29678729 -0.7520389 -0.5885191 0.29678729
		 -0.7520389 -0.5885191 0.29678729 -0.7520389 -0.5885191 0.29678729 -0.7520389 -0.5885191
		 0.45716423 -0.83524841 0.30555043 0.45716423 -0.83524841 0.30555043 0.45716423 -0.83524841
		 0.30555043 0.45716423 -0.83524841 0.30555043;
	setAttr -s 560 -ch 2240 ".fc";
	setAttr ".fc[0:499]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 68 70 192 158
		f 4 4 5 6 7
		mu 0 4 412 390 391 413
		f 4 8 9 10 11
		mu 0 4 612 614 682 676
		f 4 12 13 14 15
		mu 0 4 600 602 634 628
		f 4 16 17 18 19
		mu 0 4 606 608 662 652
		f 4 20 21 22 23
		mu 0 4 646 652 653 647
		f 4 24 25 26 27
		mu 0 4 526 528 548 546
		f 4 -27 28 29 30
		mu 0 4 546 548 568 566
		f 4 31 32 -17 33
		mu 0 4 586 588 608 606
		f 4 34 35 36 37
		mu 0 4 520 522 542 540
		f 4 -37 38 39 40
		mu 0 4 540 542 562 560
		f 4 41 42 -13 43
		mu 0 4 580 582 602 600
		f 4 44 45 46 47
		mu 0 4 532 534 554 552
		f 4 -47 48 49 50
		mu 0 4 552 554 574 572
		f 4 51 52 -9 53
		mu 0 4 592 594 614 612
		f 4 54 -10 55 56
		mu 0 4 12 18 30 28
		f 4 57 58 59 60
		mu 0 4 38 36 46 44
		f 4 -60 -49 61 62
		mu 0 4 44 46 54 48
		f 4 63 -2 64 65
		mu 0 4 460 470 514 512
		f 4 -65 66 -45 67
		mu 0 4 512 514 534 532
		f 4 68 69 70 71
		mu 0 4 364 390 502 500
		f 4 -71 72 -35 73
		mu 0 4 500 502 522 520
		f 4 74 75 76 77
		mu 0 4 424 438 508 506
		f 4 -77 78 -25 79
		mu 0 4 506 508 528 526
		f 4 -62 -46 80 81
		mu 0 4 48 54 60 58
		f 4 -81 -67 -1 82
		mu 0 4 58 60 70 68
		f 4 83 84 85 86
		mu 0 4 243 399 392 414
		f 4 87 88 89 90
		mu 0 4 342 304 305 343
		f 4 91 92 93 94
		mu 0 4 450 438 439 451
		f 4 -3 95 96 97
		mu 0 4 158 192 193 159
		f 4 98 99 100 101
		mu 0 4 228 230 440 426
		f 4 102 103 104 105
		mu 0 4 351 313 306 344
		f 4 106 107 -100 108
		mu 0 4 288 443 440 452
		f 4 109 110 111 112
		mu 0 4 167 201 194 160
		f 4 -97 113 114 115
		mu 0 4 159 193 197 161
		f 4 116 117 118 119
		mu 0 4 180 78 196 162
		f 4 120 121 122 123
		mu 0 4 165 199 195 163
		f 4 -94 124 125 126
		mu 0 4 451 439 441 453
		f 4 127 128 129 130
		mu 0 4 252 436 283 248
		f 4 131 132 133 134
		mu 0 4 232 236 282 249
		f 4 -90 135 136 137
		mu 0 4 343 305 307 345
		f 4 138 139 140 141
		mu 0 4 352 315 308 346
		f 4 142 143 144 145
		mu 0 4 349 311 309 347
		f 4 -7 146 147 148
		mu 0 4 413 391 393 415
		f 4 149 150 151 152
		mu 0 4 422 400 394 266
		f 4 153 154 155 156
		mu 0 4 242 397 395 267
		f 4 -148 157 158 159
		mu 0 4 415 393 396 416
		f 4 -137 160 161 162
		mu 0 4 345 307 310 348
		f 4 -126 163 164 165
		mu 0 4 453 441 442 454
		f 4 -115 166 167 168
		mu 0 4 161 197 198 164
		f 4 -152 169 -154 170
		mu 0 4 266 394 397 242
		f 4 -141 171 -143 172
		mu 0 4 346 308 311 349
		f 4 -130 173 -132 174
		mu 0 4 248 283 236 232
		f 4 -119 175 -121 176
		mu 0 4 162 196 199 165
		f 4 -123 177 178 179
		mu 0 4 163 195 200 166
		f 4 -134 180 181 182
		mu 0 4 249 282 284 250
		f 4 -145 183 184 185
		mu 0 4 347 309 312 350
		f 4 -156 186 187 188
		mu 0 4 267 395 398 268
		f 4 189 190 191 -181
		mu 0 4 282 275 276 284
		f 4 192 -189 193 -191
		mu 0 4 275 267 268 276
		f 4 194 195 196 -184
		mu 0 4 95 143 146 98
		f 4 197 -180 198 -196
		mu 0 4 143 163 166 146
		f 4 199 200 201 -172
		mu 0 4 94 142 145 97
		f 4 202 -177 203 -201
		mu 0 4 142 162 165 145
		f 4 204 205 206 -174
		mu 0 4 283 274 246 236
		f 4 207 -171 208 -206
		mu 0 4 274 266 242 246
		f 4 209 210 211 -161
		mu 0 4 93 141 144 96
		f 4 212 -169 213 -211
		mu 0 4 141 161 164 144
		f 4 214 215 216 -164
		mu 0 4 441 427 428 442
		f 4 217 -160 218 -216
		mu 0 4 427 415 416 428
		f 4 -207 219 -190 -133
		mu 0 4 236 246 275 282
		f 4 -209 -157 -193 -220
		mu 0 4 246 242 267 275
		f 4 220 221 -205 -129
		mu 0 4 436 429 274 283
		f 4 222 -153 -208 -222
		mu 0 4 429 422 266 274
		f 4 223 224 -215 -125
		mu 0 4 439 425 427 441
		f 4 225 -149 -218 -225
		mu 0 4 425 413 415 427
		f 4 -202 226 -195 -144
		mu 0 4 97 145 143 95
		f 4 -204 -124 -198 -227
		mu 0 4 145 165 163 143
		f 4 227 228 -200 -140
		mu 0 4 100 126 142 94
		f 4 229 -120 -203 -229
		mu 0 4 126 180 162 142
		f 4 230 231 -210 -136
		mu 0 4 91 139 141 93
		f 4 232 -116 -213 -232
		mu 0 4 139 159 161 141
		f 4 233 234 235 -104
		mu 0 4 99 72 140 92
		f 4 236 237 -86 238
		mu 0 4 224 226 414 392
		f 4 239 240 -231 -89
		mu 0 4 90 138 139 91
		f 4 241 -98 -233 -241
		mu 0 4 138 158 159 139
		f 4 242 243 -101 -108
		mu 0 4 443 277 426 440
		f 4 244 245 246 247
		mu 0 4 62 56 66 64
		f 4 248 -83 249 -246
		mu 0 4 56 58 68 66
		f 4 250 251 -245 252
		mu 0 4 50 52 56 62
		f 4 253 -82 -249 -252
		mu 0 4 52 48 58 56
		f 4 254 255 256 -73
		mu 0 4 502 504 524 522
		f 4 257 -80 258 -256
		mu 0 4 504 506 526 524
		f 4 -5 259 -255 -70
		mu 0 4 390 412 504 502
		f 4 260 -78 -258 -260
		mu 0 4 412 424 506 504
		f 4 261 262 -251 263
		mu 0 4 40 42 52 50
		f 4 264 -63 -254 -263
		mu 0 4 42 44 48 52
		f 4 265 266 -262 267
		mu 0 4 32 34 42 40
		f 4 268 -61 -265 -267
		mu 0 4 34 38 44 42
		f 4 269 270 271 272
		mu 0 4 0 6 26 24
		f 4 273 -57 274 -271
		mu 0 4 6 12 28 26
		f 4 275 276 277 -43
		mu 0 4 582 584 604 602
		f 4 278 -34 279 -277
		mu 0 4 584 586 606 604
		f 4 280 281 282 -39
		mu 0 4 542 544 564 562
		f 4 283 -31 284 -282
		mu 0 4 544 546 566 564
		f 4 -257 285 -281 -36
		mu 0 4 522 524 544 542
		f 4 -259 -28 -284 -286
		mu 0 4 524 526 546 544
		f 4 286 287 288 289
		mu 0 4 622 628 629 623
		f 4 290 291 292 293
		mu 0 4 662 670 671 663
		f 4 -278 294 295 -14
		mu 0 4 602 604 646 634
		f 4 -280 -20 -21 -295
		mu 0 4 604 606 652 646
		f 4 -75 296 -224 -93
		mu 0 4 438 424 425 439
		f 4 -261 -8 -226 -297
		mu 0 4 424 412 413 425
		f 4 -247 297 -240 298
		mu 0 4 64 66 138 90
		f 4 -250 -4 -242 -298
		mu 0 4 66 68 158 138
		f 4 299 300 301 302
		mu 0 4 272 240 243 277
		f 4 303 -303 -243 304
		mu 0 4 494 272 277 443
		f 4 305 306 307 308
		mu 0 4 73 214 167 72
		f 4 309 -309 -234 310
		mu 0 4 124 73 72 99
		f 4 311 312 -110 -307
		mu 0 4 214 76 201 167
		f 4 313 -305 -107 314
		mu 0 4 251 494 443 288
		f 4 315 -311 -103 316
		mu 0 4 340 314 313 351
		f 4 317 318 -84 -301
		mu 0 4 240 388 399 243
		f 4 319 320 321 -319
		mu 0 4 388 374 373 399
		f 4 322 -317 323 -321
		mu 0 4 374 340 351 373
		f 4 324 325 326 -313
		mu 0 4 298 261 290 297
		f 4 327 -315 328 -326
		mu 0 4 261 251 288 290
		f 4 -19 -294 329 -22
		mu 0 4 652 662 663 653
		f 4 -15 330 331 -288
		mu 0 4 628 634 635 629
		f 4 -270 332 333 334
		mu 0 4 6 0 1 7
		f 4 -274 -335 335 336
		mu 0 4 12 6 7 13
		f 4 -236 337 338 -105
		mu 0 4 92 140 218 216
		f 4 -339 339 -237 340
		mu 0 4 216 218 226 224
		f 4 341 342 343 -187
		mu 0 4 395 369 372 398
		f 4 344 -186 345 -343
		mu 0 4 369 347 350 372
		f 4 346 347 348 -178
		mu 0 4 294 259 260 296
		f 4 349 -183 350 -348
		mu 0 4 259 249 250 260
		f 4 351 352 353 -176
		mu 0 4 295 258 234 238
		f 4 354 -175 355 -353
		mu 0 4 258 248 232 234
		f 4 356 357 358 -170
		mu 0 4 394 368 371 397
		f 4 359 -173 360 -358
		mu 0 4 368 346 349 371
		f 4 361 362 363 -167
		mu 0 4 473 463 464 474
		f 4 364 -166 365 -363
		mu 0 4 463 453 454 464
		f 4 366 367 368 -158
		mu 0 4 393 367 370 396
		f 4 369 -163 370 -368
		mu 0 4 367 345 348 370
		f 4 -359 371 -342 -155
		mu 0 4 397 371 369 395
		f 4 -361 -146 -345 -372
		mu 0 4 371 349 347 369
		f 4 372 373 -357 -151
		mu 0 4 400 386 368 394
		f 4 374 -142 -360 -374
		mu 0 4 386 352 346 368
		f 4 375 376 -367 -147
		mu 0 4 391 365 367 393
		f 4 377 -138 -370 -377
		mu 0 4 365 343 345 367
		f 4 -354 378 -347 -122
		mu 0 4 238 234 259 294
		f 4 -356 -135 -350 -379
		mu 0 4 234 232 249 259
		f 4 379 380 -352 -118
		mu 0 4 475 291 258 295
		f 4 381 -131 -355 -381
		mu 0 4 291 252 248 258
		f 4 382 383 -362 -114
		mu 0 4 471 461 463 473
		f 4 384 -127 -365 -384
		mu 0 4 461 451 453 463
		f 4 -327 385 386 -111
		mu 0 4 297 290 462 472
		f 4 -329 -109 387 -386
		mu 0 4 290 288 452 462
		f 4 -322 388 -239 -85
		mu 0 4 399 373 366 392
		f 4 -324 -106 -341 -389
		mu 0 4 373 351 344 366
		f 4 -112 -387 389 390
		mu 0 4 160 194 222 220
		f 4 -390 -388 -99 391
		mu 0 4 220 222 230 228
		f 4 -64 392 -383 -96
		mu 0 4 470 460 461 471
		f 4 393 -95 -385 -393
		mu 0 4 460 450 451 461
		f 4 -69 394 -376 -6
		mu 0 4 390 364 365 391
		f 4 395 -91 -378 -395
		mu 0 4 364 342 343 365
		f 4 396 397 398 -248
		mu 0 4 496 498 518 516
		f 4 399 -74 400 -398
		mu 0 4 498 500 520 518
		f 4 -88 401 -397 -299
		mu 0 4 304 342 498 496
		f 4 -396 -72 -400 -402
		mu 0 4 342 364 500 498
		f 4 402 403 404 -79
		mu 0 4 508 510 530 528
		f 4 405 -68 406 -404
		mu 0 4 510 512 532 530
		f 4 -92 407 -403 -76
		mu 0 4 438 450 510 508
		f 4 -394 -66 -406 -408
		mu 0 4 450 460 512 510
		f 4 408 409 410 -33
		mu 0 4 588 590 610 608
		f 4 411 -54 412 -410
		mu 0 4 590 592 612 610
		f 4 413 414 415 -29
		mu 0 4 548 550 570 568
		f 4 416 -51 417 -415
		mu 0 4 550 552 572 570
		f 4 -405 418 -414 -26
		mu 0 4 528 530 550 548
		f 4 -407 -48 -417 -419
		mu 0 4 530 532 552 550
		f 4 419 420 421 422
		mu 0 4 576 578 598 596
		f 4 423 -44 424 -421
		mu 0 4 578 580 600 598
		f 4 425 426 427 -264
		mu 0 4 536 538 558 556
		f 4 428 -41 429 -427
		mu 0 4 538 540 560 558
		f 4 -399 430 -426 -253
		mu 0 4 516 518 538 536
		f 4 -401 -38 -429 -431
		mu 0 4 518 520 540 538
		f 4 431 432 433 -292
		mu 0 4 670 676 677 671
		f 4 -296 -24 434 -331
		mu 0 4 634 646 647 635
		f 4 -422 435 436 -273
		mu 0 4 596 598 622 616
		f 4 -425 -16 -287 -436
		mu 0 4 598 600 628 622
		f 4 -411 437 -291 -18
		mu 0 4 608 610 670 662
		f 4 -413 -12 -432 -438
		mu 0 4 610 612 676 670
		f 4 -308 438 -302 439
		mu 0 4 72 167 277 243
		f 4 -244 -439 -113 440
		mu 0 4 426 277 167 160
		f 4 -87 441 -235 -440
		mu 0 4 243 414 140 72
		f 4 -351 442 -328 443
		mu 0 4 260 250 251 261
		f 4 -349 -444 -325 444
		mu 0 4 296 260 261 298
		f 4 -346 445 -323 446
		mu 0 4 372 350 340 374
		f 4 -344 -447 -320 447
		mu 0 4 398 372 374 388
		f 4 -188 -448 -318 448
		mu 0 4 268 398 388 240
		f 4 -185 449 -316 -446
		mu 0 4 350 312 314 340
		f 4 -182 450 -314 -443
		mu 0 4 250 284 494 251
		f 4 -179 -445 -312 451
		mu 0 4 166 200 76 214
		f 4 -197 452 -310 -450
		mu 0 4 98 146 73 124
		f 4 -199 -452 -306 -453
		mu 0 4 146 166 214 73
		f 4 -192 453 -304 -451
		mu 0 4 284 276 272 494
		f 4 -194 -449 -300 -454
		mu 0 4 276 268 240 272
		f 4 -366 454 -382 455
		mu 0 4 464 454 252 291
		f 4 -364 -456 -380 456
		mu 0 4 474 464 291 475
		f 4 -371 457 -375 458
		mu 0 4 370 348 352 386
		f 4 -369 -459 -373 459
		mu 0 4 396 370 386 400
		f 4 -230 460 461 462
		mu 0 4 180 126 127 181
		f 4 -214 463 464 465
		mu 0 4 144 164 168 147
		f 4 -219 466 -223 467
		mu 0 4 428 416 422 429
		f 4 -217 -468 -221 468
		mu 0 4 442 428 429 436
		f 4 -159 -460 -150 -467
		mu 0 4 416 396 400 422
		f 4 -162 469 -139 -458
		mu 0 4 348 310 315 352
		f 4 -165 -469 -128 -455
		mu 0 4 454 442 436 252
		f 4 -212 -466 470 471
		mu 0 4 96 144 147 101
		f 4 -465 472 -462 473
		mu 0 4 147 168 181 127
		f 4 -471 -474 474 475
		mu 0 4 101 147 127 102
		f 4 476 477 478 -473
		mu 0 4 168 202 79 181
		f 4 -457 479 -478 480
		mu 0 4 474 475 476 477
		f 4 -461 481 482 483
		mu 0 4 127 126 128 129
		f 4 -470 -472 -476 484
		mu 0 4 315 310 316 317
		f 4 -168 -481 -477 -464
		mu 0 4 164 198 202 168
		f 4 -480 485 486 487
		mu 0 4 476 475 478 479
		f 4 -483 488 489 490
		mu 0 4 129 128 130 131
		f 4 -487 491 492 493
		mu 0 4 479 478 480 481
		f 4 -479 -488 494 495
		mu 0 4 181 79 81 183
		f 4 -463 -496 496 497
		mu 0 4 180 181 183 182
		f 4 -117 -498 498 -486
		mu 0 4 78 180 182 80
		f 4 -475 -484 499 500
		mu 0 4 102 127 129 104
		f 4 -485 -501 501 502
		mu 0 4 315 317 319 318
		f 4 -228 -503 503 -482
		mu 0 4 126 100 103 128
		f 4 504 505 506 -493
		mu 0 4 82 184 185 83
		f 4 507 508 509 -490
		mu 0 4 130 105 106 131
		f 4 -500 -491 -510 510
		mu 0 4 104 129 131 106
		f 4 -504 511 -508 -489
		mu 0 4 128 103 105 130
		f 4 -502 -511 -509 -512
		mu 0 4 318 319 321 320
		f 4 -497 512 -506 513
		mu 0 4 182 183 185 184
		f 4 -495 -494 -507 -513
		mu 0 4 183 81 83 185
		f 4 -499 -514 -505 -492
		mu 0 4 80 182 184 82
		f 4 514 515 516 517
		mu 0 4 677 683 684 678
		f 4 -334 518 519 520
		mu 0 4 7 1 2 8
		f 4 -289 521 522 523
		mu 0 4 623 629 630 624
		f 4 524 525 526 -516
		mu 0 4 19 13 14 20
		f 4 -435 527 528 529
		mu 0 4 635 647 648 636
		f 4 -332 -530 530 -522
		mu 0 4 629 635 636 630
		f 4 -23 531 532 -528
		mu 0 4 647 653 654 648
		f 4 533 -524 534 -519
		mu 0 4 617 623 624 618
		f 4 -434 -518 535 536
		mu 0 4 671 677 678 672
		f 4 -437 -290 -534 -333
		mu 0 4 616 622 623 617
		f 4 -11 537 -515 -433
		mu 0 4 676 682 683 677
		f 4 -55 -337 -525 -538
		mu 0 4 18 12 13 19
		f 4 538 539 540 541
		mu 0 4 658 654 664 672
		f 4 -531 -529 542 543
		mu 0 4 630 636 648 644
		f 4 -543 -533 -539 544
		mu 0 4 644 648 654 658
		f 4 545 546 547 548
		mu 0 4 637 642 660 665
		f 4 549 -545 550 -547
		mu 0 4 642 644 658 660
		f 4 -535 551 -546 -520
		mu 0 4 618 624 642 637
		f 4 -523 -544 -550 -552
		mu 0 4 624 630 644 642
		f 4 -548 552 -517 -527
		mu 0 4 665 660 678 684
		f 4 -551 -542 -536 -553
		mu 0 4 660 658 672 678
		f 4 -293 -537 -541 553
		mu 0 4 663 671 672 664
		f 4 -336 -521 -549 -526
		mu 0 4 13 7 8 14
		f 4 -330 -554 -540 -532
		mu 0 4 653 663 664 654
		f 4 -430 554 -424 555
		mu 0 4 558 560 580 578
		f 4 -428 -556 -420 -268
		mu 0 4 556 558 578 576
		f 4 -418 556 -412 557
		mu 0 4 570 572 592 590
		f 4 -416 -558 -409 558
		mu 0 4 568 570 590 588
		f 4 -285 559 -279 560
		mu 0 4 564 566 586 584
		f 4 -283 -561 -276 561
		mu 0 4 562 564 584 582
		f 4 -275 562 -269 563
		mu 0 4 26 28 38 34
		f 4 -272 -564 -266 -423
		mu 0 4 24 26 34 32
		f 4 -56 -53 -58 -563
		mu 0 4 28 30 36 38
		f 4 -50 -59 -52 -557
		mu 0 4 572 574 594 592
		f 4 -40 -562 -42 -555
		mu 0 4 560 562 582 580
		f 4 -30 -559 -32 -560
		mu 0 4 566 568 588 586
		f 4 564 565 566 567
		mu 0 4 69 169 203 71
		f 4 568 569 570 571
		mu 0 4 417 418 402 401
		f 4 572 573 574 575
		mu 0 4 613 679 685 615
		f 4 576 577 578 579
		mu 0 4 601 631 638 603
		f 4 580 581 582 583
		mu 0 4 607 655 666 609
		f 4 584 585 586 587
		mu 0 4 649 650 656 655
		f 4 588 589 590 591
		mu 0 4 527 547 549 529
		f 4 592 593 594 -590
		mu 0 4 547 567 569 549
		f 4 595 -584 596 597
		mu 0 4 587 607 609 589
		f 4 598 599 600 601
		mu 0 4 521 541 543 523
		f 4 602 603 604 -600
		mu 0 4 541 561 563 543
		f 4 605 -580 606 607
		mu 0 4 581 601 603 583
		f 4 608 609 610 611
		mu 0 4 533 553 555 535
		f 4 612 613 614 -610
		mu 0 4 553 573 575 555
		f 4 615 -576 616 617
		mu 0 4 593 613 615 595
		f 4 618 619 -575 620
		mu 0 4 15 29 31 21
		f 4 621 622 623 624
		mu 0 4 39 45 47 37
		f 4 625 626 -615 -623
		mu 0 4 45 49 55 47
		f 4 627 628 -567 629
		mu 0 4 465 513 515 482
		f 4 630 -612 631 -629
		mu 0 4 513 533 535 515
		f 4 632 633 634 635
		mu 0 4 375 501 503 401
		f 4 636 -602 637 -634
		mu 0 4 501 521 523 503
		f 4 638 639 640 641
		mu 0 4 430 507 509 444
		f 4 642 -592 643 -640
		mu 0 4 507 527 529 509
		f 4 644 645 -611 -627
		mu 0 4 49 59 61 55
		f 4 646 -568 -632 -646
		mu 0 4 59 69 71 61
		f 4 647 648 649 650
		mu 0 4 245 419 403 410
		f 4 651 652 653 654
		mu 0 4 353 354 323 322
		f 4 655 656 657 658
		mu 0 4 455 456 445 444
		f 4 659 660 661 -566
		mu 0 4 169 170 204 203
		f 4 662 663 664 665
		mu 0 4 229 432 446 231
		f 4 666 667 668 669
		mu 0 4 362 355 324 331
		f 4 670 -665 671 672
		mu 0 4 289 457 446 449
		f 4 673 674 675 676
		mu 0 4 178 171 205 212
		f 4 677 678 679 -661
		mu 0 4 170 172 208 204
		f 4 680 681 682 683
		mu 0 4 186 173 207 84
		f 4 684 685 686 687
		mu 0 4 176 174 206 210
		f 4 688 689 690 -657
		mu 0 4 456 458 447 445
		f 4 691 692 693 694
		mu 0 4 257 253 286 437
		f 4 695 696 697 698
		mu 0 4 233 254 285 237
		f 4 699 700 701 -653
		mu 0 4 354 356 325 323
		f 4 702 703 704 705
		mu 0 4 363 357 326 333
		f 4 706 707 708 709
		mu 0 4 360 358 327 329
		f 4 710 711 712 -570
		mu 0 4 418 420 404 402
		f 4 713 714 715 716
		mu 0 4 423 269 405 411
		f 4 717 718 719 720
		mu 0 4 244 270 406 408
		f 4 721 722 723 -712
		mu 0 4 420 421 407 404
		f 4 724 725 726 -701
		mu 0 4 356 359 328 325
		f 4 727 728 729 -690
		mu 0 4 458 459 448 447
		f 4 730 731 732 -679
		mu 0 4 172 175 209 208
		f 4 733 -721 734 -715
		mu 0 4 269 244 408 405
		f 4 735 -710 736 -704
		mu 0 4 357 360 329 326
		f 4 737 -699 738 -693
		mu 0 4 253 233 237 286
		f 4 739 -688 740 -682
		mu 0 4 173 176 210 207
		f 4 741 742 743 -686
		mu 0 4 174 177 211 206
		f 4 744 745 746 -697
		mu 0 4 254 255 287 285
		f 4 747 748 749 -708
		mu 0 4 358 361 330 327
		f 4 750 751 752 -719
		mu 0 4 270 271 409 406
		f 4 -747 753 754 755
		mu 0 4 285 287 280 279
		f 4 -755 756 -751 757
		mu 0 4 279 280 271 270
		f 4 -750 758 759 760
		mu 0 4 112 115 156 153
		f 4 -760 761 -742 762
		mu 0 4 153 156 177 174
		f 4 -737 763 764 765
		mu 0 4 111 114 155 152
		f 4 -765 766 -740 767
		mu 0 4 152 155 176 173
		f 4 -739 768 769 770
		mu 0 4 286 237 247 278
		f 4 -770 771 -734 772
		mu 0 4 278 247 244 269
		f 4 -727 773 774 775
		mu 0 4 110 113 154 151
		f 4 -775 776 -731 777
		mu 0 4 151 154 175 172
		f 4 -730 778 779 780
		mu 0 4 447 448 434 433
		f 4 -780 781 -722 782
		mu 0 4 433 434 421 420
		f 4 -698 -756 783 -769
		mu 0 4 237 285 279 247
		f 4 -784 -758 -718 -772
		mu 0 4 247 279 270 244
		f 4 -694 -771 784 785
		mu 0 4 437 286 278 435
		f 4 -785 -773 -714 786
		mu 0 4 435 278 269 423
		f 4 -691 -781 787 788
		mu 0 4 445 447 433 431
		f 4 -788 -783 -711 789
		mu 0 4 431 433 420 418
		f 4 -709 -761 790 -764
		mu 0 4 114 112 153 155
		f 4 -791 -763 -685 -767
		mu 0 4 155 153 174 176
		f 4 -705 -766 791 792
		mu 0 4 117 111 152 132
		f 4 -792 -768 -681 793
		mu 0 4 132 152 173 186
		f 4 -702 -776 794 795
		mu 0 4 108 110 151 149
		f 4 -795 -778 -678 796
		mu 0 4 149 151 172 170
		f 4 -669 797 798 799
		mu 0 4 116 109 150 74
		f 4 800 -649 801 802
		mu 0 4 225 403 419 227
		f 4 -654 -796 803 804
		mu 0 4 107 108 149 148
		f 4 -804 -797 -660 805
		mu 0 4 148 149 170 169
		f 4 -672 -664 806 807
		mu 0 4 449 446 432 281
		f 4 808 809 810 811
		mu 0 4 63 65 67 57
		f 4 -811 812 -647 813
		mu 0 4 57 67 69 59
		f 4 814 -812 815 816
		mu 0 4 51 63 57 53
		f 4 -816 -814 -645 817
		mu 0 4 53 57 59 49
		f 4 -638 818 819 820
		mu 0 4 503 523 525 505
		f 4 -820 821 -643 822
		mu 0 4 505 525 527 507
		f 4 -635 -821 823 -572
		mu 0 4 401 503 505 417
		f 4 -824 -823 -639 824
		mu 0 4 417 505 507 430
		f 4 825 -817 826 827
		mu 0 4 41 51 53 43
		f 4 -827 -818 -626 828
		mu 0 4 43 53 49 45
		f 4 829 -828 830 831
		mu 0 4 33 41 43 35
		f 4 -831 -829 -622 832
		mu 0 4 35 43 45 39
		f 4 833 834 835 836
		mu 0 4 3 25 27 9
		f 4 -836 837 -619 838
		mu 0 4 9 27 29 15
		f 4 -607 839 840 841
		mu 0 4 583 603 605 585
		f 4 -841 842 -596 843
		mu 0 4 585 605 607 587
		f 4 -605 844 845 846
		mu 0 4 543 563 565 545
		f 4 -846 847 -593 848
		mu 0 4 545 565 567 547
		f 4 -601 -847 849 -819
		mu 0 4 523 543 545 525
		f 4 -850 -849 -589 -822
		mu 0 4 525 545 547 527
		f 4 850 851 852 853
		mu 0 4 625 626 632 631
		f 4 854 855 856 857
		mu 0 4 666 667 674 673
		f 4 -579 858 859 -840
		mu 0 4 603 638 649 605
		f 4 -860 -588 -581 -843
		mu 0 4 605 649 655 607
		f 4 -658 -789 860 -642
		mu 0 4 444 445 431 430
		f 4 -861 -790 -569 -825
		mu 0 4 430 431 418 417
		f 4 861 -805 862 -810
		mu 0 4 65 107 148 67
		f 4 -863 -806 -565 -813
		mu 0 4 67 148 169 69
		f 4 863 864 865 866
		mu 0 4 273 281 245 241
		f 4 867 -808 -864 868
		mu 0 4 495 449 281 273
		f 4 869 870 871 872
		mu 0 4 75 74 178 215
		f 4 873 -800 -870 874
		mu 0 4 125 116 74 75
		f 4 -872 -677 875 876
		mu 0 4 215 178 212 77
		f 4 877 -673 -868 878
		mu 0 4 256 289 449 495
		f 4 879 -670 -874 880
		mu 0 4 341 362 331 332
		f 4 -866 -651 881 882
		mu 0 4 241 245 410 389
		f 4 -882 883 884 885
		mu 0 4 389 410 384 385
		f 4 -885 886 -880 887
		mu 0 4 385 384 362 341
		f 4 -876 888 889 890
		mu 0 4 303 302 292 265
		f 4 -890 891 -878 892
		mu 0 4 265 292 289 256
		f 4 -587 893 -855 -582
		mu 0 4 655 656 667 666
		f 4 -853 894 895 -578
		mu 0 4 631 632 639 638
		f 4 896 897 898 -837
		mu 0 4 9 10 4 3
		f 4 899 900 -897 -839
		mu 0 4 15 16 10 9
		f 4 -668 901 902 -798
		mu 0 4 109 217 219 150
		f 4 903 -803 904 -902
		mu 0 4 217 225 227 219
		f 4 -753 905 906 907
		mu 0 4 406 409 383 380
		f 4 -907 908 -748 909
		mu 0 4 380 383 361 358
		f 4 -744 910 911 912
		mu 0 4 299 301 264 263
		f 4 -912 913 -745 914
		mu 0 4 263 264 255 254
		f 4 -741 915 916 917
		mu 0 4 300 239 235 262
		f 4 -917 918 -738 919
		mu 0 4 262 235 233 253
		f 4 -735 920 921 922
		mu 0 4 405 408 382 379
		f 4 -922 923 -736 924
		mu 0 4 379 382 360 357
		f 4 -733 925 926 927
		mu 0 4 485 486 469 468
		f 4 -927 928 -728 929
		mu 0 4 468 469 459 458
		f 4 -724 930 931 932
		mu 0 4 404 407 381 378
		f 4 -932 933 -725 934
		mu 0 4 378 381 359 356
		f 4 -720 -908 935 -921
		mu 0 4 408 406 380 382
		f 4 -936 -910 -707 -924
		mu 0 4 382 380 358 360
		f 4 -716 -923 936 937
		mu 0 4 411 405 379 387
		f 4 -937 -925 -703 938
		mu 0 4 387 379 357 363
		f 4 -713 -933 939 940
		mu 0 4 402 404 378 376
		f 4 -940 -935 -700 941
		mu 0 4 376 378 356 354
		f 4 -687 -913 942 -916
		mu 0 4 239 299 263 235
		f 4 -943 -915 -696 -919
		mu 0 4 235 263 254 233
		f 4 -683 -918 943 944
		mu 0 4 487 300 262 293
		f 4 -944 -920 -692 945
		mu 0 4 293 262 253 257
		f 4 -680 -928 946 947
		mu 0 4 483 485 468 466
		f 4 -947 -930 -689 948
		mu 0 4 466 468 458 456
		f 4 -676 949 950 -889
		mu 0 4 302 484 467 292
		f 4 -951 951 -671 -892
		mu 0 4 292 467 457 289
		f 4 -650 -801 952 -884
		mu 0 4 410 403 377 384
		f 4 -953 -904 -667 -887
		mu 0 4 384 377 355 362
		f 4 953 954 -950 -675
		mu 0 4 171 221 223 205
		f 4 955 -666 -952 -955
		mu 0 4 221 229 231 223
		f 4 -662 -948 956 -630
		mu 0 4 482 483 466 465
		f 4 -957 -949 -656 957
		mu 0 4 465 466 456 455
		f 4 -571 -941 958 -636
		mu 0 4 401 402 376 375
		f 4 -959 -942 -652 959
		mu 0 4 375 376 354 353
		f 4 -809 960 961 962
		mu 0 4 497 517 519 499
		f 4 -962 963 -637 964
		mu 0 4 499 519 521 501
		f 4 -862 -963 965 -655
		mu 0 4 322 497 499 353
		f 4 -966 -965 -633 -960
		mu 0 4 353 499 501 375
		f 4 -644 966 967 968
		mu 0 4 509 529 531 511
		f 4 -968 969 -631 970
		mu 0 4 511 531 533 513
		f 4 -641 -969 971 -659
		mu 0 4 444 509 511 455
		f 4 -972 -971 -628 -958
		mu 0 4 455 511 513 465
		f 4 -597 972 973 974
		mu 0 4 589 609 611 591
		f 4 -974 975 -616 976
		mu 0 4 591 611 613 593
		f 4 -595 977 978 979
		mu 0 4 549 569 571 551
		f 4 -979 980 -613 981
		mu 0 4 551 571 573 553
		f 4 -591 -980 982 -967
		mu 0 4 529 549 551 531
		f 4 -983 -982 -609 -970
		mu 0 4 531 551 553 533
		f 4 983 984 985 986
		mu 0 4 577 597 599 579
		f 4 -986 987 -606 988
		mu 0 4 579 599 601 581
		f 4 -826 989 990 991
		mu 0 4 537 557 559 539
		f 4 -991 992 -603 993
		mu 0 4 539 559 561 541
		f 4 -815 -992 994 -961
		mu 0 4 517 537 539 519
		f 4 -995 -994 -599 -964
		mu 0 4 519 539 541 521
		f 4 -857 995 996 997
		mu 0 4 673 674 680 679
		f 4 -896 998 -585 -859
		mu 0 4 638 639 650 649
		f 4 -834 999 1000 -985
		mu 0 4 597 619 625 599
		f 4 -1001 -854 -577 -988
		mu 0 4 599 625 631 601
		f 4 -583 -858 1001 -973
		mu 0 4 609 666 673 611
		f 4 -1002 -998 -573 -976
		mu 0 4 611 673 679 613
		f 4 1002 -865 1003 -871
		mu 0 4 74 245 281 178
		f 4 1004 -674 -1004 -807
		mu 0 4 432 171 178 281
		f 4 -1003 -799 1005 -648
		mu 0 4 245 74 150 419
		f 4 1006 -893 1007 -914
		mu 0 4 264 265 256 255
		f 4 1008 -891 -1007 -911
		mu 0 4 301 303 265 264
		f 4 1009 -888 1010 -909
		mu 0 4 383 385 341 361
		f 4 1011 -886 -1010 -906
		mu 0 4 409 389 385 383
		f 4 1012 -883 -1012 -752
		mu 0 4 271 241 389 409
		f 4 -1011 -881 1013 -749
		mu 0 4 361 341 332 330
		f 4 -1008 -879 1014 -746
		mu 0 4 255 256 495 287
		f 4 1015 -877 -1009 -743
		mu 0 4 177 215 77 211
		f 4 -1014 -875 1016 -759
		mu 0 4 115 125 75 156
		f 4 -1017 -873 -1016 -762
		mu 0 4 156 75 215 177
		f 4 -1015 -869 1017 -754
		mu 0 4 287 495 273 280
		f 4 -1018 -867 -1013 -757
		mu 0 4 280 273 241 271
		f 4 1018 -946 1019 -929
		mu 0 4 469 293 257 459
		f 4 1020 -945 -1019 -926
		mu 0 4 486 487 293 469
		f 4 1021 -939 1022 -934
		mu 0 4 381 387 363 359
		f 4 1023 -938 -1022 -931
		mu 0 4 407 411 387 381
		f 4 1024 1025 1026 -794
		mu 0 4 186 187 133 132
		f 4 1027 1028 1029 -777
		mu 0 4 154 157 179 175
		f 4 1030 -787 1031 -782
		mu 0 4 434 435 423 421
		f 4 1032 -786 -1031 -779
		mu 0 4 448 437 435 434
		f 4 -1032 -717 -1024 -723
		mu 0 4 421 423 411 407
		f 4 -1023 -706 1033 -726
		mu 0 4 359 363 333 328
		f 4 -1020 -695 -1033 -729
		mu 0 4 459 257 437 448
		f 4 1034 1035 -1028 -774
		mu 0 4 113 118 157 154;
	setAttr ".fc[500:559]"
		f 4 1036 -1026 1037 -1029
		mu 0 4 157 133 187 179
		f 4 1038 1039 -1037 -1036
		mu 0 4 118 119 133 157
		f 4 -1038 1040 1041 1042
		mu 0 4 179 187 85 213
		f 4 1043 -1042 1044 -1021
		mu 0 4 486 489 488 487
		f 4 1045 1046 1047 -1027
		mu 0 4 133 135 134 132
		f 4 1048 -1039 -1035 -1034
		mu 0 4 333 335 334 328
		f 4 -1030 -1043 -1044 -732
		mu 0 4 175 179 213 209
		f 4 1049 1050 1051 -1045
		mu 0 4 488 491 490 487
		f 4 1052 1053 1054 -1047
		mu 0 4 135 137 136 134
		f 4 1055 1056 1057 -1051
		mu 0 4 491 493 492 490
		f 4 1058 1059 -1050 -1041
		mu 0 4 187 189 87 85
		f 4 1060 1061 -1059 -1025
		mu 0 4 186 188 189 187
		f 4 -1052 1062 -1061 -684
		mu 0 4 84 86 188 186
		f 4 1063 1064 -1046 -1040
		mu 0 4 119 121 135 133
		f 4 1065 1066 -1064 -1049
		mu 0 4 333 336 337 335
		f 4 -1048 1067 -1066 -793
		mu 0 4 132 134 120 117
		f 4 -1057 1068 1069 1070
		mu 0 4 88 89 191 190
		f 4 -1054 1071 1072 1073
		mu 0 4 136 137 123 122
		f 4 1074 -1072 -1053 -1065
		mu 0 4 121 123 137 135
		f 4 -1055 -1074 1075 -1068
		mu 0 4 134 136 122 120
		f 4 -1076 -1073 -1075 -1067
		mu 0 4 336 338 339 337
		f 4 1076 -1070 1077 -1062
		mu 0 4 188 190 191 189
		f 4 -1078 -1069 -1056 -1060
		mu 0 4 189 191 89 87
		f 4 -1058 -1071 -1077 -1063
		mu 0 4 86 88 190 188
		f 4 1078 1079 1080 1081
		mu 0 4 680 681 687 686
		f 4 1082 1083 1084 -898
		mu 0 4 10 11 5 4
		f 4 1085 1086 1087 -852
		mu 0 4 626 627 633 632
		f 4 -1081 1088 1089 1090
		mu 0 4 22 23 17 16
		f 4 1091 1092 1093 -999
		mu 0 4 639 640 651 650
		f 4 -1088 1094 -1092 -895
		mu 0 4 632 633 640 639
		f 4 -1094 1095 1096 -586
		mu 0 4 650 651 657 656
		f 4 -1085 1097 -1086 1098
		mu 0 4 620 621 627 626
		f 4 1099 1100 -1079 -996
		mu 0 4 674 675 681 680
		f 4 -899 -1099 -851 -1000
		mu 0 4 619 620 626 625
		f 4 -997 -1082 1101 -574
		mu 0 4 679 680 686 685
		f 4 -1102 -1091 -900 -621
		mu 0 4 21 22 16 15
		f 4 1102 1103 1104 1105
		mu 0 4 659 675 668 657
		f 4 1106 1107 -1093 -1095
		mu 0 4 633 645 651 640
		f 4 1108 -1106 -1096 -1108
		mu 0 4 645 659 657 651
		f 4 1109 1110 1111 1112
		mu 0 4 641 669 661 643
		f 4 -1112 1113 -1109 1114
		mu 0 4 643 661 659 645
		f 4 -1084 -1113 1115 -1098
		mu 0 4 621 641 643 627
		f 4 -1116 -1115 -1107 -1087
		mu 0 4 627 643 645 633
		f 4 -1089 -1080 1116 -1111
		mu 0 4 669 687 681 661
		f 4 -1117 -1101 -1103 -1114
		mu 0 4 661 681 675 659
		f 4 1117 -1104 -1100 -856
		mu 0 4 667 668 675 674
		f 4 -1090 -1110 -1083 -901
		mu 0 4 16 17 11 10
		f 4 -1097 -1105 -1118 -894
		mu 0 4 656 657 668 667
		f 4 1118 -989 1119 -993
		mu 0 4 559 579 581 561
		f 4 -830 -987 -1119 -990
		mu 0 4 557 577 579 559
		f 4 1120 -977 1121 -981
		mu 0 4 571 591 593 573
		f 4 1122 -975 -1121 -978
		mu 0 4 569 589 591 571
		f 4 1123 -844 1124 -848
		mu 0 4 565 585 587 567
		f 4 1125 -842 -1124 -845
		mu 0 4 563 583 585 565
		f 4 1126 -833 1127 -838
		mu 0 4 27 35 39 29
		f 4 -984 -832 -1127 -835
		mu 0 4 25 33 35 27
		f 4 -1128 -625 -617 -620
		mu 0 4 29 39 37 31
		f 4 -1122 -618 -624 -614
		mu 0 4 573 593 595 575
		f 4 -1120 -608 -1126 -604
		mu 0 4 561 581 583 563
		f 4 -1125 -598 -1123 -594
		mu 0 4 567 587 589 569;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".pd[0]" -type "dataPolyComponent" Index_Data UV 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "topCon";
	rename -uid "6EC3CA4E-4771-2B10-6BC3-7FAB54999B6E";
	addAttr -ci true -sn "Mesh_Display_Type" -ln "Mesh_Display_Type" -min 0 -max 2 
		-en "Normal:Template:Reference" -at "enum";
	addAttr -ci true -sn "Skeleton_Display_Type" -ln "Skeleton_Display_Type" -min 0 
		-max 2 -en "Normal:Template:Reference" -at "enum";
	setAttr -k on ".Mesh_Display_Type";
	setAttr -k on ".Skeleton_Display_Type" 2;
createNode nurbsCurve -n "topConShape" -p "topCon";
	rename -uid "172FE3E2-48E3-222B-6D75-599F6592DE22";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		280.98969784004316 4.7982373409884731e-17 -280.98969784004322
		2.4332473058436662e-14 6.7857323231109122e-17 -397.37944157250689
		-280.98969784004316 4.7982373409884719e-17 -280.98969784004316
		-397.37944157250706 3.5177356190060272e-33 -2.0600220423069638e-14
		-280.98969784004316 -4.7982373409884725e-17 280.98969784004316
		-3.9805770442562499e-14 -6.7857323231109171e-17 397.37944157250718
		280.98969784004316 -4.7982373409884719e-17 280.98969784004316
		397.37944157250706 -9.2536792101100989e-33 5.4190494141372924e-14
		280.98969784004316 4.7982373409884731e-17 -280.98969784004322
		2.4332473058436662e-14 6.7857323231109122e-17 -397.37944157250689
		-280.98969784004316 4.7982373409884719e-17 -280.98969784004316
		;
createNode transform -n "left_foot_ik" -p "topCon";
	rename -uid "70FB686F-4CDC-5172-0AC8-83AAF211824C";
	setAttr ".rp" -type "double3" 12.019440131518991 54.678535453476485 34.810135827758998 ;
	setAttr ".sp" -type "double3" 12.019440131518991 54.678535453476485 34.810135827758998 ;
createNode nurbsCurve -n "left_foot_ikShape" -p "left_foot_ik";
	rename -uid "E606246E-46B8-73E6-3458-598FEF3ED326";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		35.318712590682843 54.678535453476485 11.510863368595142
		12.019440131518992 54.678535453476485 1.8599887225835587
		-11.279832327644861 54.678535453476485 11.51086336859515
		-20.930706973656463 54.678535453476485 34.810135827758998
		-11.279832327644861 54.678535453476485 58.10940828692285
		12.019440131518987 54.678535453476485 67.760282932934459
		35.318712590682843 54.678535453476485 58.109408286922843
		44.969587236694444 54.678535453476485 34.810135827759005
		35.318712590682843 54.678535453476485 11.510863368595142
		12.019440131518992 54.678535453476485 1.8599887225835587
		-11.279832327644861 54.678535453476485 11.51086336859515
		;
createNode ikHandle -n "left_leg_ik_handle" -p "left_foot_ik";
	rename -uid "9B093A93-41F4-BB40-0822-C2B982464C51";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 16.740499999999997 53.737799999999993 35.32739999999999 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "left_leg_ik_handle_poleVectorConstraint1" -p "left_leg_ik_handle";
	rename -uid "3AD05934-45B4-94C5-27AE-AAA727526C9C";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "left_leg_pvW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -286.47817757376515 -252.18308655853235 3.1446000000000041 ;
	setAttr -k on ".w0";
createNode transform -n "right_foot_ik" -p "topCon";
	rename -uid "497D85B1-4671-D23C-50BE-4F9250217F94";
	setAttr ".rp" -type "double3" 12.019423010170469 54.678500000000042 -34.810128126067958 ;
	setAttr ".sp" -type "double3" 12.019423010170469 54.678500000000042 -34.810128126067958 ;
createNode nurbsCurve -n "right_foot_ikShape" -p "right_foot_ik";
	rename -uid "C95847FD-4A22-055F-4A37-E690290303D2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		35.318695469334322 54.678500000000042 -58.109400585231811
		12.01942301017047 54.678500000000042 -67.760275231243398
		-11.279849448993383 54.678500000000042 -58.109400585231811
		-20.930724095004983 54.678500000000042 -34.810128126067958
		-11.279849448993383 54.678500000000042 -11.510855666904106
		12.019423010170465 54.678500000000042 -1.8599810208924978
		35.318695469334322 54.678500000000042 -11.51085566690411
		44.969570115345924 54.678500000000042 -34.810128126067951
		35.318695469334322 54.678500000000042 -58.109400585231811
		12.01942301017047 54.678500000000042 -67.760275231243398
		-11.279849448993383 54.678500000000042 -58.109400585231811
		;
createNode ikHandle -n "right_leg_ik_handle" -p "right_foot_ik";
	rename -uid "9E556BB3-49E2-BFA0-436D-84B2F096181F";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 16.740504354148378 53.737783374771794 -35.32742457945858 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "right_leg_ik_handle_poleVectorConstraint1" -p
		 "right_leg_ik_handle";
	rename -uid "A5CCA7FD-41E8-7B5E-64F9-C6961870CDF7";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "right_leg_pvW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -286.47816579125629 -252.18316258830265 -3.1450185330094627 ;
	setAttr -k on ".w0";
createNode transform -n "center_cog" -p "topCon";
	rename -uid "962202E2-46A4-23A7-6BBB-77B6AFD3025E";
	setAttr ".rp" -type "double3" 0 563.51405376879643 0 ;
	setAttr ".sp" -type "double3" 0 563.51405376879643 0 ;
createNode nurbsCurve -n "center_cogShape" -p "center_cog";
	rename -uid "53AD8CB5-43BF-BB71-BFCC-3AB86E8F0B90";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		128.20334706922409 563.51405376879643 -128.20334706922412
		1.1101846482425546e-14 563.51405376879643 -181.30691216692165
		-128.20334706922409 563.51405376879643 -128.20334706922409
		-181.30691216692173 563.51405376879643 -9.3989823431346823e-15
		-128.20334706922409 563.51405376879643 128.20334706922409
		-1.8161637393235606e-14 563.51405376879643 181.30691216692176
		128.20334706922409 563.51405376879643 128.20334706922409
		181.30691216692173 563.51405376879643 2.4724759596752486e-14
		128.20334706922409 563.51405376879643 -128.20334706922412
		1.1101846482425546e-14 563.51405376879643 -181.30691216692165
		-128.20334706922409 563.51405376879643 -128.20334706922409
		;
createNode transform -n "bottom_torso" -p "center_cog";
	rename -uid "1D48CF37-48AB-6AEB-646C-579CAB0CA4B5";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".rp" -type "double3" 1.6893831428279855 563.18231012059277 1.2539203008425267e-16 ;
	setAttr ".sp" -type "double3" 1.6893831428279855 563.18231012059277 1.2539203008425267e-16 ;
createNode nurbsCurve -n "bottom_torsoShape" -p "|topCon|center_cog|bottom_torso";
	rename -uid "4AE1F873-477E-7525-C2A4-6D99CEEAF139";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		89.174934488565 563.18231012059277 -87.485551345737022
		1.6893831428279931 563.18231012059277 -123.72325322482901
		-85.796168202909016 563.18231012059277 -87.485551345737008
		-122.03387008200109 563.18231012059277 -6.2884432649801802e-15
		-85.796168202909016 563.18231012059277 87.485551345737008
		1.6893831428279731 563.18231012059277 123.72325322482909
		89.174934488565 563.18231012059277 87.485551345737008
		125.41263636765707 563.18231012059277 1.6997488388416753e-14
		89.174934488565 563.18231012059277 -87.485551345737022
		1.6893831428279931 563.18231012059277 -123.72325322482901
		-85.796168202909016 563.18231012059277 -87.485551345737008
		;
createNode transform -n "mid_torso" -p "|topCon|center_cog|bottom_torso";
	rename -uid "58B3B7D1-4E44-8207-1B84-AA95D5E54313";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".rp" -type "double3" -9.0495163762149922 620.51155397043976 -6.8168235306516791e-15 ;
	setAttr ".sp" -type "double3" -9.0495163762149922 620.51155397043976 -6.8168235306516791e-15 ;
createNode nurbsCurve -n "mid_torsoShape" -p "|topCon|center_cog|bottom_torso|mid_torso";
	rename -uid "FB7FD08A-4D5D-09F8-B271-CFA8278D4944";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		65.351534952327981 620.51155397043976 -74.401051328542977
		-9.0495163762149868 620.51155397043976 -105.2189758436422
		-83.450567704757944 620.51155397043976 -74.401051328542962
		-114.26849221985725 620.51155397043976 -1.2271393818664868e-14
		-83.450567704757944 620.51155397043976 74.401051328542962
		-9.0495163762150028 620.51155397043976 105.21897584364228
		65.351534952327981 620.51155397043976 74.401051328542962
		96.169459467427259 620.51155397043976 7.5318510546584289e-15
		65.351534952327981 620.51155397043976 -74.401051328542977
		-9.0495163762149868 620.51155397043976 -105.2189758436422
		-83.450567704757944 620.51155397043976 -74.401051328542962
		;
createNode transform -n "upper_torso" -p "|topCon|center_cog|bottom_torso|mid_torso";
	rename -uid "C02764DE-4B79-5775-2458-1780D8B86653";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".rp" -type "double3" 4.9103169027355911 716.66218456694264 -1.7864623661089073e-14 ;
	setAttr ".sp" -type "double3" 4.9103169027355911 716.66218456694264 -1.7864623661089073e-14 ;
createNode nurbsCurve -n "upper_torsoShape" -p "upper_torso";
	rename -uid "EA264295-49D7-BFFE-289B-698B563B7F75";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		92.170603577035763 713.30471512885572 -87.26028667430019
		4.9103169027355964 652.20848377889058 -123.4046808713595
		-82.349969771564545 713.30471512885572 -87.260286674300175
		-118.49436396862399 743.8078288092612 -2.8603196266686658e-14
		-82.349969771564545 713.30471512885572 87.260286674300147
		4.9103169027355786 652.20848377889058 123.40468087135957
		92.170603577035763 713.30471512885572 87.260286674300147
		128.31499777409516 743.8078288092612 -5.3772230597110494e-15
		92.170603577035763 713.30471512885572 -87.26028667430019
		4.9103169027355964 652.20848377889058 -123.4046808713595
		-82.349969771564545 713.30471512885572 -87.260286674300175
		;
createNode transform -n "head_1" -p "upper_torso";
	rename -uid "CCC3B1D7-47CD-5DD2-256D-39862E21C4A7";
	setAttr ".rp" -type "double3" 1.5677368264283391 831.98119719953263 -2.9054752278737049e-14 ;
	setAttr ".sp" -type "double3" 1.5677368264283391 831.98119719953263 -2.9054752278737049e-14 ;
createNode nurbsCurve -n "head_1Shape" -p "head_1";
	rename -uid "09D459AB-4390-CD09-DEC5-4E956A5C67B9";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		100.25392276573567 831.98119719953263 -98.686185939307393
		1.5677368264283476 831.98119719953263 -139.56334257424143
		-97.118449112878992 831.98119719953263 -98.68618593930735
		-137.99560574781313 831.98119719953263 -3.6289740602825187e-14
		-97.118449112878992 831.98119719953263 98.686185939307279
		1.5677368264283249 831.98119719953263 139.56334257424149
		100.25392276573567 831.98119719953263 98.686185939307265
		141.13107940066982 831.98119719953263 -1.0022548528609514e-14
		100.25392276573567 831.98119719953263 -98.686185939307393
		1.5677368264283476 831.98119719953263 -139.56334257424143
		-97.118449112878992 831.98119719953263 -98.68618593930735
		;
createNode transform -n "head_2" -p "head_1";
	rename -uid "F2BC38B7-425F-5DAC-588C-B594AF3B8D22";
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
	setAttr ".rp" -type "double3" 2.6819301851972206 892.14763857305752 -3.6020643467558955e-14 ;
	setAttr ".sp" -type "double3" 2.6819301851972206 892.14763857305752 -3.6020643467558955e-14 ;
createNode nurbsCurve -n "head_2Shape" -p "head_2";
	rename -uid "72D8D657-4003-81A6-CFE0-3086416A6CD8";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		76.621497525116354 892.14763857305752 -73.939567339919194
		2.6819301851972268 892.14763857305752 -104.5663389281124
		-71.25763715472192 892.14763857305752 -73.939567339919165
		-101.88440874291518 892.14763857305752 -4.1441380942624034e-14
		-71.25763715472192 892.14763857305752 73.939567339919094
		2.6819301851972099 892.14763857305752 104.56633892811239
		76.621497525116354 892.14763857305752 73.93956733991908
		107.24826911330962 892.14763857305752 -2.176096874798803e-14
		76.621497525116354 892.14763857305752 -73.939567339919194
		2.6819301851972268 892.14763857305752 -104.5663389281124
		-71.25763715472192 892.14763857305752 -73.939567339919165
		;
createNode transform -n "right_leg_pv" -p "topCon";
	rename -uid "FCA7215F-4B7E-F01B-78B5-FC9B3FEB72FF";
	setAttr ".rp" -type "double3" -265.76397757376515 267.13791344146756 -38.472443112468042 ;
	setAttr ".sp" -type "double3" -265.76397757376515 267.13791344146756 -38.472443112468042 ;
createNode nurbsSurface -n "right_leg_pvShape" -p "right_leg_pv";
	rename -uid "5F355AFA-4EF6-586F-67B4-9CB9A4B688C5";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-265.76397757376515 243.67420600357775 -38.472443112468049
		-261.07318848165869 243.67420600357775 -43.163232204574506
		-259.13020002147641 243.67420600357775 -38.472443112468042
		-261.07318848165869 243.67420600357775 -33.781654020361579
		-265.76397757376515 243.67420600357775 -31.838665560179308
		-270.4547666658716 243.67420600357775 -33.781654020361579
		-272.39775512605388 243.67420600357775 -38.472443112468042
		-270.4547666658716 243.67420600357775 -43.163232204574506
		-265.76397757376515 243.67420600357775 -45.106220664756783
		-261.07318848165869 243.67420600357775 -43.163232204574506
		-259.13020002147641 243.67420600357775 -38.472443112468042
		-261.07318848165869 243.67420600357775 -33.781654020361579
		-251.30024487436077 248.75147953009042 -52.936175811872403
		-245.30917062772829 248.75147953009042 -38.472443112468042
		-251.3002448743608 248.75147953009042 -24.008710413063682
		-265.76397757376515 248.75147953009042 -18.017636166431178
		-280.22771027316952 248.75147953009042 -24.008710413063682
		-286.218784519802 248.75147953009042 -38.472443112468042
		-280.22771027316952 248.75147953009042 -52.936175811872403
		-265.76397757376515 248.75147953009042 -58.92725005850491
		-251.30024487436077 248.75147953009042 -52.936175811872403
		-245.30917062772829 248.75147953009042 -38.472443112468042
		-251.3002448743608 248.75147953009042 -24.008710413063682
		-245.4161930564016 267.13791344146756 -58.820227629831592
		-236.98786474506434 267.13791344146756 -38.472443112468042
		-245.4161930564016 267.13791344146756 -18.124658595104492
		-265.76397757376515 267.13791344146756 -9.6963302837672281
		-286.1117620911287 267.13791344146756 -18.124658595104496
		-294.54009040246598 267.13791344146756 -38.472443112468042
		-286.1117620911287 267.13791344146756 -58.820227629831592
		-265.76397757376515 267.13791344146756 -67.24855594116886
		-245.4161930564016 267.13791344146756 -58.820227629831592
		-236.98786474506434 267.13791344146756 -38.472443112468042
		-245.4161930564016 267.13791344146756 -18.124658595104492
		-251.30024487436077 285.5243473528447 -52.936175811872403
		-245.30917062772829 285.5243473528447 -38.472443112468035
		-251.3002448743608 285.5243473528447 -24.008710413063678
		-265.76397757376515 285.5243473528447 -18.017636166431178
		-280.22771027316952 285.5243473528447 -24.008710413063682
		-286.218784519802 285.5243473528447 -38.472443112468042
		-280.22771027316952 285.5243473528447 -52.936175811872403
		-265.76397757376515 285.5243473528447 -58.92725005850491
		-251.30024487436077 285.5243473528447 -52.936175811872403
		-245.30917062772829 285.5243473528447 -38.472443112468035
		-251.3002448743608 285.5243473528447 -24.008710413063678
		-261.07318848165869 290.60162087935737 -43.163232204574498
		-259.13020002147641 290.60162087935737 -38.472443112468042
		-261.07318848165869 290.60162087935737 -33.781654020361586
		-265.76397757376515 290.60162087935737 -31.838665560179312
		-270.4547666658716 290.60162087935737 -33.781654020361586
		-272.39775512605388 290.60162087935737 -38.472443112468042
		-270.4547666658716 290.60162087935737 -43.163232204574498
		-265.76397757376515 290.60162087935737 -45.106220664756769
		-261.07318848165869 290.60162087935737 -43.163232204574498
		-259.13020002147641 290.60162087935737 -38.472443112468042
		-261.07318848165869 290.60162087935737 -33.781654020361586
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		-265.76397757376515 290.60162087935737 -38.472443112468035
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode transform -n "left_leg_pv" -p "topCon";
	rename -uid "34962F8B-4801-171E-228B-EB8F20514BA5";
	setAttr ".rp" -type "double3" -265.76397757376515 267.13791344146756 38.472 ;
	setAttr ".sp" -type "double3" -265.76397757376515 267.13791344146756 38.472 ;
createNode nurbsSurface -n "left_leg_pvShape" -p "left_leg_pv";
	rename -uid "D899711D-4C4B-12E9-A2B1-ECB3A837A275";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-265.76397757376515 243.67420600357775 38.471999999999994
		-261.07318848165869 243.67420600357775 33.781210907893538
		-259.13020002147641 243.67420600357775 38.472000000000001
		-261.07318848165869 243.67420600357775 43.162789092106465
		-265.76397757376515 243.67420600357775 45.105777552288735
		-270.4547666658716 243.67420600357775 43.162789092106465
		-272.39775512605388 243.67420600357775 38.472000000000001
		-270.4547666658716 243.67420600357775 33.781210907893538
		-265.76397757376515 243.67420600357775 31.838222447711264
		-261.07318848165869 243.67420600357775 33.781210907893538
		-259.13020002147641 243.67420600357775 38.472000000000001
		-261.07318848165869 243.67420600357775 43.162789092106465
		-251.30024487436077 248.75147953009042 24.008267300595641
		-245.30917062772829 248.75147953009042 38.472000000000001
		-251.3002448743608 248.75147953009042 52.935732699404362
		-265.76397757376515 248.75147953009042 58.926806946036862
		-280.22771027316952 248.75147953009042 52.935732699404362
		-286.218784519802 248.75147953009042 38.472000000000001
		-280.22771027316952 248.75147953009042 24.008267300595641
		-265.76397757376515 248.75147953009042 18.017193053963137
		-251.30024487436077 248.75147953009042 24.008267300595641
		-245.30917062772829 248.75147953009042 38.472000000000001
		-251.3002448743608 248.75147953009042 52.935732699404362
		-245.4161930564016 267.13791344146756 18.124215482636455
		-236.98786474506434 267.13791344146756 38.472000000000001
		-245.4161930564016 267.13791344146756 58.819784517363551
		-265.76397757376515 267.13791344146756 67.248112828700812
		-286.1117620911287 267.13791344146756 58.819784517363544
		-294.54009040246598 267.13791344146756 38.472000000000001
		-286.1117620911287 267.13791344146756 18.124215482636451
		-265.76397757376515 267.13791344146756 9.6958871712991872
		-245.4161930564016 267.13791344146756 18.124215482636455
		-236.98786474506434 267.13791344146756 38.472000000000001
		-245.4161930564016 267.13791344146756 58.819784517363551
		-251.30024487436077 285.5243473528447 24.008267300595641
		-245.30917062772829 285.5243473528447 38.472000000000008
		-251.3002448743608 285.5243473528447 52.935732699404369
		-265.76397757376515 285.5243473528447 58.926806946036862
		-280.22771027316952 285.5243473528447 52.935732699404362
		-286.218784519802 285.5243473528447 38.472000000000001
		-280.22771027316952 285.5243473528447 24.008267300595637
		-265.76397757376515 285.5243473528447 18.017193053963137
		-251.30024487436077 285.5243473528447 24.008267300595641
		-245.30917062772829 285.5243473528447 38.472000000000008
		-251.3002448743608 285.5243473528447 52.935732699404369
		-261.07318848165869 290.60162087935737 33.781210907893552
		-259.13020002147641 290.60162087935737 38.472000000000001
		-261.07318848165869 290.60162087935737 43.162789092106465
		-265.76397757376515 290.60162087935737 45.105777552288728
		-270.4547666658716 290.60162087935737 43.162789092106458
		-272.39775512605388 290.60162087935737 38.472000000000001
		-270.4547666658716 290.60162087935737 33.781210907893545
		-265.76397757376515 290.60162087935737 31.838222447711274
		-261.07318848165869 290.60162087935737 33.781210907893552
		-259.13020002147641 290.60162087935737 38.472000000000001
		-261.07318848165869 290.60162087935737 43.162789092106465
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		-265.76397757376515 290.60162087935737 38.472000000000008
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode transform -n "left_hand_ik" -p "topCon";
	rename -uid "1C9AC383-4EA2-DAF1-5754-2EA0E9F24CB8";
	setAttr ".rp" -type "double3" -8.332830000000083 695.10000000000014 347.807 ;
	setAttr ".sp" -type "double3" -8.332830000000083 695.10000000000014 347.807 ;
createNode nurbsCurve -n "left_hand_ikShape" -p "left_hand_ik";
	rename -uid "7008D719-461B-83F9-E207-148EBDE85859";
	setAttr -k off ".v";
	setAttr ".tw" yes;
createNode ikHandle -n "left_arm_ik_handle" -p "left_hand_ik";
	rename -uid "DC519E9D-4168-AFC4-D1B2-048A2BAFA249";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -8.332830000000083 695.10000000000014 347.807 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "left_arm_ik_handle_poleVectorConstraint1" -p "left_arm_ik_handle";
	rename -uid "5EDA202A-43B8-F40A-105A-65B59090306E";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "nurbsSphere4W0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.9103199999999889 -133.60789460156172 132.1027169462381 ;
	setAttr -k on ".w0";
createNode transform -n "left_hand_1" -p "left_hand_ik";
	rename -uid "16AFDE49-4BD1-B2E8-AAFE-858B961AA3A6";
	setAttr ".rp" -type "double3" -8.3328286667368658 694.056 371.95799999999986 ;
	setAttr ".sp" -type "double3" -8.3328286667368658 694.056 371.95799999999986 ;
createNode nurbsCurve -n "left_hand_Shape1" -p "|topCon|left_hand_ik|left_hand_1";
	rename -uid "B3054F6B-4520-E884-CBCF-F88CD95CF4F3";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		13.823759042215364 716.21258770895224 371.95799999999986
		-8.3328286667368641 725.39014683390928 371.95799999999986
		-30.489416375689096 716.21258770895224 371.95799999999986
		-39.666975500646132 694.05600000000004 371.95799999999986
		-30.489416375689096 671.89941229104784 371.95799999999986
		-8.3328286667368694 662.7218531660908 371.95799999999986
		13.823759042215364 671.89941229104784 371.95799999999986
		23.001318167172403 694.05600000000004 371.95799999999986
		13.823759042215364 716.21258770895224 371.95799999999986
		-8.3328286667368641 725.39014683390928 371.95799999999986
		-30.489416375689096 716.21258770895224 371.95799999999986
		;
createNode transform -n "left_hand_2" -p "|topCon|left_hand_ik|left_hand_1";
	rename -uid "454D50CA-4807-71A0-0202-F39C75E752A0";
	setAttr ".rp" -type "double3" -8.332827445000202 694.07900000000018 392.76299999999981 ;
	setAttr ".sp" -type "double3" -8.332827445000202 694.07900000000018 392.76299999999981 ;
createNode nurbsCurve -n "left_hand_Shape2" -p "|topCon|left_hand_ik|left_hand_1|left_hand_2";
	rename -uid "DCB1C4C8-47CE-F173-A319-8DBE76B4CD53";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.715562792780652 712.12739023778101 392.76299999999981
		-8.3328274450002002 719.60327825327204 392.76299999999981
		-26.38121768278106 712.12739023778101 392.76299999999981
		-33.857105698272065 694.07900000000018 392.76299999999981
		-26.38121768278106 676.03060976221934 392.76299999999981
		-8.3328274450002038 668.55472174672832 392.76299999999981
		9.715562792780652 676.03060976221934 392.76299999999981
		17.191450808271657 694.07900000000018 392.76299999999981
		9.715562792780652 712.12739023778101 392.76299999999981
		-8.3328274450002002 719.60327825327204 392.76299999999981
		-26.38121768278106 712.12739023778101 392.76299999999981
		;
createNode transform -n "right_hand_ik" -p "topCon";
	rename -uid "47BF1F03-4DFD-8F22-5603-A0A979759DAC";
	setAttr ".rp" -type "double3" -8.332830000000083 695.10000000000014 -347.807 ;
	setAttr ".sp" -type "double3" -8.332830000000083 695.10000000000014 -347.807 ;
createNode nurbsCurve -n "right_hand_ikShape" -p "right_hand_ik";
	rename -uid "8494D951-4BF1-A059-EA6A-AEB356B4E843";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		20.732780873740268 666.03954985643952 -348.35444961955488
		32.77214778975187 695.10000330621222 -347.80699995089083
		20.732776197571653 724.16045481925062 -347.25955031099426
		-8.3328333065506115 736.19768602790271 -347.03278927420035
		-37.398440873740427 724.16045014356075 -347.25955038044515
		-49.437807789752071 695.09999669378806 -347.80700004910921
		-37.398436197571819 666.03954518074966 -348.35444968900578
		-8.3328266934495581 654.00231397209757 -348.58121072579968
		20.732780873740268 666.03954985643952 -348.35444961955488
		32.77214778975187 695.10000330621222 -347.80699995089083
		20.732776197571653 724.16045481925062 -347.25955031099426
		;
createNode ikHandle -n "right_arm_ik_handle" -p "right_hand_ik";
	rename -uid "1F6D3C7B-4AB0-FFB5-CE33-8A982608A06B";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -8.3328300000002393 695.1 -347.80692812606804 ;
	setAttr ".roc" yes;
createNode poleVectorConstraint -n "right_arm_ik_handle_poleVectorConstraint1" -p
		 "right_arm_ik_handle";
	rename -uid "D3C7CD91-4EBB-8481-48D7-F4847487DED9";
	addAttr -dcb 0 -ci true -k true -sn "w0" -ln "right_arm_pvW0" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr ".rst" -type "double3" -4.9103199999999081 -133.60789460156161 -132.10297187393195 ;
	setAttr -k on ".w0";
createNode transform -n "right_hand_1" -p "right_hand_ik";
	rename -uid "F168EECA-42E3-9BB3-B698-9581FA210726";
	setAttr ".rp" -type "double3" -8.332830000000305 694.05599999999981 -371.95751350406749 ;
	setAttr ".sp" -type "double3" -8.332830000000305 694.05599999999981 -371.95751350406749 ;
createNode nurbsCurve -n "right_hand_Shape1" -p "|topCon|right_hand_ik|right_hand_1";
	rename -uid "7F8E03A7-4990-68E8-8AE4-43AB8FD7ED2A";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		13.823757708951925 716.21258770895201 -371.95751350406749
		-8.3328300000003033 725.39014683390906 -371.95751350406749
		-30.489417708952537 716.21258770895201 -371.95751350406749
		-39.666976833909573 694.05599999999981 -371.95751350406749
		-30.489417708952537 671.89941229104761 -371.95751350406749
		-8.3328300000003086 662.72185316609057 -371.95751350406749
		13.823757708951925 671.89941229104761 -371.95751350406749
		23.001316833908966 694.05599999999981 -371.95751350406749
		13.823757708951925 716.21258770895201 -371.95751350406749
		-8.3328300000003033 725.39014683390906 -371.95751350406749
		-30.489417708952537 716.21258770895201 -371.95751350406749
		;
createNode transform -n "right_hand_2" -p "|topCon|right_hand_ik|right_hand_1";
	rename -uid "49BD73BA-4524-68CA-5553-CE87015404AB";
	setAttr ".rp" -type "double3" -8.3328300000003601 694.07905136037232 -392.76279747846604 ;
	setAttr ".sp" -type "double3" -8.3328300000003601 694.07905136037232 -392.76279747846604 ;
createNode nurbsCurve -n "right_hand_Shape2" -p "|topCon|right_hand_ik|right_hand_1|right_hand_2";
	rename -uid "260CBE96-45E2-75B0-41C0-17AA26E48FE2";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		9.7155602377804939 712.12744159815315 -392.76279747846604
		-8.3328300000003583 719.60332961364418 -392.76279747846604
		-26.381220237781218 712.12744159815315 -392.76279747846604
		-33.857108253272223 694.07905136037232 -392.76279747846604
		-26.381220237781218 676.03066112259148 -392.76279747846604
		-8.3328300000003619 668.55477310710046 -392.76279747846604
		9.7155602377804939 676.03066112259148 -392.76279747846604
		17.191448253271499 694.07905136037232 -392.76279747846604
		9.7155602377804939 712.12744159815315 -392.76279747846604
		-8.3328300000003583 719.60332961364418 -392.76279747846604
		-26.381220237781218 712.12744159815315 -392.76279747846604
		;
createNode transform -n "left_arm_pv" -p "topCon";
	rename -uid "E28F729E-499D-9080-D271-95949581736E";
	setAttr ".rp" -type "double3" 0 581.20010539843838 211.42371694623819 ;
	setAttr ".sp" -type "double3" 0 581.20010539843838 211.42371694623819 ;
createNode nurbsSurface -n "left_arm_pvShape" -p "left_arm_pv";
	rename -uid "692EFE7E-4A35-D03E-B83A-DF9427892E6F";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".tw" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode transform -n "right_arm_pv" -p "topCon";
	rename -uid "D867702A-4BB0-1614-8FB2-009D57561E06";
	setAttr ".rp" -type "double3" 0 581.20010539843838 -211.424 ;
	setAttr ".sp" -type "double3" 0 581.20010539843838 -211.424 ;
createNode nurbsSurface -n "right_arm_pvShape" -p "right_arm_pv";
	rename -uid "6F357A4B-40CA-5724-AB5C-FDB0B4EE8A9B";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".dvu" 0;
	setAttr ".dvv" 0;
	setAttr ".cpr" 4;
	setAttr ".cps" 4;
	setAttr ".cc" -type "nurbsSurface" 
		3 3 0 2 no 
		9 0 0 0 1 2 3 4 4 4
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		
		77
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		2.1089175795141311e-15 559.22414391880386 -211.42400000000001
		4.3933636945522752 559.22414391880386 -215.81736369455228
		6.2131545212733927 559.22414391880386 -211.42400000000001
		4.3933636945522752 559.22414391880386 -207.03063630544773
		1.0524289692731079e-15 559.22414391880386 -205.21084547872661
		-4.3933636945522743 559.22414391880386 -207.03063630544773
		-6.2131545212733963 559.22414391880386 -211.42400000000001
		-4.3933636945522752 559.22414391880386 -215.81736369455228
		-1.5776236300683786e-15 559.22414391880386 -217.6371545212734
		4.3933636945522752 559.22414391880386 -215.81736369455228
		6.2131545212733927 559.22414391880386 -211.42400000000001
		4.3933636945522752 559.22414391880386 -207.03063630544773
		13.546641488572277 563.979486514835 -224.97064148857228
		19.157844117744958 563.979486514835 -211.42400000000001
		13.546641488572275 563.979486514835 -197.87735851142773
		-1.8446812382596305e-16 563.979486514835 -192.26615588225505
		-13.546641488572277 563.979486514835 -197.87735851142773
		-19.157844117744972 563.979486514835 -211.42400000000001
		-13.546641488572275 563.979486514835 -224.97064148857228
		-1.4349342922152854e-15 563.979486514835 -230.58184411774496
		13.546641488572277 563.979486514835 -224.97064148857228
		19.157844117744958 563.979486514835 -211.42400000000001
		13.546641488572275 563.979486514835 -197.87735851142773
		19.057607581118891 581.20010539843838 -230.48160758111888
		26.951527107602633 581.20010539843838 -211.42400000000001
		19.05760758111888 581.20010539843838 -192.36639241888111
		-1.903438215904711e-15 581.20010539843838 -184.47247289239738
		-19.057607581118887 581.20010539843838 -192.36639241888113
		-26.951527107602647 581.20010539843838 -211.42400000000001
		-19.05760758111888 581.20010539843838 -230.48160758111891
		-3.7476009523126767e-16 581.20010539843838 -238.37552710760264
		19.057607581118891 581.20010539843838 -230.48160758111888
		26.951527107602633 581.20010539843838 -211.42400000000001
		19.05760758111888 581.20010539843838 -192.36639241888111
		13.546641488572281 598.42072428204176 -224.97064148857228
		19.157844117744958 598.42072428204176 -211.42400000000001
		13.546641488572272 598.42072428204176 -197.87735851142773
		-2.5215583274747921e-15 598.42072428204176 -192.26615588225505
		-13.546641488572281 598.42072428204176 -197.87735851142773
		-19.157844117744972 598.42072428204176 -211.42400000000001
		-13.546641488572272 598.42072428204176 -224.97064148857228
		9.0215591143354367e-16 598.42072428204176 -230.58184411774496
		13.546641488572281 598.42072428204176 -224.97064148857228
		19.157844117744958 598.42072428204176 -211.42400000000001
		13.546641488572272 598.42072428204176 -197.87735851142773
		4.3933636945522716 603.1760668780729 -215.81736369455228
		6.2131545212733839 603.1760668780729 -211.42400000000001
		4.3933636945522663 603.1760668780729 -207.03063630544773
		-1.9300308736028155e-15 603.1760668780729 -205.21084547872661
		-4.3933636945522707 603.1760668780729 -207.03063630544773
		-6.2131545212733883 603.1760668780729 -211.42400000000001
		-4.3933636945522663 603.1760668780729 -215.81736369455228
		1.4048362128075456e-15 603.1760668780729 -217.6371545212734
		4.3933636945522716 603.1760668780729 -215.81736369455228
		6.2131545212733839 603.1760668780729 -211.42400000000001
		4.3933636945522663 603.1760668780729 -207.03063630544773
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		-1.911872758287121e-15 603.1760668780729 -211.42400000000001
		
		;
	setAttr ".nufa" 4.5;
	setAttr ".nvfa" 4.5;
createNode lightLinker -s -n "lightLinker1";
	rename -uid "C08C8D0A-430B-472E-F0C4-74A02FB999E6";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode shapeEditorManager -n "shapeEditorManager";
	rename -uid "9B66F722-4B6C-0BDB-962B-3EB41F1FA01F";
createNode poseInterpolatorManager -n "poseInterpolatorManager";
	rename -uid "C27D12D3-4FF1-C69D-97E5-52BB4774AB09";
createNode displayLayerManager -n "layerManager";
	rename -uid "B61CB59F-4327-9042-7CF3-72B017452EBA";
createNode displayLayer -n "defaultLayer";
	rename -uid "B9DD8CB3-4007-E23C-6115-179FAD55A808";
	setAttr ".ufem" -type "stringArray" 0  ;
createNode renderLayerManager -n "renderLayerManager";
	rename -uid "A86DC169-42A4-1DEB-984D-B1AB1098DAE8";
createNode renderLayer -n "defaultRenderLayer";
	rename -uid "E0FD1764-4410-BA54-62C8-DEBA1009A97C";
	setAttr ".g" yes;
createNode aiOptions -s -n "defaultArnoldRenderOptions";
	rename -uid "EDE23938-47ED-2090-17A1-BCB4F04DFB7D";
	addAttr -ci true -sn "ARV_options" -ln "ARV_options" -dt "string";
	setAttr ".version" -type "string" "5.2.1.1";
	setAttr ".ARV_options" -type "string" "Test Resolution=100%;Color Management.Gamma=1;Color Management.Exposure=0;Background.BG=BG Color;Background.Color=0 0 0;Background.Image=;Background.Scale=1            1;Background.Offset=0            0;Background.Apply Color Management=1;Foreground.Enable FG=0;Foreground.Image=;Foreground.Scale=1            1;Foreground.Offset=0            0;Foreground.Apply Color Management=1;";
createNode aiAOVFilter -s -n "defaultArnoldFilter";
	rename -uid "AA045E5A-4241-F197-39E8-4BB8A1AD9D12";
	setAttr ".ai_translator" -type "string" "gaussian";
createNode aiAOVDriver -s -n "defaultArnoldDriver";
	rename -uid "18ABF9F8-470A-04FD-5C35-8BBB8B5C18A2";
	setAttr ".ai_translator" -type "string" "exr";
createNode aiAOVDriver -s -n "defaultArnoldDisplayDriver";
	rename -uid "1DEB39FC-4997-570C-63F1-E1AC65875C62";
	setAttr ".output_mode" 0;
	setAttr ".ai_translator" -type "string" "maya";
createNode phong -n "Material";
	rename -uid "D83F5DFF-4A52-5BBD-289E-CBBE111E89CC";
	setAttr ".dc" 1;
	setAttr ".c" -type "float3" 0.80000001 0.80000001 0.80000001 ;
	setAttr ".sc" -type "float3" 0.2 0.2 0.2 ;
	setAttr ".rfl" 0;
	setAttr ".rc" -type "float3" 0.80000001 0.80000001 0.80000001 ;
	setAttr ".cp" 25;
createNode shadingEngine -n "garbSG";
	rename -uid "E36B527A-445F-0877-628C-A6A0EBE4D96A";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
	rename -uid "A03E0CA5-4246-4C22-00DD-DF93E2384433";
createNode script -n "uiConfigurationScriptNode";
	rename -uid "8F7DE769-4942-FD84-B2C0-F191ABF4F493";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $nodeEditorPanelVisible = stringArrayContains(\"nodeEditorPanel1\", `getPanel -vis`);\n\tint    $nodeEditorWorkspaceControlOpen = (`workspaceControl -exists nodeEditorPanel1Window` && `workspaceControl -q -visible nodeEditorPanel1Window`);\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\n\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n"
		+ "            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n"
		+ "            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1690\n            -height 876\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n"
		+ "            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n"
		+ "            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n"
		+ "            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|left\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"wireframe\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n"
		+ "            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n"
		+ "            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 0\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n"
		+ "            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1\n            -height 1\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"|persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n"
		+ "            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -holdOuts 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 1\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 32768\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -depthOfFieldPreview 1\n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n"
		+ "            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -controllers 0\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n"
		+ "            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -bluePencil 1\n            -greasePencils 0\n            -shadows 0\n            -captureSequenceNumber -1\n            -width 1032\n            -height 869\n            -sceneRenderFilter 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n"
		+ "            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n"
		+ "            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 0\n            -showReferenceMembers 0\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n"
		+ "            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n"
		+ "            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n"
		+ "                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n"
		+ "                -snapValue \"none\" \n                -showPlayRangeShades \"on\" \n                -lockPlayRangeShades \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -keyMinScale 1\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -valueLinesToggle 0\n                -outliner \"graphEditor1OutlineEd\" \n                -highlightAffectedCurves 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showAssignedMaterials 0\n                -showTimeEditor 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -organizeByClip 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showParentContainers 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n"
		+ "                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -showUfeItems 1\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n"
		+ "                -ignoreOutlinerColor 0\n                -renderFilterVisible 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayValues 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"timeEditorPanel\" (localizedPanelLabel(\"Time Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Time Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayValues 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -initialized 0\n"
		+ "                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n"
		+ "                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif ($nodeEditorPanelVisible || $nodeEditorWorkspaceControlOpen) {\n\t\tif (\"\" == $panelName) {\n\t\t\tif ($useSceneConfig) {\n\t\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -connectionMinSegment 0.03\n"
		+ "                -connectionOffset 0.03\n                -connectionRoundness 0.8\n                -connectionTension -100\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\t}\n\t\t} else {\n\t\t\t$label = `panel -q -label $panelName`;\n"
		+ "\t\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -consistentNameSize 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -connectNodeOnCreation 0\n                -connectOnDrop 0\n                -copyConnectionsOnPaste 0\n                -connectionStyle \"bezier\" \n                -connectionMinSegment 0.03\n                -connectionOffset 0.03\n                -connectionRoundness 0.8\n                -connectionTension -100\n                -defaultPinnedState 0\n                -additiveGraphingMode 0\n                -connectedGraphingMode 1\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n"
		+ "                -gridVisibility 1\n                -crosshairOnEdgeDragging 0\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                -showUnitConversions 0\n                -editorMode \"default\" \n                -hasWatchpoint 0\n                $editorName;\n\t\t\tif (!$useSceneConfig) {\n\t\t\t\tpanel -e -l $label $panelName;\n\t\t\t}\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"shapePanel\" (localizedPanelLabel(\"Shape Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tshapePanel -edit -l (localizedPanelLabel(\"Shape Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"posePanel\" (localizedPanelLabel(\"Pose Editor\")) `;\n\tif (\"\" != $panelName) {\n"
		+ "\t\t$label = `panel -q -label $panelName`;\n\t\tposePanel -edit -l (localizedPanelLabel(\"Pose Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"profilerPanel\" (localizedPanelLabel(\"Profiler Tool\")) `;\n"
		+ "\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Profiler Tool\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"contentBrowserPanel\" (localizedPanelLabel(\"Content Browser\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Content Browser\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"Stereo\" (localizedPanelLabel(\"Stereo\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Stereo\")) -mbv $menusOkayInPanels  $panelName;\n{ string $editorName = ($panelName+\"Editor\");\n            stereoCameraView -e \n                -editorChanged \"updateModelPanelBar\" \n                -camera \"|persp\" \n                -useInteractiveMode 0\n"
		+ "                -displayLights \"default\" \n                -displayAppearance \"wireframe\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -holdOuts 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 1\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 32768\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n"
		+ "                -depthOfFieldPreview 1\n                -maxConstantTransparency 1\n                -rendererOverrideName \"stereoOverrideVP2\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 4 4 \n                -bumpResolution 4 4 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 0\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -controllers 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n"
		+ "                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -bluePencil 1\n                -greasePencils 0\n                -shadows 0\n                -captureSequenceNumber -1\n                -width 0\n                -height 0\n"
		+ "                -sceneRenderFilter 0\n                -displayMode \"centerEye\" \n                -viewColor 0 0 0 1 \n                -useCustomBackground 1\n                $editorName;\n            stereoCameraView -e -viewSelected 0 $editorName;\n            stereoCameraView -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName; };\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"ToggledOutliner\")) `;\n\tif (\"\" != $panelName) {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"ToggledOutliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -showShapes 0\n            -showAssignedMaterials 0\n            -showTimeEditor 1\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n"
		+ "            -organizeByLayer 1\n            -organizeByClip 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showParentContainers 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n            -directSelect 0\n            -isSet 0\n            -isSetMember 0\n            -showUfeItems 1\n            -displayMode \"DAG\" \n            -expandObjects 0\n"
		+ "            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            -ignoreOutlinerColor 0\n            -renderFilterVisible 0\n            -renderFilterIndex 0\n            -selectionOrder \"chronological\" \n            -expandAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-userCreated false\n"
		+ "\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"single\\\" -ps 1 100 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Top View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"|persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1690\\n    -height 876\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Top View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -camera \\\"|persp\\\" \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -holdOuts 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 32768\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -depthOfFieldPreview 1\\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -controllers 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 0\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -bluePencil 1\\n    -greasePencils 0\\n    -shadows 0\\n    -captureSequenceNumber -1\\n    -width 1690\\n    -height 876\\n    -sceneRenderFilter 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 5 -size 12 -divisions 5 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	rename -uid "FBE2D7C3-4ED1-157C-15E5-70982EA67A99";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 80 -ast 0 -aet 80 ";
	setAttr ".st" 6;
createNode ikRPsolver -n "ikRPsolver";
	rename -uid "8AA3ADED-4403-2E0A-83AC-EC9B303B7BA8";
createNode skinCluster -n "skinCluster1";
	rename -uid "AAB97EB8-4CAA-8A16-7C81-49952E5B77C2";
	setAttr -s 242 ".wl";
	setAttr ".wl[0:99].w"
		5 4 0.033150280720425432 5 0.2850647781518541 6 0.041150382730740613 
		9 0.39733565241778057 10 0.24329890597919918
		5 0 0.45512577720477149 1 0.037619103058782438 2 0.028287473930742775 
		3 0.023841868600931775 23 0.45512577720477149
		5 4 0.021836235698031437 5 0.28089685352166294 6 0.025242895407876963 
		9 0.44418184570290137 10 0.22784216966952725
		5 0 0.338983420388761 1 0.12875768565320372 2 0.096596388229802793 
		3 0.11070667947008347 23 0.32495582625814912
		5 3 0.12031857308404931 4 0.22911477693841256 5 0.089064520166762834 
		9 0.28075106490538765 10 0.28075106490538765
		5 3 0.16757045030695297 4 0.27403819591783046 5 0.084958390538392436 
		9 0.23671648161841208 10 0.23671648161841208
		5 0 0.27539139863004047 1 0.1874779980038474 2 0.17302134230835003 
		3 0.089129332255563729 23 0.27497992880219829
		5 4 0.058691459360804533 5 0.36830498859908634 6 0.10155957264477673 
		9 0.36830498859908634 10 0.10313899079624603
		5 4 0.062344831684224347 5 0.36945100562325467 6 0.090558556400227766 
		9 0.36945100562325467 10 0.10819460066903858
		5 0 0.40272041603465192 1 0.081422203257771983 2 0.079961842308198236 
		23 0.40832852102534678 27 0.027567017374031097
		5 3 0.18132116640530604 4 0.43206230359150444 5 0.14137444695788995 
		9 0.1226210415226498 10 0.1226210415226498
		5 3 0.24961749948961659 4 0.40065312230795502 5 0.12554503041980117 
		9 0.11209217389131367 10 0.11209217389131367
		5 0 0.40368424086494953 1 0.17850916966694949 2 0.031780812708950557 
		3 0.14496148454548557 23 0.24106429221366485
		5 0 0.37747852652084068 1 0.19471985439906167 3 0.25625746163779906 
		4 0.051956656153706586 23 0.11958750128859205
		5 0 0.34194199371817846 1 0.20770847762164685 2 0.071964563945954893 
		3 0.28098486565373421 23 0.097400099060485634
		5 0 0.36414311821528411 1 0.21016326966912932 2 0.052934205082101948 
		3 0.13098359329495221 23 0.24177581373853238
		5 0 0.10004779381492655 1 0.34518404213667381 2 0.34518404213667381 
		23 0.10479224041533051 27 0.10479188149639528
		5 4 0.10319685703047138 5 0.36054391372845973 6 0.33772765544632094 
		9 0.099266091389025812 16 0.099265482405722263
		5 4 0.14891548980027042 5 0.29264169962443776 6 0.26926589591229 
		9 0.14458879485618994 16 0.14458811980681191
		5 0 0.084080403717643332 1 0.33588678762683144 2 0.33588678762683133 
		23 0.12207332348911415 27 0.12207269753957972
		5 3 0.082347843829677705 4 0.5084227409586779 5 0.30201127115227711 
		9 0.053609170120633945 16 0.053608973938733276
		5 3 0.12697861482168596 4 0.40601379368027452 5 0.28117731068185225 
		9 0.09291527023438266 16 0.092915010581804597
		5 0 0.26225608357088454 1 0.33483602106912796 3 0.14737890979121565 
		23 0.12776494016406725 27 0.12776404540470457
		5 0 0.26918349774407674 1 0.27812351342820024 2 0.14917723403948946 
		3 0.23135386525808571 23 0.072161889530147946
		5 4 0.1427700685824275 5 0.27779650984348714 6 0.25616782987851627 
		9 0.23083433649470686 16 0.092431255200862303
		5 0 0.14191003761839269 1 0.2332742773694379 2 0.2332742773694379 
		23 0.28476260368905376 27 0.10677880395367761
		5 3 0.16898063149220569 4 0.4073935871421871 5 0.22351196325569855 
		9 0.10415774301854189 10 0.09595607509136686
		5 0 0.29861675105209079 1 0.25053667160441573 2 0.11991935584175889 
		3 0.25102768245374391 23 0.079899539047990784
		5 0 0.079214262752571443 1 0.34004085242649212 2 0.34004085242649212 
		23 0.16523916179395529 27 0.075464870600488995
		5 4 0.10335807990861955 5 0.3303584123062428 6 0.31132368933829185 
		9 0.19457797039914462 16 0.060381848047701192
		5 3 0.11067818799595798 4 0.48360658102600063 5 0.24808016693614846 
		9 0.080230680899376586 10 0.07740438314251645
		5 0 0.31183883624634928 1 0.30029842720015071 3 0.13899336546682212 
		23 0.16654136391924573 27 0.082328007167432216
		5 4 0.039170141316437875 5 0.20347951715335405 6 0.033427498254970114 
		9 0.39316179779740795 10 0.33076104547783014
		5 4 0.033026095310756627 5 0.19418133835873302 6 0.02555736423881334 
		9 0.41086457313308061 10 0.33637062895861636
		5 4 0.10738733279492785 5 0.29040430089562658 6 0.088526155851637992 
		9 0.32182340709096996 10 0.19185880336683758
		5 4 0.11506862183390615 5 0.27987335051155743 6 0.085497092034068564 
		9 0.31128842687514013 10 0.20827250874532768
		5 4 0.22318379691307902 5 0.27525593147959021 6 0.19771518129591031 
		9 0.15192283882792254 16 0.151922251483498
		5 4 0.25113733616090722 5 0.31724348880460207 6 0.18609441775653093 
		9 0.12276265811095664 16 0.12276209916700317
		5 4 0.23448941432556625 5 0.31198918327197916 6 0.17657349146594603 
		9 0.19792783987709778 16 0.079020071059410571
		5 4 0.21556929679401626 5 0.28516945945758476 6 0.18024994161308011 
		9 0.21784478257114653 16 0.10116651956417229
		5 3 0.18088497638708792 4 0.3045352919643321 5 0.084994989489480546 
		9 0.21479237107954971 10 0.21479237107954971
		5 3 0.25484278422585771 4 0.35157339419745681 5 0.072305937515896007 
		9 0.16063894203039478 10 0.16063894203039478
		5 3 0.30083515687065027 4 0.43623380094477471 5 0.097507571053612643 
		9 0.08271173556548117 10 0.08271173556548117
		5 3 0.2376102001042564 4 0.47182347960992599 5 0.11007456626531538 
		9 0.090245877010251108 10 0.090245877010251108
		5 0 0.03804229830496661 1 0.03804229830496661 3 0.23749748321795142 
		4 0.6001384693763383 5 0.08627945079577698
		5 3 0.21196443962483513 4 0.51305452238625793 5 0.17491885359485135 
		9 0.050031165618056497 16 0.050031018775999067
		5 0 0.041596061901485855 1 0.041596061901485855 3 0.24306548217778576 
		4 0.58023887152526177 5 0.093503522493980848
		5 3 0.24933402996785398 4 0.49062990940057788 5 0.13984458032994723 
		9 0.060590745593003918 10 0.059600734708616848
		5 0 0.10425826478533631 1 0.10425816674044958 3 0.37908418003167654 
		4 0.37720628772154391 9 0.035193100720993721
		5 0 0.076358186680856588 1 0.060945435086050118 3 0.41826862012260518 
		4 0.41826862012260518 9 0.026159137987883052
		5 0 0.089373685461350896 1 0.06335471226037083 3 0.41610297761017978 
		4 0.41586235534972649 23 0.01530626931837189
		5 0 0.13999148845340065 1 0.13999148845340065 3 0.38608063031970097 
		4 0.30752754412470368 23 0.026408848648794062
		5 0 0.17607611942682849 1 0.17607611942682849 3 0.43133478717756552 
		4 0.20355177407650607 23 0.012961199892271541
		5 0 0.10715111769496696 1 0.069050209134832993 3 0.42384065351718847 
		4 0.38909270421819253 23 0.010865315434819093
		5 0 0.16312799389203697 1 0.16312799389203697 3 0.42241445642318387 
		4 0.23462398391725986 23 0.016705571875482206
		5 0 0.10014868378476259 1 0.066087275362684425 3 0.42090008457384542 
		4 0.40072383662127903 23 0.012140119657428613
		5 0 0.42372159329549175 1 0.062270654368090629 2 0.043449319164981494 
		3 0.046836839875944315 23 0.42372159329549175
		5 4 0.0078950921713633552 5 0.34066073439997746 6 0.013127822625998138 
		9 0.52751837025853443 10 0.11079798054412661
		5 0 0.31643463179904141 1 0.0046174177850003047 2 0.0041199881405537568 
		3 0.00067451941355653759 23 0.67415344286184808
		5 4 0.0034404731040528842 5 0.48684368079588225 6 0.015919521814545926 
		9 0.48684368079588225 10 0.0069526434896366365
		5 0 0.38973737622236393 1 0.19611777805120603 3 0.23229546138697657 
		4 0.049523869109258746 23 0.13232551523019476
		5 3 0.17703284555735607 4 0.26974751657726265 5 0.065805561088248893 
		9 0.24370703838856619 10 0.24370703838856619
		5 5 0.27758312654996153 6 0.72235313247749366 7 1.6993779214552828e-05 
		9 2.3373734034766148e-05 16 2.3373459295496243e-05
		5 0 0.00016446199078486894 1 0.4996235583552589 2 0.49962355835525912 
		23 0.00029421199751663668 27 0.00029420930118038746
		5 0 0.0095063133754747682 1 0.46600192608099372 2 0.46600192608099372 
		23 0.045698870650249766 27 0.012790963812287963
		5 4 0.0025921261751145515 5 0.47339442557578126 6 0.50255552713684104 
		7 0.0032585514115638734 9 0.018199369700699363
		5 3 0.26884745246019653 4 0.37077750224641765 5 0.058431497854287223 
		9 0.15097177371954934 10 0.15097177371954934
		5 0 0.077562914709490269 1 0.067502629543327017 3 0.41343563874601169 
		4 0.41343563874601191 9 0.028063178255159048
		5 0 0.35071853168901962 1 0.25506070245922619 3 0.23264460984819241 
		4 0.050543025450299373 23 0.11103313055326235
		5 0 0.35654335351582755 1 0.20565576443288497 3 0.2902007141798173 
		4 0.063558182929649196 23 0.084041984941820982
		5 0 0.36979285684659741 1 0.20425524189454111 3 0.30935551328382288 
		4 0.048173573606577411 23 0.068422814368461288
		5 0 0.32101897499962406 1 0.31498570720157248 3 0.20479727908695594 
		4 0.035523380531865323 23 0.12367465817998206
		5 0 0.36276075023886933 1 0.36404138894596533 3 0.18588371757771482 
		23 0.043657233499241235 27 0.043656909738209244
		5 0 0.3369100037583645 1 0.24780850847143496 2 0.075178179344542767 
		3 0.29549089982540871 23 0.044612408600249148
		5 0 0.355025669280645 1 0.35513556594935131 3 0.19032407145921632 
		23 0.067308930454732227 27 0.032205762856055212
		5 0 0.35948826799212569 1 0.22497034715114109 2 0.061208634518178387 
		3 0.30018283602294521 23 0.054149914315609747
		5 0 0.35755692630633784 1 0.21250629615577488 3 0.27611875109230155 
		4 0.061902300834783368 23 0.091915725610802357
		5 0 0.41404466621179264 1 0.078083582946221641 2 0.03519798613742825 
		3 0.061478637589216741 23 0.41119512711534062
		5 0 0.34012387662904453 1 0.16196148699840104 2 0.077695489206347321 
		3 0.18387230005465049 23 0.23634684711155662
		5 0 0.29569319682255907 1 0.20351120940373421 2 0.13548454777187849 
		3 0.18443462334235952 23 0.18087642265946877
		5 0 0.38752543153691404 1 0.10329505708846207 2 0.071395109549638666 
		3 0.050258970288071504 23 0.38752543153691382
		5 0 0.17514369086580689 1 0.31135898557557928 2 0.27972093092013162 
		3 0.1319503733451349 23 0.10182601929334716
		5 0 0.19577486840752073 1 0.23030960482472074 2 0.19557097298194528 
		23 0.18917262978947874 27 0.18917192399633453
		5 0 0.21450891939968031 1 0.21996287324348143 2 0.20454952340015448 
		23 0.21438842225969779 27 0.14659026169698608
		5 0 0.20019048197646336 1 0.28903218951857274 2 0.24926256747016781 
		3 0.1454133471437325 23 0.11610141389106364
		5 0 0.40485233353046268 1 0.11851394761354578 2 0.045254108397515203 
		3 0.12091494926762042 23 0.31046466119085603
		5 5 0.3162307791135765 6 0.042098372874585184 7 0.02417447628299766 
		9 0.38269543411278178 10 0.23480093761605897
		5 4 0.036684835726550409 5 0.25198405096063042 6 0.050387250378287805 
		9 0.35291711750531352 10 0.30802674542921782
		5 4 0.040038142050662487 5 0.17262243645885908 6 0.037093321722230101 
		9 0.36564380329171109 10 0.38460229647653726
		5 3 0.080842637619778585 4 0.13988112241185366 5 0.071293442441733704 
		9 0.35399139876331703 10 0.35399139876331703
		5 4 0.026904197716122263 5 0.1470778090471227 6 0.02347616970855879 
		9 0.36930682705829915 10 0.43323499646989716
		5 3 0.12855419874287471 4 0.18864315422911873 5 0.073648263724545554 
		9 0.30452880948149297 10 0.30462557382196809
		5 3 0.14813360292968686 4 0.20624286010164428 5 0.061932467993350199 
		9 0.29167406907930826 10 0.29201699989601043
		5 4 0.026374808975138195 5 0.23573509111583205 6 0.034178417089625865 
		9 0.36911813237224911 10 0.33459355044715489
		5 5 0.25209580492086336 6 0.044784974193785826 7 0.029648507426761464 
		9 0.33257274126391173 10 0.34089797219467766
		5 5 0.19846491020878473 6 0.041883157423322784 9 0.32660386692151677 
		10 0.39889700447066007 11 0.034151060975715727
		5 4 0.028035086675377331 5 0.12627236784412627 9 0.34003903998442125 
		10 0.47485896040085085 11 0.030794545095224363
		5 3 0.044757747345106465 4 0.082086105814119223 5 0.055224546490259541 
		9 0.40766112581381669 10 0.41027047453669807
		5 4 0.020318165464620921 5 0.10045471181940907 9 0.30474101870335146 
		10 0.54719603224761904 11 0.027290071764999493
		5 3 0.067881338914881517 4 0.10935832050113835 5 0.06003011541808502 
		9 0.37762835923018823 10 0.38510186593570683;
	setAttr ".wl[100:199].w"
		5 3 0.056142104250648396 4 0.089857715468184177 5 0.047255759786738058 
		9 0.39534126782027557 10 0.41140315267415384
		5 5 0.17702087668224303 6 0.030932473043181422 9 0.31432069300816889 
		10 0.4473351941371444 11 0.030390763129262158
		5 5 0.086595369375288306 6 0.013115651336255934 9 0.24911306042799172 
		10 0.62910455680301292 11 0.022071362057451156
		5 4 0.045448177290781509 5 0.07936869944788208 6 0.014008774290603855 
		9 0.43058717448536626 10 0.43058717448536626
		5 3 0.019949206926435158 4 0.061562631234075807 5 0.090028125335551171 
		9 0.41423001825196892 10 0.41423001825196892
		5 4 0.19838648799665828 5 0.22833057940179144 6 0.058421117359003093 
		9 0.26262293507999884 10 0.25223888016254836
		5 3 0.07859855632645775 4 0.23575210527097398 5 0.21270097800626589 
		9 0.24067332046644335 10 0.23227503992985901
		5 4 0.31789912103556917 5 0.30842646758712144 6 0.12060873021410536 
		9 0.12653303976054744 16 0.12653264140265666
		5 4 0.35924039762506976 5 0.35924039762506976 6 0.09292679829686501 
		9 0.094296367092083444 16 0.094296039360912062
		5 4 0.33754591061236044 5 0.34000318516465433 6 0.085701367734620573 
		9 0.13779535971667026 10 0.098954176771694366
		5 4 0.3201461068821248 5 0.30907212109436061 6 0.10256931692102786 
		9 0.16095920585250403 10 0.10725324924998268
		5 4 0.025175964799699416 5 0.056095885532077616 6 0.010523485983493546 
		9 0.44723083717049844 10 0.46097382651423102
		5 3 0.011874004425357252 4 0.029906256508323816 5 0.059385043087369443 
		9 0.428916180232919 10 0.46991851574603044
		5 4 0.016408901096621614 5 0.041283015740151141 9 0.37947110840869597 
		10 0.55198025347679891 11 0.010856721277732493
		5 4 0.015412126591414357 5 0.036166311852753236 9 0.33637056555416012 
		10 0.60089465023171451 11 0.011156345769957759
		5 4 0.0015987205601503398 5 0.0069324380009961909 6 0.00074926999385884526 
		9 0.40840111534475476 10 0.5823184561002398
		5 4 0.0029944406338029304 5 0.011014494800812144 9 0.27805538152729786 
		10 0.70605707232504 11 0.0018786107130470899
		5 3 0.0029463295036552578 4 0.0075409719341719946 5 0.013870896776903063 
		9 0.40964571150039214 10 0.56599609028487763
		5 4 0.0060822530750000957 5 0.020123453003179571 9 0.29311628699160636 
		10 0.6764419770734571 11 0.0042360298567569047
		5 3 0.0068599960381615374 4 0.015078366776188529 5 0.024352959198733551 
		9 0.40266210424713472 10 0.55104657373978172
		5 3 0.0078249234629307156 4 0.01577060620371622 5 0.017994950443506793 
		9 0.40979554846412619 10 0.5486139714257201
		5 4 0.0053866523495241176 5 0.041320479080497012 9 0.21645280440320283 
		10 0.72858608343130193 11 0.0082539807354741583
		5 4 0.0048100699882681281 5 0.026287508083258946 9 0.20960942773088412 
		10 0.75303128650936479 11 0.006261707688223964
		5 4 0.0063239725224025593 5 0.034262372562136038 9 0.26580899738538538 
		10 0.68639251902572562 11 0.0072121385043503921
		5 4 0.0020950164848343991 5 0.0057445301716401043 6 0.00068258227725988231 
		9 0.48112814939177218 10 0.51034972167449344
		5 4 0.0032172797928765639 5 0.01287784786318621 6 0.0014565787010310458 
		9 0.39841982175050639 10 0.58402847189239981
		5 4 0.0051230561083398119 5 0.012690915823663452 6 0.0015948138754990069 
		9 0.46912911106259181 10 0.51146210312990592
		5 3 0.00098120237502749065 4 0.0033203842232424324 5 0.0061870927468072419 
		9 0.48521830478224698 10 0.50429301587267583
		5 4 0.0031471841942815526 5 0.027350988561114929 6 0.0027704085926615238 
		9 0.24282212050681901 10 0.72390929814512306
		5 4 0.0035092333140767959 5 0.028867692010429581 6 0.002652980359101411 
		9 0.32437584163346661 10 0.64059425268292558
		5 4 0.0050019894408403704 5 0.039214332348401784 6 0.00391914968959578 
		9 0.37371857452326485 10 0.57814595399789714
		5 4 7.1333126625178291e-07 5 3.9383819079365939e-06 6 3.4280294567662576e-07 
		9 0.0033101644324077251 10 0.99668484105147237
		5 4 0.033150358851386189 5 0.28506566353075835 6 0.041150535419545886 
		16 0.3973356966201444 17 0.24329774557816525
		5 0 0.4551257719693883 1 0.037619130287554668 2 0.028287459700538288 
		3 0.023841866073130581 27 0.45512577196938819
		5 4 0.021836272035141064 5 0.28089800950559707 6 0.025242967767067338 
		16 0.4441820359981643 17 0.22784071469403028
		5 0 0.33898334513397477 1 0.12875797794973087 2 0.096596503292382715 
		3 0.11070693274486869 27 0.32495524087904309
		5 3 0.12031896636695437 4 0.22911616330157106 5 0.089064755241827442 
		16 0.28075005754482363 17 0.28075005754482363
		5 3 0.1675709573969964 4 0.27403966389009493 5 0.084958498367322041 
		16 0.23671544017279331 17 0.23671544017279331
		5 0 0.27539133905382224 1 0.18747843565926867 2 0.17302168573907323 
		3 0.089129448624622659 27 0.27497909092321321
		5 4 0.058691667777450461 5 0.36830487192939215 6 0.10156017116943619 
		16 0.36830487192939215 17 0.1031384171943291
		5 4 0.062345058780296982 5 0.369450936312606 6 0.090559038118861859 
		16 0.36945093631260623 17 0.1081940304756288
		5 0 0.40272047364119851 1 0.08142228657093066 2 0.079961915252980242 
		23 0.027567019544701872 27 0.40832830499018874
		5 3 0.18132125815637626 4 0.43206363651336138 5 0.14137455422024947 
		16 0.12262027555500642 17 0.12262027555500642
		5 3 0.24961771632088739 4 0.4006539788987874 5 0.12554507724328665 
		16 0.11209161376851925 17 0.11209161376851925
		5 0 0.40368426091581655 1 0.17851023198800275 2 0.031780819269140947 
		3 0.1449622224951293 27 0.24106246533191056
		5 0 0.37747866778242001 1 0.19472018186890663 3 0.25625819767327823 
		4 0.051956582187420393 27 0.11958637048797478
		5 0 0.34194213769250503 1 0.20770862692295464 2 0.07196447915990771 
		3 0.28098528471446033 27 0.097399471510172322
		5 0 0.36414277018903246 1 0.21016436755811826 2 0.052934327928271192 
		3 0.13098414850651138 27 0.24177438581806665
		5 4 0.14277014383612535 5 0.27779691051852357 6 0.25616818714741879 
		9 0.092431605731323044 16 0.23083315276660923
		5 0 0.14191012450258036 1 0.23327455733386485 2 0.23327455733386485 
		23 0.10677910348390539 27 0.28476165734578462
		5 3 0.16898069623434539 4 0.40739407422588225 5 0.22351214562774549 
		16 0.10415739393944455 17 0.095955689972582248
		5 0 0.2986167379891464 1 0.25053683128137927 2 0.11991937281595316 
		3 0.2510278646653909 27 0.079899193248130357
		5 0 0.07921420077413914 1 0.34004118638567021 2 0.34004118638567021 
		23 0.075465078251967702 27 0.16523834820255281
		5 4 0.10335806602297867 5 0.33035908641901912 6 0.31132430466924621 
		9 0.060382070764536118 16 0.19457647212421991
		5 3 0.11067815480844773 4 0.48360728824043286 5 0.24808035246233293 
		16 0.080230263180076145 17 0.077403941308710159
		5 0 0.31183811299906528 1 0.30029928205763323 3 0.13899368040563753 
		23 0.082328619987332985 27 0.16654030455033095
		5 4 0.039170300135156225 5 0.2034802321706688 6 0.033427637528723846 
		16 0.39316173871806648 17 0.33076009144738477
		5 4 0.033026235019626947 5 0.19418212227343956 6 0.02555746739217182 
		16 0.41086459555382637 17 0.33636957976093523
		5 4 0.10738783139357914 5 0.2904046026248191 6 0.088526567261324185 
		16 0.32182300986460871 17 0.19185798885566896
		5 4 0.11506909448989884 5 0.27987359005154944 6 0.085497423159018968 
		16 0.31128812192894539 17 0.20827177037058736
		5 4 0.23448988509031823 5 0.31198883370652758 6 0.1765738265399549 
		9 0.079020416673429558 16 0.19792703798976971
		5 4 0.21556963212137084 5 0.2851691336271262 6 0.18025022466047141 
		9 0.10116692194008636 16 0.21784408765094515
		5 3 0.18088545338582918 4 0.3045368464926464 5 0.084995092779997228 
		16 0.21479130367076354 17 0.21479130367076354
		5 3 0.25484335839703859 4 0.35157479098911071 5 0.07230589475004974 
		16 0.16063797793190043 17 0.16063797793190043
		5 3 0.300835360468025 4 0.43623458636108847 5 0.097507515030703443 
		16 0.082711269070091561 17 0.082711269070091561
		5 3 0.23761027180993696 4 0.47182466397462886 5 0.1100745170810789 
		16 0.090245273567177584 17 0.090245273567177584
		5 0 0.04159597183372446 1 0.04159597183372446 3 0.24306531011564073 
		4 0.58023934320491433 5 0.093503403011995959
		5 3 0.24933407052997267 4 0.49063040133743785 5 0.13984458183694143 
		16 0.060590485679099318 17 0.05960046061654873
		5 0 0.10425788871023113 1 0.10425779065417756 3 0.37908468105670046 
		4 0.37720684857792103 16 0.03519279100096994
		5 0 0.076357793559819309 1 0.060945095886264691 3 0.41826911427083086 
		4 0.41826911427083086 16 0.02615888201225432
		5 0 0.089373430575673374 1 0.063354506690722248 3 0.41610326778804307 
		4 0.41586264932441624 27 0.015306145621145122
		5 0 0.13999123963730575 1 0.13999123963730575 3 0.38608113280201473 
		4 0.30752778907434181 27 0.026408598849031999
		5 0 0.16312786279841895 1 0.16312786279841895 3 0.42241483857674111 
		4 0.23462399496979633 27 0.0167054408566247
		5 0 0.10014855655608018 1 0.066087175493479242 3 0.42090024137783877 
		4 0.40072397618929001 27 0.012140050383311701
		5 0 0.42372141930850749 1 0.062270832885637964 2 0.043449374750401504 
		3 0.046836953746945756 27 0.42372141930850737
		5 4 0.0078950475440107588 5 0.34066225886844148 6 0.01312778677733438 
		16 0.52751858186608747 17 0.11079632494412595
		5 0 0.31643708632101936 1 0.0046174155480695365 2 0.004119982345284553 
		3 0.00067451480807304411 27 0.67415100097755343
		5 4 0.0034404738129639807 5 0.48684364928901974 6 0.015919666379816431 
		16 0.48684364928901974 17 0.0069525612291800511
		5 0 0.38973745223136663 1 0.19611831483232484 3 0.23229632739921183 
		4 0.049523805863081319 27 0.1323240996740154
		5 3 0.17703346567123524 4 0.26974910656039947 5 0.065805612203047134 
		16 0.24370590778265908 17 0.24370590778265908
		5 0 0.0095062381978986681 1 0.46600238378676001 2 0.4660023837867599 
		23 0.012790958262188333 27 0.04569803596639313
		5 4 0.002592015261362087 5 0.47339467826480947 6 0.50255661611927083 
		7 0.0032584155780333015 16 0.018198274776524404
		5 3 0.26884809211667904 4 0.37077914398839523 5 0.058431405706633875 
		16 0.15097067909414597 17 0.15097067909414597
		5 0 0.077562459095988801 1 0.067502211652921001 3 0.41343623142556069 
		4 0.41343623142556069 16 0.028062866399968925
		5 0 0.35071809928962649 1 0.25506169072927615 3 0.23264545653998076 
		4 0.05054296113361878 27 0.11103179230749788
		5 0 0.35654338268829472 1 0.20565598365926979 3 0.29020155499766204 
		4 0.063558055724002138 27 0.084041022930771397
		5 0 0.36979303653841505 1 0.20425529371045151 3 0.30935596302542284 
		4 0.048173482359923352 27 0.068422224365787279
		5 0 0.32101838024300339 1 0.31498692171861686 3 0.20479780473547191 
		4 0.035523370086786334 27 0.12367352321612161
		5 0 0.35502597399479513 1 0.3551358694260412 3 0.19032404368874234 
		23 0.032205889217282027 27 0.067308223673139239
		5 0 0.35948829647523312 1 0.22497044602862987 2 0.061208580539294309 
		3 0.30018311553547733 27 0.054149561421365365
		5 0 0.35755682146555062 1 0.2125066849195166 3 0.27611971958957243 
		4 0.061902182739641362 27 0.091914591285719008
		5 0 0.41404443150634168 1 0.078084026221472966 2 0.035198087655869709 
		3 0.06147895213424498 27 0.41119450248207068
		5 0 0.34012401795746372 1 0.16196186018824119 2 0.077695494412792762 
		3 0.18387283122101428 27 0.23634579622048812
		5 0 0.29569322316396279 1 0.20351151877450604 2 0.13548462242238607 
		3 0.18443492303258313 27 0.18087571260656191
		5 0 0.38752497164721206 1 0.10329553279331717 2 0.071395371246561368 
		3 0.050259152665697401 27 0.38752497164721206
		5 0 0.21450841988043332 1 0.21996312343545898 2 0.20454974711036966 
		23 0.14659081009796157 27 0.21438789947577652
		5 0 0.20019042756079658 1 0.28903238632042616 2 0.24926271073732989 
		3 0.14541341851335929 27 0.11610105686808804
		5 0 0.40485270334153234 1 0.1185144478947488 2 0.045254137839582563 
		3 0.12091549461322493 27 0.31046321631091128
		5 5 0.31623180259216949 6 0.04209845178662603 7 0.024174488000725355 
		16 0.38269584514942268 17 0.23479941247105637
		5 4 0.036684888550748813 5 0.25198475460444297 6 0.050387396556695441 
		16 0.35291729495525115 17 0.30802566533286158;
	setAttr ".wl[200:241].w"
		5 4 0.040038255230080463 5 0.17262295219810664 6 0.037093438300492373 
		16 0.36564385036442665 17 0.38460150390689396
		5 3 0.080842911896041272 4 0.13988192226873125 5 0.071293638080105054 
		16 0.35399076387756123 17 0.35399076387756123
		5 4 0.026904291588509766 5 0.14707845402506559 6 0.023476255557952044 
		16 0.36930729436820853 17 0.4332337044602641
		5 3 0.12855464260992977 4 0.18864415100263834 5 0.073648387473445293 
		16 0.30452804372686154 17 0.30462477518712516
		5 3 0.14813415647626502 4 0.20624400722574124 5 0.061932543055162104 
		16 0.29167321199394142 17 0.29201608124889022
		5 4 0.026374848865027421 5 0.23573602488248493 6 0.034178516049844419 
		16 0.36911862024739817 17 0.33459198995524514
		5 5 0.25209672197223471 6 0.044785097683856398 7 0.029648565263472367 
		16 0.33257326972306889 17 0.34089634535736768
		5 5 0.19846566686843337 6 0.041883301075970499 16 0.32660437246565022 
		17 0.39889592338676472 18 0.034150736203181244
		5 4 0.02803517085824558 5 0.12627285462061824 16 0.34003963896918016 
		17 0.47485807344132264 18 0.03079426211063338
		5 3 0.044757889789621967 4 0.082086571087877716 5 0.0552247082063742 
		16 0.40766086098283616 17 0.4102699699332899
		5 4 0.020318245591782084 5 0.10045522656100911 16 0.30474196267546055 
		17 0.54719475342501112 18 0.027289811746737135
		5 3 0.06788159172018092 4 0.10935896363177346 5 0.060030271784769322 
		16 0.37762802307098753 17 0.38510114979228877
		5 3 0.056142318386327369 4 0.089858260402542522 5 0.047255882931936964 
		16 0.39534112986155256 17 0.41140240841764053
		5 5 0.17702174556141911 6 0.030932592378792319 16 0.31432148762561435 
		17 0.4473337201273625 18 0.030390454306811861
		5 5 0.086595957712604338 6 0.013115713440482703 16 0.24911423121202284 
		17 0.62910296895069684 18 0.022071128684193319
		5 4 0.045448543128967997 5 0.079369166284756196 6 0.014008849464747387 
		16 0.43058672056076419 17 0.43058672056076419
		5 3 0.019949286191087556 4 0.061563099665646152 5 0.090028587024589463 
		16 0.41422951355933846 17 0.41422951355933846
		5 4 0.19838741998254575 5 0.22833124658088311 6 0.058421277241086526 
		16 0.26262213784590488 17 0.2522379183495797
		5 3 0.078598696688364403 4 0.23575309400657415 5 0.21270144960633577 
		16 0.24067258758840746 17 0.2322741721103182
		5 4 0.33754652834717414 5 0.34000359422895932 6 0.085701427055174695 
		16 0.13779480272462497 17 0.098953647644066833
		5 4 0.3201466887197682 5 0.30907236145603484 6 0.1025694277770285 
		16 0.1609587350386282 17 0.10725278700854035
		5 4 0.025176079063615155 5 0.056096042499312548 6 0.010523515251495565 
		16 0.44723103798960862 17 0.46097332519596823
		5 3 0.011874031845593967 4 0.02990641374842052 5 0.059385242094785032 
		16 0.42891659086105993 17 0.4699177214501406
		5 4 0.016408949900223548 5 0.041283107711816106 16 0.37947217252058563 
		17 0.55197916117095946 18 0.010856608696415151
		5 4 0.015412190713622454 5 0.036166432593244757 16 0.33637189435152892 
		17 0.60089324719011972 18 0.011156235151484175
		5 4 0.0015987231177864386 5 0.0069324497291288548 6 0.0007492698095585684 
		16 0.40840362565753813 17 0.58231593168598805
		5 4 0.0029944471993903756 5 0.011014521278539132 16 0.27805749068837976 
		17 0.70605495425986442 18 0.001878586573826358
		5 3 0.0029463287657499932 4 0.0075409950443995725 5 0.013870914884196025 
		16 0.40964723532181407 17 0.56599452598384026
		5 4 0.0060822799260931825 5 0.020123540965950239 16 0.29311818212599516 
		17 0.67644001175138335 18 0.0042359852305780907
		5 3 0.0068600082409123698 4 0.015078436899571872 5 0.024353024187876601 
		16 0.402663338824162 17 0.55104519184747713
		5 3 0.007824935999374347 4 0.015770676230443734 5 0.017994987377099706 
		16 0.4097967665199021 17 0.54861263387318016
		5 4 0.0053866759467040472 5 0.04132079340971008 16 0.2164543487723134 
		17 0.72858428997556801 18 0.0082538918957045532
		5 4 0.0048100954222612706 5 0.026287693858597989 16 0.20961112686590982 
		17 0.75302943908852682 18 0.0062616447647040859
		5 4 0.0063239933818416329 5 0.03426254518489124 16 0.26581057884782711 
		17 0.68639082626988768 18 0.0072120563155524094
		5 4 0.0020950251563433692 5 0.0057445450612453484 6 0.00068258310717835653 
		16 0.48112930688957267 17 0.5103485397856603
		5 4 0.0032172947983557746 5 0.012877906386508559 6 0.0014565828421083094 
		16 0.39842204367639766 17 0.58402617229662968
		5 4 0.005123088421129198 5 0.012690973371563935 6 0.0015948192748284314 
		16 0.46913019419859897 17 0.5114609247338795
		5 3 0.00098120392668377002 4 0.003320404856083938 5 0.0061871179047983107 
		16 0.48521917460730879 17 0.50429209870512526
		5 4 0.0031471975396763297 5 0.027351206125801122 6 0.0027704209535670738 
		16 0.24282417150565719 17 0.72390700387529827
		5 4 0.003509249749619958 5 0.028867916904241432 6 0.002652992024651247 
		16 0.32437810942541978 17 0.64059173189606755
		5 4 0.0050020054814523271 5 0.039214569156466997 6 0.0039191616106443719 
		16 0.37372028175612737 17 0.57814398199530892
		5 4 7.1332864430109411e-07 5 3.938371972476466e-06 6 3.4280104898853765e-07 
		16 0.0033103096889918098 17 0.99668469580934238;
	setAttr -s 31 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.15680631874941806 0.98762937299386533 1.9203235634255452e-17 -0
		 0.98762937299386533 0.15680631874941817 -1.2094971503808443e-16 0 -1.2246467991473527e-16 -3.0814879110195767e-33 -1 0
		 -556.54303159690778 -88.362564335046656 6.815686422329173e-14 1;
	setAttr ".pm[1]" -type "matrix" 0.062080978492094471 0.9980711157575215 -2.6805962794083368e-17 -0
		 -0.99807111575752139 0.06208097849209461 2.431781747614721e-16 0 2.4437325261173941e-16 1.165751815773164e-17 1 -0
		 560.29701578098332 -43.035829167130757 -1.3702319106699251e-13 1;
	setAttr ".pm[2]" -type "matrix" -0.15680631874941806 0.98762937299386544 4.6009198428338808e-17 -0
		 0.98762937299386522 0.15680631874941817 -3.6412788979955656e-16 0 -3.6683793252647465e-16 -1.1657518157731646e-17 -1 0
		 -494.44900661422514 -90.513875048975763 1.909852228003692e-13 1;
	setAttr ".pm[3]" -type "matrix" -0.18411737904022696 -0.98290426326034253 -1.3957429162126204e-16 0
		 0.98290426326034275 -0.18411737904022704 9.8401839147183031e-17 0 -1.2241763996512782e-16 -1.1907067755956352e-16 1 -0
		 -553.24324881388691 105.35215275458967 -5.5307772665672655e-14 1;
	setAttr ".pm[4]" -type "matrix" 0.14368067469030996 -0.9896241022330331 -1.3957429162126204e-16 -0
		 0.98962410223303332 0.14368067469030998 9.8401839147183031e-17 0 -7.7326703334545362e-17 -1.5226452567993771e-16 1 -0
		 -612.77294890466533 -98.111138247060367 -5.5505734429841339e-14 1;
	setAttr ".pm[5]" -type "matrix" 0.03918556669535149 -0.99923195073154225 -1.3957429162126201e-16 -0
		 0.99923195073154247 0.03918556669535149 9.8401839147183043e-17 0 -9.2856963973329775e-17 -1.4332302351953412e-16 1 -0
		 -716.30416625084229 -23.176268293953683 -5.1970899344200789e-14 1;
	setAttr ".pm[6]" -type "matrix" -0.094809092627995709 -0.99549547259395166 -1.3957429162126201e-16 0
		 0.99549547259395188 -0.094809092627995736 9.8401839147183018e-17 0 -1.1119149730874616e-16 -1.2961618631700397e-16 1 -0
		 -769.3250427595915 80.440057302969578 -5.1339961442449999e-14 1;
	setAttr ".pm[7]" -type "matrix" 0.01851534400602068 -0.99982857632513089 -1.3957429162126204e-16 -0
		 0.99982857632513111 0.01851534400602068 9.8401839147183031e-17 0 -9.580070471853827e-17 -1.4137230918591036e-16 1 -0
		 -831.86760311193916 -13.836949993469942 -5.2594911904574821e-14 1;
	setAttr ".pm[8]" -type "matrix" 0.99999999999999956 -1.1553258350005527e-15 -1.3957429162126204e-16 -0
		 1.1587952819525064e-15 0.99999999999999978 9.8401839147183031e-17 -0 1.3957429162126194e-16 -9.8401839147183203e-17 1 -0
		 -2.681930185198262 -892.14763857305729 -5.1393996453069635e-14 1;
	setAttr ".pm[9]" -type "matrix" -1.5766239482585878e-15 2.4677145545861107e-15 0.99999999999999933 -0
		 0.73593101176182885 -0.67705653082088391 2.8513938317956838e-15 -0 0.67705653082088413 0.73593101176182896 -6.6920065569024372e-16 -0
		 -522.48680778374171 542.33987591893447 -4.9103200000020788 1;
	setAttr ".pm[10]" -type "matrix" 3.4980822239674598e-16 2.9074030344419489e-15 0.99999999999999956 -0
		 0.13832224525180573 -0.99038727600292753 2.8513938317956845e-15 0 0.99038727600292775 0.13832224525180556 -6.6920065569024381e-16 -0
		 -20.315310508424602 718.90861069118 -4.9103200000020051 1;
	setAttr ".pm[11]" -type "matrix" 7.2247398346767108e-16 2.8378494569569721e-15 0.99999999999999956 -0
		 0.0092185452926791221 -0.99995750830857133 2.8513938317956842e-15 0 0.99995750830857155 0.0092185452926789278 -6.6920065569024391e-16 -0
		 205.00054560892085 698.27647776334459 1.8655299999980184 1;
	setAttr ".pm[12]" -type "matrix" 6.9514825669555819e-16 2.8446664300672847e-15 0.99999999999999956 -0
		 0.018834962757058496 -0.9998226063547172 2.8513938317956842e-15 0 0.99982260635471742 0.018834962757058309 -6.6920065569024381e-16 -0
		 334.65304677480185 701.52762421506532 8.3328299999980118 1;
	setAttr ".pm[13]" -type "matrix" -4.3011583918344602e-16 2.8302375544423468e-15 -0.99999999999999956 -0
		 0.0011079563370642758 -0.99999938621618911 -2.8510374858595874e-15 0 -0.99999938621618922 -0.0011079563370644666 3.4757632316284618e-16 0
		 -372.72626894604508 693.64346131545778 -8.3328299999981468 1;
	setAttr ".pm[14]" -type "matrix" -4.7931821436030371e-16 2.8223214379433684e-15 -0.99999999999999956 -0
		 0.018515344006025149 -0.99982857632513078 -2.851037485859587e-15 0 -0.99982857632513089 -0.01851534400602534 3.4757632316284609e-16 0
		 -405.54658103968353 686.68793147065594 -8.3328299999981397 1;
	setAttr ".pm[15]" -type "matrix" -0.039185566695350713 0.99923195073154225 8.8199496530749458e-16 -0
		 -0.99923195073154247 -0.039185566695350692 -3.7199933019456096e-16 0 -5.3753042576038589e-16 -8.1335366304822532e-16 0.99999999999999978 -0
		 693.41658891137172 35.532042351583996 403.41714125562231 1;
	setAttr ".pm[16]" -type "matrix" 2.1246014704267292e-15 -2.4460094339290237e-15 -0.99999999999999956 -0
		 -0.73593101176182851 0.67705653082088435 -3.2575685969753614e-15 0 0.67705653082088457 0.73593101176182874 -5.6995409058207253e-16 -0
		 522.48682682667959 -542.33985522008913 4.9103200000024678 1;
	setAttr ".pm[17]" -type "matrix" 8.7206016437896295e-17 -3.2387171333270584e-15 -0.99999999999999956 -0
		 -0.13832224525180481 0.99038727600292775 -3.2575685969753618e-15 0 0.99038727600292797 0.13832224525180481 -5.6995409058207233e-16 -0
		 20.315338364123779 -718.90860680071933 4.910320000002371 1;
	setAttr ".pm[18]" -type "matrix" -3.3192307148995568e-16 -3.3320012565859256e-08 -0.99999999999999911 0
		 -0.0092185452926782426 0.99995750830857089 -3.3318596777028903e-08 0 0.99995750830857166 0.0092185452926782045 -3.0716258523514159e-10 -0
		 -205.00061747979939 -698.27647848807669 -1.8655067334188549 1;
	setAttr ".pm[19]" -type "matrix" 3.2044664045228539e-10 -8.0441621725000886e-08 -0.99999999999999634 -0
		 -0.018834962757057604 0.99982260635471398 -8.043338753095104e-08 0 0.99982260635471742 0.018834962757057531 -1.1947253623882827e-09 -0
		 -334.65311863331425 -701.52762623911212 -8.332773675218526 1;
	setAttr ".pm[20]" -type "matrix" 6.3520487975324653e-16 -7.4469919509060005e-09 0.99999999999999933 -0
		 -0.0011079563370652119 0.99999938621618911 7.4469874186885004e-09 0 -0.99999938621618922 -0.0011079563370652496 -8.2500983843974439e-12 -0
		 372.72675544167942 -693.6434608384958 8.332824834442464 1;
	setAttr ".pm[21]" -type "matrix" 1.2963464074173682e-10 2.235533044725487e-08 0.99999999999999911 -0
		 -0.018515344006026085 0.99982857632513056 -2.2349097946466464e-08 0 -0.999828576325131 -0.018515344006026113 5.4352926022973773e-10 -0
		 405.54678257662613 -686.68787618304839 8.3328452985614483 1;
	setAttr ".pm[22]" -type "matrix" 0.039185566695350012 -0.99923195073154192 -2.0556205081424268e-16 -0
		 0.99923195073154225 0.039185566695350046 9.857608915558894e-16 0 -1.0956517615357539e-15 -5.2602619318358425e-16 0.99999999999999956 -0
		 -693.41623516036577 -35.532028478995038 -403.41700000000048 1;
	setAttr ".pm[23]" -type "matrix" -0.15679621116079887 0.98756904080779062 0.011060642097021305 -0
		 0.98762928672710615 0.15680683367561513 -9.4400928281942299e-05 0 -0.0018276116998478893 0.010909012357140287 -0.99993882477122875 0
		 -509.71336248634782 -101.50439727101816 -35.505351246728459 1;
	setAttr ".pm[24]" -type "matrix" 0.071462979010819633 0.99738192525596758 -0.011060642097021314 -0
		 -0.99744286054030218 0.071468391934084219 9.4400928282064163e-05 0 0.00088464008402846638 0.011025612321109005 0.99993882477122886 -0
		 265.5723931882938 -20.147722938677138 35.505351246728452 1;
	setAttr ".pm[25]" -type "matrix" -0.95844736613851533 -0.28505492213345057 0.011060637849371937 -0
		 -0.28507335966558345 0.95850569674892272 -9.4386645434053941e-05 0 -0.010574779010435114 -0.0032435578234796905 -0.99993882481956176 0
		 30.99052304575579 -46.850594874609897 -35.505351944856777 1;
	setAttr ".pm[26]" -type "matrix" -0.15679621132418931 0.9875690408294221 0.011060637849371939 -0
		 0.98762928672850037 0.15680683367543038 -9.4386645434053914e-05 0 -0.0018275969284890505 0.010909010401533805 -0.99993882481956176 0
		 -29.473817142537658 134.3405408076022 -35.505351944856784 1;
	setAttr ".pm[27]" -type "matrix" 0.15679621384860573 -0.98756905686476415 -0.011059170221222995 -0
		 -0.98762928674967854 -0.15680683354076405 9.4388768125691611e-05 0 -0.001827368890694805 0.010907560596157814 -0.99993884105218078 -0
		 509.71327688297606 101.50444880272651 35.505303207731473 1;
	setAttr ".pm[28]" -type "matrix" -0.07146298031067444 -0.99738194164916549 0.011059155359065369 -0
		 0.99744286055121789 -0.071468391796386407 -9.438983308832972e-05 0 0.00088452276313636888 0.011024130177842983 0.99993884121645304 -0
		 -265.57251857229869 20.147785577698688 -35.505302907505857 1;
	setAttr ".pm[29]" -type "matrix" 0.95844738186473677 0.28505492690540796 -0.011059152028233177 -0
		 0.28507335953348817 -0.95850569678899833 9.4378633065701339e-05 0 -0.010573357126367141 -0.0032431265760458181 -0.99993884125434873 0
		 -30.990574359499234 46.850596658850257 35.505303454949555 1;
	setAttr ".pm[30]" -type "matrix" 0.15679621399085625 -0.98756905704591091 -0.011059152028233179 -0
		 -0.98762928675089245 -0.15680683353921848 9.4378633065700878e-05 0 -0.0018273560288380483 0.010907544217367292 -0.99993884125434895 -0
		 29.473871518447488 -134.34087839885791 35.505305787318981 1;
	setAttr ".gm" -type "matrix" 100 0 0 0 -0 -1.6292067961387602e-05 -99.999999999998678 0
		 0 99.999999999998678 -1.6292067961387602e-05 0 3.6557316780090332 161.18052673339844 0 1;
	setAttr -s 16 ".ma";
	setAttr -s 31 ".dpf[0:30]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 16 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 16 ".ifcl";
createNode dagPose -n "bindPose1";
	rename -uid "E2257C26-495D-3395-F14B-41A8AF2EC28A";
	setAttr -s 31 ".wm";
	setAttr -s 46 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 3.1415926535897931 -1.2168542521030101 0 0
		 563.51405376879643 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.098085012205471195 0.99517803953898243 1
		 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 -2.88691974891276 7.8129954425729409
		 -2.4973214890124123e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.047649554244525973 0.99886411487263771 2.9176937043178456e-18 6.11627870530965e-17 1
		 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 59.477188771236058 7.1054273576010019e-15
		 -6.5905075957932607e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.047649554244525973 0.99886411487263771 2.9176937043178456e-18 6.11627870530965e-17 1
		 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 -0.59254572285419671 1.6164649138543008
		 -5.2826107800067994e-17 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.9999039678043834 -0.013858397059202329 -8.4858207999326121e-19 6.1226459681318814e-17 1
		 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 58.326376222729209 -1.4210854715202004e-13
		 1.9796176416870335e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.16393346231424727 0.9864713984367024 1
		 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 97.158739747287996 -1.4210854715202004e-14
		 -3.534835085640569e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.052467938656974945 0.99862260910370337 1
		 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 56.867538368461055 1.7763568394002505e-14
		 -6.3093790175077693e-16 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.067023372781539026 0.99775140566224552 1
		 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 58.759836629845836 5.6843418860808015e-14
		 1.2549504621248279e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.056703623619576482 0.99839105518249183 1
		 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 60.176757094367673 8.2198113017797041e-14
		 -1.2009154515051715e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.70053003361525412 0.71362292003761407 1
		 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 35.0198978734702 1.3733262287262811
		 -45.371628126067975 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.66368227042983796 -0.24397918746707836 0.26979856530307783 0.65361206702477603 1
		 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 -50.142253252473665 -0.00035324688565196993
		 -7.1942451995710144e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.33738420001756392 0.94136703871471328 1
		 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 -133.39016696660815 0.00017428602893687639
		 -6.775850000000025 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.064728963164722819 0.99790288171125152 1
		 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 -136.38379277953931 -0.00026018198116162239
		 -6.4672999999999936 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.0048086818200618756 0.99998843822273931 1
		 1 1 yes;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 -24.165964918742077 0.58893942487873119
		 -1.4210854715202004e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0099718512964512875 0.99995027985481433 -1.6035959929030648e-18 -1.6080426935850183e-16 1
		 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 20.805296744352859 -1.1368683772161603e-13
		 -7.1054273576010019e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.0087041128421622409 0.99996211849231109 1
		 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 10.656170497062567 -1.2129186544029835e-13
		 -6.5800582896441245e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48536584844035074 -0.51421784602226595 0.49480356555609434 0.50514298125676826 1
		 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 35.019897873470313 1.3733262287261851
		 45.371599999999994 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.24397918746707828 -0.66368227042983785 -0.65361206702477614 0.26979856530307811 1
		 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 50.142253252473779 0.00035324688576565677
		 9.7699626167013776e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.33738420001756403 0.94136703871471317 1
		 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 133.39026600533569 -0.00016045380448304059
		 6.7758500000000108 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1.6625066671020599e-08 1.0783848287061418e-09 0.064728963164722791 0.99790288171125141 1
		 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 136.3837927795395 0.00026039747172035277
		 6.467299999991293 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 2.3561302636965301e-08 -1.1330011759807413e-10 -0.0048086818200618687 0.99998843822273897 1
		 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 24.166379467191064 -0.58893161548871831
		 5.5118649200380787e-08 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.0099718512964513048 0.99995027985481366 -3.6497097955208862e-08 -2.7799042162765322e-10 1
		 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 -20.805012713223448 -5.1045709597019595e-05
		 -3.7125857943465235e-13 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1.4900032540592828e-08 -1.2969647768332587e-10 -0.0087041128421622375 0.99996211849231098 1
		 1 1 yes;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 -10.655821174937159 -0.00029624550427342911
		 1.3879812854611373e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.4853658540545987 -0.51421784045877439 0.49480357133531028 0.50514297586484058 1
		 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 -0.010909246962794971 -0.0018276127172699244
		 9.9690408868146323e-06 0 -46.894398495393652 13.528202567486003 35.327424579458587 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 4.0636235075843583e-31 1.7469556380552789e-32
		 1.6621664668699601e-15 0 -246.84990221864285 -58.639166378603136 4.9737991503207013e-14 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.042950424723212977 0.99907720473249495 2.6299550079651057e-18 6.1175835043836742e-17 1
		 1 1 yes;
	setAttr ".xm[25]" -type "matrix" "xform" 1 1 1 8.5001450322863572e-17 8.5028555377175685e-17
		 3.3306690738754701e-16 0 213.13709907530944 2.4868995751603507e-14 -1.4210854715202004e-14 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.77969273705926923 -0.6261623078539823 -4.6652727797633084e-09 5.8091636260386334e-09 1
		 1 1 yes;
	setAttr ".xm[26]" -type "matrix" "xform" 1 1 1 0 0 0 0 160.30064856315661 -4.6185277964332406e-14
		 6.8137435467150344e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.7520793032234071 0.65907262244990461 1
		 1 1 yes;
	setAttr ".xm[27]" -type "matrix" "xform" 1 1 1 -0.010907795108166413 -0.0018273699077116271
		 9.9663898602900462e-06 0 -46.89447543219984 13.528202282289456 -35.32739999999999 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -1 6.123233995736766e-17 1 1 1 yes;
	setAttr ".xm[28]" -type "matrix" "xform" 1 1 1 9.8663574979255608e-30 7.5603455405647432e-22
		 -5.1244507821963882e-14 0 246.84969702424189 58.639145268860155 2.1316282072803006e-14 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.042950424723213261 -0.99907720473249495 -7.4437095125390171e-09 3.2000578490365688e-10 1
		 1 1 yes;
	setAttr ".xm[29]" -type "matrix" "xform" 1 1 1 -1.3118846287074606e-16 -1.7347234759768075e-18
		 1.1378785315959747e-34 0 -213.1372117529973 1.289528966097464e-05 2.2026824808563106e-13 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0.77969273705926945 -0.62616230785398197 -3.6583152507461955e-09 4.5553074579907894e-09 1
		 1 1 yes;
	setAttr ".xm[30]" -type "matrix" "xform" 1 1 1 0 0 0 0 -160.30102741125862 -9.6428994432073978e-05
		 -2.3323694335886103e-06 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.75207930322340699 0.65907262244990483 1
		 1 1 yes;
	setAttr ".xm[31]" -type "matrix" "xform" 1 1 1 0 0 0 0 60.176757094367872 8.8817841970012523e-14
		 -1.2009154515051669e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 -0.70053003361525412 0.71362292003761407 1
		 1 1 yes;
	setAttr ".xm[32]" -type "matrix" "xform" 1 1 1 0 0 0 0 10.656170497062567 -1.2129186544029835e-13
		 -6.5800582896441245e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48536584844035074 -0.51421784602226595 0.49480356555609434 0.50514298125676826 1
		 1 1 yes;
	setAttr ".xm[33]" -type "matrix" "xform" 1 1 1 0 0 0 0 -10.655821174937159 -0.00029624550427342911
		 1.3879812854611373e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.4853658540545987 -0.51421784045877439 0.49480357133531028 0.50514297586484058 1
		 1 1 yes;
	setAttr ".xm[34]" -type "matrix" "xform" 1 1 1 0 0 0 0 160.30064856315661 -4.6185277964332406e-14
		 6.8137435467150344e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.7520793032234071 0.65907262244990461 1
		 1 1 yes;
	setAttr ".xm[35]" -type "matrix" "xform" 1 1 1 0 0 0 0 -160.30102741125862 -9.6428994432073978e-05
		 -2.3323694335886103e-06 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.75207930322340699 0.65907262244990483 1
		 1 1 yes;
	setAttr ".xm[36]" -type "matrix" "xform" 1 1 1 0 0 0 0 10.656170497062567 -1.2129186544029835e-13
		 -6.5800582896441245e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48536584844035074 -0.51421784602226595 0.49480356555609434 0.50514298125676826 1
		 1 1 yes;
	setAttr ".xm[37]" -type "matrix" "xform" 1 1 1 0 0 0 0 -10.655821174937159 -0.00029624550427342911
		 1.3879812854611373e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.4853658540545987 -0.51421784045877439 0.49480357133531028 0.50514297586484058 1
		 1 1 yes;
	setAttr ".xm[38]" -type "matrix" "xform" 1 1 1 0 0 0 0 160.30064856315664 0
		 2.8421709430404007e-14 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.7520793032234071 0.65907262244990461 1
		 1 1 yes;
	setAttr ".xm[39]" -type "matrix" "xform" 1 1 1 0 0 0 0 -160.30102741125856 -9.6428994531549961e-05
		 -2.3323695117483112e-06 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0.75207930322340699 0.65907262244990483 1
		 1 1 yes;
	setAttr ".xm[40]" -type "matrix" "xform" 1 1 1 0 0 0 0 10.656170497062567 -1.2129186544029835e-13
		 -6.5800582896441245e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48536584844035074 -0.51421784602226595 0.49480356555609434 0.50514298125676826 1
		 1 1 yes;
	setAttr ".xm[41]" -type "matrix" "xform" 1 1 1 0 0 0 0 -10.655821174937159 -0.00029624550427342911
		 1.3879812854611373e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.4853658540545987 -0.51421784045877439 0.49480357133531028 0.50514297586484058 1
		 1 1 yes;
	setAttr ".xm[42]" -type "matrix" "xform" 1 1 1 0 0 0 0 10.656170497062567 -1.2129186544029835e-13
		 -6.5800582896441245e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48536584844035074 -0.51421784602226595 0.49480356555609434 0.50514298125676826 1
		 1 1 yes;
	setAttr ".xm[43]" -type "matrix" "xform" 1 1 1 0 0 0 0 -10.655821174937159 -0.00029624550427342911
		 1.3879812854611373e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.4853658540545987 -0.51421784045877439 0.49480357133531028 0.50514297586484058 1
		 1 1 yes;
	setAttr ".xm[44]" -type "matrix" "xform" 1 1 1 0 0 0 0 10.656170497062567 -1.2129186544029835e-13
		 -6.5800582896441245e-15 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.48536584844035074 -0.51421784602226595 0.49480356555609434 0.50514298125676826 1
		 1 1 yes;
	setAttr ".xm[45]" -type "matrix" "xform" 1 1 1 0 0 0 0 -10.655821174937159 -0.00029624550427342911
		 1.3879812854611373e-09 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 -0.4853658540545987 -0.51421784045877439 0.49480357133531028 0.50514297586484058 1
		 1 1 yes;
	setAttr -s 31 ".m";
	setAttr -s 31 ".p";
	setAttr -s 46 ".g[12:45]" yes yes yes no no no no yes yes yes no no 
		yes yes no no yes yes no no no no no no no no no no no no no no no no;
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster2";
	rename -uid "880991F4-443E-0ABF-3AF8-308F3BE892CB";
	setAttr -s 66 ".wl";
	setAttr ".wl[0:65].w"
		5 5 0.048909129113872719 6 0.3104167288152086 7 0.42109994607897544 
		8 0.19615388224744573 9 0.023420313744497552
		5 5 0.0067730797106290899 6 0.040322748613393203 7 0.47457283082699447 
		8 0.47457283082699425 9 0.0037585100219890057
		5 4 0.098190911413113327 5 0.19900017264708161 6 0.050583789666466719 
		9 0.33565362687178113 10 0.31657149940155715
		5 5 0.019485207652966383 6 0.073122419913722606 7 0.44650976104514511 
		8 0.446509761045145 9 0.014372850343020839
		5 4 0.04840014519516387 5 0.33071465669380773 6 0.075762786284933423 
		9 0.35538337991026708 10 0.18973903191582786
		5 5 0.053375553183546483 6 0.30788821340577599 7 0.4015209042864194 
		8 0.20052031213426719 9 0.03669501698999094
		5 4 0.074967076590385706 5 0.18206784949708618 6 0.039615851998489764 
		9 0.36597781458387363 10 0.33737140733016474
		5 5 0.021019618831408318 6 0.074504849877623347 7 0.44460397876472535 
		8 0.44460397876472513 9 0.015267573761517847
		5 4 0.23278497433362727 5 0.27871899647104248 6 0.11181242788845988 
		9 0.22081369527553413 10 0.15586990603133632
		5 4 0.29436904938292446 5 0.28928267496990712 6 0.13605197099925384 
		9 0.14014803771273532 16 0.14014826693517929
		5 5 0.0081486444615932116 6 0.047335490430515499 7 0.46960660647835878 
		8 0.46960660647835878 9 0.0053026521511736679
		5 4 0.056889974580336782 5 0.3231580985022135 6 0.077339280344720651 
		9 0.34365670034423995 10 0.19895594622848903
		5 5 0.016941532891068176 6 0.066615860024117332 7 0.45297079629011028 
		8 0.4529707962901105 16 0.010501014504593686
		5 5 0.017736048215045482 6 0.068843012121238653 7 0.45078604093228886 
		8 0.45078604093228863 9 0.011848857799138489
		5 5 0.041632442432949883 6 0.32158385932252798 7 0.4188375104435495 
		8 0.18745536342041716 9 0.03049082438055551
		5 4 0.30013641775420852 5 0.30520400633414091 6 0.14354290935396816 
		9 0.12555821374047976 16 0.12555845281720271
		5 4 0.2291720543251286 5 0.29135190804930211 6 0.11521960282112116 
		9 0.21922216369818387 10 0.14503427110626413
		5 5 0.019147054365965453 6 0.069079909184052268 7 0.44964374825595765 
		8 0.44964374825595765 9 0.012485539938067088
		5 5 0.018473512549904769 6 0.067200761535007605 7 0.45158056040421035 
		8 0.45158056040421035 16 0.011164605106666892
		5 5 0.0069702278464723946 6 0.044734962392595438 7 0.47182314197571157 
		8 0.47182314197571157 9 0.0046485258095089995
		5 4 0.1058691066397531 5 0.3001700821186819 6 0.19406278955209316 
		9 0.30017008211868168 10 0.099727939570790222
		5 5 0.0060125756498480929 6 0.036581733315329858 7 0.47721953247968757 
		8 0.47721953247968757 16 0.0029666260754469113
		5 5 0.042527746173854394 6 0.31243239621711422 7 0.44313368133836795 
		8 0.18767818374485851 16 0.014227992525804815
		5 4 0.1399507573944056 5 0.29945180203332039 6 0.29001946652896504 
		9 0.13528886044795865 16 0.13528911359535034
		5 5 0.0051174433657225037 6 0.03574206666585384 7 0.47825020302229337 
		8 0.47825020302229337 16 0.0026400839238369502
		5 5 0.028773438370041989 6 0.33074889421426146 7 0.46511547942550441 
		8 0.16450086211520257 16 0.010861325874989693
		5 4 0.16482959085671606 5 0.26618903751255163 6 0.2489838069772265 
		9 0.15999865075628419 16 0.1599989138972216
		5 5 0.0059345113728642308 6 0.040149784739012338 7 0.47524040759776937 
		8 0.47524040759776937 9 0.0034348886925847908
		5 5 0.035481526898669687 6 0.32850406464003723 7 0.43871374902241111 
		8 0.17838411841880819 9 0.018916541020073778
		5 4 0.11965239476395126 5 0.2974759319706296 6 0.17202220181129579 
		9 0.2974759319706296 10 0.11337353948349367
		5 4 5.2024803309635603e-06 5 3.8578830354356443e-05 6 2.0815266946588866e-06 
		9 0.91531456323232729 10 0.0846395739302927
		5 5 0.019132661009037925 6 0.070893969796747794 7 0.44776300662797808 
		8 0.44776300662797797 9 0.014447355938258341
		5 5 0.01076764591061322 6 0.04559421522274211 7 0.46836091360111215 
		8 0.46836091360111204 9 0.0069163116644205052
		5 5 0.0097032325697864949 6 0.041929685598282945 7 0.47129788795717831 
		8 0.4712978879571782 16 0.0057713059175740168
		5 3 0.031500423917032491 4 0.77393112800729935 5 0.10992448063646244 
		9 0.04288466357097627 10 0.041759303868229486
		5 3 1.509502924924191e-07 4 0.99999891385810746 5 8.57926657904797e-07 
		9 3.8632400434117435e-08 16 3.8632541740540327e-08
		5 4 0.0079221467201832721 5 0.34057801238142521 6 0.013189545094863192 
		9 0.52467950172918187 10 0.11363079407434638
		5 5 0.038144669484590187 6 0.31701713336377391 7 0.43491175214247896 
		8 0.18057367587547565 9 0.029352769133681234
		5 5 0.0060778903700304384 6 0.038935601792391278 7 0.4754233610427086 
		8 0.47542336104270883 9 0.0041397857521606766
		5 5 0.048909130017122708 6 0.31041666977514265 7 0.42109991676169284 
		8 0.19615395687825615 16 0.023420326567785615
		5 5 0.0067730797662957486 6 0.040322746600317379 7 0.47457283155107116 
		8 0.47457283155107105 16 0.0037585105312447037
		5 4 0.098190713914439426 5 0.19899997105564213 6 0.050583738030656973 
		16 0.33565378962138526 17 0.31657178737787617
		5 5 0.019485206286283998 6 0.073122413286621307 7 0.44650976605330278 
		8 0.44650976605330278 16 0.014372848320489101
		5 4 0.048400149056254338 5 0.33071447228692347 6 0.075762807918687849 
		16 0.35538329713646677 17 0.18973927360166756
		5 5 0.053375552796923985 6 0.30788811456321619 7 0.40152086337872506 
		8 0.20052044118404555 16 0.036695028077089215
		5 4 0.074966889741998124 5 0.18206757224011347 6 0.039615794802931509 
		16 0.36597798433717338 17 0.33737175887778353
		5 5 0.021019617253273414 6 0.07450484321470148 7 0.44460398398563028 
		8 0.44460398398563028 16 0.015267571560764595
		5 4 0.23278468074360109 5 0.27871884904877003 6 0.11181235086646822 
		16 0.22081393441629246 17 0.15587018492486815
		5 5 0.008148644374339431 6 0.047335484164358538 7 0.46960660957408895 
		8 0.46960660957408895 16 0.0053026523131240464
		5 4 0.056889969906730516 5 0.32315794200616915 6 0.077339296537417612 
		16 0.34365663270351499 17 0.19895615884616782
		5 5 0.017736047705166388 6 0.068843009411795289 7 0.45078604273425027 
		8 0.45078604273425016 16 0.011848857414537915
		5 5 0.041632448287875899 6 0.32158374981941834 7 0.41883746055523458 
		8 0.18745550407553829 16 0.030490837261932816
		5 4 0.22917172597743979 5 0.29135172886522998 6 0.11521950967169915 
		16 0.21922245747149874 17 0.14503457801413247
		5 5 0.019147053764685933 6 0.069079906518615911 7 0.44964375010189694 
		8 0.44964375010189672 16 0.012485539512904582
		5 5 0.0069702279465772227 6 0.044734956659103847 7 0.47182314467270092 
		8 0.47182314467270092 16 0.0046485260489170606
		5 4 0.10586898978255835 5 0.30017018304989018 6 0.194062585718803 
		16 0.30017018304989018 17 0.099728058398858357
		5 5 0.0059345114827690157 6 0.040149782684991135 7 0.475240408326981 
		8 0.475240408326981 16 0.0034348891782778846
		5 5 0.035481531574374357 6 0.32850400032892618 7 0.43871371566318995 
		8 0.17838419957085808 16 0.018916552862651446
		5 4 0.11965227213233916 5 0.29747600820309178 6 0.17202205344963761 
		16 0.29747600820309178 17 0.1133736580118397
		5 4 5.2021638572868795e-06 5 3.857647930766185e-05 6 2.0814039810324067e-06 
		16 0.91531602684826874 17 0.084638113104585286
		5 5 0.019132659424940093 6 0.07089396230848545 7 0.44776301245291622 
		8 0.44776301245291611 16 0.014447353360742219
		5 5 0.010767645757981338 6 0.045594213881397394 7 0.46836091439666244 
		8 0.46836091439666233 16 0.0069163115672964525
		5 3 0.031500588276184543 4 0.77392932652439084 5 0.10992498760148148 
		16 0.042885227751060337 17 0.041759869846882847
		5 4 0.0079221924933723931 5 0.34057773518405676 6 0.013189624155230036 
		16 0.52467898144844138 17 0.11363146671889927
		5 5 0.038144678362899836 6 0.31701700333477484 7 0.43491168548152637 
		8 0.18057385024017719 16 0.029352782580621749
		5 5 0.0060778906343351528 6 0.038935597052012577 7 0.47542336316409606 
		8 0.47542336316409606 16 0.004139785985460236;
	setAttr -s 31 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.15680631874941806 0.98762937299386533 1.9203235634255452e-17 -0
		 0.98762937299386533 0.15680631874941817 -1.2094971503808443e-16 0 -1.2246467991473527e-16 -3.0814879110195767e-33 -1 0
		 -556.54303159690778 -88.362564335046656 6.815686422329173e-14 1;
	setAttr ".pm[1]" -type "matrix" 0.062080978492094471 0.9980711157575215 -2.6805962794083368e-17 -0
		 -0.99807111575752139 0.06208097849209461 2.431781747614721e-16 0 2.4437325261173941e-16 1.165751815773164e-17 1 -0
		 560.29701578098332 -43.035829167130757 -1.3702319106699251e-13 1;
	setAttr ".pm[2]" -type "matrix" -0.15680631874941806 0.98762937299386544 4.6009198428338808e-17 -0
		 0.98762937299386522 0.15680631874941817 -3.6412788979955656e-16 0 -3.6683793252647465e-16 -1.1657518157731646e-17 -1 0
		 -494.44900661422514 -90.513875048975763 1.909852228003692e-13 1;
	setAttr ".pm[3]" -type "matrix" -0.18411737904022696 -0.98290426326034253 -1.3957429162126204e-16 0
		 0.98290426326034275 -0.18411737904022704 9.8401839147183031e-17 0 -1.2241763996512782e-16 -1.1907067755956352e-16 1 -0
		 -553.24324881388691 105.35215275458967 -5.5307772665672655e-14 1;
	setAttr ".pm[4]" -type "matrix" 0.14368067469030996 -0.9896241022330331 -1.3957429162126204e-16 -0
		 0.98962410223303332 0.14368067469030998 9.8401839147183031e-17 0 -7.7326703334545362e-17 -1.5226452567993771e-16 1 -0
		 -612.77294890466533 -98.111138247060367 -5.5505734429841339e-14 1;
	setAttr ".pm[5]" -type "matrix" 0.03918556669535149 -0.99923195073154225 -1.3957429162126201e-16 -0
		 0.99923195073154247 0.03918556669535149 9.8401839147183043e-17 0 -9.2856963973329775e-17 -1.4332302351953412e-16 1 -0
		 -716.30416625084229 -23.176268293953683 -5.1970899344200789e-14 1;
	setAttr ".pm[6]" -type "matrix" -0.094809092627995709 -0.99549547259395166 -1.3957429162126201e-16 0
		 0.99549547259395188 -0.094809092627995736 9.8401839147183018e-17 0 -1.1119149730874616e-16 -1.2961618631700397e-16 1 -0
		 -769.3250427595915 80.440057302969578 -5.1339961442449999e-14 1;
	setAttr ".pm[7]" -type "matrix" 0.01851534400602068 -0.99982857632513089 -1.3957429162126204e-16 -0
		 0.99982857632513111 0.01851534400602068 9.8401839147183031e-17 0 -9.580070471853827e-17 -1.4137230918591036e-16 1 -0
		 -831.86760311193916 -13.836949993469942 -5.2594911904574821e-14 1;
	setAttr ".pm[8]" -type "matrix" 0.99999999999999956 -1.1553258350005527e-15 -1.3957429162126204e-16 -0
		 1.1587952819525064e-15 0.99999999999999978 9.8401839147183031e-17 -0 1.3957429162126194e-16 -9.8401839147183203e-17 1 -0
		 -2.681930185198262 -892.14763857305729 -5.1393996453069635e-14 1;
	setAttr ".pm[9]" -type "matrix" -1.5766239482585878e-15 2.4677145545861107e-15 0.99999999999999933 -0
		 0.73593101176182885 -0.67705653082088391 2.8513938317956838e-15 -0 0.67705653082088413 0.73593101176182896 -6.6920065569024372e-16 -0
		 -522.48680778374171 542.33987591893447 -4.9103200000020788 1;
	setAttr ".pm[10]" -type "matrix" 3.4980822239674598e-16 2.9074030344419489e-15 0.99999999999999956 -0
		 0.13832224525180573 -0.99038727600292753 2.8513938317956845e-15 0 0.99038727600292775 0.13832224525180556 -6.6920065569024381e-16 -0
		 -20.315310508424602 718.90861069118 -4.9103200000020051 1;
	setAttr ".pm[11]" -type "matrix" 7.2247398346767108e-16 2.8378494569569721e-15 0.99999999999999956 -0
		 0.0092185452926791221 -0.99995750830857133 2.8513938317956842e-15 0 0.99995750830857155 0.0092185452926789278 -6.6920065569024391e-16 -0
		 205.00054560892085 698.27647776334459 1.8655299999980184 1;
	setAttr ".pm[12]" -type "matrix" 6.9514825669555819e-16 2.8446664300672847e-15 0.99999999999999956 -0
		 0.018834962757058496 -0.9998226063547172 2.8513938317956842e-15 0 0.99982260635471742 0.018834962757058309 -6.6920065569024381e-16 -0
		 334.65304677480185 701.52762421506532 8.3328299999980118 1;
	setAttr ".pm[13]" -type "matrix" -4.3011583918344602e-16 2.8302375544423468e-15 -0.99999999999999956 -0
		 0.0011079563370642758 -0.99999938621618911 -2.8510374858595874e-15 0 -0.99999938621618922 -0.0011079563370644666 3.4757632316284618e-16 0
		 -372.72626894604508 693.64346131545778 -8.3328299999981468 1;
	setAttr ".pm[14]" -type "matrix" -4.7931821436030371e-16 2.8223214379433684e-15 -0.99999999999999956 -0
		 0.018515344006025149 -0.99982857632513078 -2.851037485859587e-15 0 -0.99982857632513089 -0.01851534400602534 3.4757632316284609e-16 0
		 -405.54658103968353 686.68793147065594 -8.3328299999981397 1;
	setAttr ".pm[15]" -type "matrix" -0.039185566695350713 0.99923195073154225 8.8199496530749458e-16 -0
		 -0.99923195073154247 -0.039185566695350692 -3.7199933019456096e-16 0 -5.3753042576038589e-16 -8.1335366304822532e-16 0.99999999999999978 -0
		 693.41658891137172 35.532042351583996 403.41714125562231 1;
	setAttr ".pm[16]" -type "matrix" 2.1246014704267292e-15 -2.4460094339290237e-15 -0.99999999999999956 -0
		 -0.73593101176182851 0.67705653082088435 -3.2575685969753614e-15 0 0.67705653082088457 0.73593101176182874 -5.6995409058207253e-16 -0
		 522.48682682667959 -542.33985522008913 4.9103200000024678 1;
	setAttr ".pm[17]" -type "matrix" 8.7206016437896295e-17 -3.2387171333270584e-15 -0.99999999999999956 -0
		 -0.13832224525180481 0.99038727600292775 -3.2575685969753618e-15 0 0.99038727600292797 0.13832224525180481 -5.6995409058207233e-16 -0
		 20.315338364123779 -718.90860680071933 4.910320000002371 1;
	setAttr ".pm[18]" -type "matrix" -3.3192307148995568e-16 -3.3320012565859256e-08 -0.99999999999999911 0
		 -0.0092185452926782426 0.99995750830857089 -3.3318596777028903e-08 0 0.99995750830857166 0.0092185452926782045 -3.0716258523514159e-10 -0
		 -205.00061747979939 -698.27647848807669 -1.8655067334188549 1;
	setAttr ".pm[19]" -type "matrix" 3.2044664045228539e-10 -8.0441621725000886e-08 -0.99999999999999634 -0
		 -0.018834962757057604 0.99982260635471398 -8.043338753095104e-08 0 0.99982260635471742 0.018834962757057531 -1.1947253623882827e-09 -0
		 -334.65311863331425 -701.52762623911212 -8.332773675218526 1;
	setAttr ".pm[20]" -type "matrix" 6.3520487975324653e-16 -7.4469919509060005e-09 0.99999999999999933 -0
		 -0.0011079563370652119 0.99999938621618911 7.4469874186885004e-09 0 -0.99999938621618922 -0.0011079563370652496 -8.2500983843974439e-12 -0
		 372.72675544167942 -693.6434608384958 8.332824834442464 1;
	setAttr ".pm[21]" -type "matrix" 1.2963464074173682e-10 2.235533044725487e-08 0.99999999999999911 -0
		 -0.018515344006026085 0.99982857632513056 -2.2349097946466464e-08 0 -0.999828576325131 -0.018515344006026113 5.4352926022973773e-10 -0
		 405.54678257662613 -686.68787618304839 8.3328452985614483 1;
	setAttr ".pm[22]" -type "matrix" 0.039185566695350012 -0.99923195073154192 -2.0556205081424268e-16 -0
		 0.99923195073154225 0.039185566695350046 9.857608915558894e-16 0 -1.0956517615357539e-15 -5.2602619318358425e-16 0.99999999999999956 -0
		 -693.41623516036577 -35.532028478995038 -403.41700000000048 1;
	setAttr ".pm[23]" -type "matrix" -0.15679621116079887 0.98756904080779062 0.011060642097021305 -0
		 0.98762928672710615 0.15680683367561513 -9.4400928281942299e-05 0 -0.0018276116998478893 0.010909012357140287 -0.99993882477122875 0
		 -509.71336248634782 -101.50439727101816 -35.505351246728459 1;
	setAttr ".pm[24]" -type "matrix" 0.071462979010819633 0.99738192525596758 -0.011060642097021314 -0
		 -0.99744286054030218 0.071468391934084219 9.4400928282064163e-05 0 0.00088464008402846638 0.011025612321109005 0.99993882477122886 -0
		 265.5723931882938 -20.147722938677138 35.505351246728452 1;
	setAttr ".pm[25]" -type "matrix" -0.95844736613851533 -0.28505492213345057 0.011060637849371937 -0
		 -0.28507335966558345 0.95850569674892272 -9.4386645434053941e-05 0 -0.010574779010435114 -0.0032435578234796905 -0.99993882481956176 0
		 30.99052304575579 -46.850594874609897 -35.505351944856777 1;
	setAttr ".pm[26]" -type "matrix" -0.15679621132418931 0.9875690408294221 0.011060637849371939 -0
		 0.98762928672850037 0.15680683367543038 -9.4386645434053914e-05 0 -0.0018275969284890505 0.010909010401533805 -0.99993882481956176 0
		 -29.473817142537658 134.3405408076022 -35.505351944856784 1;
	setAttr ".pm[27]" -type "matrix" 0.15679621384860573 -0.98756905686476415 -0.011059170221222995 -0
		 -0.98762928674967854 -0.15680683354076405 9.4388768125691611e-05 0 -0.001827368890694805 0.010907560596157814 -0.99993884105218078 -0
		 509.71327688297606 101.50444880272651 35.505303207731473 1;
	setAttr ".pm[28]" -type "matrix" -0.07146298031067444 -0.99738194164916549 0.011059155359065369 -0
		 0.99744286055121789 -0.071468391796386407 -9.438983308832972e-05 0 0.00088452276313636888 0.011024130177842983 0.99993884121645304 -0
		 -265.57251857229869 20.147785577698688 -35.505302907505857 1;
	setAttr ".pm[29]" -type "matrix" 0.95844738186473677 0.28505492690540796 -0.011059152028233177 -0
		 0.28507335953348817 -0.95850569678899833 9.4378633065701339e-05 0 -0.010573357126367141 -0.0032431265760458181 -0.99993884125434873 0
		 -30.990574359499234 46.850596658850257 35.505303454949555 1;
	setAttr ".pm[30]" -type "matrix" 0.15679621399085625 -0.98756905704591091 -0.011059152028233179 -0
		 -0.98762928675089245 -0.15680683353921848 9.4378633065700878e-05 0 -0.0018273560288380483 0.010907544217367292 -0.99993884125434895 -0
		 29.473871518447488 -134.34087839885791 35.505305787318981 1;
	setAttr ".gm" -type "matrix" 100 0 0 0 -0 -1.6292067961387602e-05 -99.999999999998678 0
		 0 99.999999999998678 -1.6292067961387602e-05 0 3.6557316780090332 909.06689453125 0 1;
	setAttr -s 10 ".ma";
	setAttr -s 31 ".dpf[0:30]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 10 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 10 ".ifcl";
createNode skinCluster -n "skinCluster3";
	rename -uid "E7694E93-4DB1-007A-60D7-21A3A14DC4D6";
	setAttr -s 148 ".wl";
	setAttr ".wl[0:118].w"
		5 24 0.08554786991300331 25 0.91406643781510033 26 1.4895576795195547e-05 
		28 0.00018014857586135088 29 0.00019064811923982054
		5 0 0.41518382807455884 1 0.10743327011848844 2 0.066816712821150925 
		3 0.07156370146709351 23 0.33900248751870843
		5 24 0.54389761496706346 25 0.45598394935136499 26 2.6945263064102792e-06 
		28 5.7922239483502126e-05 29 5.7818915781581862e-05
		5 0 0.47581906976108113 1 0.022569703649959565 2 0.01590415658004336 
		3 0.0098880002478347346 23 0.47581906976108113
		5 0 0.37971631773549841 1 0.33087326091787117 2 0.096703962611947178 
		3 0.1126116811237399 23 0.080094777610943399
		5 24 0.52411455815240682 25 0.4749901574547834 26 8.4076210659321041e-06 
		28 0.00044406380395011608 29 0.0004428129677938069
		5 24 0.15151787566404223 25 0.8462850407432948 26 3.6074175914562276e-05 
		28 0.0010321006831878639 29 0.0011289087335606318
		5 0 0.40968194254252177 1 0.18950224059386475 2 0.080614277593951955 
		3 0.032250393419796158 23 0.28795114584986542
		5 23 0.0018396743417982691 24 0.99790953032582708 25 6.8633071023649362e-06 
		27 9.2880262540773144e-05 28 0.00015105176273170644
		5 23 0.063804350084866582 24 0.9213882420043793 25 0.00048579890065163895 
		27 0.0054506797821604292 28 0.008870929227942085
		2 24 0.99933335334386841 25 0.00066664665613152358
		2 24 0.99994383092238315 25 5.6169077616945772e-05
		4 0 0.0091987454091793895 1 0.010651914517028552 2 0.010651914517028552 
		23 0.96949742555676355
		5 0 0.0033095125578067493 1 0.0035082046806717043 2 0.0035082046806717043 
		23 0.98186917791867545 27 0.0078049001621744657
		4 0 0.00017320361460610242 1 0.00027035561595291732 2 0.00027035561595291732 
		23 0.99928608515348816
		4 0 0.0033689942515927715 1 0.0056424732714800228 2 0.0056424732714800228 
		23 0.98534605920544716
		2 24 0.99998830652477633 25 1.1693475223756861e-05
		5 23 0.0032688720714996694 24 0.98566391815622678 25 0.0016310556251078214 
		27 0.0013125041973909414 28 0.0081236499497749064
		2 24 0.99687107890756854 25 0.0031289210924314126
		2 24 0.99954905661909743 25 0.00045094338090255525
		5 0 0.24969091153694115 1 0.10245698957246958 2 0.10245698957246958 
		3 0.025379280266906272 23 0.52001582905121346
		5 0 0.2253577189418243 1 0.028145538462385381 2 0.028145538462385381 
		23 0.7127797233170905 27 0.0055714808163144511
		4 0 0.13559129956279645 1 0.14499080673731227 2 0.14499080673731227 
		23 0.57442708696257905
		4 0 0.11082977469691579 1 0.32177722852715784 2 0.32177722852715784 
		23 0.24561576824876846
		5 24 0.44980195920969013 25 0.54983578669057953 26 1.2596920358377125e-05 
		28 0.00017470207545409433 29 0.00017495510391781906
		5 0 0.47099201453933909 1 0.031149305076724539 2 0.019607929732138676 
		3 0.016883154489992436 23 0.46136759616180523
		5 24 0.47257210575942377 25 0.52388594762747032 26 3.2434533502467811e-05 
		28 0.0017513350190767542 29 0.0017581770605267113
		5 0 0.21927132741024327 1 0.74200087985043328 2 0.01707478915805891 
		3 0.0098152584693319746 23 0.01183774511193262
		2 24 0.99972756585985323 25 0.00027243414014686346
		5 23 0.019703313684543113 24 0.97746036305150763 25 0.00010118449126583622 
		27 0.0010666387242924999 28 0.0016685000483908647
		4 0 0.00024028489794756223 1 0.00042049185870495404 2 0.00042049185870495404 
		23 0.99891873138464249
		4 0 0.0050694685365100653 1 0.0053528882538664254 2 0.0053528882538664254 
		23 0.98422475495575712
		5 23 0.00042820981132898276 24 0.99838462557497432 25 0.00019363844282184738 
		27 0.00014936705025543848 28 0.0008441591206194512
		2 24 0.99848121814891866 25 0.0015187818510812624
		4 0 0.029652672518963134 1 0.42114900285712603 2 0.42114900285712603 
		23 0.1280493217667848
		5 0 0.22001941205650774 1 0.039314006359277155 2 0.039314006359277155 
		3 0.0082697027213208766 23 0.69308287250361711
		5 0 0.39013769232963891 1 0.20317012861201089 2 0.10196309548375704 
		3 0.12453285723022876 23 0.18019622634436441
		5 24 0.52789928500375394 25 0.47182242857231521 26 3.8092960560277278e-06 
		28 0.00013735935011017955 29 0.00013711777776468997
		5 24 0.035602562501615309 25 0.9639704484209014 26 1.20890996351662e-05 
		28 0.00019569882358753469 29 0.00021920115426064154
		5 0 0.43394843982652948 1 0.068643294521380918 2 0.043208084453943162 
		3 0.021802153332582973 23 0.43239802786556347
		2 24 0.9992263086695532 25 0.00077369133044690808
		2 24 0.999994988835605 25 5.0111643950315885e-06
		4 0 0.00087083691468419866 1 0.0011127420502004555 2 0.0011127420502004555 
		23 0.99690367898491494
		4 0 0.0072953374692576352 1 0.010384779114581416 2 0.010384779114581416 
		23 0.97193510430157948
		2 24 0.99992288143592334 25 7.7118564076674139e-05
		2 24 0.99670983080380982 25 0.0032901691961901005
		4 0 0.20425102261141651 1 0.052605401748259555 2 0.052605401748259555 
		23 0.6905381738920644
		5 0 0.19015935272184978 1 0.19794047384080649 2 0.19794047384080649 
		3 0.033861450413923661 23 0.38009824918261365
		5 24 0.30878774196030079 25 0.69118010043440858 26 5.9518151409260198e-07 
		28 1.5762341142024066e-05 29 1.5800082634542043e-05
		4 23 0.55337933716112875 24 0.4461936906306046 25 8.7095153560566483e-06 
		28 0.00041826269291061417
		5 23 0.53350947257687709 24 0.45933415518598153 25 8.4091678458499984e-05 
		27 0.0035589780665685271 28 0.0035133024921143148
		4 23 0.53729588860907129 24 0.45152510797448331 25 8.0586532354350859e-05 
		28 0.01109841688409104
		4 23 0.55600970989354825 24 0.44258059295491903 25 7.9770960208778029e-06 
		28 0.0014017200555118505
		5 23 0.54699597558282964 24 0.45105086324314936 25 2.5266346928197587e-05 
		27 0.00096815492559991316 28 0.00095973990149298631
		4 23 0.54970203184534472 24 0.44472720957583989 25 2.3322882234948932e-05 
		28 0.005547435696580387
		4 23 0.55735711212907801 24 0.44218512367904494 25 5.3521221673842442e-06 
		28 0.00045241206970965257
		5 23 0.52741931587960789 24 0.4570712053669847 25 0.00011431939371908647 
		27 0.0077675963269070647 28 0.0076275630327812096
		4 23 0.49635198130212799 24 0.50361654288533664 25 5.5504808036134214e-07 
		28 3.0920764454999474e-05
		5 23 0.49380605676694161 24 0.50281960861771913 25 3.3636031957349833e-05 
		27 0.0016694201089233333 28 0.0016712784744585798
		4 23 0.49576125025614182 24 0.49980645220802006 25 3.8629878923421286e-05 
		28 0.0043936676569147322
		4 23 0.49758796220411794 24 0.50222350580051578 25 1.3891837180133742e-06 
		28 0.00018714281164817614
		5 23 0.49501010014103686 24 0.50430270570287683 25 7.4028640016620444e-06 
		27 0.00033980243951203939 28 0.00033998885257266004
		4 23 0.49610499701259325 24 0.5021102834911938 25 1.1118767503574717e-05 
		28 0.0017736007287093554
		4 23 0.49691084982288281 24 0.50308054070887698 25 1.0216681091032133e-07 
		28 8.5073014293405029e-06
		5 23 0.49214360269626067 24 0.50073266961032925 25 5.1023119280477405e-05 
		27 0.0035336557358006692 28 0.0035390488383289058
		5 0 0.66729957988751687 1 0.13654227631155785 2 0.0098373068457166899 
		3 0.15606001889780208 23 0.030260818057406599
		5 0 0.64114595429112708 1 0.1267728966070546 2 0.016203415788779995 
		3 0.1283241649595846 23 0.08755356835345357
		5 0 0.69931871628860509 1 0.093568106289616287 2 0.012642930080081091 
		3 0.077682576557901631 23 0.11678767078379583
		5 0 0.64596665019222044 1 0.095505191779229379 2 0.014612730371726113 
		3 0.058672195362350181 23 0.18524323229447404
		5 0 0.65055057590613885 1 0.15013696725045755 2 0.014752917886736123 
		3 0.072233720636909013 23 0.11232581831975852
		5 0 0.67676163683209201 1 0.20466386674433987 2 0.0084949381334906079 
		3 0.071434893617513054 23 0.038644664672564347
		5 0 0.98719672406461956 1 0.0089156856909337941 2 7.3181266641341293e-05 
		3 0.0036225362663579075 23 0.00019187271144730653
		5 0 0.80071033002040359 1 0.09377040511617693 2 0.002556792012353946 
		3 0.097053022360082772 23 0.0059094504909828196
		5 0 0.88624940450008338 1 0.058159198513010692 2 0.0025896289423775229 
		3 0.03960015742130199 23 0.013401610623226378
		3 28 0.085580899129847704 29 0.9144041996147777 30 1.4901255374571997e-05
		5 0 0.41518419873887841 1 0.10743368221834862 2 0.066816848130139411 
		3 0.071563929076024088 27 0.33900134183660946
		3 28 0.54395977512261906 29 0.45603752998303809 30 2.69489434286667e-06
		5 0 0.47581899310693954 1 0.022569792414865435 2 0.015904197652093461 
		3 0.0098880237191620798 27 0.47581899310693954
		5 0 0.37971573204157394 1 0.33087437058121916 2 0.096703987594823917 
		3 0.11261181317081292 27 0.08009409661157009
		3 28 0.52457995432091908 29 0.47541163087663585 30 8.4148024450138801e-06
		3 28 0.15184340303571198 29 0.84812044596930924 30 3.6150994978790389e-05
		5 0 0.40968137430549051 1 0.18950379656473121 2 0.080614779614955417 
		3 0.032250571797015147 27 0.28794947771780771
		5 23 9.2882102120866758e-05 24 0.00015105426518226132 27 0.0018396874077286386 
		28 0.9979095127896892 29 6.8634352790208209e-06
		4 23 0.0054994798093415016 27 0.064375002362552169 28 0.92963537007265751 
		29 0.00049014775544887958
		4 23 0.018935967247877442 27 0.082357789453140109 28 0.89801106518334661 
		29 0.00069517811563582651
		4 23 0.002066028010829838 27 0.013033517466890302 28 0.98484400881616596 
		29 5.6445706113748472e-05
		5 0 0.008994829905799067 1 0.010415771082961624 2 0.010415771082961624 
		23 0.02216943018431769 27 0.94800419774396005
		4 0 0.003335546061206617 1 0.0035357950404656994 2 0.0035357950404656994 
		27 0.98959286385786194
		4 0 0.00017320450266753001 1 0.00027035650027564228 2 0.00027035650027564228 
		27 0.99928608249678119
		4 0 0.0033690069837638719 1 0.0056424859790618386 2 0.0056424859790618386 
		27 0.98534602105811253
		5 23 1.0525057308078044e-05 24 6.8763331047604997e-05 27 2.8184237634513006e-05 
		28 0.99988083362507429 29 1.1693748935540698e-05
		4 23 0.0013232586507037534 27 0.0032956342587553331 28 0.99373669045343438 
		29 0.0016444166371065705
		4 23 0.0041151811496143529 27 0.006519192239830893 28 0.98605956729382771 
		29 0.003306059316726952
		4 23 0.00065235629354205415 27 0.0010754682810776659 28 0.99781638431485531 
		29 0.00045579111052495685
		5 0 0.24969136505297579 1 0.10245718066400085 2 0.10245718066400085 
		3 0.025379276855154242 27 0.52001499676386842
		4 0 0.22662093629442118 1 0.028303127689878013 2 0.028303127689878013 
		27 0.7167728083258228
		4 0 0.1355919446610076 1 0.14499181778182169 2 0.14499181778182169 
		27 0.57442441977534908
		4 0 0.11082964731056036 1 0.32177806665216591 2 0.32177806665216591 
		27 0.24561421938510791
		4 24 0.00017473642299532644 28 0.44988174147564758 29 0.54993092278517819 
		30 1.2599316178926115e-05
		5 0 0.47099237213850181 1 0.031149453252428932 2 0.019607982376725817 
		3 0.016883211093456498 27 0.46136698113888691
		3 28 0.47423557372654179 29 0.52573187851813896 30 3.2547755319138613e-05
		5 0 0.21926571153201363 1 0.74200727802859168 2 0.017074524436289585 
		3 0.0098151031616630444 27 0.011837382841442071
		4 23 0.011945335175647959 27 0.04574573884444233 28 0.94202748634551936 
		29 0.00028143963439038541
		4 23 0.0010684294939979329 27 0.019736173481312059 28 0.97909404275543577 
		29 0.00010135426925427439
		4 0 0.00024028697036856017 1 0.00042049474657045784 2 0.00042049474657045784 
		27 0.99891872353649047
		4 0 0.005069471808555541 1 0.0053528832998796768 2 0.0053528832998796768 
		27 0.98422476159168515
		4 23 0.00014949515533588581 27 0.00042857402571747506 28 0.9992281267320452 
		29 0.00019380408690145048
		4 23 0.00241493242910485 27 0.0034429560042721508 28 0.99254129798020896 
		29 0.0016008135864140943
		4 0 0.029652381213392254 1 0.42115045577983085 2 0.42115045577983062 
		27 0.12804670722694633
		5 0 0.22002002935745871 1 0.039313969337625099 2 0.039313969337625099 
		3 0.0082696734361531517 27 0.69308235853113798
		5 0 0.39013778405550348 1 0.20317075007357585 2 0.10196319693424921 
		3 0.12453313715712866 27 0.18019513177954288
		3 28 0.52804417935332593 29 0.47195201036564755 30 3.8102810265155019e-06
		3 28 0.035616695870795988 29 0.96437121038626816 30 1.209374293596458e-05
		5 0 0.43394809852653765 1 0.068643837072203603 2 0.043208367030496422 
		3 0.0218022846125327 27 0.43239741275822963
		4 23 0.012365261313863608 27 0.087940409755085144 28 0.89890324377134367 
		29 0.00079108515970758664
		4 23 0.00010633467666759398 27 0.0014134333810570958 28 0.99847521980959031 
		29 5.0121326850092823e-06
		4 0 0.00087083711740195476 1 0.0011127402792948356 2 0.0011127402792948356 
		27 0.99690368232400839
		5 0 0.0070537350219717432 1 0.010040848645405174 2 0.010040848645405174 
		23 0.033119851269365896 27 0.93974471641785196
		1 23 8.8074167016490072e-05;
	setAttr ".wl[118:147].w"
		3 27 0.0001897674927896569 28 0.99964497702953303 29 7.7181310660877155e-05
		4 23 0.0032615310655565214 27 0.0065010378624689481 28 0.98685890097780138 
		29 0.0033785300941731383
		4 0 0.20425187599736483 1 0.052605474469200712 2 0.052605474469200712 
		27 0.69053717506423373
		5 0 0.19015944098401208 1 0.19794102508793018 2 0.19794102508793018 
		3 0.033861465354964711 27 0.38009704348516288
		3 28 0.30879373658245779 29 0.69120566824337648 30 5.9517416562160087e-07
		4 23 0.00042113618064830406 27 0.55318713510696793 28 0.44638301553871285 
		29 8.7131736708895179e-06
		4 23 0.0035715257831106209 27 0.53538923761844492 28 0.46095484833584549 
		29 8.438826259909345e-05
		4 23 0.011488677402075674 27 0.53183598922802966 28 0.45659384268280373 
		29 8.1490687091054876e-05
		4 23 0.0014209354096888476 27 0.55536667571233267 28 0.44320440069138967 
		29 7.9881865888628268e-06
		4 23 0.00096908695949114135 27 0.54751954625675525 28 0.45148607607973634 
		29 2.5290704017246488e-05
		4 23 0.0057009458703332098 27 0.54706560001019167 28 0.44721000133695726 
		29 2.3452782517785953e-05
		4 23 0.00045586250146248706 27 0.55715058595130729 28 0.44238819704400728 
		29 5.3545032229053827e-06
		4 23 0.0078272828318411251 27 0.53147205961104349 28 0.46058545956848351 
		29 0.00011519798863188143
		4 23 3.0917854362957655e-05 27 0.49633500889179122 28 0.50363351817615032 
		29 5.5507769567430638e-07
		4 23 0.001672216403724464 27 0.4946319099754104 28 0.50366218119707484 
		29 3.36924237903362e-05
		4 23 0.0044056990210619958 27 0.49354256253593842 28 0.50201293819119774 
		29 3.880025180187275e-05
		4 23 0.00018713442458633941 27 0.49749299019681864 28 0.50231848595207929 
		29 1.3894265157160769e-06
		4 23 0.00033992008630139719 27 0.49517762715183605 28 0.50447504732322346 
		29 7.4054386391177964e-06
		4 23 0.0017749727672320044 27 0.49521073240267854 28 0.50300315638606807 
		29 1.113844402142544e-05
		4 23 8.5068210831453982e-06 27 0.49690452375079608 28 0.50308686725971541 
		29 1.0216840545216394e-07
		4 23 0.0035461997888291742 27 0.49389070822523978 28 0.50251188765725574 
		29 5.1204328675431272e-05
		5 0 0.66729970689207718 1 0.13654243119091863 2 0.0098372265553980411 
		3 0.15606038675464851 27 0.030260248606957502
		5 0 0.64114655884270799 1 0.12677328061572804 2 0.016203340662782909 
		3 0.12832460641181562 27 0.087552213466965387
		5 0 0.69931981701131329 1 0.093568556724133237 2 0.0126428901295342 
		3 0.077682897457461764 27 0.11678583867755758
		5 0 0.64596748402074056 1 0.095506055469991358 2 0.014612763074280824 
		3 0.058672613379510824 27 0.18524108405547651
		5 0 0.65055004135454086 1 0.15013839668026877 2 0.014752944418757193 
		3 0.072234194433235338 27 0.1123244231131979
		5 0 0.67675943404012795 1 0.20466616516047534 2 0.0084949408746337243 
		3 0.071435330031250577 27 0.038644129893512298
		5 0 0.98719709267793432 1 0.0089154593192807014 2 7.3177538046356866e-05 
		3 0.0036224100131533243 27 0.00019186045158534702
		5 0 0.80071057068914753 1 0.093770341912067581 2 0.002556755256904492 
		3 0.097053032601261552 27 0.0059092995406189617
		5 0 0.88624991881885273 1 0.058159197252096895 2 0.0025895926302736912 
		3 0.039600056734999181 27 0.013401234563777505;
	setAttr -s 31 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.15680631874941806 0.98762937299386533 1.9203235634255452e-17 -0
		 0.98762937299386533 0.15680631874941817 -1.2094971503808443e-16 0 -1.2246467991473527e-16 -3.0814879110195767e-33 -1 0
		 -556.54303159690778 -88.362564335046656 6.815686422329173e-14 1;
	setAttr ".pm[1]" -type "matrix" 0.062080978492094471 0.9980711157575215 -2.6805962794083368e-17 -0
		 -0.99807111575752139 0.06208097849209461 2.431781747614721e-16 0 2.4437325261173941e-16 1.165751815773164e-17 1 -0
		 560.29701578098332 -43.035829167130757 -1.3702319106699251e-13 1;
	setAttr ".pm[2]" -type "matrix" -0.15680631874941806 0.98762937299386544 4.6009198428338808e-17 -0
		 0.98762937299386522 0.15680631874941817 -3.6412788979955656e-16 0 -3.6683793252647465e-16 -1.1657518157731646e-17 -1 0
		 -494.44900661422514 -90.513875048975763 1.909852228003692e-13 1;
	setAttr ".pm[3]" -type "matrix" -0.18411737904022696 -0.98290426326034253 -1.3957429162126204e-16 0
		 0.98290426326034275 -0.18411737904022704 9.8401839147183031e-17 0 -1.2241763996512782e-16 -1.1907067755956352e-16 1 -0
		 -553.24324881388691 105.35215275458967 -5.5307772665672655e-14 1;
	setAttr ".pm[4]" -type "matrix" 0.14368067469030996 -0.9896241022330331 -1.3957429162126204e-16 -0
		 0.98962410223303332 0.14368067469030998 9.8401839147183031e-17 0 -7.7326703334545362e-17 -1.5226452567993771e-16 1 -0
		 -612.77294890466533 -98.111138247060367 -5.5505734429841339e-14 1;
	setAttr ".pm[5]" -type "matrix" 0.03918556669535149 -0.99923195073154225 -1.3957429162126201e-16 -0
		 0.99923195073154247 0.03918556669535149 9.8401839147183043e-17 0 -9.2856963973329775e-17 -1.4332302351953412e-16 1 -0
		 -716.30416625084229 -23.176268293953683 -5.1970899344200789e-14 1;
	setAttr ".pm[6]" -type "matrix" -0.094809092627995709 -0.99549547259395166 -1.3957429162126201e-16 0
		 0.99549547259395188 -0.094809092627995736 9.8401839147183018e-17 0 -1.1119149730874616e-16 -1.2961618631700397e-16 1 -0
		 -769.3250427595915 80.440057302969578 -5.1339961442449999e-14 1;
	setAttr ".pm[7]" -type "matrix" 0.01851534400602068 -0.99982857632513089 -1.3957429162126204e-16 -0
		 0.99982857632513111 0.01851534400602068 9.8401839147183031e-17 0 -9.580070471853827e-17 -1.4137230918591036e-16 1 -0
		 -831.86760311193916 -13.836949993469942 -5.2594911904574821e-14 1;
	setAttr ".pm[8]" -type "matrix" 0.99999999999999956 -1.1553258350005527e-15 -1.3957429162126204e-16 -0
		 1.1587952819525064e-15 0.99999999999999978 9.8401839147183031e-17 -0 1.3957429162126194e-16 -9.8401839147183203e-17 1 -0
		 -2.681930185198262 -892.14763857305729 -5.1393996453069635e-14 1;
	setAttr ".pm[9]" -type "matrix" -1.5766239482585878e-15 2.4677145545861107e-15 0.99999999999999933 -0
		 0.73593101176182885 -0.67705653082088391 2.8513938317956838e-15 -0 0.67705653082088413 0.73593101176182896 -6.6920065569024372e-16 -0
		 -522.48680778374171 542.33987591893447 -4.9103200000020788 1;
	setAttr ".pm[10]" -type "matrix" 3.4980822239674598e-16 2.9074030344419489e-15 0.99999999999999956 -0
		 0.13832224525180573 -0.99038727600292753 2.8513938317956845e-15 0 0.99038727600292775 0.13832224525180556 -6.6920065569024381e-16 -0
		 -20.315310508424602 718.90861069118 -4.9103200000020051 1;
	setAttr ".pm[11]" -type "matrix" 7.2247398346767108e-16 2.8378494569569721e-15 0.99999999999999956 -0
		 0.0092185452926791221 -0.99995750830857133 2.8513938317956842e-15 0 0.99995750830857155 0.0092185452926789278 -6.6920065569024391e-16 -0
		 205.00054560892085 698.27647776334459 1.8655299999980184 1;
	setAttr ".pm[12]" -type "matrix" 6.9514825669555819e-16 2.8446664300672847e-15 0.99999999999999956 -0
		 0.018834962757058496 -0.9998226063547172 2.8513938317956842e-15 0 0.99982260635471742 0.018834962757058309 -6.6920065569024381e-16 -0
		 334.65304677480185 701.52762421506532 8.3328299999980118 1;
	setAttr ".pm[13]" -type "matrix" -4.3011583918344602e-16 2.8302375544423468e-15 -0.99999999999999956 -0
		 0.0011079563370642758 -0.99999938621618911 -2.8510374858595874e-15 0 -0.99999938621618922 -0.0011079563370644666 3.4757632316284618e-16 0
		 -372.72626894604508 693.64346131545778 -8.3328299999981468 1;
	setAttr ".pm[14]" -type "matrix" -4.7931821436030371e-16 2.8223214379433684e-15 -0.99999999999999956 -0
		 0.018515344006025149 -0.99982857632513078 -2.851037485859587e-15 0 -0.99982857632513089 -0.01851534400602534 3.4757632316284609e-16 0
		 -405.54658103968353 686.68793147065594 -8.3328299999981397 1;
	setAttr ".pm[15]" -type "matrix" -0.039185566695350713 0.99923195073154225 8.8199496530749458e-16 -0
		 -0.99923195073154247 -0.039185566695350692 -3.7199933019456096e-16 0 -5.3753042576038589e-16 -8.1335366304822532e-16 0.99999999999999978 -0
		 693.41658891137172 35.532042351583996 403.41714125562231 1;
	setAttr ".pm[16]" -type "matrix" 2.1246014704267292e-15 -2.4460094339290237e-15 -0.99999999999999956 -0
		 -0.73593101176182851 0.67705653082088435 -3.2575685969753614e-15 0 0.67705653082088457 0.73593101176182874 -5.6995409058207253e-16 -0
		 522.48682682667959 -542.33985522008913 4.9103200000024678 1;
	setAttr ".pm[17]" -type "matrix" 8.7206016437896295e-17 -3.2387171333270584e-15 -0.99999999999999956 -0
		 -0.13832224525180481 0.99038727600292775 -3.2575685969753618e-15 0 0.99038727600292797 0.13832224525180481 -5.6995409058207233e-16 -0
		 20.315338364123779 -718.90860680071933 4.910320000002371 1;
	setAttr ".pm[18]" -type "matrix" -3.3192307148995568e-16 -3.3320012565859256e-08 -0.99999999999999911 0
		 -0.0092185452926782426 0.99995750830857089 -3.3318596777028903e-08 0 0.99995750830857166 0.0092185452926782045 -3.0716258523514159e-10 -0
		 -205.00061747979939 -698.27647848807669 -1.8655067334188549 1;
	setAttr ".pm[19]" -type "matrix" 3.2044664045228539e-10 -8.0441621725000886e-08 -0.99999999999999634 -0
		 -0.018834962757057604 0.99982260635471398 -8.043338753095104e-08 0 0.99982260635471742 0.018834962757057531 -1.1947253623882827e-09 -0
		 -334.65311863331425 -701.52762623911212 -8.332773675218526 1;
	setAttr ".pm[20]" -type "matrix" 6.3520487975324653e-16 -7.4469919509060005e-09 0.99999999999999933 -0
		 -0.0011079563370652119 0.99999938621618911 7.4469874186885004e-09 0 -0.99999938621618922 -0.0011079563370652496 -8.2500983843974439e-12 -0
		 372.72675544167942 -693.6434608384958 8.332824834442464 1;
	setAttr ".pm[21]" -type "matrix" 1.2963464074173682e-10 2.235533044725487e-08 0.99999999999999911 -0
		 -0.018515344006026085 0.99982857632513056 -2.2349097946466464e-08 0 -0.999828576325131 -0.018515344006026113 5.4352926022973773e-10 -0
		 405.54678257662613 -686.68787618304839 8.3328452985614483 1;
	setAttr ".pm[22]" -type "matrix" 0.039185566695350012 -0.99923195073154192 -2.0556205081424268e-16 -0
		 0.99923195073154225 0.039185566695350046 9.857608915558894e-16 0 -1.0956517615357539e-15 -5.2602619318358425e-16 0.99999999999999956 -0
		 -693.41623516036577 -35.532028478995038 -403.41700000000048 1;
	setAttr ".pm[23]" -type "matrix" -0.15679621116079887 0.98756904080779062 0.011060642097021305 -0
		 0.98762928672710615 0.15680683367561513 -9.4400928281942299e-05 0 -0.0018276116998478893 0.010909012357140287 -0.99993882477122875 0
		 -509.71336248634782 -101.50439727101816 -35.505351246728459 1;
	setAttr ".pm[24]" -type "matrix" 0.071462979010819633 0.99738192525596758 -0.011060642097021314 -0
		 -0.99744286054030218 0.071468391934084219 9.4400928282064163e-05 0 0.00088464008402846638 0.011025612321109005 0.99993882477122886 -0
		 265.5723931882938 -20.147722938677138 35.505351246728452 1;
	setAttr ".pm[25]" -type "matrix" -0.95844736613851533 -0.28505492213345057 0.011060637849371937 -0
		 -0.28507335966558345 0.95850569674892272 -9.4386645434053941e-05 0 -0.010574779010435114 -0.0032435578234796905 -0.99993882481956176 0
		 30.99052304575579 -46.850594874609897 -35.505351944856777 1;
	setAttr ".pm[26]" -type "matrix" -0.15679621132418931 0.9875690408294221 0.011060637849371939 -0
		 0.98762928672850037 0.15680683367543038 -9.4386645434053914e-05 0 -0.0018275969284890505 0.010909010401533805 -0.99993882481956176 0
		 -29.473817142537658 134.3405408076022 -35.505351944856784 1;
	setAttr ".pm[27]" -type "matrix" 0.15679621384860573 -0.98756905686476415 -0.011059170221222995 -0
		 -0.98762928674967854 -0.15680683354076405 9.4388768125691611e-05 0 -0.001827368890694805 0.010907560596157814 -0.99993884105218078 -0
		 509.71327688297606 101.50444880272651 35.505303207731473 1;
	setAttr ".pm[28]" -type "matrix" -0.07146298031067444 -0.99738194164916549 0.011059155359065369 -0
		 0.99744286055121789 -0.071468391796386407 -9.438983308832972e-05 0 0.00088452276313636888 0.011024130177842983 0.99993884121645304 -0
		 -265.57251857229869 20.147785577698688 -35.505302907505857 1;
	setAttr ".pm[29]" -type "matrix" 0.95844738186473677 0.28505492690540796 -0.011059152028233177 -0
		 0.28507335953348817 -0.95850569678899833 9.4378633065701339e-05 0 -0.010573357126367141 -0.0032431265760458181 -0.99993884125434873 0
		 -30.990574359499234 46.850596658850257 35.505303454949555 1;
	setAttr ".pm[30]" -type "matrix" 0.15679621399085625 -0.98756905704591091 -0.011059152028233179 -0
		 -0.98762928675089245 -0.15680683353921848 9.4378633065700878e-05 0 -0.0018273560288380483 0.010907544217367292 -0.99993884125434895 -0
		 29.473871518447488 -134.34087839885791 35.505305787318981 1;
	setAttr ".gm" -type "matrix" 100 0 0 0 -0 -1.6292067961387602e-05 -99.999999999998678 0
		 0 99.999999999998678 -1.6292067961387602e-05 0 103.66405487060547 213.98854064941406 0 1;
	setAttr -s 12 ".ma";
	setAttr -s 31 ".dpf[0:30]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 12 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 12 ".ifcl";
createNode skinCluster -n "skinCluster4";
	rename -uid "807286B4-4D6C-BB82-8123-179E14541B5B";
	setAttr -s 66 ".wl";
	setAttr ".wl[0:65].w"
		2 6 0.80688939874268251 7 0.19311060125731752
		3 6 0.16269844742584005 7 0.43148895892859618 8 0.40581259364556371
		2 6 0.94736768677351924 7 0.052632313226480799
		3 6 0.063623042535359942 7 0.49790308159715368 8 0.43847387586748643
		1 6 1
		1 6 1
		3 6 0.092878154904435961 7 0.45356092254778202 8 0.45356092254778202
		3 6 0.09709475142159299 7 0.45145262428920363 8 0.45145262428920341
		1 6 1
		1 6 1
		3 6 0.007218429844397964 7 0.4963907850778011 8 0.49639078507780099
		3 6 0.0017776405242421881 7 0.49911117973787894 8 0.49911117973787894
		3 6 0.32716478617068562 7 0.40374705091985263 8 0.2690881629094618
		3 6 0.43548894035014896 7 0.43539103818371272 8 0.12912002146613835
		2 6 0.59309961546919232 7 0.40690038453080768
		3 6 0.26415303175165344 7 0.55172863637063163 8 0.18411833187771501
		3 6 0.50665677752461213 7 0.46025474594298893 8 0.033088476532398901
		2 6 0.87671567310590837 7 0.12328432689409165
		3 6 0.16248270764365941 7 0.7473878878185074 8 0.090129404537833233
		3 6 0.55551943871940324 7 0.43689264990441157 8 0.0075879113761852428
		2 6 0.98580875745883623 7 0.014191242541163806
		3 6 0.067089241389519952 7 0.90173265198720987 8 0.031178106623270172
		3 6 0.62826097405159242 7 0.36998881497289299 8 0.0017502109755146004
		2 6 0.99659163776432724 7 0.0034083622356727151
		3 6 0.32785852528074322 7 0.40369004901659483 8 0.26845142570266206
		3 6 0.43560447562812371 7 0.43560142712513561 8 0.12879409724674071
		2 6 0.61942135214340743 7 0.38057864785659251
		3 6 0.32821412195854471 7 0.40153197332287727 8 0.27025390471857802
		3 6 0.43390093510457095 7 0.43389799760800252 8 0.1322010672874265
		2 6 0.61473841517160777 7 0.38526158482839234
		2 6 0.90608228682242786 7 0.093917713177572124
		3 6 0.090295480844175172 7 0.48033636328327928 8 0.42936815587254562
		3 6 0.03239116422645795 7 0.48380441788677103 8 0.48380441788677103
		3 6 0.018322021153149792 7 0.49083898942342513 8 0.49083898942342502
		2 6 0.98051182435463868 7 0.019488175645361309
		1 6 1
		2 6 0.74666407763898124 7 0.25333592236101882
		3 6 0.47790738931196036 7 0.46893350229100828 8 0.053159108397031307
		3 6 0.2968838820415971 7 0.49162623294157076 8 0.21148988501683202
		2 6 0.80688934733658291 7 0.19311065266341712
		3 6 0.16269834529415403 7 0.43148899632589066 8 0.40581265837995528
		2 6 0.9473676631142407 7 0.05263233688575937
		3 6 0.063622874630903048 7 0.49790315401945312 8 0.43847397134964383
		1 6 1
		3 6 0.09709471222045625 7 0.45145264388977185 8 0.45145264388977185
		1 6 1
		3 6 0.0072183963200114359 7 0.49639080183999412 8 0.49639080183999434
		3 6 0.32716472813142045 7 0.40374706991552339 8 0.26908820195305627
		3 6 0.4354889365638796 7 0.43539103688194186 8 0.12912002655417848
		2 6 0.59309958189120537 7 0.40690041810879463
		3 6 0.26415279510797735 7 0.55172888287851518 8 0.1841183220135075
		3 6 0.50665674584990716 7 0.46025479972199929 8 0.033088454428093612
		2 6 0.87671565284401676 7 0.12328434715598331
		3 6 0.16248241833574614 7 0.74738828776744082 8 0.090129293896813026
		3 6 0.5555194252820822 7 0.43689267476765287 8 0.0075878999502649383
		2 6 0.98580876087705671 7 0.014191239122943384
		3 6 0.32821409732985979 7 0.40153198101430249 8 0.27025392165583767
		3 6 0.4339009338072321 7 0.43389799649759181 8 0.13220106969517609
		2 6 0.61473840414687364 7 0.38526159585312636
		2 6 0.90608224847811825 7 0.093917751521881768
		3 6 0.09029531774207103 7 0.48033642858921211 8 0.42936825366871689
		3 6 0.032391098214226047 7 0.48380445089288682 8 0.48380445089288715
		2 6 0.98051181928611186 7 0.019488180713888196
		2 6 0.74666402339241689 7 0.25333597660758311
		3 6 0.47790737713479292 7 0.46893353349589967 8 0.053159089369307487
		3 6 0.29688370857000279 7 0.49162637590718539 8 0.21148991552281185;
	setAttr -s 31 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.15680631874941806 0.98762937299386533 1.9203235634255452e-17 -0
		 0.98762937299386533 0.15680631874941817 -1.2094971503808443e-16 0 -1.2246467991473527e-16 -3.0814879110195767e-33 -1 0
		 -556.54303159690778 -88.362564335046656 6.815686422329173e-14 1;
	setAttr ".pm[1]" -type "matrix" 0.062080978492094471 0.9980711157575215 -2.6805962794083368e-17 -0
		 -0.99807111575752139 0.06208097849209461 2.431781747614721e-16 0 2.4437325261173941e-16 1.165751815773164e-17 1 -0
		 560.29701578098332 -43.035829167130757 -1.3702319106699251e-13 1;
	setAttr ".pm[2]" -type "matrix" -0.15680631874941806 0.98762937299386544 4.6009198428338808e-17 -0
		 0.98762937299386522 0.15680631874941817 -3.6412788979955656e-16 0 -3.6683793252647465e-16 -1.1657518157731646e-17 -1 0
		 -494.44900661422514 -90.513875048975763 1.909852228003692e-13 1;
	setAttr ".pm[3]" -type "matrix" -0.18411737904022696 -0.98290426326034253 -1.3957429162126204e-16 0
		 0.98290426326034275 -0.18411737904022704 9.8401839147183031e-17 0 -1.2241763996512782e-16 -1.1907067755956352e-16 1 -0
		 -553.24324881388691 105.35215275458967 -5.5307772665672655e-14 1;
	setAttr ".pm[4]" -type "matrix" 0.14368067469030996 -0.9896241022330331 -1.3957429162126204e-16 -0
		 0.98962410223303332 0.14368067469030998 9.8401839147183031e-17 0 -7.7326703334545362e-17 -1.5226452567993771e-16 1 -0
		 -612.77294890466533 -98.111138247060367 -5.5505734429841339e-14 1;
	setAttr ".pm[5]" -type "matrix" 0.03918556669535149 -0.99923195073154225 -1.3957429162126201e-16 -0
		 0.99923195073154247 0.03918556669535149 9.8401839147183043e-17 0 -9.2856963973329775e-17 -1.4332302351953412e-16 1 -0
		 -716.30416625084229 -23.176268293953683 -5.1970899344200789e-14 1;
	setAttr ".pm[6]" -type "matrix" -0.094809092627995709 -0.99549547259395166 -1.3957429162126201e-16 0
		 0.99549547259395188 -0.094809092627995736 9.8401839147183018e-17 0 -1.1119149730874616e-16 -1.2961618631700397e-16 1 -0
		 -769.3250427595915 80.440057302969578 -5.1339961442449999e-14 1;
	setAttr ".pm[7]" -type "matrix" 0.01851534400602068 -0.99982857632513089 -1.3957429162126204e-16 -0
		 0.99982857632513111 0.01851534400602068 9.8401839147183031e-17 0 -9.580070471853827e-17 -1.4137230918591036e-16 1 -0
		 -831.86760311193916 -13.836949993469942 -5.2594911904574821e-14 1;
	setAttr ".pm[8]" -type "matrix" 0.99999999999999956 -1.1553258350005527e-15 -1.3957429162126204e-16 -0
		 1.1587952819525064e-15 0.99999999999999978 9.8401839147183031e-17 -0 1.3957429162126194e-16 -9.8401839147183203e-17 1 -0
		 -2.681930185198262 -892.14763857305729 -5.1393996453069635e-14 1;
	setAttr ".pm[9]" -type "matrix" -1.5766239482585878e-15 2.4677145545861107e-15 0.99999999999999933 -0
		 0.73593101176182885 -0.67705653082088391 2.8513938317956838e-15 -0 0.67705653082088413 0.73593101176182896 -6.6920065569024372e-16 -0
		 -522.48680778374171 542.33987591893447 -4.9103200000020788 1;
	setAttr ".pm[10]" -type "matrix" 3.4980822239674598e-16 2.9074030344419489e-15 0.99999999999999956 -0
		 0.13832224525180573 -0.99038727600292753 2.8513938317956845e-15 0 0.99038727600292775 0.13832224525180556 -6.6920065569024381e-16 -0
		 -20.315310508424602 718.90861069118 -4.9103200000020051 1;
	setAttr ".pm[11]" -type "matrix" 7.2247398346767108e-16 2.8378494569569721e-15 0.99999999999999956 -0
		 0.0092185452926791221 -0.99995750830857133 2.8513938317956842e-15 0 0.99995750830857155 0.0092185452926789278 -6.6920065569024391e-16 -0
		 205.00054560892085 698.27647776334459 1.8655299999980184 1;
	setAttr ".pm[12]" -type "matrix" 6.9514825669555819e-16 2.8446664300672847e-15 0.99999999999999956 -0
		 0.018834962757058496 -0.9998226063547172 2.8513938317956842e-15 0 0.99982260635471742 0.018834962757058309 -6.6920065569024381e-16 -0
		 334.65304677480185 701.52762421506532 8.3328299999980118 1;
	setAttr ".pm[13]" -type "matrix" -4.3011583918344602e-16 2.8302375544423468e-15 -0.99999999999999956 -0
		 0.0011079563370642758 -0.99999938621618911 -2.8510374858595874e-15 0 -0.99999938621618922 -0.0011079563370644666 3.4757632316284618e-16 0
		 -372.72626894604508 693.64346131545778 -8.3328299999981468 1;
	setAttr ".pm[14]" -type "matrix" -4.7931821436030371e-16 2.8223214379433684e-15 -0.99999999999999956 -0
		 0.018515344006025149 -0.99982857632513078 -2.851037485859587e-15 0 -0.99982857632513089 -0.01851534400602534 3.4757632316284609e-16 0
		 -405.54658103968353 686.68793147065594 -8.3328299999981397 1;
	setAttr ".pm[15]" -type "matrix" -0.039185566695350713 0.99923195073154225 8.8199496530749458e-16 -0
		 -0.99923195073154247 -0.039185566695350692 -3.7199933019456096e-16 0 -5.3753042576038589e-16 -8.1335366304822532e-16 0.99999999999999978 -0
		 693.41658891137172 35.532042351583996 403.41714125562231 1;
	setAttr ".pm[16]" -type "matrix" 2.1246014704267292e-15 -2.4460094339290237e-15 -0.99999999999999956 -0
		 -0.73593101176182851 0.67705653082088435 -3.2575685969753614e-15 0 0.67705653082088457 0.73593101176182874 -5.6995409058207253e-16 -0
		 522.48682682667959 -542.33985522008913 4.9103200000024678 1;
	setAttr ".pm[17]" -type "matrix" 8.7206016437896295e-17 -3.2387171333270584e-15 -0.99999999999999956 -0
		 -0.13832224525180481 0.99038727600292775 -3.2575685969753618e-15 0 0.99038727600292797 0.13832224525180481 -5.6995409058207233e-16 -0
		 20.315338364123779 -718.90860680071933 4.910320000002371 1;
	setAttr ".pm[18]" -type "matrix" -3.3192307148995568e-16 -3.3320012565859256e-08 -0.99999999999999911 0
		 -0.0092185452926782426 0.99995750830857089 -3.3318596777028903e-08 0 0.99995750830857166 0.0092185452926782045 -3.0716258523514159e-10 -0
		 -205.00061747979939 -698.27647848807669 -1.8655067334188549 1;
	setAttr ".pm[19]" -type "matrix" 3.2044664045228539e-10 -8.0441621725000886e-08 -0.99999999999999634 -0
		 -0.018834962757057604 0.99982260635471398 -8.043338753095104e-08 0 0.99982260635471742 0.018834962757057531 -1.1947253623882827e-09 -0
		 -334.65311863331425 -701.52762623911212 -8.332773675218526 1;
	setAttr ".pm[20]" -type "matrix" 6.3520487975324653e-16 -7.4469919509060005e-09 0.99999999999999933 -0
		 -0.0011079563370652119 0.99999938621618911 7.4469874186885004e-09 0 -0.99999938621618922 -0.0011079563370652496 -8.2500983843974439e-12 -0
		 372.72675544167942 -693.6434608384958 8.332824834442464 1;
	setAttr ".pm[21]" -type "matrix" 1.2963464074173682e-10 2.235533044725487e-08 0.99999999999999911 -0
		 -0.018515344006026085 0.99982857632513056 -2.2349097946466464e-08 0 -0.999828576325131 -0.018515344006026113 5.4352926022973773e-10 -0
		 405.54678257662613 -686.68787618304839 8.3328452985614483 1;
	setAttr ".pm[22]" -type "matrix" 0.039185566695350012 -0.99923195073154192 -2.0556205081424268e-16 -0
		 0.99923195073154225 0.039185566695350046 9.857608915558894e-16 0 -1.0956517615357539e-15 -5.2602619318358425e-16 0.99999999999999956 -0
		 -693.41623516036577 -35.532028478995038 -403.41700000000048 1;
	setAttr ".pm[23]" -type "matrix" -0.15679621116079887 0.98756904080779062 0.011060642097021305 -0
		 0.98762928672710615 0.15680683367561513 -9.4400928281942299e-05 0 -0.0018276116998478893 0.010909012357140287 -0.99993882477122875 0
		 -509.71336248634782 -101.50439727101816 -35.505351246728459 1;
	setAttr ".pm[24]" -type "matrix" 0.071462979010819633 0.99738192525596758 -0.011060642097021314 -0
		 -0.99744286054030218 0.071468391934084219 9.4400928282064163e-05 0 0.00088464008402846638 0.011025612321109005 0.99993882477122886 -0
		 265.5723931882938 -20.147722938677138 35.505351246728452 1;
	setAttr ".pm[25]" -type "matrix" -0.95844736613851533 -0.28505492213345057 0.011060637849371937 -0
		 -0.28507335966558345 0.95850569674892272 -9.4386645434053941e-05 0 -0.010574779010435114 -0.0032435578234796905 -0.99993882481956176 0
		 30.99052304575579 -46.850594874609897 -35.505351944856777 1;
	setAttr ".pm[26]" -type "matrix" -0.15679621132418931 0.9875690408294221 0.011060637849371939 -0
		 0.98762928672850037 0.15680683367543038 -9.4386645434053914e-05 0 -0.0018275969284890505 0.010909010401533805 -0.99993882481956176 0
		 -29.473817142537658 134.3405408076022 -35.505351944856784 1;
	setAttr ".pm[27]" -type "matrix" 0.15679621384860573 -0.98756905686476415 -0.011059170221222995 -0
		 -0.98762928674967854 -0.15680683354076405 9.4388768125691611e-05 0 -0.001827368890694805 0.010907560596157814 -0.99993884105218078 -0
		 509.71327688297606 101.50444880272651 35.505303207731473 1;
	setAttr ".pm[28]" -type "matrix" -0.07146298031067444 -0.99738194164916549 0.011059155359065369 -0
		 0.99744286055121789 -0.071468391796386407 -9.438983308832972e-05 0 0.00088452276313636888 0.011024130177842983 0.99993884121645304 -0
		 -265.57251857229869 20.147785577698688 -35.505302907505857 1;
	setAttr ".pm[29]" -type "matrix" 0.95844738186473677 0.28505492690540796 -0.011059152028233177 -0
		 0.28507335953348817 -0.95850569678899833 9.4378633065701339e-05 0 -0.010573357126367141 -0.0032431265760458181 -0.99993884125434873 0
		 -30.990574359499234 46.850596658850257 35.505303454949555 1;
	setAttr ".pm[30]" -type "matrix" 0.15679621399085625 -0.98756905704591091 -0.011059152028233179 -0
		 -0.98762928675089245 -0.15680683353921848 9.4378633065700878e-05 0 -0.0018273560288380483 0.010907544217367292 -0.99993884125434895 -0
		 29.473871518447488 -134.34087839885791 35.505305787318981 1;
	setAttr ".gm" -type "matrix" 100 0 0 0 -0 -1.6292067961387602e-05 -99.999999999998678 0
		 0 99.999999999998678 -1.6292067961387602e-05 0 -0.33757352828979492 772.7559814453125 0 1;
	setAttr -s 7 ".ma";
	setAttr -s 31 ".dpf[0:30]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 7 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 7 ".ifcl";
createNode skinCluster -n "skinCluster5";
	rename -uid "0AA16AE7-4838-3DD5-1EDE-DD979D678359";
	setAttr -s 424 ".wl";
	setAttr ".wl[0:166].w"
		3 24 0.0019262896177860451 25 0.98158253916715699 26 0.016491171215056999
		3 24 3.9514460438674304e-05 25 0.99971228282859792 26 0.00024820271096337757
		3 24 0.49747027584833448 25 0.49747027584833436 26 0.0050594483033311799
		3 24 0.50011771848875919 25 0.49987540872865177 26 6.8727825890561108e-06
		3 24 0.002223945855111526 25 0.97925553167365165 26 0.018520522471236908
		3 24 0.00016530200295071328 25 0.99883174976019584 26 0.0010029482368534826
		3 24 0.49750909348240707 25 0.49750909348240707 26 0.0049818130351858506
		3 24 0.50004992535634696 25 0.49993299524398144 26 1.7079399671672125e-05
		3 24 0.41255436568111103 25 0.58604788140570052 26 0.0013977529131884256
		3 24 0.11117416487212321 25 0.88644586344313714 26 0.0023799716847396684
		3 24 0.011811095193878629 25 0.98422474954831307 26 0.003964155257808309
		3 24 0.0014074582414786544 25 0.99827786772987048 26 0.00031467402865085402
		3 24 0.0024754985704456643 25 0.9975167587163658 26 7.7427131885336354e-06
		3 24 0.37182838772071264 25 0.62816688925371755 26 4.7230255697475128e-06
		3 24 0.00245911879788962 25 0.99699184339424896 26 0.0005490378078613884
		3 24 0.010468748734901293 25 0.98949581266692721 26 3.5438598171531759e-05
		3 24 0.42820667222888154 25 0.5717801237241158 26 1.3204047002711931e-05
		3 24 0.41564873166162186 25 0.58293403500584373 26 0.0014172333325344079
		3 24 0.11577007617760728 25 0.88175586152006669 26 0.002474062302326099
		3 24 0.012899493421932586 25 0.98281274009231812 26 0.0042877664857493064
		3 24 0.0010786070406886303 25 0.99096725215988479 26 0.0079541407994265356
		3 24 0.49923997438157003 25 0.49923997438157003 26 0.0015200512368599159
		3 24 0.49927666688050121 25 0.49927666688050121 26 0.0014466662389976213
		3 24 0.0017920105348174275 25 0.98577947273098177 26 0.012428516734200833
		3 24 0.014680267685873022 25 0.98086601423489272 26 0.0044537180792341164
		3 24 0.086041884336719415 25 0.91298714596707409 26 0.00097096969620653515
		3 24 0.4276549447944834 25 0.57160048953268894 26 0.00074456567282765555
		3 24 0.011257916300009572 25 0.98528484407889094 26 0.0034572396210995121
		3 24 0.068531518218215889 25 0.9307267731274238 26 0.00074170865436032276
		3 24 0.41754294171044004 25 0.58180373635649851 26 0.00065332193306139627
		3 24 0.49322866657392778 25 0.50676451165569825 26 6.8217703740020989e-06
		3 24 0.49740104383031947 25 0.50258136832352551 26 1.7587846155084899e-05
		3 24 0.49689998406735481 25 0.50219810126063902 26 0.00090191467200621096
		3 24 0.49739165083993325 25 0.50169562071310025 26 0.00091272844696652501
		3 24 0.49758335587681368 25 0.50161127794755955 26 0.0008053661756267735
		3 24 0.49654500856980927 25 0.50273592071011186 26 0.0007190707200787599
		3 24 0.2839870093866122 25 0.70853790607158873 26 0.0074750845417990213
		3 24 0.027572118089425474 25 0.97241434083325129 26 1.3541077323217858e-05
		3 24 0.066354396655037465 25 0.93360585444785604 26 3.9748897106350231e-05
		3 24 0.28745628111933924 25 0.7049943936094929 26 0.0075493252711678728
		3 24 0.18141914991824154 25 0.8177628174488778 26 0.0008180326328806163
		3 24 0.20499182615070483 25 0.79401681428428805 26 0.00099135956500722897
		3 24 0.055289742289571064 25 0.93393293768452668 26 0.010777320025902285
		3 24 0.0014548986922539799 25 0.9984479093449854 26 9.7191962760538703e-05
		3 24 0.0030697096496286112 25 0.99672313362212317 26 0.00020715672824822971
		3 24 0.057392230073953554 25 0.93150463539011041 26 0.011103134535935939
		3 24 0.016083513662827113 25 0.98230215853437086 26 0.0016143278028020725
		3 24 0.021453117760203557 25 0.97639184359554387 26 0.0021550386442525408
		3 24 0.0062911436252534063 25 0.97986731059583565 26 0.013841545778910981
		3 24 0.0068917576302682674 25 0.97820662004999348 26 0.014901622319738303
		3 24 0.00033711189809546466 25 0.99915150580612022 26 0.00051138229578427891
		3 24 0.00070492333719878028 25 0.99824476035809695 26 0.0010503163047043361
		3 24 0.0036568721619137799 25 0.98951189908515758 26 0.0068312287529286317
		3 24 0.0051447813773850513 25 0.98557665561949337 26 0.009278563003121628
		3 24 0.50041289592081717 25 0.49958062971020811 26 6.4743689747546146e-06
		3 24 0.50021479595127827 25 0.49976849753152047 26 1.6706517201234188e-05
		3 24 0.49962950257678179 25 0.49962950257678179 26 0.00074099484643640342
		3 24 0.49928074593538208 25 0.49928074593538196 26 0.0014385081292359624
		3 24 0.49927585143431025 25 0.49927585143431047 26 0.0014482971313793456
		3 24 0.49958903387500792 25 0.49958903387500764 26 0.0008219322499844917
		3 24 3.1759475575805074e-05 25 0.99617131115474045 26 0.0037969293696837737
		3 24 7.8230909985803603e-05 25 0.9932080772491596 26 0.0067136918408545699
		3 24 9.768989171771164e-05 25 0.98947170887002878 26 0.010430601238253451
		3 24 0.00026401714533213919 25 0.98021502110452829 26 0.019520961750139603
		3 24 0.00041034335006587123 25 0.96603298348253896 26 0.033556673167395218
		3 24 0.00091292096691296832 25 0.93852709303580384 26 0.060559985997283176
		3 24 2.0714600928748001e-05 25 0.7337819016654511 26 0.26619738373362023
		3 24 5.0175398441421085e-05 25 0.73442642094893795 26 0.26552340365262056
		3 24 5.7541267427224269e-05 25 0.66175106420855578 26 0.33819139452401703
		3 24 0.00014622715373632653 25 0.65714562073806027 26 0.34270815210820343
		3 24 0.00015404908966590343 25 0.61667639980492528 26 0.38316955110540885
		3 24 0.00028253386728806885 25 0.59470195020301442 26 0.40501551592969759
		3 24 0.00016282304818108931 25 0.94525152779528132 26 0.054585649156537665
		3 24 0.00039663061047751059 25 0.9002048524261782 26 0.099398516963344383
		3 24 0.0010538858384152457 25 0.80664137499650124 26 0.19230473916508353
		3 24 0.00014110654802311037 25 0.9371063771478747 26 0.062752516304102224
		3 24 5.7299474081796669e-05 25 0.96764814794538934 26 0.032294552580528851
		3 24 0.00055039719734539761 25 0.85640722621186594 26 0.14304237659078867
		3 24 3.0996363770910373e-05 25 0.4999845018181146 26 0.49998450181811449
		3 24 1.4934237746146315e-05 25 0.49999253288112677 26 0.49999253288112711
		3 24 6.7566460631721944e-05 25 0.49996621676968411 26 0.49996621676968411
		3 24 6.8193194782629278e-05 25 0.49996590340260855 26 0.49996590340260888
		3 24 0.00012156079340525159 25 0.49993921960329735 26 0.49993921960329735
		3 24 0.00022578150063368009 25 0.49988710924968333 26 0.49988710924968305
		3 24 3.338839964114794e-05 25 0.99975850248113607 26 0.00020810911922275403
		3 24 1.9302986223877604e-05 25 0.99985917167408378 26 0.00012152533969231512
		3 24 0.49756012062365534 25 0.49756012062365557 26 0.0048797587526890661
		3 24 0.49756244158911933 25 0.49756244158911955 26 0.0048751168217610807
		3 24 0.0016969160813378062 25 0.98367898735478354 26 0.014624096563878627
		3 24 0.0017997055131082993 25 0.9828507116395705 26 0.015349582847321349
		3 24 0.50057166008949627 25 0.49942806962126035 26 2.7028924337650061e-07
		3 24 0.50043516298712998 25 0.49956445428579987 26 3.8272707018259886e-07
		3 24 0.010574780484032979 25 0.98591210662906148 26 0.0035131128869055881
		3 24 0.010959729506906374 25 0.98541229134199404 26 0.0036279791510996332
		3 24 0.10252867960148292 25 0.89538067967885704 26 0.0020906407196600676
		3 24 0.10426097829191239 25 0.89361411091363596 26 0.0021249107944516792
		3 24 0.40789241168951518 25 0.59087147193286504 26 0.0012361163776197738
		3 24 0.40914365466151925 25 0.58961290965880764 26 0.0012434356796730565
		3 24 0.13676740414687108 25 0.8632323514444068 26 2.4440872220131715e-07
		3 24 0.18127781605461171 25 0.81872179319462324 26 3.90750765133471e-07
		3 24 7.0540222961179927e-05 25 0.99992925771755281 26 2.0205948607292653e-07
		3 24 0.0002566959849548072 25 0.99974256607765988 26 7.3793738531535698e-07
		3 24 0.0010868296653782151 25 0.99867220892272845 26 0.00024096141189337876
		3 24 0.001237387283221471 25 0.99848917094468359 26 0.00027344177209502824
		3 24 0.49945963322112907 25 0.49945963322112885 26 0.001080733557742176
		3 24 0.49946995235736369 25 0.49946995235736402 26 0.0010600952852722731
		3 24 0.4628198277530135 25 0.53717999380907455 26 1.7843791200572139e-07
		3 24 0.47499279942633182 25 0.52500692127565918 26 2.7929800902817364e-07
		3 24 0.49688684709303355 25 0.50235955496072815 26 0.0007535979462382986
		3 24 0.49708866108273164 25 0.50215368671219696 26 0.0007576522050714501
		3 24 0.0032736541911860748 25 0.99672510310169438 26 1.2427071195351673e-06
		3 24 0.0051910884507066811 25 0.99480691352099604 26 1.9980282972653231e-06
		3 24 0.27899664713855332 25 0.71398120372069451 26 0.007022149140752219
		3 24 0.28033677365289922 25 0.71261241420062538 26 0.0070508121464753631
		3 24 0.00092076169640167415 25 0.99901870105751356 26 6.0537246084793324e-05
		3 24 0.0011073604405932021 25 0.99881997593790006 26 7.2663621506761195e-05
		3 24 0.052841142551179283 25 0.93699482151006241 26 0.010164035938758314
		3 24 0.05361825719940895 25 0.93609681164157532 26 0.010284931159015714
		3 24 0.0057559610877851372 25 0.98158679585792807 26 0.012657243054286792
		3 24 0.0059693075526821163 25 0.98098937964721322 26 0.013041312800104694
		3 24 0.00024747833047491905 25 0.99937786766653947 26 0.00037465400298561174
		3 24 0.00030174183695109215 25 0.99924430538218245 26 0.00045395278086643117
		3 24 0.50256315496258164 25 0.4974366860329173 26 1.5900450112731128e-07
		3 24 0.50193812880270028 25 0.4980616214596843 26 2.4973761542594967e-07
		3 24 0.49936735429811979 25 0.49936735429811968 26 0.0012652914037605822
		3 24 0.49936543125836164 25 0.49936543125836164 26 0.0012691374832767134
		3 24 3.9320677798627543e-05 25 0.99650129640227936 26 0.003459382919922024
		3 24 2.1550880797103556e-05 25 0.99800737142086327 26 0.0019710776983395688
		3 24 2.0083101919484611e-07 25 0.9999733580100123 26 2.6441158968593199e-05
		3 24 4.2363959204151218e-06 25 0.99946266988220844 26 0.00053309372187115909
		3 24 8.4643646084171375e-05 25 0.69784896716078004 26 0.30206638919313578
		3 24 4.1861252090829566e-05 25 0.7508326675345266 26 0.24912547121338258
		3 24 1.1651059720435883e-05 25 0.78597021262936362 26 0.21401813631091593
		3 24 3.7526590367573871e-05 25 0.6935736944236337 26 0.3063887789859987
		3 24 1.0227774920692228e-05 25 0.99305340289861721 26 0.0069363693264621181
		3 24 2.2161963075383245e-06 25 0.99832615367410216 26 0.0016716301295902273
		3 24 5.7431260844818014e-05 25 0.97666825179868832 26 0.023274316940466895
		3 24 8.5636439886271646e-05 25 0.96788825553081836 26 0.032026108029295432
		3 24 3.122998183420676e-05 25 0.49998438500908277 26 0.49998438500908299
		3 24 1.0297385939106883e-05 25 0.49999485130703042 26 0.49999485130703042
		3 24 2.0861187056309162e-05 25 0.49998956940647182 26 0.49998956940647182
		3 24 4.7395615942769488e-05 25 0.49997630219202843 26 0.49997630219202877
		3 24 9.9659984395187451e-05 25 0.49995017000780229 26 0.49995017000780251
		3 24 4.6714320689715255e-05 25 0.49997664283965526 26 0.49997664283965504
		3 24 6.5638477497904115e-05 25 0.67617550467163012 26 0.32375885685087202
		3 24 2.2792591690430333e-05 25 0.98769445214004525 26 0.012282755268264276
		3 24 4.9389134574932846e-05 25 0.97639861445683696 26 0.023551996408588093
		3 24 0.00013490267347207875 25 0.63569509923908951 26 0.36416999808743844
		3 24 0.00011144612462141 25 0.94749564566421585 26 0.052392908211162763
		3 24 3.2419014474407352e-05 25 0.97302834563464802 26 0.026939235350877562
		3 24 0.00025324714781736632 25 0.89038677274213407 26 0.10935998011004865
		3 24 1.8743740433286037e-05 25 0.98315042121315521 26 0.016830835046411457
		3 24 0.00010104364887080931 25 0.95135049609670908 26 0.048548460254420153
		3 24 0.00011789294823528287 25 0.93618512385815933 26 0.063696983193605436
		3 24 0.00027972411849625699 25 0.90179322011245588 26 0.097927055769047838
		3 24 0.00044345253475156507 25 0.85306541971764849 26 0.14649112774760001
		3 24 8.9782283486902805e-05 25 0.94166475899145208 26 0.058245458725061011
		3 24 5.9105209974020353e-05 25 0.95760383420243 26 0.042337060587596023
		3 24 0.00017757963151305778 25 0.92795425209061455 26 0.071868168277872402
		3 24 0.00022795821070614072 25 0.90266434389477324 26 0.097107697894520631
		3 24 0.50024335833091216 25 0.49975007220789713 26 6.5694611906880563e-06
		3 24 0.5001191233915866 25 0.49986414785177846 26 1.6728756634923491e-05
		4 24 0.49579824953812868 25 0.49579824953812879 26 0.00076659574158951713 
		29 0.0076369051821529865
		3 24 0.49750628198667463 25 0.49750628198667496 26 0.0049874360266504211
		3 24 0.49754393854162043 25 0.49754393854162088 26 0.0049121229167587085
		3 24 0.49957376111290436 25 0.49957376111290464 26 0.00085247777419095486
		1 24 0.50137324754735646;
	setAttr ".wl[166:303].w"
		2 25 0.49862655929046035 26 1.9316218330310467e-07
		3 24 0.50104376429149056 25 0.49895594477054017 26 2.9093796927286428e-07
		3 24 0.49758945365550949 25 0.49758945365550949 26 0.0048210926889809816
		3 24 0.49758709395030326 25 0.49758709395030354 26 0.0048258120993931687
		3 24 0.0069173768718871383 25 0.99128266715621494 26 0.0017999559718979723
		3 24 0.032239332955408866 25 0.96757122603749912 26 0.00018944100709209706
		3 24 0.41108579393874234 25 0.5888356614393514 26 7.8544621906286369e-05
		3 24 0.00033464106051003567 25 0.99736798144279815 26 0.0022973774966918406
		3 24 0.49996152479661887 25 0.49996152479661843 26 7.6950406762728955e-05
		3 24 0.49996233164374998 25 0.49996233164374976 26 7.5336712500304103e-05
		3 24 0.00065873466877377725 25 0.99502417164915724 26 0.0043170936820690715
		3 24 0.0050347662131564356 25 0.99364327179883549 26 0.0013219619880081124
		3 24 0.020543551763012369 25 0.9793408797094878 26 0.0001155685274998058
		3 24 0.38928946703446193 25 0.61065234500745802 26 5.8187958080048602e-05
		3 24 0.49919709459035799 25 0.50071543842856769 26 8.7466981074218818e-05
		3 24 0.49814930258744727 25 0.50178394701919959 26 6.6750393353209742e-05
		3 24 0.076835102970931463 25 0.9230596793853193 26 0.00010521764374934167
		3 24 0.10504865877370849 25 0.89479323830058777 26 0.00015810292570372892
		3 24 0.0066743076455721851 25 0.99277889710779121 26 0.00054679524663654277
		3 24 0.0096186021696883194 25 0.98959248672578559 26 0.00078891110452603604
		3 24 0.0014612244360034568 25 0.99606714113340966 26 0.0024716344305867359
		3 24 0.0022170890303328424 25 0.99413124273164988 26 0.0036516682380173097
		3 24 0.49995702004376597 25 0.49995702004376597 26 8.5959912468117213e-05
		3 24 0.49996683632992162 25 0.4999668363299215 26 6.6327340156889666e-05
		3 24 0.00018068331438983709 25 0.98435388074174346 26 0.015465435943866724
		3 24 0.00044870369693292858 25 0.96699258276702393 26 0.032558713536043157
		3 24 7.5597177056759121e-05 25 0.6807860454464203 26 0.31913835737652291
		3 24 0.0001520785419897709 25 0.64036788027023506 26 0.35948004118777516
		3 24 0.00028436218212878457 25 0.91145473929249654 26 0.088260898525374568
		3 24 0.00057838631823565871 25 0.86628664091343477 26 0.13313497276832964
		3 24 4.0825646190194265e-05 25 0.49997958717690488 26 0.49997958717690488
		3 24 9.3092233329429504e-05 25 0.49995345388333529 26 0.49995345388333529
		3 24 4.2260627615933574e-05 25 0.49997886968619204 26 0.49997886968619204
		3 24 1.5554528026985756e-05 25 0.49999222273598648 26 0.49999222273598648
		3 24 0.49998787739738237 25 0.49998787739738237 26 2.4245205235250167e-05
		3 24 0.49998801043833169 25 0.49998801043833169 26 2.3979123336652193e-05
		3 24 3.7138074024737867e-05 25 0.74272896806485833 26 0.25723389386111695
		3 24 3.2611551778142555e-05 25 0.98449374331679773 26 0.015473645131424154
		3 24 5.676707571453411e-05 25 0.97540830094810216 26 0.024534931976183356
		3 24 8.1339579219597227e-05 25 0.68529054896966746 26 0.31462811145111302
		3 24 8.0777991919022129e-05 25 0.95584607598113491 26 0.044073146026946154
		3 24 0.00014475741454999001 25 0.93070925960901074 26 0.069145982976439285
		3 24 0.00015690250919605213 25 0.92944683881727763 26 0.070396258673526321
		3 24 0.00026913578909716792 25 0.89770795950478899 26 0.10202290470611385
		3 24 0.4999547867238166 25 0.4999547867238166 26 9.042655236685655e-05
		3 24 0.49996467258702965 25 0.49996467258702953 26 7.0654825940775996e-05
		4 26 0.0023377216828343161 28 0.0019218252450982213 29 0.97928794691827403 
		30 0.01645250615379347
		5 25 0.00010399235922581891 26 3.7964891179548455e-05 28 3.9511863436642207e-05 
		29 0.9995703509889291 30 0.0002481798972288141
		3 28 0.49747029721340374 29 0.49747029721340363 30 0.0050594055731926152
		3 28 0.50011771291436469 29 0.4998754143186388 30 6.8727669965462751e-06
		4 26 0.0042723693742630284 28 0.0022144454653612315 29 0.97507220340492551 
		30 0.018440981755450319
		5 25 0.00095227429204331208 26 0.0002474614562183304 28 0.00016509790819638117 
		29 0.99763347869558594 30 0.0010016876479559684
		3 28 0.49750911397078723 29 0.49750911397078734 30 0.004981772058425424
		3 28 0.50004991887609385 29 0.49993300196929868 30 1.7079154607465265e-05
		3 28 0.41255486861939294 29 0.58604739068499134 30 0.0013977406956157145
		3 28 0.11117481385153834 29 0.88644523088869975 30 0.0023799552597619099
		3 28 0.011811244616801612 29 0.98422461826127994 30 0.0039641371219185139
		5 24 0.00026135083546689471 25 0.00073564592092190757 28 0.001406092583352535 
		29 0.9972825472360759 30 0.00031436342418278196
		5 24 4.6323313313661782e-05 25 6.0136677455100747e-05 28 0.0024753202919044 
		29 0.99741047765383417 30 7.7420634926497464e-06
		3 28 0.37182961552826371 29 0.62816566145061936 30 4.7230211168892451e-06
		3 28 0.0024590976487519223 29 0.9969918777004495 30 0.00054902465079859812
		3 28 0.010468494614012813 29 0.9894960680753504 30 3.5437310636806066e-05
		3 28 0.42820587130668697 29 0.5717809248751009 30 1.3203818212094966e-05
		3 28 0.4156488234100566 29 0.58293395728631969 30 0.0014172193036237749
		3 28 0.1157701475165461 29 0.88175581863328112 30 0.0024740338501727972
		3 28 0.01289950599729759 29 0.98281279073666683 30 0.0042877032660355672
		4 26 0.00098463857674833044 28 0.0010775816682558664 29 0.98999142329037293 
		30 0.0079463564646228401
		3 28 0.49923998067714165 29 0.49923998067714154 30 0.0015200386457168834
		3 28 0.49927667310240148 29 0.49927667310240137 30 0.0014466537951970564
		4 26 0.003960320642797938 28 0.0017848883231987953 29 0.9818759220865293 
		30 0.012378868947473936
		3 28 0.014680191569973095 29 0.98086617894214301 30 0.0044536294878838928
		3 28 0.086041478184949019 29 0.91298756811185444 30 0.00097095370319652985
		3 28 0.4276547627690977 29 0.57160067960329575 30 0.00074455762760663243
		5 24 0.0020428044891539171 25 0.0051526787331478731 28 0.011177102040976971 
		29 0.97819505300413689 30 0.0034323617325842616
		5 24 0.0024813204540574479 25 0.0032835567258464511 28 0.068137214564291435 
		29 0.92536047633214735 30 0.00073743192365734482
		3 28 0.41754362508746279 29 0.58180305797182863 30 0.000653316940708534
		3 28 0.49322888871810816 29 0.50676428952730279 30 6.8217545890869233e-06
		3 28 0.49740091200050052 29 0.50258150042129335 30 1.7587578206114121e-05
		3 28 0.49690007168698452 29 0.50219802142012726 30 0.00090190689288822555
		3 28 0.49739166467738849 29 0.50169561537658591 30 0.00091271994602559476
		3 28 0.49758333382943298 29 0.50161130782885155 30 0.00080535834171544072
		4 24 0.0064546499004960881 28 0.49334010438349252 29 0.4994908222208887 
		30 0.00071442349512260595
		3 28 0.28398767297343241 29 0.70853731278765619 30 0.0074750142389114595
		5 24 0.00012518207032357207 25 0.00013894330082211504 28 0.027565255657785333 
		29 0.97215708140548729 30 1.353756558174965e-05
		3 28 0.066353454071743895 29 0.93360679808340852 30 3.9747844847627998e-05
		3 28 0.28745648808905999 29 0.70499426504793483 30 0.0075492468630052123
		5 24 0.0038646369706320507 25 0.0044787884244561014 28 0.17990663889378011 
		29 0.81093873218181034 30 0.00081120352932136596
		3 28 0.20499134579628103 29 0.79401730803610993 30 0.00099134616760915089
		3 28 0.055290119990130664 29 0.9339326525028977 30 0.010777227506971665
		5 24 0.00016659118132979165 25 0.00035507291501872193 28 0.0014541844090095958 
		29 0.99792700869813122 30 9.7142796510662073e-05
		3 28 0.0030696717715423764 29 0.99672317693654633 30 0.00020715129191133596
		3 28 0.05739233907488598 29 0.93150466299623502 30 0.011102997928879048
		5 24 0.0018877590366197911 25 0.0037311338298118579 28 0.01599341011153034 
		29 0.97678243808890253 30 0.0016052589331353941
		3 28 0.021452987778252153 29 0.97639201565778688 30 0.0021549965639610217
		4 26 0.0031861084390776193 28 0.0062711879991732885 29 0.97674536260766187 
		30 0.013797340954087262
		4 26 0.0050806848689960247 28 0.0068567553144533513 29 0.97323690416660125 
		30 0.014825655649949367
		5 25 0.00048378631664056267 26 0.00012431643533798167 28 0.00033691982130164446 
		29 0.99854389735579252 30 0.00051108007092723437
		4 26 0.00037523612826344407 28 0.00070464866314738531 29 0.99787022783570545 
		30 0.0010498873728836096
		4 26 0.0013920943778648321 28 0.0036518664168028762 29 0.98813431421151554 
		30 0.0068217249938168261
		4 26 0.0040403835424583142 28 0.0051239548879631952 29 0.98159482064072068 
		30 0.0092408409288578016
		3 28 0.50041288430213549 29 0.49958064134374014 30 6.4743541244237098e-06
		3 28 0.50021478312788203 29 0.49976851060200328 30 1.6706270114725769e-05
		4 24 0.0073490689076655315 28 0.49595769395043271 29 0.4959576939504326 
		30 0.00073554319146911982
		3 28 0.4992807521294485 29 0.49928075212944839 30 0.0014384957411031421
		3 28 0.49927585783701373 29 0.49927585783701373 30 0.0014482843259725436
		3 28 0.49958903770719792 29 0.49958903770719781 30 0.00082192458560428926
		5 25 0.00016283949030802022 26 0.00011217688470624032 28 3.1753394142599654e-05 
		29 0.99589720218521471 30 0.0037960280456283565
		5 25 0.00038795963444180563 26 0.00025571506338333911 28 7.8183025049681113e-05 
		29 0.99256882650554534 30 0.0067093157715798248
		5 25 0.0016885397634783135 26 0.00089738292568240427 28 9.7430820247926392e-05 
		29 0.98691400583963529 30 0.010402640650956078
		5 25 0.0045066215568854663 26 0.0022206548660920161 28 0.00026222724766750106 
		29 0.97362234373591738 30 0.01938815259343759
		5 25 0.0014440743024466633 26 0.0010379960172959017 28 0.00040934123964246529 
		29 0.96363529736494413 30 0.033473291075670958
		5 25 0.029227305613210647 26 0.012350675163817989 28 0.00087493279594434334 
		29 0.89950813373260396 30 0.058038952694422948
		5 25 0.00021275913443144138 26 0.00020695846819160469 28 2.0707785469049303e-05 
		29 0.73348136632718364 30 0.26607820828472423
		5 25 0.00049703332549797495 26 0.00047727070903454561 28 5.0128786464277348e-05 
		29 0.73371917902891515 30 0.2652563881500879
		5 25 0.0024441238562017626 26 0.0023179778128512266 28 5.7264501508583135e-05 
		29 0.65861400988895047 30 0.33656662394048797
		5 25 0.0062099879177440727 26 0.0057346023553079351 28 0.00014447491884391899 
		29 0.64930741894135224 30 0.3386035158667518
		5 25 0.0011199702148589454 26 0.0010885490083907047 28 0.00015371705163247793 
		29 0.6153204843393244 30 0.38231727938579346
		5 25 0.019172425405524107 26 0.017685455784735571 28 0.00027211119883560191 
		29 0.57278981775632509 30 0.39008018985457965
		5 25 0.0010189519259893561 26 0.00082459049932137737 28 0.00016252668767564974 
		29 0.94351029343243786 30 0.054483637454575642
		5 25 0.010620031926071387 26 0.0070502066034381933 28 0.00038960645847258409 
		29 0.88430376285714041 30 0.097636392154877419
		5 25 0.056195776871936262 26 0.033483839563058664 28 0.00095934750752782818 
		29 0.73430843074347052 30 0.17505260531400682
		5 25 0.0038620513973212094 26 0.0027207944107993953 28 0.00014016966134431866 
		29 0.93094270883586705 30 0.062334275694667991
		5 25 0.00037606136880402867 26 0.00031337183642041346 28 5.7263352037721745e-05 
		29 0.96698111609515336 30 0.032272187347584469
		5 25 0.0023900368476813989 26 0.0020344793210942194 28 0.00054798113124180845 
		29 0.85262019348302365 30 0.14240730921695888
		5 25 0.00044052044064551386 26 0.00044052044064551386 28 3.0969618636079952e-05 
		29 0.49954399475003647 30 0.49954399475003647
		5 25 0.00021044438918538388 26 0.00021044438918538388 28 1.4928322619512175e-05 
		29 0.49978209144950475 30 0.49978209144950497
		5 25 0.004369717631378881 26 0.004369717631378881 28 6.6971061658259397e-05 
		29 0.49559679683779201 30 0.49559679683779201
		5 25 0.0048227536578257417 26 0.0048227536578257417 28 6.7530128167622063e-05 
		29 0.49514348127809044 30 0.49514348127809044
		5 25 0.0012143888462367277 26 0.0012143888462367277 28 0.0001212689784963491 
		29 0.49872497666451515 30 0.49872497666451515
		5 25 0.025250372423231696 26 0.025250372423231696 28 0.00021436794576488845 
		29 0.47464244360388586 30 0.47464244360388586
		5 25 0.00013846850254879453 26 4.1715214737376057e-05 28 3.3381838723961191e-05 
		29 0.99957837121500825 30 0.00020806322898161126
		5 25 5.9614559065479589e-05 26 2.0371608036289817e-05 28 1.9302611817126373e-05 
		29 0.99977919133615389 30 0.00012151988492728966
		3 28 0.49756014075891558 29 0.49756014075891558 30 0.004879718482168797
		3 28 0.49756246211033145 29 0.49756246211033145 30 0.0048750757793371317
		4 26 0.0023589416804221377 28 0.0016929383553368686 29 0.98135868374204849 
		30 0.014589436222192579
		4 26 0.0029667939799243206 28 0.0017943784891804257 29 0.97993505384609636 
		30 0.015303773684798874
		3 28 0.50057162118114795 29 0.49942810852789604 30 2.7029095606654618e-07
		3 28 0.50043512789808831 29 0.49956448938288456 30 3.8271902703211477e-07;
	setAttr ".wl[304:422].w"
		3 28 0.010574881721972363 29 0.98591203088633494 30 0.0035130873916926647
		3 28 0.010959782914999426 29 0.9854122792802813 30 0.0036279378047192699
		3 28 0.10252914658726005 29 0.89538022948780993 30 0.002090623924929902
		3 28 0.10426123072919283 29 0.89361387959799587 30 0.0021248896728113501
		3 28 0.40789280961134239 29 0.59087108517446174 30 0.001236105214195894
		3 28 0.40914388827364995 29 0.58961268788123444 30 0.0012434238451154713
		3 28 0.13676919890872591 29 0.86323055668055837 30 2.4441071577257754e-07
		3 28 0.18127543950517325 29 0.81872416975666407 30 3.9073816272523196e-07
		5 24 1.4600084815720466e-06 25 1.9515814095721198e-06 28 7.0545370071664946e-05 
		29 0.99992584096823012 30 2.0207180698021723e-07
		5 24 7.2582109858248311e-06 25 1.0240005065166462e-05 28 0.00025667371083440546 
		29 0.99972509020868827 30 7.3786442633341481e-07
		5 24 0.00021934138265133893 25 0.00066106678580179373 28 0.001085895121213937 
		29 0.99779294647269634 30 0.00024075023763664964
		5 24 0.0002933722926590994 25 0.0010067653957505469 28 0.0012357827122249541 
		29 0.9971909967758813 30 0.00027308282348413582
		3 28 0.49945963770845103 29 0.49945963770845092 30 0.0010807245830980632
		3 28 0.49946995686232493 29 0.49946995686232493 30 0.0010600862753501793
		3 28 0.46282061186843404 29 0.53717920969220911 30 1.7843935691335552e-07
		3 28 0.47499221166596106 29 0.52500750904367022 30 2.7929036877675094e-07
		3 28 0.49688691512777511 29 0.5023594935450777 30 0.00075359132714721737
		3 28 0.49708869893699825 29 0.50215365574917203 30 0.0007576453138297334
		4 25 1.6325011935487365e-05 28 0.0032736474706313219 29 0.99670878482657699 
		30 1.2426908562252378e-06
		4 25 3.8998327126857878e-05 28 0.0051907136502476721 29 0.99476829016246548 
		30 1.9978601599841798e-06
		3 28 0.27899717935319535 29 0.71398073944335771 30 0.0070220812034469263
		3 28 0.28033713120103637 29 0.71261212768374438 30 0.0070507411152191902
		5 24 0.0001163040863090817 25 0.00026255265035925467 28 0.00092043537696792556 
		29 0.99864019298633699 30 6.051490002678335e-05
		5 24 0.00017035345084026333 25 0.00042869402276097418 28 0.0011066976206297395 
		29 0.99822163582022228 30 7.2619085546783284e-05
		3 28 0.05284143160150688 29 0.93699463029508268 30 0.010163938103410333
		3 28 0.053618447928785569 29 0.93609673494660961 30 0.010284817124604804
		4 26 0.0032506978248963429 28 0.0057373120115820509 29 0.97839602132631187 
		30 0.012615968837209689
		4 26 0.0038754132778391352 28 0.0059462090466858601 29 0.97718778948335006 
		30 0.012990588192124996
		5 25 0.00041534706953965116 26 9.8390878949810651e-05 28 0.0002473583563329351 
		29 0.99886443909936062 30 0.00037446459581700389
		5 25 0.00067773839161756389 26 0.00013776235460355058 28 0.00030149634404770781 
		29 0.99842942845901739 30 0.00045357445071379446
		3 28 0.5025630445681204 29 0.49743679642586774 30 1.5900601188492409e-07
		3 28 0.50193805394879465 29 0.49806169632002867 30 2.497311767279996e-07
		3 28 0.49936735977887886 29 0.49936735977887864 30 0.0012652804422425396
		3 28 0.49936543682651735 29 0.49936543682651746 30 0.0012691263469651781
		5 25 0.0004037327642402199 26 0.00022733546172777855 28 3.9292961329042117e-05 
		29 0.9958728036220269 30 0.0034568351906761304
		5 25 0.00014215831648172717 26 8.8447723481422126e-05 28 2.1545751194758295e-05 
		29 0.99777731344361986 30 0.0019705347652222722
		5 25 1.4362924287056682e-06 26 9.3039604977576453e-07 28 2.0088103102393415e-07 
		29 0.99997098583277033 30 2.6446597720098365e-05
		5 25 4.7406706079415225e-05 26 2.7968315741330776e-05 28 4.2354332518939117e-06 
		29 0.99938743637129257 30 0.00053295317363470749
		5 25 0.0029315742388401674 26 0.0027266243126185313 28 8.4160974421563668e-05 
		29 0.69391403231147064 30 0.3003436081626491
		5 25 0.00043269187899513782 26 0.0004151420567116753 28 4.1827589788634636e-05 
		29 0.75020465326706243 30 0.24890568520744213
		5 25 0.00012926921239458423 26 0.0001256161077324134 28 1.1649267958983634e-05 
		29 0.78577649967792551 30 0.21395696573398851
		5 25 0.0014246178967007557 26 0.0013546631276157072 28 3.742024688345259e-05 
		29 0.69166277919096331 30 0.30552051953783688
		5 25 0.00016686641961303271 26 0.00012629328004487179 28 1.0223549463113745e-05 
		29 0.9927634136827187 30 0.0069332030681602541
		5 25 2.1366413764447064e-05 26 1.7162141074751896e-05 28 2.2162117544378145e-06 
		29 0.99828771403382666 30 0.0016715411995796184
		5 25 0.00050765988580130995 26 0.00039563621126913781 28 5.7378957487598822e-05 
		29 0.97578726497657287 30 0.023252059968869174
		5 25 0.0012661079578082225 26 0.00092403579947238535 28 8.5444335054025124e-05 
		29 0.96577121406827804 30 0.031953197839387354
		5 25 0.0017572513219539874 26 0.0017572513219539874 28 3.1117085795063701e-05 
		29 0.4982271901351486 30 0.49822719013514838
		5 25 0.00015322860204193458 26 0.00015322860204193458 28 1.0294437001705982e-05 
		29 0.49984162417945738 30 0.49984162417945716
		5 25 0.00032187124834693541 26 0.00032187124834693541 28 2.0848007312372859e-05 
		29 0.49966770474799693 30 0.49966770474799693
		5 25 0.0027194124753284216 26 0.0027194124753284216 28 4.713402611474936e-05 
		29 0.49725702051161419 30 0.49725702051161419
		5 25 0.0079842523664093758 26 0.0079842523664093758 28 9.8061843822114283e-05 
		29 0.49196671671167957 30 0.49196671671167957
		5 25 0.000566977269129532 26 0.000566977269129532 28 4.6662654544260997e-05 
		29 0.49940969140359831 30 0.49940969140359831
		5 25 0.00057108376212579998 26 0.00055337322346259404 28 6.5568706780287162e-05 
		29 0.67542250097358059 30 0.32338747333405066
		5 25 0.00019018361483808376 26 0.00015211237325589839 28 2.2785283911087138e-05 
		29 0.98735676972369746 30 0.012278149004297469
		5 25 0.00086402843301725972 26 0.00063185514148256683 28 4.9311576462170482e-05 
		29 0.97494060899386226 30 0.023514195855175744
		5 25 0.0066923435239203679 26 0.0062367471623020389 28 0.00013315318647830199 
		29 0.62748689175335182 30 0.35945086437394741
		5 25 0.00079290894588364609 26 0.00065362175192612028 28 0.00011128723733516298 
		29 0.94612664563662785 30 0.052315536428227116
		5 25 0.00023988284287970881 26 0.00020360913084777624 28 3.2406771765128504e-05 
		29 0.97259684744072272 30 0.026927253813784743
		5 25 0.0013600701755285519 26 0.0011673664132367226 28 0.00025261703561601855 
		29 0.88813818708038395 30 0.10908175929523468
		5 25 0.00014869330408209702 26 0.00012550281698604939 28 1.8739927612852509e-05 
		29 0.9828807882573829 30 0.016826275693936223
		5 25 0.00073881487989673595 26 0.00060747737707594635 28 0.00010090953994901915 
		29 0.95007124692011657 30 0.048481551282961818
		5 25 0.00074440716795191083 26 0.00063010895292574438 28 0.00011773570214445885 
		29 0.93489943267538289 30 0.063608315501594917
		5 25 0.0070956220724671505 26 0.0050243137223231144 28 0.00027632191399617562 
		29 0.89086971994864383 30 0.096734022342569698
		5 25 0.017161043278921744 26 0.011730738556757669 28 0.00043062446134403269 
		29 0.82842613895914397 30 0.14225145474383261
		5 25 0.0023430889969314693 26 0.0017487790939093055 28 8.9409129473316004e-05 
		29 0.93781699097998739 30 0.058001731799698433
		5 25 0.0013983214718977165 26 0.0010567780461585125 28 5.8955736948093682e-05 
		29 0.95525730352880411 30 0.042228641216191416
		5 25 0.003775381822746881 26 0.0027441220860717195 28 0.0001764132586630789 
		29 0.92190974608784126 30 0.071394336744677128
		5 25 0.0067103363820258657 26 0.0047849608536510626 28 0.00022532703149353013 
		29 0.89229450797934284 30 0.095984867753486774
		3 28 0.50024334984786656 29 0.49975008070554133 30 6.5694465920861108e-06
		3 28 0.50011911371972806 29 0.49986415776778292 30 1.6728512488964016e-05
		5 24 0.0079458797176754029 25 0.0079458797176754029 28 0.49167401391050491 
		29 0.49167401391050491 30 0.00076021274363933687
		3 28 0.49750630322656769 29 0.4975063032265678 30 0.0049873935468645429
		3 28 0.49754395893935738 29 0.49754395893935721 30 0.0049120821212854052
		3 28 0.49957376501282169 29 0.49957376501282169 30 0.00085246997435665917
		3 28 0.50137317623682631 29 0.49862663059935025 30 1.9316382349164907e-07
		3 28 0.5010437085024686 29 0.49895600056644335 30 2.9093108813755339e-07
		3 28 0.49758947412716331 29 0.4975894741271632 30 0.0048210517456735305
		3 28 0.49758711404641343 29 0.49758711404641331 30 0.0048257719071732111
		3 28 0.006917331958535459 29 0.99128275091212337 30 0.0017999171293412196
		3 28 0.03223898292435342 29 0.96757158039391322 30 0.00018943668173339513
		3 28 0.41108531614844862 29 0.58883614027163234 30 7.8543579919090596e-05
		5 25 0.0007499829313448624 26 0.00030759012391997612 28 0.00033430271311137033 
		29 0.99631313248302089 30 0.0022949917486029567
		3 28 0.49996152502441138 29 0.49996152502441116 30 7.6949951177466216e-05
		3 28 0.49996233203503232 29 0.49996233203503265 30 7.5335929935077642e-05
		5 25 0.0047817862853253793 26 0.0011923299186290238 28 0.000654785722332441 
		29 0.98907997624858202 30 0.0042911218251311303
		5 24 0.0009056874132100653 25 0.0023812003873611913 28 0.0050183243215560459 
		29 0.99037716573509216 30 0.0013176221427805221
		5 24 0.00050834774874353715 25 0.00066627508695927303 28 0.02051983107053287 
		29 0.97819011241117648 30 0.00011543368258774463
		5 24 0.00064729249866868474 25 0.00065537172762455086 28 0.38878340166042646 
		29 0.60985582220322887 30 5.8111910051542533e-05
		3 28 0.49919706163750488 29 0.50071547240570513 30 8.746595678989188e-05
		5 24 0.00082106814388157277 25 0.00082106814388157277 28 0.49733138163554141 
		29 0.50095984167823493 30 6.6640398460487098e-05
		5 24 0.00069277502478315423 25 0.00078680553103634056 28 0.076722416123798182 
		29 0.92169294110079047 30 0.0001050622195917985
		3 28 0.10504792907116255 29 0.89479397095653201 30 0.00015809997230547418
		5 24 0.00075618524026440552 25 0.0015369914307333196 28 0.0066591473415020027 
		29 0.99050213110957375 30 0.0005455448779265648
		3 28 0.0096185235134262706 29 0.98959258266515893 30 0.000788893821414775
		5 25 0.0018152935738165272 26 0.00053837219225446632 28 0.0014578279853926976 
		29 0.99372267108168411 30 0.0024658351668521423
		4 26 0.0014315376508205863 28 0.0022138927363634189 29 0.9927082322481986 
		30 0.0036463373646173918
		3 28 0.49995702052815144 29 0.49995702052815133 30 8.5958943697277555e-05
		3 28 0.49996683652019819 29 0.49996683652019808 30 6.6326959603708237e-05
		5 25 0.00074510423350337258 26 0.0005149494450981015 28 0.00018046315354931683 
		29 0.98311353914729449 30 0.015445944020554797
		5 25 0.010078898488254294 26 0.0046792035710386051 28 0.00044206165967016511 
		29 0.95272377040537337 30 0.032076065875663633
		5 25 0.00065390255874995509 26 0.00063179709784545225 28 7.5504117592617204e-05 
		29 0.6799181214768526 30 0.31872067474895932
		5 25 0.007307820693203378 26 0.0067717516970197608 28 0.00014993151467406027 
		29 0.63136234036844086 30 0.35440815572666201
		5 25 0.0014780995945691375 26 0.0012287609031825337 28 0.00028360185799830649 
		29 0.90898911880293842 30 0.088020418841311704
		5 25 0.020733296963728227 26 0.013254199580811778 28 0.00055870871102262749 
		29 0.83685013852376811 30 0.12860365622066933
		5 25 0.00049604918010248027 26 0.00049604918010248027 28 4.0786342370563465e-05 
		29 0.49948355764871238 30 0.49948355764871216
		5 25 0.0073640861840136912 26 0.0073640861840136912 28 9.1714656538308728e-05 
		29 0.4925900564877172 30 0.4925900564877172
		5 25 0.0025929304412419304 26 0.0025929304412419304 28 4.2037648386069577e-05 
		29 0.4973860507345651 30 0.4973860507345651
		5 25 0.00022194574070482051 26 0.00022194574070482051 28 1.5547988990776815e-05 
		29 0.4997702802647998 30 0.4997702802647998
		3 28 0.49998787746949297 29 0.49998787746949286 30 2.4245061014222807e-05
		3 28 0.49998801054453673 29 0.49998801054453684 30 2.3978910926487019e-05
		5 25 0.00037247357036225098 26 0.00035887962357865084 28 3.7112984938308382e-05 
		29 0.74219391838077886 30 0.25703761544034198
		5 25 0.00028816178150775732 26 0.00022683664420612037 28 3.2594730196550496e-05 
		29 0.98398750965433546 30 0.015464897189754091
		5 25 0.00088558602170143584 26 0.00064960490023882557 28 5.667625126862373e-05 
		29 0.97391331471356568 30 0.024494818113225309
		5 25 0.0031013010619351821 26 0.0028947374472800633 28 8.0848155378727038e-05 
		29 0.68119518546202174 30 0.31272792787338433
		5 25 0.00057743702621115009 26 0.00047977792857012566 28 8.0694960983577528e-05 
		29 0.95483667413074347 30 0.044025415953491631
		5 25 0.00090598856382663278 26 0.0007619803867300462 28 0.00014452086990998688 
		29 0.92915831124639348 30 0.069029198933139968
		5 25 0.003651253603544419 26 0.0026561639846844716 28 0.00015590459436379058 
		29 0.92358989713634287 30 0.069946780681064386
		5 25 0.0076577704370093345 26 0.0054127771690861762 28 0.00026560614517782228 
		29 0.8859809292378652 30 0.10068291701086156
		1 28 0.49995478722187836;
	setAttr ".wl[422:423].w"
		2 29 0.49995478722187825 30 9.0425556243425726e-05
		3 28 0.49996467279099177 29 0.49996467279099177 30 7.0654418016470429e-05;
	setAttr -s 31 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.15680631874941806 0.98762937299386533 1.9203235634255452e-17 -0
		 0.98762937299386533 0.15680631874941817 -1.2094971503808443e-16 0 -1.2246467991473527e-16 -3.0814879110195767e-33 -1 0
		 -556.54303159690778 -88.362564335046656 6.815686422329173e-14 1;
	setAttr ".pm[1]" -type "matrix" 0.062080978492094471 0.9980711157575215 -2.6805962794083368e-17 -0
		 -0.99807111575752139 0.06208097849209461 2.431781747614721e-16 0 2.4437325261173941e-16 1.165751815773164e-17 1 -0
		 560.29701578098332 -43.035829167130757 -1.3702319106699251e-13 1;
	setAttr ".pm[2]" -type "matrix" -0.15680631874941806 0.98762937299386544 4.6009198428338808e-17 -0
		 0.98762937299386522 0.15680631874941817 -3.6412788979955656e-16 0 -3.6683793252647465e-16 -1.1657518157731646e-17 -1 0
		 -494.44900661422514 -90.513875048975763 1.909852228003692e-13 1;
	setAttr ".pm[3]" -type "matrix" -0.18411737904022696 -0.98290426326034253 -1.3957429162126204e-16 0
		 0.98290426326034275 -0.18411737904022704 9.8401839147183031e-17 0 -1.2241763996512782e-16 -1.1907067755956352e-16 1 -0
		 -553.24324881388691 105.35215275458967 -5.5307772665672655e-14 1;
	setAttr ".pm[4]" -type "matrix" 0.14368067469030996 -0.9896241022330331 -1.3957429162126204e-16 -0
		 0.98962410223303332 0.14368067469030998 9.8401839147183031e-17 0 -7.7326703334545362e-17 -1.5226452567993771e-16 1 -0
		 -612.77294890466533 -98.111138247060367 -5.5505734429841339e-14 1;
	setAttr ".pm[5]" -type "matrix" 0.03918556669535149 -0.99923195073154225 -1.3957429162126201e-16 -0
		 0.99923195073154247 0.03918556669535149 9.8401839147183043e-17 0 -9.2856963973329775e-17 -1.4332302351953412e-16 1 -0
		 -716.30416625084229 -23.176268293953683 -5.1970899344200789e-14 1;
	setAttr ".pm[6]" -type "matrix" -0.094809092627995709 -0.99549547259395166 -1.3957429162126201e-16 0
		 0.99549547259395188 -0.094809092627995736 9.8401839147183018e-17 0 -1.1119149730874616e-16 -1.2961618631700397e-16 1 -0
		 -769.3250427595915 80.440057302969578 -5.1339961442449999e-14 1;
	setAttr ".pm[7]" -type "matrix" 0.01851534400602068 -0.99982857632513089 -1.3957429162126204e-16 -0
		 0.99982857632513111 0.01851534400602068 9.8401839147183031e-17 0 -9.580070471853827e-17 -1.4137230918591036e-16 1 -0
		 -831.86760311193916 -13.836949993469942 -5.2594911904574821e-14 1;
	setAttr ".pm[8]" -type "matrix" 0.99999999999999956 -1.1553258350005527e-15 -1.3957429162126204e-16 -0
		 1.1587952819525064e-15 0.99999999999999978 9.8401839147183031e-17 -0 1.3957429162126194e-16 -9.8401839147183203e-17 1 -0
		 -2.681930185198262 -892.14763857305729 -5.1393996453069635e-14 1;
	setAttr ".pm[9]" -type "matrix" -1.5766239482585878e-15 2.4677145545861107e-15 0.99999999999999933 -0
		 0.73593101176182885 -0.67705653082088391 2.8513938317956838e-15 -0 0.67705653082088413 0.73593101176182896 -6.6920065569024372e-16 -0
		 -522.48680778374171 542.33987591893447 -4.9103200000020788 1;
	setAttr ".pm[10]" -type "matrix" 3.4980822239674598e-16 2.9074030344419489e-15 0.99999999999999956 -0
		 0.13832224525180573 -0.99038727600292753 2.8513938317956845e-15 0 0.99038727600292775 0.13832224525180556 -6.6920065569024381e-16 -0
		 -20.315310508424602 718.90861069118 -4.9103200000020051 1;
	setAttr ".pm[11]" -type "matrix" 7.2247398346767108e-16 2.8378494569569721e-15 0.99999999999999956 -0
		 0.0092185452926791221 -0.99995750830857133 2.8513938317956842e-15 0 0.99995750830857155 0.0092185452926789278 -6.6920065569024391e-16 -0
		 205.00054560892085 698.27647776334459 1.8655299999980184 1;
	setAttr ".pm[12]" -type "matrix" 6.9514825669555819e-16 2.8446664300672847e-15 0.99999999999999956 -0
		 0.018834962757058496 -0.9998226063547172 2.8513938317956842e-15 0 0.99982260635471742 0.018834962757058309 -6.6920065569024381e-16 -0
		 334.65304677480185 701.52762421506532 8.3328299999980118 1;
	setAttr ".pm[13]" -type "matrix" -4.3011583918344602e-16 2.8302375544423468e-15 -0.99999999999999956 -0
		 0.0011079563370642758 -0.99999938621618911 -2.8510374858595874e-15 0 -0.99999938621618922 -0.0011079563370644666 3.4757632316284618e-16 0
		 -372.72626894604508 693.64346131545778 -8.3328299999981468 1;
	setAttr ".pm[14]" -type "matrix" -4.7931821436030371e-16 2.8223214379433684e-15 -0.99999999999999956 -0
		 0.018515344006025149 -0.99982857632513078 -2.851037485859587e-15 0 -0.99982857632513089 -0.01851534400602534 3.4757632316284609e-16 0
		 -405.54658103968353 686.68793147065594 -8.3328299999981397 1;
	setAttr ".pm[15]" -type "matrix" -0.039185566695350713 0.99923195073154225 8.8199496530749458e-16 -0
		 -0.99923195073154247 -0.039185566695350692 -3.7199933019456096e-16 0 -5.3753042576038589e-16 -8.1335366304822532e-16 0.99999999999999978 -0
		 693.41658891137172 35.532042351583996 403.41714125562231 1;
	setAttr ".pm[16]" -type "matrix" 2.1246014704267292e-15 -2.4460094339290237e-15 -0.99999999999999956 -0
		 -0.73593101176182851 0.67705653082088435 -3.2575685969753614e-15 0 0.67705653082088457 0.73593101176182874 -5.6995409058207253e-16 -0
		 522.48682682667959 -542.33985522008913 4.9103200000024678 1;
	setAttr ".pm[17]" -type "matrix" 8.7206016437896295e-17 -3.2387171333270584e-15 -0.99999999999999956 -0
		 -0.13832224525180481 0.99038727600292775 -3.2575685969753618e-15 0 0.99038727600292797 0.13832224525180481 -5.6995409058207233e-16 -0
		 20.315338364123779 -718.90860680071933 4.910320000002371 1;
	setAttr ".pm[18]" -type "matrix" -3.3192307148995568e-16 -3.3320012565859256e-08 -0.99999999999999911 0
		 -0.0092185452926782426 0.99995750830857089 -3.3318596777028903e-08 0 0.99995750830857166 0.0092185452926782045 -3.0716258523514159e-10 -0
		 -205.00061747979939 -698.27647848807669 -1.8655067334188549 1;
	setAttr ".pm[19]" -type "matrix" 3.2044664045228539e-10 -8.0441621725000886e-08 -0.99999999999999634 -0
		 -0.018834962757057604 0.99982260635471398 -8.043338753095104e-08 0 0.99982260635471742 0.018834962757057531 -1.1947253623882827e-09 -0
		 -334.65311863331425 -701.52762623911212 -8.332773675218526 1;
	setAttr ".pm[20]" -type "matrix" 6.3520487975324653e-16 -7.4469919509060005e-09 0.99999999999999933 -0
		 -0.0011079563370652119 0.99999938621618911 7.4469874186885004e-09 0 -0.99999938621618922 -0.0011079563370652496 -8.2500983843974439e-12 -0
		 372.72675544167942 -693.6434608384958 8.332824834442464 1;
	setAttr ".pm[21]" -type "matrix" 1.2963464074173682e-10 2.235533044725487e-08 0.99999999999999911 -0
		 -0.018515344006026085 0.99982857632513056 -2.2349097946466464e-08 0 -0.999828576325131 -0.018515344006026113 5.4352926022973773e-10 -0
		 405.54678257662613 -686.68787618304839 8.3328452985614483 1;
	setAttr ".pm[22]" -type "matrix" 0.039185566695350012 -0.99923195073154192 -2.0556205081424268e-16 -0
		 0.99923195073154225 0.039185566695350046 9.857608915558894e-16 0 -1.0956517615357539e-15 -5.2602619318358425e-16 0.99999999999999956 -0
		 -693.41623516036577 -35.532028478995038 -403.41700000000048 1;
	setAttr ".pm[23]" -type "matrix" -0.15679621116079887 0.98756904080779062 0.011060642097021305 -0
		 0.98762928672710615 0.15680683367561513 -9.4400928281942299e-05 0 -0.0018276116998478893 0.010909012357140287 -0.99993882477122875 0
		 -509.71336248634782 -101.50439727101816 -35.505351246728459 1;
	setAttr ".pm[24]" -type "matrix" 0.071462979010819633 0.99738192525596758 -0.011060642097021314 -0
		 -0.99744286054030218 0.071468391934084219 9.4400928282064163e-05 0 0.00088464008402846638 0.011025612321109005 0.99993882477122886 -0
		 265.5723931882938 -20.147722938677138 35.505351246728452 1;
	setAttr ".pm[25]" -type "matrix" -0.95844736613851533 -0.28505492213345057 0.011060637849371937 -0
		 -0.28507335966558345 0.95850569674892272 -9.4386645434053941e-05 0 -0.010574779010435114 -0.0032435578234796905 -0.99993882481956176 0
		 30.99052304575579 -46.850594874609897 -35.505351944856777 1;
	setAttr ".pm[26]" -type "matrix" -0.15679621132418931 0.9875690408294221 0.011060637849371939 -0
		 0.98762928672850037 0.15680683367543038 -9.4386645434053914e-05 0 -0.0018275969284890505 0.010909010401533805 -0.99993882481956176 0
		 -29.473817142537658 134.3405408076022 -35.505351944856784 1;
	setAttr ".pm[27]" -type "matrix" 0.15679621384860573 -0.98756905686476415 -0.011059170221222995 -0
		 -0.98762928674967854 -0.15680683354076405 9.4388768125691611e-05 0 -0.001827368890694805 0.010907560596157814 -0.99993884105218078 -0
		 509.71327688297606 101.50444880272651 35.505303207731473 1;
	setAttr ".pm[28]" -type "matrix" -0.07146298031067444 -0.99738194164916549 0.011059155359065369 -0
		 0.99744286055121789 -0.071468391796386407 -9.438983308832972e-05 0 0.00088452276313636888 0.011024130177842983 0.99993884121645304 -0
		 -265.57251857229869 20.147785577698688 -35.505302907505857 1;
	setAttr ".pm[29]" -type "matrix" 0.95844738186473677 0.28505492690540796 -0.011059152028233177 -0
		 0.28507335953348817 -0.95850569678899833 9.4378633065701339e-05 0 -0.010573357126367141 -0.0032431265760458181 -0.99993884125434873 0
		 -30.990574359499234 46.850596658850257 35.505303454949555 1;
	setAttr ".pm[30]" -type "matrix" 0.15679621399085625 -0.98756905704591091 -0.011059152028233179 -0
		 -0.98762928675089245 -0.15680683353921848 9.4378633065700878e-05 0 -0.0018273560288380483 0.010907544217367292 -0.99993884125434895 -0
		 29.473871518447488 -134.34087839885791 35.505305787318981 1;
	setAttr ".gm" -type "matrix" 100 0 0 0 -0 -7.154738859542511e-06 -43.915473937987699 0
		 0 99.999999999998678 -1.6292067961387602e-05 0 0 33.957920074462891 0 1;
	setAttr -s 6 ".ma";
	setAttr -s 31 ".dpf[0:30]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 6 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 6 ".ifcl";
createNode skinCluster -n "skinCluster6";
	rename -uid "24971AA7-4AC0-704E-9635-8ABA4A8BD5BB";
	setAttr -s 568 ".wl";
	setAttr ".wl[0:99].w"
		5 10 0.00070090482773483859 11 0.99735688398840228 12 0.001388026357807185 
		13 0.00038845101718897095 14 0.0001657338088666001
		5 10 0.0054444230275384102 11 0.98021418792521564 12 0.010053356575344957 
		13 0.0029745343153370052 14 0.0013134981565638706
		5 10 0.0033435891080251814 11 0.98835811567952958 12 0.0058009970764385478 
		13 0.0017363485946220094 14 0.00076094954138462671
		5 10 0.010524325320618715 11 0.96442759507864795 12 0.01719661588148259 
		13 0.0054050166845001999 14 0.0024464470347504573
		5 4 0.0039459452283534839 5 0.0081052768293692137 9 0.16622968971094157 
		10 0.81760009785033516 11 0.0041189903810006133
		5 4 0.0036445335732334179 5 0.027096756758411521 9 0.24111070188183886 
		10 0.72488224674727553 11 0.0032657610392406613
		5 4 0.003262991024339437 5 0.0071160081911620029 9 0.17081182394305866 
		10 0.81551281982513968 11 0.0032963570163002565
		5 4 0.0030172189107995026 5 0.023582872924172559 9 0.24753240423771056 
		10 0.72335472433587322 11 0.0025127795914441169
		5 4 0.0003622588425147193 5 0.0011845707825746425 9 0.0052647523462558332 
		10 0.97341242533934447 11 0.019775992689310389
		5 9 1.8593681953315625e-05 10 0.50482343083996062 11 0.49513293021517546 
		12 1.6452610315827933e-05 13 8.5926525947573945e-06
		5 9 9.0980832072219055e-05 10 0.080263002164682559 11 0.9191607314835919 
		12 0.00033171154069054198 13 0.00015357397896281554
		5 4 0.00025949812354270269 5 0.00058469001048869738 9 0.0033080314819370479 
		10 0.9779085225082681 11 0.017939257875763417
		5 9 8.1877254484533417e-06 10 0.49999025368501943 11 0.49999025368501943 
		12 7.41348963600941e-06 13 3.8914148767896133e-06
		5 9 5.3818591475183453e-05 10 0.061444442062310978 11 0.93820658688571457 
		12 0.00020114650807081218 13 9.4005952428503556e-05
		5 10 0.024377298769256765 11 0.97549345026655121 12 7.5020000397759255e-05 
		13 3.4591665844898547e-05 14 1.9639297949415492e-05
		5 9 1.6019987091829149e-05 10 0.49998052660878656 11 0.49998052660878656 
		12 1.5055633466307984e-05 13 7.8711618688157176e-06
		5 4 0.00024400782152907634 5 0.00053560569791327251 9 0.0029259182995946373 
		10 0.97757739426460066 11 0.018717073916362358
		5 9 0.00015375954679434079 10 0.12202799995312789 11 0.87704151265461161 
		12 0.00052869625812699346 13 0.00024803158733900779
		5 9 9.6387169663465149e-06 10 0.51830975738589891 11 0.48166808595896771 
		12 8.2118875617543e-06 13 4.3060506053371755e-06
		5 4 0.000368978378093732 5 0.0012236567877467572 9 0.0056341625489152811 
		10 0.97464026509712409 11 0.01813293718812017
		5 10 0.01513115382075377 11 0.9829606018943734 12 0.0011894152573677533 
		13 0.00047254756549814259 14 0.00024628146200685719
		5 10 0.038294925411616436 11 0.95365740454037229 12 0.0050222412432016986 
		13 0.0019905187469545799 14 0.0010349100578551122
		5 10 0.0093749207117788686 11 0.98958166502243083 12 0.00064765233951712478 
		13 0.00026020004533564782 14 0.00013556188093748816
		5 10 0.015639331636031872 11 0.98169149314881232 12 0.0016683176176852227 
		13 0.00066051441823484732 14 0.00034034317923562965
		5 10 0.0047963643346278281 11 0.99435743158866918 12 0.0005362718893180046 
		13 0.000205595765237469 14 0.00010433642214757166
		5 10 0.0019964666763268445 11 0.99777669846077932 12 0.00014232731007344379 
		13 5.5784958079686529e-05 14 2.8722594740809512e-05
		5 10 0.057058345846379942 11 0.93135252555881587 12 0.0071383365501465805 
		13 0.0029140857391229528 14 0.0015367063055346211
		5 10 0.028433423591365348 11 0.96805768418978633 12 0.0021642172252206333 
		13 0.00088051911300914226 14 0.00046415588061852748
		5 11 0.95935086069097353 12 0.037574052404792356 13 0.002281924816672872 
		14 0.00051149677759008657 15 0.00028166530997114679
		5 11 0.91611179755081973 12 0.076385242941368156 13 0.005470509585923297 
		14 0.0013027587156207781 15 0.00072969120626790052
		5 11 0.92577926030951652 12 0.067302234005527969 13 0.005076296134530655 
		14 0.0011840618881658424 15 0.00065814766225899134
		5 11 0.88485834314359246 12 0.10285339970472415 13 0.0088621423849579994 
		14 0.0021890665539443148 15 0.0012370482127810403
		5 11 0.0052006401899120413 12 0.020894204235844194 13 0.11943875914899811 
		14 0.42723319821262284 15 0.42723319821262284
		5 11 0.0055711484959487705 12 0.021897179536122406 13 0.12175658062484093 
		14 0.42538754567154402 15 0.42538754567154391
		5 11 0.0052527850048449294 12 0.021074931207041233 13 0.1200171106523293 
		14 0.42682758656789233 15 0.42682758656789221
		5 11 0.0056246638732332863 12 0.022078021576752323 13 0.12231435684520828 
		14 0.4249914788524029 15 0.42499147885240313
		5 11 0.57137215151580445 12 0.4195203263230482 13 0.0076978434091921647 
		14 0.00096253892020129069 15 0.00044713983175403114
		5 11 0.013912770290647575 12 0.52047712701197724 13 0.45277700085873263 
		14 0.010168633338867934 15 0.0026644684997747413
		5 11 0.0012103501426162541 12 0.025518420767337183 13 0.50497156298797852 
		14 0.41551230163495007 15 0.052787364467118049
		5 11 0.0021604338885465447 12 0.038406728926891315 13 0.47175870385503271 
		14 0.40958283579967752 15 0.07809129752985193
		5 11 0.025069484588397126 12 0.51922955983272057 13 0.43480932303708453 
		14 0.01634106620629364 15 0.0045505663355042648
		5 11 0.58131393981378343 12 0.41080485477748846 13 0.006603028040268126 
		14 0.00086921760480166202 15 0.0004089597636581837
		5 11 0.56827071672206941 12 0.42398839155223433 13 0.0065602481863890859 
		14 0.00080688237061259875 15 0.00037376116869454674
		5 11 0.01076940204735603 12 0.52947054181621978 13 0.44993978318074612 
		14 0.0078144989292987434 15 0.002005774026379358
		5 11 0.00089986818083321717 12 0.020082088737457496 13 0.52154435695558288 
		14 0.41455663141469995 15 0.042917054711426324
		5 11 0.0017635340643091418 12 0.032988471328369101 13 0.48362896538967048 
		14 0.41257068796428592 15 0.069048341253365481
		5 11 0.021162939017739042 12 0.52830532343892711 13 0.43313641730119168 
		14 0.013662207049624111 15 0.0037331131925182003
		5 11 0.57841106149581278 12 0.41493797004749405 13 0.0055871436187527861 
		14 0.00072409147022251972 15 0.00033973336771784808
		5 11 0.49776862568460373 12 0.49947836025076014 13 0.002439858782640616 
		14 0.00021882546163051714 15 9.4329820365003222e-05
		5 11 0.49917305121458716 12 0.49841899837979542 13 0.0021227758925356278 
		14 0.00019875199113355923 15 8.6422521948259468e-05
		5 11 0.49795225500508167 12 0.50055956474263186 13 0.0013231803244338988 
		14 0.00011543775028225263 15 4.9562177570337887e-05
		5 11 0.49936944379347509 12 0.49936944379347509 13 0.0011153736066824719 
		14 0.00010169417076987627 15 4.4044635597456367e-05
		5 11 0.0015248627863970296 12 0.48800307820748784 13 0.50615339496968392 
		14 0.0036011363929268209 15 0.0007175276435044686
		5 11 0.003479908157230214 12 0.48758557201293323 13 0.49982983804950526 
		14 0.007523177379551986 15 0.0015815044007792818
		5 11 0.0006402537443895819 12 0.48439475832578704 13 0.51312139386706412 
		14 0.0015462001905652172 15 0.00029739387219413475
		5 11 0.0020926744131923069 12 0.48805910874255237 13 0.50429613719464184 
		14 0.0046137614741922194 15 0.00093831817542113644
		5 11 0.0001685334365952992 12 0.0032394324010757893 13 0.48290937285972169 
		14 0.48290937285972158 15 0.030773288442885582
		5 11 6.837095122502923e-05 12 0.0013789336870368644 13 0.49177385935360174 
		14 0.49177385935360174 15 0.015004976654534615
		5 11 0.00035903446207274681 12 0.0063671685628069385 13 0.46922065236081506 
		14 0.46951908195677061 15 0.0545340626575345
		5 11 0.0002078655759973069 12 0.0038584943312589127 13 0.47912876262390874 
		14 0.47953637042333658 15 0.037268507045498458
		5 10 0.0012553649629934559 11 0.99528114668655199 12 0.0024730529333582907 
		13 0.0006924487249452409 14 0.00029798669215090877
		5 10 0.00035496607232580557 11 0.99864768217173849 12 0.00071690921526436965 
		13 0.0001967662290967426 14 8.3676311574656405e-05
		5 10 0.0065110593819333474 11 0.97809594176288184 12 0.010648526296039072 
		13 0.0032788351631006488 14 0.0014656373960451434
		5 10 0.0042699557577039098 11 0.98549537164825762 12 0.0071230985212374382 
		13 0.0021575304520899362 14 0.00095404362071109874
		5 4 0.0029109063159800767 5 0.014220554596524061 9 0.22489932008469576 
		10 0.75535530476424684 11 0.0026139142385532468
		5 4 0.0029351840579780585 5 0.0096649345935097275 9 0.19885104473716675 
		10 0.78567492004726558 11 0.0028739165640798941
		5 4 0.0018794193055038037 5 0.0092695087573377941 9 0.15470188434809823 
		10 0.8322706023708567 11 0.0018785852182034806
		5 4 0.0018582136006231845 5 0.0059795842967708158 9 0.12959787040126608 
		10 0.86058865102561799 11 0.0019756806757220413
		5 9 0.00012122256516449788 10 0.10800046364800987 11 0.89125663515778064 
		12 0.00042297503179533895 13 0.00019870359724968289
		5 9 8.6996577786995513e-05 10 0.087504516272469635 11 0.91194993860939366 
		12 0.00031211058666850028 13 0.00014643795368129818
		5 9 9.180579873251047e-06 10 0.5095299747169586 11 0.49044877681445292 
		12 7.9125906459390335e-06 13 4.1552980692693153e-06
		5 9 8.7148535151685369e-06 10 0.50215455836639145 11 0.49782503442028236 
		12 7.6654565012680006e-06 13 4.0269033098002568e-06
		5 4 0.00038291757230837087 5 0.0011144085012161941 9 0.0055921685131833663 
		10 0.97239852253104309 11 0.020511982882248932
		5 4 0.00039036711757386273 5 0.00098717693016583778 9 0.0053017106421952122 
		10 0.97012093969955182 11 0.023199805610513258
		5 4 0.00019730615425704671 5 0.00056480972348612061 9 0.0027527429332032029 
		10 0.98323659750014125 11 0.013248543688912323
		5 4 0.00019834972183171997 5 0.00048905018315738974 9 0.0025517940025337905 
		10 0.9819658121733803 11 0.014794993919096746
		5 9 1.9624784402227437e-05 10 0.50026187817301804 11 0.49969131756392976 
		12 1.785772151961389e-05 13 9.3217571302840707e-06
		5 9 1.8656481112256092e-05 10 0.49995539333358474 11 0.49999958558649577 
		12 1.7320282644045081e-05 13 9.0443161631976867e-06
		5 9 3.7209006117987067e-05 10 0.040836205800705475 11 0.95891993888716331 
		12 0.00014149954321521091 13 6.5146762798064435e-05
		5 10 0.023636989254768096 11 0.9762359325707749 12 7.385071684182393e-05 
		13 3.3954215667339077e-05 14 1.9273241947869312e-05
		5 10 0.035434996079730774 11 0.95816090445102908 12 0.0039675285909509398 
		13 0.001600060938073916 14 0.00083650994021517572
		5 10 0.021846063175559972 11 0.97443929020800657 12 0.0023126278493410205 
		13 0.00092312936658349855 14 0.00047888940050902615
		5 10 0.022416470814097096 11 0.97495888443292389 12 0.001619448286654655 
		13 0.00065867930392045305 14 0.00034651716240391227
		5 10 0.015834033782483829 11 0.9823753543622431 12 0.0011071575675240055 
		13 0.00044844977529525688 14 0.00023500451245385678
		5 10 0.0051334412903752151 11 0.99425540151269165 12 0.00038323078765804559 
		13 0.00015025354798892731 14 7.7672861286147204e-05
		5 10 0.0019415017226450892 11 0.9978354657623536 12 0.00014016846405670374 
		13 5.4701945005136179e-05 14 2.8162105939537927e-05
		5 10 0.011284001274816246 11 0.9866088340849023 12 0.0013327203434525365 
		13 0.00051248793952132863 14 0.00026195635730759566
		5 10 0.0033828553739039618 11 0.9960237025439761 12 0.00037732485017329874 
		13 0.0001434161401869652 14 7.2701091759708793e-05
		5 11 0.98174969005749901 12 0.017051060438562721 13 0.0008907429196854113 
		14 0.00019888586023966768 15 0.00010962072401324863
		5 11 0.99065361201281277 12 0.008769409233065838 13 0.00043097593714183213 
		14 9.430997395312237e-05 15 5.1692843026539404e-05
		5 11 0.94319456603852436 12 0.051797559302710378 13 0.0036650857059064897 
		14 0.00086191197434044549 15 0.00048087697851823468
		5 11 0.95480339901295608 12 0.041390122033170912 13 0.0028011594305788821 
		14 0.00064653400587685904 15 0.0003587855174171737
		5 11 0.0047409394260527924 12 0.019104484071861518 13 0.11289189501998534 
		14 0.4316313407410502 15 0.4316313407410502
		5 11 0.0045758550619836025 12 0.018634341485079579 13 0.11165725981734823 
		14 0.43256627181779428 15 0.43256627181779428
		5 11 0.004797361534825388 12 0.019302357054133825 13 0.11356647963164095 
		14 0.43116690088969989 15 0.43116690088969989
		5 11 0.004631515508857556 12 0.018831940862852614 13 0.11234360770534871 
		14 0.43209646796147061 15 0.43209646796147061
		5 11 0.71465124877680641 12 0.28454885873757563 13 0.0006846282032670828 
		14 7.8982877028208992e-05 15 3.6281405322624551e-05
		5 11 0.70187401476030353 12 0.29722379483523625 13 0.00077447113563521095 
		14 8.7639273627306962e-05 15 4.007999519770418e-05
		5 11 0.003654132221385538 12 0.60845223050881414 13 0.38498085462994436 
		14 0.0023426838523153948 15 0.00057009878754056109
		5 11 0.0019673390010967844 12 0.62460386404780133 13 0.37179914130172093 
		14 0.0013167363972595846 15 0.00031291925212135536;
	setAttr ".wl[100:199].w"
		5 11 0.00030198721365545241 12 0.0076276925242385624 13 0.58521199054714979 
		14 0.38806575766866497 15 0.018792572046291083
		5 11 0.00016500980373060315 12 0.0044638903968592858 13 0.6253655961695862 
		14 0.35884827605183434 15 0.01115722757798958
		5 11 0.70106105248480977 12 0.29758604079829987 13 0.0011586086286577287 
		14 0.00013321477830034148 15 6.1083309932399323e-05
		5 11 0.7119639954709378 12 0.28681602223328356 13 0.0010416926048476146 
		14 0.00012206979595750436 15 5.6219894973300403e-05
		5 11 0.0035389584853028288 12 0.5902207020348702 13 0.40327365078060706 
		14 0.0023881941603294998 15 0.0005784945388903176
		5 11 0.0056448178662993221 12 0.58335026430214298 13 0.40644784137374473 
		14 0.0036517849421013994 15 0.0009052915157114678
		5 11 0.00029785154692223706 12 0.0076406479002290063 13 0.58587197794168888 
		14 0.38779246340316709 15 0.018397059207992877
		5 11 0.00046923664943554705 12 0.011263176686864605 13 0.55862993220813517 
		14 0.40285527282791916 15 0.026782381627645544
		5 11 0.49940500999612109 12 0.49914973583970601 13 0.0012826111120341295 
		14 0.0001136840867198271 15 4.8958965418880196e-05
		5 11 0.4999603929236146 12 0.49867206753424315 13 0.0012115042829271246 
		14 0.00010896973379993238 15 4.7065525415217862e-05
		5 11 0.49969257814655538 12 0.49969257814655538 13 0.00054621718741158331 
		14 4.7976492953636403e-05 15 2.0650026524083169e-05
		5 11 0.49918036402960325 12 0.50015782493139471 13 0.00058897843149103375 
		14 5.0961841948946911e-05 15 2.1870765561822644e-05
		5 11 0.0013208242558005298 12 0.48749985644614607 13 0.50749799035248133 
		14 0.0030725525404648015 15 0.00060877640510732117
		5 11 0.0018582426927264105 12 0.48806195910010991 13 0.5050386544839558 
		14 0.0041946415997645434 15 0.00084650212344334762
		5 11 0.0010101549563920039 12 0.48634345519449351 13 0.50987118039430568 
		14 0.0023201443554349758 15 0.00045506509937384248
		5 11 0.00061916823178246054 12 0.48390645859324199 13 0.51372544231199713 
		14 0.0014666741111659107 15 0.00028225675181242681
		5 11 0.00011290647047582236 12 0.0021925976208411121 13 0.48718859079988386 
		14 0.48735834417464358 15 0.023147560934155646
		5 11 7.3150593634688799e-05 12 0.0014608554501757457 13 0.49118840330625779 
		14 0.49119460245802321 15 0.016082988191908603
		5 11 0.00015223886046382602 12 0.0029255375653713216 13 0.48404222901690713 
		14 0.48404640916079295 15 0.028833585396464873
		5 11 0.00020627790008071717 12 0.0038572283147462294 13 0.47959595630549967 
		14 0.47971792521709766 15 0.036622612262575689
		5 11 4.3723665426110875e-05 12 0.00041291690482944909 13 0.021184888365673421 
		14 0.48919575373951479 15 0.48916271732455618
		5 11 3.1534851971566924e-05 12 0.0002996188684411414 13 0.016261347114654136 
		14 0.49172337923014187 15 0.49168411993479127
		5 11 0.00010538506067236373 12 0.00095288912341413802 13 0.038612433232940703 
		14 0.48016464629148642 15 0.48016464629148642
		5 11 8.6391037895358082e-05 12 0.00078574533041769084 13 0.033399605834840938 
		14 0.48286412889842295 15 0.48286412889842295
		5 11 8.8156264744266533e-07 12 8.5527289923588781e-06 13 0.00060559860199238796 
		14 0.49969248355318391 15 0.49969248355318391
		5 11 4.886538729460613e-06 12 4.6705354167492603e-05 13 0.0029872288296661264 
		14 0.49848058963871833 15 0.49848058963871855
		5 11 1.1209497085055085e-05 12 0.00010641561326929504 13 0.0063534264601684378 
		14 0.49676447421473863 15 0.49676447421473863
		5 11 4.3394922506553411e-06 12 4.1813100926056792e-05 13 0.0027511681063467209 
		14 0.49860133965023834 15 0.49860133965023823
		5 10 0.0021968982707410139 11 0.99218059648938506 12 0.00395682022074455 
		13 0.0011613069477851982 14 0.00050437807134432399
		5 10 0.0009801121309145895 11 0.99637387979254732 12 0.0018824040158831584 
		13 0.00053450009835362465 14 0.00022910396230132254
		5 10 0.0081789004998002388 11 0.97098992888112445 12 0.014463663520390132 
		13 0.0044000801890446131 14 0.0019674269096407329
		5 10 0.010892170096765377 11 0.96266855550909125 12 0.018173518861009499 
		13 0.0056907672214051123 14 0.0025749883117286458
		5 4 0.0040067129003388737 5 0.03783706876757105 6 0.0036787627984718805 
		9 0.26772449223988443 10 0.6867529632937337
		5 4 0.004344501355754783 5 0.040801930656231618 6 0.0040358215777943534 
		9 0.28484037713720867 10 0.66597736927301054
		5 4 0.0028648424018123736 5 0.0059154006693994971 9 0.15177824694484082 
		10 0.83654154565136007 11 0.002899964332587125
		5 4 0.0024613919759564366 5 0.0049681782902931057 9 0.12956771234299985 
		10 0.86047354704021162 11 0.002529170350539049
		5 10 0.041037431272904573 11 0.95874693072841799 12 0.00012458515006208131 
		13 5.798337635711842e-05 14 3.3069472258220768e-05
		5 10 0.02200270055082924 11 0.97788752516408306 12 6.3639028652800325e-05 
		13 2.9417445659562327e-05 14 1.6717810775427233e-05
		5 9 8.2630375749786801e-06 10 0.49999007354736735 11 0.49999007354736735 
		12 7.6030509816820987e-06 13 3.9868167087638227e-06
		5 9 1.1059939825275026e-05 10 0.49998657912791816 11 0.49998657912791805 
		12 1.0360144543738199e-05 13 5.4216597949246141e-06
		5 4 0.00018403505685280053 5 0.00040355537746353666 9 0.0023122748974060965 
		10 0.98342276983121557 11 0.01367736483706186
		5 4 0.00013382558335612042 5 0.00029017017796503504 9 0.0016465236680111653 
		10 0.98715880748042772 11 0.010770673090239912
		5 9 0.00010925829661684403 10 0.092747642382544557 11 0.90657353429052967 
		12 0.00038887999530146501 13 0.00018068503500742169
		5 9 0.00014519603759618647 10 0.11552031256790035 11 0.88359738722799763 
		12 0.00050220635089445937 13 0.00023489781561128708
		5 9 1.0524313681261614e-05 10 0.51107877772817611 11 0.48889672928438621 
		12 9.1747187400495398e-06 13 4.7939550162733219e-06
		5 9 7.5998301538498223e-06 10 0.52001424527304374 11 0.47996824323623943 
		12 6.5055581247732718e-06 13 3.4061024382221433e-06
		5 4 0.00039982196731469646 5 0.0013881901340089418 9 0.006004927792508539 
		10 0.97237026278950922 11 0.019836797316658588
		5 4 0.00047300996277774982 5 0.0016490287883327143 9 0.0072017757812249013 
		10 0.96880654838640845 11 0.021869637081256232
		5 10 0.052997586713580971 11 0.93553355552140616 12 0.0071061088476848926 
		13 0.0028619422725107506 14 0.001500806644817012
		5 10 0.063420776747627056 11 0.9230191744635492 12 0.0083466846701314677 
		13 0.0034115664045374161 14 0.0018017977141549981
		5 10 0.02033234992521454 11 0.9770966334132386 12 0.0015968165580277506 
		13 0.00063946296502829071 14 0.00033473713849080665
		5 10 0.027708659491498994 11 0.96880901529720587 12 0.0021513886799072548 
		13 0.00087190546658932324 14 0.00045903106479849501
		5 10 0.010485776852150651 11 0.98773413320887893 12 0.0011174133764944923 
		13 0.00043811816050675278 14 0.00022455840196901279
		5 10 0.0051309380009131305 11 0.99398895496367701 12 0.00055639396703018875 
		13 0.00021458291256559815 14 0.00010913015581414598
		5 10 0.0052530222374513698 11 0.99417022426912993 12 0.00035928510173849961 
		13 0.00014318992000244844 14 7.4278471677773482e-05
		5 10 0.0019307637425667512 11 0.99785620695315258 12 0.00013343461165051234 
		13 5.2515439684800011e-05 14 2.7079252945411276e-05
		5 11 0.91886693708356093 12 0.073531745103049148 13 0.005580935610075298 
		14 0.0012989555002717017 15 0.00072142670304286053
		5 11 0.93268809092560334 12 0.061523752574952371 13 0.0042705732997374533 
		14 0.00097710101941668849 15 0.00054048218029008831
		5 11 0.89264928783136843 12 0.096760463069367683 13 0.0076710843185489909 
		14 0.0018674122397031928 15 0.0010517525410117265
		5 11 0.88045981678094143 12 0.10684864803683286 13 0.0091497928254964372 
		14 0.0022625549739764645 15 0.001279187382752888
		5 11 0.0057405219417960823 12 0.022782909218467397 13 0.12541621251766183 
		14 0.42303017816103733 15 0.42303017816103733
		5 11 0.0057208475137729619 12 0.02271602675537052 13 0.12521493771190254 
		14 0.42317409400947698 15 0.42317409400947698
		5 11 0.0058360541307756943 12 0.022772113602989037 13 0.12437369129485865 
		14 0.42350907048568825 15 0.42350907048568825
		5 11 0.0058560009264916526 12 0.022838821782333853 13 0.12457374030814011 
		14 0.42336571849151716 15 0.42336571849151716
		5 11 0.57064136863121506 12 0.42016173178497351 13 0.0076839678881185213 
		14 0.0010278428628291137 15 0.000485088832863695
		5 11 0.56759841594685578 12 0.42355483161191509 13 0.0073972164489407907 
		14 0.0009850304271967333 15 0.00046450556509154082
		5 11 0.027460924616540535 12 0.51562053277464959 13 0.43403929557239979 
		14 0.017852418208333113 15 0.0050268288280769288
		5 11 0.026368912826314119 12 0.51790254737600938 13 0.43384000275248763 
		14 0.017098259282707487 15 0.0047902777624814069
		5 11 0.0023462273668245779 12 0.040717251765731328 13 0.46686809725284006 
		14 0.40801734541871038 15 0.08205107819589369
		5 11 0.0022346189974694099 12 0.039292852512242578 13 0.469763420438985 
		14 0.40894944325519794 15 0.079759664796104968
		5 11 0.54257053340472328 12 0.44224623421058651 13 0.012740926001167189 
		14 0.0016638163961679119 15 0.0007784899873552421
		5 11 0.54504377445171193 12 0.43932495709230862 13 0.013106605259321617 
		14 0.0017194697535633824 15 0.00080519344309453573
		5 11 0.020147035380901065 12 0.50438637444146683 13 0.45617655951650221 
		14 0.015168207138447515 15 0.0041218235226824886
		5 11 0.021098444213442379 12 0.50266178913575288 13 0.45598262959819114 
		14 0.015910879215562636 15 0.0043462578370509165
		5 11 0.0018032389864592714 12 0.034901022109297283 13 0.48224108717318337 
		14 0.41279063538693389 15 0.068264016344126183
		5 11 0.0019046113423671061 12 0.036360169674210241 13 0.47906501331350476 
		14 0.41205167159114703 15 0.07061853407877082
		5 11 0.49710515751349171 12 0.49986946736201204 13 0.0026822416181910137 
		14 0.0002398322627521986 15 0.00010330124355306973
		5 11 0.49698220417032318 12 0.49954877652576202 13 0.0030727015574137451 
		14 0.00027690908703763018 15 0.0001194086594634598
		5 11 0.49940501600115944 12 0.49919267070427115 13 0.0012394794606104753 
		14 0.00011359432645898339 15 4.9239507500040907e-05
		5 11 0.49943966415736063 12 0.49943966415736063 13 0.00099146702947055295 
		14 9.0165470048648958e-05 15 3.9039185759553718e-05
		5 11 0.0012073472268721163 12 0.48747640516694152 13 0.50784742261262328 
		14 0.0028981008677673326 15 0.00057072412579571618
		5 11 0.0014769015579855734 12 0.48800470549498204 13 0.5062941025603549 
		14 0.0035237305515672004 15 0.00070055983511036954
		5 11 0.0022224091137892704 12 0.48808067118345638 13 0.50381908033324119 
		14 0.0048815131979861861 15 0.00099632617152699277
		5 11 0.0018855913462422417 12 0.48794655608947957 13 0.50516115343227375 
		14 0.0041645464846775601 15 0.00084215264732699288
		5 11 0.00021560206939385323 12 0.0039890961911483117 13 0.47852770951053714 
		14 0.47895850874201434 15 0.038309083486906349
		5 11 0.00018088861896201454 12 0.0033886766374631554 13 0.48113693377514577 
		14 0.48161202900204614 15 0.033681471966382832
		5 11 0.00012458380661850045 12 0.0024475510927454511 13 0.48659330464528094 
		14 0.48659330464528083 15 0.024241255810074309
		5 11 0.00015388994419437792 12 0.0029851163948120107 13 0.48415363197585054 
		14 0.48415363197585054 15 0.028553729709292586
		5 11 0.0046827317864121263 12 0.019032586227774425 13 0.11310160243013484 
		14 0.43159153977783937 15 0.43159153977783926
		5 11 0.004701377976614584 12 0.019098580538958194 13 0.11332802724858981 
		14 0.43143600711791874 15 0.43143600711791863
		5 11 0.0047298353408357884 12 0.019063303592660306 13 0.11274345349111213 
		14 0.43173170378769593 15 0.43173170378769593
		5 11 0.0047486096846468652 12 0.019129224334322664 13 0.11296908014204486 
		14 0.43157654291949288 15 0.43157654291949288
		5 11 0.00011437546137413436 12 0.0010306976426546278 13 0.040845309389233697 
		14 0.47900480875336876 15 0.47900480875336876
		5 11 0.00010690314499682373 12 0.00096543579747861174 13 0.038921559769483277 
		14 0.48000305064402066 15 0.48000305064402066
		5 11 9.5354052811962291e-05 12 0.0008861149296798855 13 0.03867825828788142 
		14 0.48021374885347251 15 0.48012652387615423
		5 11 0.0001023816254013807 12 0.00094934548531724525 13 0.040693594083872769 
		14 0.47916926906928242 15 0.47908540973612623
		5 11 0.00041327395741772721 12 0.0038880224340631687 13 0.12767740589517179 
		14 0.44931572363512889 15 0.41870557407821835
		5 11 0.00030182047079807376 12 0.0028980079274247474 13 0.11049795690868409 
		14 0.46171398446951445 15 0.42458823022357867
		5 11 0.00024018072828689517 12 0.0024032918304130843 13 0.10664932971142928 
		14 0.47303285318315136 15 0.41767434454671937
		5 11 0.00015502916435490325 12 0.001584702491618112 13 0.084695679340909311 
		14 0.49269236030941632 15 0.42087222869370133;
	setAttr ".wl[200:299].w"
		5 11 4.8197295360825044e-05 12 0.00050658451509373408 13 0.038707411003581674 
		14 0.54407824164919771 15 0.41665956553676609
		5 11 7.5441686109975852e-05 12 0.00077599653280134074 13 0.050951737374568054 
		14 0.5197623869308674 15 0.42843443747565324
		5 11 0.00012671108658277399 12 0.0012809082273208341 13 0.070884049632754867 
		14 0.49790161665696148 15 0.42980671439638002
		5 11 9.1352413774802577e-05 12 0.00094338277243680277 13 0.059756736454859162 
		14 0.51439674550817371 15 0.42481178285075549
		5 11 0.00033993793613033726 12 0.0033443356846555167 13 0.12669597907713079 
		14 0.45796022467809733 15 0.41165952262398603
		5 11 0.00031179418708754397 12 0.0030844988121024458 13 0.12191794803583168 
		14 0.46175429325876277 15 0.41293146570621547
		5 11 0.0003492073175901177 12 0.0033181230923490703 13 0.11796422931047769 
		14 0.45592419234224674 15 0.42244424793733643
		5 11 0.0003792384725129707 12 0.0035839746070446209 13 0.12249138675025534 
		14 0.45268454523648954 15 0.42086085493369757
		5 11 0.20350267844478323 12 0.77381445823581962 13 0.021148903472314028 
		14 0.0011113748318335695 15 0.00042258501524970624
		5 11 0.1659675675025441 12 0.81843551386847657 13 0.014599276160716603 
		14 0.00072409541048264658 15 0.00027354705777994919
		5 11 0.24277793040776682 12 0.7343995842189307 13 0.021111169402487039 
		14 0.0012342913425437614 15 0.00047702462827168505
		5 11 0.20884700356850766 12 0.77477027231159434 13 0.015215067311747555 
		14 0.00084365259473165506 15 0.00032400421341879763
		5 11 0.14877479539893285 12 0.84119757583200339 13 0.0093746008387526926 
		14 0.00047338368364446354 15 0.00017964424666655978
		5 11 0.13355450374122593 12 0.85684574201725838 13 0.0089978028950570381 
		14 0.00043702363322530366 15 0.00016492771323341866
		5 11 0.085928611023407847 12 0.90904654445590682 13 0.0047252311722537519 
		14 0.0002178559612080267 15 8.17573872235046e-05
		5 11 0.10093087204192898 12 0.89355776996206393 13 0.0051695104153333636 
		14 0.00024818466628663349 15 9.3662914387223801e-05
		5 11 0.22918659763977905 12 0.74027411211261485 13 0.028388294372350985 
		14 0.0015565542502433201 15 0.000594441625011775
		5 11 0.22145201293144037 12 0.75016811850344989 13 0.026410579147486238 
		14 0.0014257753237046794 15 0.00054351409391884919
		5 11 0.21766739428557369 12 0.76476506359205387 13 0.016297752982610967 
		14 0.00091696687704849933 15 0.00035282226271307946
		5 11 0.22763682195613053 12 0.75299814296249978 13 0.017945003263546172 
		14 0.0010249546987716573 15 0.00039507711905187743
		5 11 0.30468441426567294 12 0.64833715607618825 13 0.042907660426868645 
		14 0.0029213105815633178 15 0.0011494586497068752
		5 11 0.49900448179722035 12 0.49900448179722035 13 0.0017591746390588634 
		14 0.00016176577441993765 15 7.009599208050279e-05
		5 11 0.49808328198533425 12 0.5002690718188525 13 0.0014630532925905477 
		14 0.00012908322706378067 15 5.550967615893822e-05
		5 11 0.49667474330051126 12 0.50103252207618831 13 0.0020365230523039257 
		14 0.00017922626708212944 15 7.6985303914453156e-05
		5 11 0.49897563054183425 12 0.49945995415757372 13 0.0013866921667037315 
		14 0.0001241698285111642 15 5.3553305377149478e-05
		5 11 0.27538958588773899 12 0.67549675931307096 13 0.045225676244265191 
		14 0.0028025385069449641 15 0.0010854400479798851
		5 11 0.28125969477687479 12 0.67230284893887438 13 0.042691857085838343 
		14 0.002697137105769088 15 0.0010484620926434049
		5 11 0.29124028212396269 12 0.66297714685832421 13 0.041975579728257939 
		14 0.0027372867749793947 15 0.0010697045144758008
		5 11 0.10603354820197151 12 0.88149547797581074 13 0.011758712169794914 
		14 0.00051983245124527153 15 0.00019242920117754886
		5 11 0.13536092853475715 12 0.84519107000372395 13 0.01830399965304115 
		14 0.00083442078365944061 15 0.00030958102481827262
		5 11 0.16985485150447829 12 0.80968270949545607 13 0.019126172894029399 
		14 0.00097062841477819136 15 0.00036563769125800817
		5 11 0.12054208932549558 12 0.86646566787742474 13 0.012214231743786389 
		14 0.00056679137500414739 15 0.00021121967828916186
		5 11 0.24997646794288739 12 0.6913947684583297 13 0.054156336741065951 
		14 0.0032349779948316303 15 0.0012374488628853367
		5 11 0.24017220105573603 12 0.70196501513878451 13 0.053588679241252518 
		14 0.0030958330268648117 15 0.001178271537362209
		5 11 0.26653279154826914 12 0.6777592397356178 13 0.051220580076028123 
		14 0.0032371958287940993 15 0.0012501928112907974
		5 11 0.26123557416446791 12 0.6813356073449347 13 0.052882143124444031 
		14 0.0032829125285476013 15 0.0012637628376056787
		5 11 0.066484187082846979 12 0.40944369223529842 13 0.40920019068939145 
		14 0.084812345268828235 15 0.030059584723635017
		5 11 0.065898345139331863 12 0.40976475745315699 13 0.40962047424522374 
		14 0.084731447490095368 15 0.029984975672192202
		5 11 0.078730275239458108 12 0.39837916760025027 13 0.39616842012085091 
		14 0.092338373411225541 15 0.034383763628215061
		5 11 0.076607678579951238 12 0.40053672600544504 13 0.39860720710543268 
		14 0.090764979512420532 15 0.033483408796750484
		5 11 0.078639878390727891 12 0.39126221251701809 13 0.39118694601376319 
		14 0.10056153749530271 15 0.038349425583188129
		5 11 0.077561725333022705 12 0.39225480887077113 13 0.3922244713916816 
		14 0.099985604808668999 15 0.037973389595855611
		5 11 0.09069885007219243 12 0.38024740357197595 13 0.37887627517752348 
		14 0.1072914986370872 15 0.042885972541220931
		5 11 0.089329264234158226 12 0.38142598302187281 13 0.38027189124190436 
		14 0.10658737420120645 15 0.042385487300858234
		5 4 0.00040156486519776443 5 0.01206216704966427 6 0.00028981474254832963 
		9 0.93432634244178392 10 0.052920110900805784
		5 4 0.0004339711141213253 5 0.0079108012435172449 6 0.00026141893613575374 
		9 0.88730584463247408 10 0.10408796407375154
		5 3 0.0019394236721925254 4 0.0072314271765973707 5 0.011570066253050704 
		9 0.48962954144907972 10 0.48962954144907972
		5 3 0.0030952464486734593 4 0.011187747518068301 5 0.018259780866991905 
		9 0.48372861258313321 10 0.48372861258313321
		5 4 0.0029547993100272462 5 0.0095101997145028306 6 0.00093059683633107092 
		9 0.49330220206956943 10 0.49330220206956943
		5 3 0.0014161461026456514 4 0.0057555501719487962 5 0.010349864027542708 
		9 0.49123921984893149 10 0.49123921984893137
		5 4 0.0054313943059319421 5 0.015999269887978871 6 0.0016204590020661041 
		9 0.48847443840201155 10 0.48847443840201155
		5 4 0.0018098496161047665 5 0.011660441392702975 6 0.00077542885049985402 
		9 0.54927511924973926 10 0.43647916089095318
		5 4 0.0028345586157590566 5 0.039798154571534822 6 0.0016848788354792308 
		9 0.6959353209595619 10 0.25974708701766502
		5 4 0.0036957206262429845 5 0.021849248162627212 6 0.0015497119455225765 
		9 0.52425424094325612 10 0.44865107832235113
		5 4 0.00079908939811765159 5 0.022094423435499572 6 0.00056844642001918828 
		9 0.88933654652760163 10 0.087201494218761902
		5 3 0.0015385306547925945 4 0.0059978194536323649 5 0.010000970966892654 
		9 0.49123133946234121 10 0.49123133946234121
		5 4 0.00063682271317389927 5 0.0096520038142683413 6 0.00022126150883354365 
		9 0.96337425409092103 10 0.026115657872803275
		5 4 0.0014429268127547318 5 0.01602577096423561 6 0.0004510469510439441 
		9 0.91570618901428347 10 0.066374066257682288
		5 4 0.01601285308402273 5 0.036994835258161697 6 0.0025429694912981232 
		9 0.49235472834775212 10 0.45209461381876531
		5 4 0.018171729827988894 5 0.042572693419667303 6 0.0029882240605485027 
		9 0.49052414498180191 10 0.44574320770999337
		5 4 0.0091184078958138313 5 0.034400187123642588 6 0.0019044757926029447 
		9 0.57835518702595534 10 0.3762217421619854
		5 4 0.013855129128492119 5 0.034584706934000065 6 0.0023069495370475609 
		9 0.50112018090313859 10 0.44813303349732175
		5 4 0.012541262604295974 5 0.043946303766591317 6 0.0025705387198976032 
		9 0.5543273341843814 10 0.38661456072483369
		5 4 0.0051455431092596359 5 0.031041354280291478 6 0.0013134154873344849 
		9 0.72444289269888928 10 0.23805679442422512
		5 4 0.0036726633455421392 5 0.035977176920954042 6 0.0011540607001916004 
		9 0.83377698295668656 10 0.12541911607662573
		5 4 0.0079386902075751532 5 0.044164320668515736 6 0.0020173853868742163 
		9 0.67121634367520067 10 0.27466326006183434
		5 4 0.0081912980810869649 5 0.031779458092683621 6 0.0016695641313147643 
		9 0.59368122643116894 10 0.36467845326374582
		5 4 0.0037357318073607091 5 0.026242832772315896 6 0.00098074133795406906 
		9 0.785008629428254 10 0.18403206465411537
		5 4 0.0011159160838129271 5 0.01606944352681472 6 0.00038673907103244266 
		9 0.94079433409501567 10 0.04163356722332421
		5 4 0.014554935953245942 5 0.034505514957460723 6 0.002339242142352988 
		9 0.49557476041321891 10 0.45302554653372151
		5 4 0.0071676726305308488 5 0.028544923465430087 6 0.001469540749512738 
		9 0.60460800145017979 10 0.3582098617043466
		5 4 0.0029617767013879028 5 0.021457380529832409 6 0.00077765306688571008 
		9 0.81127180182157355 10 0.1635313878803204
		5 9 6.3264668011530839e-05 10 0.53298101418897093 11 0.46688136229178795 
		12 4.8660640344646765e-05 13 2.5698210884970338e-05
		5 9 2.920925704547867e-05 10 0.51319160400356623 11 0.48674360977878206 
		12 2.3233693269806375e-05 13 1.2343267336338275e-05
		5 9 4.6248436633494079e-05 10 0.50296581556652697 11 0.49692886648059514 
		12 3.8644552798264674e-05 13 2.0424963446286048e-05
		5 9 4.1010950617553745e-05 10 0.56383517442197795 11 0.43607789762089211 
		12 2.9994785895558657e-05 13 1.5922220616821277e-05
		5 9 5.2710896541541122e-05 10 0.50787695466418814 11 0.49200420696792002 
		12 4.3284093837442097e-05 13 2.2843377512810666e-05
		5 9 5.8555114855577493e-05 10 0.51715104944940216 11 0.48271893861989923 
		12 4.6776741534579062e-05 13 2.4680074308595587e-05
		5 9 3.4387604170327355e-05 10 0.53019338966598029 11 0.46973194409484054 
		12 2.6292564867562462e-05 13 1.3986070141388523e-05
		5 9 3.8336892702741746e-05 10 0.54801543044848977 11 0.45190257424296515 
		12 2.8501476533978243e-05 13 1.5156939308376372e-05
		5 9 3.4214116992767085e-05 10 0.5696067254368764 11 0.43032058792638322 
		12 2.5148754710738309e-05 13 1.3323765036968391e-05
		5 9 4.0696713123727663e-05 10 0.55227621440986863 11 0.4476362481986666 
		12 3.0646787127151779e-05 13 1.6193891214054773e-05
		5 9 3.1537426374904045e-05 10 0.50334957648078438 11 0.49657879002975625 
		12 2.622203273560195e-05 13 1.3874030348881692e-05
		5 9 2.6733106764998622e-05 10 0.50749975807938164 11 0.49244028311303351 
		12 2.1709092716579611e-05 13 1.1516608103324743e-05
		5 17 0.00070089960673778821 18 0.99735697405264589 19 0.0013879694939534532 
		20 0.00038842947284273174 21 0.00016572737382018581
		5 17 0.0054445926522592256 18 0.98021408412038413 19 0.010053334830262302 
		20 0.0029744895249786329 21 0.0013134988721157119
		5 17 0.0033435961238775245 18 0.98835838255324604 19 0.0058008225060917264 
		20 0.0017362711804101935 21 0.00076092763637436933
		5 17 0.010524580402056739 18 0.96442759926886668 19 0.017196482917081739 
		20 0.0054049047525601985 21 0.0024464326594346281
		5 4 0.0039459703208414324 5 0.008105318461807693 16 0.16623169314202663 
		17 0.81759808066185957 18 0.0041189374134646805
		5 4 0.0036445539833300632 5 0.027097016191993305 16 0.2411133412268823 
		17 0.72487937150355164 18 0.0032657170942426822
		5 4 0.0032630053144005943 5 0.0071160318334175243 16 0.17081379728132784 
		17 0.81551085652473643 18 0.003296309046117483
		5 4 0.0030172305028706393 5 0.023583066018774735 16 0.24753508656716622 
		17 0.72335187524900024 18 0.0025127416621881847
		5 4 0.00036226232571719109 5 0.0011845836970432154 16 0.0052648230903133836 
		17 0.97341265720902026 18 0.019775673677906006
		5 16 1.8594113075805357e-05 17 0.50482629899192566 18 0.49513006150532535 
		19 1.6452744006255511e-05 20 8.5926456668189639e-06
		5 16 9.0982644428823581e-05 17 0.080266585009187014 18 0.91915714654969338 
		19 0.00033171278362548011 20 0.00015357301306535875
		5 4 0.00025949863956016236 5 0.00058469141161136048 16 0.0033080509946567735 
		17 0.9779089191206678 18 0.017938839833503903
		5 16 8.187627479991472e-06 17 0.49999025390203367 18 0.49999025390203355 
		19 7.413292235678828e-06 20 3.8912762170923011e-06
		5 16 5.3817934912077873e-05 17 0.061445928759602989 18 0.93820510993076611 
		19 0.00020114097410759343 20 9.4002400611270704e-05
		5 17 0.024377915588366775 18 0.97549283754094407 19 7.5017836629087152e-05 
		20 3.459031009309254e-05 21 1.963872396698574e-05
		5 16 1.6019879364788693e-05 17 0.49998052694328898 18 0.49998052694328898 
		19 1.5055311587403869e-05 20 7.8709224698919659e-06;
	setAttr ".wl[300:399].w"
		5 4 0.00024400916125512449 5 0.00053560883246482973 16 0.0029259450608363474 
		17 0.97757775278313086 18 0.018716684162312869
		5 16 0.00015376128722561439 17 0.122032233334896 18 0.87703728363331246 
		19 0.00052869378792923651 20 0.00024802795663675267
		5 16 9.6389514323755113e-06 17 0.51831627488659249 18 0.48166156814627198 
		19 8.2119635755236112e-06 20 4.3060521276432366e-06
		5 4 0.00036898082781832841 5 0.0012236665621203405 16 0.0056342230436065696 
		17 0.974640524213083 18 0.018132605353371901
		5 17 0.0151317455686564 18 0.98296000666278383 19 0.0011894206651662691 
		20 0.00047254439887660458 21 0.00024628270451703988
		5 17 0.038296060135459976 18 0.95365630510516752 19 0.005022232831069358 
		20 0.0019904930880680267 21 0.0010349088402352373
		5 17 0.0093750173935349106 18 0.98958159968267834 19 0.00064763477434469873 
		20 0.00026018993479429229 21 0.00013555821464793252
		5 17 0.01563948515233865 18 0.98169141578134222 19 0.0016682749733008431 
		20 0.00066048960270675516 21 0.00034033449031161328
		5 17 0.0047963932361814159 18 0.99435743081924122 19 0.00053625561930846261 
		20 0.0002055870339212326 21 0.00010433329134770061
		5 17 0.0019964736243419435 18 0.99777670006208985 19 0.0001423223543283965 
		20 5.5782355790963845e-05 21 2.8721603448790647e-05
		5 17 0.05705975084301794 18 0.93135122038170359 19 0.0071382948637049451 
		20 0.0029140362836459965 21 0.0015366976279275269
		5 17 0.02843429112138076 18 0.96805683787146002 19 0.0021642101378849593 
		20 0.00088050638903653625 21 0.00046415448023775819
		5 18 0.95935390276004917 19 0.037571265840299635 20 0.0022817096284399943 
		21 0.00051146663004764256 22 0.00028165514116354772
		5 18 0.91611328656679281 19 0.076383878086973209 20 0.0054703587488845098 
		21 0.0013027671327860177 22 0.00072970946456353634
		5 18 0.92578361102177487 19 0.067298365602735333 20 0.0050758860552308303 
		21 0.0011840059799060468 22 0.00065813134035294988
		5 18 0.88486085726104691 19 0.1028511858570276 20 0.0088618296859908352 
		21 0.0021890601133940724 22 0.0012370670825407591
		5 18 0.0052005310431753882 19 0.020894493013262658 20 0.11944028669960453 
		21 0.42723234462197879 22 0.42723234462197868
		5 18 0.0055712585330467655 19 0.021898332152250566 20 0.12176198431436462 
		21 0.425384212500169 22 0.425384212500169
		5 18 0.0052526756986728002 19 0.021075225018586403 20 0.12001864914839884 
		21 0.42682672506717101 22 0.42682672506717101
		5 18 0.0056247743846565773 19 0.022079180243156207 20 0.12231975885247878 
		21 0.42498814325985423 22 0.42498814325985423
		5 18 0.57138406246648932 19 0.41950930927906244 20 0.0076970103085450981 
		21 0.00096248812656274309 22 0.00044712981934038107
		5 18 0.013913539600291191 19 0.52049584183821496 20 0.45275801777400909 
		21 0.010168116009765677 22 0.0026644847777190926
		5 18 0.0012103368116824957 19 0.025520828988043219 20 0.50498274342096294 
		21 0.41549863030411188 22 0.052787460475199555
		5 18 0.0021606298125638416 19 0.038413237781539301 20 0.47176376405094428 
		21 0.40957196837052728 22 0.078090399984425282
		5 18 0.02507192920143007 19 0.51924466382823586 20 0.43479178396402041 
		21 0.01634089511661594 22 0.0045507278896976715
		5 18 0.5813233708315354 19 0.41079564333444507 20 0.0066027742527660907 
		21 0.00086923379300759477 22 0.00040897778824573191
		5 18 0.56828277745041744 19 0.42397709816355678 20 0.006559531757348796 
		21 0.00080683978195855949 22 0.00037375284671845065
		5 18 0.010770008633620714 19 0.52949254046152416 20 0.44991757488385831 
		21 0.0078140902449689818 22 0.0020057857760277954
		5 18 0.00089985247849165887 19 0.020083949645339556 20 0.52155852567969418 
		21 0.41454072845466561 22 0.042916943741809092
		5 18 0.0017637101676528904 19 0.032994473468352395 20 0.48363503160824051 
		21 0.41255887907858291 22 0.069047905677171395
		5 18 0.021165170223503383 19 0.52832236273413669 20 0.43311705645661303 
		21 0.013662139824644403 22 0.0037332707611025646
		5 18 0.57842058290187981 19 0.41492858821586209 20 0.0055869671952085498 
		21 0.0007241106368264281 22 0.00033975105022306291
		5 18 0.49777141692060584 19 0.49947598512314023 20 0.0024394671312330607 
		21 0.00021880621394385028 22 9.432461107702027e-05
		5 18 0.49917439187740076 19 0.49841778805657394 20 0.0021226401750145426 
		21 0.0001987539485922681 22 8.642594241856189e-05
		5 18 0.49795513212279602 19 0.50055694356192404 20 0.0013229403818818214 
		21 0.0001154254182371221 22 4.9558515160927692e-05
		5 18 0.49936946450024883 19 0.49936946450024861 20 0.0011153260516676517 
		21 0.0001016975365664538 22 4.4047411268491302e-05
		5 18 0.0015248090796886008 19 0.4880184800500163 20 0.50613866646002825 
		21 0.0036005780359306002 22 0.00071746637433630039
		5 18 0.0034801606849341293 19 0.48759642532694747 20 0.49981911854524241 
		21 0.0075227758300157052 22 0.0015815196128603614
		5 18 0.00064020113752637664 19 0.48441845455107652 20 0.51309810733435079 
		21 0.0015458827639055937 22 0.0002973542131406514
		5 18 0.0020928490065885118 19 0.48807346212865776 20 0.5042818032156704 
		21 0.0046135493420235959 22 0.00093833630705976283
		5 18 0.00016852545195892337 19 0.0032396118439083767 20 0.48290997266127844 
		21 0.48290997266127844 22 0.030771917381575843
		5 18 6.8364649653222126e-05 19 0.0013789536838655306 20 0.49177450094212455 
		21 0.49177450094212444 22 0.015003679782232295
		5 18 0.00035907040078865814 19 0.0063683798061840019 20 0.46922166985636427 
		21 0.46951747904559338 22 0.054533400891069769
		5 18 0.00020789164234283033 19 0.0038593387409462934 20 0.47913003071918853 
		21 0.47953405409619737 22 0.037268684801325017
		5 17 0.0012554093006825039 18 0.99528110763823219 19 0.0024730550919778711 
		20 0.00069244009978232054 21 0.0002979878693252247
		5 17 0.00035497304874036018 18 0.99864769251586827 19 0.00071689855409764948 
		20 0.00019676057249425326 21 8.3675308799522941e-05
		5 17 0.0065111901848915917 18 0.97809604147701734 19 0.010648395564082612 
		20 0.0032787507953816993 21 0.0014656219786269048
		5 17 0.0042700084773252128 18 0.98549555333157945 19 0.0071229552994952223 
		20 0.0021574569512023403 21 0.00095402594039778574
		5 4 0.002910916444476168 5 0.014220630029390979 16 0.22490181914178259 
		17 0.7553527602807022 18 0.0026138741036481872
		5 4 0.0029351944804059931 5 0.0096649712821129646 16 0.19885329235890423 
		17 0.78567266893784804 18 0.0028738729407288273
		5 4 0.0018794331797107166 5 0.0092695947413170658 16 0.15470429375145006 
		17 0.83226811583456384 18 0.0018785624929583686
		5 4 0.0018582275941575069 5 0.0059796307191920918 16 0.12959991130992443 
		17 0.86058657326008414 18 0.0019756571166417414
		5 16 0.00012122318852660013 17 0.10800397182813531 18 0.89125313493746827 
		19 0.00042297055090997047 20 0.00019869949495984559
		5 16 8.6996399279974364e-05 17 0.087507108237459885 18 0.91194735636273294 
		19 0.00031210510533692621 20 0.00014643389519022751
		5 16 9.1806923768145919e-06 17 0.5095348000527552 18 0.49044395143553304 
		19 7.9125694810547691e-06 20 4.1552498538130194e-06
		5 16 8.7148602720414945e-06 17 0.5021568896704639 18 0.49782270330945155 
		19 7.6653490440836942e-06 20 4.0268107684066997e-06
		5 4 0.00038291928650992626 5 0.001114414570296172 16 0.0055922162664855677 
		17 0.97239887949245252 18 0.020511570384255745
		5 4 0.00039036833517793937 5 0.00098718064916786599 16 0.0053017482311279165 
		17 0.97012139224055594 18 0.023199310543970363
		5 4 0.00019730803290440009 5 0.00056481564160933606 16 0.0027527798041807102 
		17 0.98323678036335382 18 0.013248316157951764
		5 4 0.00019835121742518927 5 0.00048905417191171105 16 0.002551822853024088 
		17 0.98196606157268462 18 0.014794710184954379
		5 16 1.9625091753775218e-05 17 0.50026264518643782 18 0.49969055030676396 
		19 1.7857734683780096e-05 20 9.3216803606967828e-06
		5 16 1.8656592595268136e-05 17 0.49995558192487422 18 0.49999939719754954 
		19 1.7320129993331939e-05 20 9.0441549876714702e-06
		5 16 3.7209740073333734e-05 17 0.040838259848151107 18 0.95891788400099798 
		19 0.00014150006830204067 20 6.5146342475525725e-05
		5 17 0.023638011765804934 18 0.97623491143871111 19 7.3850121633056376e-05 
		20 3.395359335329768e-05 21 1.9273080497613661e-05
		5 17 0.035435838063392074 18 0.95816013737284178 19 0.0039674934175014309 
		20 0.0016000284803957584 21 0.00083650266586896872
		5 17 0.021846474037211013 18 0.97443894787513552 19 0.0023125918825922282 
		20 0.00092310419708505336 21 0.00047888200797623672
		5 17 0.022417058578619493 18 0.97495832707423336 19 0.0016194341015939224 
		20 0.00065866606176210706 21 0.00034651418379106458
		5 17 0.015834354419872351 18 0.98237506744207859 19 0.0011071399018670141 
		20 0.00044843744913021819 21 0.00023500078705165599
		5 17 0.0051336579101772762 18 0.99425518302452076 19 0.00038323301364287581 
		20 0.00015025269828056472 21 7.7673353378497566e-05
		5 17 0.0019415612670130459 18 0.99783540840636364 19 0.0001401674814130113 
		20 5.4700923692020822e-05 21 2.8161921518328635e-05
		5 17 0.011284403493501993 18 0.98660843464027659 19 0.0013327222271635533 
		20 0.00051248273989822719 21 0.00026195689915970685
		5 17 0.003382949903998898 18 0.9960236144997282 19 0.00037732178111193502 
		20 0.0001434132711658123 21 7.2700543995066961e-05
		5 18 0.9817499948089824 19 0.017050770283720505 20 0.00089072202854815412 
		21 0.00019888847309700047 22 0.00010962440565194553
		5 18 0.99065424541035152 19 0.0087688145396615062 20 0.000430942145825868 
		21 9.4306069091292557e-05 22 5.1691835069835171e-05
		5 18 0.94319647310992194 19 0.051795832669129446 20 0.0036649130840824837 
		21 0.0008619008064360505 22 0.00048088033043009777
		5 18 0.9548058550823334 19 0.041387894738216399 20 0.0028009599068760022 
		21 0.00064651026539598252 22 0.00035878000717828076
		5 18 0.0047409858521006978 19 0.019105323997386209 20 0.11289625196678345 
		21 0.43162871909186484 22 0.43162871909186484
		5 18 0.0045758027082189656 19 0.018634782444581416 20 0.11165970706292684 
		21 0.43256485389213656 22 0.43256485389213634
		5 18 0.0047974083124569123 19 0.01930320365706804 20 0.11357084103112657 
		21 0.43116427349967423 22 0.43116427349967423
		5 18 0.0046314631396335926 19 0.018832387798504355 20 0.11234606609953116 
		21 0.43209504148116551 22 0.43209504148116551
		5 18 0.71467008153915135 19 0.28453002849035808 20 0.00068461887436925811 
		21 7.8986849402453463e-05 22 3.6284246718975843e-05
		5 18 0.70190004356368407 19 0.29719785013503147 20 0.00077439097174546829 
		21 8.7635754117829811e-05 22 4.0079575420993237e-05
		5 18 0.0036547083276150779 19 0.60849451547959266 20 0.3849378580695853 
		21 0.0023427667774209415 22 0.00057015134578599031
		5 18 0.0019675632695679043 19 0.6246593510428291 20 0.37174342392897397 
		21 0.0013167260412691255 22 0.00031293571735987297
		5 18 0.00030202641295053938 19 0.007629517700107359 20 0.58523240194253789 
		21 0.38804255337761367 22 0.018793500566790588
		5 18 0.00016501492901560303 19 0.0044645704387907973 20 0.62539855905464137 
		21 0.35881460794442505 22 0.01115724763312718
		5 18 0.70108528793341829 19 0.29756194241888689 20 0.0011584793285645287 
		21 0.00013320821370723933 22 6.1082105423141498e-05
		5 18 0.7119825421300332 19 0.28679751231638539 20 0.0010416503193589686 
		21 0.00012207251745642334 22 5.6222716765948059e-05
		5 18 0.0035393311206773606 19 0.59026352205445787 20 0.40323046559130166 
		21 0.0023881605325982999 22 0.00057852070096490419
		5 18 0.005645596871517054 19 0.58338514465661417 20 0.40641204877201592 
		21 0.0036518513680163 22 0.0009053583318367189
		5 18 0.00029786196499523835 19 0.0076418060333632715 20 0.58589687326412943 
		21 0.38776629052363049 22 0.018397168213881588
		5 18 0.00046928801772927491 19 0.011265596343072172 20 0.55864652669153492 
		21 0.40283535486103061 22 0.026783234086632964
		5 18 0.49940811696949833 19 0.49914681841820513 20 0.0012824306973826726 
		21 0.00011367660755538117 22 4.8957307358544763e-05
		5 18 0.49996240132002889 19 0.49867017819042153 20 0.0012113871188106262 
		21 0.0001089673986760414 22 4.7065972062809522e-05
		5 18 0.49969260102709123 19 0.49969260102709123 20 0.00054617126752225936 
		21 4.797615517101726e-05 22 2.0650523124315845e-05
		5 18 0.4991824944585429 19 0.5001557889132886 20 0.00058888881968786313 
		21 5.0957992868941983e-05 22 2.1869815611715874e-05
		5 18 0.0013208283719304376 19 0.48751700337154164 20 0.50748124339984702 
		21 0.0030721801801186933 22 0.00060874467656225819
		5 18 0.00185833128506704 19 0.4880767966631922 20 0.50502406849544557 
		21 0.0041943114646120043 22 0.00084649209168325616
		5 18 0.0010102054436576427 19 0.48636391747670682 20 0.50985085638533345 
		21 0.0023199603652671067 22 0.00045506032903480111
		5 18 0.00061915784009008285 19 0.48393168329991731 20 0.51370045969548694 
		21 0.0014664629937769672 22 0.00028223617072867967;
	setAttr ".wl[400:499].w"
		5 18 0.00011291615572198147 19 0.0021930029589811027 20 0.48718950212168183 
		21 0.48735696475300433 22 0.023147614010610815
		5 18 7.315096706810698e-05 19 0.001461014208000923 20 0.49118879984801644 
		21 0.49119460579840402 22 0.016082429178510601
		5 18 0.00015224067331355397 19 0.0029258664907196243 20 0.48404258232780467 
		21 0.4840464973191384 22 0.028832813189023806
		5 18 0.00020629223239874864 19 0.0038578673933093136 20 0.47959662710145401 
		21 0.47971695110037094 22 0.036622262172467002
		5 18 4.3710621177475648e-05 19 0.0004128218857780502 20 0.021181337729142796 
		21 0.48919752006664458 22 0.489164609697257
		5 18 3.1523583296192708e-05 19 0.00029953235247421684 20 0.016257743828326301 
		21 0.49172515544974166 22 0.49168604478616162
		5 18 0.00010541398130460259 19 0.00095321038283100411 20 0.038625454781068616 
		21 0.48015796042739772 22 0.48015796042739795
		5 18 8.641734676190414e-05 19 0.00078603415847972057 20 0.033411953762242465 
		21 0.482857797366258 22 0.48285779736625789
		5 18 8.8109849273706843e-07 19 8.5488146999501264e-06 20 0.00060536398134236537 
		21 0.4996926030527325 22 0.4996926030527325
		5 18 4.8891922289693927e-06 19 4.673384478348305e-05 20 0.0029891767281082847 
		21 0.49847960011743958 22 0.49847960011743958
		5 18 1.1213563644364169e-05 19 0.0001064613022241355 20 0.0063563923109636765 
		21 0.49676296641158396 22 0.49676296641158396
		5 18 4.3385174681012843e-06 19 4.1806570415379874e-05 20 0.0027509149001425418 
		21 0.49860147000598698 22 0.49860147000598698
		5 17 0.002196890520505485 18 0.99218082301733346 19 0.0039566777101693665 
		20 0.0011612481229818544 21 0.00050436062900967912
		5 17 0.00098010179404277005 18 0.99637401312997753 19 0.0018823217806410335 
		20 0.00053446892052725823 21 0.00022909437481147274
		5 17 0.008179137835456941 18 0.97098982022327274 19 0.014463609907014275 
		20 0.0044000076347803967 21 0.0019674243994756563
		5 17 0.010892452901495431 18 0.96266850188161013 19 0.018173407906660711 
		20 0.005690659512410213 21 0.0025749777978236158
		5 4 0.0040067309431865783 5 0.037837445232887824 6 0.0036787804102774925 
		16 0.26772713851187535 17 0.68674990490177279
		5 4 0.0043445174242596552 5 0.040802301957309058 6 0.0040358378536668451 
		16 0.28484292459127691 17 0.66597441817348746
		5 4 0.0028648562519024744 5 0.0059154220273197403 16 0.15178012252042458 
		17 0.83653967636100801 18 0.0028999228393451794
		5 4 0.0024614071134135226 5 0.0049682024176461734 16 0.12956949396568188 
		17 0.86047175980161661 18 0.0025291367016417672
		5 17 0.041038278938668654 18 0.95874609094830421 19 0.00012458094874542064 
		20 5.7980822561475934e-05 21 3.3068341720302617e-05
		5 17 0.022003090193417146 18 0.97788714004303023 19 6.3636588120897589e-05 
		20 2.9416012698935559e-05 21 1.6717162732818265e-05
		5 16 8.2628900288168531e-06 17 0.49999007382731286 18 0.49999007382731286 
		19 7.60280408569693e-06 20 3.9866512597737144e-06
		5 16 1.1059779164492219e-05 17 0.49998657946271413 18 0.49998657946271435 
		19 1.0359842627594238e-05 20 5.4214527793264063e-06
		5 4 0.00018403535664688993 5 0.00040355617827304911 16 0.0023122876961649361 
		17 0.98342308850509397 18 0.013677032263821066
		5 4 0.00013382601243381743 5 0.00029017120642650023 16 0.0016465352540484754 
		17 0.98715904868758542 18 0.010770418839505856
		5 16 0.00010926039241040303 17 0.092751607065217595 18 0.90656956761945773 
		19 0.00038888115428128571 20 0.00018068376863290386
		5 16 0.00014519814766514492 17 0.11552461753107821 18 0.88359308365365963 
		19 0.00050220556003743064 20 0.00023489510755947751
		5 16 1.0524626008945869e-05 17 0.51108373676113494 18 0.48889176977835025 
		19 9.174852410339229e-06 20 4.7939820955268377e-06
		5 16 7.6000634765236929e-06 17 0.52002142040730071 18 0.47996106774445529 
		19 6.5056595578736365e-06 20 3.4061252094396831e-06
		5 4 0.00039982559654931827 5 0.0013882048054704825 16 0.006005005912779149 
		17 0.97237048919864832 18 0.019836474486552635
		5 4 0.00047301355840443472 5 0.001649043813914386 16 0.0072018593948224496 
		17 0.96880682081571179 18 0.02186926241714688
		5 17 0.0529990234828112 18 0.93553218599442689 19 0.0071060864895368678 
		20 0.0028619015388095118 21 0.0015008024944155849
		5 17 0.06342234946271591 18 0.92301770681421946 19 0.0083466429380278911 
		20 0.0034115116685467005 21 0.0018017891164900746
		5 17 0.020333104621056971 18 0.97709587770854534 19 0.0015968215271499463 
		20 0.00063945781254413231 21 0.00033473833070373139
		5 17 0.027709567424904814 18 0.96880811971483571 19 0.0021513869539310343 
		20 0.00087189505928326127 21 0.00045903084704512651
		5 17 0.010485832534730604 18 0.98773413747673722 19 0.001117379062203084 
		20 0.00043809940163755986 21 0.00022455152469162717
		5 17 0.0051309408711075876 18 0.99398898640485955 19 0.00055637391062499358 
		20 0.00021457256569911478 21 0.00010912624770877179
		5 17 0.0052530399062651334 18 0.99417022834073765 19 0.00035927260642772981 
		20 0.00014318324895859402 21 7.4275897610850153e-05
		5 17 0.001930751809873423 18 0.9978562290537647 19 0.00013342862879739669 
		20 5.2512462269459265e-05 21 2.7078045295014542e-05
		5 18 0.91887179158365806 19 0.073527439732241281 20 0.0055804706405721124 
		21 0.0012988908003748836 22 0.00072140724315371751
		5 18 0.93269249624549 19 0.061519792399056195 20 0.0042701973210651908 
		21 0.00097704847706230145 22 0.00054046555732640384
		5 18 0.89265123085710185 19 0.096758713505925201 20 0.0076708604051415477 
		21 0.0018674195740226111 22 0.001051775657808761
		5 18 0.88046216134319522 19 0.10684657428022032 20 0.0091494983712421766 
		21 0.0022625554572303819 22 0.0012792105481118485
		5 18 0.0057403827923178767 19 0.022783141225555852 20 0.12541735484144523 
		21 0.42302956057034058 22 0.42302956057034058
		5 18 0.0057207084407515226 19 0.022716256963281112 20 0.12521607602437815 
		21 0.42317347928579463 22 0.42317347928579463
		5 18 0.0058361797329212839 19 0.022773344621394714 20 0.12437930508204752 
		21 0.4235055852818182 22 0.4235055852818182
		5 18 0.0058561267134530203 19 0.022840055010657261 20 0.12457935299397739 
		21 0.42336223264095613 22 0.42336223264095613
		5 18 0.57064997448365629 19 0.42015336994211705 20 0.0076836826744760668 
		21 0.001027862599273629 22 0.0004851103004768851
		5 18 0.56760692835041471 19 0.42354653600958386 20 0.0073969568211114511 
		21 0.00098505161006058042 22 0.00046452720882930748
		5 18 0.02746356390178558 19 0.51563475207732068 20 0.43402246269092676 
		21 0.017852222249244282 22 0.0050269990807227253
		5 18 0.026371497671599228 19 0.51791720983322898 20 0.43382275114876345 
		21 0.017098093692420276 22 0.0047904476539879877
		5 18 0.0023464444655634186 19 0.040724132872026947 20 0.46687271179712481 
		21 0.40800682904495622 22 0.082049881820328546
		5 18 0.0022348306992019942 19 0.039299611942491763 20 0.46976825524010579 
		21 0.40893870987589642 22 0.079758592242303972
		5 18 0.54257939502116725 19 0.44223878174919212 20 0.012739616247170732 
		21 0.0016637324566131648 22 0.00077847452585691305
		5 18 0.54505281714515941 19 0.43931736613317091 20 0.013105257104122952 
		21 0.0017193824560367789 22 0.00080517716151010381
		5 18 0.020147971860895005 19 0.50440046082840662 20 0.45616235125444388 
		21 0.015167385414188965 22 0.004121830642065615
		5 18 0.021099422569124088 19 0.50267542074421678 20 0.45596886561260108 
		21 0.015910024886453138 22 0.0043462661876048428
		5 18 0.0018032073498030091 19 0.034903911206099182 20 0.4822490205642852 
		21 0.4127793817552331 22 0.068264479124579544
		5 18 0.0019045800966116634 19 0.036363181413174538 20 0.47907249516905215 
		21 0.41204069253632286 22 0.07061905078483878
		5 18 0.49710775131397361 19 0.49986736284920236 20 0.0026817828600191147 
		21 0.00023980852805893796 22 0.00010329444874604982
		5 18 0.4969846596294783 19 0.49954686407176491 20 0.0030721919737293152 
		21 0.00027688297119626907 22 0.0001194013538311512
		5 18 0.4994058236469392 19 0.4991919108486817 20 0.0012394250598820628 
		21 0.0001135979141079655 22 4.9242530389211718e-05
		5 18 0.49943967767217229 19 0.49943967767217218 20 0.00099143339198836354 
		21 9.0169267733992823e-05 22 3.9041995933146643e-05
		5 18 0.0012072649604076575 19 0.48749341187027384 20 0.50783110690308597 
		21 0.0028975585064035326 22 0.00057065775982903587
		5 18 0.0014768181415135051 19 0.48802010490100839 20 0.50627947499562942 
		21 0.0035231152368611386 22 0.00070048672498756169
		5 18 0.0022225980527089515 19 0.4880945950411385 20 0.50380516343111137 
		21 0.0048812967561025402 22 0.0009963467189386243
		5 18 0.0018857596978411333 19 0.4879617897956916 20 0.50514590164592199 
		21 0.0041643755246977936 22 0.00084217333584744652
		5 18 0.00021562970798543902 19 0.0039899786205067692 20 0.47852901660127339 
		21 0.47895611787484099 22 0.038309257195393291
		5 18 0.00018091361950258678 19 0.0033894632841689285 20 0.48113836656566861 
		21 0.48160938116068819 22 0.033681875369971727
		5 18 0.00012457213706364587 19 0.0024475803194508863 20 0.48659413776010507 
		21 0.48659413776010507 22 0.024239572023275369
		5 18 0.00015387748058157304 19 0.0029851867936529926 20 0.48415443158056293 
		21 0.48415443158056315 22 0.028552072564639253
		5 18 0.004682662817509179 19 0.019032974078589025 20 0.11310375132276525 
		21 0.4315903058905684 22 0.43159030589056818
		5 18 0.0047013089835261567 19 0.019098970337866988 20 0.11333018007264797 
		21 0.43143477030297944 22 0.43143477030297944
		5 18 0.0047298835926675546 19 0.019064149472024484 20 0.11274784537997676 
		21 0.4317290607776656 22 0.4317290607776656
		5 18 0.004748658056108284 19 0.01913007244769202 20 0.11297347351959498 
		21 0.43157389798830237 22 0.43157389798830237
		5 18 0.00011440737058124641 19 0.0010310495239211834 20 0.040859154528187343 
		21 0.47899769428865513 22 0.47899769428865513
		5 18 0.0001069340531776863 19 0.00096577530365853093 20 0.038935178992958751 
		21 0.47999605582510252 22 0.47999605582510252
		5 18 9.5328945857228105e-05 19 0.00088594158380728236 20 0.03867289857384365 
		21 0.48021638160645985 22 0.48012944929003204
		5 18 0.00010235574724977918 19 0.00094916965019778237 20 0.040688321010536321 
		21 0.47917186555320457 22 0.47908828803881154
		5 18 0.00041333987926912646 19 0.0038888858109428692 20 0.12770193606153818 
		21 0.44930647820901976 22 0.41868936003923002
		5 18 0.00030187815367111544 19 0.0028987464302558082 20 0.1105232195455092 
		21 0.46170488421415123 22 0.42457127165641256
		5 18 0.00024014575676305799 19 0.0024031113140860335 20 0.10664624372282624 
		21 0.47303402493633617 22 0.41767647426998861
		5 18 0.00015499977620094896 19 0.0015845159336402637 20 0.084690849788773351 
		21 0.49269514675441012 22 0.42087448774697539
		5 18 4.8193542560216255e-05 19 0.0005065816035207093 20 0.038709572715538196 
		21 0.54408185447350776 22 0.4166537976648732
		5 18 7.5454963024670642e-05 19 0.00077618685021183344 20 0.050965284855169241 
		21 0.51975904755541891 22 0.42842402577617533
		5 18 0.00012672801410926952 19 0.0012811667120720058 20 0.070899158184337466 
		21 0.49789718599308663 22 0.42979576109639461
		5 18 9.1347693678771005e-05 19 0.0009434010144959683 20 0.059760701579361267 
		21 0.51439737499445293 22 0.42480717471801116
		5 18 0.00033988455866098898 19 0.0033440447216263819 20 0.12669047360829347 
		21 0.45796176881220602 22 0.41166382829921305
		5 18 0.00031174255262624321 19 0.0030842050554111449 20 0.12191202627889323 
		21 0.46175609216791058 22 0.41293593394515882
		5 18 0.00034927395795664168 19 0.0033189650196483902 20 0.11799050827588495 
		21 0.45591454288790362 22 0.42242670985860642
		5 18 0.00037930747853642414 19 0.0035848510820104115 20 0.12251743992129642 
		21 0.45267486711949928 22 0.4208435343986574
		5 18 0.20351331132220121 19 0.77380789298098418 20 0.02114496067495971 
		21 0.0011112725084518331 22 0.00042256251340295559
		5 18 0.16597661988085782 19 0.81842951066242353 20 0.014596319394817819 
		21 0.00072402056698333227 22 0.00027352949491757361
		5 18 0.2427955669190219 19 0.734384557739719 20 0.021108586804448277 
		21 0.0012342598371174256 22 0.00047702869969337247
		5 18 0.20886554177329564 19 0.77475353869443264 20 0.015213270930207936 
		21 0.00084363861641987496 22 0.00032400998564392021
		5 18 0.14878964366872344 19 0.84118407085681501 20 0.0093732742276540458 
		21 0.00047336686072069098 22 0.00017964438608699578
		5 18 0.13356508595412014 19 0.8568368340716197 20 0.0089961683693918892 
		21 0.00043699024120591934 22 0.00016492136366245971
		5 18 0.085936174826963141 19 0.90903991973870835 20 0.0047243141586755747 
		21 0.00021783764065389267 22 8.1753634999159525e-05
		5 18 0.10094366056415642 19 0.8935456715632113 20 0.0051688244048978824 
		21 0.00024817919128726609 22 9.3664276447237487e-05;
	setAttr ".wl[500:567].w"
		5 18 0.22919720341920022 19 0.74026883987304937 20 0.028383136404424563 
		21 0.0015564103522299237 22 0.00059440995109577349
		5 18 0.22146237377659644 19 0.75016282015795799 20 0.026405682284005132 
		21 0.0014256399546196559 22 0.00054348382682081905
		5 18 0.21768612031814338 19 0.76474823995929087 20 0.016295858322856911 
		21 0.00091695259475539507 22 0.00035282880495339351
		5 18 0.2276552699733288 19 0.75298182321617635 20 0.017942887884882765 
		21 0.0010249356904272688 22 0.00039508323518489371
		5 18 0.30469905981376666 19 0.64832771881387519 20 0.042902557271549707 
		21 0.0029212078161097782 22 0.0011494562846987096
		5 18 0.4990045325259635 19 0.4990045325259635 20 0.0017590678005546796 
		21 0.00016176806728541269 22 7.0099080233000549e-05
		5 18 0.49808585293350971 19 0.50026671324724903 20 0.0014628508409408943 
		21 0.00012907504523181309 22 5.550793306850892e-05
		5 18 0.49667810001998058 19 0.50102952994857708 20 0.0020361801866629111 
		21 0.00017920929725731799 22 7.6980547522078997e-05
		5 18 0.49897687119896972 19 0.49945884944002739 20 0.0013865583215125655 
		21 0.00012416720623599213 22 5.3553833254323681e-05
		5 18 0.27540154693619251 19 0.67549194380240107 20 0.045218768272233346 
		21 0.0028023376439612899 22 0.0010854033452118927
		5 18 0.28127274016287507 19 0.6722960839352079 20 0.042685762270336096 
		21 0.0026969760288076232 22 0.0010484376027734587
		5 18 0.29125429307295764 19 0.66296877855681602 20 0.041970079221210446 
		21 0.002737157820858207 22 0.0010696913281575133
		5 18 0.10604152941103927 19 0.88148974553087878 20 0.011756511064810923 
		21 0.0005197922005443275 22 0.00019242179272666883
		5 18 0.13536858104993094 19 0.84518714906752967 20 0.018300364548476528 
		21 0.00083434143071484221 22 0.00030956390334809387
		5 18 0.16987074915950073 19 0.80966919140169114 20 0.019123804331334763 
		21 0.00097061101914935096 22 0.00036564408832411141
		5 18 0.12055452919262057 19 0.86645496017571821 20 0.0122125148439886 
		21 0.00056677456215731043 22 0.00021122122551545317
		5 18 0.2499888398384339 19 0.6913902990499492 20 0.0541486477242746 
		21 0.0032347902845206613 22 0.0012374231028216391
		5 18 0.24018366043693096 19 0.70196190707014128 20 0.053580571419291581 
		21 0.0030956238425032833 22 0.0011782372311329686
		5 18 0.26654722014130994 19 0.67775117482972858 20 0.05121433285731701 
		21 0.0032370810476173139 22 0.0012501911240270557
		5 18 0.26124922474782825 19 0.68132895405744165 20 0.052875301692779313 
		21 0.0032827677910154079 22 0.0012637517109352593
		5 18 0.066486187234743921 19 0.40944497628578774 20 0.409200434491993 
		21 0.084808862166720553 22 0.030059539820754794
		5 18 0.065900253061972114 19 0.40976597368506967 20 0.40962088767145832 
		21 0.084727936738319243 22 0.029984948843180634
		5 18 0.078733290038524598 19 0.39838092996649571 20 0.39616730310068887 
		21 0.092334992792577023 22 0.034383484101713803
		5 18 0.076610569626526112 19 0.40053845433182517 20 0.39860620172888622 
		21 0.09076160726489671 22 0.033483167047865824
		5 18 0.078642080639994533 19 0.39126324628295217 20 0.39118746705671276 
		21 0.10055781613669938 22 0.038349389883641172
		5 18 0.077563834720296668 19 0.39225578757165652 20 0.39222512218263833 
		21 0.099981872204795708 22 0.037973383320612758
		5 18 0.090701948689664177 19 0.38024889418331687 20 0.37887574333933299 
		21 0.10728782944604745 22 0.04288558434163868
		5 18 0.089332263550704921 19 0.38142742618553699 20 0.38027146226248365 
		21 0.10658370239167661 22 0.042385145609597837
		5 4 0.00040155244644192212 5 0.012061939473810833 6 0.0002898054939937325 
		16 0.93432959892093048 17 0.052917103664823086
		5 4 0.00043396660369060102 5 0.0079107756632324239 6 0.00026141570516819215 
		16 0.88731006875673546 17 0.10408377327117334
		5 3 0.0019394414195393298 4 0.0072315405827946544 5 0.01157020729572565 
		16 0.48962940535097016 17 0.48962940535097016
		5 3 0.0030952719601664157 4 0.01118790932455592 5 0.01825998207905305 
		16 0.48372841831811231 17 0.48372841831811231
		5 4 0.0029548592311543311 5 0.0095103724818956775 6 0.00093061147161151007 
		16 0.4933020784076691 17 0.49330207840766932
		5 3 0.0014161594563665727 4 0.005755642872058298 5 0.010349998333353273 
		16 0.49123909966911095 17 0.49123909966911095
		5 4 0.0054314900501917082 5 0.015999513172593807 6 0.001620480075706601 
		16 0.48847425835075392 17 0.48847425835075392
		5 4 0.0018098859687460658 5 0.011660671863517944 6 0.00077544169228891215 
		16 0.54927748826498557 17 0.43647651221046158
		5 4 0.0028345794100386669 5 0.039798629473579379 6 0.001684887915753247 
		16 0.69593865189258108 17 0.25974325130804776
		5 4 0.0036957836186130538 5 0.021849603991762757 6 0.0015497329586737164 
		16 0.5242557584513069 17 0.44864912097964366
		5 4 0.00079907672750008439 5 0.022094315776134087 6 0.00056843680724990165 
		16 0.88934018727069941 17 0.087197983418416614
		5 3 0.0015385450586718054 4 0.0059979154250204324 5 0.010001097809209719 
		16 0.49123122085354909 17 0.49123122085354909
		5 4 0.0006368696653464345 5 0.0096527500405353683 6 0.00022127654902480119 
		16 0.96337282654062384 17 0.02611627720446958
		5 4 0.0014430137069843976 5 0.016026768091800293 6 0.00045107126516817886 
		16 0.91570452389493417 17 0.066374623041113009
		5 4 0.016013442790895442 5 0.036996064620540801 6 0.0025430407757832041 
		16 0.49235467660986254 17 0.45209277520291796
		5 4 0.018172354801283083 5 0.042574004221442738 6 0.0029883013364722191 
		16 0.49052402909320381 17 0.44574131054759808
		5 4 0.0091187812125333619 5 0.034401521090405271 6 0.0019045388326015013 
		16 0.57835565240251963 17 0.37621950646194025
		5 4 0.013855660923592902 5 0.034585920062802922 6 0.0023070181532734465 
		16 0.50112028682150067 17 0.44813111403883005
		5 4 0.012541714698731912 5 0.04394777579004102 6 0.0025706114859806706 
		16 0.55432763040998312 17 0.38661226761526329
		5 4 0.0051457726911961193 5 0.031042717271012463 6 0.0013134646571172262 
		16 0.72444260489937595 17 0.2380554404812982
		5 4 0.0036728137614810111 5 0.035978682174653318 6 0.001154100908439093 
		16 0.83377586465248021 17 0.12541853850294635
		5 4 0.0079389874360814624 5 0.044165920451830806 6 0.0020174468580772633 
		16 0.67121632379907237 17 0.2746613214549381
		5 4 0.0081916491393215201 5 0.03178075402091611 6 0.0016696222972903724 
		16 0.59368170790468267 17 0.36467626663778935
		5 4 0.0037359127594974537 5 0.026244102498500229 6 0.00098078187581568822 
		16 0.78500781448794821 17 0.18403138837823838
		5 4 0.001115980849010443 5 0.016070425434279764 6 0.00038675930624390727 
		16 0.94079284927231988 17 0.041633985138146014
		5 4 0.014555491257176935 5 0.034506711891497836 6 0.0023393108411253229 
		16 0.49557478289934215 17 0.45302370311085777
		5 4 0.0071679965509340669 5 0.028546158415188812 6 0.0014695953206518148 
		16 0.60460850421071011 17 0.35820774550251516
		5 4 0.0029619347483663149 5 0.021458529377112866 6 0.00077768897827600058 
		16 0.81127068489574194 17 0.16353116200050288
		5 16 6.3266241780186069e-05 17 0.5329863710809557 18 0.46687600332066248 
		19 4.8661119020063181e-05 20 2.5698237581573711e-05
		5 16 2.9209374731128161e-05 17 0.51319587567198843 18 0.48673933847612116 
		19 2.3233449252467305e-05 20 1.2343027906819642e-05
		5 16 4.6248793104617416e-05 17 0.50296761844474036 18 0.49692706383063878 
		19 3.8644289647866467e-05 20 2.042464186830621e-05
		5 16 4.1011955085124422e-05 17 0.56384326850452848 18 0.43606980224003955 
		19 2.9995068956169032e-05 20 1.5922231390710152e-05
		5 16 5.2711668677889858e-05 17 0.50787978000821732 18 0.49200138105793007 
		19 4.3284091934464186e-05 20 2.2843173240340315e-05
		5 16 5.8556303855014344e-05 17 0.51715506738016892 18 0.48271491932993754 
		19 4.6776995969401001e-05 20 2.4679990068966866e-05
		5 16 3.438797463538498e-05 17 0.53019954534060942 18 0.46972578833156636 
		19 2.6292461593138853e-05 20 1.3985891595642471e-05
		5 16 3.8337559316442353e-05 17 0.54802281904849748 18 0.45189518499993153 
		19 2.8501548145347472e-05 20 1.5156844109127926e-05
		5 16 3.4215102593559892e-05 17 0.56961539724425558 18 0.43031191472330887 
		19 2.5149099109194436e-05 20 1.3323830732813766e-05
		5 16 4.0697914415214887e-05 17 0.55228353003502439 18 0.44762893083950789 
		19 3.0647228726514542e-05 20 1.6193982326046094e-05
		5 16 3.1537560563351732e-05 17 0.50335168047526047 18 0.49657668643502079 
		19 2.6221764693105897e-05 20 1.3873764462070419e-05
		5 16 2.6733162028273536e-05 17 0.50750304572608551 18 0.4924369959262207 
		19 2.1708823257492636e-05 20 1.1516362408077888e-05;
	setAttr -s 31 ".pm";
	setAttr ".pm[0]" -type "matrix" -0.15680631874941806 0.98762937299386533 1.9203235634255452e-17 -0
		 0.98762937299386533 0.15680631874941817 -1.2094971503808443e-16 0 -1.2246467991473527e-16 -3.0814879110195767e-33 -1 0
		 -556.54303159690778 -88.362564335046656 6.815686422329173e-14 1;
	setAttr ".pm[1]" -type "matrix" 0.062080978492094471 0.9980711157575215 -2.6805962794083368e-17 -0
		 -0.99807111575752139 0.06208097849209461 2.431781747614721e-16 0 2.4437325261173941e-16 1.165751815773164e-17 1 -0
		 560.29701578098332 -43.035829167130757 -1.3702319106699251e-13 1;
	setAttr ".pm[2]" -type "matrix" -0.15680631874941806 0.98762937299386544 4.6009198428338808e-17 -0
		 0.98762937299386522 0.15680631874941817 -3.6412788979955656e-16 0 -3.6683793252647465e-16 -1.1657518157731646e-17 -1 0
		 -494.44900661422514 -90.513875048975763 1.909852228003692e-13 1;
	setAttr ".pm[3]" -type "matrix" -0.18411737904022696 -0.98290426326034253 -1.3957429162126204e-16 0
		 0.98290426326034275 -0.18411737904022704 9.8401839147183031e-17 0 -1.2241763996512782e-16 -1.1907067755956352e-16 1 -0
		 -553.24324881388691 105.35215275458967 -5.5307772665672655e-14 1;
	setAttr ".pm[4]" -type "matrix" 0.14368067469030996 -0.9896241022330331 -1.3957429162126204e-16 -0
		 0.98962410223303332 0.14368067469030998 9.8401839147183031e-17 0 -7.7326703334545362e-17 -1.5226452567993771e-16 1 -0
		 -612.77294890466533 -98.111138247060367 -5.5505734429841339e-14 1;
	setAttr ".pm[5]" -type "matrix" 0.03918556669535149 -0.99923195073154225 -1.3957429162126201e-16 -0
		 0.99923195073154247 0.03918556669535149 9.8401839147183043e-17 0 -9.2856963973329775e-17 -1.4332302351953412e-16 1 -0
		 -716.30416625084229 -23.176268293953683 -5.1970899344200789e-14 1;
	setAttr ".pm[6]" -type "matrix" -0.094809092627995709 -0.99549547259395166 -1.3957429162126201e-16 0
		 0.99549547259395188 -0.094809092627995736 9.8401839147183018e-17 0 -1.1119149730874616e-16 -1.2961618631700397e-16 1 -0
		 -769.3250427595915 80.440057302969578 -5.1339961442449999e-14 1;
	setAttr ".pm[7]" -type "matrix" 0.01851534400602068 -0.99982857632513089 -1.3957429162126204e-16 -0
		 0.99982857632513111 0.01851534400602068 9.8401839147183031e-17 0 -9.580070471853827e-17 -1.4137230918591036e-16 1 -0
		 -831.86760311193916 -13.836949993469942 -5.2594911904574821e-14 1;
	setAttr ".pm[8]" -type "matrix" 0.99999999999999956 -1.1553258350005527e-15 -1.3957429162126204e-16 -0
		 1.1587952819525064e-15 0.99999999999999978 9.8401839147183031e-17 -0 1.3957429162126194e-16 -9.8401839147183203e-17 1 -0
		 -2.681930185198262 -892.14763857305729 -5.1393996453069635e-14 1;
	setAttr ".pm[9]" -type "matrix" -1.5766239482585878e-15 2.4677145545861107e-15 0.99999999999999933 -0
		 0.73593101176182885 -0.67705653082088391 2.8513938317956838e-15 -0 0.67705653082088413 0.73593101176182896 -6.6920065569024372e-16 -0
		 -522.48680778374171 542.33987591893447 -4.9103200000020788 1;
	setAttr ".pm[10]" -type "matrix" 3.4980822239674598e-16 2.9074030344419489e-15 0.99999999999999956 -0
		 0.13832224525180573 -0.99038727600292753 2.8513938317956845e-15 0 0.99038727600292775 0.13832224525180556 -6.6920065569024381e-16 -0
		 -20.315310508424602 718.90861069118 -4.9103200000020051 1;
	setAttr ".pm[11]" -type "matrix" 7.2247398346767108e-16 2.8378494569569721e-15 0.99999999999999956 -0
		 0.0092185452926791221 -0.99995750830857133 2.8513938317956842e-15 0 0.99995750830857155 0.0092185452926789278 -6.6920065569024391e-16 -0
		 205.00054560892085 698.27647776334459 1.8655299999980184 1;
	setAttr ".pm[12]" -type "matrix" 6.9514825669555819e-16 2.8446664300672847e-15 0.99999999999999956 -0
		 0.018834962757058496 -0.9998226063547172 2.8513938317956842e-15 0 0.99982260635471742 0.018834962757058309 -6.6920065569024381e-16 -0
		 334.65304677480185 701.52762421506532 8.3328299999980118 1;
	setAttr ".pm[13]" -type "matrix" -4.3011583918344602e-16 2.8302375544423468e-15 -0.99999999999999956 -0
		 0.0011079563370642758 -0.99999938621618911 -2.8510374858595874e-15 0 -0.99999938621618922 -0.0011079563370644666 3.4757632316284618e-16 0
		 -372.72626894604508 693.64346131545778 -8.3328299999981468 1;
	setAttr ".pm[14]" -type "matrix" -4.7931821436030371e-16 2.8223214379433684e-15 -0.99999999999999956 -0
		 0.018515344006025149 -0.99982857632513078 -2.851037485859587e-15 0 -0.99982857632513089 -0.01851534400602534 3.4757632316284609e-16 0
		 -405.54658103968353 686.68793147065594 -8.3328299999981397 1;
	setAttr ".pm[15]" -type "matrix" -0.039185566695350713 0.99923195073154225 8.8199496530749458e-16 -0
		 -0.99923195073154247 -0.039185566695350692 -3.7199933019456096e-16 0 -5.3753042576038589e-16 -8.1335366304822532e-16 0.99999999999999978 -0
		 693.41658891137172 35.532042351583996 403.41714125562231 1;
	setAttr ".pm[16]" -type "matrix" 2.1246014704267292e-15 -2.4460094339290237e-15 -0.99999999999999956 -0
		 -0.73593101176182851 0.67705653082088435 -3.2575685969753614e-15 0 0.67705653082088457 0.73593101176182874 -5.6995409058207253e-16 -0
		 522.48682682667959 -542.33985522008913 4.9103200000024678 1;
	setAttr ".pm[17]" -type "matrix" 8.7206016437896295e-17 -3.2387171333270584e-15 -0.99999999999999956 -0
		 -0.13832224525180481 0.99038727600292775 -3.2575685969753618e-15 0 0.99038727600292797 0.13832224525180481 -5.6995409058207233e-16 -0
		 20.315338364123779 -718.90860680071933 4.910320000002371 1;
	setAttr ".pm[18]" -type "matrix" -3.3192307148995568e-16 -3.3320012565859256e-08 -0.99999999999999911 0
		 -0.0092185452926782426 0.99995750830857089 -3.3318596777028903e-08 0 0.99995750830857166 0.0092185452926782045 -3.0716258523514159e-10 -0
		 -205.00061747979939 -698.27647848807669 -1.8655067334188549 1;
	setAttr ".pm[19]" -type "matrix" 3.2044664045228539e-10 -8.0441621725000886e-08 -0.99999999999999634 -0
		 -0.018834962757057604 0.99982260635471398 -8.043338753095104e-08 0 0.99982260635471742 0.018834962757057531 -1.1947253623882827e-09 -0
		 -334.65311863331425 -701.52762623911212 -8.332773675218526 1;
	setAttr ".pm[20]" -type "matrix" 6.3520487975324653e-16 -7.4469919509060005e-09 0.99999999999999933 -0
		 -0.0011079563370652119 0.99999938621618911 7.4469874186885004e-09 0 -0.99999938621618922 -0.0011079563370652496 -8.2500983843974439e-12 -0
		 372.72675544167942 -693.6434608384958 8.332824834442464 1;
	setAttr ".pm[21]" -type "matrix" 1.2963464074173682e-10 2.235533044725487e-08 0.99999999999999911 -0
		 -0.018515344006026085 0.99982857632513056 -2.2349097946466464e-08 0 -0.999828576325131 -0.018515344006026113 5.4352926022973773e-10 -0
		 405.54678257662613 -686.68787618304839 8.3328452985614483 1;
	setAttr ".pm[22]" -type "matrix" 0.039185566695350012 -0.99923195073154192 -2.0556205081424268e-16 -0
		 0.99923195073154225 0.039185566695350046 9.857608915558894e-16 0 -1.0956517615357539e-15 -5.2602619318358425e-16 0.99999999999999956 -0
		 -693.41623516036577 -35.532028478995038 -403.41700000000048 1;
	setAttr ".pm[23]" -type "matrix" -0.15679621116079887 0.98756904080779062 0.011060642097021305 -0
		 0.98762928672710615 0.15680683367561513 -9.4400928281942299e-05 0 -0.0018276116998478893 0.010909012357140287 -0.99993882477122875 0
		 -509.71336248634782 -101.50439727101816 -35.505351246728459 1;
	setAttr ".pm[24]" -type "matrix" 0.071462979010819633 0.99738192525596758 -0.011060642097021314 -0
		 -0.99744286054030218 0.071468391934084219 9.4400928282064163e-05 0 0.00088464008402846638 0.011025612321109005 0.99993882477122886 -0
		 265.5723931882938 -20.147722938677138 35.505351246728452 1;
	setAttr ".pm[25]" -type "matrix" -0.95844736613851533 -0.28505492213345057 0.011060637849371937 -0
		 -0.28507335966558345 0.95850569674892272 -9.4386645434053941e-05 0 -0.010574779010435114 -0.0032435578234796905 -0.99993882481956176 0
		 30.99052304575579 -46.850594874609897 -35.505351944856777 1;
	setAttr ".pm[26]" -type "matrix" -0.15679621132418931 0.9875690408294221 0.011060637849371939 -0
		 0.98762928672850037 0.15680683367543038 -9.4386645434053914e-05 0 -0.0018275969284890505 0.010909010401533805 -0.99993882481956176 0
		 -29.473817142537658 134.3405408076022 -35.505351944856784 1;
	setAttr ".pm[27]" -type "matrix" 0.15679621384860573 -0.98756905686476415 -0.011059170221222995 -0
		 -0.98762928674967854 -0.15680683354076405 9.4388768125691611e-05 0 -0.001827368890694805 0.010907560596157814 -0.99993884105218078 -0
		 509.71327688297606 101.50444880272651 35.505303207731473 1;
	setAttr ".pm[28]" -type "matrix" -0.07146298031067444 -0.99738194164916549 0.011059155359065369 -0
		 0.99744286055121789 -0.071468391796386407 -9.438983308832972e-05 0 0.00088452276313636888 0.011024130177842983 0.99993884121645304 -0
		 -265.57251857229869 20.147785577698688 -35.505302907505857 1;
	setAttr ".pm[29]" -type "matrix" 0.95844738186473677 0.28505492690540796 -0.011059152028233177 -0
		 0.28507335953348817 -0.95850569678899833 9.4378633065701339e-05 0 -0.010573357126367141 -0.0032431265760458181 -0.99993884125434873 0
		 -30.990574359499234 46.850596658850257 35.505303454949555 1;
	setAttr ".pm[30]" -type "matrix" 0.15679621399085625 -0.98756905704591091 -0.011059152028233179 -0
		 -0.98762928675089245 -0.15680683353921848 9.4378633065700878e-05 0 -0.0018273560288380483 0.010907544217367292 -0.99993884125434895 -0
		 29.473871518447488 -134.34087839885791 35.505305787318981 1;
	setAttr ".gm" -type "matrix" 100 0 0 0 -0 -1.6292067961387602e-05 -99.999999999998678 0
		 0 99.999999999998678 -1.6292067961387602e-05 0 0 0 0 1;
	setAttr -s 18 ".ma";
	setAttr -s 31 ".dpf[0:30]"  4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 
		4 4 4 4 4 4 4 4 4 4 4;
	setAttr -s 18 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 5;
	setAttr ".ucm" yes;
	setAttr -s 18 ".ifcl";
createNode makeNurbCircle -n "makeNurbCircle1";
	rename -uid "1FF57CC9-4673-F3DD-F501-088DE83F99C1";
	setAttr ".nr" -type "double3" 0 1 0 ;
createNode transformGeometry -n "transformGeometry1";
	rename -uid "5F772E62-45F6-EB38-5B0B-A1BC6878F05D";
	setAttr ".txf" -type "matrix" 2.9837284541818211e-06 -37.085274845736905 -0.69862370195987922 0
		 3.2044483733173479e-10 -0.018834962757057871 0.99982260635471754 0 -37.091854705053322 -2.9834230307026646e-06 -4.4314631467084832e-08 0
		 -8.332830000000083 695.10000000000014 347.80700000000002 1;
createNode makeNurbSphere -n "makeNurbSphere1";
	rename -uid "CF9EB508-488A-4466-4C30-4AABDEE80FDB";
	setAttr ".ax" -type "double3" 0 1 0 ;
createNode transformGeometry -n "transformGeometry2";
	rename -uid "7506F9A5-42D9-B9F8-3D85-BAB40E469B56";
	setAttr ".txf" -type "matrix" 21.975961479634499 0 0 0 0 21.975961479634499 0 0
		 0 0 21.975961479634499 0 0 581.20010539843838 211.42371694623819 1;
createNode nodeGraphEditorInfo -n "MayaNodeEditorSavedTabsInfo";
	rename -uid "FDD7DD8C-4B95-7E34-7145-FCA62499DB62";
	setAttr ".tgi[0].tn" -type "string" "Untitled_1";
	setAttr ".tgi[0].vl" -type "double2" 142.502329509471 -1661.8712797159724 ;
	setAttr ".tgi[0].vh" -type "double2" 2219.2422029908848 -378.80610757727209 ;
	setAttr -s 4 ".tgi[0].ni";
	setAttr ".tgi[0].ni[0].x" 646.89398193359375;
	setAttr ".tgi[0].ni[0].y" -404.841796875;
	setAttr ".tgi[0].ni[0].nvs" 18306;
	setAttr ".tgi[0].ni[1].x" 1136.3585205078125;
	setAttr ".tgi[0].ni[1].y" 32.689060211181641;
	setAttr ".tgi[0].ni[1].nvs" 18306;
	setAttr ".tgi[0].ni[2].x" 788.5714111328125;
	setAttr ".tgi[0].ni[2].y" -118.57142639160156;
	setAttr ".tgi[0].ni[2].nvs" 18304;
	setAttr ".tgi[0].ni[3].x" 1452.43017578125;
	setAttr ".tgi[0].ni[3].y" -66.933341979980469;
	setAttr ".tgi[0].ni[3].nvs" 18306;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 0;
	setAttr -av ".unw";
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
	setAttr ".fprt" yes;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".st";
	setAttr -cb on ".an";
	setAttr -cb on ".pt";
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 6 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 9 ".dsm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :initialParticleSE;
	setAttr -av -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -cb on ".an";
	setAttr -cb on ".il";
	setAttr -cb on ".vo";
	setAttr -cb on ".eo";
	setAttr -cb on ".fo";
	setAttr -cb on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultRenderGlobals;
	addAttr -ci true -h true -sn "dss" -ln "defaultSurfaceShader" -dt "string";
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".macc";
	setAttr -k on ".macd";
	setAttr -k on ".macq";
	setAttr -k on ".mcfr";
	setAttr -cb on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr ".ren" -type "string" "arnold";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -cb on ".sdf";
	setAttr -k on ".outf";
	setAttr -cb on ".imfkey";
	setAttr -k on ".gama";
	setAttr -cb on ".an";
	setAttr -cb on ".ar";
	setAttr -k on ".fs";
	setAttr -k on ".ef";
	setAttr -av -k on ".bfs";
	setAttr -cb on ".me";
	setAttr -cb on ".se";
	setAttr -k on ".be";
	setAttr -cb on ".ep";
	setAttr -k on ".fec";
	setAttr -k on ".ofc";
	setAttr -cb on ".ofe";
	setAttr -cb on ".efe";
	setAttr -cb on ".oft";
	setAttr -cb on ".umfn";
	setAttr -cb on ".ufe";
	setAttr -cb on ".pff";
	setAttr -cb on ".peie";
	setAttr -cb on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -k on ".lpr";
	setAttr -cb on ".gv";
	setAttr -cb on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -cb on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pram";
	setAttr -k on ".poam";
	setAttr -k on ".prlm";
	setAttr -k on ".polm";
	setAttr -cb on ".prm";
	setAttr -cb on ".pom";
	setAttr -cb on ".pfrm";
	setAttr -cb on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -k on ".bls";
	setAttr -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -cb on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -cb on ".isl";
	setAttr -cb on ".ism";
	setAttr -cb on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
	setAttr -cb on ".rsb";
	setAttr -k on ".ope";
	setAttr -k on ".oppf";
	setAttr -cb on ".hbl";
	setAttr ".dss" -type "string" "lambert1";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultColorMgtGlobals;
	setAttr ".cfe" yes;
	setAttr ".cfp" -type "string" "<MAYA_RESOURCES>/OCIO-configs/Maya2022-default/config.ocio";
	setAttr ".vtn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".vn" -type "string" "ACES 1.0 SDR-video";
	setAttr ".dn" -type "string" "sRGB";
	setAttr ".wsn" -type "string" "ACEScg";
	setAttr ".otn" -type "string" "ACES 1.0 SDR-video (sRGB)";
	setAttr ".potn" -type "string" "ACES 1.0 SDR-video (sRGB)";
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k off ".ctrs" 256;
	setAttr -av -k off ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
	setAttr -k on ".soll";
	setAttr -k on ".sosl";
	setAttr -k on ".bswa";
	setAttr -k on ".shml";
	setAttr -k on ".hwel";
select -ne :ikSystem;
	setAttr -s 4 ".sol";
connectAttr "root_parentConstraint1.ctx" "root.tx";
connectAttr "root_parentConstraint1.cty" "root.ty";
connectAttr "root_parentConstraint1.ctz" "root.tz";
connectAttr "root_parentConstraint1.crx" "root.rx";
connectAttr "root_parentConstraint1.cry" "root.ry";
connectAttr "root_parentConstraint1.crz" "root.rz";
connectAttr "topCon.Skeleton_Display_Type" "root.ovdt";
connectAttr "root.s" "hips.is";
connectAttr "hips.s" "hips_end.is";
connectAttr "root.s" "|root|bottom_torso.is";
connectAttr "bottom_torso_parentConstraint1.ctx" "|root|bottom_torso.tx";
connectAttr "bottom_torso_parentConstraint1.cty" "|root|bottom_torso.ty";
connectAttr "bottom_torso_parentConstraint1.ctz" "|root|bottom_torso.tz";
connectAttr "bottom_torso_parentConstraint1.crx" "|root|bottom_torso.rx";
connectAttr "bottom_torso_parentConstraint1.cry" "|root|bottom_torso.ry";
connectAttr "bottom_torso_parentConstraint1.crz" "|root|bottom_torso.rz";
connectAttr "|root|bottom_torso.s" "|root|bottom_torso|mid_torso.is";
connectAttr "mid_torso_parentConstraint1.ctx" "|root|bottom_torso|mid_torso.tx";
connectAttr "mid_torso_parentConstraint1.cty" "|root|bottom_torso|mid_torso.ty";
connectAttr "mid_torso_parentConstraint1.ctz" "|root|bottom_torso|mid_torso.tz";
connectAttr "mid_torso_parentConstraint1.crx" "|root|bottom_torso|mid_torso.rx";
connectAttr "mid_torso_parentConstraint1.cry" "|root|bottom_torso|mid_torso.ry";
connectAttr "mid_torso_parentConstraint1.crz" "|root|bottom_torso|mid_torso.rz";
connectAttr "|root|bottom_torso|mid_torso.s" "chest.is";
connectAttr "chest_parentConstraint1.ctx" "chest.tx";
connectAttr "chest_parentConstraint1.cty" "chest.ty";
connectAttr "chest_parentConstraint1.ctz" "chest.tz";
connectAttr "chest_parentConstraint1.crx" "chest.rx";
connectAttr "chest_parentConstraint1.cry" "chest.ry";
connectAttr "chest_parentConstraint1.crz" "chest.rz";
connectAttr "chest.s" "neck.is";
connectAttr "neck_orientConstraint1.crx" "neck.rx";
connectAttr "neck_orientConstraint1.cry" "neck.ry";
connectAttr "neck_orientConstraint1.crz" "neck.rz";
connectAttr "neck.s" "|root|bottom_torso|mid_torso|chest|neck|head.is";
connectAttr "head_orientConstraint1.crx" "|root|bottom_torso|mid_torso|chest|neck|head.rx"
		;
connectAttr "head_orientConstraint1.cry" "|root|bottom_torso|mid_torso|chest|neck|head.ry"
		;
connectAttr "head_orientConstraint1.crz" "|root|bottom_torso|mid_torso|chest|neck|head.rz"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.s" "head_end.is";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.ro" "head_orientConstraint1.cro"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.pim" "head_orientConstraint1.cpim"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.jo" "head_orientConstraint1.cjo"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.is" "head_orientConstraint1.is"
		;
connectAttr "head_2.r" "head_orientConstraint1.tg[0].tr";
connectAttr "head_2.ro" "head_orientConstraint1.tg[0].tro";
connectAttr "head_2.pm" "head_orientConstraint1.tg[0].tpm";
connectAttr "head_orientConstraint1.w0" "head_orientConstraint1.tg[0].tw";
connectAttr "neck.ro" "neck_orientConstraint1.cro";
connectAttr "neck.pim" "neck_orientConstraint1.cpim";
connectAttr "neck.jo" "neck_orientConstraint1.cjo";
connectAttr "neck.is" "neck_orientConstraint1.is";
connectAttr "head_1.r" "neck_orientConstraint1.tg[0].tr";
connectAttr "head_1.ro" "neck_orientConstraint1.tg[0].tro";
connectAttr "head_1.pm" "neck_orientConstraint1.tg[0].tpm";
connectAttr "neck_orientConstraint1.w0" "neck_orientConstraint1.tg[0].tw";
connectAttr "chest.s" "right_clavicle.is";
connectAttr "right_clavicle.s" "right_shoulder.is";
connectAttr "right_shoulder.s" "right_elbow.is";
connectAttr "right_elbow.s" "right_wrist.is";
connectAttr "right_wrist_orientConstraint1.crx" "right_wrist.rx";
connectAttr "right_wrist_orientConstraint1.cry" "right_wrist.ry";
connectAttr "right_wrist_orientConstraint1.crz" "right_wrist.rz";
connectAttr "right_wrist.s" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.is"
		;
connectAttr "right_hand_1_orientConstraint1.crx" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.rx"
		;
connectAttr "right_hand_1_orientConstraint1.cry" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.ry"
		;
connectAttr "right_hand_1_orientConstraint1.crz" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.rz"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.s" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.is"
		;
connectAttr "right_hand_2_orientConstraint1.crx" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.rx"
		;
connectAttr "right_hand_2_orientConstraint1.cry" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.ry"
		;
connectAttr "right_hand_2_orientConstraint1.crz" "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.rz"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.s" "right_hand_end.is"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.ro" "right_hand_2_orientConstraint1.cro"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.pim" "right_hand_2_orientConstraint1.cpim"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.jo" "right_hand_2_orientConstraint1.cjo"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.is" "right_hand_2_orientConstraint1.is"
		;
connectAttr "|topCon|right_hand_ik|right_hand_1|right_hand_2.r" "right_hand_2_orientConstraint1.tg[0].tr"
		;
connectAttr "|topCon|right_hand_ik|right_hand_1|right_hand_2.ro" "right_hand_2_orientConstraint1.tg[0].tro"
		;
connectAttr "|topCon|right_hand_ik|right_hand_1|right_hand_2.pm" "right_hand_2_orientConstraint1.tg[0].tpm"
		;
connectAttr "right_hand_2_orientConstraint1.w0" "right_hand_2_orientConstraint1.tg[0].tw"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.ro" "right_hand_1_orientConstraint1.cro"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.pim" "right_hand_1_orientConstraint1.cpim"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.jo" "right_hand_1_orientConstraint1.cjo"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.is" "right_hand_1_orientConstraint1.is"
		;
connectAttr "|topCon|right_hand_ik|right_hand_1.r" "right_hand_1_orientConstraint1.tg[0].tr"
		;
connectAttr "|topCon|right_hand_ik|right_hand_1.ro" "right_hand_1_orientConstraint1.tg[0].tro"
		;
connectAttr "|topCon|right_hand_ik|right_hand_1.pm" "right_hand_1_orientConstraint1.tg[0].tpm"
		;
connectAttr "right_hand_1_orientConstraint1.w0" "right_hand_1_orientConstraint1.tg[0].tw"
		;
connectAttr "right_wrist.ro" "right_wrist_orientConstraint1.cro";
connectAttr "right_wrist.pim" "right_wrist_orientConstraint1.cpim";
connectAttr "right_wrist.jo" "right_wrist_orientConstraint1.cjo";
connectAttr "right_wrist.is" "right_wrist_orientConstraint1.is";
connectAttr "right_hand_ik.r" "right_wrist_orientConstraint1.tg[0].tr";
connectAttr "right_hand_ik.ro" "right_wrist_orientConstraint1.tg[0].tro";
connectAttr "right_hand_ik.pm" "right_wrist_orientConstraint1.tg[0].tpm";
connectAttr "right_wrist_orientConstraint1.w0" "right_wrist_orientConstraint1.tg[0].tw"
		;
connectAttr "right_wrist.tx" "effector5.tx";
connectAttr "right_wrist.ty" "effector5.ty";
connectAttr "right_wrist.tz" "effector5.tz";
connectAttr "right_wrist.opm" "effector5.opm";
connectAttr "chest.s" "left_clavicle.is";
connectAttr "left_clavicle.s" "left_shoulder.is";
connectAttr "left_shoulder.s" "left_elbow.is";
connectAttr "left_elbow.s" "left_wrist.is";
connectAttr "left_wrist_orientConstraint1.crx" "left_wrist.rx";
connectAttr "left_wrist_orientConstraint1.cry" "left_wrist.ry";
connectAttr "left_wrist_orientConstraint1.crz" "left_wrist.rz";
connectAttr "left_wrist.s" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.is"
		;
connectAttr "left_hand_1_orientConstraint1.crx" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.rx"
		;
connectAttr "left_hand_1_orientConstraint1.cry" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.ry"
		;
connectAttr "left_hand_1_orientConstraint1.crz" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.rz"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.s" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.is"
		;
connectAttr "left_hand_2_orientConstraint1.crx" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.rx"
		;
connectAttr "left_hand_2_orientConstraint1.cry" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.ry"
		;
connectAttr "left_hand_2_orientConstraint1.crz" "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.rz"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.s" "left_hand_end.is"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.ro" "left_hand_2_orientConstraint1.cro"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.pim" "left_hand_2_orientConstraint1.cpim"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.jo" "left_hand_2_orientConstraint1.cjo"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.is" "left_hand_2_orientConstraint1.is"
		;
connectAttr "|topCon|left_hand_ik|left_hand_1|left_hand_2.r" "left_hand_2_orientConstraint1.tg[0].tr"
		;
connectAttr "|topCon|left_hand_ik|left_hand_1|left_hand_2.ro" "left_hand_2_orientConstraint1.tg[0].tro"
		;
connectAttr "|topCon|left_hand_ik|left_hand_1|left_hand_2.pm" "left_hand_2_orientConstraint1.tg[0].tpm"
		;
connectAttr "left_hand_2_orientConstraint1.w0" "left_hand_2_orientConstraint1.tg[0].tw"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.ro" "left_hand_1_orientConstraint1.cro"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.pim" "left_hand_1_orientConstraint1.cpim"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.jo" "left_hand_1_orientConstraint1.cjo"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.is" "left_hand_1_orientConstraint1.is"
		;
connectAttr "|topCon|left_hand_ik|left_hand_1.r" "left_hand_1_orientConstraint1.tg[0].tr"
		;
connectAttr "|topCon|left_hand_ik|left_hand_1.ro" "left_hand_1_orientConstraint1.tg[0].tro"
		;
connectAttr "|topCon|left_hand_ik|left_hand_1.pm" "left_hand_1_orientConstraint1.tg[0].tpm"
		;
connectAttr "left_hand_1_orientConstraint1.w0" "left_hand_1_orientConstraint1.tg[0].tw"
		;
connectAttr "left_wrist.ro" "left_wrist_orientConstraint1.cro";
connectAttr "left_wrist.pim" "left_wrist_orientConstraint1.cpim";
connectAttr "left_wrist.jo" "left_wrist_orientConstraint1.cjo";
connectAttr "left_wrist.is" "left_wrist_orientConstraint1.is";
connectAttr "left_hand_ik.r" "left_wrist_orientConstraint1.tg[0].tr";
connectAttr "left_hand_ik.ro" "left_wrist_orientConstraint1.tg[0].tro";
connectAttr "left_hand_ik.pm" "left_wrist_orientConstraint1.tg[0].tpm";
connectAttr "left_wrist_orientConstraint1.w0" "left_wrist_orientConstraint1.tg[0].tw"
		;
connectAttr "left_wrist.tx" "effector4.tx";
connectAttr "left_wrist.ty" "effector4.ty";
connectAttr "left_wrist.tz" "effector4.tz";
connectAttr "left_wrist.opm" "effector4.opm";
connectAttr "chest.ro" "chest_parentConstraint1.cro";
connectAttr "chest.pim" "chest_parentConstraint1.cpim";
connectAttr "chest.rp" "chest_parentConstraint1.crp";
connectAttr "chest.rpt" "chest_parentConstraint1.crt";
connectAttr "chest.jo" "chest_parentConstraint1.cjo";
connectAttr "upper_torso.t" "chest_parentConstraint1.tg[0].tt";
connectAttr "upper_torso.rp" "chest_parentConstraint1.tg[0].trp";
connectAttr "upper_torso.rpt" "chest_parentConstraint1.tg[0].trt";
connectAttr "upper_torso.r" "chest_parentConstraint1.tg[0].tr";
connectAttr "upper_torso.ro" "chest_parentConstraint1.tg[0].tro";
connectAttr "upper_torso.s" "chest_parentConstraint1.tg[0].ts";
connectAttr "upper_torso.pm" "chest_parentConstraint1.tg[0].tpm";
connectAttr "chest_parentConstraint1.w0" "chest_parentConstraint1.tg[0].tw";
connectAttr "|root|bottom_torso|mid_torso.ro" "mid_torso_parentConstraint1.cro";
connectAttr "|root|bottom_torso|mid_torso.pim" "mid_torso_parentConstraint1.cpim"
		;
connectAttr "|root|bottom_torso|mid_torso.rp" "mid_torso_parentConstraint1.crp";
connectAttr "|root|bottom_torso|mid_torso.rpt" "mid_torso_parentConstraint1.crt"
		;
connectAttr "|root|bottom_torso|mid_torso.jo" "mid_torso_parentConstraint1.cjo";
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.t" "mid_torso_parentConstraint1.tg[0].tt"
		;
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.rp" "mid_torso_parentConstraint1.tg[0].trp"
		;
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.rpt" "mid_torso_parentConstraint1.tg[0].trt"
		;
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.r" "mid_torso_parentConstraint1.tg[0].tr"
		;
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.ro" "mid_torso_parentConstraint1.tg[0].tro"
		;
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.s" "mid_torso_parentConstraint1.tg[0].ts"
		;
connectAttr "|topCon|center_cog|bottom_torso|mid_torso.pm" "mid_torso_parentConstraint1.tg[0].tpm"
		;
connectAttr "mid_torso_parentConstraint1.w0" "mid_torso_parentConstraint1.tg[0].tw"
		;
connectAttr "|root|bottom_torso.ro" "bottom_torso_parentConstraint1.cro";
connectAttr "|root|bottom_torso.pim" "bottom_torso_parentConstraint1.cpim";
connectAttr "|root|bottom_torso.rp" "bottom_torso_parentConstraint1.crp";
connectAttr "|root|bottom_torso.rpt" "bottom_torso_parentConstraint1.crt";
connectAttr "|root|bottom_torso.jo" "bottom_torso_parentConstraint1.cjo";
connectAttr "|topCon|center_cog|bottom_torso.t" "bottom_torso_parentConstraint1.tg[0].tt"
		;
connectAttr "|topCon|center_cog|bottom_torso.rp" "bottom_torso_parentConstraint1.tg[0].trp"
		;
connectAttr "|topCon|center_cog|bottom_torso.rpt" "bottom_torso_parentConstraint1.tg[0].trt"
		;
connectAttr "|topCon|center_cog|bottom_torso.r" "bottom_torso_parentConstraint1.tg[0].tr"
		;
connectAttr "|topCon|center_cog|bottom_torso.ro" "bottom_torso_parentConstraint1.tg[0].tro"
		;
connectAttr "|topCon|center_cog|bottom_torso.s" "bottom_torso_parentConstraint1.tg[0].ts"
		;
connectAttr "|topCon|center_cog|bottom_torso.pm" "bottom_torso_parentConstraint1.tg[0].tpm"
		;
connectAttr "bottom_torso_parentConstraint1.w0" "bottom_torso_parentConstraint1.tg[0].tw"
		;
connectAttr "root.ro" "root_parentConstraint1.cro";
connectAttr "root.pim" "root_parentConstraint1.cpim";
connectAttr "root.rp" "root_parentConstraint1.crp";
connectAttr "root.rpt" "root_parentConstraint1.crt";
connectAttr "root.jo" "root_parentConstraint1.cjo";
connectAttr "center_cog.t" "root_parentConstraint1.tg[0].tt";
connectAttr "center_cog.rp" "root_parentConstraint1.tg[0].trp";
connectAttr "center_cog.rpt" "root_parentConstraint1.tg[0].trt";
connectAttr "center_cog.r" "root_parentConstraint1.tg[0].tr";
connectAttr "center_cog.ro" "root_parentConstraint1.tg[0].tro";
connectAttr "center_cog.s" "root_parentConstraint1.tg[0].ts";
connectAttr "center_cog.pm" "root_parentConstraint1.tg[0].tpm";
connectAttr "root_parentConstraint1.w0" "root_parentConstraint1.tg[0].tw";
connectAttr "root.s" "right_leg_1.is";
connectAttr "right_leg_1.s" "right_leg_2.is";
connectAttr "right_leg_2.s" "right_foot_1.is";
connectAttr "right_foot_1_orientConstraint1.crx" "right_foot_1.rx";
connectAttr "right_foot_1_orientConstraint1.cry" "right_foot_1.ry";
connectAttr "right_foot_1_orientConstraint1.crz" "right_foot_1.rz";
connectAttr "right_foot_1.s" "right_foot_2.is";
connectAttr "right_foot_1.ro" "right_foot_1_orientConstraint1.cro";
connectAttr "right_foot_1.pim" "right_foot_1_orientConstraint1.cpim";
connectAttr "right_foot_1.jo" "right_foot_1_orientConstraint1.cjo";
connectAttr "right_foot_1.is" "right_foot_1_orientConstraint1.is";
connectAttr "right_foot_ik.r" "right_foot_1_orientConstraint1.tg[0].tr";
connectAttr "right_foot_ik.ro" "right_foot_1_orientConstraint1.tg[0].tro";
connectAttr "right_foot_ik.pm" "right_foot_1_orientConstraint1.tg[0].tpm";
connectAttr "right_foot_1_orientConstraint1.w0" "right_foot_1_orientConstraint1.tg[0].tw"
		;
connectAttr "right_foot_1.tx" "effector2.tx";
connectAttr "right_foot_1.ty" "effector2.ty";
connectAttr "right_foot_1.tz" "effector2.tz";
connectAttr "right_foot_1.opm" "effector2.opm";
connectAttr "root.s" "left_leg_1.is";
connectAttr "left_leg_1.s" "left_leg_2.is";
connectAttr "left_leg_2.s" "left_foot_1.is";
connectAttr "left_foot_1_orientConstraint1.crx" "left_foot_1.rx";
connectAttr "left_foot_1_orientConstraint1.cry" "left_foot_1.ry";
connectAttr "left_foot_1_orientConstraint1.crz" "left_foot_1.rz";
connectAttr "left_foot_1.s" "left_foot_2.is";
connectAttr "left_foot_1.ro" "left_foot_1_orientConstraint1.cro";
connectAttr "left_foot_1.pim" "left_foot_1_orientConstraint1.cpim";
connectAttr "left_foot_1.jo" "left_foot_1_orientConstraint1.cjo";
connectAttr "left_foot_1.is" "left_foot_1_orientConstraint1.is";
connectAttr "left_foot_ik.r" "left_foot_1_orientConstraint1.tg[0].tr";
connectAttr "left_foot_ik.ro" "left_foot_1_orientConstraint1.tg[0].tro";
connectAttr "left_foot_ik.pm" "left_foot_1_orientConstraint1.tg[0].tpm";
connectAttr "left_foot_1_orientConstraint1.w0" "left_foot_1_orientConstraint1.tg[0].tw"
		;
connectAttr "left_foot_1.tx" "effector3.tx";
connectAttr "left_foot_1.ty" "effector3.ty";
connectAttr "left_foot_1.tz" "effector3.tz";
connectAttr "left_foot_1.opm" "effector3.opm";
connectAttr "topCon.Mesh_Display_Type" "mesh.ovdt";
connectAttr "skinCluster1.og[0]" "garbShape.i";
connectAttr "skinCluster2.og[0]" "crownShape.i";
connectAttr "skinCluster3.og[0]" "legsShape.i";
connectAttr "skinCluster4.og[0]" "headShape.i";
connectAttr "skinCluster5.og[0]" "feetShape.i";
connectAttr "skinCluster6.og[0]" "CubeShape.i";
connectAttr "left_leg_1.msg" "left_leg_ik_handle.hsj";
connectAttr "effector3.hp" "left_leg_ik_handle.hee";
connectAttr "ikRPsolver.msg" "left_leg_ik_handle.hsv";
connectAttr "left_leg_ik_handle_poleVectorConstraint1.ctx" "left_leg_ik_handle.pvx"
		;
connectAttr "left_leg_ik_handle_poleVectorConstraint1.cty" "left_leg_ik_handle.pvy"
		;
connectAttr "left_leg_ik_handle_poleVectorConstraint1.ctz" "left_leg_ik_handle.pvz"
		;
connectAttr "left_leg_ik_handle.pim" "left_leg_ik_handle_poleVectorConstraint1.cpim"
		;
connectAttr "left_leg_1.pm" "left_leg_ik_handle_poleVectorConstraint1.ps";
connectAttr "left_leg_1.t" "left_leg_ik_handle_poleVectorConstraint1.crp";
connectAttr "left_leg_pv.t" "left_leg_ik_handle_poleVectorConstraint1.tg[0].tt";
connectAttr "left_leg_pv.rp" "left_leg_ik_handle_poleVectorConstraint1.tg[0].trp"
		;
connectAttr "left_leg_pv.rpt" "left_leg_ik_handle_poleVectorConstraint1.tg[0].trt"
		;
connectAttr "left_leg_pv.pm" "left_leg_ik_handle_poleVectorConstraint1.tg[0].tpm"
		;
connectAttr "left_leg_ik_handle_poleVectorConstraint1.w0" "left_leg_ik_handle_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "right_leg_1.msg" "right_leg_ik_handle.hsj";
connectAttr "effector2.hp" "right_leg_ik_handle.hee";
connectAttr "ikRPsolver.msg" "right_leg_ik_handle.hsv";
connectAttr "right_leg_ik_handle_poleVectorConstraint1.ctx" "right_leg_ik_handle.pvx"
		;
connectAttr "right_leg_ik_handle_poleVectorConstraint1.cty" "right_leg_ik_handle.pvy"
		;
connectAttr "right_leg_ik_handle_poleVectorConstraint1.ctz" "right_leg_ik_handle.pvz"
		;
connectAttr "right_leg_ik_handle.pim" "right_leg_ik_handle_poleVectorConstraint1.cpim"
		;
connectAttr "right_leg_1.pm" "right_leg_ik_handle_poleVectorConstraint1.ps";
connectAttr "right_leg_1.t" "right_leg_ik_handle_poleVectorConstraint1.crp";
connectAttr "right_leg_pv.t" "right_leg_ik_handle_poleVectorConstraint1.tg[0].tt"
		;
connectAttr "right_leg_pv.rp" "right_leg_ik_handle_poleVectorConstraint1.tg[0].trp"
		;
connectAttr "right_leg_pv.rpt" "right_leg_ik_handle_poleVectorConstraint1.tg[0].trt"
		;
connectAttr "right_leg_pv.pm" "right_leg_ik_handle_poleVectorConstraint1.tg[0].tpm"
		;
connectAttr "right_leg_ik_handle_poleVectorConstraint1.w0" "right_leg_ik_handle_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "transformGeometry1.og" "left_hand_ikShape.cr";
connectAttr "left_shoulder.msg" "left_arm_ik_handle.hsj";
connectAttr "effector4.hp" "left_arm_ik_handle.hee";
connectAttr "ikRPsolver.msg" "left_arm_ik_handle.hsv";
connectAttr "left_arm_ik_handle_poleVectorConstraint1.ctx" "left_arm_ik_handle.pvx"
		;
connectAttr "left_arm_ik_handle_poleVectorConstraint1.cty" "left_arm_ik_handle.pvy"
		;
connectAttr "left_arm_ik_handle_poleVectorConstraint1.ctz" "left_arm_ik_handle.pvz"
		;
connectAttr "left_arm_ik_handle.pim" "left_arm_ik_handle_poleVectorConstraint1.cpim"
		;
connectAttr "left_shoulder.pm" "left_arm_ik_handle_poleVectorConstraint1.ps";
connectAttr "left_shoulder.t" "left_arm_ik_handle_poleVectorConstraint1.crp";
connectAttr "left_arm_pv.t" "left_arm_ik_handle_poleVectorConstraint1.tg[0].tt";
connectAttr "left_arm_pv.rp" "left_arm_ik_handle_poleVectorConstraint1.tg[0].trp"
		;
connectAttr "left_arm_pv.rpt" "left_arm_ik_handle_poleVectorConstraint1.tg[0].trt"
		;
connectAttr "left_arm_pv.pm" "left_arm_ik_handle_poleVectorConstraint1.tg[0].tpm"
		;
connectAttr "left_arm_ik_handle_poleVectorConstraint1.w0" "left_arm_ik_handle_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "right_shoulder.msg" "right_arm_ik_handle.hsj";
connectAttr "effector5.hp" "right_arm_ik_handle.hee";
connectAttr "ikRPsolver.msg" "right_arm_ik_handle.hsv";
connectAttr "right_arm_ik_handle_poleVectorConstraint1.ctx" "right_arm_ik_handle.pvx"
		;
connectAttr "right_arm_ik_handle_poleVectorConstraint1.cty" "right_arm_ik_handle.pvy"
		;
connectAttr "right_arm_ik_handle_poleVectorConstraint1.ctz" "right_arm_ik_handle.pvz"
		;
connectAttr "right_arm_ik_handle.pim" "right_arm_ik_handle_poleVectorConstraint1.cpim"
		;
connectAttr "right_shoulder.pm" "right_arm_ik_handle_poleVectorConstraint1.ps";
connectAttr "right_shoulder.t" "right_arm_ik_handle_poleVectorConstraint1.crp";
connectAttr "right_arm_pv.t" "right_arm_ik_handle_poleVectorConstraint1.tg[0].tt"
		;
connectAttr "right_arm_pv.rp" "right_arm_ik_handle_poleVectorConstraint1.tg[0].trp"
		;
connectAttr "right_arm_pv.rpt" "right_arm_ik_handle_poleVectorConstraint1.tg[0].trt"
		;
connectAttr "right_arm_pv.pm" "right_arm_ik_handle_poleVectorConstraint1.tg[0].tpm"
		;
connectAttr "right_arm_ik_handle_poleVectorConstraint1.w0" "right_arm_ik_handle_poleVectorConstraint1.tg[0].tw"
		;
connectAttr "transformGeometry2.og" "left_arm_pvShape.cr";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "garbSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "garbSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr ":defaultArnoldDisplayDriver.msg" ":defaultArnoldRenderOptions.drivers"
		 -na;
connectAttr ":defaultArnoldFilter.msg" ":defaultArnoldRenderOptions.filt";
connectAttr ":defaultArnoldDriver.msg" ":defaultArnoldRenderOptions.drvr";
connectAttr "Material.oc" "garbSG.ss";
connectAttr "garbShape.iog" "garbSG.dsm" -na;
connectAttr "garbSG.msg" "materialInfo1.sg";
connectAttr "Material.msg" "materialInfo1.m";
connectAttr "garbShapeOrig.w" "skinCluster1.ip[0].ig";
connectAttr "garbShapeOrig.o" "skinCluster1.orggeom[0]";
connectAttr "bindPose1.msg" "skinCluster1.bp";
connectAttr "root.wm" "skinCluster1.ma[0]";
connectAttr "hips.wm" "skinCluster1.ma[1]";
connectAttr "hips_end.wm" "skinCluster1.ma[2]";
connectAttr "|root|bottom_torso.wm" "skinCluster1.ma[3]";
connectAttr "|root|bottom_torso|mid_torso.wm" "skinCluster1.ma[4]";
connectAttr "chest.wm" "skinCluster1.ma[5]";
connectAttr "neck.wm" "skinCluster1.ma[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.wm" "skinCluster1.ma[7]"
		;
connectAttr "right_clavicle.wm" "skinCluster1.ma[9]";
connectAttr "right_shoulder.wm" "skinCluster1.ma[10]";
connectAttr "right_elbow.wm" "skinCluster1.ma[11]";
connectAttr "left_clavicle.wm" "skinCluster1.ma[16]";
connectAttr "left_shoulder.wm" "skinCluster1.ma[17]";
connectAttr "left_elbow.wm" "skinCluster1.ma[18]";
connectAttr "right_leg_1.wm" "skinCluster1.ma[23]";
connectAttr "left_leg_1.wm" "skinCluster1.ma[27]";
connectAttr "root.liw" "skinCluster1.lw[0]";
connectAttr "hips.liw" "skinCluster1.lw[1]";
connectAttr "hips_end.liw" "skinCluster1.lw[2]";
connectAttr "|root|bottom_torso.liw" "skinCluster1.lw[3]";
connectAttr "|root|bottom_torso|mid_torso.liw" "skinCluster1.lw[4]";
connectAttr "chest.liw" "skinCluster1.lw[5]";
connectAttr "neck.liw" "skinCluster1.lw[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.liw" "skinCluster1.lw[7]"
		;
connectAttr "right_clavicle.liw" "skinCluster1.lw[9]";
connectAttr "right_shoulder.liw" "skinCluster1.lw[10]";
connectAttr "right_elbow.liw" "skinCluster1.lw[11]";
connectAttr "left_clavicle.liw" "skinCluster1.lw[16]";
connectAttr "left_shoulder.liw" "skinCluster1.lw[17]";
connectAttr "left_elbow.liw" "skinCluster1.lw[18]";
connectAttr "right_leg_1.liw" "skinCluster1.lw[23]";
connectAttr "left_leg_1.liw" "skinCluster1.lw[27]";
connectAttr "root.obcc" "skinCluster1.ifcl[0]";
connectAttr "hips.obcc" "skinCluster1.ifcl[1]";
connectAttr "hips_end.obcc" "skinCluster1.ifcl[2]";
connectAttr "|root|bottom_torso.obcc" "skinCluster1.ifcl[3]";
connectAttr "|root|bottom_torso|mid_torso.obcc" "skinCluster1.ifcl[4]";
connectAttr "chest.obcc" "skinCluster1.ifcl[5]";
connectAttr "neck.obcc" "skinCluster1.ifcl[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.obcc" "skinCluster1.ifcl[7]"
		;
connectAttr "right_clavicle.obcc" "skinCluster1.ifcl[9]";
connectAttr "right_shoulder.obcc" "skinCluster1.ifcl[10]";
connectAttr "right_elbow.obcc" "skinCluster1.ifcl[11]";
connectAttr "left_clavicle.obcc" "skinCluster1.ifcl[16]";
connectAttr "left_shoulder.obcc" "skinCluster1.ifcl[17]";
connectAttr "left_elbow.obcc" "skinCluster1.ifcl[18]";
connectAttr "right_leg_1.obcc" "skinCluster1.ifcl[23]";
connectAttr "left_leg_1.obcc" "skinCluster1.ifcl[27]";
connectAttr "root.msg" "skinCluster1.ptt";
connectAttr "root.msg" "bindPose1.m[0]";
connectAttr "hips.msg" "bindPose1.m[1]";
connectAttr "hips_end.msg" "bindPose1.m[2]";
connectAttr "|root|bottom_torso.msg" "bindPose1.m[3]";
connectAttr "|root|bottom_torso|mid_torso.msg" "bindPose1.m[4]";
connectAttr "chest.msg" "bindPose1.m[5]";
connectAttr "neck.msg" "bindPose1.m[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.msg" "bindPose1.m[7]";
connectAttr "right_clavicle.msg" "bindPose1.m[9]";
connectAttr "right_shoulder.msg" "bindPose1.m[10]";
connectAttr "right_elbow.msg" "bindPose1.m[11]";
connectAttr "right_wrist.msg" "bindPose1.m[12]";
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.msg" "bindPose1.m[13]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.msg" "bindPose1.m[14]"
		;
connectAttr "left_clavicle.msg" "bindPose1.m[16]";
connectAttr "left_shoulder.msg" "bindPose1.m[17]";
connectAttr "left_elbow.msg" "bindPose1.m[18]";
connectAttr "left_wrist.msg" "bindPose1.m[19]";
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.msg" "bindPose1.m[20]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.msg" "bindPose1.m[21]"
		;
connectAttr "right_leg_1.msg" "bindPose1.m[23]";
connectAttr "right_leg_2.msg" "bindPose1.m[24]";
connectAttr "right_foot_1.msg" "bindPose1.m[25]";
connectAttr "left_leg_1.msg" "bindPose1.m[27]";
connectAttr "left_leg_2.msg" "bindPose1.m[28]";
connectAttr "left_foot_1.msg" "bindPose1.m[29]";
connectAttr "head_end.msg" "bindPose1.m[31]";
connectAttr "right_foot_2.msg" "bindPose1.m[38]";
connectAttr "left_foot_2.msg" "bindPose1.m[39]";
connectAttr "right_hand_end.msg" "bindPose1.m[44]";
connectAttr "left_hand_end.msg" "bindPose1.m[45]";
connectAttr "bindPose1.w" "bindPose1.p[0]";
connectAttr "bindPose1.m[0]" "bindPose1.p[1]";
connectAttr "bindPose1.m[1]" "bindPose1.p[2]";
connectAttr "bindPose1.m[0]" "bindPose1.p[3]";
connectAttr "bindPose1.m[3]" "bindPose1.p[4]";
connectAttr "bindPose1.m[4]" "bindPose1.p[5]";
connectAttr "bindPose1.m[5]" "bindPose1.p[6]";
connectAttr "bindPose1.m[6]" "bindPose1.p[7]";
connectAttr "bindPose1.m[5]" "bindPose1.p[9]";
connectAttr "bindPose1.m[9]" "bindPose1.p[10]";
connectAttr "bindPose1.m[10]" "bindPose1.p[11]";
connectAttr "bindPose1.m[11]" "bindPose1.p[12]";
connectAttr "bindPose1.m[12]" "bindPose1.p[13]";
connectAttr "bindPose1.m[13]" "bindPose1.p[14]";
connectAttr "bindPose1.m[5]" "bindPose1.p[16]";
connectAttr "bindPose1.m[16]" "bindPose1.p[17]";
connectAttr "bindPose1.m[17]" "bindPose1.p[18]";
connectAttr "bindPose1.m[18]" "bindPose1.p[19]";
connectAttr "bindPose1.m[19]" "bindPose1.p[20]";
connectAttr "bindPose1.m[20]" "bindPose1.p[21]";
connectAttr "bindPose1.m[0]" "bindPose1.p[23]";
connectAttr "bindPose1.m[23]" "bindPose1.p[24]";
connectAttr "bindPose1.m[24]" "bindPose1.p[25]";
connectAttr "bindPose1.m[0]" "bindPose1.p[27]";
connectAttr "bindPose1.m[27]" "bindPose1.p[28]";
connectAttr "bindPose1.m[28]" "bindPose1.p[29]";
connectAttr "bindPose1.m[7]" "bindPose1.p[31]";
connectAttr "bindPose1.m[25]" "bindPose1.p[38]";
connectAttr "bindPose1.m[29]" "bindPose1.p[39]";
connectAttr "bindPose1.m[14]" "bindPose1.p[44]";
connectAttr "bindPose1.m[21]" "bindPose1.p[45]";
connectAttr "root.bps" "bindPose1.wm[0]";
connectAttr "hips.bps" "bindPose1.wm[1]";
connectAttr "hips_end.bps" "bindPose1.wm[2]";
connectAttr "|root|bottom_torso.bps" "bindPose1.wm[3]";
connectAttr "|root|bottom_torso|mid_torso.bps" "bindPose1.wm[4]";
connectAttr "chest.bps" "bindPose1.wm[5]";
connectAttr "neck.bps" "bindPose1.wm[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.bps" "bindPose1.wm[7]"
		;
connectAttr "right_clavicle.bps" "bindPose1.wm[9]";
connectAttr "right_shoulder.bps" "bindPose1.wm[10]";
connectAttr "right_elbow.bps" "bindPose1.wm[11]";
connectAttr "right_wrist.bps" "bindPose1.wm[12]";
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.bps" "bindPose1.wm[13]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.bps" "bindPose1.wm[14]"
		;
connectAttr "left_clavicle.bps" "bindPose1.wm[16]";
connectAttr "left_shoulder.bps" "bindPose1.wm[17]";
connectAttr "left_elbow.bps" "bindPose1.wm[18]";
connectAttr "left_wrist.bps" "bindPose1.wm[19]";
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.bps" "bindPose1.wm[20]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.bps" "bindPose1.wm[21]"
		;
connectAttr "right_leg_1.bps" "bindPose1.wm[23]";
connectAttr "right_leg_2.bps" "bindPose1.wm[24]";
connectAttr "right_foot_1.bps" "bindPose1.wm[25]";
connectAttr "left_leg_1.bps" "bindPose1.wm[27]";
connectAttr "left_leg_2.bps" "bindPose1.wm[28]";
connectAttr "left_foot_1.bps" "bindPose1.wm[29]";
connectAttr "head_end.bps" "bindPose1.wm[31]";
connectAttr "right_foot_2.bps" "bindPose1.wm[38]";
connectAttr "left_foot_2.bps" "bindPose1.wm[39]";
connectAttr "right_hand_end.bps" "bindPose1.wm[44]";
connectAttr "left_hand_end.bps" "bindPose1.wm[45]";
connectAttr "crownShapeOrig.w" "skinCluster2.ip[0].ig";
connectAttr "crownShapeOrig.o" "skinCluster2.orggeom[0]";
connectAttr "|root|bottom_torso.wm" "skinCluster2.ma[3]";
connectAttr "|root|bottom_torso|mid_torso.wm" "skinCluster2.ma[4]";
connectAttr "chest.wm" "skinCluster2.ma[5]";
connectAttr "neck.wm" "skinCluster2.ma[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.wm" "skinCluster2.ma[7]"
		;
connectAttr "head_end.wm" "skinCluster2.ma[8]";
connectAttr "right_clavicle.wm" "skinCluster2.ma[9]";
connectAttr "right_shoulder.wm" "skinCluster2.ma[10]";
connectAttr "left_clavicle.wm" "skinCluster2.ma[16]";
connectAttr "left_shoulder.wm" "skinCluster2.ma[17]";
connectAttr "|root|bottom_torso.liw" "skinCluster2.lw[3]";
connectAttr "|root|bottom_torso|mid_torso.liw" "skinCluster2.lw[4]";
connectAttr "chest.liw" "skinCluster2.lw[5]";
connectAttr "neck.liw" "skinCluster2.lw[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.liw" "skinCluster2.lw[7]"
		;
connectAttr "head_end.liw" "skinCluster2.lw[8]";
connectAttr "right_clavicle.liw" "skinCluster2.lw[9]";
connectAttr "right_shoulder.liw" "skinCluster2.lw[10]";
connectAttr "left_clavicle.liw" "skinCluster2.lw[16]";
connectAttr "left_shoulder.liw" "skinCluster2.lw[17]";
connectAttr "|root|bottom_torso.obcc" "skinCluster2.ifcl[3]";
connectAttr "|root|bottom_torso|mid_torso.obcc" "skinCluster2.ifcl[4]";
connectAttr "chest.obcc" "skinCluster2.ifcl[5]";
connectAttr "neck.obcc" "skinCluster2.ifcl[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.obcc" "skinCluster2.ifcl[7]"
		;
connectAttr "head_end.obcc" "skinCluster2.ifcl[8]";
connectAttr "right_clavicle.obcc" "skinCluster2.ifcl[9]";
connectAttr "right_shoulder.obcc" "skinCluster2.ifcl[10]";
connectAttr "left_clavicle.obcc" "skinCluster2.ifcl[16]";
connectAttr "left_shoulder.obcc" "skinCluster2.ifcl[17]";
connectAttr "bindPose1.msg" "skinCluster2.bp";
connectAttr "|root|bottom_torso.msg" "skinCluster2.ptt";
connectAttr "legsShapeOrig.w" "skinCluster3.ip[0].ig";
connectAttr "legsShapeOrig.o" "skinCluster3.orggeom[0]";
connectAttr "root.wm" "skinCluster3.ma[0]";
connectAttr "hips.wm" "skinCluster3.ma[1]";
connectAttr "hips_end.wm" "skinCluster3.ma[2]";
connectAttr "|root|bottom_torso.wm" "skinCluster3.ma[3]";
connectAttr "right_leg_1.wm" "skinCluster3.ma[23]";
connectAttr "right_leg_2.wm" "skinCluster3.ma[24]";
connectAttr "right_foot_1.wm" "skinCluster3.ma[25]";
connectAttr "right_foot_2.wm" "skinCluster3.ma[26]";
connectAttr "left_leg_1.wm" "skinCluster3.ma[27]";
connectAttr "left_leg_2.wm" "skinCluster3.ma[28]";
connectAttr "left_foot_1.wm" "skinCluster3.ma[29]";
connectAttr "left_foot_2.wm" "skinCluster3.ma[30]";
connectAttr "root.liw" "skinCluster3.lw[0]";
connectAttr "hips.liw" "skinCluster3.lw[1]";
connectAttr "hips_end.liw" "skinCluster3.lw[2]";
connectAttr "|root|bottom_torso.liw" "skinCluster3.lw[3]";
connectAttr "right_leg_1.liw" "skinCluster3.lw[23]";
connectAttr "right_leg_2.liw" "skinCluster3.lw[24]";
connectAttr "right_foot_1.liw" "skinCluster3.lw[25]";
connectAttr "right_foot_2.liw" "skinCluster3.lw[26]";
connectAttr "left_leg_1.liw" "skinCluster3.lw[27]";
connectAttr "left_leg_2.liw" "skinCluster3.lw[28]";
connectAttr "left_foot_1.liw" "skinCluster3.lw[29]";
connectAttr "left_foot_2.liw" "skinCluster3.lw[30]";
connectAttr "root.obcc" "skinCluster3.ifcl[0]";
connectAttr "hips.obcc" "skinCluster3.ifcl[1]";
connectAttr "hips_end.obcc" "skinCluster3.ifcl[2]";
connectAttr "|root|bottom_torso.obcc" "skinCluster3.ifcl[3]";
connectAttr "right_leg_1.obcc" "skinCluster3.ifcl[23]";
connectAttr "right_leg_2.obcc" "skinCluster3.ifcl[24]";
connectAttr "right_foot_1.obcc" "skinCluster3.ifcl[25]";
connectAttr "right_foot_2.obcc" "skinCluster3.ifcl[26]";
connectAttr "left_leg_1.obcc" "skinCluster3.ifcl[27]";
connectAttr "left_leg_2.obcc" "skinCluster3.ifcl[28]";
connectAttr "left_foot_1.obcc" "skinCluster3.ifcl[29]";
connectAttr "left_foot_2.obcc" "skinCluster3.ifcl[30]";
connectAttr "bindPose1.msg" "skinCluster3.bp";
connectAttr "right_leg_2.msg" "skinCluster3.ptt";
connectAttr "headShapeOrig.w" "skinCluster4.ip[0].ig";
connectAttr "headShapeOrig.o" "skinCluster4.orggeom[0]";
connectAttr "|root|bottom_torso|mid_torso.wm" "skinCluster4.ma[4]";
connectAttr "chest.wm" "skinCluster4.ma[5]";
connectAttr "neck.wm" "skinCluster4.ma[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.wm" "skinCluster4.ma[7]"
		;
connectAttr "head_end.wm" "skinCluster4.ma[8]";
connectAttr "right_clavicle.wm" "skinCluster4.ma[9]";
connectAttr "left_clavicle.wm" "skinCluster4.ma[16]";
connectAttr "|root|bottom_torso|mid_torso.liw" "skinCluster4.lw[4]";
connectAttr "chest.liw" "skinCluster4.lw[5]";
connectAttr "neck.liw" "skinCluster4.lw[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.liw" "skinCluster4.lw[7]"
		;
connectAttr "head_end.liw" "skinCluster4.lw[8]";
connectAttr "right_clavicle.liw" "skinCluster4.lw[9]";
connectAttr "left_clavicle.liw" "skinCluster4.lw[16]";
connectAttr "|root|bottom_torso|mid_torso.obcc" "skinCluster4.ifcl[4]";
connectAttr "chest.obcc" "skinCluster4.ifcl[5]";
connectAttr "neck.obcc" "skinCluster4.ifcl[6]";
connectAttr "|root|bottom_torso|mid_torso|chest|neck|head.obcc" "skinCluster4.ifcl[7]"
		;
connectAttr "head_end.obcc" "skinCluster4.ifcl[8]";
connectAttr "right_clavicle.obcc" "skinCluster4.ifcl[9]";
connectAttr "left_clavicle.obcc" "skinCluster4.ifcl[16]";
connectAttr "bindPose1.msg" "skinCluster4.bp";
connectAttr "|root|bottom_torso|mid_torso.msg" "skinCluster4.ptt";
connectAttr "feetShapeOrig.w" "skinCluster5.ip[0].ig";
connectAttr "feetShapeOrig.o" "skinCluster5.orggeom[0]";
connectAttr "right_leg_2.wm" "skinCluster5.ma[24]";
connectAttr "right_foot_1.wm" "skinCluster5.ma[25]";
connectAttr "right_foot_2.wm" "skinCluster5.ma[26]";
connectAttr "left_leg_2.wm" "skinCluster5.ma[28]";
connectAttr "left_foot_1.wm" "skinCluster5.ma[29]";
connectAttr "left_foot_2.wm" "skinCluster5.ma[30]";
connectAttr "right_leg_2.liw" "skinCluster5.lw[24]";
connectAttr "right_foot_1.liw" "skinCluster5.lw[25]";
connectAttr "right_foot_2.liw" "skinCluster5.lw[26]";
connectAttr "left_leg_2.liw" "skinCluster5.lw[28]";
connectAttr "left_foot_1.liw" "skinCluster5.lw[29]";
connectAttr "left_foot_2.liw" "skinCluster5.lw[30]";
connectAttr "right_leg_2.obcc" "skinCluster5.ifcl[24]";
connectAttr "right_foot_1.obcc" "skinCluster5.ifcl[25]";
connectAttr "right_foot_2.obcc" "skinCluster5.ifcl[26]";
connectAttr "left_leg_2.obcc" "skinCluster5.ifcl[28]";
connectAttr "left_foot_1.obcc" "skinCluster5.ifcl[29]";
connectAttr "left_foot_2.obcc" "skinCluster5.ifcl[30]";
connectAttr "bindPose1.msg" "skinCluster5.bp";
connectAttr "right_foot_2.msg" "skinCluster5.ptt";
connectAttr "CubeShapeOrig.w" "skinCluster6.ip[0].ig";
connectAttr "CubeShapeOrig.o" "skinCluster6.orggeom[0]";
connectAttr "|root|bottom_torso.wm" "skinCluster6.ma[3]";
connectAttr "|root|bottom_torso|mid_torso.wm" "skinCluster6.ma[4]";
connectAttr "chest.wm" "skinCluster6.ma[5]";
connectAttr "neck.wm" "skinCluster6.ma[6]";
connectAttr "right_clavicle.wm" "skinCluster6.ma[9]";
connectAttr "right_shoulder.wm" "skinCluster6.ma[10]";
connectAttr "right_elbow.wm" "skinCluster6.ma[11]";
connectAttr "right_wrist.wm" "skinCluster6.ma[12]";
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.wm" "skinCluster6.ma[13]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.wm" "skinCluster6.ma[14]"
		;
connectAttr "right_hand_end.wm" "skinCluster6.ma[15]";
connectAttr "left_clavicle.wm" "skinCluster6.ma[16]";
connectAttr "left_shoulder.wm" "skinCluster6.ma[17]";
connectAttr "left_elbow.wm" "skinCluster6.ma[18]";
connectAttr "left_wrist.wm" "skinCluster6.ma[19]";
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.wm" "skinCluster6.ma[20]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.wm" "skinCluster6.ma[21]"
		;
connectAttr "left_hand_end.wm" "skinCluster6.ma[22]";
connectAttr "|root|bottom_torso.liw" "skinCluster6.lw[3]";
connectAttr "|root|bottom_torso|mid_torso.liw" "skinCluster6.lw[4]";
connectAttr "chest.liw" "skinCluster6.lw[5]";
connectAttr "neck.liw" "skinCluster6.lw[6]";
connectAttr "right_clavicle.liw" "skinCluster6.lw[9]";
connectAttr "right_shoulder.liw" "skinCluster6.lw[10]";
connectAttr "right_elbow.liw" "skinCluster6.lw[11]";
connectAttr "right_wrist.liw" "skinCluster6.lw[12]";
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.liw" "skinCluster6.lw[13]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.liw" "skinCluster6.lw[14]"
		;
connectAttr "right_hand_end.liw" "skinCluster6.lw[15]";
connectAttr "left_clavicle.liw" "skinCluster6.lw[16]";
connectAttr "left_shoulder.liw" "skinCluster6.lw[17]";
connectAttr "left_elbow.liw" "skinCluster6.lw[18]";
connectAttr "left_wrist.liw" "skinCluster6.lw[19]";
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.liw" "skinCluster6.lw[20]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.liw" "skinCluster6.lw[21]"
		;
connectAttr "left_hand_end.liw" "skinCluster6.lw[22]";
connectAttr "|root|bottom_torso.obcc" "skinCluster6.ifcl[3]";
connectAttr "|root|bottom_torso|mid_torso.obcc" "skinCluster6.ifcl[4]";
connectAttr "chest.obcc" "skinCluster6.ifcl[5]";
connectAttr "neck.obcc" "skinCluster6.ifcl[6]";
connectAttr "right_clavicle.obcc" "skinCluster6.ifcl[9]";
connectAttr "right_shoulder.obcc" "skinCluster6.ifcl[10]";
connectAttr "right_elbow.obcc" "skinCluster6.ifcl[11]";
connectAttr "right_wrist.obcc" "skinCluster6.ifcl[12]";
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1.obcc" "skinCluster6.ifcl[13]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|right_clavicle|right_shoulder|right_elbow|right_wrist|right_hand_1|right_hand_2.obcc" "skinCluster6.ifcl[14]"
		;
connectAttr "right_hand_end.obcc" "skinCluster6.ifcl[15]";
connectAttr "left_clavicle.obcc" "skinCluster6.ifcl[16]";
connectAttr "left_shoulder.obcc" "skinCluster6.ifcl[17]";
connectAttr "left_elbow.obcc" "skinCluster6.ifcl[18]";
connectAttr "left_wrist.obcc" "skinCluster6.ifcl[19]";
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1.obcc" "skinCluster6.ifcl[20]"
		;
connectAttr "|root|bottom_torso|mid_torso|chest|left_clavicle|left_shoulder|left_elbow|left_wrist|left_hand_1|left_hand_2.obcc" "skinCluster6.ifcl[21]"
		;
connectAttr "left_hand_end.obcc" "skinCluster6.ifcl[22]";
connectAttr "bindPose1.msg" "skinCluster6.bp";
connectAttr "|root|bottom_torso.msg" "skinCluster6.ptt";
connectAttr "makeNurbCircle1.oc" "transformGeometry1.ig";
connectAttr "makeNurbSphere1.os" "transformGeometry2.ig";
connectAttr "topCon.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[0].dn";
connectAttr "mesh.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[1].dn";
connectAttr "topConShape.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[2].dn";
connectAttr "root.msg" "MayaNodeEditorSavedTabsInfo.tgi[0].ni[3].dn";
connectAttr "garbSG.pa" ":renderPartition.st" -na;
connectAttr "Material.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "crownShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "legsShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "headShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "feetShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "CubeShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "right_leg_pvShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "left_leg_pvShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "left_arm_pvShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "right_arm_pvShape.iog" ":initialShadingGroup.dsm" -na;
connectAttr "ikRPsolver.msg" ":ikSystem.sol" -na;
// End of fencerRig.ma
